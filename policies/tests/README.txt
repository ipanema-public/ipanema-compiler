How to run run_test.sh ?
I assume you launch the script from the rep policies/tests

run all test (all phases):
../../run_test.sh all

run all typechecking test:
../../run_test.sh all/typechecker
or
../../run_test.sh typechecker

run attach test (for all phases):
../../run_test.sh attach

run attach test (typechecker phase)
../../run_test.sh attach/typechecker



notes

policies/tests/detach/verifier/detach_partial_ko.ipanema: Failure 
pass the verification but should not.

I removed old tests and only have simple one in this rep


