scheduler Linux3 = {
  const int default_quanta = 20;

  type sched_group = struct {
    set<core> cores; // a sched group is a list of cores
    lazy int load = sum (cores.load); // and it has a load
    int total_capacity;// When to compute? = sum (cores.capacity); // and a capacity

    bool imbalanced; // is the load balanced INSIDE the group? (hack for taskset, see NOTE in __LoadBalancing)
    bool overloaded;
  };

  type sched_domain = struct {
    int level;
    set<sched_group> groups; // a sched domain is a list of sched_groups
  };

  process = {
    int quanta;
    int vruntime;      // time spent running on CPUs
    int priority;         // niceness
    int load;             // "load" metric for load balancing
    set<core> allowed_cores; // taskset rules - is a bitmask
  }

  void update_process(process);

  core = {
    int system id;
    int load;
    int capacity; // not all cores can handle the same "load"
    set<sched_domain> sd; // scheduling domains: hierarchy for load balancing

    processes = {
        RUNNING process current;     // current running thread
        READY queue<process> ready:order = {
          lowest vruntime
        };     // ready threads - in rr select "fifo" but we don't want fifo here
        BLOCKED set<process> blocked;
        TERMINATED process terminated;
    }
  }

  cores = { //global ?
    ACTIVE set<core> active_cores; // cores with task(s)
    IDLE set<core> idle_cores; // should be a bitmask rather than a queue - cores with no task
    SLEEPING set<core> sleeping_cores; // should be a bitmask rather than a queue - cores with no task
  }

  handler (process_event e) {
    //Note: all "On" events run on all cores and are not always synchronized

    //End of period
    On tick {
      current.quanta--;

      if (current.quanta <= 0) {
        update_process(current);
        current => ready; // place it back in ready list
      }
    }

    On yield {
      update_process(current);
      current => ready;
    }

    On block {
      update_process(current);
      current => blocked;
    }

    //Simple unblocking policy. The linux policy is more complex, but this illustrate

    On unblock {
      if(!empty(idle_cores)) {
        e.target => first(idle_cores).ready;
      } else if(!empty(sleeping_cores)) {
        e.target => first(sleeping_cores).ready;
      } else {
        e.target => ready;
      }
    }

    On schedule {
	    last_context_switch = now();
          first(ready) => current;
          current.quanta = default_quanta;  // TODO: adaptative tick count
    }

  } // handler(process_event e)

  handler (core_event ce) {
       On idle.enter { //No task left in ready, transition from ACTIVE to IDLE
          if (empty(ready)) {
              if (empty(idle_cores)) {
                ce.target => idle_cores;
              } else {
                ce.target => sleeping_cores; //Deep sleep, disable all events
              }
          }
       }
      On idle.exit {
          ce.target => active_cores;
          if(!empty(sleeping_cores))
            first(sleeping_cores) => idle_cores;
      }
  }

  interface = {
      void init() {
  		system_cores() => active_cores;
  	}
      void attach (process p, int wcet, int period) {}
  }
  
  void update_process(process p) { // Update load and vruntime
        p.load = 1024 - 1024 * (default_quanta - p.quanta) / default_quanta; //%used quanta * 1024
        p.load = p.load * p.priority;
        p.vruntime += (now() - last_context_switch)/p.priority; 
  }
}
