;;; ipanema-mode.el --- Ipanema major mode ipanema-mode

;;; Commentary:
;; Ipanema major mode ipanema-mode

;;; Code:

(defvar ipanema-mode-hook nil)

(defvar ipanema-mode-map
  (make-keymap)
  "Keymap for ipanema-mode.")

(add-to-list 'auto-mode-alist '("\\.ipanema\\'" . ipanema-mode))

;; lock-keywords regexp
;; (regexp-opt '("const" "system" "shared" "On" "if" "else" "in" "to" "order" "break" "return" "foreach" "do" "until" "handler" "synchronized" "READY" "RUNNING" "BLOCKED" "TERMINATED" "ACTIVE" "SLEEPING") t)
;; lock-type regexp
;; (regexp-opt '("scheduler" "domain" "group" "core" "thread" "int" "time" "void" "bool" "set" "core_event" "thread_event") t)
;; lock-constant
;; (regexp-opt '("true" "false" "NULL") t)
;; lock-builtin regexp
;; (regexp-opt '("load" "cload" "count" "valid" "min" "max" "first" "highest" "lowest" "now") t)
;; lock-operator regexp
;; (regexp-opt '("=" "+" "-" "*" "/" ".") t)

(defconst ipanema-font-lock-keywords
  (list
   '("\\<\\(ACTIVE\\|BLOCKED\\|On\\|R\\(?:EADY\\|UNNING\\)\\|SLEEPING\\|TERMINATED\\|break\\|const\\|do\\|else\\|foreach\\|handler\\|i[fn]\\|order\\|return\\|s\\(?:hared\\|y\\(?:nchronized\\|stem\\)\\)\\|to\\|until\\)\\>" . font-lock-keyword-face)
   '("\\<\\(bool\\|core\\(?:_event\\)?\\|domain\\|group\\|int\\|s\\(?:cheduler\\|et\\)\\|t\\(?:hread\\(?:_event\\)?\\|ime\\)\\|void\\)\\>" . font-lock-type-face)
   '("\\<\\(NULL\\|\\(?:fals\\|tru\\)e\\)\\>" . font-lock-constant-face)
   '("\\<\\(c\\(?:load\\|ount\\)\\|first\\|highest\\|lo\\(?:ad\\|west\\)\\|m\\(?:ax\\|in\\)\\|now\\|valid\\)\\>" . font-lock-builtin-face)
   '("\\<[A-Za-z_][A-Za-z0-0_]*\\>" . font-lock-variable-name-face)))

(defvar ipanema-mode-syntax-table
  (let ((st (make-syntax-table)))
    (modify-syntax-entry ?_ "w" st)
    (modify-syntax-entry ?* ". 23b" st)
    (modify-syntax-entry ?/ ". 124" st)
    (modify-syntax-entry ?\n ">" st)
    st)
  "Syntax table for ipanema-mode.")

(defun ipanema-mode ()
  "Major mode for editing Ipanema programs."
  (interactive)
  (kill-all-local-variables)
  (set-syntax-table ipanema-mode-syntax-table)
  (use-local-map ipanema-mode-map)
  (set (make-local-variable 'font-lock-defaults) '(ipanema-font-lock-keywords))
  (setq major-mode 'ipanema-mode)
  (setq mode-name "ipanema")
  (run-hooks 'ipanema-mode-hook))

(provide 'ipanema-mode)

;;; ipanema-mode.el ends here
