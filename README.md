To run the program please follow the instructions 

1. First you must install Ocaml and the associated tools using:

```
 sudo apt install ocaml ocamlbuild menhir
```

2. Compile with bytecode and native version with respectively

```
 make
 make native
```

3. Test

```
 make policies
```