#! /bin/bash

OS=`uname -s`
COMP=./main.d.byte

if [[ "x$OS" = "xLinux" ]]; then
COMP="$(dirname $(readlink -f $0))/$COMP"
else
COMP="$(dirname $0)/$COMP"
fi

# output colors/format definitions if terminal
if [[ -t 1 && "x$OS" = "xLinux" ]] ; then
    default="\e[0m"
    default_color="\e[39m"
    default_bcolor="\e[49m"
    red="\e[31m"
    green="\e[32m"
    yellow="\e[93m"
    bold="\e[1m"
    dim="\e[2m"
    bblack="\e[40m"
fi

# stages enumeration
STAGE_CGEN=0
STAGE_POST=1
STAGE_VERI=2
STAGE_TYPE=3
STAGE_PARS=4
STAGE_BEG=5

# success/failure counters
nr_succ=0
nr_fail=0

# usage: get_expected_result filename
# if success expected, print "ok", else "ko" (if filename ends with "_ko.*"
# return 0 if file extension recognized, 1 otherwise
function get_expected_result {
    filename=$1

    if [[ $filename = *.bossa ]]
    then
	expected=$(echo ${TEST%.bossa} | rev | cut -d'_' -f1 | rev)
    elif  [[ $TEST = *.ipanema ]]
    then
	expected=$(echo ${TEST%.ipanema} | rev | cut -d'_' -f1 | rev)
    elif  [[ $TEST = *.ipa ]]
    then
	expected=$(echo ${TEST%.ipa} | rev | cut -d'_' -f1 | rev)
    else
	return 1
    fi

    if [[ $expected != "ko" ]]
    then
	expected="ok"
    fi
    echo $expected
    return 0
}

# usage: get_expected_stage filename
# return the expected stage based on the directory of the file
function get_expected_stage {
    filename=$1

	if [[ "x$OS" == "xLinux" ]]; then
	    dir=$(basename $(dirname $(readlink -f  $filename)))
	else
		dir="$(basename $(dirname $filename))"
	fi

    if [[ $dir == "parser" ]] ; then
	return $STAGE_PARS
    fi
    if [[ $dir == "typechecker" ]] ; then
	return $STAGE_TYPE
    fi
    if [[ $dir == "verifier" ]] ; then
	return $STAGE_VERI
    fi
    if [[ $dir == "postprocessor" ]] ; then
	return $STAGE_POST
    fi
    if [[ $dir == "c-generator" ]] ; then
	return $STAGE_CGEN
    fi
    return $STAGE_CGEN
}

# usage: print_stages logfile
# print success/fail for each stage, parsing the logfile passed in argument
function print_stages {
    logfile=$1
    stage=$STAGE_BEG
    # check if parsing failed
    grep 'Parsing - ok' $logfile > /dev/null
    ret=$?
    if [[ $ret -ne 0 ]] ; then
	echo -e "${red}Parsing ko $default|"
    else
	echo -e "${green}Parsing ok $default|"
	stage=$((stage-1))
    fi
    # check if type checking failed
    grep 'Type checking - ok' $logfile > /dev/null
    ret=$?
    if [[ $ret -ne 0 ]] ; then
	echo -e " ${red}Type checking ko $default|"
    else
	echo -e " ${green}Type checking ok $default|"
	stage=$((stage-1))
    fi
    # check if verification failed
    grep 'Verifying - ok' $logfile > /dev/null
    ret=$?
    if [[ $ret -ne 0 ]] ; then
	echo -e " ${red}Verification ko $default|"
    else
	echo -e " ${green}Verification ok $default|"
	stage=$((stage-1))
    fi
    # check if postprocessing failed
    grep 'Postprocessing - ok' $logfile > /dev/null
    ret=$?
    if [[ $ret -ne 0 ]] ; then
	echo -e " ${red}Postprocessing ko $default|"
    else
	echo -e " ${green}Postprocessing ok $default|"
	stage=$((stage-1))
    fi
    # check if C generation failed
    grep 'Generating C code - ok' $logfile > /dev/null
    ret=$?
    if [[ $ret -ne 0 ]] ; then
	echo -e " ${red}C code generation ko $default"
    else
	echo -e " ${green}C code generation ok $default"
	stage=$((stage-1))
    fi
    return $stage
}

# usage: success_or_failure exp real ok
# if   ok="ok" then return 0 if real >= exp, 1 otherwise
# else return 0 if real == exp-1, 1 otherwise
function success_or_failure {
    exp=$1
    real=$2
    ok=$3
    if [[ $ok == "ok" ]] ; then
	if [[ $real -le $exp ]] ; then
	    return 0
	else
	    return 1
	fi
    else
	if [[ $real -eq $((exp+1)) ]] ; then
	    return 0
	else
	    return 1
	fi
    fi
}


################################
###     Script beginning     ###
################################

# for each test file
while [[ $# -gt 0 ]]
do
    # get test file and expected result
    TEST=$1
    shift
    expected=$(get_expected_result $TEST)
    if [[ $? -ne 0 ]] ; then continue ; fi
    get_expected_stage $TEST
    fail_at=$?

    # run test
    #echo "$COMP $DEBUG $TEST > $TEST.log 2> $TEST.err"
    echo -n "Testing $TEST: "
    $COMP $DEBUG $TEST > $TEST.log 2> $TEST.err
    err=$?

    # get ending stage
    fail_str=$(print_stages $TEST.log)
    failed_at=$?

    # check success or failure and print
    success_or_failure $fail_at $failed_at $expected
    succ=$?
    mod_str=$(echo -e "${red}Module build KO$default")
    if [[ $succ -eq 0 ]] ; then
	# compile module
	if [[ !$err && "x$OS" = "xLinux" ]] ; then
		pushd c-code > /dev/null
		MOD=`basename $TEST .ipanema`.ko
		COMPLOG=`basename $TEST .ipanema`.mod.log
		make -s -S $MOD 2> $COMPLOG > $COMPLOG
		mod=$?
		popd > /dev/null

		if [[ $mod -eq 0 ]] ; then
			mod_str=$(echo -e "${green}Module build OK$default")

			echo -e "${bold}${green}Success${default}"
			nr_succ=$((nr_succ+1))
		else
			echo -e "${bold}${red}Failure${default}"
			nr_fail=$((nr_fail+1))
		fi
	else
		echo -e "${bold}${green}Success${default}"
		nr_succ=$((nr_succ+1))
	fi

    else
	echo -e "${bold}${red}Failure${default}"
	nr_fail=$((nr_fail+1))
    fi
    echo -en $fail_str
    echo " | $mod_str"
done

# print test summary
echo -e "${bold}=== \
${bblack}${yellow}Tests: $((nr_fail+nr_succ)), \
${green}Success: $nr_succ, \
${red}Failure: ${nr_fail}\
${default_color}${default_bcolor} ===${default}"
