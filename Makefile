.PHONY:: comp native cb
.PHONY:: main.d.byte _build/compiler/main.native clean linux3

comp main.d.byte:
	ocamlbuild -cflag -g -use-menhir -libs unix compiler/main.d.byte
	ln -sf _build/compiler/main.d.byte main.d.byte

native: cb

cb: _build/compiler/main.native
	ln -sf $^ $@

_build/compiler/main.native:
	ocamlbuild -use-menhir -libs unix compiler/`basename $@`

build:
	ocamlbuild -libs str gen.native

t: comp
	./main.d.byte ./policies/main/verifier/lockverif_shared_variable.bossa
try:
	./gen.native checker/input/test.bossa
z3:
	z3 -smt2 checker/output/a.smt2

clean:
	rm -rf _build
	find policies -name '*.err' -delete -o -name '*.log' -delete

DIRS=$(shell find compiler -type d)
INCLUDE=$(DIRS:%=-I %)

-include Makefile.override

FILE?=policies/smp/sosp.ipanema
debug: main.d.byte
	ocamldebug $(INCLUDE) main.d.byte $(FILE)

test: _build/compiler/main.native
	./_build/compiler/main.native -d $(IPAFLAGS) $(FILE)

EUROSYSFILES=$(shell find policies/eurosys2020 -name "*.ipanema" -type f)
EXAMPLEFILES=$(shell find policies/smp policies/eurosys2020 -name "*.ipanema" -type f)
TESTFILES=$(shell find policies/tests -name "*.ipanema" -type f)
.PHONY:: $(EXAMPLEFILES) $(TESTFILES)

tests: $(TESTFILES) main.d.byte
	@./run_tests.sh $(TESTFILES)

policies-dev: $(EXAMPLEFILES) main.d.byte
	@DEBUG="-dev" ./run_tests.sh $(EXAMPLEFILES)

policies policies-prod: $(EXAMPLEFILES) main.d.byte
	@DEBUG="-prod" ./run_tests.sh $(EXAMPLEFILES)

eurosys2020: $(EUROSYSFILES) main.d.byte
	@DEBUG="-prod" ./run_tests.sh $(EUROSYSFILES)

# $(TESTFILES):
# 	-$(MAKE) test FILE=$@

#####################################
#
# Targets to test
#
#####################################
linux3: cb
	./cb policies/smp/linux3.ipanema


cleanlogs:
	rm -rf policies/smp/*.log policies/smp/*.err
	rm -rf policies/tests/*/*.log policies/tests/*/*.err

archive:
	git archive --prefix ipanema-compiler/ -o ../ipanema-compiler.tar HEAD
