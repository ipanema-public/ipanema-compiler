(* From the document "Another attempt at event types for virtual schedulers" *)

module B = Objects
module VB = Vobjects
module CS = Class_state

type entry = Class_state.classname * Vobjects.vtype * Vobjects.modified

(* ------------------------------ Misc ------------------------------ *)

let has_target = ref true (* false for bossa.schedule *)
let current_name = ref (None : string option)

let internal_error str = raise (Error.Error(str^": internal error"))

let error str =
  match !current_name with
    Some x -> raise (Error.Error("In handler "^x^": "^str))
  | None -> raise (Error.Error(str))

let entry2c (cls,vt,modd) =
  Printf.sprintf "%s %s%s" (VB.vt2c vt) (String.sub (CS.class2c cls) 0 2)
    (if modd = VB.MODIFIED then "!" else "")

let type2c (input,output) =
  Printf.sprintf "%s -> %s"
    (Aux.set2c (List.map entry2c input))
    (Aux.set2c (List.map entry2c output))

let print_config config =
  Printf.printf "%s\n" (Aux.set2c (List.map entry2c config))

type valid = INVALID | VALID of (VB.vtype * CS.classname) list

let print_triple str (preempt,target,others) =
  let entry l =
    Aux.set2c
      (List.map
	 (function (v,c) ->
	   Printf.sprintf "%s : %s" (CS.class2c c) (VB.vt2c v))
	 l) in
  let tpentry = function
      INVALID ->  "-"
    | VALID tpentry -> entry tpentry in
  Printf.printf "%s (%s | %s | %s)\n" str
    (tpentry preempt) (tpentry target) (Aux.set2c (List.map entry others))


let print_pre_triple (preempt,target,others) =
  let entry l =
    Aux.set2c
      (List.map
	 (function (v,c) ->
	   Printf.sprintf "%s : %s" (CS.class2c c) (VB.vp2c v))
	 l) in
  Printf.printf "(%s | %s | %s)\n" (entry preempt) (entry target)
    (Aux.set2c (List.map entry others))

let print_pre_pre_triple (preempt,target,others) =
  let entry l =
    Aux.set2c
      (List.map
	 (function (v,c) ->
	   Printf.sprintf "%s : %s" (CS.class2c c) (VB.vp2c v))
	 l) in
  Printf.printf "(%s | %s | %s)\n" (entry preempt) (entry target)
    (entry others)

(* -------------------------- Preprocessing ------------------------- *)

(* section 2 *)

let preprocess_one input output =
  List.map
    (function (ivp,icls) ->
      let output_entries =
	List.filter (function (ovp,ocls) -> ivp = ovp) output in
      match output_entries with
	[(ovp,ocls)] -> (ivp,icls,ocls)
      (* variable stays where it is if it is not mentioned *)
      |	[] -> (ivp,icls,icls)
      |	_ -> error "multiple uses not yet handled")
    input
    
let explode entry =
  Aux.flat_map
    (function
	(cls,VB.MEMBER(vprocs),_) ->
	  List.map (function vp -> (vp,cls)) vprocs
      | _ -> [])
    entry
    
let preprocess ((inputs,outputs) : (entry list list * entry list list)) =
  let e_inputs = List.map explode inputs in
  let e_outputs = List.map explode outputs in
  List.map2
    (function orig_input ->
      function input ->
	(Aux.option_filter
	   (function
	       (icls,VB.EMPTY,imod)  -> Some icls
	     | _ -> None)
	   orig_input,
	 List.map (function output -> preprocess_one input output) e_outputs))
    inputs e_inputs

(* -------------------------- Partitioning -------------------------- *)
(* section 4, part 1 *)

let add_to_preempt item (preempt,target,other) = (item::preempt,target,other)
let add_to_target  item (preempt,target,other) = (preempt,item::target,other)
let add_to_other   item (preempt,target,other) = (preempt,target,item::other)

(* any triple in which target is not empty, but tgt does not appear is
discarded.  Furthermore, if there is a source, the source and target
should be on the same scheduler.  This doesn't ensure that the source and
target are distributed to the same scheduler later if they are both in
other, but then they don't move, so perhaps it doesn't matter where they are *)
let filter_target (triples :
		     (((VB.vproc * CS.classname) list) *
			((VB.vproc * CS.classname) list) *
			((VB.vproc * CS.classname) list)) list) =
  let contains thing entry =
    List.exists (function (x,_) -> VB.vp_member thing [x]) entry in
  List.filter
    (function (preempt,target,other) ->
      let src_and_tgt =
	(List.exists (contains VB.TGT) [preempt;target;other]) &&
	(List.exists (contains VB.SRC) [preempt;target;other]) in
      if src_and_tgt
      then
	if contains VB.TGT preempt
	then target = [] && contains VB.SRC preempt
	else if contains VB.TGT target
	then contains VB.SRC target
	else if contains VB.TGT other
	then target = [] && contains VB.SRC target
	else false
      else (target = [] || (contains VB.TGT target)))
    triples

let rec distribute = function
    [] -> [([],[],[])]
  | ((p,c1,c2)::r) when c1 = c2 ->
      let rest = distribute r in
      Aux.union_list
	((List.map (add_to_target (p,c1)) rest) @
	 (List.map (add_to_other (p,c1)) rest))
  | ((p,CS.RUNNING,CS.READY)::r)   ->
      let rest = distribute r in
      Aux.union_list
	((List.map (add_to_preempt (p,CS.RUNNING)) rest) @
	 (List.map (add_to_target (p,CS.RUNNING)) rest))
  | ((p,c1,c2)::r) ->
      let rest = distribute r in
      (List.map (add_to_target (p,c1)) rest)

let do_distribute (preprocessed :
		     (VB.vproc * CS.classname * CS.classname) list) =
  let res = (distribute preprocessed) in
  if !has_target
  then filter_target res
  else res

(* ------------------------- Partition other ------------------------ *)

let glue x = function
    (xs :: xss) -> (x::xs) :: xss
  | _ -> internal_error "glue"

let rec partitions = function
    [] -> [[]]
  | [x] -> [[[x]]]
  | (x :: xs) ->
      (List.map (glue x) (partitions xs)) @
      (List.map (function y -> [x] :: y) (partitions xs))
	
let partition_other triples =
  Aux.flat_map
    (function
	(preempt,target,[]) -> [(preempt,target,[])]
      | (preempt,target,other) ->
	  List.map (function other -> (preempt,target,other))
	    (partitions other))
    triples
    
(* -------------------- Regroup class information ------------------- *)

let regroup entry =
  let regroup_class cls =
    let (vps,_) =
      List.split (List.filter (function (_,c) -> c = cls) entry) in
    match vps with
      [] -> []
    | vps -> [(VB.MEMBER vps,cls)] in
  (regroup_class CS.NOWHERE) @ (regroup_class CS.RUNNING) @
  (regroup_class CS.READY) @ (regroup_class CS.BLOCKED)

let regroup_all triples =
  List.map
    (function (preempt,target,other) ->
      (regroup preempt,regroup target,List.map regroup other))
    triples

(* ----------------- Detect invalid tuple components ---------------- *)
(* The notes say that a target component that does not contain target
should be considered as invalid, but such triples should have been discarded
by filter_triples *)

let detect_invalid triples =
  List.map
    (function
	([],[],other) -> (INVALID,INVALID,other)
      |	([],target,other) -> (INVALID,VALID target,other)
      |	(preempt,[],other) -> (VALID preempt,INVALID,other)
      |	(preempt,target,other) -> (VALID preempt,VALID target,other))
    triples

(* --------------------------- Add empties -------------------------- *)
(* if running is nonempty in any element, add that it is empty in the others *)

let add_empty_running (preempt,target,other) =
  let test_for_running entry =
    List.exists (function (_,CS.RUNNING) -> true | _ -> false) entry in
  let new_tpentry = function
      INVALID -> INVALID
    | VALID tpentry -> VALID ((VB.EMPTY,CS.RUNNING)::tpentry) in
  let do_preempt_target target_preempt other = function
      INVALID -> None
    | VALID(entry) ->
	if test_for_running entry
	then
	  Some (new_tpentry target_preempt,
		List.map (function other -> (VB.EMPTY,CS.RUNNING)::other)
		  other)
	else None in
  let do_other preempt target other =
    if List.exists test_for_running other
    then
      Some (new_tpentry preempt, new_tpentry target,
	    List.map
	      (function other ->
		if test_for_running other
		then other
		else (VB.EMPTY,CS.RUNNING)::other)
	      other)
    else None in
  match do_preempt_target target other preempt with
    None ->
      (match do_preempt_target preempt other target with
	None ->
	  (match do_other preempt target other with
	    None -> (preempt,target,other)
	  | Some res -> res)
      |	Some (new_preempt,new_other) -> (new_preempt,target,new_other))
  | Some (new_target,new_other) -> (preempt,new_target,new_other)

(* for an empty class, add that it is empty everywhere *)

let add_empty_classes empties (preempt,target,other) =
  let empty_entries = List.map (function cls -> (VB.EMPTY,cls)) empties in
  let new_tpentry = function
      INVALID -> INVALID
    | VALID tpentry -> VALID (empty_entries@tpentry) in
  (new_tpentry preempt,new_tpentry target,
   List.map (function other -> empty_entries@other) other)

(* normalize class information *)
let class_order e1 e2 =
  match (e1,e2) with
    (e1,e2) when e1 = e2 -> 0
  | ((_,c1),(_,c2)) -> if CS.less_than(c1,c2) then 1 else -1

let normalize_triple (preempt,target,other) =
  let new_tpentry = function
      INVALID -> INVALID
    | VALID tpentry -> VALID (List.sort class_order tpentry) in
  (new_tpentry preempt,new_tpentry target,
   List.map (function other -> List.sort class_order other) other)

(* add empties and S variables for unknown things *)

let new_svar sctr =
  let c = !sctr in
  sctr := c + 1;
  VB.MEMBER([VB.VP(VB.S(c,"e_s"),[])])

let add_empties_and_S sctr current_info =
  let not_found =
    List.filter
      (function cls ->
	not(List.exists (function (_,cls1) -> cls = cls1) current_info))
      (CS.all_classes_nowhere true) in
  let new_entries =
    List.fold_left
      (function rest ->
	function cls ->
	  let svar = new_svar sctr in
	  (List.map (function l -> (svar,cls) :: l) rest)
	    @ (List.map (function l -> (VB.EMPTY,cls) :: l) rest))
      [[]] not_found in
  List.map (List.sort class_order)
    (List.map (function new_entry -> current_info @ new_entry) new_entries)

let add_all_empties_and_S (preempt,target,other) =
  let sctr = ref 0 in
  let new_tpentry = function
      INVALID -> [INVALID]
    | VALID tpentry ->
	List.map (function x -> VALID x) (add_empties_and_S sctr tpentry) in
  let new_preempt = new_tpentry preempt in
  let new_target = new_tpentry target in
  let new_other =
    List.map (function other -> add_empties_and_S sctr other) other in
  let all_other =
    List.map List.rev
      (List.fold_left
	 (function rest ->
	   function other ->
	     Aux.flat_map
	       (function other ->
		 List.map
		   (function rest -> other :: rest)
		   rest)
	       other)
	 [[]] new_other) in
  Aux.flat_map
    (function preempt ->
      Aux.flat_map
	(function target ->
	  List.map
	    (function other -> (preempt,target,other))
	    all_other)
	new_target)
    new_preempt
    
let add_empties empties triples =
  Aux.union_list
    (List.map
       (function triple ->
	 let empty_running = add_empty_running triple in
	 let empty_classes =
	   normalize_triple (add_empty_classes empties empty_running) in
	 add_all_empties_and_S empty_classes)
       triples)

(* ----------------------------- Step 1 ----------------------------- *)

let step1 entries =
  let empties_preprocessed = preprocess entries in
  Aux.flat_map
    (function (empties,preprocessed) ->
      Aux.flat_map
	(function one_preprocessed ->
	  let triples = do_distribute one_preprocessed in
	  let triples = partition_other triples in
	  let triples = regroup_all triples in
	  let triples = detect_invalid triples in
	  let triples = add_empties empties triples in
	  List.map (function t -> (empties,t)) triples)
	preprocessed)
    empties_preprocessed
    
(* ------------------------------------------------------------------ *)

(* Step 2: creating the output configurations for the possible input
configurations *)

(* --------------------------- Get outputs -------------------------- *)

let new_svar_o sctr =
  let c = !sctr in
  sctr := c + 1;
  VB.MEMBER([VB.VP(VB.S(c,"o"),[])])

let instantiate_unknowns outputs =
  Aux.union_list
    (Aux.flat_map
       (function (output : entry list) ->
	 let sctr = ref 0 in
	 List.fold_left
	   (function rest ->
	     function
		 (cls,VB.UNKNOWN,modd) ->
		   let svar = new_svar_o sctr in
		   (List.map (function rest -> (cls,VB.EMPTY,modd) :: rest)
		     rest) @
		   (List.map (function rest -> (cls,svar,modd) :: rest)
		     rest)
	       | x -> (List.map (function rest -> x :: rest) rest))
	   [[]] output)
       outputs)

(* it is not clear that Quasi.get_outputs would do the right thing with a
type in which there are unknowns.  But fortunately there are none in the
inputs created here, only members and empties *)
let get_output nm types (preempt,target,other) =
  let reorder entry =
    List.map (function (vt,cls) -> (cls,vt,VB.UNMODIFIED)) entry in
  let other_entries =
    List.map
      (function other -> let other = reorder other in (other,other))
      other in
  let pnm = Ast.mkEVENT_NAME(["preempt"],false,0) in
  match (preempt,target) with
    (INVALID,INVALID) -> [other_entries]
  | (VALID preempt,INVALID) ->
      let preempt = reorder preempt in
      let preempt_output =
	instantiate_unknowns(Quasi.get_outputs false preempt types pnm) in
      List.map
	(function preempt_output -> (preempt,preempt_output) :: other_entries)
	preempt_output
  | (INVALID,VALID target) ->
      let target = reorder target in
      let target_output =
	instantiate_unknowns(Quasi.get_outputs true target types nm) in
      List.map
	(function target_output -> (target,target_output) :: other_entries)
	target_output
  | (VALID preempt,VALID target) ->
      let preempt = reorder preempt in
      let target = reorder target in
      let preempt_output =
	instantiate_unknowns(Quasi.get_outputs false preempt types pnm) in
      let target_output =
	instantiate_unknowns(Quasi.get_outputs true target types nm) in
      Aux.flat_map
	(function preempt_output ->
	  List.map
	    (function target_output ->
	      (preempt,preempt_output)::(target,target_output)
	      ::other_entries)
	    target_output)
	preempt_output
	
(* ------------------- The special case of running ------------------ *)
(* running is empty in the output if it is nonempty in either preempt or
target, and it is empty in everything returned by get_output *)

let get_running_empty_output (preempt,target,others) =
  let tp_entry = function
      INVALID -> false
    | VALID entry ->
	List.exists
	  (function (VB.MEMBER(_),CS.RUNNING) -> true | _ -> false)
	  entry in
  tp_entry preempt || tp_entry target

(* ----------------- Convert configurations to types ---------------- *)

let create_final_process_variable entry =
  let drop_s = VB.drop_vp (function VB.S(_,_) -> true | _ -> false) in
  let drop_p = VB.drop_vp (function VB.P(_) -> true | _ -> false) in
  let all_variables : VB.vproc list =
    Aux.union_list
      (Aux.flat_map
	 (function (cls,vt,_) -> drop_s(VB.known_contents vt))
	 entry) in
  let some_variables : VB.vproc list = Aux.union_list (drop_p all_variables) in
  match all_variables with
    [x] -> x
  | [] -> error "create_final_process_variable should not give []"
  | l ->
      (match some_variables with
	[] -> VB.EQUAL (List.sort compare l)
      |	[x] -> x
      |	l1 -> VB.EQUAL (List.sort compare l1))

let class_order2 e1 e2 =
  match (e1,e2) with
    (e1,e2) when e1 = e2 -> 0
  | ((c1,_,_),(c2,_,_)) -> if CS.less_than(c1,c2) then 1 else -1

let compute_state entry =
  let get_info cls =
    let (_,info,_) =
      List.find (function (cls1,info,_) -> cls = cls1) entry in
    info in
  if get_info CS.RUNNING = VB.EMPTY
  then if get_info CS.READY = VB.EMPTY
  then CS.BLOCKED
  else CS.READY
  else CS.RUNNING

let process_empty running_empty_output empties sched_outputs =
  let all_empty cls entry (* an input or output config *) =
    List.for_all
      (function (_,VB.EMPTY,_) -> true
	| (cls1,_,_) -> not (cls = cls1))
      entry in
  let empty_cls cls scheds = List.for_all (all_empty cls) scheds in
  let empty_running_input = List.mem CS.RUNNING empties in
  let empty_ready_input = List.mem CS.READY empties in
  let empty_running_output =
    (empty_cls CS.RUNNING sched_outputs) &&
    (running_empty_output || (List.mem CS.RUNNING empties)) in
  let empty_ready_output =
    (List.mem CS.READY empties) && (empty_cls CS.READY sched_outputs) in
  (empty_running_input,empty_ready_input,
   empty_running_output,empty_ready_output)

let create_type (running_empty_output : bool) empties iolist =
  let (inputs,outputs) = List.split iolist in
  (* empties *)
  let (empty_running_input,empty_ready_input,
       empty_running_output,empty_ready_output) =
    process_empty running_empty_output empties outputs in
  (* nonempties *)
  let one_scheduler config =
    (create_final_process_variable config,compute_state config) in
  let input = List.map one_scheduler inputs in
  let output = List.map one_scheduler outputs in
  let regroup cls entry =
    let (vps,_) =
      List.split (List.filter (function (_,c) -> c = cls) entry) in
    match vps with
      [] -> (cls,VB.UNKNOWN,VB.MODIFIED)
    | vps -> (cls,VB.MEMBER vps,VB.MODIFIED) in
  let mkempty cls = (cls,VB.EMPTY,VB.MODIFIED) in
  (* create a type *)
  let process_entry er ed entry =
    [(if er then mkempty CS.RUNNING else regroup CS.RUNNING entry);
      (if ed then mkempty CS.READY else regroup CS.READY entry);
      (regroup CS.BLOCKED entry)] in
  (((process_entry empty_running_input empty_ready_input input) : entry list),
   ((process_entry empty_running_output empty_ready_output output) : entry list))

(* ------------------- Effect of forwardImmediate ------------------- *)
(* For forward immediate, we have to know what the child scheduler might do.
At the moment, there is only one list of possible output states associated
with each rule.  Thus, we have to insist that there is only one state
to which the event can be forwarded in a given event type, ie schedulers in
only one state can receive the event.  This is fine for an event that has
a target, because only the target can receive the event.  For other events,
we have to check that there is only one possible state of the recipient, ie
only one state contains process variables. *)

(* succeeds if there is only one possible receiving state *)
let get_receiving_state input =
  let contains thing entry =
    List.find
      (function
	  (_,VB.MEMBER(l),_) -> VB.vp_member thing l
	| _ -> false)
      entry in
  let (cls,_,_) =
    try contains VB.TGT input
    with Not_found ->
      (try contains VB.SRC input
      with Not_found ->
	let all =
	  List.filter
	    (function (_,VB.MEMBER(l),_) -> true | _ -> false)
	    input in
	(match all with
	  [x] -> x
	| _ ->
	    print_config input;
	    error "only one possible forwardImmediate source supported")) in
  cls

(* yet another function for instantiating unknowns with [] and not [] *)
let final_instantiate (input,output) =
  let sctr = ref 0 in
  List.fold_left2
    (function rest ->
      function (cin,info_in,min) as x ->
	function (cout,info_out,mout) as y ->
	  match (info_in,info_out) with
	    (VB.UNKNOWN,VB.UNKNOWN) ->
	      let svar = new_svar_o sctr in
	      (List.map
		 (function (rest_in,rest_out) ->
		   let empty = VB.EMPTY in
		   ((cin,empty,min)::rest_in,(cout,empty,mout)::rest_out))
		 rest) @
	      (List.map
		 (function (rest_in,rest_out) ->
		   ((cin,svar,min)::rest_in,(cout,svar,mout)::rest_out))
		 rest)
	  | (VB.UNKNOWN,o) ->
	      let svar = new_svar_o sctr in
	      (List.map
		 (function (rest_in,rest_out) ->
		   ((cin,VB.EMPTY,min)::rest_in,y::rest_out))
		 rest) @
	      (List.map
		 (function (rest_in,rest_out) ->
		   ((cin,svar,min)::rest_in,y::rest_out))
		 rest)
	  | (i,VB.UNKNOWN) ->
	      let svar = new_svar_o sctr in
	      (List.map
		 (function (rest_in,rest_out) ->
		   (x::rest_in,(cout,VB.EMPTY,mout)::rest_out))
		 rest) @
	      (List.map
		 (function (rest_in,rest_out) ->
		   (x::rest_in,(cout,svar,mout)::rest_out))
		 rest)
	  | _ ->
	      List.map
		(function (rest_in,rest_out) -> (x::rest_in,y::rest_out))
		rest)
    [([],[])] input output

(* for each rule, compute the scheduler state of the lhs and rhs, return an
environment mapping lhs scheduler states to all possible rhs scheduler states*)
let abstract_maps types =
  let types =
    Aux.union_list (List.flatten (List.map final_instantiate types)) in
  let class_order (c1,_) (c2,_) =
    if c1 = c2 then 0 else if CS.less_than(c1,c2) then 1 else -1 in
  let scheduler_state_types =
    List.sort class_order
      (List.map
	 (function (in_entry,out_entry) ->
	   (compute_state in_entry, compute_state out_entry))
	 types) in
  let rec loop = function
      [] -> []
    | (cin,cout)::rest ->
	(match loop rest with
	  [] -> [(cin,[cout])]
	| ((cin1,cout_list)::rest1) as x ->
	    if cin = cin1
	    then (cin1,Aux.union [cout] cout_list)::rest1
	    else (cin,[cout])::x) in
  loop scheduler_state_types

(* no nowhere/terminated for virtual schedulers, but the event type
processing in verifier/events.ml expects there to be information about it.
add it at the end to eliminate instantiation with and without a process
variable *)
(* empty is better for psclasses2vsclasses *)
let dummy_term_entry = [(CS.TERMINATED,VB.EMPTY,VB.UNMODIFIED)]

let add_terminated entry =
  [(CS.NOWHERE,VB.UNKNOWN,VB.UNMODIFIED)] @
  entry @ dummy_term_entry

(* put it together: compute the set of maps, compute the class receiving the
event, and annotate the rule with the possible outputs according to the
input *)
let add_output_info nm types =
  let env = abstract_maps types in
  List.map
    (function (input,output) ->
      let cls = get_receiving_state input in
      let outputs =
	try List.assoc cls env
	with Not_found ->
	  error (Printf.sprintf "%s not found in env" (CS.class2c cls)) in
      (nm,(add_terminated input,outputs),add_terminated output))
    types

(* ------------------ Abstract over blocked classes ----------------- *)

(* don't care about quasi types for VSes *)
(* terminated is also equivalent to blocked at this level *)

let terminated2blocked (config : entry list) =
  let (term,not_term) =
    List.partition (function (CS.TERMINATED,vt,modd) -> true | _ -> false)
      config in
  match term with
    [] -> config
  | [(_,vtt,modt)] ->
      let (block,not_block_term) =
	List.partition (function (CS.BLOCKED,vt,modd) -> true | _ -> false)
	  not_term in
      (match block with
	[] -> not_term@((CS.BLOCKED,vtt,modt)::dummy_term_entry)
      |	[(_,vtb,modb)] ->
	  let union_vtype = function
	      (VB.MEMBER(l1),VB.MEMBER(l2)) -> VB.MEMBER(Aux.union l1 l2)
	    | (VB.MEMBER(l1),x) -> VB.MEMBER(l1)
	    | (x,VB.MEMBER(l2)) -> VB.MEMBER(l2)
	    | (VB.UNKNOWN,x) -> VB.UNKNOWN
	    | (x,VB.UNKNOWN) -> VB.UNKNOWN
	    | (VB.EMPTY,x) -> VB.EMPTY
	    | (x,VB.EMPTY) -> VB.EMPTY
	    | (VB.BOT,VB.BOT) -> VB.BOT in
	  let ormod modt = function
	      VB.MODIFIED -> VB.MODIFIED
	    | _ -> modt in
	  not_block_term@
	  ((CS.BLOCKED,union_vtype(vtt,vtb),ormod modt modb)::dummy_term_entry)
      |	_ -> raise (Error.Error "multiple blockeds"))
  | _ -> raise (Error.Error "multiple terminateds")
	
let psclasses2vsclasses events =
  List.map
    (function (nm,inputs_outputs) ->
      (nm,
       Aux.union_list
	 (List.map
	    (function (q,inputs,outputs) ->
	      (List.map terminated2blocked inputs,
	       List.map terminated2blocked outputs))
	    inputs_outputs)))
    events

(* --------------------------- Entry point -------------------------- *)

let update_name nm =
  let snm = Ast.event_name2c nm in
  Ast.mkEVENT_NAME([B.id2c(B.fresh_idx snm)],false,0)

(* not correct???, but good enough for now *)
let unflatten all_types =
  Compile_events.collect_vs_types all_types
  (*let res =
    List.fold_left
      (function rest ->
	function (nm,(input,extra),output) ->
	  match rest with
	    [] -> [(nm,[([(input,extra)],[output])])]
	  | (nm1,info)::rest when Ast.event_name_equal nm nm1 ->
	      (nm,([(input,extra)],[output]) :: info)::rest
	  | _ -> (nm,[([(input,extra)],[output])]) :: rest)
      [] all_types in
  List.rev res*)

let ps2vs events =
  let events = psclasses2vsclasses events in
  let flat_events =
    Aux.flat_map
      (function (nm,inputs_outputs) ->
	Aux.flat_map
	  (function (inputs,outputs) ->
	    List.map (function input -> (nm,input,outputs)) inputs)
	  inputs_outputs)
      events in
  unflatten
    (Aux.flat_map
       (function (nm,inputs_outputs) ->
	 let snm = Ast.event_name2c nm in
	 current_name := Some snm;
	 has_target := not(snm = "bossa.schedule");
	 let res =
	   Aux.union_list
	     (Aux.flat_map
		(function (input,output) ->
		  let tripleslist_per_input = step1 (input,output) in
		  Aux.union_list
		    (Aux.flat_map
		       (function (empties,tripleslist) ->
			 Aux.flat_map
			   (function triple ->
			     let running_empty_output =
			       get_running_empty_output triple in
			     let outputs = get_output nm flat_events triple in
			     List.map
			       (create_type running_empty_output empties)
			       outputs)
			   tripleslist)
		       tripleslist_per_input))
		inputs_outputs) in
	 add_output_info nm res)
       events)
