type entry = Class_state.classname * Vobjects.vtype * Vobjects.modified

val ps2vs :
    (Ast.event_name *
       (Objects.isquasi * entry list list * entry list list) list)
    list ->
      (Ast.event_name *
	 (Objects.isquasi *
	    (entry list * Class_state.classname list) list *
	    entry list list)
	 list)
	list
