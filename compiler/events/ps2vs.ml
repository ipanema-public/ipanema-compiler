(* Functions:

1.  subsets: Generates a set of all subsets of the set of processes.
    target_subsets: Generates a set of all subsets of the set of processes
      that contain the source and/or target, if these are mentioned by the
      event type at all.
    partitions: Generates a set of all partitions of a set of processes.

2.  restrict_ps (pi): Restricts an event type to a set of processes,
    producing a process scheduler type.

3.  restrict_vs (pi_v): Restricts an event type to a set of processes,
    producing a new event type.

4.  concretize: (sigma) Expand unknown components of a process scheduler
    type into empty and non-empty components.

5.  abstract (alpha): Infers a set of pairs of starting and ending states
    from a set of concrete process scheduler types.

6.  create_template (gamma): create a template from an event type.

7.  inject (delta): updates a virtual scheduler type with contents
    descriptions according to a set of pairs of input and output scheduler
    states.

8.  adjust_modified: a class is modified if anything changes from the
    input configuration to the output configuration.
*)

module B = Objects
module VB = Vobjects
module CS = Class_state
open Format

(* ------------------------------ Misc ------------------------------ *)

let rec lookup3 x = function
    [] -> raise (Error.Error("lookup3: unknown class"));
  | ((y,v,_)::r) -> if x = y then v else lookup3 x r

let rec lookup x = function
    [] -> raise Not_found
  | ((y,v)::r) -> if x = y then v else lookup x r

let internal_error str = raise (Error.Error(str^": internal error"))

(* ---------------------------- Subsets ----------------------------- *)

let rec collect_ps = function
    VB.EQUAL(l) -> Aux.flat_map collect_ps l
  | x -> [x]

(* second argument is a subset/partition of the first argument *)
let check_src_tgt l0 l =
  let src_present = VB.vp_member VB.SRC l0 in
  let tgt_present = VB.vp_member VB.TGT l0 in
  List.filter
    (function
	[] -> false
      |	s ->
	  let srcok = if src_present then VB.vp_member VB.SRC s else true in
	  let tgtok = if tgt_present then VB.vp_member VB.TGT s else true in
	  srcok && tgtok)
    l

let src_tgt_present l0 l =
  let src_present = VB.vp_member VB.SRC l0 in
  let tgt_present = VB.vp_member VB.TGT l0 in
  (not src_present || VB.vp_member VB.SRC l) &&
  (not tgt_present || VB.vp_member VB.TGT l)

let subsets l =
  List.fold_left
    (function rest ->
      (function x ->
	(List.map (function y -> x::y) rest) @ rest))
    [[]] l

let target_subsets l = check_src_tgt l (subsets l)

let glue x = function
    (xs :: xss) -> (x::xs) :: xss
  | _ -> internal_error "glue"

let rec partitions = function
    [] -> [[]]
  | [x] -> [[[x]]]
  | (x :: xs) ->
      (List.map (glue x) (partitions xs)) @
      (List.map (function y -> [x] :: y) (partitions xs))

(* only want to consider partitions in which source and target are
together, if source and target are both mentioned *)
let target_partitions l0 =
  let pts = partitions l0 in
  let src_present = VB.vp_member VB.SRC l0 in
  let tgt_present = VB.vp_member VB.TGT l0 in
  if src_present && tgt_present
  then
    List.filter
      (function part ->
	List.for_all
	  (function elem ->
	    if VB.vp_member VB.SRC elem
	    then VB.vp_member VB.TGT elem
	    else true)
	  part)
      pts
  else pts

(* input and output should mention the same processes *)
let collect_processes (input,output) =
  Aux.map_union
    (function (cls,vs,modd) -> VB.known_contents vs)
    input

let make_equal l = VB.EQUAL(List.sort compare l)

let make_description config =
  match collect_processes (config,config) with
    [] -> []
  | [x] -> [x]
  | l -> [make_equal l]

(* --------------------- Printing subsets, etc ---------------------- *)

let print_subset l =
  Printf.printf "%s\n" (Aux.set2c (List.map (function x -> VB.vp2c x) l))

let print_partition l =
  Printf.printf "%s\n"
    (Aux.set2c
       (List.map
	  (function l ->
	    (Aux.set2c (List.map (function x -> VB.vp2c x) l)))
	  l))

let entry2c (cls,vt,modd) =
  Printf.sprintf "%s %s%s" (VB.vt2c vt) (String.sub (CS.class2c cls) 0 2)
    (if modd = VB.MODIFIED then "!" else "")

let type2c (input,output) =
  Printf.sprintf "%s -> %s"
    (Aux.set2c (List.map entry2c input))
    (Aux.set2c (List.map entry2c output))

let print_config config =
  Printf.printf "%s\n" (Aux.set2c (List.map entry2c config))

(* --------------------------- Restrict_ps -------------------------- *)

let member_one p l =
  match Aux.intersect (collect_ps l) p with
    [] -> None
  | [x] -> Some x
  | l1 -> Some (make_equal l1)

let contains_src_tgt l =
  let ps = VB.collect_ps (VB.EQUAL(l)) in
  Aux.member VB.SRC ps || Aux.member VB.TGT ps

let restrict_running p = function
    (cls,VB.EMPTY,modd) -> (cls,VB.EMPTY,modd)
  | (cls,VB.MEMBER([l]),modd) as x ->
      (match member_one p l with
	None -> (cls,VB.EMPTY,modd)
      |	Some x -> (cls,VB.MEMBER([x]),modd))
  | x -> x

let restrict_not_running p isinput = function
    (cls,VB.EMPTY,modd) -> (cls,VB.EMPTY,modd)
  | (cls,VB.MEMBER(l),modd) ->
      (match Aux.option_filter (member_one p) l with
	[] ->
	  (* was (cls,VB.UNKNOWN,modd) *)
	  if isinput
	  then
	    if contains_src_tgt l
	    then (Printf.printf "dropping src/tgt\n"; (cls,VB.UNKNOWN,modd))
	    else (cls,VB.EMPTY,modd) 
	  else (cls,VB.UNKNOWN,modd)
      |	l1 -> (cls,VB.MEMBER(l1),modd))
  | x -> x

let restrict_ps (input,output) p keep_nowhere =
  let p = Aux.flat_map collect_ps p in
  let restrict_config config isinput =
    Aux.option_filter
      (function
	  (CS.RUNNING,vs,modd) as x -> Some(restrict_running p x)
	| (CS.NOWHERE,vs,modd) as x ->
	    if keep_nowhere
	    then Some(restrict_not_running p isinput x)
	    else None
	| x -> Some(restrict_not_running p isinput x))
      config
  in (restrict_config input true, restrict_config output false)

(* --------------------------- Restrict_vs -------------------------- *)

let restrict_vs (input,output) p all_etypes nm =
  let p = Aux.flat_map collect_ps p in
  let restrict_config config isinput =
    List.map
      (function
	  (CS.RUNNING,vs,modd) as x -> restrict_running p x
	| (CS.NOWHERE,vs,modd) as x -> x
	| x -> restrict_not_running p isinput x)
      config in
  let new_input = restrict_config input true in
  List.map (function output -> (new_input,output))
    (Quasi.get_outputs true new_input all_etypes nm)

(* --------------------------- concretize --------------------------- *)

let concretize_one count = function
    (VB.EMPTY,VB.EMPTY) as x -> [x]
  | (VB.EMPTY,VB.UNKNOWN) ->
      (* used to describe overall effect of bossa.schedule *)
      let n = !count in
      let s = VB.VP(VB.S(n,"concretize"),[]) in
      count := n+1;
      [(VB.EMPTY,VB.EMPTY);(VB.EMPTY,VB.MEMBER([s]))]     
  | (VB.EMPTY,VB.MEMBER(l)) as x -> [x]
	(* the following can occur, in the presence of the following
	   no longer valid type, if the other rules for process.new do not
	   mention src:
	   process.new.fork :
	   [[] = RUNNING, src in READY, tgt in NOWHERE] ->
	   {[[src,tgt] in READY],
	   [src in READY, tgt in BLOCKED]}
	   perhaps more concretization is needed *)
  | (VB.UNKNOWN,VB.EMPTY) -> internal_error("concretize_one, unknown-empty")
  | (VB.UNKNOWN,VB.UNKNOWN) ->
      let n = !count in
      let s = VB.VP(VB.S(n,"concretize"),[]) in
      count := n+1;
      [(VB.EMPTY,VB.EMPTY);(VB.MEMBER([s]),VB.MEMBER([s]))]
  | (VB.UNKNOWN,VB.MEMBER(l)) ->
      let n = !count in
      let s = VB.VP(VB.S(n,"concretize"),[]) in
      count := n+1;
      [(VB.EMPTY,VB.MEMBER(l));(VB.MEMBER([s]),VB.MEMBER(s::l))]
  | (VB.MEMBER(l),VB.EMPTY) as x -> [x]
  | (VB.MEMBER(l),VB.UNKNOWN) ->
      let n = !count in
      let s = VB.VP(VB.S(n,"concretize"),[]) in
      count := n+1;
      [(VB.MEMBER(l),VB.EMPTY);(VB.MEMBER(s::l),VB.MEMBER([s]))]
  | (VB.MEMBER(l1),VB.MEMBER(l2)) as x -> [x]
  | _ -> internal_error("concretize_one, no match")

let concretize (input,output) =
  let r_in = lookup3 CS.RUNNING input in
  let r_out = lookup3 CS.RUNNING output in
  let d_in = lookup3 CS.READY input in
  let d_out = lookup3 CS.READY output in
  let count = ref 0 in
  let crs = concretize_one count (r_in,r_out) in
  let cds = concretize_one count (d_in,d_out) in
  Aux.flat_map
    (function (r_in,r_out) ->
      List.map
	(function (d_in,d_out) ->
	  ([(CS.RUNNING,r_in,VB.MODIFIED);(CS.READY,d_in,VB.MODIFIED)],
	   [(CS.RUNNING,r_out,VB.MODIFIED);(CS.READY,d_out,VB.MODIFIED)]))
	cds)
    crs

(* ---------------------------- abstract ---------------------------- *)

(* classes is assumed to be ordered according to priority.  so we just
return the first class that has something non-empty, and return that.  if
none does, then the result is BLOCKED, because a scheduler need not even
have any processes to be blocked. *)

let abstract_config classes config =
  let rec loop = function
      [] -> internal_error "abstract_config"
    | CS.BLOCKED :: _ -> [CS.BLOCKED]
    | cls :: rest ->
	(match lookup3 cls config with
	  VB.MEMBER(l) -> [cls]
	| VB.UNKNOWN -> cls :: loop rest
	| _ -> loop rest) in
  loop classes

let abstract types =
  let classes = CS.all_classes false in
  let get_one = function
      [x] -> x
    | _ -> internal_error "abstract: only one abstraction should be possible"
  in
  Aux.make_set
    (List.map
       (function (input,output) ->
	 (get_one(abstract_config classes input),
	  get_one(abstract_config classes output)))
       types)

(* ------------------------ child concretize ------------------------ *)

let child_concretize config cls =
  let rec loop = function
      [] -> []
    | ((cls1,vt,modd) :: rest) as x ->
	if cls = cls1
	then
	  (match vt with
	    VB.EMPTY ->
	      raise(Error.Error(Printf.sprintf "unexpected empty entry: %s"
				  (CS.class2c cls)))
	  | VB.UNKNOWN -> 
	      if cls = CS.BLOCKED
	      then x (* can be blocked with everything empty *)
	      else (cls1,VB.MEMBER([VB.VP(VB.S(0,"x"),[])]),modd) :: rest
	  | _ -> x)
	else
	  (match vt with
	    VB.EMPTY -> (cls1,VB.EMPTY,modd) :: loop rest
	  | VB.UNKNOWN -> (cls1,VB.EMPTY,modd) :: loop rest
	  | _ ->
	      if cls1 = CS.NOWHERE
	      then (cls1,vt,modd) :: loop rest
	      else
		raise(Error.Error("unexpected non-empty entry: "^
				  (VB.vt2c vt)))) in
  loop config

(* ------------------------ create_template ------------------------- *)

let create_template (input,output) fwd_output =
  let ct = function
(*      (CS.NOWHERE,vt,modd) as x -> x
    |*) (cls,VB.EMPTY,modd) -> (cls,VB.EMPTY,VB.UNMODIFIED)
    | (cls,_,modd) -> (cls,VB.UNKNOWN,VB.UNMODIFIED)
  in (List.map ct input, List.map ct output, fwd_output)

(* ---------------- add forward information, if any ----------------- *)

let update_fwd_outputs fwd_outputs fwd_output =
  match (fwd_outputs,fwd_output) with
    (None,_) -> fwd_output
  | (Some x, None) -> fwd_outputs
  | (Some x, Some y) -> Some (List.sort compare (Aux.union x y))

(* ----------------------------- inject ----------------------------- *)

let inject (input, output, fwd_output) types vs_in vs_out get_fwd_outputs =
  let inject_one config cls vs =
    List.map
      (function (cls1, vs1, modd1) as x ->
	if cls = cls1
	then
	  let new_vs =
	    match vs1 with
	      VB.EMPTY -> raise(Error.Error("inject: class must be empty"))
	    | VB.MEMBER(l) ->
		(match Aux.intersect vs l with
		  [] -> VB.MEMBER(vs @ l)
		| _ -> raise(Error.Error("inject: value already in list")))
	    | _ ->
		(match vs with
		  [] -> VB.UNKNOWN
		| _ -> VB.MEMBER(vs))
	  in (cls1, new_vs, VB.UNMODIFIED)
	else x)
      config
  in
  List.map
    (function (in_state,out_state) ->
      (inject_one input in_state vs_in, inject_one output out_state vs_out,
       (update_fwd_outputs (get_fwd_outputs in_state) fwd_output)))
    types

(* ------------------- adjust the modified fields ------------------- *)

let adjust_modified (input,output,f) =
  let (i,o) =
    List.split
      (List.map2
	 (function (i_cls,i_vt,i_modd) as i ->
	   function (o_cls,o_vt,o_modd) as o ->
	     if i_vt = o_vt
	     then (i,o)
	     else
	       match (i_vt,o_vt) with
		 (VB.MEMBER(il),VB.MEMBER(ol)) ->
		   if (List.sort compare il) = (List.sort compare ol)
		   then (i,o)
		   else (i,(o_cls,o_vt,VB.MODIFIED))
	       | _ -> (i,(o_cls,o_vt,VB.MODIFIED)))
	 input output) in
  (i,o,f)

(* ----------------------- fixpoint iteration ----------------------- *)

let normalize (input,output,f) =
  let normalize_one l =
    List.map
      (function (cls,vt,modd) as x ->
	match vt with
	  VB.MEMBER(l) -> (cls,VB.MEMBER(List.sort compare l),modd)
	| _ -> x)
      l in
  (normalize_one input, normalize_one output,f)

let fix_iter nm f l =
  let process init =
    (* make_set needed because a single rule can be made in multiple ways *)
    let no_fwd =
      List.map (function (i,o,f) -> (nm,i,[o])) init in
    List.sort compare
      (Aux.make_set (List.map normalize (Aux.map_union (f no_fwd) init))) in
  let first = process l in
  [first]
(*
  let second = process first in
  if not (first = second)
  then raise (Error.Error("not at a fixed point"))
  else [first]*)

(* --------------- tgt=p0 in the input is irrelevant ---------------- *)

(* This is a problem for unblock.timer.notarget, in the case where
there is preemption.  The output type specifies two processes, both of which
are represented as P.  They become equal, and then pfreeprocs removes
everything.  It's not clear what to do about this.  Maybe the whole PS2VS
has to be rethought, with respect to the new perspective on event types ... *)

let pfreeify all_types =
  Aux.make_set
    (List.map
       (function (nm,(input,f),output) ->
	 let (in_equals,new_input) =
	   List.fold_left
	     (function (in_equals,new_input) ->
	       function (cls,vt,modd) as x ->
		 let (new_in_equals,new_vt) =
		   (match vt with
		     VB.MEMBER(l) ->
		       let (local_in_equals,new_l) =
			 (List.fold_left
			    (function (local_in_equals,new_l) ->
			      function
				  VB.EQUAL(l1) as x ->
				    let procs =
				      List.sort compare (VB.collect_ps x) in
				    let pfreeprocs =
				      List.filter
					(function
					    VB.P(_) -> false
					  | VB.S(_) -> false
					  | VB.Q(_) -> false
					  | _ -> true)
					procs in
				    let vpify x = VB.VP(x,[]) in
				    if List.length procs =
				      List.length pfreeprocs
				    then (* no change *)
				      (local_in_equals,x::new_l)
				    else (* record old and new list *)
				      let new_desc =
					(match pfreeprocs with
					  [] -> internal_error "only ps"
					| [y] -> vpify y
					| _ ->
					    VB.EQUAL(List.map vpify
						       pfreeprocs)) in
				      ((procs,new_desc)::local_in_equals,
				       new_desc::new_l)
				| x -> (local_in_equals,x::new_l))
			    ([],[]) l) in
		       (local_in_equals,VB.MEMBER(new_l))
		   | _ -> ([],vt)) in
		 (new_in_equals@in_equals,(cls,new_vt,modd)::new_input))
	     ([],[]) input in
	 let new_output =
	   List.map
	     (function (cls,vt,modd) ->
	       let new_vt =
		 match vt with
		   VB.MEMBER(l) ->
		     let new_l =
		       List.map
			 (function
			     VB.EQUAL(l1) as x ->
			       let procs =
				 List.sort compare (VB.collect_ps x) in
			       (try lookup procs in_equals
			       with Not_found -> x)
			   | x -> x)
			 l in
		     VB.MEMBER(new_l)
		 | _ -> vt in
	       (cls,new_vt,modd))
	     output in
	 (nm,(List.rev new_input,f),new_output))
       all_types)
    
(* ---------------- pre and post processing of types ---------------- *)
    
let flatten (ins, outs) =
  Aux.flat_map
    (function input ->
      List.map
	(function output -> (input,output))
	outs)
    ins
    
(* not correct, but good enough for now *)
let unflatten all_types =
  Compile_events.collect_vs_types all_types
(*  List.map (function (nm,(i,f),o) -> (nm,[[(i,f)],[o]])) all_types*)
    
(* ----------------------- The main function ------------------------ *)

type entry = Class_state.classname * Vobjects.vtype * Vobjects.modified

let ps2vs events =
  B.schedtype := B.VIRTSCHED;
  let classes = CS.all_classes false in
  unflatten
    (pfreeify
       (Aux.flat_map
	  (function (nm,types) ->
	    let flat_types = Aux.flat_map flatten types in
	    let fwd_flat_types =
	      List.map (function (i,o) -> (i,o,None)) flat_types in
	    let new_types =
	      (fix_iter nm
		 (function all_etypes ->
		   function (i,o,f) ->
		     let etype = (i,o) in
		     let processes = collect_processes etype in
		     let vs_subsets = target_subsets processes in
		     Aux.map_union
		       (function vs_processes ->
			 let restricted_types =
			   restrict_vs etype vs_processes all_etypes nm in
			 Aux.map_union
			   (function restricted_type ->
			 let template = create_template restricted_type f in
			 let ps_partitions = target_partitions vs_processes in
			 Aux.map_union
			   (function ps_partition ->
			     List.fold_left
			       (function templates ->
				 function ps_subset ->
				   (* computing the state change of the child*)
				   let (i,o) as ps_type =
				     restrict_ps restricted_type ps_subset
				       true in
				   let state_changes =
				     abstract (concretize ps_type) in
				   let cdi = make_description i in
				   let cdo = make_description o in
				   (* computing the behavior of the child *)
				   let (i,_) as ps_type =
				     restrict_ps etype ps_subset true in
				   let get_fwd_outputs ai =
				     let (ri,ro) = restricted_type in
				     let (_,global_output,_) =
				       adjust_modified(ri,ro,[]) in
				     if src_tgt_present vs_processes ps_subset
				     then
				       let child_input =
					 child_concretize i ai in
				       let child_outputs =
					 Quasi.get_outputs true child_input
					   all_etypes nm in
				       let compatible_child_outputs =
					 List.filter
					   (function output ->
					     let (_,moutput,_) =
					       (adjust_modified
						  (child_input,output,[])) in
					       List.for_all2
						 (function (c1,_,modd1) ->
						   function (c2,_,modd2) ->
						     modd1 = modd2 ||
						     modd2 = VB.MODIFIED)
						 moutput
						 global_output)
					   child_outputs in
				       if compatible_child_outputs = []
				       then
					 raise(Error.Error "no child outputs");
				       Some
					 (List.sort compare
					    (Aux.map_union
					       (abstract_config classes)
					       compatible_child_outputs))
				     else None in
				   Aux.map_union
				     (function template ->
				       let inject_res =
					 inject template state_changes
					   cdi cdo get_fwd_outputs in
				       List.map adjust_modified inject_res)
				     templates)
			       [template] ps_partition)
			   ps_partitions)
			   restricted_types)
		       vs_subsets)
		 fwd_flat_types) in
	    let o2l = function
		Some x -> x
	      |	None -> [] in
	    match new_types with
	      [x;y] ->
		(Aux.flat_map
		   (function new_type ->
		     List.map
		       (function (i,o,f) ->
			 (nm, (i, o2l f), o))
		       new_type)
		   [x])
		@
		(Aux.flat_map
		   (function new_type ->
		     Aux.option_filter
		       (function (i,o,f) ->
			 let Ast.EVENT_NAME(id,b,a) = nm in
			 Some (Ast.EVENT_NAME(id@[B.mkId "second"],b,a),
			       (i, o2l f), o))
		       new_type)
		   [y])
	    | _ ->
		Aux.flat_map
		  (function new_type ->
		    List.map
		      (function (i,o,f) ->
			(nm, (i, o2l f), o))
		      new_type)
		  new_types)
	  events))
