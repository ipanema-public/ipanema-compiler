module B = Objects
module VB = Vobjects
module CS = Class_state

type 'a fa_state = EDGE of int * 'a * int | FINAL of int
type entry = CS.classname * VB.vtype * VB.modified

(* -------------------- Miscellaneous operations -------------------- *)

let error str = raise (Error.Error str)

(* -------------------- Convert the NFA to a DFA -------------------- *)

let rec update_edge_targets edge_targets nm tgt =
  match edge_targets with
    [] -> [(nm,[tgt])]
  | ((name,l) as x)::rest ->
      if nm = name
      then (name,Aux.union [tgt] l)::rest
      else x :: (update_edge_targets rest nm tgt)

let collect_all_edges n edges =
  List.filter (function (EDGE(src,_,tgt)) -> src = n | _ -> false) edges

let rec nfa_to_dfa_loop edges all_states final all_final next_index = function
    [] -> ([],all_final)
  | ((cur,index)::rest) ->
      let edge_targets =
	List.fold_left
	  (function edge_targets ->
	    function n ->
	      let n_edges = collect_all_edges n edges in
	      List.fold_left
		(function edge_targets ->
		  function
		      (EDGE(src,nm,tgt)) ->
			update_edge_targets edge_targets nm tgt
		    | _ -> edge_targets)
		edge_targets n_edges)
	  [] cur in
      let edge_targets =
	List.map (function (nm,l) -> (nm,List.sort compare l))
	  edge_targets in
      let (new_edges,new_states,all_states,all_final,next_index) =
	List.fold_left
	  (function (new_edges,new_states,all_states,all_final,next_index) ->
	    function (nm,tgts) ->
	      try
		let dest = List.assoc tgts all_states in
		(Aux.union [EDGE(index,nm,dest)] new_edges,
		 new_states,all_states,all_final,next_index)
	      with Not_found ->
		(Aux.union [EDGE(index,nm,next_index)] new_edges,
		 (tgts,next_index)::new_states,
		 (tgts,next_index)::all_states,
		 (if (List.exists (function n -> Aux.member n final) tgts)
		 then next_index::all_final
		 else all_final),
		 next_index+1))
	  ([],[],all_states,all_final,next_index) edge_targets in
      let (newer_edges,all_final) =
	nfa_to_dfa_loop edges all_states final all_final next_index
	  (new_states @ rest) in
      (new_edges @ newer_edges, all_final)

let rec nfa_to_dfa edges start_states =
  let (real_edges,final_states) =
    List.partition
      (function EDGE(_,_,_) -> true | FINAL(_) -> false)
      edges in
  let final = List.map (function (FINAL(n)) -> n | _ -> -1) final_states in
  let all_final =
    match Aux.intersect start_states final with [] -> [] | _ -> [0] in
  nfa_to_dfa_loop real_edges [(start_states,0)] final all_final
    1 [(start_states,0)]
