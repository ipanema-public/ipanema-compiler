module B = Objects
module VB = Vobjects
module CS = Class_state
open Format

(* -------------------- Miscellaneous operations -------------------- *)

let fix_underlines s =
  let count = ref 0 in
  String.iter (function c -> if c = '_' then count := !count + 1) s;
  let res = String.create ((String.length s) + (!count)) in
  count := 0;
  String.iter
    (function c ->
      if c = '_' then (String.set res (!count) '\\'; count := !count + 1);
      String.set res (!count) c;
      count := !count + 1)
    s;
  res

let drop_underlines s =
  let count = ref 0 in
  String.iter (function c -> if c = '_' then count := !count + 1) s;
  let res = String.create ((String.length s) - (!count)) in
  count := 0;
  String.iter
    (function c ->
      if not(c = '_') then (String.set res (!count) c; count := !count + 1))
    s;
  res

let rec print_between between_fn elem_fn = function
    [] -> ()
  | [x] -> elem_fn x
  | x :: xs -> (elem_fn x; between_fn(); print_between between_fn elem_fn xs)

let rec print_between2 between_fn1 between_fn2 elem_fn = function
    [] -> ()
  | [x] -> elem_fn x
  | [x;y] -> elem_fn x; between_fn1(); elem_fn y
  | x :: y :: zs -> (elem_fn x; between_fn1(); elem_fn y; between_fn2();
		     print_between2 between_fn1 between_fn2 elem_fn zs)

let pp_id id = print_string(Printf.sprintf "%s" (B.id2c(id)))

let pp_id_no_underlines id =
  print_string(Printf.sprintf "%s" (drop_underlines(B.id2c(id))))

let pp_comma _ = (print_string ","; print_space())
let pp_comma_nl _ = (print_string ",\\\\"; print_space())

let pp_semic _ = (print_string ";"; print_cut())

let nl_ifnonempty = function
    x::xs -> force_newline()
  | _ -> ()

let pp_list f l =
  print_string "["; open_box 0; print_between pp_semic f l; close_box();
  print_string "]"

let print_nl_string str = print_string str; force_newline()

type entry = CS.classname * VB.vtype * VB.modified
type type_rule = B.isquasi * entry list list * entry list list
type vs_type_rule =
    B.isquasi * (entry list * CS.classname list) list * entry list list

let internal_error str = raise (Error.Error(str^": internal error"))

(* ------------------------- Nice newlines -------------------------- *)

let rec print_between_ar n between_fn elem_fn l last =
  if (List.length l > n)
  then
    (print_string "\\begin{array}[t]{@{}l}"; force_newline();
     let rec loop = function
	 (_,[]) -> ()
       | (0,l) -> print_string "\\\\"; loop (n,l)
       | (m,[x]) -> elem_fn x
       | (m,x :: xs) -> elem_fn x; between_fn(); loop (m-1,xs) in
     loop (n,l);
     print_string last;
     print_string "\\end{array}")
  else (print_between between_fn elem_fn l; print_string last)

(* -------------------------- Event types --------------------------- *)

let class2c cls =
  match CS.class2c(cls) with
    "RUNNING" -> "R"
  | "READY" -> "D"
  | "BLOCKED" -> "B"
  | "TERMINATED" -> "T"
  | "NOWHERE" -> "N"
  | _ -> raise (Error.Error("pp_class: internal error"))

let pp_class cls = print_string (class2c cls)

let print_subscript n =
  print_string "{"; print_int n; print_string "}"

let pp_vprocname = function
    VB.SRC -> print_string "{\\mtt{e.source}}"
  | VB.TGT -> print_string "{\\mtt{e.target}}"
  | VB.P(n) -> print_string "p_"; print_subscript n;
  | VB.Q(n) -> print_string "q_"; print_subscript n;
  | VB.S(n,str) ->
      print_string (fix_underlines str); print_string "_"; print_subscript n
  | VB.ARG(n) -> print_string "{\\mbox{\\it{arg}}}_"; print_subscript n
  | VB.UNKNOWNPROC -> print_string "{\\msf{x}}"
  | VB.BOTPROC -> internal_error "pp_vprocname"

let rec pp_vp = function
    VB.VP(vp,[]) -> pp_vprocname vp
  | VB.EQUAL(vps) ->
      print_between (function x -> print_string " = ") pp_vp vps
  | _ -> internal_error "pp_vp"

let pp_mod = function
    VB.MODIFIED -> print_string " !"
  | _ -> ()

let rec pp_val = function
    (cls,VB.UNKNOWN,VB.UNMODIFIED) -> ()
      (*print_string "? ="; print_space(); pp_class cls; pp_mod m;*)
  | (cls,VB.UNKNOWN,m) ->
      pp_class cls; pp_mod m
  | (cls,VB.EMPTY,m) ->
      print_string "[\\,] ="; print_space(); pp_class cls; pp_mod m
  | (cls,VB.MEMBER([p]),m) ->
      pp_vp p; print_space(); print_string "\\in";
      print_space(); pp_class cls; pp_mod m
  | (cls,VB.MEMBER(l),m) ->
      print_string "\\{";
      print_between pp_comma pp_vp l;
      print_string "\\}"; print_space(); print_string "\\subseteq";
      print_space(); pp_class cls; pp_mod m
  | (cls,VB.BOT,m) -> internal_error "pp_val"

let pp_config x =
  print_string "\\langle"; print_space();
  print_between pp_comma pp_val
  (List.filter
    (function
	(cls,VB.UNKNOWN,VB.UNMODIFIED) -> (*true*)false
      | _ -> true)
    x);
  print_string "\\rangle"

let pp_nl_comma _ =  pp_comma(); print_string "\\\\"; force_newline()

let pp_entry pp_config = function
    [entry] -> pp_config entry
  | [x;y] -> print_string "\\{"; pp_config x; pp_comma(); pp_config y;
      print_string "\\}"
  | l ->
      print_string "\\{\\begin{array}[t]{@{}l@{}}";
      print_between2 pp_comma pp_nl_comma pp_config l;
      print_string "\\}\\end{array}"

let pp_type(q,input,output) =
  let arrow =
    match q with
      B.QUASI -> "\\stackrel{q}{\\rightarrow}"
    | B.NOT_QUASI -> "\\rightarrow" in
  if List.length input = 1 && List.length output = 1
  then (pp_entry pp_config input;
	print_string arrow;
	pp_entry pp_config output)
  else (pp_entry pp_config input;
	print_string ("\\\\"^arrow);
	pp_entry pp_config output)

let pp_iconfig (input,f) =
  pp_config input;
  print_string "^";
  print_string (Aux.set2c(List.map class2c f))

let pp_vs_type(q,input,output) =
  Objects.schedtype := Objects.VIRTSCHED;
  let arrow =
    match q with
      B.QUASI -> "\\stackrel{q}{\\rightarrow}"
    | B.NOT_QUASI -> "\\rightarrow" in
  if List.length input = 1 && List.length output = 1
  then (pp_entry pp_iconfig input; print_string arrow;
	pp_entry pp_config output)
  else (pp_entry pp_iconfig input; print_string ("\\\\"^arrow);
	pp_entry pp_config output);
  Objects.schedtype := Objects.PROCSCHED

let pp_events events pp_type =
  List.iter
    (function (Ast.EVENT_NAME(nmlst,star,_) as nm,types) ->
      print_string "\\item {\\tt ";
      print_string(fix_underlines(Ast.event_name2c nm));
      print_nl_string "}:";
      (*print_string "\\";
      List.iter pp_id_no_underlines nmlst;
      if star then print_string "star"; force_newline();*)
      print_string "\\[\\{";
      let types = List.sort compare types in
      print_between_ar 1 pp_comma_nl pp_type types "\\}";
      print_nl_string "\\]")
    events

let pp_interfaces interfaces =
  List.iter
    (function (nm,types) ->
      print_string "\\item {\\tt ";
      print_string(fix_underlines(B.id2c nm));
      print_string "}:";
      force_newline();
      (*print_string "\\"; pp_id_no_underlines nm;*)
      print_string "\\[\\{";
      print_between_ar 1 pp_comma pp_type types "\\}";
      print_string "\\]"; force_newline())
    interfaces

(* --------- automaton of application event quasi-sequences --------- *)

let pp_automaton automaton =
  print_nl_string "\\[\\begin{array}{lll}";
  let aut =
    Aux.map_union
      (function
	  Nfa2dfa.EDGE(src,(nm,_,Quasi.REQUIRED),tgt) ->
	    [(src,fix_underlines (Ast.event_name2c nm),tgt)]
	| Nfa2dfa.EDGE(src,(nm,_,Quasi.OPTIONAL),tgt) ->
	    if src = tgt then [] else [(src,"interrupts",tgt)]
	| _ -> [])
      automaton in
  print_between2
    (function _ -> print_nl_string " &\\phantom{xxx}& ")
    (function _ -> print_nl_string " \\\\ ")
    (function (src,nm,tgt) ->
      print_int src;
      print_string
	(Printf.sprintf
	   "\\stackrel{\\mbox{\\rm{\\scriptsize{%s}}}}{\\rightarrow}"
	   nm);
      print_int tgt)
    aut;
  print_nl_string "\\end{array}\\]";
  force_newline()

(* ------------------- quasi-sequence transitions ------------------- *)

let pp_arrow nm =
  print_string
    (List.fold_left
       (function arrow ->
	 function nm ->
	   Printf.sprintf
	     "\\stackrel{\\mbox{\\rm{\\scriptsize{%s}}}}{%s}"
	     (fix_underlines (Ast.event_name2c nm)) arrow)
       "\\rightarrow" [nm])

let rec pp_prevs n = function
    Model.START(config) -> pp_config config; 1
  | Model.TRANSITION(prev,nm,config) ->
      let ct = pp_prevs n prev in
      let ct =
	if ct = n
	then
	  (print_nl_string "\\\\"; 1)
	else ct+1 in
      pp_arrow nm;
      pp_config config;
      ct

let pp_transition n prev nm =
  let _ = pp_prevs n prev in pp_arrow nm; print_nl_string "\\ldots"

let rec last_config = function
    Model.START(config) -> config
  | Model.TRANSITION(prev,nms,config) -> config

let pp_transitions quasi_transitions =
  List.iter
    (function (Ast.EVENT_NAME(nmlst,star,_) as nm,transition) ->
      print_string "\\item {\\tt ";
      print_string(fix_underlines(Ast.event_name2c nm));
      print_nl_string "}:";
      print_string "$";
      pp_config (last_config transition);
      print_nl_string "$";
      print_nl_string "\\[\\begin{array}{l}";
      pp_transition 2 transition nm;
      print_nl_string "\\end{array}\\]")
    quasi_transitions

(* -------------------------- entry point --------------------------- *)

let pretty_print out events interfaces vsinterfaces
    unmatched_transitions init_automaton automaton sched_state_effects =
  (set_formatter_out_channel (open_out out);
   set_margin 80;
   open_box 0;
   print_nl_string
     "\\section{Automaton of application events from initial state}";
   pp_automaton init_automaton;
   print_nl_string "\\section{Automaton of application events}";
   pp_automaton automaton;
   (match unmatched_transitions with
     (x::xs) ->
       print_nl_string "\\section{Unmatched inputs}";
       print_nl_string "\\begin{itemize}";
       pp_transitions unmatched_transitions;
       print_nl_string "\\end{itemize}";
   | _ -> ());
   print_nl_string "\\section{Event types}";
   print_nl_string "\\begin{itemize}";
   pp_events events pp_type;
   force_newline();
   pp_interfaces interfaces;
   print_nl_string "\\end{itemize}";
   print_nl_string "\\section{Scheduler state effects}";
   print_nl_string "\\begin{itemize}";
   pp_events sched_state_effects pp_vs_type;
   force_newline();
   Objects.schedtype := Objects.VIRTSCHED;
   pp_interfaces vsinterfaces;
   Objects.schedtype := Objects.PROCSCHED;
   print_string "\\end{itemize}";
   close_box();
   print_newline())
