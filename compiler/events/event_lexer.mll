{
                                                                      
(* $Id$ *)
open Event_parser
open Format
open Lexing

type error =
  | Illegal_character of char
  | Unterminated_comment

(* Error defines error types and positions*)
exception Error of error * int * int

(* To store the position of the beginning of a comment and bitpat *)
let comment_start_pos = ref 0

(* Report Error  *)

let report_error fmt = function
  | Illegal_character c  -> fprintf fmt "Illegal character (%s)" (Char.escaped c)
  | Unterminated_comment -> fprintf fmt "Comment not terminated"


}

(* define rules *)

let id = ['a'-'z' 'A'-'Z'] ['a'-'z' 'A'-'Z' '0'-'9' '_']*
let decimal_literal = ['0'-'9']+

rule token = parse
    	|   	"["             { LBRACK        }
    	|   	"]"             { RBRACK        }
    	|   	"("             { LPAREN        }
    	|   	")"             { RPAREN        }
    	|   	"{"             { LBRACE        }
    	|   	"}"             { RBRACE        }
	|	","		{ COMMA		}
	|	"::"		{ COLONCOLON	}
	|	"="		{ EQUAL		}
	|	"in"		{ IN      	}
	|	"src"		{ SRC  		}
	|	"tgt"		{ TGT  		}
	|	"p"		{ P    		}
	|	"q"		{ Q    		}
	|	"x"		{ X    		}
	|	"arg"		{ ARG  		}
	|	"-"     	{ HYPHEN	}
	|	"!"     	{ BANG		}
	|	"application_events"  { APPEV	}
	|	"initial_events"      { INITEV	}
	|	"system_events"       { SYSEV	}
	|	"top_level_events"    { TOPLEV	}
	|	"scheduler_events"    { SCHEDLEV}
	|	"event_types"         { TYPES 	}
	|	"quasi_event_types"   { QUASITYPES }
	|	"vs_interface"        { VSINTERFACE }
	|	"automaton"           { AUTOMATON  }
	|	"init_automaton"      { IAUTOMATON }
	|	"final"               { FINAL      }
	|	"interface"     { INTERFACE	}
	|	":"		{ COLON		}
	|	";"		{ SEMIC		}
	|	"->"		{ RIGHTARROW	}
	|	"-u>"		{ UNINTARROW	}
	|	"."		{ DOT       	}
	|	"*"		{ TIMES     	}
    	|   eof                 { EOF           }
    	|   [' ' '\t' ]
        	{ token lexbuf }    (* skip whitespace      *)
    	|   '\n' | '\r' | "\r\n"
		{ 
	  	Misc.lineno := !Misc.lineno+1;
          	Misc.cr_index := (lexeme_start lexbuf) + 1;
	  	Misc.incr lexbuf;
          	token lexbuf  (* skip whitespace *)
		}    
    	|   "//" [^'\n' '\r']*       
		{ token lexbuf }    (* skip // comment      *)
    	|   "/*"
        	{ 
          	comment_start_pos := Lexing.lexeme_start lexbuf;
          	comment lexbuf; 
	  	token lexbuf 
		}
    	| decimal_literal
		{ INTEGER(int_of_string(lexeme lexbuf)) }
    	| id                 
		{ IDENT (lexeme lexbuf) }
    	|   _
        { 
	  raise (Error(Illegal_character ((Lexing.lexeme lexbuf).[0]),
		       Lexing.lexeme_start lexbuf,
		       Lexing.lexeme_end lexbuf))
	}

and comment = parse
    | "*/"                  
	{ () } 
    | '\n' | '\r' | "\r\n"
        { 
	  Misc.lineno := !Misc.lineno+1; 
          Misc.cr_index := (lexeme_start lexbuf) + 1;
	  Misc.incr lexbuf;
	  comment lexbuf 
	}
    | eof
        { 
          raise (Error (Unterminated_comment, 
			!comment_start_pos,
			!comment_start_pos + 2))
        }
    | _  
        { comment lexbuf }











