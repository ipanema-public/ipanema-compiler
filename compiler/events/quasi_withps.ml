module B = Objects
module VB = Vobjects
module CS = Class_state

(* -------------------- Miscellaneous operations -------------------- *)

exception LookupErrException of string

let rec lookup x = function
    [] -> raise (LookupErrException (VB.vp2c x))
  | ((y,v)::r) -> if x = y then v else lookup x r

let error str = raise (Error.Error str)

let flat_map f l = List.flatten (List.map f l)

let print_config config =
  List.iter
    (function (cls,vt,modif) ->
      Printf.printf "%s%s:%s "
	(CS.class2c cls)
	(if modif = VB.MODIFIED then "!" else "")
	(VB.vt2c vt))
    config;
  Printf.printf "\n"

(* ------------------- Rename the input and output ------------------ *)

let ctr = ref 0

(* verification already ensures that the variables referred to by the
input type are all distinct *)
let rec mk_input_env input str =
  let (entries,env) =
    List.fold_left
      (function (entries,env) ->
	function (cls,vt,modif) as x ->
	  match vt with
	    VB.UNKNOWN -> (x::entries,env)
	  | VB.EMPTY -> (x::entries,env)
	  | VB.MEMBER(l) ->
	      let (m_entries,m_env) =
		List.fold_left
		  (function (m_entries,m_env) ->
		    function (VB.VP(vp,l) as x) ->
		      match vp with
			VB.UNKNOWNPROC -> (x::m_entries,m_env)
		      | VB.BOTPROC -> error "unexpected bottom value"
		      | _ ->
			  new_name x m_entries
			    (Printf.sprintf "%s_%s" str (CS.class2c cls))
			    m_env)
		  ([],env) l
	      in ((cls,VB.MEMBER(List.rev m_entries),modif)::entries,m_env)
	  | VB.BOT -> error "unexpected bottom value")
      ([],[]) input in
  (List.rev entries,env)

and new_name (VB.VP(vp,l) as x) entries str env =
  let n = !ctr in
  ctr := !ctr + 1;
  (VB.VP(VB.S(n,"new"),l)::entries,(VB.VP(vp,l),VB.VP(VB.S(n,"new"),l))::env)

let rename_output env id_on_unbound entries =
  List.map
    (function (cls,vt,modif) ->
      (cls,
       (match vt with
	 VB.UNKNOWN -> vt
       | VB.EMPTY -> vt
       | VB.MEMBER(l) ->
	   VB.MEMBER(List.map
		       (function (VB.VP(vp,l) as x) ->
			 match vp with
			   VB.UNKNOWNPROC -> x
			 | VB.BOTPROC -> error "unexpected bottom value"
			 | _ ->
			     (try lookup x env
			     with LookupErrException str ->
			       if id_on_unbound
			       then x
			       else raise (LookupErrException str)))
		       l)
       | VB.BOT -> error "unexpected bottom value"),
       modif))
    entries
	
let rename_type nm input outputs =
  let (new_input,env) = mk_input_env input (Ast.event_name2c nm)
  in (nm, new_input, List.map (rename_output env false) outputs)

(* --------- Collect types compatible with the configuration -------- *)

let check_compatible cur input =
  List.for_all2
    (function (cur_cls,cur_vt,cur_mod) ->
      function (input_cls,input_vt,input_mod) ->
	match cur_vt with
	  VB.UNKNOWN -> true
	| VB.EMPTY ->
	    (match input_vt with
	      VB.UNKNOWN -> true
	    | VB.EMPTY -> true
	    | VB.MEMBER(_) -> false
	    | VB.BOT -> error "unexpected bottom value")
	| VB.MEMBER(_) ->
	    (match input_vt with
	      VB.UNKNOWN -> true
	    | VB.EMPTY -> false
	    | VB.MEMBER(_) -> true
	    | VB.BOT -> error "unexpected bottom value")
	| VB.BOT -> error "unexpected bottom value")
    cur input

let possible_types cur types =
  List.filter
    (function (nm,input,_) ->
      check_compatible cur input)
    types

(* --- Build environments from a configuration and a single input --- *)

(* make a list of all possible environments *)
(* We know that RUNNING is a process variable.  We have to assume that
the other classes are queues.  For one input, we get a set of
environments, representing all of the possible matches.  The classes
are assumed to occur in the same order in both configurations. *)

let vplist2c l =
  Aux.set2c (List.map VB.vp2c l)

let rec envs_for_compatible_input cur input =
  let envs =
  List.fold_left2
    (function envs ->
      function (cur_cls,cur_vt,cur_mod) ->
	function (input_cls,input_vt,input_mod) ->
	  if not (cur_cls = input_cls) then error "non-matching classes";
	  match cur_vt with
	    VB.UNKNOWN -> envs
	  | VB.EMPTY -> envs
	  | VB.MEMBER(cur_l) as x ->
	      (match input_vt with
		VB.UNKNOWN ->
		  List.map
		    (function env ->
		      (List.map (function cur -> (cur,cur)) cur_l) @ env)
		    envs
	      | VB.EMPTY -> error "incompatible types"
	      | VB.MEMBER(input_l) ->
		  (match cur_cls with
		    CS.RUNNING ->
		      (match (cur_l,input_l) with
			([cur_vp],[input_vp]) ->
			  List.map
			    (function x -> [(cur_vp,input_vp)] @ x)
			    envs
		      |	_ ->
			  error
			    (Printf.sprintf "only one process expected %d %d"
			       (List.length cur_l) (List.length input_l)))
		  | _ ->
		      let new_envs = perm cur_l input_l in
		      flat_map
			(function new_env ->
			  List.map
			    (function old_env -> new_env @ old_env)
			    envs)
			new_envs)
	      | VB.BOT -> error "unexpected bottom value")
	  | VB.BOT -> error "unexpected bottom value")
    [[]] cur input in
  let drop_id =
    List.filter
      (function env -> List.exists (function (x,y) -> not (x = y)) env)
      envs in
  if (List.length drop_id) < ((List.length envs) - 1)
  then error (Printf.sprintf "too many identical environments %d %d"
		(List.length drop_id) (List.length envs))
  else drop_id

(* env represented as a pair of a list of names from lst and a list of
names from replst or lst *)

and perm lst replst =
 List.map (function (a,b) -> List.combine a b)
    (List.fold_left
       (function envs ->
	 function lst_elem ->
	   flat_map
	     (function (lst_env,rep_env) ->
	       List.fold_left
		 (function acc ->
		   function rep_elem ->
		     if (Aux.member rep_elem rep_env)
		     then acc
		     else (lst_elem::lst_env,rep_elem::rep_env)::acc)
		 [(lst_elem::lst_env,lst_elem::rep_env)] replst)
	     envs)
       [([],[])] lst)

(* --- Build outputs from a configuration and a single environment -- *)

(* compute the effect of a type on the current configuration based on
a given environment *)

let rec compute_output cur output env =
  let res =
  List.fold_left
    (function res ->
      function (cur_cls,cur_vt,cur_mod) ->
	match cur_vt with
	  VB.UNKNOWN -> res
	| VB.EMPTY -> res
	| VB.MEMBER(cur_l) ->
	    List.fold_left
	      (function res ->
		function cur ->
		  let type_name = lookup cur env in
		  match hunt type_name output with
		    Some dest ->
		      add cur dest (del cur res false) false
		  | None -> res)
	      res cur_l
	| VB.BOT -> error "unexpected bottom value")
    cur cur in
  res

and hunt name output =
  let destinations =
    List.fold_left
      (function destinations ->
	function (cls,vt,modif) ->
	  match vt with
	    VB.UNKNOWN -> destinations
	  | VB.EMPTY -> destinations
	  | VB.MEMBER(l) ->
	      if (Aux.member name l)
	      then cls::destinations
	      else destinations
	  | VB.BOT -> error "unexpected bottom value")
      [] output in
  match destinations with
    [] -> None
  | [destination] -> Some destination
  | _ -> error "duplicated variables not supported"

(* if we delete all of the information about an output state, the
modification becomes unmodified *)
and del name res is_output =
  List.map
    (function (cls,vt,modif) as x ->
       (match vt with
	 VB.UNKNOWN -> x
       | VB.EMPTY -> x
       | VB.MEMBER(l) ->
	   (match Aux.remove name l with
	     [] -> 
	       (match cls with
		 CS.RUNNING -> (cls, VB.EMPTY, modif)
	       | _ -> (cls, VB.UNKNOWN,
		       if is_output then VB.UNMODIFIED else modif))
	   | new_l -> (cls, VB.MEMBER new_l, modif))
       | VB.BOT -> error "unexpected bottom value"))
    res

(* when we add information to an output state, the modification becomes
modified *)
and add name dest res is_output =
  List.map
    (function (cls,vt,modif) as x ->
      if cls = dest
      then
	(cls,
	 (match vt with
	   VB.UNKNOWN -> VB.MEMBER([name])
	 | VB.EMPTY -> VB.MEMBER([name])
	 | VB.MEMBER(l) -> VB.MEMBER(name :: l)
	 | VB.BOT -> error "unexpected bottom value"),
	 if is_output then VB.MODIFIED else modif)
      else x)
    res

let output_for_all_envs cur output envs =
  List.map (function env -> compute_output cur output env) envs

(* -------------------- History of quasisequences ------------------- *)

type entry = Class_state.classname * Vobjects.vtype * Vobjects.modified

type sequence = START of entry list
  | TRANSITION of sequence * Ast.event_name list * entry list
  | ENDED of sequence

let update_transition config prev cur nm new_inputs res =
  if (config = cur)
  then
    let try_add =
      Aux.option_filter
	(function dst ->
	  if dst = config then None else Some(TRANSITION(prev,[nm],dst)))
	new_inputs in
    (match try_add with
      [] -> res
    | _ -> try_add @ res)
  else res

let add_nm prev1 config1 nms config2 prev cur nm new_inputs transitions =
  let res =
    List.fold_left
      (function (transitions,new_inputs) ->
	function new_input ->
	  if cur = config1 && config2 = new_input
	  then (TRANSITION(prev1,nm::nms,config2)::transitions,new_inputs)
	  else (transitions,new_input::new_inputs))
      ([],[]) new_inputs in
  (match res with
    ([],[]) -> (prev::transitions,[])
  | ([],res_new_inputs) -> (prev::transitions,res_new_inputs)
  | (res_transitions,[]) -> (res_transitions@transitions,[])
  | (res_transitions,res_new_inputs) ->
      (res_transitions@transitions,res_new_inputs))

let update_transitions transitions cur nm new_inputs =
  let (transitions,new_inputs) =
    List.fold_left
      (function (transitions,new_inputs) ->
	function prev ->
	  match prev with
	    TRANSITION(TRANSITION(_,_,config1) as prev1,nms,config2) ->
	      add_nm prev1 config1 nms config2 prev cur nm new_inputs
		transitions
	  | TRANSITION(START(config1) as prev1,nms,config2) ->
	      add_nm prev1 config1 nms config2 prev cur nm new_inputs
		transitions
	  | _ -> (prev::transitions,new_inputs))
      ([],new_inputs) transitions in
  Aux.union transitions
  (List.fold_left
    (function res ->
      function prev ->
	match prev with
	  START(config) ->
	    update_transition config prev cur nm new_inputs res
	| TRANSITION(_,_,config) ->
	    update_transition config prev cur nm new_inputs res
	| ENDED(_) -> res)
    [] transitions)

(* a transition that is a prefix of another comes from the threading of all
of the transitions through the computations in iterate... *)
let rec sequence_prefix elem = function
    START(config) as x -> elem = x
  | (TRANSITION(prev,nm,config)) as x -> elem = x || sequence_prefix elem prev
  | ENDED(prev) -> sequence_prefix elem prev

let drop_redundant_transitions transitions =
  let transitions = List.rev(List.sort compare transitions) in
  List.fold_left
    (function res ->
      function elem ->
	match res with
	  [] -> [elem]
	| (h::_) -> if sequence_prefix elem h then res else elem::res)
    [] transitions

(* ----------------------- Put it all together ---------------------- *)

(* Because transitions is threaded through everything done one each
iteration, we can end up adding something to a transition created on the
current iteration.  The result is more verbose, but not incorrect, so it
doesn't seem to matter.  The alternative would be to extend only the
transitions of the previous iterations, and then combine them all, but that
would be inconvenient for combining multiple transitions that lead to the
same state. *)
let rec iterate transitions curs types all =
  let (transitions,new_configs) =
    List.fold_left
      (function (transitions,new_configs) ->
	function cur ->
	  let pt = possible_types cur types in
	  List.fold_left
	    (function (transitions,new_configs) ->
	      function (nm,input,outputs) ->
		let envs = envs_for_compatible_input cur input in
		let new_inputs =
		  List.fold_left
		    (function new_inputs ->
		      function output ->
			Aux.union 
			  (normalize(output_for_all_envs cur output envs))
			  new_inputs)
		    [] outputs in
		(update_transitions transitions cur nm new_inputs,
		 Aux.union new_inputs new_configs))
	    (transitions,new_configs) pt)
      (transitions,[]) curs in
  let new_configs =
    List.filter (function config -> not (Aux.member config all)) new_configs in
  let transitions = drop_redundant_transitions transitions in
  match new_configs with
    [] -> (transitions, all) (* nothing new, so done *)
  | _ -> iterate transitions new_configs types (new_configs @ all)

and inner_normalize t =
  List.map
    (function (cls,vt,modif) ->
      (cls,
       (match vt with
	 VB.UNKNOWN -> vt
       | VB.EMPTY -> vt
       | VB.MEMBER(l) -> VB.MEMBER(List.sort compare l)
       | VB.BOT -> vt),
       modif))
    t

and normalize lst = List.map inner_normalize lst

let compute_outputs cur types = iterate [START(cur)] [cur] types [cur]

(* ------------- Try to generate a reasonable output type ----------- *)

let rec mk_monotone_output new_input old_outputs =
  List.map
    (function old_output ->
      List.fold_left2
	(function output ->
	  function (input_cls,input_vt,input_modif) ->
	    function (output_cls,output_vt,output_modif) ->
	      match input_vt with
		VB.UNKNOWN -> output
	      | VB.EMPTY ->
		  if output_vt = VB.UNKNOWN && output_modif = VB.UNMODIFIED
		  then mk_empty input_cls output
		  else output
	      | VB.MEMBER(l) ->
		  List.fold_left
		    (function output ->
		      function (VB.VP(vp,l) as x) ->
			match hunt x output with
			  Some dest ->
			    if CS.less_than(dest,input_cls)
			    then
                              (* leave output where it is, but update modif
				 not the most efficient way to do this...*)
			      add x dest (del x output true) true
			    else
			      (* move output to where it is in the input *)
			      add x input_cls (del x output true)
				(not (input_cls = CS.RUNNING))
			| None -> error "mk_monotone_output: dropped process")
		    output l
	      | VB.BOT -> output)
	old_output new_input old_output)
    old_outputs

and mk_empty dest output =
  List.map
    (function (cls,vt,modif) as x ->
      if cls = dest then (cls,VB.EMPTY,modif) else x)
    output

(* try to show that new_input implies input, in which case new_input is
redundant *)
let find_redundant name new_input (nm,input,outputs) =
  if name = nm
  then
    let res =
      List.fold_left2
	(function res ->
	  function (input_cls,input_vt,input_modif) ->
	    function (new_input_cls,new_input_vt,new_input_modif) ->
	      if input_vt = new_input_vt
	      then res
	      else
		match (input_vt,new_input_vt) with
		  (VB.UNKNOWN,VB.MEMBER(l2)) ->
		    (match res with
		      Some(l3) ->
			Some((List.map (function x -> (x,input_cls)) l2) @ l3)
		    | None -> None)
		| (VB.UNKNOWN,_) -> res
		| (VB.MEMBER(l1),VB.MEMBER(l2)) ->
		    if Aux.subset l1 l2
		    then
		      (match res with
			Some(l3) ->
			  Some((List.map (function x -> (x,input_cls))
				  (Aux.subtract l2 l1))
			       @ l3)
		      | _ -> None)
		    else None
		| _ -> None)
	(Some([])) input new_input in
    match res with
      Some(l) ->
	Some(nm,new_input,
	     List.map
	       (function output ->
		 List.fold_left
		   (function output ->
		     function (vp,cls) ->
		       add vp cls output false)
		   output l)
	       outputs)
    | None -> None
  else None

let check_monotone_redundant new_input old_outputs name types =
  let same_name_and_inputs =
    Aux.option_filter (find_redundant name new_input) types in
  match same_name_and_inputs with
    [] -> 
      List.fold_left
	(function res ->
	  function output ->
	    let tryadd = (name,new_input,output) in
	    if Aux.member tryadd res then res else tryadd::res)
	[] (mk_monotone_output new_input old_outputs)
  | l ->
      flat_map
	(function (nm,input,outputs) ->
	  (List.map
	     (function output -> (nm,input,output))
	     outputs))
	l

(* --------- Collect types compatible with the configuration -------- *)

let process inits traversed =
  let collect_transitions = ref ([] : (Ast.event_name * sequence) list) in
  let collect_rules =
    List.fold_left
      (function res ->
	function (name,input,outputs) ->
	  let (transitions, new_input) = compute_outputs input traversed in
	  collect_transitions :=
	    (List.map (function x -> (name,x)) transitions) @
	    (!collect_transitions);
	  Aux.union
	    (flat_map
	       (function new_input ->
		 check_monotone_redundant new_input outputs name inits)
	       new_input)
	    res)
      [] inits in
  (!collect_transitions,collect_rules)

(* ------------------- Management of p/q variables ------------------ *)

(* Initially change all p's in RUNNING to q's.  Change RUNNING back to p at
the end.  We assume that there are no q's in the original type... *)

let qify_config input recognize modify =
  let (env,new_input) =
    List.fold_left
      (function (env,new_input) ->
	function
	    (CS.RUNNING,VB.MEMBER(l),modif) ->
	      let (env,lres) =
		List.fold_left
		  (function (env,lres) ->
		    function (VB.VP(vp,l)) ->
		      match recognize vp with
			Some(n) ->
			  let newvp = modify n in
			  (((VB.VP(vp,l)),(VB.VP(newvp,l)))::env,
			   (VB.VP(newvp,l))::lres)
		      | None -> (env,(VB.VP(vp,l))::lres))
		  (env,[]) l in
	      (env,(CS.RUNNING,VB.MEMBER(lres),modif)::new_input)
	  | entry -> (env,entry::new_input))
      ([],[]) input in
  (env, List.rev new_input)
    
let qify (nm,input,outputs) =
  let (env,input) =
    qify_config input
      (function VB.P(n) -> Some n | _ -> None)
      (function n -> VB.Q(n)) in
  match env with
    [] -> (nm,input,outputs)
  | _ -> (nm, input, List.map (rename_output env true) outputs)

let unqify (nm,input,output) =
  let (env,input) =
    qify_config input
      (function VB.Q(n) -> Some n | _ -> None)
      (function n -> VB.P(n)) in
  match env with
    [] -> (nm,input,output)
  | _ -> (nm, input, rename_output env true output)

(* --------------------------- Entry point -------------------------- *)

(*
let input2c l =
  List.fold_left
    (function vars ->
      function (cls,vt,modif) ->
	Printf.sprintf "%s %s" vars (VB.vt2c vt))
    "" l
*)

let normalize_all (nm,input,output) =
  (nm,inner_normalize input,inner_normalize output)

let make_all_types system_events event_types =
  let (sched,non_sched) =
    List.partition
      (function (nm,_,_) -> Ast.event_name2c nm = "bossa.schedule")
      event_types in
(*  let non_sched = List.map qify non_sched in*)
  let traversed =
    List.map (function (nm,input,outputs) -> rename_type nm input outputs)
      non_sched in
  let is_system_event (nm,_,_) = Aux.member nm system_events in
  let (system_event_inits,application_event_inits) =
    List.partition is_system_event non_sched in
  let (system_event_traversed,application_event_traversed) =
    List.partition is_system_event traversed in
  let (application_transitions,application_res) =
    process application_event_inits system_event_traversed in
  let (system_transitions,system_res) =
    process system_event_inits
      (system_event_traversed @ application_event_traversed) in
  (system_transitions @ application_transitions,
   (flat_map
      (function (nm,input,outputs) ->
	List.map (function output -> (nm,input,output)) outputs)
      sched) @
   (List.map normalize_all
      ((*List.map unqify*) (system_res @ application_res))))
