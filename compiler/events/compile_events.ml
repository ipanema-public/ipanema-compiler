(* should check that every event is either top level or scheduler level *)

(* now the type rules are all just one input and one output.  But they are
still lists to avoid perturbing the rest of the implementation too much.
This should be fixed at some point. *)

module B = Objects
module VB = Vobjects
module CS = Class_state

(* -------------------- Miscellaneous operations -------------------- *)
exception LookupErrException

let rec lookup x = function
    [] -> raise LookupErrException
  | ((y,v)::r) -> if x = y then v else lookup x r

let rec lookup3 x = function
    [] -> raise LookupErrException
  | ((y,v1,v2)::r) -> if x = y then (v1,v2) else lookup3 x r

let error str = raise (Error.Error str)

let warning str =
  Printf.printf "warning: %s\n" str

let print_config config =
  List.iter
    (function (cls,vt,modif) ->
      Printf.printf "%s%s:%s "
	(CS.class2c cls)
	(if modif = VB.MODIFIED then "!" else "")
	(VB.vt2c vt))
    config;
  Printf.printf "\n"

(* -------------- information from the configuration file ----------- *)

type pre_entry =
    PREENTRY of Ast.event_name * pre_config list * pre_config list
  | INTENTRY of B.identifier * pre_config list * pre_config list
  | GENINTENTRY of Ast.event_name * pre_config list * pre_config list
and pre_config = PRECONF of (string (* class *) * VB.vtype * VB.modified) list

(* ---------------- verification of list of classes ----------------- *)

let check_classes classes =
  List.map (function cls -> (CS.class2c cls,cls)) classes

(* ----------------- verification of event classes ------------------ *)

let rec check_event_classes system_events application_events types =
  (* a type must be defined for each mentioned event *)
  let types =
    Aux.option_filter
      (function
	  PREENTRY(nm,_,_) -> Some nm
	| INTENTRY(nm,_,_) -> None
	| GENINTENTRY(nm,_,_) -> Some nm)
      types
  and all_categorized =
    Aux.union system_events application_events in
  List.iter
    (function nm ->
      if not(Aux.member nm types)
      then error (Printf.sprintf "no type for event %s" (Ast.event_name2c nm)))
    all_categorized;

  (* at least one category must be provided for each typed event *)
  List.iter
    (function nm ->
      if not(Aux.member nm all_categorized)
      then
	error
	  (Printf.sprintf "no category for event %s" (Ast.event_name2c nm)))
    types

(* ------------------------- collapse types ------------------------- *)
(* Create a version of the types with all of the various blocked classes
unified as just BLOCKED *)

(* FIXME: Remove config *)
let abstract_config config =
  let (rest,blocked_vt,blocked_mod) =
    (List.fold_left
       (function (rest,blocked_vt,blocked_mod) ->
	 function
	    x -> (x::rest,blocked_vt,blocked_mod))
       ([],VB.BOT,VB.UNMODIFIED)
       config) in
  let new_config = (CS.BLOCKED,blocked_vt,blocked_mod)::rest in
  List.sort
    (function (c1,_,_) -> function (c2,_,_) ->
      if c1 = c2 then 0 else if CS.less_than(c1,c2) then 1 else -1)
    new_config

let drop_redundant = function
    (q,[input],[output]) ->
      let new_output =
	List.map2
	  (function (icls,ivt,imodd) ->
	    function (ocls,ovt,omodd) as x ->
	      if icls = ocls
	      then
		match (ivt,ovt,omodd) with
		  (VB.MEMBER(l1),VB.MEMBER(l2),VB.MODIFIED)
		  when Aux.set_equal l1 l2 ->
		    (ocls,VB.UNKNOWN,VB.MODIFIED)
		| _ -> x
	      else error "internal error")
	  input output in
      (q,[input],[new_output])
  | x -> x

let abstract_blocked events =
  let tmp = Objects.schedtype in
  Objects.schedtype := B.VIRTSCHED;
  let res = 
    List.map
      (function (nm,inputs_outputs) ->
	(nm,
	 Aux.union_list
	   (List.map
	      (function (q,inputs,outputs) ->
		drop_redundant
		  (q,
		   List.sort compare
		     (Aux.union_list(List.map abstract_config inputs)),
		   List.sort compare
		     (Aux.union_list(List.map abstract_config outputs))))
	      inputs_outputs)))
      events in
  Objects.schedtype := B.PROCSCHED;
  res

(* --------- data structures related to event handler types --------- *)

type entry = CS.classname * VB.vtype * VB.modified

type type_rule = Objects.isquasi * entry list list * entry list list
type vs_type_rule =
    Objects.isquasi * (entry list * Class_state.classname list) list *
      entry list list

(* ----------- verification that no processes are dropped ----------- *)

let check_no_drop (nm,input,output) =
  let all_processes config =
    List.sort compare
      (List.fold_left
	 (function res -> 
	   function (_,vt,_) -> Aux.union (VB.known_contents vt) res)
	 [] config) in
  if not (Aux.subset (all_processes input) (all_processes output))
  then
    (error
       (Printf.sprintf "processes dropped in rule for %s:\n"
	  (Ast.event_name2c nm));
     Printf.printf "Input: "; print_config input;
     Printf.printf "Output: "; print_config output;)

(* ------------ verification of modification information ------------ *)

(* modification now means that ranndom processes are allowed to move around
in the class, so either modification or no modification is always allowed
*)

(* schedule must end up with something in running.  no other handler or
   function can add to running *)
let rec check_running_modifications types in_schedule =
  List.iter
    (function (nm, configs) ->
      List.iter
	(function (q,[input],outputs) ->
	  List.iter
	    (function output ->
	      List.iter2
		(function (incls, invt, inmodd) as inp ->
		  function
		      (CS.RUNNING, outvt, outmodd) ->
			check_running invt outvt inmodd outmodd
			  (in_schedule nm)
		    | _ -> ())
		input output)
	    outputs
	  | _ -> raise (Error.Error "cannot happen"))
	configs)
    types


and check_running invt outvt inmodd outmodd in_schedule =
  check_leq1 invt;
  check_leq1 outvt;
  if in_schedule
  then () (*
    ((match invt with
      VB.EMPTY -> ()
    | VB.UNKNOWN ->
	warning("running state on entry to bossa.schedule is actually empty")
    | VB.BOT -> ()
    | _ ->
	error("running state on entry to bossa.schedule must be empty"));
     (match outvt with
       VB.EMPTY ->
	 error("running state on exit from bossa.schedule cannot be empty")
     | VB.UNKNOWN ->
	 warning("running state on exit from bossa.schedule is actually "^
		 "non-empty")
     | _ -> ());
     (match outmodd with
       VB.MODIFIED -> ()
     | _ -> error("it must be possible to add to the running state in "^
		  "bossa.schedule"))) *)
  else
    (match outmodd with
      VB.MODIFIED ->
	(match (invt,outvt) with
	  (VB.UNKNOWN,_) ->
	    error("outside bossa.schedule, running cannot be modified and "^
		  "initially unknown")
	| (_,VB.UNKNOWN) ->
	    error("outside bossa.schedule, running cannot be modified to "^
		  "unknown")
	| (_,VB.EMPTY) -> ()
	| _ ->
	    let inkc = VB.known_contents invt
	    and outkc = VB.known_contents outvt in
	    (match inkc with
	      [VB.VP(VB.UNKNOWNPROC,_)] ->
		error("outside bossa.schedule, running cannot be modified "^
		      "and initially contain an unknown object")
	    | _ ->
		()
		(*
		if not(inkc = outkc)
		then
		  error("outside bossa.schedule, initial and final values of "
			^"running must be identical")
		*)
		))
    | _ -> ())

and check_leq1 = function
    VB.MEMBER([_]) -> ()
  | VB.MEMBER(_) -> error("running can only contain one element")
  | _ -> ()

(* ----------------- verification of handler types ------------------ *)

let rec collect_values keepqs = function
    VB.MEMBER(l) ->
      if Aux.is_set l
      then List.filter keepqs l
      else (error("duplication not allowed in a member specification"); [])
  | _ -> []

let normalize config env keepqs modfn =
  let pre_classes = List.map (function (cls,x,_) -> cls) config in
  if not(Aux.is_set pre_classes)
  then error("type must map each class to only one specification");
  let pre_values = List.map (function (cls,x,_) -> x) config in
  if not(Aux.is_set(List.flatten(List.map (collect_values keepqs) pre_values)))
  then error("a process can be specified to be in only one class");
  try
    let classes =
      List.map
	(function (cls,x,m) ->
	  let cs_class = lookup cls env in
	  (cs_class, (x, modfn cs_class x m)))
	config in
    List.map
      (function (_,cls) ->
	try
	  let (vl,modd) = lookup cls classes in
	  (cls,vl,modd)
	with LookupErrException -> (cls,VB.UNKNOWN,VB.UNMODIFIED))
      env
  with LookupErrException -> (error("unknown class"); [])

(* -------------- implement backward references with Qs ------------- *)

(* convert every type to map a single input configuration to a single
   output configuration *)

let keepqs _ = true

let dropqs = function
    VB.VP(VB.Q(_),_) -> false
  | _ -> true

(* check that specific interface types are compatible with the generic one  *)

let check_compatible_interface_type generic_interface_types (nm,input,output) =
  (* collect the argument names mentioned in the input *)
  let snm = B.id2c nm in
  if not (snm = "attach" || snm = "detach")
  then
    (let args =
      Aux.map_union
	(function (cls,vp,_) ->
	  List.map (function x -> (cls,x))
	    (Aux.map_union VB.collect_ps (VB.known_contents vp)))
	input in
  (* find a process in a configuration *)
    let find v config =
      let (cls,_,_) =
	List.find
	  (function (cls,vp,_) ->
	    Aux.member v (Aux.map_union VB.collect_ps (VB.known_contents vp)))
	  config
      in cls
    in
  (* check whether a transition is allowed by the generic interface type *)
    let check_allowed (icls,ocls) =
      List.exists
	(function (_,input,output) ->
	  let gicls = find VB.TGT input in
	  let gocls = try find VB.TGT output with Not_found -> gicls in
	  gicls = icls && gocls = ocls)
	generic_interface_types in
  (* for each argument, check that the input position and the output position
     are allowed by one of the generic interface types *)
    List.iter
      (function (icls,v) ->
	let ocls = try find v output with Not_found -> icls in
	if not (check_allowed (icls,ocls))
	then
	  error
	    (Printf.sprintf "incompatible interface type for %s" (B.id2c nm)))
      args)
      
(* ---------- collecting related configuration information ---------- *)

(* input is required to be unmodified *)
let check_unmodified cls x modd =
  if modd = VB.MODIFIED
  then error "all input classes should be unmodified";
  VB.UNMODIFIED

(* on output, if nothing is specified, the default is unmodified *)
let make_modified input printer nm cls x = function
    VB.BOTMOD ->
      let (input_x,input_mod) = lookup3 cls input in
      if x = input_x && cls = CS.RUNNING
      then raise(Error.Error("normalize: "^(printer nm)^
			     ": cannot move around within RUNNING"))
      else VB.UNMODIFIED
  | x -> x

let split_types types env printer =
  List.flatten
    (List.map
       (function (nm,inputs,outputs) ->
	 List.flatten
	   (List.map
	      (function PRECONF(input) ->
		List.map
		  (function PRECONF(output) ->
		    let norm_input =
		      normalize input env dropqs check_unmodified in
		    let norm_output =
		      normalize output env keepqs
			(make_modified norm_input printer nm) in
		    let output_variables =
		      List.flatten
			(List.map
			   (function (co,vo,mo) -> VB.known_contents vo)
			   norm_output) in
		    let (norm_input,norm_output) =
		      List.split
			(List.map2
			   (function (ci,vi,mi) as i ->
			     function (co,vo,mo) as o ->
			       if not(ci = co)
			       then
				 raise
				   (Error.Error("normalize: internal error"));
			       match (ci,vi,vo,mo) with
				 (_,VB.EMPTY,VB.UNKNOWN,VB.UNMODIFIED) ->
				   (i,(co,VB.EMPTY,mo))
			       | (CS.RUNNING,(VB.MEMBER(l) as x),VB.UNKNOWN,_)
				   when not((Aux.intersect
					       (VB.known_contents x)
					       output_variables) = [])
				 ->
				   (i,(CS.RUNNING,VB.EMPTY,VB.MODIFIED))
			       | _ -> (i,o))
			   norm_input norm_output) in
		    (nm,norm_input,norm_output))
		  outputs)
	      inputs))
       types)

(* step 1: collect types with same event name *)
let assoc_types_nmls types checks =
  List.fold_left
    (function
	[] -> (function (nm,q,input,output) -> [(nm,[(q,[input],[output])])])
      | (nm1,types1)::rest as y ->
	  (function (nm,q,input,output) ->
	    checks (nm, nm1);
	    if nm = nm1
	    then (nm,((q,[input],[output])::types1))::rest
	    else (nm,[(q,[input],[output])])::y))
    [] types

let rec check_event_name = function (* new name, then old name *)
    ((Ast.EVENT_NAME(nmlst,star,_) as nm),
     (Ast.EVENT_NAME(nmlst1,star1,_) as nm1)) ->
      if nmlst = nmlst1 && not(star1 = star)
      then error ("event repeated with and without *: "^
		  (Ast.event_name2c(nm1))^" and "^
		  (Ast.event_name2c(nm)));
      if proper_prefix(nmlst,nmlst1) && not star
      then error ("* expected for hierarchical event "^
		  (Ast.event_name2c(nm))^" as compared to "^
		  (Ast.event_name2c(nm1)))

and proper_prefix = function
    ([],[]) -> false
  | ([],_) -> true
  | (_,[]) -> false
  | (x::xs,y::ys) -> B.ideq(x,y) && proper_prefix(xs,ys)

let check_interface_name (newname, oldname) = ()

let reverse_sort_by_name types =
  List.rev
    (List.sort
       (function (Ast.EVENT_NAME(nm,_,_),_,_,_) ->
	 function (Ast.EVENT_NAME(nm1,_,_),_,_,_) -> compare nm nm1)
       types)

(* to avoid recursive modules... *)
(* don't care about the difference between quasi types and other types when
constructing the automaton *)
let drop_one_many types =
  List.flatten
    (List.map
       (function (nm,types) ->
	 List.map
	   (function (q,[input],outputs) -> (nm,input,outputs)
	     | _ -> raise (Error.Error "cannot happen"))
	   types)
       types)

let id x = x
let id2 x y = y
let truefn x y = true

let add_modifier modifier types =
  List.map (function (nm,input,output) -> (nm,modifier,input,output)) types

let preprocess_event_types types quasitypes class_env =
  let flat_types =
    add_modifier B.NOT_QUASI (split_types types class_env Ast.event_name2c) in
  let flat_quasitypes =
    add_modifier B.QUASI (split_types quasitypes class_env Ast.event_name2c) in
  let flat_types = flat_types @ flat_quasitypes in
  if not(Aux.is_set flat_types)
  then error("duplicate specifications not allowed");
  let types = reverse_sort_by_name flat_types in
(*  List.iter check_no_drop types; *)
  let one = assoc_types_nmls types check_event_name in
  check_running_modifications one
    (function nm ->
      (Ast.event_name2c nm) = "bossa.schedule.*" ||
      (Ast.event_name2c nm) = "bossa.schedule.block" ||
      (Ast.event_name2c nm) = "restart");
  one

let check_event_types types quasitypes system_events app_events
    proposed_init_automaton proposed_automaton class_env =
  let collected_event_and_quasitypes =
    preprocess_event_types types quasitypes class_env in
  let (unmatched_transitions,app_init_automaton,app_automaton) =
    Quasi.make_all_types system_events
      (drop_one_many collected_event_and_quasitypes)
      proposed_init_automaton proposed_automaton in
  (unmatched_transitions, collected_event_and_quasitypes,
   (abstract_blocked collected_event_and_quasitypes),
   app_init_automaton,app_automaton)

let check_interface_types types generic_interface_types class_env =
  Printf.printf "check interface types\n";
  let flat_types = split_types types class_env B.id2c in
  let flat_gtypes =
    split_types generic_interface_types class_env Ast.event_name2c in
  if not(Aux.is_set flat_types)
  then error("duplicate specifications not allowed");
  List.iter (check_compatible_interface_type flat_gtypes) flat_types;
  let flat_types = add_modifier B.NOT_QUASI flat_types in
  let types =
    List.rev
      (List.sort
	 (function (nm,_,_,_) -> function (nm1,_,_,_) -> compare nm nm1)
	 flat_types) in
  let one = assoc_types_nmls types check_interface_name in
  check_running_modifications one (function nm -> false);
  (one, abstract_blocked one)

let check_vs_interface_types types class_env =
  let flat_types = split_types types class_env B.id2c in
  if not(Aux.is_set flat_types)
  then error("duplicate specifications not allowed");
  let flat_types = add_modifier B.NOT_QUASI flat_types in
  let types =
    List.rev
      (List.sort
	 (function (nm,_,_,_) -> function (nm1,_,_,_) -> compare nm nm1)
	 flat_types) in
  let one = assoc_types_nmls types check_interface_name in
  check_running_modifications one (function nm -> false);
  one

let collect_vs_types flat_types =
  let types =
    List.rev
      (List.sort
	 (function (nm,_,_) -> function (nm1,_,_) -> compare nm nm1)
	 flat_types) in
  let types =
    List.map (function (nm,input,output) -> (nm,B.NOT_QUASI,input,output))
      types in
  assoc_types_nmls types check_interface_name

(* -------------------------- unblock.timer ------------------------- *)

let check_unblock_timer types =
  let found1 = ref false in
  let found2 = ref false in
  let found3 = ref false in
  List.iter
    (function
	PREENTRY(nm,_,_) ->
	  if Ast.event_name2c nm = "unblock.timer.target.*"
	  then found1 := true
	  else if Ast.event_name2c nm = "unblock.timer.notarget.*"
	  then found2 := true
	  else if Ast.event_name2c nm = "unblock.timer.persistent.*"
	  then found3 := true
	  else if Ast.event_name2c2 nm = Some "unblock.timer"
	  then raise (Error.Error "invalid timer type specification (target or notarget required)")
      |	_ -> ())
    types;
  if not (!found1 && !found2 && !found3)
  then raise (Error.Error "type required for unblock.timer.*")

(* ------------ verification of configuration information ----------- *)

let compile_config (initial_events,system_events,application_events,
		    top_level_events,scheduler_events,
		    types,proposed_init_automaton,
		    proposed_automaton,quasitypes,vsinterface)  =
  let classes = CS.all_classes_nowhere false in
  let class_env = check_classes classes in
  Objects.schedtype := B.VIRTSCHED;
  let vsclasses = CS.all_classes_nowhere true in
  let vsclass_env = check_classes vsclasses in
  Objects.schedtype := B.PROCSCHED;
  (* check that a type is defined for each event *)
  check_event_classes system_events application_events
    (types @ quasitypes @ vsinterface);
  (* check some particularities of unblock.timer *)
  check_unblock_timer types;
  (* classify the types according to how they are labelled by the parser *)
  let (event_types,interface_types,generic_interface_types) =
    List.fold_left
      (function (eprev,iprev,giprev) ->
	function
	    PREENTRY(name,input,output) ->
	      ((name,input,output)::eprev,iprev,giprev)
	  | INTENTRY(name,input,output) ->
	      (eprev,(name,input,output)::iprev,giprev)
	  | GENINTENTRY(name,input,output) ->
	      (eprev,iprev,(name,input,output)::giprev))
      ([],[],[]) types in
  if generic_interface_types = []
  then error "no generic interface type supplied";
  (* classify the quasitypes according to how they are labelled by the parser*)
  let quasi_event_types =
    List.map
      (function
	  PREENTRY(name,input,output) -> (name,input,output)
	| INTENTRY(name,input,output) ->
	    error "no quasi sequences for interfaces"
	| GENINTENTRY(_,input,output) ->
	    error "no quasi sequences for interfaces")
      quasitypes in
  let vs_interface_types =
    List.map
      (function
	  INTENTRY(name,input,output) -> (name,input,output)
	| PREENTRY(_,input,output)
	| GENINTENTRY(_,input,output) -> error "only interface types expected")
      vsinterface in
  (initial_events,system_events,application_events,
   top_level_events,scheduler_events,
   check_event_types (generic_interface_types@event_types) quasi_event_types
     system_events application_events
     proposed_init_automaton proposed_automaton class_env,
   check_interface_types interface_types generic_interface_types class_env,
   check_vs_interface_types vs_interface_types vsclass_env)
