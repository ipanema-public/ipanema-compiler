type entry = Class_state.classname * Vobjects.vtype * Vobjects.modified
type intinfo = INT | UNINT (* interrupt permitted before event *)
type required = REQUIRED | OPTIONAL

val make_all_types :
    Ast.event_name list
    -> (Ast.event_name * entry list * entry list list) list
	-> (intinfo * Ast.event_name) Nfa2dfa.fa_state list
	  -> (intinfo * Ast.event_name) Nfa2dfa.fa_state list
	      -> ((Ast.event_name * (entry list) Model.sequence) list *
		    (Ast.event_name * intinfo * required)
		    Nfa2dfa.fa_state list *
		    (Ast.event_name * intinfo * required)
		    Nfa2dfa.fa_state list)

(* used to get the output configurations resulting from a given input
configuration *)
val get_outputs :
    bool (* only accept rule if src/tgt matches? *) ->
    entry list -> (Ast.event_name * entry list * entry list list) list ->
      Ast.event_name -> entry list list
