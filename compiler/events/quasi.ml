module B = Objects
module VB = Vobjects
module CS = Class_state

(* ----------------------- Exported functions ----------------------- *)

type intinfo = INT | UNINT

(* -------------------- Miscellaneous operations -------------------- *)

exception LookupErrException of string

let rec lookup x = function
    [] -> raise (LookupErrException (VB.vp2c x))
  | ((y,v)::r) -> if x = y then v else lookup x r

let rec lookup_int x = function
    [] -> raise (LookupErrException (Printf.sprintf "%d" x))
  | ((y,v)::r) -> if x = y then v else lookup_int x r

let error str = raise (Error.Error str)

let print_config config =
  List.iter
    (function (cls,vt,modif) ->
      Printf.printf "%s%s:%s "
	(CS.class2c cls)
	(if modif = VB.MODIFIED then "!" else "")
	(VB.vt2c vt))
    config;
  Printf.printf "\n"

let print_config_all config =
  List.iter
    (function (cls,vt,modif) ->
      Printf.printf "%s%s:%s "
	(CS.class2c cls)
	(if modif = VB.MODIFIED then "!" else "")
	(VB.vt2c_all vt))
    config;
  Printf.printf "\n"

let internal_error str = raise (Error.Error(str^": internal error"))

(* ------------------- Rename the input and output ------------------ *)

let ctr = ref 0

(* verification already ensures that the variables referred to by the
input type are all distinct *)
let rec mk_input_env input str rename_unknown_only =
  let (entries,env) =
    List.fold_left
      (function (entries,env) ->
	function (cls,vt,modif) as x ->
	  match vt with
	    VB.UNKNOWN -> (x::entries,env)
	  | VB.EMPTY -> (x::entries,env)
	  | VB.MEMBER(l) ->
	      let (m_entries,m_env) =
		List.fold_left
		  (function (m_entries,m_env) ->
		    function
			(VB.VP(vp,l) as x) ->
			  (match vp with
			    VB.UNKNOWNPROC ->
			      if rename_unknown_only
			      then
				new_name x m_entries
				  (Printf.sprintf "%s_%s" str (CS.class2c cls))
				  m_env
			      else (x::m_entries,m_env)
			  | VB.BOTPROC -> error "unexpected bottom value"
			  | _ ->
			      if not rename_unknown_only
			      then
				new_name x m_entries
				  (Printf.sprintf "%s_%s" str (CS.class2c cls))
				  m_env
			      else (x::m_entries,m_env))
		      |	VB.EQUAL(l) -> internal_error "mk_input_env")
		  ([],env) l
	      in ((cls,VB.MEMBER(List.rev m_entries),modif)::entries,m_env)
	  | VB.BOT -> error "unexpected bottom value")
      ([],[]) input in
  (List.rev entries,env)

and new_name vp entries str env =
  match vp with
    VB.VP(vp,l) as x ->
      let n = !ctr in
      ctr := !ctr + 1;
      (VB.VP(VB.S(n,"new"),l)::entries,
       (VB.VP(vp,l),VB.VP(VB.S(n,"new"),l))::env)
  | _ -> internal_error "new_name"

let rename_output env id_on_unbound entries =
  List.map
    (function (cls,vt,modif) ->
      (cls,
       (match vt with
	 VB.UNKNOWN -> vt
       | VB.EMPTY -> vt
       | VB.MEMBER(l) ->
	   VB.MEMBER(List.map
		       (function
			   (VB.VP(vp,l) as x) ->
			     (match vp with
			       VB.UNKNOWNPROC -> x
			     | VB.BOTPROC -> error "unexpected bottom value"
			     | _ ->
				 (try lookup x env
				 with LookupErrException str ->
				   if id_on_unbound
				   then x
				   else raise (LookupErrException str)))
			 | _ -> internal_error "rename_output")
		       l)
       | VB.BOT -> error "unexpected bottom value"),
       modif))
    entries
	
let rename_type nm input outputs =
  let (new_input,env) = mk_input_env input (Ast.event_name2c nm) false
  in (nm, new_input, List.map (rename_output env false) outputs)

(* --------- Collect types compatible with the configuration -------- *)

(* cur is the current configuration *)

let check_compatible cur input match_tgt =
  (if not (List.length cur = List.length input)
  then
    begin
      Printf.printf "compatible cur %d input %d\n"
	(List.length cur) (List.length input);
      print_config_all cur;
      print_config_all input
    end);
  List.for_all2
    (function (cur_cls,cur_vt,cur_mod) ->
      function (input_cls,input_vt,input_mod) ->
	(if not (cur_cls = input_cls)
	then
	  raise
	    (Error.Error "check_compat: inconsistent class and input class"));
	match cur_vt with
	  VB.UNKNOWN -> true
	| VB.EMPTY ->
	    (match input_vt with
	      VB.UNKNOWN -> true
	    | VB.EMPTY -> true
	    | VB.MEMBER(_) -> false
	    | VB.BOT -> error "unexpected bottom value")
	| VB.MEMBER(l) ->
	    (match input_vt with
	      VB.UNKNOWN ->
		if match_tgt
		then
		  let ps_list = VB.collect_ps (VB.EQUAL l) in
		  not (Aux.member VB.SRC ps_list || Aux.member VB.TGT ps_list)
		else true
	    | VB.EMPTY -> false
	    | VB.MEMBER(l1) ->
		if match_tgt
		then
		  let ps_list = VB.collect_ps (VB.EQUAL l) in
		  let ps_list1 = VB.collect_ps (VB.EQUAL l1) in
		  let src_needed = Aux.member VB.SRC ps_list in
		  let src_found = Aux.member VB.SRC ps_list1 in
		  let tgt_needed = Aux.member VB.TGT ps_list in
		  let tgt_found = Aux.member VB.TGT ps_list1 in
		  (not src_needed || src_found) &&
		  (not tgt_needed || tgt_found)
		else true
	    | VB.BOT -> error "unexpected bottom value")
	| VB.BOT -> error "unexpected bottom value")
    cur input

let possible_types cur types name match_tgt =
  Aux.option_filter
    (function (nm,input,output) ->
      if Ast.event_name_equal nm name && check_compatible cur input match_tgt
      then Some (nm,input,output)
      else None)
    types

(* --- Build environments from a configuration and a single input --- *)

(* make a list of all possible environments *)
(* We know that RUNNING is a process variable.  We have to assume that
the other classes are queues.  For one input, we get a set of
environments, representing all of the possible matches.  The classes
are assumed to occur in the same order in both configurations. *)

(* cur is the current configuration *)

let vplist2c l =
  Aux.set2c (List.map VB.vp2c l)

let rec envs_for_compatible_input cur input =
  (if not (List.length cur = List.length input)
  then
    Printf.printf "cur %d input %d\n" (List.length cur) (List.length input));
  let envs =
  List.fold_left2
    (function envs ->
      function (cur_cls,cur_vt,cur_mod) ->
	function (input_cls,input_vt,input_mod) ->
	  match cur_vt with
	    VB.UNKNOWN -> envs
	  | VB.EMPTY -> envs
	  | VB.MEMBER(cur_l) as x ->
	      (match input_vt with
		VB.UNKNOWN ->
		  List.map
		    (function env ->
		      (List.map (function cur -> (cur,cur)) cur_l) @ env)
		    envs
	      | VB.EMPTY -> error "incompatible types"
	      | VB.MEMBER(input_l) ->
		  (* BUG: This is not correct, since a process mentioned in
		     the current configuration might not be one of the ones
		     mentioned in the rule *)
		  (match cur_cls with
		    CS.RUNNING ->
		      (match (cur_l,input_l) with
			([cur_vp],[input_vp]) ->
			  List.map
			    (function x -> [(cur_vp,input_vp)] @ x)
			    envs
		      |	_ ->
			  error
			    (Printf.sprintf "only one process expected %d %d"
			       (List.length cur_l) (List.length input_l)))
		  | _ ->
		      let new_envs = perm cur_l input_l in
		      Aux.flat_map
			(function new_env ->
			  List.map (function old_env -> new_env @ old_env)
			    envs)
			new_envs)
	      | VB.BOT -> error "unexpected bottom value")
	  | VB.BOT -> error "unexpected bottom value")
    [[]] cur input in
  envs

(* env represented as a pair of a list of names from lst and a list of
names from replst or lst *)

and perm lst replst =
 List.map (function (a,b) -> List.combine a b)
    (List.fold_left
       (function envs ->
	 function lst_elem ->
	   Aux.flat_map
	     (function (lst_env,rep_env) ->
	       List.fold_left
		 (function acc ->
		   function rep_elem ->
		     if (Aux.member rep_elem rep_env)
		     then acc
		     else (lst_elem::lst_env,rep_elem::rep_env)::acc)
		 [(lst_elem::lst_env,lst_elem::rep_env)] replst)
	     envs)
       [([],[])] lst)

(* only allow envs that match src/tgt *)
let check_match_tgts config envs =
  List.filter
    (function env ->
      List.for_all
	(function
	    (VB.VP(VB.TGT,_),VB.VP(VB.TGT,_)) -> true
	  | (VB.VP(VB.SRC,_),VB.VP(VB.SRC,_)) -> true
	  | (VB.VP(VB.TGT,_),_) -> false
	  | (_,VB.VP(VB.TGT,_)) -> false
	  | (VB.VP(VB.SRC,_),_) -> false
	  | (_,VB.VP(VB.SRC,_)) -> false
	  | (VB.EQUAL(_),_) -> internal_error "check_match_tgts"
	  | (_,VB.EQUAL(_)) -> internal_error "check_match_tgts"
	  | _ -> true)
	env)
    envs

(* --- Build outputs from a configuration and a single environment -- *)

(* compute the effect of a type on the current configuration based on
a given environment *)
 
let rec compute_output cur output env =
  let res =
  List.fold_left
    (function res ->
      function (cur_cls,cur_vt,cur_mod) ->
	match cur_vt with
	  VB.UNKNOWN -> res
	| VB.EMPTY -> res
	| VB.MEMBER(cur_l) ->
	    List.fold_left
	      (function res ->
		function cur ->
		  let type_name = lookup cur env in
		  match hunt type_name output with
		    Some dest -> add cur dest (del cur res false) false
		  | None -> res)
	      res cur_l
	| VB.BOT -> error "unexpected bottom value")
    cur cur in
  (* account for other effects that take place in the rule *)
  let postprocessed =
    List.map2
      (function (out_cls,out_vt,out_mod) ->
	function (res_cls,res_vt,res_mod) ->
	  (if not (out_cls = res_cls)
	  then raise (Error.Error "post: inconsistent class and input class"));
	  (out_cls,
	   (match (out_vt,res_vt) with
	     (VB.EMPTY,VB.UNKNOWN) -> VB.EMPTY
	   | (VB.MEMBER(x),VB.UNKNOWN) -> VB.MEMBER([VB.VP(VB.UNKNOWNPROC,[])])
	   | (VB.EMPTY,VB.MEMBER(x)) ->
	       raise (Error.Error ("conflict in "^(CS.class2c out_cls)))
	   | (VB.MEMBER(x),VB.EMPTY) -> VB.MEMBER([VB.VP(VB.UNKNOWNPROC,[])])
	   | _ -> res_vt),
	   VB.UNMODIFIED))
      output res in
  postprocessed

and hunt name output =
  let destinations =
    List.fold_left
      (function destinations ->
	function (cls,vt,modif) ->
	  match vt with
	    VB.UNKNOWN -> destinations
	  | VB.EMPTY -> destinations
	  | VB.MEMBER(l) ->
	      if (Aux.member name l)
	      then cls::destinations
	      else destinations
	  | VB.BOT -> error "unexpected bottom value")
      [] output in
  match destinations with
    [] -> None
  | [destination] -> Some destination
  | _ -> error "duplicated variables not supported"

(* if we delete all of the information about an output state, the
modification becomes unmodified *)
and del name res is_output =
  List.map
    (function (cls,vt,modif) as x ->
       (match vt with
	 VB.UNKNOWN -> x
       | VB.EMPTY -> x
       | VB.MEMBER(l) ->
	   (match Aux.remove name l with
	     [] -> 
	       (match cls with
		 CS.RUNNING -> (cls, VB.EMPTY, modif)
	       | _ -> (cls, VB.UNKNOWN,
		       if is_output then VB.UNMODIFIED else modif))
	   | new_l -> (cls, VB.MEMBER new_l, modif))
       | VB.BOT -> error "unexpected bottom value"))
    res

(* when we add information to an output state, the modification becomes
modified *)
and add name dest res is_output =
  List.map
    (function (cls,vt,modif) as x ->
      if cls = dest
      then
	(cls,
	 (match vt with
	   VB.UNKNOWN -> VB.MEMBER([name])
	 | VB.EMPTY -> VB.MEMBER([name])
	 | VB.MEMBER(l) -> VB.MEMBER(name :: l)
	 | VB.BOT -> error "unexpected bottom value"),
	 if is_output then VB.MODIFIED else modif)
      else x)
    res

let print_env env =
  List.iter
    (function (cur, next) ->
      Printf.printf "(%s,%s) " (VB.vp2c cur) (VB.vp2c next))
    env;
  Printf.printf "\n"

let output_for_all_envs cur output envs =
  List.map (function env -> compute_output cur output env) envs

(* -------------------- History of quasisequences ------------------- *)

type entry = Class_state.classname * Vobjects.vtype * Vobjects.modified

let rec search config = function
    Model.START(config1) as x -> if config = config1 then Some x else None
  | (Model.TRANSITION(prev,nm,config1)) as x ->
      if config = config1 then Some x else search config prev

let rec find_mention transitions new_input =
  match transitions with
    [] -> error "no matching transition"
  | (transition::rest) ->
      (match search new_input transition with
	None -> find_mention rest new_input
      |	Some(transition) -> transition)

let rec print_transitions = function
    Model.START(config) -> print_config config
  | Model.TRANSITION(prev,nm,config) -> print_transitions prev;
      Printf.printf " -%s-> " (Ast.event_name2c nm);
      print_config config

let rec print_qs_transitions = function
    Model.START((_,config)) -> print_config config
  | Model.TRANSITION(prev,nm,(_,config)) -> print_qs_transitions prev;
      Printf.printf " -%s-> " (Ast.event_name2c nm);
      print_config config

(* ----------------------- Put it all together ---------------------- *)

and normalize t =
  List.map
    (function (cls,vt,modif) ->
      (cls,
       (match vt with
	 VB.UNKNOWN -> vt
       | VB.EMPTY -> vt
       | VB.MEMBER(l) -> VB.MEMBER(List.sort compare (Aux.make_set l))
       | VB.BOT -> vt),
       modif))
    t

let apply_rules match_tgts print_name possible_types config
    build_transition_state =
  let (config,config_env) = mk_input_env config "config" true in
  let rev_env = List.map (function (x,y) -> (y,x)) config_env in
  Aux.map_union
    (function (nm,input,outputs) ->
      let envs = envs_for_compatible_input config input in
      let envs =
	if match_tgts (* only allow envs that match src/tgt *)
	then check_match_tgts config envs
	else envs in
      let new_inputs =
	Aux.map_union
	  (function output ->
	    let outres = output_for_all_envs config output envs in
	    List.map
	      (function out -> normalize (rename_output rev_env true out))
	      outres)
	  outputs in
      List.map (build_transition_state nm) new_inputs)
    possible_types

(* In the spirit of apply_rules, but to be used from the outside *)
let get_outputs match_tgt config types nm =
  let possible = possible_types config types nm true in
  apply_rules match_tgt Ast.event_name2c possible
    config (function nm -> function sts -> sts)

(* ---------------------- Explore an automaton ---------------------- *)

(* Idea: we want to explore all of the transitions allowed by the provided
automaton.  At any point we have a configuration and are at a particular
point in the automaton.  At this point in the automaton there are a number
of possible event names.  Event names are either optional or required.  We
should be able to find at least one matching rule for every required event
name.  For each event name (optional or required) for which we have a rule,
we repeat the process with respect to all possible outputs generated by all
possible matching rules.  This process ends when we reach a final state. *)

(* We need to provide Model.model with the following information:

   1. A list of initial configurations.

   2.  A function (get_output) that when provided a configuration returns a
       list of pairs each containing an event name and a new configuration.
       This result represents all of the things that can happen given the
       input configuration.

   3.  A function (find_transition) that takes a triple of an input
       configuration, an event name, and an output configuration as well as
       a sequence of such transitions and returns an indication of whether
       the triple occurs in the sequence of transitions.

   4.  A function (print_config) that prints a configuration
*)

(* The key point is to decide the type of a configuration.  Because our
exploration is to be guided by the automaton provided with the event types,
the configuration must indicate where we are in this automaton.  Of course
the configuration must also contain the contents of the various classes *)

let lookup_all n aut =
  List.filter
    (function Nfa2dfa.EDGE(src,_,tgt) -> n = src | _ -> false)
    aut

type required = REQUIRED | OPTIONAL

(* no one reads from terminated, so it should always be unknown *)
let drop_terminated input =
  List.map
    (function
	(CS.TERMINATED,vt,modif) -> (CS.TERMINATED,VB.UNKNOWN,modif)
      |	x -> x)
    input

let aut_compute_outputs starting_state types aut =

  let tbl =
    (Hashtbl.create(500) :
       (int * entry list, (int * entry list) Model.sequence) Hashtbl.t) in
  let exttbl =
    (Hashtbl.create(500) :
       (int * entry list, (int * entry list) Model.sequence) Hashtbl.t) in
  
  let get_output_states (state,config) =
    if state = -1 then []
    else
      let out_edges = lookup_all state aut in
      Aux.map_union
	(function
	    Nfa2dfa.EDGE(src,(nm,int,required),tgt) ->
	      let res =
		apply_rules false Ast.event_name2c
		  (possible_types config types nm false)
		  config
		  (function nm ->
		    function new_input ->
		      (nm,(tgt,drop_terminated new_input))) in
	      (match (required,res) with
		(REQUIRED,[]) ->
		  (Printf.printf "error for %s between %d and %d\n"
		     (Ast.event_name2c nm) src tgt;
		   [(nm,(-1,config))])
	      |	_ -> res)
	  | _ -> [])
	out_edges in
  
  let find_transition (src,nm,tgt) tbl =
    try Model.PREVTRANS(Hashtbl.find tbl src,[])
    with Not_found -> Model.NOT_FOUND in

  Model.model [starting_state] get_output_states find_transition tbl exttbl
    (function (aut_state,config) ->
      Printf.printf "%d: " aut_state; print_config config; Printf.printf "\n")

(* --- Collect the transitions that end in -1, indicating an error -- *)

let collect_unmatched_transitions transitions =
  let rec strip = function
      Model.START(_,config) -> Model.START(config)
    | Model.TRANSITION(prev,nm,(_,config)) ->
	Model.TRANSITION(strip prev,nm,config) in
  let (configs,unmatched_transitions) = 
    List.fold_left
      (function (configs,unmatched_transitions) ->
	function
	    Model.START(-1,config) ->
	      internal_error "collect_unmatched_transitions"
	  | Model.TRANSITION(prev,nm,(-1,config)) as x ->
	      if Aux.member (nm,config) configs
	      then (configs,unmatched_transitions)
	      else ((nm,config) :: configs,
		    (nm,strip prev) :: unmatched_transitions)
	  | _ -> (configs,unmatched_transitions))
      ([],[]) transitions in
  unmatched_transitions

(* ------------------------ Adding interrupts ----------------------- *)

(* add interrupts before any interruptible event and after any state that
has no outgoing transition *)

let add_interrupts automaton sys_events =
  let (final_states,all_states) =
    let (start_states,end_states) =
      List.fold_left
	(function (start_states,end_states) ->
	  function
	      Nfa2dfa.EDGE(src,_,tgt) ->
		(Aux.union [src] start_states, Aux.union [tgt] end_states)
	    | _ -> (start_states,end_states))
	([],[]) automaton in
    (Aux.subtract end_states start_states,Aux.union start_states end_states) in
  let max_state =
    List.fold_left
      (function cur_max -> function n -> if n > cur_max then n else cur_max)
      0 all_states in
  let ct = ref max_state in
  let new_state _ = (ct := (!ct) + 1; !ct) in
  Aux.flat_map
    (function
	Nfa2dfa.EDGE(src,(interruptible,nm),tgt) ->
	  let snm = Ast.event_name2c nm in
	  let nm_required =
	    if snm = "bossa.schedule" then OPTIONAL else REQUIRED in
	  let edges_before =
	    match interruptible with
	      UNINT -> []
	    | INT ->
		let int_state = new_state() in
		Nfa2dfa.EDGE(int_state,(nm,INT,nm_required),tgt) ::
		(Aux.flat_map
		   (function int_nm ->
		     let int_label = (int_nm,INT,OPTIONAL) in
		     [Nfa2dfa.EDGE(src,int_label,int_state);
		       Nfa2dfa.EDGE(int_state,int_label,int_state)])
		   sys_events) in
	  let edges_after =
	    if Aux.member tgt final_states
	    then
	      let int_state2 = new_state() in
	      Nfa2dfa.EDGE(src,(nm,INT,nm_required),int_state2) ::
	      (Aux.flat_map
		 (function int_nm ->
		   let int_label = (int_nm,INT,OPTIONAL) in
		   [Nfa2dfa.EDGE(int_state2,int_label,int_state2);
		     Nfa2dfa.EDGE(int_state2,int_label,tgt)])
		 sys_events)
	    else [] in
	  Nfa2dfa.EDGE(src,(nm,interruptible,nm_required),tgt) ::
	  edges_before @ edges_after
      |	_ -> [])
    automaton

(* --------------------------- Entry point -------------------------- *)

(* the starting states should really be specified with the automata in the
configuration file *)

let print_type
    ((nm,input,outputs) : (Ast.event_name * entry list * entry list list)) =
  Printf.printf "%s\n" (Ast.event_name2c nm);
  Printf.printf "input:\n";
  print_config input;
  Printf.printf "outputs:\n";
  List.iter print_config outputs;
  Printf.printf "\n"

let make_all_types sys_events event_types init_automaton automaton =
  (* no interrupts in the initial events, but add_interrupts seems to
     change the type, so we have to call it *)
  let init_automaton = add_interrupts init_automaton [] in
  let automaton = add_interrupts automaton sys_events in
  Printf.printf "converted to automata\n"; flush stdout;
  let (init_dfa,init_final) = Nfa2dfa.nfa_to_dfa init_automaton [0] in
  let (dfa,dfa_final) = Nfa2dfa.nfa_to_dfa automaton [0] in
  Printf.printf "converted to DF automata\n"; flush stdout;
  let (init_configs,init_transitions) =
    aut_compute_outputs
      (0,List.map
	 (function
	     CS.NOWHERE ->
	       (CS.NOWHERE, VB.MEMBER([VB.VP(VB.UNKNOWNPROC,[])]),
		VB.UNMODIFIED)
	   | cls -> (cls, VB.EMPTY, VB.UNMODIFIED))
	 (CS.all_classes_nowhere true))
      event_types init_dfa in
  Printf.printf "computed outputs from init_dfa\n"; flush stdout;
  let (configs,transitions) =
    aut_compute_outputs
      (0,List.map
	 (function
	     CS.RUNNING ->
	       (CS.RUNNING, VB.MEMBER([VB.VP(VB.UNKNOWNPROC,[])]),
		VB.UNMODIFIED)
	   | cls -> (cls, VB.UNKNOWN, VB.UNMODIFIED))
	 (CS.all_classes_nowhere true))
      event_types dfa in
  Printf.printf "computed outputs from standard init_dfa\n"; flush stdout;
  let unmatched_transitions =
    List.sort compare
      (collect_unmatched_transitions init_transitions @
       collect_unmatched_transitions transitions) in
  Printf.printf "transitions %d unmatched %d\n"
    (List.length transitions)
    (List.length unmatched_transitions);
  (unmatched_transitions, init_dfa, dfa)
