type entry = Class_state.classname * Vobjects.vtype * Vobjects.modified

type 'a fa_state = EDGE of int * 'a * int | FINAL of int

val nfa_to_dfa :
    'a fa_state list -> int list -> ('a fa_state list * int list)
