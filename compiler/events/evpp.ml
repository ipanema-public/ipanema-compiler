module B = Objects
module VB = Vobjects
module CS = Class_state
open Format

(* -------------------- Miscellaneous operations -------------------- *)

let rec print_between between_fn elem_fn = function
    [] -> ()
  | [x] -> elem_fn x
  | x :: xs -> (elem_fn x; between_fn(); print_between between_fn elem_fn xs)

let pp_id id = print_string(Printf.sprintf "\"%s\"" (B.id2c(id)))

let pp_comma _ = (print_string ","; print_space())

let pp_semic _ = (print_string ";"; print_cut())

let nl_ifnonempty = function
    x::xs -> force_newline()
  | _ -> ()

let pp_list f l =
  print_string "["; open_box 0; print_between pp_semic f l; close_box();
  print_string "]"

type entry = CS.classname * VB.vtype * VB.modified
type type_rule = B.isquasi * entry list list * entry list list
type vs_type_rule =
    B.isquasi * (entry list * CS.classname list) list * entry list list

let internal_error str = raise (Error.Error(str^": internal error"))

(* ------------------------- Initialization ------------------------- *)

let pp_type_decl() =
  print_string "module B = Objects"; force_newline();
  print_string "module VB = Vobjects"; force_newline();
  print_string "module CS = Class_state"; force_newline();
  force_newline();
  print_string "type entry = (CS.classname * VB.vtype * VB.modified)";
  force_newline();
  force_newline();
  print_string "type event_class_type =
    Objects.isquasi *
      (entry list * Class_state.classname list) list *
      entry list list";
  force_newline();
  force_newline()

(* -------------------------- Event types --------------------------- *)

let pp_vprocname = function
    VB.SRC -> print_string "VB.SRC"
  | VB.TGT -> print_string "VB.TGT"
  | VB.P(n) -> print_string "VB.P("; print_int n; print_string ")"
  | VB.Q(n) -> print_string "VB.Q("; print_int n; print_string ")"
  | VB.S(n,str) -> print_string "VB.P("; print_int n; print_string ")"
  | VB.ARG(n) -> print_string "VB.ARG("; print_int n; print_string ")"
  | VB.UNKNOWNPROC -> print_string "VB.UNKNOWNPROC"
  | VB.BOTPROC -> internal_error "pp_vprocname"

let rec pp_vp = function
    VB.VP(vp,[]) ->
      print_string "VB.VP("; pp_vprocname vp; print_string ",[])"
  | VB.EQUAL(vps) ->
      print_string "VB.EQUAL([";
      print_between (function x -> print_string "; ") pp_vp vps;
      print_string "])"
  | _ -> internal_error "pp_vp"

let rec pp_vt = function
    VB.UNKNOWN -> print_string "VB.UNKNOWN"
  | VB.EMPTY -> print_string "VB.EMPTY"
  | VB.MEMBER(l) ->
      print_string "VB.MEMBER("; pp_list pp_vp l; print_string ")"
  | VB.BOT -> internal_error "pp_vt"

let pp_mod = function
    VB.MODIFIED -> print_string "VB.MODIFIED"
  | VB.UNMODIFIED -> print_string "VB.UNMODIFIED"
  | VB.BOTMOD -> internal_error "pp_mod"

let pp_entry (cls,vt,modd) =
  print_string "(CS.";
  print_string (CS.class2c(cls));
  pp_comma();
  pp_vt(vt);
  pp_comma();
  pp_mod(modd);
  print_string ")"

let pp_quasi = function
    B.QUASI -> print_string "B.QUASI"
  | B.NOT_QUASI -> print_string "B.NOT_QUASI"

let pp_type(q,input,output) =
  print_string "(";
  pp_quasi q;
  pp_comma();
  pp_list
    (function x ->
      print_string "("; pp_list pp_entry x; pp_comma(); print_string "[])")
    input;
  pp_comma();
  pp_list (pp_list pp_entry) output;
  print_string ")"

let pp_vs_type(q,input,output) =
  print_string "(";
  pp_quasi q;
  pp_comma();
  let tmp = !Objects.schedtype in
  Objects.schedtype := Objects.VIRTSCHED;
  pp_list
    (function (i,f) ->
      print_string "("; pp_list pp_entry i; pp_comma();
      pp_list
	(function cls -> print_string "CS."; print_string (CS.class2c(cls)))
	f;
      print_string ")")
    input;
  pp_comma();
  pp_list (pp_list pp_entry) output;
  Objects.schedtype := tmp;
  print_string ")"

let pp_mk_event (Ast.EVENT_NAME(nmlst,star,_)) =
  print_string "Ast.mkEVENT_NAME(";
  pp_list pp_id nmlst;
  if star then print_string ",true,-1)" else print_string ",false,-1)"

let pp_events str events pp_type =
  print_string str; print_break 0 2;
  pp_list
    (function (name,types) ->
      print_string "("; pp_mk_event name; print_string ",";
      force_newline();
      print_string "  ";
      open_box 0;
      pp_list pp_type types;
      close_box();
      print_string ")")
    events

let pp_interfaces str interfaces =
  print_string str;
  force_newline();
  print_string "  ";
  open_box 0;
  pp_list
    (function (nm,types) ->
      print_string "(B.mkId(";
      pp_id nm;
      print_string "),";
      force_newline();
      print_string "  ";
      open_box 0;
      pp_list pp_type types;
      close_box();
      print_string ")")
    interfaces;
  close_box()

let pp_automaton str automaton =
  print_string "let app_events_"; print_string str;
  print_string "automaton _ ="; print_break 0 2;
  pp_list
    (function
	Nfa2dfa.EDGE(src,(nm,int,reqd),tgt) ->
	  print_string "(";
	  print_int src; print_string ",";
	  pp_mk_event nm;
	  print_string ",";
	  (match int with
	    Quasi.INT -> print_string "B.INTIBLE,"
	  | Quasi.UNINT -> print_string "B.UNINTIBLE,");
	  print_int tgt;
	  print_string ")"
      |	_ -> ())
    automaton

(* ------------------------- initial events ------------------------- *)

let pp_event_list str events =
  print_string "let "; print_string str; print_string " _ = "; print_break 0 2;
  pp_list pp_mk_event events;
  force_newline(); force_newline()

(* --------------------- event declaration point -------------------- *)

let pp_decl_event_list str events =
  print_string "let "; print_string str; print_string " _ = "; print_break 0 2;
  pp_list (pp_list pp_mk_event) events;
  force_newline(); force_newline()

(* -------------------------- entry point --------------------------- *)

let pretty_print out
    initial_events system_events application_events
    top_level_events scheduler_events
    events interfaces vsinterfaces init_automaton automaton vs_events =
  (set_formatter_out_channel (open_out out);
   set_margin 80;
   open_box 0;
   pp_type_decl();
   pp_event_list "initial_event_specifications" initial_events;
   pp_event_list "system_event_specifications" system_events;
   pp_event_list "application_event_specifications" application_events;
   pp_decl_event_list "top_level_events" top_level_events;
   pp_decl_event_list "scheduler_events" scheduler_events;
   pp_automaton "init_" init_automaton;
   force_newline();
   force_newline();
   pp_automaton "" automaton;
   force_newline();
   force_newline();
   pp_events "let event_specifications _ =" events pp_type;
   force_newline();
   force_newline();
   pp_events "let vs_event_specifications _ =" vs_events pp_vs_type;
   force_newline();
   force_newline();
   pp_interfaces "let interface_specifications _ =" interfaces;
   force_newline();
   force_newline();
   let tmp = !Objects.schedtype in
   Objects.schedtype := Objects.VIRTSCHED;
   pp_interfaces "let vs_interface_specifications _ =" vsinterfaces;
   Objects.schedtype := tmp;
   close_box();
   print_newline())
