%{
(* $Id$ *)

module B = Objects
module VB = Vobjects
module CS = Class_state

%}

/* Grammar of language-level event types:

classes : classes = { class_name (, class_name)* }

initial_events : initial_events = { event_name (, event_name)* }

system_events : system_events = { event_name (, event_name)* }

application_events : application_events = { event_name (, event_name)* }

transition : event_name (-u-> event_name)*

top_level_events : top_level_events = { [transition (, transition)*] }

scheduler_events : scheduler_events = { [transition (, transition)*] }

event : event_name : config -> config

config : one_config | { one_config (, one_config)* }

one_config : [ ] | [ constraint_(, constraint)* ]

constraint_: [] = class_name | [ object_ (, object)* ] in class_name

object_ : src | tgt | p | q

iconstraint_: iobject_ :: iconstraint_ | [] = class_name |
             [ iobject_ (, iobject)* ] in class_name

iobject_ : p | q | arg

p's cannot be repeated in the input, q's cannot be repeated in the output
*/

%token LBRACE RBRACE LBRACK RBRACK LPAREN RPAREN
%token COMMA COLONCOLON EQUAL IN SRC TGT P Q X ARG
%token COLON RIGHTARROW UNINTARROW DOT TIMES INTERFACE HYPHEN BANG
%token INITEV SYSEV APPEV TOPLEV SCHEDLEV TYPES QUASITYPES VSINTERFACE
%token AUTOMATON IAUTOMATON SEMIC FINAL
%token EOF
%token <int> INTEGER
%token <string> IDENT

%start event_spec
%type <(Ast.event_name list * Ast.event_name list * Ast.event_name list * Ast.event_name list list * Ast.event_name list list * Compile_events.pre_entry list * (Quasi.intinfo * Ast.event_name) Nfa2dfa.fa_state list * (Quasi.intinfo * Ast.event_name) Nfa2dfa.fa_state list * Compile_events.pre_entry list * Compile_events.pre_entry list)> event_spec

/* start syntax definition */
%%

event_spec :
  initev sysev appev toplev schedlev types init_automaton automaton
  quasitypes vsinterface EOF
  {($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)}

initev :
  INITEV EQUAL LBRACE event_names RBRACE { $4 }

sysev :
  SYSEV EQUAL LBRACE event_names RBRACE { $4 }

appev :
  APPEV EQUAL LBRACE event_names RBRACE { $4 }

event_names :
                      { [] }
  | multi_event_names { $1 }

multi_event_names :
  event_type                           { [$1] }
  | event_type COMMA multi_event_names { $1::$3 }

toplev :
  TOPLEV EQUAL LBRACE transitions RBRACE { $4 }

schedlev :
  SCHEDLEV EQUAL LBRACE transitions RBRACE { $4 }

transitions :
                      { [] }
  | multi_transitions { $1 }

multi_transitions :
  transition                           { [$1] }
  | transition COMMA multi_transitions { $1::$3 }

transition :
  event_type                         { [$1] }
  | event_type UNINTARROW transition { $1::$3 }

types : TYPES LBRACE events RBRACE             { $3 }

automaton : AUTOMATON LBRACE aut RBRACE        { $3 }
init_automaton : IAUTOMATON LBRACE aut RBRACE  { $3 }

quasitypes : QUASITYPES LBRACE eevents RBRACE   { $3 }

vsinterface : VSINTERFACE LBRACE ieevents RBRACE   { $3 }

eevents :
  events                           { $1 }
  |                                { [] }

events :
  event                            { [$1] }
  | event events                   { $1 :: $2 }

event :
  event_type COLON config RIGHTARROW config
      { Compile_events.PREENTRY($1,$3,$5) }
  | INTERFACE IDENT COLON iconfig RIGHTARROW iconfig
      { Compile_events.INTENTRY(B.mkId($2),$4,$6) }
  | INTERFACE COLON giconfig RIGHTARROW giconfig
      { Compile_events.GENINTENTRY(Ast.mkEVENT_NAME(["generic_interface"],
						    false,-1),$3,$5) }
  ;



ieevents :
  ievents                           { $1 }
  |                                { [] }

ievents :
  ievent                            { [$1] }
  | ievent ievents                   { $1 :: $2 }

ievent :
  INTERFACE IDENT COLON iconfig RIGHTARROW iconfig
      { Compile_events.INTENTRY(B.mkId($2),$4,$6) }
  ;


event_type :
  IDENT                            { Ast.mkEVENT_NAME([$1],false,-1) }
  | IDENT hevt_list                { let (l,s) = $2 in
                                     Ast.mkEVENT_NAME($1::l,s,-1) }
  ;

aut :
  FINAL INTEGER SEMIC aut       { Nfa2dfa.FINAL($2)::$4 }
  | INTEGER aut_entries SEMIC aut
                                { (match $2 with
                                    (Nfa2dfa.EDGE(_,nm,tgt)::rest) ->
				      (Nfa2dfa.EDGE($1,nm,tgt)::rest)@$4
                                  | _ -> raise (Error.Error "internal error"))}
  |                             { [] }
  ;

aut_entries :
  rightarrow event_type RIGHTARROW INTEGER aut_entries
                           { let cur = Nfa2dfa.EDGE(0,($1,$2),$4) in
                             (match $5 with
			       [] -> [cur]
                             | (Nfa2dfa.EDGE(_,nm,tgt)::rest) ->
				 cur::(Nfa2dfa.EDGE($4,nm,tgt)::rest)
                             | _ -> raise (Error.Error "internal error")) }
  |                        { [] }

rightarrow :
  RIGHTARROW                    { Quasi.INT }
  | UNINTARROW                  { Quasi.UNINT }

hevt_list :
  DOT IDENT			{ ([$2],false) }
  | DOT TIMES			{ ([],true) }
  | DOT IDENT hevt_list	        { let (l,s) = $3 in ($2::l,s) }
  ;

/* types for event handlers */

config :
  one_config                         { [Compile_events.PRECONF($1)] }
  | LBRACE configs RBRACE            { $2 }

configs :
  one_config                         { [Compile_events.PRECONF($1)] }
  | one_config COMMA configs         { Compile_events.PRECONF($1) :: $3 }

one_config :
    LBRACK RBRACK                    { [] }
  | LBRACK constraints RBRACK        { $2 }

constraints :
    constraint_ modd { let (x,y) = $1 in [(x,y,$2)] }
  | constraint_ modd COMMA constraints
      { let (x,y) = $1 in (x,y,$2) :: $4 }

constraint_ :
  LBRACK RBRACK EQUAL IDENT          { ($4,VB.EMPTY) }
  | object_ IN IDENT                 { ($3,VB.MEMBER([$1])) }
  | LBRACK objects RBRACK IN IDENT   { ($5,VB.MEMBER($2)) }
  | IDENT                            { ($1,VB.UNKNOWN) }

objects :
    object_                          { [$1] }
  | object_ COMMA objects            { $1 :: $3 }

object_ :
    SRC                              { VB.VP(VB.SRC,[]) }
  | TGT                              { VB.VP(VB.TGT,[]) }
  | P                                { VB.VP(VB.P(0),[]) }
  | P HYPHEN INTEGER                 { VB.VP(VB.P($3),[]) }
  | Q                                { VB.VP(VB.Q(0),[]) }
  | Q HYPHEN INTEGER                 { VB.VP(VB.Q($3),[]) }
  | X                                { VB.VP(VB.UNKNOWNPROC,[]) }

modd :
  BANG                               { VB.MODIFIED }
  | COLON                            { VB.UNMODIFIED }
  |                                  { VB.BOTMOD }

/* types for interface functions */

iconfig :
  ione_config                        { [Compile_events.PRECONF($1)] }
  | LBRACE iconfigs RBRACE           { $2 }

iconfigs :
  ione_config                        { [Compile_events.PRECONF($1)] }
  | ione_config COMMA iconfigs       { Compile_events.PRECONF($1) :: $3 }

ione_config :
    LBRACK RBRACK                    { [] }
  | LBRACK iconstraints RBRACK       { $2 }

iconstraints :
    iconstraint_ modd { let (x,y) = $1 in [(x,y,$2)] }
  | iconstraint_ modd COMMA iconstraints
      { let (x,y) = $1 in (x,y,$2) :: $4 }

iconstraint_ :
  LBRACK RBRACK EQUAL IDENT          { ($4,VB.EMPTY) }
  | iobject_ IN IDENT                { ($3,VB.MEMBER([$1])) }
  | LBRACK iobjects RBRACK IN IDENT  { ($5,VB.MEMBER($2)) }
  | IDENT                            { ($1,VB.UNKNOWN) }

iobjects :
    iobject_                         { [$1] }
  | iobject_ COMMA iobjects          { $1 :: $3 }

iobject_ :
  ARG HYPHEN INTEGER                 { VB.VP(VB.ARG($3),[]) }
  | P                                { VB.VP(VB.P(0),[]) }
  | P HYPHEN INTEGER                 { VB.VP(VB.P($3),[]) }
  | Q                                { VB.VP(VB.Q(0),[]) }
  | Q HYPHEN INTEGER                 { VB.VP(VB.Q($3),[]) }

/* generic type for interface functions */

giconfig :
  gione_config                       { [Compile_events.PRECONF($1)] }
  | LBRACE giconfigs RBRACE          { $2 }

giconfigs :
  gione_config                       { [Compile_events.PRECONF($1)] }
  | gione_config COMMA giconfigs { Compile_events.PRECONF($1) :: $3 }

gione_config :
    LBRACK RBRACK                    { [] }
  | LBRACK giconstraints RBRACK      { $2 }

giconstraints :
    giconstraint_ modd { let (x,y) = $1 in [(x,y,$2)] }
  | giconstraint_ modd COMMA giconstraints
      { let (x,y) = $1 in (x,y,$2) :: $4 }

giconstraint_ :
  LBRACK RBRACK EQUAL IDENT          { ($4,VB.EMPTY) }
  | giobject_ IN IDENT               { ($3,VB.MEMBER([$1])) }
  | LBRACK giobjects RBRACK IN IDENT { ($5,VB.MEMBER($2)) }
  | IDENT                            { ($1,VB.UNKNOWN) }

giobjects :
    giobject_                        { [$1] }
  | giobject_ COMMA giobjects        { $1 :: $3 }

giobject_ :
  TGT                                { VB.VP(VB.TGT,[]) }
  | P                                { VB.VP(VB.P(0),[]) }
