type pre_entry =
    PREENTRY of Ast.event_name * pre_config list * pre_config list
  | INTENTRY of Objects.identifier * pre_config list * pre_config list
  | GENINTENTRY of Ast.event_name * pre_config list * pre_config list
and pre_config =
    PRECONF of (string (* class *) * Vobjects.vtype * Vobjects.modified) list

type entry = Class_state.classname * Vobjects.vtype * Vobjects.modified

type type_rule = Objects.isquasi * entry list list * entry list list
type vs_type_rule =
    Objects.isquasi * (entry list * Class_state.classname list) list *
      entry list list

val compile_config :
      (Ast.event_name list *
       Ast.event_name list *
       Ast.event_name list *
       Ast.event_name list list *
       Ast.event_name list list *
       pre_entry list *
       (Quasi.intinfo * Ast.event_name) Nfa2dfa.fa_state list *
       (Quasi.intinfo * Ast.event_name) Nfa2dfa.fa_state list *
       pre_entry list *
       pre_entry list) ->
	   (Ast.event_name list *
	    Ast.event_name list *
	    Ast.event_name list *
	    Ast.event_name list list *
	    Ast.event_name list list *
	    ((Ast.event_name * (entry list) Model.sequence) list *
	       (Ast.event_name * type_rule list) list *
	       (Ast.event_name * type_rule list) list *
	       (Ast.event_name * Quasi.intinfo * Quasi.required)
	       Nfa2dfa.fa_state list *
	       (Ast.event_name * Quasi.intinfo * Quasi.required)
	       Nfa2dfa.fa_state list)
	    * ((Objects.identifier * type_rule list) list *
		 (Objects.identifier * type_rule list) list)
	    * ((Objects.identifier * type_rule list) list))

val collect_vs_types :
    (Ast.event_name * (entry list * Class_state.classname list) * entry list)
    list -> (Ast.event_name * vs_type_rule list) list
