type entry = Class_state.classname * Vobjects.vtype * Vobjects.modified

type type_rule = Objects.isquasi * entry list list * entry list list
type vs_type_rule =
    Objects.isquasi * (entry list * Class_state.classname list) list *
      entry list list

val pretty_print :
    string ->
      (Ast.event_name * type_rule list) list ->
	(Objects.identifier * type_rule list) list ->
	  (Objects.identifier * type_rule list) list ->
	    (Ast.event_name * (entry list) Model.sequence) list ->
	      (Ast.event_name * Quasi.intinfo * Quasi.required)
		Nfa2dfa.fa_state list ->
		  (Ast.event_name * Quasi.intinfo * Quasi.required)
		    Nfa2dfa.fa_state list ->
		      (Ast.event_name * vs_type_rule list) list
		      -> unit
