(* $Id$ *)

module B = Objects
module CS = Class_state
module IPA = Ipanema

open Format
open Ioctl

let policy_name = ref ""
let external_functions = ref ([] : B.identifier list)

let init_external_functions _ =
  external_functions :=
    List.map B.mkId
      ["create_ipanema_timer";"create_static_ipanema_timer";
	"create_persistent_ipanema_timer";
	"delete_ipanema_timer";"delete_static_ipanema_timer";
	"start_absolute_timer";"start_relative_timer";"stop_timer"]

(* ----------------------- Environment lookup ----------------------- *)

exception LookupErrException

let rec lookup_id x = function
    [] -> raise LookupErrException
  | ((y,v)::r) -> if B.ideq(x,y) then v else lookup_id x r

(* ----------------------- Instance variables ----------------------- *)
(* For HLS *)
(* should be different from all local variables *)

let instance_variables = ref ([] : B.identifier list)

(* -------------------- Miscellaneous operations -------------------- *)

let print_nl_string (str: string) : unit = print_string str; print_cut()

let print_debug _ =
  if !IPA.debug then print_nl_string "pr_info(\"%s: %d\", __func__, __line__);"

let rec print_between between_fn elem_fn = function
    [] -> ()
  | [x] -> elem_fn x
  | x :: xs -> (elem_fn x; between_fn(); print_between between_fn elem_fn xs)

let is_struct ar id =
  try
    (match lookup_id id (!CS.state_env) with
      CS.STATE(state_id,_,CS.QUEUE(CS.SELECT(_)),_) ->
	ar || ((!CS.selected_size) = 1)
    | CS.STATE(state_id,_,CS.QUEUE(_),_) -> true
    | _ -> false)
  with LookupErrException -> false

let is_state id =
  try let _ = lookup_id id (!CS.state_env) in true
  with LookupErrException -> false

let is_cstate id =
  try let _ = lookup_id id (!CS.cstate_env) in true
  with LookupErrException -> false

let pp_id id =
  let nm = B.id2c id in
  (* if ((is_struct false id) && not (!B.csharp)) then print_string "&"; *)
(*  if is_state id
  then
    if (!B.hls)
    then print_string "scheduler_instance->"
    else print_string "state_info."
    else *)if is_cstate id then
    print_string "cstate_info.";
  if (nm="NULL") && (!B.csharp)
  then print_string "null"
  else
    (if List.mem id (!instance_variables)
    then print_string "scheduler_instance->";
     print_string nm)

let pp_id_cs id =
  if ((is_struct false id) && not (!B.csharp)) then
    if is_state id then
      if (!B.hls)
      then ("&" ^"scheduler_instance->" ^ B.id2c(id))
      else ("&" ^"state_info." ^ B.id2c(id))
    else if is_cstate id then
      ("&" ^"cstate_info." ^ B.id2c(id))
    else ("&" ^B.id2c(id))
  else
    if is_state id then
      if (!B.hls)
      then ("scheduler_instance->" ^ B.id2c(id))
      else ("state_info." ^ B.id2c(id))
    else B.id2c(id)

let pp_ar_id id =
  if (is_struct true id) then print_string "&";
  if is_state id then    (if (!B.hls)
    then print_string "scheduler_instance->"
    else print_string "state_info.");
  print_string(B.id2c(id))

(* & not wanted, eg for structure fields *)
let pp_struct_id id = print_string(B.id2c(id))
(* & not wanted, eg for field reference *)
let pp_struct_id2 id =
  if is_state id
  then
    (if (!B.hls)
    then print_string "scheduler_instance->"
     else print_string "state_info.");
  pp_id id

let pp_comma _ = (print_string ","; print_space())
let pp_comma_nl _ = (print_string ","; print_cut ())
let pp_semic _ = (print_string ";"; print_cut ())

let nl_ifnonempty = function
    x::xs -> force_newline()
  | _ -> ()

let pp_arrayref_int id index =
  pp_ar_id id; print_string "["; print_int index; print_string "]"

let pp_arrayref_int2 index =
  print_string "["; print_int index; print_string "]"

let pp_arrayref_id id index =
  pp_ar_id id; print_string "["; pp_id index; print_string "]"

let pp_arrayref index =
  print_string "["; pp_id index; print_string "]"

let _start_block newline =
  if newline then print_cut ();
  print_string "{"; print_cut() ;
  open_vbox 8; print_string "\t"

let start_top _ =
  _start_block true

let start_block _ =
  _start_block false

let end_block cut =
  close_box(); print_cut (); print_string "}";
  if cut then print_cut ()

let start_case _ =
  open_vbox 2; print_string "  "

let end_case _ =
  close_box()

let de_structify str =
  if !B.csharp
  then
    let len = String.length str in
    if len > 7 && String.sub str 0 7 = "struct "
    then String.sub str 7 (len-7)
    else str
  else str

let pp_arglist long l =
  print_string "("; open_box 0;
  print_between (if long then pp_comma_nl else pp_comma)
    (function str -> print_string (de_structify str)) l;
  close_box(); print_string ")"

let pp_for_header id ptr maxfn =
  print_string "for ("; open_box 0; pp_id(id);
  print_string " = "; maxfn(); print_string ".next; ";
  pp_id(id); print_string " != &"; maxfn(); print_string ";"; print_space();
  pp_id(id); print_string " = "; pp_id(ptr);
  print_string ") "; close_box()

let pp_for_header_deref id ptr maxfn =
  print_string "for ("; open_box 0; pp_id(id);
  print_string " = "; maxfn(); print_string "->next; ";
  pp_id(id); print_string " != "; maxfn(); print_string ";"; print_space();
  pp_id(id); print_string " = "; pp_id(ptr);
  print_string ") "; close_box()

(* convert one name into another *)
let rec pp_state_cst state_exp =
  (* FIXME *)
  match state_exp with
    Ast.VAR(id, _, _) ->
    print_string (Printf.sprintf "%s_STATE" (String.uppercase (B.id2c id)))
  | Ast.FIELD(_,id, _, _) ->
    print_string (Printf.sprintf "%s_STATE" (String.uppercase (B.id2c id)))
  | Ast.PRIM(Ast.VAR(B.ID("per_cpu", _), _, _), [exp; core], attr)
      when B.ty attr = B.QUEUE B.PROCESS || B.ty attr = B.PROCESS ->
     pp_state_cst exp
  | _ -> failwith "State expected"

let check_simulate simfn modfn =
  print_string "\n#ifdef __KERNEL__"; force_newline();
  modfn();
  print_string "\n#else"; force_newline();
  simfn();
  print_string "\n#endif"; force_newline()

let process_state pre_array process_array_element process_queue process_var
    process_prev_var = function
    Ast.QUEUE(clsname,shared,CS.EMPTY,id,vis,po,attr) -> () (* shouldn't be needed *)
  | Ast.QUEUE(clsname,shared,CS.SELECT(_),id,vis,po,attr) ->
      let sz = !CS.selected_size in
      if sz > 1
      then
	(start_block();
	 let new_id = B.fresh_idx "i" in
	 print_string "int "; pp_id new_id; pp_semic();
	 pre_array id;
	 print_string "for ("; pp_id new_id; print_string "=0; ";
	 pp_id new_id; print_string " != "; print_int sz;
	 print_string "; "; pp_id new_id; print_string "++) ";
	 start_block();
	 process_array_element id new_id;
	 end_block false;
	 end_block true)
      else process_queue id
  | Ast.QUEUE(clsname,shared,queue_typ,id,vis,po, attr) ->
      process_queue id
  | Ast.PROCESS(clsname,shared,id,previd,vis,attr) ->
      process_var id;
      match previd with
	Ast.NO_PREV -> ()
      | Ast.BOSSA_PREV id -> force_newline(); process_prev_var id
      | Ast.RTS_PREV id -> ()

(* --------------------- Information about types -------------------- *)
let pp_nr_topo_cs _ = (!policy_name) ^"_ipa_nr_topology_levels"
let pp_topo_cs _ = (!policy_name) ^"_ipa_topology"

let pp_process_cs _ = (!policy_name) ^"_ipa_process"
let pp_process_struct_cs _ = "struct "^ pp_process_cs ()
let pp_process_struct _ = print_string (pp_process_struct_cs ())

let pp_core_struct_cs _ = "struct "^ (!policy_name) ^"_ipa_core"
let pp_core_struct _ = print_string (pp_core_struct_cs ())

let pp_domain_struct_cs _ = "struct "^ (!policy_name) ^"_ipa_sched_domain"
let pp_domain_struct _ = print_string (pp_domain_struct_cs ())

let pp_group_struct_cs _ = "struct "^ (!policy_name) ^"_ipa_sched_group"
let pp_group_struct _ = print_string (pp_group_struct_cs ())

let pp_scheduler_struct_cs _ = ("struct " ^ (!policy_name) ^ "_sched_inst_struct")
let pp_scheduler_struct _ = print_string (pp_scheduler_struct_cs ())

let rec pp_rawtype_cs = function
    B.PROCESS -> pp_process_struct_cs()
  | B.SRCPROCESS -> pp_process_struct_cs()
  | B.SCHEDULER_INST -> pp_scheduler_struct_cs()
  | B.RANGE(id) -> pp_id_cs id
  | B.QUEUE(B.PROCESS) ->  "struct ipanema_rq"
  | B.SET(B.CORE) -> "cpumask_t"
  | B.SET(B.DOMAIN) ->  pp_domain_struct_cs()
  | B.SET(B.GROUP) ->  pp_group_struct_cs()
  | B.CORE -> pp_core_struct_cs()
  | B.DOMAIN ->  pp_domain_struct_cs()
  | B.GROUP ->  pp_group_struct_cs()
  | B.INDR(ty) -> pp_rawtype_cs ty
  | ty -> B.type2ctype ty

let rec pp_type_cs = function
    B.PROCESS -> pp_process_struct_cs() ^ " *"
  | B.SRCPROCESS -> pp_process_struct_cs() ^ " *"
  | B.SCHEDULER_INST -> pp_scheduler_struct_cs() ^ " *"
  | B.RANGE(id) -> pp_id_cs id
  | B.QUEUE(B.PROCESS) ->  "struct ipanema_rq *"
  | B.SET(B.CORE) -> "cpumask_t"
  | B.SET(B.DOMAIN) ->  pp_domain_struct_cs() ^ " *"
  | B.SET(B.GROUP) ->  pp_group_struct_cs() ^ " *"
  | B.CORE -> pp_core_struct_cs() ^ " *"
  | B.DOMAIN ->  pp_domain_struct_cs() ^ " *"
  | B.GROUP ->  pp_group_struct_cs() ^ " *"
  | B.INDR(ty) -> pp_type_cs ty ^ " *"
  | ty -> B.type2ctype ty

let pp_type = function
    ty -> print_string (pp_type_cs ty)

let rec pp_etype = function (* for external functions - policy type not known *)
    B.PROCESS        -> print_string (pp_process_struct_cs() ^ " *")
  | B.SRCPROCESS     -> print_string (pp_process_struct_cs() ^ " *")
  | B.SCHEDULER_INST -> print_string "SCHEDULER_INSTANCE_STRUCT *"
  | B.RANGE(_)       -> print_string "int"
  | B.CONST ty       -> print_string "const "; pp_etype ty
  | ty               -> pp_type ty

let pp_process_of task =
  "(("^pp_process_struct_cs()^" *)policy_metadata("^task^"))"

(* --------------------- C/Csharp compatibility --------------------- *)

let mk_public str = print_string (de_structify str)

let mk_inline _ = print_string "inline "

let mk_static _ = print_string "static "

let mk_static_private _ = print_string "static "

let mk_empty_prototype _ = print_string "(void)"

let fix_slashes s =
  let count = ref 0 in
  String.iter (function c -> if c = '\\' then count := !count + 1) s;
  let res = String.create ((String.length s) + (!count)) in
  count := 0;
  String.iter
    (function c ->
      if c = '\\' then (String.set res (!count) '\\'; count := !count + 1);
      String.set res (!count) c;
      count := !count + 1)
    s;
  res

let mk_printk_string str = str

let rec div_string l s =
  if ((String.length s) > l) then
    let first = String.sub s 0 l in
    let last_char = String.get first (l-1) in
    if last_char = '\\'
    then
      (* surely there is a better way to do the following *)
      let tmp = Printf.sprintf "%c" (String.get s l) in
      (first ^ tmp) ::
      (div_string l (String.sub s (l+1) ((String.length s)-(l+1))))
    else
      first ::
      (div_string l (String.sub s l ((String.length s)-l)))
  else [s]

let mk_printk str = print_nl_string ("printk(\""^str^"\");")

let mk_attr p = "attr("^p^")"
