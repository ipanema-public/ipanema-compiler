module B = Objects
module CS = Class_state
module G = Genaux
module A = Ast2c
module IPA = Ipanema

open Format
open Ioctl
(* ----------------------- Creation of scheduling domain ------------------------ *)

let init_topo _ =
  let ipa_nr_topo = G.pp_nr_topo_cs ()
  and ipa_topo = G.pp_topo_cs () in
  G.print_nl_string ("static int init_topology(void)
{
	struct topology_level *t = per_cpu(topology_levels, 0);
	size_t size;
	int i;\n");
  if !IPA.debug then G.print_nl_string "pr_info(\"%s: entry\", __func__);";
  G.print_nl_string ("
	"^ipa_nr_topo^" = 0;
	while (t) {
		 "^ipa_nr_topo^"++;
		t = t->next;
	}

	size = "^ipa_nr_topo^" * sizeof(struct list_head);
	"^ipa_topo^" = kzalloc(size, GFP_KERNEL);
	if(!"^ipa_topo^") {
		"^ipa_nr_topo^" = 0;
		return -ENOMEM;
	}

	for (i = 0 ; i < "^ipa_nr_topo^" ; i++) {
		INIT_LIST_HEAD("^ipa_topo^" + i);
	}\n");
  if !IPA.debug then G.print_nl_string "pr_info(\"%s: exit\", __func__);";
  G.print_nl_string ("
	return 0;
}");
  print_newline ()

let destroy_topo _ =
  let ipa_nr_topo = G.pp_nr_topo_cs ()
  and ipa_topo = G.pp_topo_cs ()
  and ipanema_sched_domain = G.pp_domain_struct_cs () in
  G.print_nl_string ("static void destroy_scheduling_domains(void)
{
	"^ipanema_sched_domain^" *sd, *tmp;
	int i;

	for (i = 0 ; i < "^ipa_nr_topo^" ; i++) {
		list_for_each_entry_safe(sd, tmp, "^ipa_topo^" + i, siblings) {
			list_del(&sd->siblings);
			kfree(sd->groups);
			kfree(sd);
		}
	}

	kfree("^ipa_topo^");
}");
  print_newline ()

let create_sched_dom (sd, groups) domvars =
  let ipanema_core = G.pp_core_struct_cs ()
  and ipanema_sched_domain = G.pp_domain_struct_cs ()
  and ipa_topo = G.pp_topo_cs () in
  G.print_nl_string ("static int create_scheduling_domains(unsigned int cpu)
{
	struct topology_level *t = per_cpu(topology_levels, cpu);
	"^ipanema_core^" *c = &ipanema_core(cpu);
	size_t sd_size = sizeof("^ipanema_sched_domain^");
	unsigned int level = 0;
	"^ipanema_sched_domain^" *sd, *lower_sd = NULL;
	bool seen;

	c->sd = NULL;

	/* For each topological level exported by the runtime for cpu */
	while (t) {
		/* If cpu is present in the current level */
		seen = false;
		list_for_each_entry(sd, "^ipa_topo^" + level, siblings) {
			if (cpumask_test_cpu(cpu, &sd->cores)) {
				seen = true;
				break;
			}
		}
		if (!seen) {
			sd = kmalloc(sd_size, GFP_KERNEL);
			if (!sd)
				goto mem_fail;
			INIT_LIST_HEAD(&sd->siblings);
			sd->parent = NULL;
			sd->___sched_group_idx = 0;
			sd->groups = NULL;
			cpumask_copy(&sd->cores, &t->cores);

			sd->flags = t->flags;");
  G.print_nl_string "/* -- Start of user variables initialisation */";
  G.print_between print_cut
    (function x ->
	       match x with
	         Ast.VARDECL(ty,id,_,false, _, Some dexp,attr) ->
	          print_string ("sd->"^(B.id2c id));
	          print_string " = ";
	          A.pp_exp dexp;
                  G.print_nl_string ";"
	       | _ -> ()
    ) domvars;
  G.print_nl_string "/* -- End of user variables initialisation */";
  G.print_nl_string
    ("			spin_lock_init(&sd->lock);
			list_add_tail(&sd->siblings, "^ipa_topo^" + level);
		}
		if (lower_sd) {
			lower_sd->parent = sd;
		}
		else {
			c->sd = sd;
		}
		if (seen)
			break;

		lower_sd = sd;
		t  = t->next;
		level++;
	}

	return 0;

mem_fail:
	destroy_scheduling_domains();
	return -ENOMEM;
}");
  print_newline ()
    
let build_groups (sd, groups) : unit =
  let ipanema_sched_group = G.pp_group_struct_cs ()
  and ipanema_sched_domain = G.pp_domain_struct_cs ()
  and ipa_topo = G.pp_topo_cs () in
  G.print_nl_string ("static int build_groups("^ipanema_sched_domain^" *sd, unsigned int lvl)
{
	"^ipanema_sched_domain^" *sdl;
	"^ipanema_sched_group^" *sg = NULL;
	int n = 0;

	list_for_each_entry(sdl, &"^ipa_topo^"[lvl - 1], siblings) {
		if (cpumask_subset(&sdl->cores, &sd->cores)) {
			n++;
			sg = krealloc(sg,
				n * sizeof("^ipanema_sched_group^"),
				GFP_KERNEL);
			if (!sg)
				goto mem_fail;
			cpumask_copy(&sg[n - 1].cores, &sdl->cores);
			sg[n - 1].id = n - 1;
		}
	}

	sd->___sched_group_idx = n;
	sd->groups = sg;

	return 0;

mem_fail:
	destroy_scheduling_domains();
	return -ENOMEM;
}");
  print_newline ()

let build_lower_groups (sd, groups) : unit =
  let ipanema_core = G.pp_core_struct_cs ()
  and ipanema_sched_group = G.pp_group_struct_cs ()
  and ipanema_sched_domain = G.pp_domain_struct_cs () in
  G.print_nl_string ("static int build_lower_groups("^ipanema_sched_domain^" *sd)
{
	int cpu, n, i = 0;


	n = cpumask_weight(&sd->cores);
	sd->groups = kzalloc(n * sizeof("^ipanema_sched_group^"),
				GFP_KERNEL);
	if (!sd->groups)
		goto mem_fail;
	sd->___sched_group_idx = n;


	for_each_cpu(cpu, &sd->cores) {
		cpumask_clear(&sd->groups[i].cores);
		cpumask_set_cpu(cpu, &sd->groups[i].cores);
		i++;
	}

	return 0;

mem_fail:
	destroy_scheduling_domains();
	return -ENOMEM;
}");
  print_newline ()

let create_sched_grps (sd, groups) : unit =
  let ipa_nr_topo = G.pp_nr_topo_cs ()
  and ipa_topo = G.pp_topo_cs ()
  and ipanema_sched_domain = G.pp_domain_struct_cs () in
  G.print_nl_string ("/* Scheduling domains must be up to date for all CPUs */
static int create_scheduling_groups(void)
{
	"^ipanema_sched_domain^" *sd;
	int i;

	for (i = "^ipa_nr_topo^" - 1 ; i > 0 ; i--) {
		list_for_each_entry(sd, &"^ipa_topo^"[i], siblings) {
			if (build_groups(sd, i))
				goto fail;
		}
	}

	list_for_each_entry(sd, "^ipa_topo^", siblings) {
		if (build_lower_groups(sd))
			goto fail;
	}

	return 0;

fail:
	destroy_scheduling_domains();
	return -ENOMEM;
}");
  print_newline ()

(*
if !IPA.debug then G.print_nl_string ""
*)
let build_hierarchy _ : unit =
  let ipa_nr_topo = G.pp_nr_topo_cs () in
  G.print_nl_string ("static void build_hierarchy(void)
{
	int cpu;

        pr_info(\"%s: entry\", __func__);
	init_topology();
        pr_info(\"%s: init topo OK\", __func__);

        /* if unicore, don't build hierarchy */
        if (!"^ipa_nr_topo^")
                return;

	/* Create hierarchy for all cpus */
	for_each_possible_cpu(cpu) {
                pr_info(\"%s: domains for %d\", __func__, cpu);
		create_scheduling_domains(cpu);
	}
        pr_info(\"%s: domains OK\", __func__);
	create_scheduling_groups();
        pr_info(\"%s: exit\", __func__);
}");
  print_newline ()

let pp_global_dom _ =
  G.print_nl_string ("static struct list_head *"^G.pp_topo_cs ()^";");
  G.print_nl_string ("static unsigned int "^G.pp_nr_topo_cs ()^";")

let pp_topology sdg dom =
  open_vbox 0;
  init_topo ();
  destroy_topo ();
  create_sched_dom sdg dom;
  build_groups sdg;
  build_lower_groups sdg;
  create_sched_grps sdg;
  build_hierarchy sdg;
  close_box ()
