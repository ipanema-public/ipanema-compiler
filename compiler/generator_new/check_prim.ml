module A = Ast
module B = Objects
module T = Type
module CS = Class_state

(* ----------------------- Environment lookup ----------------------- *)

exception LookupErrException

let rec lookup_id x = function
    [] -> raise LookupErrException
  | ((y,v)::r) -> if B.ideq(x,y) then v else lookup_id x r

let is_state id =
  try let _ = lookup_id id (!CS.state_env) in true
  with LookupErrException -> false

(* -------------------------- expression and stmt --------------------------- *)
let check_fct fct f =
  match f with
    Ast.VAR(id, _,_) -> fct = (B.id2c id)
  | _ -> false
    
let rec check_exp fct e =
  match e with
  | Ast.UNARY(uop,exp,attr) -> check_exp fct exp

  | Ast.BINARY(bop,exp1,exp2,attr) ->
      let exp1 = check_exp fct exp1
      and exp2 = check_exp fct exp2 in exp1 || exp2

  | Ast.INDR(exp,attr) -> check_exp fct exp

  | Ast.VALID(exp, s, a) -> check_exp fct exp

  | Ast.PRIM(fn,pl,a) ->
     check_fct fct fn
     || (List.fold_left (fun acc p -> acc || check_exp fct p) false pl)

  | Ast.IN(exp1, exp2, s, t, b, c, a) ->
     check_exp fct exp1 || check_exp fct exp2

  | Ast.FIRST(exp, crit, stopt, attr) -> check_exp fct exp

  | exp -> false

let check_decl fct d =
  match d with
    Ast.VALDEF(vd, exp, isc, attr) -> check_exp fct exp
  | decl -> false

let rec check_stmt fct s =
  match s with
    Ast.IF(exp,stmt1,stmt2,attr) ->
      check_exp fct exp || check_stmt fct stmt1 || check_stmt fct stmt2

    | Ast.FOR(id,None,x,dir,stmt,crit,attr) ->
	check_stmt fct stmt

    | Ast.FOR(id,Some state_id,x,dir,stmt,crit,attr) ->
       (List.fold_left
	  (fun acc s -> acc||check_exp fct s) false state_id)
       || check_stmt fct stmt

    | Ast.SWITCH(exp,cases,default,attr) ->
       check_exp fct exp
       || (List.fold_left
	     (fun acc case ->
	       let Ast.SEQ_CASE(idlist,x,stmt,y) = case in
	       acc || check_stmt fct stmt) false cases)
       || (match default with
	 None -> false
	 | Some stmt -> check_stmt fct stmt)

    | Ast.SEQ(decls,stmts,attr) ->
       List.fold_left (fun acc d -> acc || check_decl fct d)
	 (List.fold_left (fun acc s -> acc || check_stmt fct s) false stmts)
	 decls

    | Ast.RETURN(Some(exp),attr) -> check_exp fct exp

    | Ast.MOVE(exp,state,x,y,z,a,b,attr) ->
       check_exp fct exp || check_exp fct state

    | Ast.MOVEFWD(exp,x,state_end,attr) ->
       check_exp fct exp

    | Ast.ASSIGN(expl,expr,sorted_fld,attr) ->
       check_exp fct expl || check_exp fct expr

    | Ast.PRIMSTMT(f,args,attr) ->
       check_fct fct f || List.fold_left (fun acc a -> acc || check_exp fct a) false args

    | Ast.ASSERT(exp,attr) -> check_exp fct exp

    | Ast.STEAL _ -> if fct = "steal_for" then true else false

    | x -> false

(* -------------------------- top-level blocks --------------------------- *)
(*fct: function to check *)
let check_handler fct (A.EVENT(nm, v, stmt, _, attr)) =
  check_stmt fct stmt

let check_function fct (A.FUNDEF(tl, ty, nm, pl, stmt, inl, attr)) =
  check_stmt fct stmt

(* -------------------------- entry point --------------------------- *)

let check_for fct
      (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		     fundecls,valdefs,domains,pcstate,cstates,criteria,
		     trace,handlers,chandlers,ifunctions,functions,attr)) =
  let (new_handlers, new_chandlers, new_ifunctions, new_functions) =
    match pcstate with
      Ast.CORE _ ->
      (List.map (check_handler fct) handlers,
       List.map (check_handler fct) chandlers,
       List.map (check_function fct) ifunctions,
       List.map (check_function fct) functions)
    | Ast.PSTATE _ ->
       (List.map (check_handler  fct) handlers,
        List.map (check_handler  fct) chandlers,
        List.map (check_function fct) ifunctions,
        List.map (check_function fct) functions)
  in
  List.exists (fun v -> v) (new_handlers@new_chandlers@new_ifunctions@new_functions)
