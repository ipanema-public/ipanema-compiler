open Format

let device_name = ref ""
let bossa_p= ref ""

(*--------------------- generate bossa_ioctl.h ----------------------*)
let ioctl_h nm=  
"/*ioctl.h\n\n"^
" */\n\n"^
"#ifndef BOSSA_IOCTL_H\n"^
"#define BOSSA_IOCTL_H\n\n"^
"#include <linux/ioctl.h>\n\n"^
"#define MAJOR_NUM 0xAA //170\n"^
"#define IOCTL_SET_POLICY _IOW(MAJOR_NUM, 0, char*)\n"^
"#define IOCTL_GET_POLICY _IOR(MAJOR_NUM, 1, char*)\n"^
"#define DEVICE_NAME \"bossa_"^nm^"\"\n\n"^
"#endif\n"

(*-------------------- generate bossa_xxx.c as a module-------------*)

let device_proc nm=
"/*\n"^
"\n"^
"device/proc\n"^
"\n"^
"*/\n"^
"#define __NO_VERSIONS__\n"^
"\n"^
"/* Standard in kernel modules */\n"^
"#include <linux/kernel.h>   /* We're doing kernel work */\n"^
"#include <linux/module.h>   /* Specifically, a module */\n"^
"\n"^
"#include <linux/fs.h>\n"^
"#include <linux/wrapper.h> /* compatible*/\n"^
"#include <asm/uaccess.h>\n"^
"#include <linux/proc_fs.h>\n"^
"\n"^
"\n"^
"#include \"bossa_ioctl.h\"\n"^
"#include \"printk.h\"\n"^
"\n"^
"\n"^
"#define SUCCESS 0\n"^
"\n"^
"#define DBG(x,y) {char s[80]; \\\n"^
"                sprintf(s, x, y); \\\n"^
"                print_string(s); \\\n"^
"                }\n"^
"\n"^
"\n"^
"static int dev_open = 0;\n"^
"\n"^
"static int dev_bossa_"^nm^"_open(struct inode *inode,\n"^
"				struct file *fd)\n"^
"{\n"^
"\n"^
"#ifdef DEBUG\n"^
"    DBG(\"device_open (%p)\n\", fd);\n"^
"#endif\n"^
"\n"^
"    if (dev_open)\n"^
"      return (-EBUSY);\n"^
"\n"^
"    dev_open++;\n"^
"    MOD_INC_USE_COUNT;\n"^
"\n"^
"    return SUCCESS;\n"^
"}\n"^
"\n"^
"static int dev_bossa_"^nm^"_release(struct inode *inode,\n"^
"				   struct file *fd)\n"^
"{\n"^
"\n"^
"#ifdef DEBUG\n"^
"  DBG(\"device_release (%p)\n\", fd);\n"^
"#endif\n"^
"\n"^
"    dev_open--;\n"^
"    MOD_DEC_USE_COUNT;\n"^
"\n"^    
"    return 0;\n"^
"}\n"^
"\n"^
"struct bossa_policy_t {\n"^
(!bossa_p)^
"} bossa_kern;\n"^
"\n"^
"static ssize_t dev_bossa_"^nm^"_read(struct file *fd,\n"^
"				    char *p,\n"^
"				    size_t length, loff_t *a)\n"^
"{\n"^
"\n"^  
"  copy_to_user(p, &bossa_kern, sizeof(struct bossa_policy_t));\n"^
"  \n"^
"  return length;\n"^
"}\n"^
"\n"^
"static ssize_t dev_bossa_"^nm^"_write(struct file *fd,\n"^
"				     const char *p,\n"^
"				     size_t length, loff_t *a)\n"^
"{\n"^
"  copy_from_user(&bossa_kern, p, length);\n"^
"  return length;\n"^
"}\n"^
"\n"^
"/*ioctl*/\n"^
"int dev_bossa_"^nm^"_ioctl(struct inode *ionde,\n"^
"			  struct file *fd,\n"^
"			  unsigned int ioctl_num,\n"^
"			  unsigned long param)\n"^
"{\n"^
"\n"^
"  switch (ioctl_num) {\n"^
"  case IOCTL_SET_POLICY:\n"^
"    dev_bossa_"^nm^"_write(fd, (char*)param, sizeof(bossa_kern), 0);\n"^
"    break;\n"^
"\n"^
"  case IOCTL_GET_POLICY:\n"^
"    dev_bossa_"^nm^"_read(fd, (char*)param, sizeof(bossa_kern), 0);\n"^
"    break;\n"^
"  }\n"^
"\n"^
"  return SUCCESS;\n"^
"}\n"^
"\n"^
"/* declaration section*/\n"^
"struct file_operations procfops = {\n"^
"  NULL,\n"^
"  dev_bossa_"^nm^"_read,\n"^
"  dev_bossa_"^nm^"_write,\n"^
"  NULL,\n"^
"  NULL,\n"^
"  dev_bossa_"^nm^"_ioctl,\n"^
"  NULL,\n"^
"  dev_bossa_"^nm^"_open,\n"^
"  NULL,\n"^
"  dev_bossa_"^nm^"_release\n"^
"};\n"^
"\n"^
"static struct inode_operations inode_ops =\n"^
"{\n"^
"  &procfops,\n"^
"  NULL, /*create*/\n"^
"  NULL,/*lookup*/\n"^
"  NULL,\n"^
"  NULL,\n"^
"  NULL,\n"^
"  NULL, /*mkdir*/\n"^
"  NULL, /*rmdir*/\n"^
"  NULL, /*mknod*/\n"^
"  NULL, /*rename*/\n"^
"  NULL,\n"^
"  NULL,\n"^
"  NULL, /*readpage*/\n"^
"  NULL,\n"^ 
"  NULL, /*bmap*/\n"^
"  NULL, \n"^
"  NULL\n"^
"};\n"^
"\n"^
"static struct proc_dir_entry procbossa = \n"^
"{\n"^
"  0,\n"^
"  11, /*length of the filename*/\n"^
"  \"bossa_"^nm^"\",\n"^
"  S_IFREG | S_IRUGO | S_IWUSR,\n"^
"  1,\n"^
"  0,0, /*uid, gid*/\n"^
"  100,\n"^
"  &inode_ops,\n"^
"  NULL\n"^
"};\n"^
"\n"^
"int init_module()\n"^
"{\n"^
"  int ret;\n"^
" \n"^
"  ret = module_register_chrdev(MAJOR_NUM,\n"^
"			       DEVICE_NAME,\n"^
"			       &procfops);\n"^
" \n"^
"  if (ret < 0) {\n"^
"    DBG(\"%s registation fail\n\", __FILE__);\n"^
"    return ret;\n"^
"  }\n"^
"\n"^
"  //  DBG(\"Installed Bossa scheduler:%s\n\", DEVICE_NAME);\n"^
"\n"^
"  return proc_register(&proc_root, &procbossa);\n"^
"\n"^
"}\n"^
"\n"^
"void cleanup_module()\n"^
"{\n"^
"  int ret;\n"^
"\n"^
"  ret= module_unregister_chrdev(MAJOR_NUM, DEVICE_NAME);\n"^
"\n"^
"  if (ret < 0)\n"^
"    DBG(\"Error in unregister:%d\n\", ret);\n"^
"\n"^
"  proc_unregister(&proc_root, &procbossa);\n"^
"\n"^
"}\n"

let ioctl dirname nm=
  device_name:=nm;
  Printf.printf "ioctl...\n";
  (Printf.printf "d=%s\n" (!device_name));
  print_string  (!device_name);
  let f=open_out (dirname^"/"^"bossa_ioctl.h") in 
  (set_formatter_out_channel (f);
  set_margin 80;  
  open_box 0;
  print_string (ioctl_h(nm));
  close_box ();
  close_out(f););
  set_formatter_out_channel (open_out (dirname^"/bossa_"^(!device_name)^".c"));
  set_margin 80;  
  open_box 0;
  print_string (device_proc(nm));
  close_box ();








