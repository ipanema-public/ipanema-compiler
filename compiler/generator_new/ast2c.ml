(* $Id$ *)
module L = List

module B = Objects
module CS = Class_state
module G = Genaux

open Format
open Ioctl

type phase = HANDLERS | INTERFACE | INTERFACE_LABEL of string | ATTACH | DETACH
let current_phase = ref HANDLERS
              
let event_param = ref(B.mkId "")

let break_or_cont_labels = ref ([] : B.identifier option list)
(* --------------------------- Expressions -------------------------- *)

let list_of_const_val_enum =
  ref ["SchedState.NO_STATE"; "SchedState.RUNNING"; "SchedState.READY";
	"SchedState.BLOCKED"; "SchedState.TERMINATED"; "sched_type.PS";
	"sched_type.VS"]

let rec is_const_val_enum nm_val l =
  match l with
    [] -> false
  | t::q -> (nm_val = t) || is_const_val_enum nm_val q

let is_const_exp = function
    Ast.INT _ -> true
  | _ -> false

(* for passing processes and schedulers to external (kernel-defined or
parent-defined) functions *)
let mk_cast exp ext =
  if ext
  then
    (match B.ty(Ast.get_exp_attr exp) with
      B.PROCESS -> Some ("("^G.pp_process_struct_cs ()^" *)")
        | _ -> None)
  else None

let pp_order = function
    Ast.HIGHEST -> print_string " HIGHEST"
  | Ast.LOWEST -> print_string " LOWEST"

let self = ref "ipanema_internal_self"

let pp_com attr =
  match B.com attr with
    None -> ()
  | Some(str) -> print_space () ;
                 print_string "/* "; print_string str; print_string " */";
                 print_space ()

let rec pp_ucexp = function
    Ast.SELF att -> print_string !self
  | Ast.SUM att -> print_string "sum"
  | Ast.MIN att -> print_string "min"
  | Ast.MAX att -> print_string "max"
  | Ast.COUNT att -> print_string "count"
  | Ast.FNOR att -> print_string "or"
  | Ast.DISTANCE att -> print_string "distance"
  | Ast.INT(n,attr) -> print_int n
  | Ast.VAR(id,_,attr) ->
    	(match (B.ty attr) with
	B.ENUM(name) ->
	  if ((!B.csharp) &&
	      (is_const_val_enum ((B.id2c(name))^"."^(B.id2c(id)))
		 !list_of_const_val_enum))
	  then (G.pp_id name; print_string "."; G.pp_id id;)
	  else G.pp_id id
	| _ -> G.pp_id id)
    | Ast.FIELD(Ast.VAR(id,_,_) as exp,fld,_,_) ->
	let not_target _ =
	  if (G.is_struct false id)
	  then (G.pp_struct_id2 id; print_string "."; G.pp_id fld)
	  else (G.pp_struct_id2 id; print_string "->"; G.pp_id fld) in
	(match B.ty(Ast.get_exp_attr(exp)) with
(*	  B.PEVENT | B.CEVENT ->
	    let str = B.id2c fld in
	    if str = "target"
	    then print_string "tgt"
	    else if str = "type"
	    then print_string "event"
	    else not_target()
*)	| B.TIME -> G.pp_struct_id2 id; print_string "."; G.pp_id fld
	| _ -> not_target())

    | Ast.FIELD(exp,fld,_,attr) when G.is_state fld ->
       pp_exp exp; print_string "."; G.pp_id fld

    | Ast.FIELD(exp,fld,_,attr) ->
	(match B.ty(Ast.get_exp_attr(exp)) with
	  B.TIME
	| B.OPAQUE "ipanema_rq"
	| B.OPAQUE "ipa_metadata" -> pp_exp exp; print_string "."; G.pp_id fld
	| _ -> print_paren exp; print_string "->"; G.pp_id fld)

    | Ast.CAST(typ, exp,attr) -> print_string "("; G.pp_type typ; print_string ")"; pp_exp exp

    | Ast.TYPE(typ,attr) ->
       (match typ with
          B.CORE -> G.pp_core_struct()
        | _ -> G.pp_type typ)

    | Ast.LCORE(obj, exp, core, attr) ->
       (open_vbox 0;
	let fct = match obj with
	    "core" -> Ipanema.policy_core
	  | "state_info" -> Ipanema.policy_si
	in
	begin
	  match core with
	    None ->
	      print_string "/* Is it safe to call smp_proc_id here ?  */";
	      print_cut ();
	      print_string (fct^"(smp_processor_id())")
	  | Some cexp ->
	     print_string (fct^"(");
	    pp_exp cexp;
	    print_string ")"
	end;
        begin
          match exp with
            Ast.INT_NULL _ -> ()
          | _ ->
	     print_string ".";
             pp_exp exp
        end;
	close_box ())

    | Ast.BOOL(true,attr) -> print_string "true"

    | Ast.BOOL(false,attr) -> print_string "false"

    | Ast.UNARY(Ast.NOT,exp,attr) ->
	(print_string "!"; print_paren exp)

    | Ast.UNARY(Ast.COMPLEMENT,exp,attr) ->
	(print_string "~"; print_paren exp)

    | Ast.BINARY(bop,exp1,exp2,attr) ->
	(match bop with
	  Ast.OR | Ast.AND -> open_box 0
	| _ -> ());
	(if (!B.csharp) then
    	  (if (B.type2ctype (B.ty (Ast.get_exp_attr exp1))="SchedState") then
	    print_string "(int)";
	   print_paren exp1; pp_bop bop;
	   if (B.type2ctype (B.ty (Ast.get_exp_attr exp2))="SchedState") then
	     print_string "(int)";
	   print_paren exp2)
	else
	  (print_paren exp1; pp_bop bop; print_paren exp2));
	(match bop with
	  Ast.OR | Ast.AND -> close_box()
	| _ -> ())

    | Ast.TERNARY(exp1,exp2,exp3,attr) ->
       (print_paren exp1; print_string " ? ";
	print_paren exp2; print_string " : ";
	print_paren exp3)

    | Ast.PBINARY(bop,exp1,states1,exp2,states2,attr) ->
	raise (Error.Error "internal error: unexpected pbinary")

    | Ast.CONDOP(exp1, exp2, exp3, attr) ->
       begin
	 print_string "(";
	 pp_exp exp1;
	 print_string " ? ";
	 pp_exp exp2;
	 print_string " : ";
	 pp_exp exp3;
	 print_string ")"
       end

    | Ast.INDR(exp,attr) ->
	print_string "*"; print_string "("; pp_exp exp; print_string ")"

    | Ast.SELECT(_,attr) ->
	print_string "select()"

    | Ast.FIRST(exp,crit,_,attr)
      when B.ty attr = B.PROCESS ->
	print_string "policy_metadata(ipanema_first_task(&"; pp_exp exp; print_string "))"

    | Ast.FIRST(exp,None,_,attr)
      when B.ty attr = B.CORE ->
       print_string "cpumask_first(";
       pp_exp exp; print_string ")"

    | Ast.FIRST(exp,None,_,attr)
      when B.ty attr = B.GROUP ->
	print_string "__ffs("; pp_exp exp; print_string ")"

    | Ast.FIRST (_, Some _, _, attr) ->
       failwith (__LOC__ ^": FIRST with crit must be compiled before\n"^
		   "Check line "^ string_of_int (B.line attr))

    | Ast.FIRST(exp,crit,_,attr) ->
	print_string "first("; pp_exp exp; print_string ")"

    | Ast.EMPTY(id,_,tv,_,attr) when B.ty attr = B.SET B.CORE ->
       (print_string "cpumask_empty(";
        G.pp_id id; print_string ")")

    | Ast.EMPTY(id,state,tv,_,attr) when B.ty attr = B.SET B.GROUP ->
       let [CS.COMP(domid, _)] = state in
       (print_string "bitmap_empty("; G.pp_id id;
        print_string (", "^B.id2c domid^"->___sched_group_idx)"))

    | Ast.EMPTY(id,_,tv,_,attr) ->
	(print_string "empty("; G.pp_id id; print_string ")")

    | Ast.VALID(exp,sti,attr) ->
	(print_string "("; pp_exp exp; print_string ")")

    | Ast.PRIM((Ast.FIELD(Ast.PARENT(_),id,_,fn_attr)),args,attr) ->
	G.pp_id id; print_string "("; open_box 0;
	G.print_between G.pp_comma
	  (function exp ->
	    match mk_cast exp true with
	      Some x -> print_string x ; print_paren exp
	    | None -> pp_exp exp)
	  args;
	close_box(); print_string ")"

    | Ast.PRIM(f,args,attr) as exp ->
	let ext =
	  (match f with
	    Ast.VAR(id,_,_) -> Aux.member id (!G.external_functions)
	  | _ -> false (* assumed not to be external *)) in
	(match mk_cast exp ext with Some x -> print_string x | None -> ());
	let print_app _ =
	  print_paren f; print_string "(";
	  open_box 0;
	  G.print_between G.pp_comma
	    (function exp ->
	      match mk_cast exp ext with
	      	Some x -> print_string x ; print_paren exp
	      | None -> pp_exp exp)
	    args;
	  close_box(); print_string ")" in
	if (!B.csharp) then
	(match f with
	  Ast.VAR(id,_,_) ->
	  (match (B.id2c(id)) with
	    "next_exists" -> pp_ucexp (L.hd args); print_string "->sched"
	  | "empty_proc" ->
	      (print_string "((";
	       pp_ucexp (L.hd args);
	       print_string ") == null)";)
	  | "empty_queue" ->
	      (print_string "list_empty(ref ";
	       print_paren (L.hd args);
	       print_string ")";)
	  | "state_of" ->
	      (print_string "("; print_paren (L.hd args);
	       print_string ")->state ";)
	  | "attr" ->
	      print_string "((task_struct *)((char *)(";
	      print_paren (L.hd args);
	      G.print_nl_string
		")-(ulong)((int)(&((task_struct *)0)->bossa)/2)))"
	  | "select_sort" ->
	      (print_string "select_sort(ref ";
	       print_paren (L.hd args);
	       print_string ")")
	  | "select_partially_sorted" ->
	      (print_string "select_fifo_partially_sorted(&(";
	       pp_ucexp (L.hd args); G.print_nl_string("));");)
	  | _ -> print_app())
	| _ -> print_app())
	else print_app()

    | Ast.SCHEDCHILD(arg,procs,attr) ->
	(print_string "next("; open_box 0; pp_exp arg; close_box();
	 print_string ")")

    | Ast.SRCONSCHED(attr) ->
	open_box 0;
	print_string "src_next_list != src_next_list->next &&";
	print_cut();
	print_string "src_next_list->sched->ops == &EXPORTS";
	close_box()

    | Ast.ALIVE(_,_,attr) -> print_string "target_alive"

    | Ast.AREF(exp1,Some(exp2),_) ->
	(print_string "(&";
	 pp_exp exp1; print_string "["; pp_exp exp2; print_string "])")

    | Ast.AREF(exp1,None,_) ->
       (print_string "&";
        pp_exp exp1)

    | Ast.IN(exp,locexp,[_],_,_,_,attr) ->
	(* used when there is only one state possible *)
	if (!B.csharp) then
	  (print_string "("; pp_exp exp; print_string ")->state == ";
	   G.pp_state_cst locexp)
	else
	  (print_string "state_of("; pp_exp exp; print_string ") == ";
	   G.pp_state_cst locexp)

    | Ast.IN(_,_,_,_,_,_,attr) ->
	raise
	  (Error.Error
	     (Printf.sprintf "only one state expected for IN in %s"
		(B.loc2c attr)))

    | Ast.MOVEFWDEXP(exp,state_end,attr) ->
	print_string "forwardImmediate(";
	open_box 0; pp_exp exp; print_string ", ";
	G.pp_id (!event_param);
	G.pp_comma_nl();
	G.print_nl_string "src_next_list->next,";
	print_string "tgt_next_list->next";
	close_box();
	print_string ")"

    | Ast.PARENT(attr) -> failwith "parent must be used in a call"

    | Ast.INT_STRING (s, a) ->
     print_string "\""; print_string s; print_string "\""

    | Ast.INT_NULL a ->
     print_string "NULL"

and pp_exp exp =
  pp_ucexp exp;
  pp_com (Ast.get_exp_attr exp)

(* eventually, compare the precedence of the expression with the precedence
   of the context, as done for devil *)
and print_paren exp =
  match exp with
    Ast.INT_STRING _
  | Ast.INT_NULL _
  | Ast.SELF _
  | Ast.SUM _
  | Ast.MIN _
  | Ast.MAX _
  | Ast.COUNT _
  | Ast.FNOR _
  | Ast.DISTANCE _
  | Ast.INT _
  | Ast.LCORE _
  | Ast.VAR _
  | Ast.FIELD _ -> pp_exp exp

    | Ast.BOOL(v,attr) -> pp_exp exp

    | Ast.PARENT(attr) -> pp_exp exp

    | Ast.UNARY(uop,uexp,attr) -> pp_exp exp

    | Ast.BINARY(bop,exp1,exp2,attr) ->
	(print_string "("; pp_exp exp; print_string ")")

    | Ast.TERNARY(exp1,exp2,exp3,attr) ->
	(print_string "("; pp_exp exp; print_string ")")

    | Ast.PBINARY(bop,exp1,states1,exp2,states2,attr) ->
	raise (Error.Error "internal error: unexpected pbinary")

    | Ast.INDR(exp,attr) -> pp_exp exp

    | Ast.SELECT(_,attr) -> pp_exp exp

    | Ast.FIRST(_,crit,_,attr) -> pp_exp exp

    | Ast.EMPTY(id,_,_,_,attr) -> pp_exp exp

    | Ast.VALID(_,_,attr) -> pp_exp exp

    | Ast.PRIM(f,args,attr) -> pp_exp exp

    | Ast.SCHEDCHILD(arg,procs,attr) -> pp_exp exp

    | Ast.SRCONSCHED(attr) -> pp_exp exp

    | Ast.ALIVE(_,_,attr) -> pp_exp exp

    | Ast.AREF(exp1,exp2,attr) ->
       (print_string "("; pp_exp exp; print_string ")")

    | Ast.IN(exp1,id,_,_,_,_,attr) ->
	(print_string "("; pp_exp exp; print_string ")")

    | Ast.MOVEFWDEXP(exp1,state_end,attr) -> pp_exp exp

and pp_bop = function
  | Ast.AND -> print_space(); print_string "&&"; print_space()
  | Ast.OR -> print_space(); print_string "||"; print_space()
  | op -> print_string (" "^(Ast.bop2c op)^" ")

(* -------------------------- Declarations ------------------------- *)

let need_comma = ref false

let pp_static_timer_structures id =
  let snm = B.id2c id in
  G.print_nl_string ("static struct timer_list _"^snm^"_timer;");
  G.print_nl_string ("static struct bossa_timer_data _"^snm^"_timer_data;")

let pp_decl global define (Ast.VARDECL(ty,id,imported,lz,_,de,attr)) =
  need_comma := false;
  (if global
  then
    match ty with
      B.TIMER(_) -> pp_static_timer_structures id
    | _ -> ());
  if not (B.id2c id = "current") then
  (if imported
  then
    (if not (!B.csharp) then
     begin
       pp_com attr;
       if global then print_string "extern ";
       print_string (B.type2c(ty)); need_comma := true;
     end)
  else
    (if global then print_string "static ";
     if not define then
       begin
         pp_com attr;
         G.pp_type ty
       end;
     need_comma := true);
   if not imported || not (!B.csharp)
   then begin print_string " "; G.pp_struct_id(id); need_comma := true end)

let pp_decl_cs global (Ast.VARDECL(ty,id,imported,lz,_,de,attr)) =
  need_comma := false;
  if not (B.id2c id = "current") then
  if not imported
  then begin print_string "    "; G.pp_struct_id(id); need_comma := true end

let pp_def global = function
    Ast.VALDEF(decl,exp,isconst,attr) ->
      (open_box 2;
       if isconst=Ast.CONST then
	 (if not global then (print_string "const"; print_space());
	  pp_decl global false decl;
	  if is_const_exp exp then
	    (print_string " = ";
             pp_exp exp);
	  print_string ";")
       else if isconst=Ast.DEFCONST
       then
	 if (!B.csharp)
	 then (
	   print_string "private const uint ";
	   pp_decl false false decl;
	   print_string " = ";
	   pp_exp exp;
	   print_string ";")
	 else (
	   print_string "#define ";
	   pp_decl false true decl;
	   print_string "  ";
	   pp_exp exp;
           print_cut ())
       else
	 (if isconst = Ast.CONSTVAR then
	     (print_string "const"; print_space () );
	  pp_decl global false decl;
	  if not global then
	    (print_string " = ";
             pp_exp exp);
	  print_string ";");
       close_box();
       print_cut())
  | Ast.UNINITDEF(decl,attr) ->
      (pp_decl global false decl;
       if (!need_comma) then print_string ";"; print_cut())
  | Ast.SYSDEF(decl,isconst,attr) ->
     if isconst = VARIABLE then
       (pp_decl global false decl;
	if (!need_comma) then print_string ";"; print_cut())
  | Ast.DUMMYDEF(id,exp,attr) ->
      (print_string "???"; print_space(); G.pp_id(id);
       print_string " = "; pp_exp exp; print_string ";";
       print_cut())

let pp_def_cs global = function
    Ast.VALDEF(decl,exp,isconst,attr) ->
      (open_box 2;
       pp_decl_cs global decl;
       print_string " = ";
       pp_exp exp; print_string ";";
       close_box();print_cut())
  | Ast.UNINITDEF(decl,attr) ->
      (pp_decl_cs global decl;
       if (!need_comma) then print_string ";"; print_cut())
  | Ast.SYSDEF(decl,isconst,attr) ->
      (pp_decl_cs global decl;
       if (!need_comma) then print_string ";"; print_cut())
  | Ast.DUMMYDEF(id,exp,attr) ->
      (print_string "???"; print_space(); G.pp_id(id);
       print_string " = "; pp_exp exp; print_string ";";
       print_cut())

let pp_defs global defs =
  List.iter (pp_def global) defs;
  G.nl_ifnonempty(defs)

let pp_initdef = function
    Ast.VALDEF(Ast.VARDECL(_,id,_,lz,_,de,_),exp,isconst,attr) ->
      if not (is_const_exp exp) && not (isconst = Ast.DEFCONST) then
	begin
	  print_string (B.id2c id);
	  print_string " = ";
	  pp_exp exp;
	  G.print_nl_string ";"
	end
  |_ -> ()

let pp_initdefs defs =
  List.iter pp_initdef defs;
  G.nl_ifnonempty(defs)

(* --------------------------- Statements -------------------------- *)

let print_casename idlist =
  List.iter
    (function id ->
      print_string "case "; G.pp_state_cst (Ast.VAR(id,[],B.mkAttr(0))); G.print_nl_string ":")
    idlist

let print_caseid idlist =
  List.iter
    (function id ->
      print_string "case "; G.pp_id id; G.print_nl_string ":")
    idlist

let pp_return (mutex, retty) =
  if mutex then
    (print_cut ();
     G.print_nl_string "/* Generated if synchronized keyword is used */";
     G.print_nl_string "spin_unlock_irqrestore(&lb_lock, flags);");
  match retty with
    B.VOID -> print_string "return;"
  | B.INT -> print_string "return 0;"
  | B.BOOL -> print_string "return false;"
  | B.INDR _ -> print_string "return NULL;"
  | _ -> failwith (__LOC__ ^": Unsupported return type")

let rec pp_stmt retty stmt =
  pp_com (Ast.get_stm_attr stmt);
  pp_ustmt retty stmt

and pp_ustmt retty = function
   Ast.IF(exp,stmt1,stmt2,attr) ->
      ((match exp with
       	Ast.BINARY(Ast.BITAND,_,_,_) -> (print_string "if (("; pp_exp exp; print_string ")!=0")
	| _ -> (print_string "if ("; pp_exp exp;));
       print_string ") ";
       match stmt1 with
	 Ast.SEQ([], [Ast.IF _], _) -> (* To avoid ambiguous if/else in C *)
	   print_string "{";
	   pp_seq retty stmt1;
	   print_string "}"
       | _ -> pp_seq retty stmt1;
       match stmt2 with
	 Ast.SEQ([],[],_) -> ()
       | _ -> (print_string " else "; pp_seq retty stmt2))

(*------ foreach -----*)
  | Ast.FOR(id,state_id,states,dir,stmt,crit, attr) ->
    (*     G.print_nl_string ("/* "^__LOC__^" */"); *)
     pp_for (B.ty attr) id (function _ -> pp_seq retty stmt)
       (move_possible stmt) state_id states dir crit

  | Ast.FOR_WRAPPER(label,[Ast.FOR(_,_,_,_,_,_,_) as s],a) ->
     G.print_nl_string ("/* "^__LOC__^" */");
	Aux.push None break_or_cont_labels;
	pp_stmt retty s;
	Aux.pop break_or_cont_labels

    | Ast.FOR_WRAPPER(label,stms,a) ->
     G.print_nl_string ("/* "^__LOC__^" */");
	Aux.push (Some label) break_or_cont_labels;
	G.print_between print_cut (pp_stmt retty) stms;
	Aux.pop break_or_cont_labels;
	print_cut();
	G.pp_id label; print_string ":;"
(*------ foreach end -----*)

    | Ast.SWITCH(exp,cases,default,attr) ->
	let is_proc_sched =
	  (match B.ty (Ast.get_exp_attr exp) with
	    B.PROCESS ->  true
	  | _ -> false) in
	(print_string "switch ("; pp_exp exp;
	 print_string ") "; pp_cases retty cases default is_proc_sched)

    | (Ast.SEQ(decls,stmts,attr) as x) -> pp_seq retty x

    | Ast.RETURN(None,attr) ->
       if fst retty then
         (G.print_nl_string "/* Generated if synchronized keyword is used */";
          G.print_nl_string "spin_unlock_irqrestore(&lb_lock, flags);");
       (match !current_phase with
	  HANDLERS -> print_string "return;"
	| INTERFACE ->
	    if (!B.csharp)
	    then print_string ("return ("^(!B.type_return)^")SUCCESS;")
	    else print_string ("return SUCCESS;")
	| INTERFACE_LABEL(l) ->
	    print_string "goto "; print_string l; print_string ";"
	| ATTACH | DETACH ->
	    if (!B.csharp)
	    then print_string ("return ("^(!B.type_return)^")SUCCESS;")
	    else print_string ("return SUCCESS;")
	)
    | Ast.RETURN(Some(exp),attr) ->
       if fst retty then
         (G.print_nl_string "/* Generated if synchronized keyword is used */";
          G.print_nl_string "spin_unlock_irqrestore(&lb_lock, flags);");
       (open_box 2; print_string "return ";
	 if (!B.csharp) then print_string ("("^(!B.type_return)^")(");
	 pp_exp exp;
	 if (!B.csharp) then print_string ")"; print_string ";"; close_box())

    | Ast.STEAL(exp,attr) ->
       (open_box 2; print_string "steal_for(policy,"; print_space ();
	pp_exp exp; print_string ");"; close_box())

    | Ast.MOVE(exp,state,_,_,dst,_,_,attr) ->
       (* only used in blast mode *)
       let cstate = 
         (match dst with
	    Some(CS.CSTATE(_,CS.UNK)) |
	    None -> raise (Error.Error "destination not known")
	    | Some(CS.STATE(_,_,CS.PROC,_)) -> print_string "move_proc("; None
	    | Some(CS.STATE(_,CS.TERMINATED,_,_)) -> print_string "move_nowhere(";None
	    | Some(CS.STATE(_,cl,CS.QUEUE(_),_)) ->
               print_string "ipa_change_queue("; Some (CS.class2c cl)
	    | Some(CS.CSTATE(_,CS.ACTIVE)) -> print_string "set_active_core(";

                                              Some "IPANEMA_ACTIVE_CORE"
	    | Some(CS.CSTATE(_,CS.SLEEPING)) -> print_string "set_sleeping_core(";
                                                Some "IPANEMA_IDLE_CORE"
	 )
       in
       open_box 0; pp_exp exp; G.pp_comma();
       print_string "&" ; pp_exp state; G.pp_comma();
       (match cstate with
          Some state -> print_string state
        | None -> G.pp_state_cst state
       ); close_box();
       print_string ");"

    | Ast.MOVEFWD(exp,_,_,attr) ->
	print_string "forwardImmediate(";
	open_box 0; pp_exp exp; print_string ", "; G.pp_id (!event_param);
	G.pp_comma_nl();
	G.print_nl_string "src_next_list->next,";
	print_string "tgt_next_list->next";
	close_box();
	print_string ");"

    | Ast.SAFEMOVEFWD(exp,x,stm,state_end,attr) ->
      raise
	(Error.Error
	   "ast2c: unexpected safe move forward statement\n")

    | Ast.ASSIGN(expl,expr,sorted_fld,attr) ->
	let ltype = B.ty (Ast.get_exp_attr expl)
	and rtype = B.ty (Ast.get_exp_attr expr) in
	(open_hbox ();
	 pp_exp expl; print_space(); print_string "="; print_space();
	 if not (ltype = rtype)
	 then
	   (print_string "("; G.pp_type ltype; print_string ")";
	    print_paren expr)
	 else pp_exp expr;
	 print_string ";"; close_box()
	)

    | Ast.DEFER(attr) -> print_string "deferEvent();"
    | Ast.BREAK(attr) ->
	(match Aux.top break_or_cont_labels with
	  None -> print_string "break;"
	| Some label -> print_string "goto "; G.pp_id label; print_string ";")
    | Ast.CONTINUE(attr) ->
       (match Aux.top break_or_cont_labels with
       | None -> print_string "continue;"
       | Some label -> failwith "no labels in continue" )
    | Ast.PRIMSTMT((Ast.FIELD(Ast.PARENT(_),id,_,fn_attr)),args,attr) ->
	G.pp_id id; print_string "("; open_box 0;
	G.print_between G.pp_comma
	  (function exp ->
	    match mk_cast exp true with
	      Some x -> print_string x ; print_paren exp
	    | None -> pp_exp exp)
	  args;
	close_box(); print_string ");"

    | Ast.PRIMSTMT(Ast.VAR(id,_,va),[exp],a) as x
	when B.id2c id = "lock"
	||  B.id2c id = "unlock"->
       let fn = "ipanema_" ^ B.id2c id ^"_core" in
       print_string fn;
       print_string "(";
       open_box 0;
       pp_exp exp;
       print_string "->id";
       close_box(); print_string ");"

    | Ast.PRIMSTMT(f,args,attr) ->
      let ext =
	(match f with
	    Ast.VAR(id,_,_) -> Aux.member id (!G.external_functions)
	  | _ -> false (* assumed not to be external *)) in
      open_hbox ();
      print_paren f; print_string "(";
      open_box 0;
      G.print_between G.pp_comma
	(function exp ->
	  match mk_cast exp ext with
	      Some x -> print_string x ; print_paren exp
	    | None -> pp_exp exp)
	args;
      close_box();
      print_string ");";
      close_box()

    | Ast.ERROR(str,attr) ->
	let einval _ =
	  G.mk_printk (str^"\\n");
	  if (!B.csharp)
	  then G.print_nl_string "set_errno(-_EINVAL);"
	  else G.print_nl_string "errno = -EINVAL;" in
	let einval_attach _ =
	  (* only for process schedulers *)
	  G.print_nl_string ("error_string = \""^str^"\\n\";");
	  if (!B.csharp)
	  then G.print_nl_string "set_errno(-_EINVAL);"
	  else G.print_nl_string "errno = -EINVAL;" in
	(match !current_phase with
	  HANDLERS ->
	    print_string "panic(\"";
	    print_string str;
	    G.print_nl_string ": file %s line %d\",";
	    if (!B.csharp)
	    then
	      (print_string "        \""; print_string (!G.policy_name);
	       print_string ".cs\", 0);";)
	    else print_string "        __FILE__, __LINE__);"
	| INTERFACE
	| INTERFACE_LABEL(_) ->
	    einval();
	    print_string "return FAILURE;"
	| DETACH -> einval();
	    print_string "return FAILURE;"
	| ATTACH -> einval_attach();
	    print_string "return FAILURE;")

    | Ast.ASSERT(exp,_) ->
	(open_box 2; print_string "bossa_assert("; pp_exp exp;
	 print_string ");"; close_box())

(* ----------------------- Start of for loops ---------------------- *)
and pp_for_domset_simple id dir =
  (* for loop *)
  if dir = Ast.DEC
  then failwith "Unsupported decrementation on set of domains";
  print_string "while("; G.pp_id id; G.print_nl_string "->parent)";
  print_string "\t"; G.pp_id id; print_string " = "; G.pp_id id ; print_string "->parent;"

and pp_for_domset set_id id print_stmt move_possible dir crit =
  G.start_block();
  (* for loop *)
  if dir = Ast.DEC
  then failwith "Unsupported decrementation on set of domains";
  G.pp_type B.DOMAIN ; print_string " ";
  G.pp_id id; print_string " = NULL"; G.pp_semic();
  print_string "for (";
  G.pp_id id; print_string " = "; pp_exp set_id; print_string "; ";
  G.pp_id id; print_string "; ";
  G.pp_id id; print_string " = "; G.pp_id id ; print_string "->parent) "; G.start_block();
  print_stmt ();
  G.end_block false; G.end_block true

and pp_for_grpset set_id id print_stmt move_possible dir crit states =
  let index = B.fresh_id() in
  let sublist = B.fresh_id() in
  let dum = B.dum __LOC__ in
  let (dom, busiest, grpset, set_id) = match states with
      [CS.COMP(domid, grpset)] ->
       let initgroup = Ast.FIELD(Ast.VAR(domid, [], B.updty dum B.DOMAIN),
			         B.mkId "groups",
			         [], B.updty dum B.GROUP)
       in
       (Ast.VAR(domid,[], B.dum __LOC__),
        initgroup,
        Some grpset, Some set_id)
    | _ ->
       match set_id with
	 Ast.FIELD(dom, _, _, a) -> (dom, set_id, None, None)
       | _ -> failwith __LOC__
  in
  let selected_size = Ast.FIELD(dom, B.mkId("___sched_group_idx"),
				[], B.dum __LOC__) in
  G.start_block();
  print_string "int "; G.pp_id index; G.pp_semic();
  begin
    match crit with
      None -> ()
    | Some crit ->
       G.pp_type B.GROUP; G.pp_id sublist; print_string "["; pp_exp selected_size; print_string"];"
  end;
  (* for loop *)
  let (test, op,min,max) =
    if dir = Ast.DEC
    then (" > ", "--",Ast.BINARY(Ast.MINUS,selected_size,
			  Ast.INT(1, B.mkAttr 0),B.mkAttr 0),
	  Ast.INT(0, B.mkAttr 0))
    else (" < ", "++",Ast.INT(0, B.mkAttr 0), selected_size) in
  begin
    match crit with
      None ->
      begin
        G.pp_type B.GROUP ; print_string " ";
        G.pp_id id; print_string " = "; pp_exp busiest ; G.pp_semic();
        print_string "for (";
        G.pp_id index; print_string " = "; pp_exp min; print_string "; ";
        G.pp_id index; print_string test; pp_exp max; print_string "; ";
        G.pp_id index; print_string op; print_string ", ";
        G.pp_id id; print_string "++) "; G.start_block();
	begin
	  match grpset with
	    None -> ()
	  | Some set_id ->
	     (print_string "if (!test_bit("; G.pp_id index;
	      print_string (", "^ B.id2c set_id) ; G.print_nl_string ")){";
	      G.print_nl_string "\tcontinue;}")
	end;
        print_stmt ();
        (*  pp_for_queue B.GROUP sublist id print_stmt move_possible dir; *)
        G.end_block false; G.end_block true
      end
    | Some crit ->
       begin
	 let set_id = 
	   match set_id with
	     Some set_id -> set_id
	   | None ->  Ast.FIELD(dom, B.mkId("groups"), [], B.dum __LOC__)
	 in
         let swp_fnid = B.mkId "NULL" in
         match crit with
           Ast.CRIT_EXPR(_,_,_,Some cmp_fnid, critattr) ->
           begin
             print_cut ();
             print_string "for (";
             G.pp_id index; print_string " = 0; ";
             G.pp_id index; print_string " != "; pp_exp selected_size; print_string "; ";
             G.pp_id index; print_string op; print_string ") "; G.start_block();
             G.pp_type B.GROUP ; print_string " ";
             G.pp_id id; print_string " = &"; pp_exp set_id ; G.pp_arrayref index; G.pp_semic();
             G.pp_id sublist; G.pp_arrayref index; print_string " = "; G.pp_id id; G.pp_semic();
             G.end_block true;
             print_string "sort("; G.pp_id sublist; print_string ", "; pp_exp selected_size;
             print_string ", sizeof("; G.pp_type B.GROUP; print_string "), "; G.pp_id cmp_fnid;
             print_string ", "; G.pp_id swp_fnid; G.print_nl_string ");";
             print_string "for (";
             G.pp_id index; print_string " = "; pp_exp min; print_string "; ";
             G.pp_id index; print_string " != "; pp_exp max; print_string "; ";
             G.pp_id index; print_string op; print_string ") "; G.start_block();
             G.pp_type B.GROUP ; print_string " ";
             G.pp_id id; print_string " = "; G.pp_id sublist ; G.pp_arrayref index; G.pp_semic();
             print_stmt ();
             (*  pp_for_queue B.GROUP sublist id print_stmt move_possible dir; *)
             G.end_block false; G.end_block true
           end
         | Ast.CRIT_EXPR(_,_,_,_, critattr) | Ast.CRIT_ID (_,_,_,_,critattr)->
	    failwith (__LOC__ ^": Uncompiled criteria for "^ string_of_int (B.line critattr))
       end
  end

and pp_for_coreset set_id id print_stmt move_possible dir crit =
  let iter =
    match set_id with
      None -> "for_each_possible_cpu"
    | Some e -> "for_each_cpu"
  in
  let index = B.fresh_id() in
  G.start_block();
  print_string "int "; G.pp_id index; G.pp_semic();
  (* for loop *)
  begin
    match crit with
      None ->
       begin
        print_string iter;
        print_string " (";
        G.pp_id index;
        (match set_id with
           Some set_id -> print_string ", "; pp_exp set_id
         | None -> ()
        );
        print_string ")";
        G.start_block();
        G.pp_type B.CORE ; print_string " ";
        G.pp_id id; print_string " = &per_cpu(core,"; G.pp_id index; print_string ");";
        print_stmt ();
        G.end_block false; G.end_block true
      end
    | Some crit ->
       begin
         match crit with
           Ast.CRIT_EXPR(_,_,_,Some cmp_fnid,_) ->
           begin
             let coreset_size = B.fresh_idx "coreset_size" in
             let sublist = B.fresh_id() in
             let array_iter = B.fresh_idx "i" in
             print_string "int "; G.pp_id array_iter; G.print_nl_string " = 0;";
             print_string "int "; G.pp_id coreset_size;
             print_string " = cpumask_weight(";
             (match set_id with
               Some set_id -> pp_exp set_id
             | None -> print_string "cpu_possible_mask"
             );
             G.print_nl_string ");";
             G.pp_type B.CORE; G.pp_id sublist; print_string "["; G.pp_id coreset_size; G.print_nl_string"];";
             G.pp_type B.CORE ; print_string " "; G.pp_id id; G.print_nl_string ";";
             print_string iter;
             print_string " (";
             G.pp_id index;
             (match set_id with
               Some set_id -> print_string ", "; pp_exp set_id
             | None -> ()
             );
             print_string ")";
             G.start_block();
             G.pp_id id; print_string " = &per_cpu(core,"; G.pp_id index; G.print_nl_string ");";
             G.pp_id sublist; G.pp_arrayref array_iter; print_string " = "; G.pp_id id; G.pp_semic();
             G.pp_id array_iter; print_string "++;";
             G.end_block true;
             print_string "sort("; G.pp_id sublist; print_string ", "; G.pp_id coreset_size;
             print_string ", sizeof("; G.pp_type B.CORE; print_string "), "; G.pp_id cmp_fnid;
             G.print_nl_string ", NULL);";
             print_string "for ("; G.pp_id array_iter; print_string " = 0; ";
             G.pp_id array_iter; print_string " != "; G.pp_id coreset_size; print_string ";";
             G.pp_id array_iter; print_string "++) "; G.start_block();
             G.pp_id id; print_string " = "; G.pp_id sublist; G.pp_arrayref array_iter;
             G.pp_semic ();
             print_stmt ();
             G.end_block true;
             G.end_block true
           end
         | Ast.CRIT_EXPR(_,_,_,None,_) ->
            begin
              print_string "forloop on:"; G.pp_id id; G.end_block true
            end
         | _ -> ()
       end
  end

and pp_for_procqueue set_id id print_stmt move_possible dir crit =
  let index = B.fresh_id() in
  let next = B.fresh_id() in
  let selected_size = !CS.selected_size in
  G.start_block();
  print_string "struct task_struct *"; G.pp_id index; G.pp_semic();
  print_string "struct task_struct *"; G.pp_id next; G.pp_semic();
  (* for loop *)
  let (op,min,max) =
    if dir = Ast.DEC
    then ("--",selected_size-1,-1)
    else ("++",0,selected_size) in
  begin
    match crit with
      None -> ()
    | Some crit -> ()
  end;
  print_string "rbtree_postorder_for_each_entry_safe (";
  G.pp_id index; print_string ", ";
  G.pp_id next; print_string ", &";
  pp_exp set_id; print_string ".root, ";
  print_string "ipanema.node_runqueue) "; G.start_block();
  if B.id2c id <> "" then
    (G.pp_process_struct (); print_string " *"; G.pp_id id;
     print_string " = policy_metadata("; G.pp_id index ;G.print_nl_string ");");
  print_stmt ();
  G.end_block false; G.end_block true

and pp_for_proc state_id id print_stmt =
  G.start_block();
  G.pp_process_struct(); print_string " *";
  G.pp_id(id); print_string " = "; G.pp_id state_id;
  G.pp_semic();
  print_string "if ("; G.pp_id id; print_string ") ";
  G.start_block();
  print_stmt(); G.end_block false; G.end_block true

and pp_for_queue ty state_id id print_stmt move_possible dir =
  let elem = B.fresh_id() in
  G.start_block();
  (match ty with
    B.PROCESS -> G.pp_process_struct()
  | B.DOMAIN -> G.pp_domain_struct ()
  | B.GROUP -> G.pp_group_struct ()
  );
  print_string " *";
  G.pp_id(id); G.pp_semic();
  print_string (G.de_structify "struct list_head *");
  G.pp_id elem; G.pp_semic();
  if move_possible
  then
    (let ptr = B.fresh_id() in
    print_string (G.de_structify "struct list_head *");
    G.pp_id(ptr); G.pp_semic();
    if dir = Ast.DEC
    then print_string "list_for_each_prev_safe("
    else print_string "list_for_each_safe(";
    G.pp_id elem; print_string ", "; G.pp_id ptr;
    print_string ", "; G.pp_id state_id;
    print_string ") ";
    G.start_block();
    G.pp_id id; print_string " = list_entry(";
    G.pp_id elem;
    print_string ", ";
    G.pp_process_struct();
    G.print_nl_string ", list_info);";
    print_stmt();
    G.end_block false;
    G.end_block true)
  else (
    if dir = Ast.DEC
    then print_string "list_for_each_prev("
    else print_string "list_for_each(";
    G.pp_id elem; print_string ", "; G.pp_id state_id;
    print_string ") ";
    G.start_block();
    G.pp_id id; print_string " = list_entry(";
    G.pp_id elem;
    print_string ", ";
    G.pp_process_struct();
    G.print_nl_string ", list_info);";
    print_stmt();
    G.end_block false;
    G.end_block true)

and pp_for_array state_id id print_stmt move_possible dir =
  (* this has to test whether a process variable is empty.
     the analysis should determine whether this is possible!!! *)
  let index = B.fresh_id() in
  let sublist = B.fresh_id() in
  let selected_size = !CS.selected_size in
  G.start_block();
  print_string "int "; G.pp_id index; G.pp_semic();
  (* for loop *)
  let (op,min,max) =
    if dir = Ast.DEC
    then ("--",selected_size-1,-1)
    else ("++",0,selected_size) in
  print_string "for (";
  G.pp_id index; print_string " = "; print_int min; print_string "; ";
  G.pp_id index; print_string " != "; print_int max; print_string "; ";
  G.pp_id index; print_string op; print_string ") "; G.start_block();
  print_string (G.de_structify "struct list_head *"); G.pp_id sublist;
  print_string " = "; G.pp_arrayref_id state_id index; G.pp_semic();
  pp_for_queue B.PROCESS sublist id print_stmt move_possible dir;
  print_cut();
  G.end_block false; G.end_block true

and pp_for_on_id ty id print_stmt move_possible states_ids_opt states dir crit =
  match states_ids_opt with
    None -> (match ty with
               B.DOMAIN ->
	        begin
	          Aux.push None break_or_cont_labels;
	          pp_for_domset_simple id dir;
	          Aux.pop break_or_cont_labels
	 end
             | _ -> failwith "Unsupported for-iteration !")
  | Some [set_id] ->
     (match B.ty (Ast.get_exp_attr set_id) with
       B.SET B.DOMAIN ->
	 begin
	   Aux.push None break_or_cont_labels;
	   pp_for_domset set_id id print_stmt move_possible dir crit;
	   Aux.pop break_or_cont_labels
	 end
     | B.SET B.GROUP ->
	begin
	  Aux.push None break_or_cont_labels;
	  pp_for_grpset set_id id print_stmt move_possible dir crit states;
	  Aux.pop break_or_cont_labels
	end
     | B.SET B.CORE ->
	begin
          let set_id_opt =
            match set_id with
              Ast.PRIM(Ast.VAR(id, _, _), [], _)
                 when (B.id2c id) = "system_cores" -> None
            | Ast.VAR(id, _, _) when (B.id2c id)= "cpu_possible_mask" -> None
            | _ -> Some (Ast.AREF(set_id, None, B.dum __LOC__))
          in
	  Aux.push None break_or_cont_labels;
	  pp_for_coreset set_id_opt id print_stmt move_possible dir crit;
	  Aux.pop break_or_cont_labels
	end
     | B.INDR B.SET B.CORE ->
	begin
	  Aux.push None break_or_cont_labels;
	  pp_for_coreset (Some set_id) id print_stmt move_possible dir crit;
	  Aux.pop break_or_cont_labels
	end
     | B.QUEUE B.PROCESS ->
	begin
	  Aux.push None break_or_cont_labels;
	  pp_for_procqueue set_id id print_stmt move_possible dir crit;
	  Aux.pop break_or_cont_labels
	end
     | _ as ty ->
	failwith ("Unsupported for-iteration on "^(B.type2c ty)^"!\nSee "^__LOC__))
  | Some states_id ->
     failwith ("Unsupported for-iteration on multiple variables!\nSee "^__LOC__)

and pp_for ty id print_stmt move_possible states_ids_opt states dir crit =
  let isforid = match (states, states_ids_opt) with
      ([], None) -> true
    | (_::_, Some []) -> failwith ("Internal failure at "^__LOC__)
    | (_::_, None) -> false
    | ([], Some [set_id]) ->
       let att = Ast.get_exp_attr set_id in
       (match B.ty att with
	 B.SET B.DOMAIN
       | B.SET B.CORE
       | B.INDR B.SET B.CORE
       | B.SET B.GROUP
       | B.QUEUE B.PROCESS -> true
       | _ as ty ->
	  failwith ("Unsupported for-iteration on "^(B.type2c ty)^
		       " at line "^string_of_int (B.line att)^
		       "!\nSee "^__LOC__))
    | (_::_, Some [set_id]) ->
       (match B.ty (Ast.get_exp_attr set_id) with
	  B.SET B.GROUP -> true
        | B.SET B.DOMAIN -> true
       | _ -> failwith ("Unsupported for-iteration on multiple variables!\nSee "^__LOC__))
    | ([], Some states_id) ->
       failwith ("Unsupported for-iteration on multiple variables!\nSee "^__LOC__)
  in
  if isforid then
    pp_for_on_id ty id print_stmt move_possible states_ids_opt states dir crit
  else
    List.iter
      (function
    CS.STATE(state_id,_,CS.PROC,_) ->
      pp_for_proc state_id id print_stmt; print_cut()
      | CS.STATE(_,CS.NOWHERE,_,_) | CS.STATE(_,CS.TERMINATED,_,_) ->
	  (* a bit sloppy to allow these, but convenient for looping over all
	     states *)
	 ()
      | CS.STATE(state_id,_,CS.QUEUE(CS.SELECT(_)),_)
	  when !CS.selected_size > 1 ->
	 pp_for_array state_id id print_stmt move_possible dir;
	  print_cut()
      | CS.STATE(state_id,_,CS.QUEUE(_),_) ->
	 pp_for_queue B.PROCESS state_id id print_stmt move_possible dir;
	print_cut()
      | CS.CSTATE _ -> failwith "Unsupported for-iteration on core states"
      )
      states

(* ------------------------ End of for loops ----------------------- *)

and pp_top_seq mutex retty s =
  let ending = (mutex, retty) in
  let (decls,stmts) =
    match s with
      Ast.SEQ(d,s,_) ->
	(match List.hd s with
	  Ast.SEQ(d2,s2,attr) -> (d@d2,s2@(List.tl s))
	| _ -> (d, s))
    | _ -> ([],[s]) in
  let stmts = List.rev(flatten stmts) in
  (G.start_block();
   (match decls with
     [] -> ()
    | x::xs -> pp_defs false decls);
   if mutex then
     (G.print_nl_string "unsigned long flags = 0;";
      print_cut ();
      G.print_nl_string "/* Generated if synchronized keyword is used */";
      G.print_nl_string "if(!spin_trylock_irqsave(&lb_lock, flags))";
      print_string "\t";
      pp_return (false, retty);
      print_cut ());
   G.print_between print_cut (pp_stmt ending) stmts;
   G.end_block false)

and pp_seq retty s =
  let (decls,stmts) =
    match s with
      Ast.SEQ(d,s,_) ->
	(if s <> [] || d <> [] then
	    match List.hd s with
	      Ast.SEQ(d2,s2,attr) -> (d@d2,s2@(List.tl s))
	    | _ -> (d, s)
	else ([],[]))
    | _ -> ([],[s]) in
  match (decls, stmts, retty) with
  (*  ([], []) -> print_string ";"
  | *)([], [s], (false, _)) ->
      print_cut (); print_string "\t";
      pp_stmt retty s; print_cut ()
  | _ ->
     let stmts = List.rev(flatten stmts) in
     (G.start_block();
      (match decls with
	[] -> ()
      | x::xs -> pp_defs false decls);
      G.print_between print_cut (pp_stmt retty) stmts;
      G.end_block false)

and pp_seq2 retty s s2 =
  let (decls,stmts) =
    match s with
      Ast.SEQ(d,s,_) -> (d,s)
    | _ -> ([],[s]) in
  let stmts = List.rev(flatten stmts) in
  let block = match decls with
      [] -> false
    | x::xs ->
       G.start_block();
      pp_defs false decls;
      true
   in
   G.print_between print_cut (pp_stmt retty) stmts; print_cut ();
   print_string s2;
   if block then G.end_block true

and pp_seq3 retty s idl idr =
  let (decls,stmts) =
    match s with
      Ast.SEQ(d,s,_) -> (d,s)
    | _ -> ([],[s]) in
  let stmts = List.rev(flatten stmts) in
  (G.start_block();
   (match decls with
     [] -> ()
   | x::xs -> pp_defs false decls);
   G.pp_id idl; print_string " = "; G.pp_id idr; G.print_nl_string "->next;";
   G.print_between print_cut (pp_stmt retty) stmts; print_cut ();
   G.end_block true)

and flatten stmts =
  List.fold_left
    (function prev ->
      function
    Ast.SEQ([],s,_) -> (flatten s)@prev
	| s -> s::prev)
    [] stmts

and pp_cases retty cases default is_proc_sched =
  (G.start_block();
   List.iter
     (function Ast.SEQ_CASE(idlist,_,stmt,_) ->
       ((if is_proc_sched then print_casename else print_caseid) idlist;
	print_string "  "; open_box 0;
	pp_seq2 retty stmt "break;"; close_box();
	print_cut()))
     cases;
   G.print_nl_string "default:";
   if (!B.csharp) then G.start_block();
   (match default with
     None ->
       G.print_nl_string
	 "  panic(\"unknown state: file: %s, line: %d!.\",";
	 if (!B.csharp) then (print_string "        \""; print_string (!G.policy_name); print_string ".cs\", 0);")
	 else print_string "        __FILE__, __LINE__);";
         pp_return retty
   | Some x -> pp_stmt retty x);
   if (!B.csharp) then (print_string "break;"; G.end_block true;);
   G.end_block true)

and move_possible = function
    Ast.IF(exp,stm1,stm2,attr) -> move_possible stm1 || move_possible stm2
  | Ast.SWITCH(exp,cases,default,attr) ->
      (List.exists
	(function Ast.SEQ_CASE(pat,states,stm1,attr) -> move_possible stm1)
	cases)
    ||
      (match default with
	None -> false
      |	Some x -> move_possible x)
  | Ast.SEQ(decls,stms,attr) -> List.exists move_possible stms
  | Ast.FOR(index,state,state_info,dir,stm1,crit,attr) -> move_possible stm1
  | Ast.FOR_WRAPPER(label,stms,attr) -> List.exists move_possible stms
  | Ast.MOVE(exp,state,srcs,verifsrcs,dst,auto_allowed,state_end,attr) -> true
  | Ast.PRIMSTMT(Ast.VAR(id,_,_),_,_) -> B.id2c id = "update_state"
  | stm -> false
