module B = Objects
module G = Genaux
module A = Ast2c

open Format

let debug = ref true

(* --------------------------- lazy attributs ------------------------ *)
let pp_lazy_count_on_process ty var param listv =
  G.print_nl_string
    (B.id2c var ^" = "^
       Ipanema.policy_si^"("^ param^"->id)."^B.id2c listv^".nr_tasks;")
  
let pp_lazy_sum_on_process first ty var param listv f =
  if first then begin
    G.print_nl_string "struct task_struct *pos, *n;";
    print_cut ();
  end;
  print_string "rbtree_postorder_for_each_entry_safe(";
  open_box 0;
  print_string "pos,"; print_space ();
  print_string "n,"; print_space ();
  print_string ("&ipanema_state("^ param^"->id)."^B.id2c listv^".root,");
  print_space ();
  print_string "ipanema.node_runqueue) ";
  close_box ();
  G.start_block ();
  print_string (B.id2c var^" += "^G.pp_process_of "pos"^"->"^B.id2c f^";");
  G.end_block true

let pp_lazy_min_on_process ty var param listv f =
  G.print_nl_string "bool fst = true;";
  G.print_nl_string "struct task_struct *pos, *n;";
  print_cut ();
  print_string "rbtree_postorder_for_each_entry_safe(";
  open_box 0;
  print_string "pos,"; print_space ();
  print_string "n,"; print_space ();
  print_string ("&ipanema_state("^ param^"->id)."^B.id2c listv^".root,");
  print_space ();
  print_string "ipanema.node_runqueue) ";
  close_box ();
  G.start_block ();
  print_string "if (";
  open_box 0;
  (match ty with
    B.INT ->
      print_string (G.pp_process_of "pos"^"->"^B.id2c f);
      print_string " < ";
      print_string (B.id2c var);
  | B.TIME ->
      print_string "ktime_compare(";
      print_string (G.pp_process_of "pos"^"->"^B.id2c f);
      print_string ", ";
      print_string (B.id2c var);
      print_string ") < 0";
  );
  print_string " || fst";
  print_string ") ";
  close_box ();
  G.start_block ();
  G.print_nl_string "fst = false;";
  print_string (B.id2c var^" = "^G.pp_process_of "pos"^"->"^B.id2c f^";");
  G.end_block false;
  G.end_block true

let pp_lazy_max_on_process ty var param listv f =
  G.print_nl_string "bool fst = true;";
  G.print_nl_string "struct task_struct *pos, *n;";
  print_cut ();
  print_string "rbtree_postorder_for_each_entry_safe(";
  open_box 0;
  print_string "pos,"; print_space ();
  print_string "n,"; print_space ();
  print_string ("per_cpu(state_info, "^ param^"->id)."^B.id2c listv^",");
  print_space ();
  print_string "ipanema.node_runqueue) ";
  close_box ();
  G.start_block ();
  print_string "if (";
  open_box 0;
  (match ty with
    B.INT ->
      print_string (G.pp_process_of "pos"^"->"^B.id2c f);
      print_string " > ";
      print_string (B.id2c var)
  | B.TIME ->
      print_string "ktime_compare(";
      print_string (G.pp_process_of "pos"^"->"^B.id2c f);
      print_string ", ";
      print_string (B.id2c var);
      print_string ") < 0";

  );
  print_string " || fst";
  print_string ") ";
  close_box ();
  G.start_block ();
  G.print_nl_string "fst = false;";
  print_string (B.id2c var^" = "^G.pp_process_of "pos"^"->"^B.id2c f^";");
  G.end_block false;
  G.end_block true

let pp_lazy_or_on_process ty var param listv f =
  G.print_nl_string "struct task_struct *pos, *n;";
  print_cut();
  print_string "rbtree_postorder_for_each_entry_safe(pos, n, ";
  print_string ("per_cpu(state_info, "^ param^"->id)."^B.id2c listv^",");
  print_string "ipanema.node_runqueue) ";
  G.start_block ();
  print_string "if (pos->";
  print_string (B.id2c var);
  print_string ")";
  G.print_nl_string (B.id2c var^" = true;");
  G.print_nl_string "break;";
  G.end_block true

let pp_lazy_count_on_core ty var param listv =
  (* TODO: Check if f is a lazy attr of listv element *)
  print_string (B.id2c var^" = cpumask_weight(");
  print_string param;
  print_string "->";
  print_string (B.id2c listv);
  G.print_nl_string ");"

let pp_lazy_sum_on_core ty tyit var param listv f =
  G.print_nl_string "int cpu;";
  print_cut ();
  print_string "for_each_cpu(";
  open_box 0;
  print_string "cpu,"; print_space ();
  print_string "&";
  print_string param;
  print_string "->";
  print_string (B.id2c listv);
  print_string ") ";
  close_box ();
  G.start_block ();
  let (isconst, fields) = Type.get_fields tyit in
  if List.mem_assoc listv fields then
    print_string (B.id2c var^" += per_cpu(core, cpu)."^B.id2c f^";")
  else
    begin
      print_string (B.id2c var^" += ipa_core_"^B.id2c f^"(");
      open_box 0;
      print_string "&";
      print_string "per_cpu(core, cpu)";
      close_box ();
      print_string (");");
    end;
  G.end_block true

let pp_lazy_min_on_core ty tyit var param listv f =
  G.pp_type ty;
  G.print_nl_string " tmp;";
  G.print_nl_string "bool fst = true;";
  G.print_nl_string "int cpu;";
  print_cut ();
  print_string "for_each_cpu(";
  open_box 0;
  print_string "cpu,"; print_space ();
  print_string "&";
  print_string param;
  print_string "->";
  print_string (B.id2c listv);
  print_string ") ";
  close_box ();
  G.start_block ();
  let (isconst, fields) = Type.get_fields tyit in
  if List.mem_assoc listv fields then
    print_string ("tmp += per_cpu(core, cpu)."^B.id2c f^";")
  else
    begin
      print_string ("tmp += ipa_core_"^B.id2c f^"(");
      open_box 0;
      print_string "&";
      print_string "per_cpu(core, cpu)";
      close_box ();
      print_string (");");
    end;
  print_string "if (tmp < ";print_string (B.id2c var);print_string " || fst)";
  G.start_block ();
  G.print_nl_string "fst = false;";
  print_string (B.id2c var^" = tmp;");
  G.end_block true;
  G.end_block true

let pp_lazy_max_on_core ty tyit var param listv f =
  G.pp_type ty;
  G.print_nl_string " tmp;";
  G.print_nl_string "bool fst = true;";
  G.print_nl_string "int cpu;";
  print_cut ();
  print_string "for_each_cpu(";
  open_box 0;
  print_string "cpu,"; print_space ();
  print_string "&";
  print_string param;
  print_string "->";
  print_string (B.id2c listv);
  print_string ") ";
  close_box ();
  G.start_block ();
  let (isconst, fields) = Type.get_fields tyit in
  if List.mem_assoc listv fields then
    print_string ("tmp += per_cpu(core, cpu)."^B.id2c f^";")
  else
    begin
      print_string ("tmp += ipa_core_"^B.id2c f^"(");
      open_box 0;
      print_string "&";
      print_string "per_cpu(core, cpu)";
      close_box ();
      print_string (");");
    end;
  print_string "if (tmp > ";print_string (B.id2c var);print_string " || fst)";
  G.start_block ();
  G.print_nl_string "fst = false;";
  print_string (B.id2c var^" = tmp;");
  G.end_block true;
  G.end_block true

let pp_lazy_or_on_core ty var param listv f =
  G.print_nl_string "int cpu;";
  print_cut();
  print_string "for_each_cpu(cpu, ";
  print_string "&";
  print_string param; print_string "->"; print_string (B.id2c listv); print_string ") ";
  G.start_block ();
  print_string "if (per_cpu(core, cpu).";
  print_string (B.id2c var);
  print_string ")";
  G.print_nl_string (B.id2c var^" = true;");
  G.print_nl_string "break;";
  G.end_block true

(* --------------------------- lazy entry point ------------------------ *)
let is_terminal = function
    Ast.VAR _ | Ast.FIELD _ | Ast.INT _-> true
  | Ast.PRIM(Ast.COUNT _, [Ast.VAR _ as e], _)
      when B.ty (Ast.get_exp_attr e) = B.SET B.CORE -> true
  | _ -> false

let decl_var ty var =
  G.pp_type ty;
  (match ty with
    B.TIME | B.INT -> G.print_nl_string (" "^B.id2c var^" = 0;")
  (*
    When ty is bool, we must have 'false' as default for or.
    TODO: the default must be 'true' for and.
  *)
  | B.BOOL         -> G.print_nl_string (" "^B.id2c var^" = false;")
  )

let pp_lazy_sum_on_process_single first ty tyit var param fatt op =
  let Ast.FIELD(Ast.VAR(v, _, it_att),f, _, _) = op in
  begin
    let it_ty = B.ty it_att in
    match it_ty with
      B.SET(B.PROCESS) -> pp_lazy_sum_on_process first ty var param v f
    | B.QUEUE(B.PROCESS) -> pp_lazy_sum_on_process first ty var param v f
    | B.SET(B.CORE) -> pp_lazy_sum_on_core ty tyit var param v f
    | B.PROCESS ->
       G.print_nl_string ("if(per_cpu(state_info, "^
		       param ^"->id)."^
		       B.id2c v ^")");
      G.print_nl_string ("\t"^
			    B.id2c var ^ " += "^
			    "per_cpu(state_info, "^
			    param ^"->id)."^
			    B.id2c v ^"->"^ B.id2c f^";")
    | _ -> failwith (string_of_int (B.line fatt) ^": Unsupported type in lazy sum expression (TC: "^B.type2c it_ty^")")
  end

let rec pp_lazy_exp ty var tyit param exp =
  match exp with
  | Ast.PRIM(Ast.COUNT fatt, [op], _) ->
     let Ast.VAR(v, _, it_att) = op in
     let it_ty = B.ty it_att in
     begin
       match it_ty with
	 B.SET(B.PROCESS) -> pp_lazy_count_on_process ty var param v
       | B.QUEUE(B.PROCESS) -> pp_lazy_count_on_process ty var param v
       | B.SET(B.CORE) -> pp_lazy_count_on_core ty var param v
       | _ -> failwith (string_of_int (B.line fatt) ^": Unsupported type in lazy count expression (TC: "^B.type2c it_ty^")")
     end

  | Ast.PRIM(Ast.SUM fatt, ops, _) ->
     ignore(List.fold_left (fun first op ->
       pp_lazy_sum_on_process_single first ty tyit var param fatt op;
       print_cut ();
       false) true ops)

  | Ast.PRIM(Ast.MIN fatt, [op], _) ->
      (*
	TODO: Check if this min is the ordering criteria of process.
	If that's the case, optim: pick first of rbtree... :)
      *)
	let Ast.FIELD(Ast.VAR(v, _, it_att),f, _, _) = op in
	begin
	  let it_ty = B.ty it_att in
	  match it_ty with
	      B.SET(B.PROCESS) -> pp_lazy_min_on_process ty var param v f
	    | B.QUEUE(B.PROCESS) -> pp_lazy_min_on_process ty var param v f
	    | B.SET(B.CORE) -> pp_lazy_min_on_core ty tyit var param v f
	    | _ -> failwith (string_of_int (B.line fatt) ^": Unsupported type in lazy min expression (TC: "^B.type2c it_ty^")")
	end

    | Ast.PRIM(Ast.MAX fatt, [op], _) ->
      (*
	TODO: Check if this min is the ordering criteria of process.
	If that's the case, optim: pick first of rbtree... :)
      *)
	let Ast.FIELD(Ast.VAR(v, _, it_att),f, _, _) = op in
	begin
	  let it_ty = B.ty it_att in
	  match it_ty with
	      B.SET(B.PROCESS) -> pp_lazy_max_on_process ty var param v f
	    | B.QUEUE(B.PROCESS) -> pp_lazy_max_on_process ty var param v f
	    | B.SET(B.CORE) -> pp_lazy_max_on_core ty tyit var param v f
	    | _ -> failwith (string_of_int (B.line fatt) ^": Unsupported type in lazy max expression (TC: "^B.type2c it_ty^")")
	end

    | Ast.PRIM(Ast.FNOR fatt, [set;fld], _) ->
       let Ast.VAR(v, _, it_att) = set and Ast.VAR(f, _, f_att) = fld in
       begin
	 let it_ty = B.ty it_att in
         let fty = B.ty f_att in
	 match it_ty with
	      B.SET(B.PROCESS) -> pp_lazy_or_on_process ty var param v f
	    | B.QUEUE(B.PROCESS) -> pp_lazy_or_on_process ty var param v f
	    | B.SET(B.CORE) -> pp_lazy_or_on_core ty var param v f
            | B.QUEUE(B.CORE) -> pp_lazy_or_on_core ty var param v f
	    | _ -> failwith (string_of_int (B.line fatt) ^": Unsupported type in lazy or expression (TC: "^B.type2c it_ty^"->"^B.type2c fty^")")
	end

    | Ast.PRIM(fn, [op], fatt) ->
       let prepend = if !Error.debug then __LOC__ else "" in
       Pp.pretty_print_exp fn;
       failwith (prepend ^ string_of_int (B.line fatt) ^": Unsupported function in lazy")

    | Ast.VALID(exp, _, attr) ->
       begin
	 if is_terminal exp then
	   A.pp_exp (Compile_core.compile_exp4lazy param exp) (* FIXME *)
	 else
	   failwith (string_of_int (B.line attr) ^": Unsupported valid expression")
       end

    | Ast.BINARY (bop, exp1, exp2, attr) ->
       begin
	 match (is_terminal exp1, is_terminal exp2) with
	   (false, false) ->
	     begin
	       let vn = B.id2c var in
	       let var1 = B.fresh_idx vn
	       and var2 = B.fresh_idx vn in
	       decl_var ty var1;
	       decl_var ty var2;

	       pp_lazy_exp ty var1 tyit param exp1;
	       pp_lazy_exp ty var2 tyit param exp2;
	       print_string (B.id2c var ^" = " ^
			       B.id2c var1 ^
			       " "^ Ast.bop2c bop ^ " " ^
			       B.id2c var2)
	     end
	 | (true, false) ->
		     begin
	       let vn = B.id2c var in
	       let var2 = B.fresh_idx vn in
	       decl_var ty var2;

	       pp_lazy_exp ty var2 tyit param exp2;
	       print_string (B.id2c var ^" = ");
	       A.pp_exp (Compile_core.compile_exp4lazy param exp1);
	       G.print_nl_string (" "^ Ast.bop2c bop ^ " " ^
				     B.id2c var2)
	     end
	 | (false, true) ->
	     begin
	       let vn = B.id2c var in
	       let var1 = B.fresh_idx vn in
	       decl_var ty var1;

	       pp_lazy_exp ty var1 tyit param exp1;
	       print_string (B.id2c var ^" = " ^
			       B.id2c var1 ^
			       " "^ Ast.bop2c bop ^ " ");
	       A.pp_exp (Compile_core.compile_exp4lazy param exp2)
	     end
	 | (true, true) ->
	     begin
	       print_string (B.id2c var ^" = ");
	       A.pp_exp (Compile_core.compile_exp4lazy param exp1);
	       print_string (" "^ Ast.bop2c bop ^ " ");
	       A.pp_exp (Compile_core.compile_exp4lazy param exp2);
	     end
       end;
      G.pp_semic ()

    | Ast.TERNARY _ ->
       print_string (B.id2c var^" = ");
       A.pp_exp (Compile_core.compile_exp4lazy param exp);
       G.pp_semic ()

    | _ ->
       failwith (__LOC__ ^ ": " ^
		   string_of_int (B.line (Ast.get_exp_attr exp)) ^
		   ": Unsupported lazy expression")

let pp_lazy ty var tyit param exp =
  decl_var ty var;
  pp_lazy_exp ty var tyit param exp;
  print_string ("return "^B.id2c var^";")

let pp_lazy_vars name namety vars =
  G.print_between (fun _ -> ())
    (function x ->
      (match x with
	Ast.VARDECL(ty,id,import,true,_,Some dexp,attr) ->
	  if not (B.id2c id = "cload"
		 && name = "core") then
	    begin
	      open_vbox 0;
	      print_string "/* Inline function for lazy attr. ";
	      G.pp_struct_id id;
	      print_cut ();
	      A.pp_exp dexp;
	      print_cut ();
	      G.print_nl_string "*/";
	      let param = B.id2c (B.fresh_idx name) in
              print_string "static ";
              if not !debug then print_string "inline ";
	      G.pp_type ty;
	      print_string (" ipa_"^name^"_");
	      G.pp_struct_id id;
	      print_string "(const ";
	      print_string (G.pp_type_cs namety);
	      print_string (param^")");
	      G.start_top ();
	      pp_lazy ty id namety param dexp;
	      G.end_block true;
	      print_cut();
	      close_box ()
	    end
	  else ()
      |	_ -> ())
    )
    vars
