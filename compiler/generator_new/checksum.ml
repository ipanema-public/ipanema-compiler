(* Compute a checksum describing the parameters of all of the interface
functions.  Nothing protects this from overflow, but hopefully there are not
so many interface functions. *)

module B = Objects

let checksum_id id =
  let str = B.id2c id in
  let sum = ref 0 in
  String.iter (function c -> sum := Char.code c + (!sum)) str;
  !sum

let rec checksum_type = function
    B.ENUM(identifier) -> 1 + checksum_id identifier
  | B.RANGE(identifier) -> 2 + checksum_id identifier
  | B.SYSTEM(typ) -> 3 + checksum_type typ
  | B.INT -> 4
  | B.BOOL -> 5
  | B.TIME -> 6
  | B.CYCLES -> 7
  | B.PROCESS -> 8
  | B.SRCPROCESS -> 9
  | B.SCHEDULER -> 10
  | B.VOID -> 11
  | B.PEVENT -> 12
  | B.CEVENT -> 13
  | B.INDR(typ) -> 14 + checksum_type typ
  | B.STRUCT(identifier) -> 15 + checksum_id identifier
  | B.NULL -> 16
  | B.EMPTY -> 17
  | B.TIMER(B.NOT_PERSISTENT) -> 18
  | B.TIMER(B.PERSISTENT(Some id)) -> 19 + checksum_id id
  | B.TIMER(B.PERSISTENT(None)) -> 20
  | B.PORT -> 21
  | B.SCHEDULER_INST -> 22
  | B.CORE -> 23
  | B.DISTANCE -> 24
  | B.CLASS id -> 25 + checksum_id id
  | B.DOMAIN -> 26
  | B.GROUP -> 27
  | B.SET ty -> 28 + checksum_type ty
  | B.QUEUE ty -> 29 + checksum_type ty

let checksum_decl (Ast.VARDECL(ty,id,_,_,_,_,_)) =
  checksum_type ty + checksum_id id
    
let checksum_fundef (Ast.FUNDEF(tl,ty,nm,params,_,_,_)) index =
  checksum_type ty + (index * checksum_id nm) +
    (List.fold_left
       (function n -> function decl -> n + checksum_decl decl)
       0 params)

let checksum functions =
  let (n,index) =
    List.fold_left
      (function (n,index) ->
	function fundef -> (n + checksum_fundef fundef index,index+1))
      (0,1) functions in
  n
