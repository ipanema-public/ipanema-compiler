module B = Objects
module CS = Class_state
module G = Genaux
module A = Ast2c
module IPA = Ipanema

open Format
open Ioctl

(* ----------------------- Generation of stealing logic ------------------------ *)
let pp_filter_core (thief, stolen, stmt) =
  begin
    let Ast.VARDECL(_, thiefid, _, _, _, _, _) = thief in
    let Ast.VARDECL(_, stolenid, _, _, _, _, _) = stolen in
    A.self := B.id2c thiefid;
    open_vbox 0;
    G.mk_static_private ();
    print_string "bool can_steal_core(";
    open_hbox ();
    print_string "struct ipanema_policy *policy,";
    print_space ();
    G.pp_core_struct (); print_string (" *"^ !A.self ^",");
    print_space ();
    G.pp_core_struct (); print_string (" *"^B.id2c stolenid^")");
    close_box ();
    print_cut ();
    A.pp_top_seq false B.BOOL stmt;
    print_cut ();
    print_cut ();
    close_box ()
  end

let pp_select_core select =
  begin
    let (vd, exp1, select_exp) = select in
    let Ast.VARDECL(_, coresetid, _, _, _, _, _) = vd in
    let coreset = B.id2c coresetid in
    let core = B.fresh_idxstr "grp" in
    open_vbox 0;
    G.mk_static_private ();
    G.pp_core_struct ();
    print_string " *select_core(";
    open_hbox ();
    print_string "struct ipanema_policy *policy,"; print_space ();
    G.pp_group_struct (); print_space (); print_string ("*"^core^",");
    print_space ();
    print_string ("cpumask_var_t "^coreset^")");
    close_box ();
    print_cut ();
    A.pp_top_seq false (B.INDR B.CORE) select_exp;
    print_cut ();
    print_cut ();
    close_box ();
    coreset
  end

let pp_migrate srcg migrcond =
  begin
    let (stolen_migr, thief_migr, dst_group, p, migrstmt, stopexpr, updatestmt) = migrcond in
    let Ast.SEQ(upddecls, updstmts, _) = updatestmt in
    let Ast.VARDECL(_, stolen_migr_id, _, _,_,_,_) = stolen_migr
    and Ast.VARDECL(_, thiefid, _,_,_,_,_) = thief_migr
    and dstg = match dst_group with
	None -> B.fresh_idxstr "dstg"
      | Some Ast.VARDECL(_, groupid, _,_,_,_,_) -> B.id2c groupid
    and Ast.VARDECL(_, B.ID(p,_), _,_,_,_,_) = p in
    let stolen = B.id2c stolen_migr_id
    and thief = B.id2c thiefid in
    A.self := thief;
    open_vbox 0;
    G.mk_static_private (); print_string "int migrate_from_to(";
    open_hbox ();
    print_string "struct ipanema_policy *policy,";
    print_space ();
    G.pp_core_struct (); print_space (); print_string ("*"^stolen^",");
    print_space ();
    G.pp_core_struct (); print_space (); print_string ("*"^thief);
    (match srcg with
      Some srcg ->
	print_string ",";
	print_space ();
	G.pp_group_struct (); print_space (); print_string ("*"^srcg^",");
	print_space ();
	G.pp_group_struct (); print_space (); print_string ("*"^dstg)
    | None -> ()
    );
    print_string ")";
    close_box ();
    G.start_top ();
    G.print_nl_string "struct task_struct *pos, *n;";
    List.iter (fun ready ->
    G.print_nl_string ("LIST_HEAD("^B.id2c ready^"_tasks);")) !CS.ready_st;
    G.print_nl_string "struct sched_ipanema_entity * imd;";
    G.pp_process_struct (); print_string " *"; print_string p;G.pp_semic ();
    G.print_nl_string "int dbg_cpt = 0, ret;";
    G.print_nl_string "unsigned long flags;";
    if List.length upddecls > 1 then
      begin
        let (upddecls, upddefs) =
          List.fold_left (fun (upddecls, upddefs) d ->
              match d with
                Ast.VALDEF(decl,exp,isconst,attr) ->
                 let Ast.VARDECL(ty,id, _,_,_,_,_) = decl in
                 let decl = Ast.UNINITDEF(decl, attr)
                 and def = Ast.ASSIGN(Ast.VAR(id, [], B.updty attr ty), exp, false, B.updty attr ty) in
                 (decl::upddecls, def::upddefs)
              | _ -> (d::upddecls, upddefs)
            ) ([],[]) upddecls
        in
        List.iter (A.pp_def false) upddecls;
        print_cut ();
        G.print_nl_string ("ipanema_lock_core("^thief^"->id);");
        A.pp_seq2 (false, B.VOID) (Ast.mkSEQ([],upddefs, -1)) "";
        G.print_nl_string ("ipanema_unlock_core("^thief^"->id);");
      end
    else
      List.iter (A.pp_def false) upddecls;      
    print_cut ();
    G.print_nl_string "/* Remove tasks from busiest */";
    G.print_nl_string "local_irq_save(flags);";
    G.print_nl_string ("ipanema_lock_core("^stolen^"->id);");
    print_cut ();
    List.iter (fun readyid ->
        let ready = B.id2c readyid in
        G.print_nl_string ("// go through "^ready^ " rq");
      print_string "rbtree_postorder_for_each_entry_safe(pos, n, &";
      print_string (IPA.policy_si^"("^stolen^"->id)."^ready^".root,");
      print_string "ipanema.node_runqueue) ";
      G.start_block ();
      print_string p; G.print_nl_string " = policy_metadata(pos);";
      G.print_nl_string "if (pos->on_cpu)";
      G.print_nl_string "\tcontinue;";
      G.print_nl_string ("if (!cpumask_test_cpu("^thief^"->id, &pos->cpus_allowed))");
      G.print_nl_string "\tcontinue;";
      print_cut ();
      G.print_nl_string "/* Migration statement from the policy */";
      A.pp_seq2 (false, B.VOID) migrstmt "";
      print_cut ();
      G.print_nl_string "/* Ensure migration cond. and stop cond. use the same ids ! */";
      print_string "if (";
      A.pp_exp stopexpr;
      G.print_nl_string ") ";
      print_string "\tgoto unlock_busiest;";
      G.end_block true;
      print_cut ()
    ) !CS.ready_st;
    G.print_nl_string "\nunlock_busiest:";
    print_string ("ipanema_unlock_core("^stolen^"->id);");
    print_cut ();
    G.print_nl_string "ret = dbg_cpt;";
    print_cut ();
    G.print_nl_string "/* Add them to my queue */";
    G.print_nl_string ("ipanema_lock_core("^thief^"->id);");
    List.iter (fun ready ->
      let rt = B.id2c ready^"_tasks" in
      print_string ("while (!list_empty(&"^ rt^")) ");
      G.start_block ();
      G.print_nl_string ("imd = list_first_entry(&"^rt^", struct sched_ipanema_entity, ipa_tasks);");
      G.print_nl_string "pos = container_of(imd, struct task_struct, ipanema);";
      G.print_nl_string (""^p^" = policy_metadata(pos);");
      G.print_nl_string ("ipa_change_queue("^p^", &"^IPA.policy_si^"("^thief^"->id)."^
			    B.id2c ready^", "^
			    "IPANEMA_READY,"^thief^"->id);");
      G.print_nl_string "smp_wmb();";
      G.print_nl_string (thief^"->cload += "^p ^"->load;");
      G.print_nl_string "smp_wmb();";
      G.print_nl_string "list_del_init(&imd->ipa_tasks);";
      G.print_nl_string "dbg_cpt--;";
      G.end_block true) !CS.ready_st;
    G.print_between print_cut (A.pp_stmt (false, B.VOID)) updstmts;
    print_cut ();
    G.print_nl_string ("ipanema_unlock_core("^thief^"->id);");
    G.print_nl_string "local_irq_restore(flags);";
    print_cut ();
    G.print_nl_string ("if (dbg_cpt != 0)");
    G.print_nl_string ("\tpr_info(\"%s: Some tasks (%d) were lost on a migration from %d to %d\\n\",");
    G.print_nl_string ("\t\t__func__, dbg_cpt, "^stolen^"->id, "^thief^"->id);");
    print_cut ();
    print_string "return ret;";
    G.end_block true;
    print_cut ();
    stolen
  end

let pp_filter_grp (thief, stolen, stmt) =
  begin
    let Ast.VARDECL(_, thiefid, _, _, _, _, _) = thief in
    let Ast.VARDECL(_, stolenid, _, _, _, _, _) = stolen in
    A.self := B.id2c thiefid;
    open_vbox 0;
    G.mk_static_private ();
    print_string "bool can_steal_group(";
    open_hbox ();
    print_string "struct ipanema_policy *policy,";
    print_space ();
    G.pp_group_struct (); print_string (" *"^B.id2c stolenid^",");
    print_space ();
    G.pp_group_struct (); print_string (" *"^ !A.self ^")");
    close_box ();
    print_cut ();
    A.pp_top_seq false B.BOOL stmt;
    print_cut ();
    print_cut ();
    close_box ()
  end

let pp_select_grp select =
  begin
    let (vd, vg, exp1, select_exp) = select in
    let Ast.VARDECL(_, domid, _, _, _, _, _) = vd
    and Ast.VARDECL(_, groupsetid, _, _, _, _, _) = vg in
    let groupset = B.id2c groupsetid
    and dom = B.id2c domid in
    open_vbox 0;
    G.mk_static_private ();
    G.pp_group_struct ();
    print_string " *select_group(";
    open_hbox ();
    print_string "struct ipanema_policy *policy,";
    print_space ();
    G.pp_domain_struct (); print_space (); print_string ("*"^dom^",");
    print_space ();
    print_string ("unsigned long *"^groupset^")");
    close_box ();
    print_cut ();
    A.pp_top_seq false (B.INDR B.GROUP) select_exp;
    print_cut ();
    print_cut ();
    close_box ();
    groupset
  end

let get_steal_blk steal =
  match steal with
    Ast.FLAT_STEAL_THREAD st -> (None, st)
  | Ast.ITER_STEAL_THREAD (st, until, post) -> (Some (until, post),st)

let pp_steal_for_dom activeid domlist (dom, dstg, dst, steal) =
  let (iterd, sg_, (iterg_until, (filter, select, migrcond)), postgrp) = match steal with
      Ast.STEAL_THREAD s -> (None, None, get_steal_blk s, [])
    | Ast.ITER_STEAL_GROUP (sg, st, post, postgrp) -> (Some post, Some sg, get_steal_blk st, postgrp)
    | Ast.FLAT_STEAL_GROUP (sg, st, postgrp) -> (None, Some sg, get_steal_blk st, postgrp)
  in
  let (groups, cores) =
    let rec found_bind = function
    Ast.DOMFOR (id, off_opt, stmts) -> found_bind (List.hd stmts)
      | Ast.DOMDOM (id, expr, bind_var, grp) ->
	 let (_, _ , _, cores) = grp in
	 (bind_var, cores)
    in found_bind (List.hd domlist)
  in
  let dst = if dst = "" then B.fresh_idxstr "core" else dst in
  let coreid = B.mkId dst in
  let core = B.id2c coreid in
  let (srcg, sg_cond) =
    match sg_ with
      None -> (None, None)
    | Some (filter, select, cond) ->
       begin
	 pp_filter_grp filter;
	 let grp = pp_select_grp select in
	 (Some grp, Some cond)
       end
  in
  let selected = pp_migrate srcg migrcond in
  let srcgid = match srcg with
      None -> B.fresh_idxstr "srcg"
    | Some id -> id
  in
  pp_filter_core filter;
  let coreset = pp_select_core select in
  open_vbox 0;
  G.mk_static_private (); print_string "void steal_for_dom(";
  open_hbox ();
  print_string "struct ipanema_policy *policy,";
  print_space ();
  G.pp_core_struct (); print_space (); print_string ("*"^core^",");
  print_space ();
  G.pp_domain_struct (); print_space (); print_string ("*"^dom^")");
  close_box ();
  G.start_top ();
  if sg_cond <> None then
    G.print_nl_string ("DECLARE_BITMAP(stealable_groups, "^dom^"->___sched_group_idx);");
  G.print_nl_string ("cpumask_var_t "^ coreset ^";");
  G.pp_core_struct (); G.print_nl_string (" *"^selected^", *c;");
  if sg_cond <> None then
    (G.pp_group_struct (); G.print_nl_string (" *sg, *"^dstg^" = NULL, *"^srcgid^";"));
  G.print_nl_string "int i;";
  print_cut ();
  if !IPA.debug then G.print_nl_string "pr_info(\"%s: steal_for_dom entry\", __func__);";
  print_cut ();
  G.print_nl_string "/* Init bitmap */";
  if sg_cond <> None then
    G.print_nl_string ("bitmap_zero(stealable_groups, "^dom^"->___sched_group_idx);");
    G.print_nl_string ("if (!zalloc_cpumask_var(&"^coreset^", GFP_KERNEL)) ");
  G.print_nl_string "\treturn;";
  (*  G.print_nl_string ("cpumask_clear("^coreset^");");*)
  print_cut ();
  if sg_cond <> None then
    begin
      G.print_nl_string ("/* Find group containing  "^core^" */");
      print_string ("for (i = 0 ; i < "^dom^"->___sched_group_idx ; i++) ");
      begin
	G.start_block ();
	G.print_nl_string ("sg = "^dom^"->groups + i;");
	print_string ("if (cpumask_test_cpu("^core^"->id, &sg->cores)) ");
	G.start_block ();
	G.print_nl_string (dstg^" = sg;");
	print_string "break;";
	G.end_block false;
	G.end_block true;
      end;
      print_cut ();
      G.print_nl_string "/* Step 1: can_steal_group */";
      if !IPA.debug then G.print_nl_string "pr_info(\"%s: step 1\", __func__);";
      print_string ("for (i = 0 ; i < "^dom^"->___sched_group_idx ; i++) ");
      begin
	G.start_block ();
	G.print_nl_string ("sg = "^dom^"->groups + i;");
	G.print_nl_string ("if (sg == "^dstg^")");
	G.print_nl_string "\tcontinue;";
	G.print_nl_string ("if (can_steal_group(policy, sg, "^dstg^"))");
	G.print_nl_string "\tbitmap_set(stealable_groups, i, 1);";
	G.end_block true;
      end;
      G.print_nl_string ("if (bitmap_empty(stealable_groups, "^dom^"->___sched_group_idx))");
      G.print_nl_string "\tgoto free_mem;";
      print_cut ();
      begin
	match sg_cond with
	  Some until ->
	    begin
	      G.print_nl_string "/* Iterate on steps 2 to 5 until all groups were tried */";
              if !IPA.debug then G.print_nl_string "pr_info(\"%s: iterate step 2-5\", __func__);";
	      print_string "while (!(";
	      A.pp_exp until;
	      print_string ")) ";
	      G.start_block ();
	    end
	| None ->
	   G.print_nl_string "/* Do not iterate on steps 2 to 5 */";
      end;
      begin
	G.print_nl_string "/* Step 2: select_group */";
        if !IPA.debug then G.print_nl_string "pr_info(\"step 2\");";
	G.print_nl_string (srcgid^" = select_group(policy, "^dom^", stealable_groups);");
	G.print_nl_string ("if (!"^srcgid^")");
	G.print_nl_string "\tgoto free_mem;";
      end;
    end (* End of if sg_cond *)
  else
    (G.print_nl_string "/* No steps 1 and 2 - no group handling */";
     if !IPA.debug then G.print_nl_string "pr_info(\"%s: no step 1 & 2\", __func__);");
  print_cut ();
  G.print_nl_string "/* Step 3: can_steal_core */";
  if !IPA.debug then G.print_nl_string "pr_info(\"%s: step 3\", __func__);";
  if sg_cond <> None then
    print_string ("for_each_cpu(i, &"^srcgid^"->cores) ")
  else
    print_string ("for_each_possible_cpu(i) ");
  begin
    G.start_block ();
    G.print_nl_string "c = &ipanema_core(i);";
    G.print_nl_string ("if(c == "^core^")");
    G.print_nl_string "\tcontinue;";
    G.print_nl_string ("if (can_steal_core(policy, "^core^", c))");
    print_string ("\tcpumask_set_cpu(i, "^coreset^");");
    G.end_block true;
  end;
  G.print_nl_string ("if (cpumask_empty("^coreset^"))");
  if sg_cond <> None
  then if iterd <> None then G.print_nl_string "\tgoto fwd_next_group;"
    else G.print_nl_string "\tcontinue;"
  else G.print_nl_string "\tgoto free_mem;";
  print_cut ();
  (match iterg_until with
    Some (until, post) ->
      G.print_nl_string "/* Iterate on steps 4 and 5 until all cores were tried */";
      if !IPA.debug then G.print_nl_string "pr_info(\"%s: iterate step 4 & 5\", __func__);";
      print_string ("while (!");
      open_hbox ();
      print_string ("(");
      A.pp_exp until;
      print_string "))";
      close_box ();
      G.start_block ();
  | None ->
     G.print_nl_string "/* Do not iterate on steps 4 and 5 */";
  );
  begin
    G.print_nl_string "/* Step 4: select_core */";
    if !IPA.debug then G.print_nl_string "pr_info(\"%s: step 4\", __func__);";
    if sg_cond <> None then
      G.print_nl_string (selected^" = select_core(policy, "^srcgid^", "^coreset^");")
    else
      G.print_nl_string (selected^" = select_core(policy, NULL, "^coreset^");");
    G.print_nl_string ("if (!"^selected^")");
    if sg_cond <> None
    then if iterd <> None then G.print_nl_string "\tgoto fwd_next_group;"
      else G.print_nl_string "\tcontinue;"
    else G.print_nl_string "\tgoto free_mem;";
    print_cut ();
    G.print_nl_string "/* Step 5: steal_thread */";
    if srcg = None then
      print_string ("migrate_from_to(policy, "^selected^", "^core^");")
    else
      print_string ("migrate_from_to(policy, "^selected^", "^core^", "^srcgid^", "^dstg^");");
    print_cut ();
    (match iterg_until with
      Some (_, post) ->
	print_cut ();
	print_string "/* Post operations in core iteration */";
	List.iter (A.pp_stmt (false, B.VOID)) post;
	G.end_block true
    | None -> ()
    );
  end;
  if sg_cond <> None then
    begin
      (match iterd with
	Some post ->
	  G.print_nl_string "\nfwd_next_group:";
	  print_string "/* Post operations in group iteration */";
	  if post <> [] then
            List.iter (A.pp_stmt (false, B.VOID)) post
          else
            G.print_nl_string ";";
       | None -> print_cut ()
      );
      G.end_block true
    end;
  G.print_nl_string "\nfree_mem:";
  if sg_cond <> None then
    begin
      G.print_nl_string ("spin_lock(&"^dom^"->lock);");
      G.print_nl_string (""^dom^"->count++;");
      if postgrp <> [] then
        List.iter (A.pp_stmt (false, B.VOID)) postgrp;
      G.print_nl_string ("spin_unlock(&"^dom^"->lock);")
    end;
  G.print_nl_string ("free_cpumask_var("^coreset^");");
  G.end_block true;
  print_newline ();
  close_box ()

let pp_steal_for activeid x doms steal =
  let Some(_, domvar, domlist) = doms in
  pp_steal_for_dom activeid domlist steal;
  if Check_prim.check_for "steal_for" x then
    begin
      open_vbox 0;
      G.mk_static_private (); print_string "void steal_for(";
      open_hbox ();
      print_string "struct ipanema_policy *policy,";
      print_space ();
      G.pp_core_struct ();
      print_space ();
      print_string "*core)";
      close_box ();
      G.start_top ();
      G.print_nl_string ("\tif (core->"^domvar^")");
      print_string ("\t\tsteal_for_dom(policy, core, core->"^domvar^");");
      G.end_block true;
      print_newline ();
      close_box ()
    end

let pp_steal_fcts activeid x doms = function
    None -> ()
  | Some steal ->
     pp_steal_for activeid x doms steal
