module B = Objects

type av =
  Field
| Value of float

let merge_float tload1 tload2 =
  match (tload1, tload2) with
(*    (Pervasives.neg_infinity, Pervasives.neg_infinity) -> Pervasives.neg_infinity
  | (infinity, infinity) -> infinity
  | (infinity, neg_infinity) -> nan
  | (neg_infinity, infinity) -> nan*)
  | (nan, _) -> nan
  | (_, nan) -> nan
  | (tload1, tload2) when tload1 = tload2 -> tload1
  | (tload1, tload2) when tload1 >= 0.0 && tload2 >= 0.0 -> infinity
  | (tload1, tload2) when tload1 <= 0.0 && tload2 <= 0.0 -> neg_infinity
  | (tload1, tload2) when tload1 <= 0.0 && tload2 >= 0.0 -> nan
  | (tload1, tload2) when tload1 >= 0.0 && tload2 <= 0.0 -> nan

let merge_type tload1 tload2 =
  match tload1, tload2 with
    Field, Field -> Field
  | Field, Value _ -> tload2
  | Value _, Field -> tload1
  | Value v1, Value v2 -> Value (merge_float v1 v2)

let merge tload1 tload2 =
  match tload1, tload2 with
    None, None -> None
  | None, Some _ -> tload2
  | Some _, None -> tload1
  | Some tload1, Some tload2 -> Some (merge_type tload1 tload2)

(* FIXME *)
let rec tload_exp tload = function
    _ -> None
           (*
    Ast.INT (n, _) -> Some (Value (float_of_int n))
  | Ast.SELF _
  | Ast.SUM _
  | Ast.MIN _
  | Ast.MAX _
  | Ast.COUNT _
  | Ast.FNOR _
  | Ast.DISTANCE _
  | Ast.TYPE _
  | Ast.BOOL _
  | Ast.UNARY _
  | Ast.INDR _
  | Ast.SELECT _
  | Ast.FIRST _
  | Ast.EMPTY _
  | Ast.VALID _
  | Ast.SCHEDCHILD _
  | Ast.SRCONSCHED _
  | Ast.ALIVE _
  | Ast.AREF _
  | Ast.IN _
  | Ast.MOVEFWDEXP _
  | Ast.PARENT _
  | Ast.INT_STRING _
  | Ast.INT_NULL _
  | Ast.VAR(_,_,_) -> tload
  | Ast.FIELD(exp,fld,_,_)
       when B.id2c fld = "load"
            && B.ty (Ast.get_exp_attr exp) = B.PROCESS -> Some Field
  | Ast.FIELD(exp,fld,_,_) -> (*FIXME *) tload
  | Ast.CAST(_, exp,_) -> tload_exp tload exp
  | Ast.LCORE(obj, exp, _,_) -> tload
  | Ast.BINARY(op, exp1, exp2, attr) ->
     let tload = tload_exp tload exp1 in
     tload_exp tload exp2
  | Ast.TERNARY(exp1, exp2, exp3, _) ->
     let tload = tload_exp tload exp1 in
     let tload = tload_exp tload exp2 in
     tload_exp tload exp3
  | Ast.PRIM(f, args, attr) ->
     let tload =
       List.fold_left (fun tload e ->
           match tload_exp tload e with
             None -> None
           | Some Field -> tload
           | Some (Value _) as v -> merge tload v) tload args
     in
     merge tload
     *)
     
(* FIXME *)
let rec tload_stmt tload = function
    Ast.IF(exp, stmt1, stmt2, _) -> tload
  | Ast.FOR(id,state_id,states,dir,stmt,crit, attr) -> tload
  | Ast.FOR_WRAPPER _ -> tload
  | Ast.SWITCH(exp,cases,default,attr) -> tload
  | Ast.SEQ(decls,stmts,attr) ->
     List.fold_left tload_stmt tload stmts
     
  | Ast.RETURN(None, attr) -> tload
                            
  | Ast.RETURN(Some exp, _)
  | Ast.STEAL(exp,_) -> tload_exp tload exp

  | Ast.MOVE(exp,state,_,_,dst,_,_,attr) -> tload
  | Ast.MOVEFWD(exp,_,_,attr) -> tload
  | Ast.SAFEMOVEFWD(exp,_,_,_,attr) -> tload

  | Ast.ASSIGN(expl,expr,sorted_fld,attr) ->
     let tloadl = tload_exp None expl in
     let tloadr = tload_exp tload expr in
     merge tloadl tloadr

  | Ast.DEFER _ -> tload
  | Ast.BREAK _ -> tload
  | Ast.CONTINUE _ -> tload
  | Ast.PRIMSTMT(_,args,attr) ->
     List.fold_left (fun tload e ->
         match tload_exp tload e with
           None -> None
         | Some Field -> tload
         | Some (Value _) as v -> merge tload v) tload args

  | Ast.ERROR _ -> tload
  | Ast.ASSERT (exp, _) -> tload_exp tload exp

let tload_function tload = function
    Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr) -> tload_stmt tload stmt
                                                    
let tload_handler tload = function
    Ast.EVENT(_,_,stmt,_,_) -> tload_stmt tload stmt

let tload handlers chandlers ifunctions functions =
  let tload = List.fold_left tload_handler None handlers in
  let tload = List.fold_left tload_handler tload chandlers in
  let tload = List.fold_left tload_function tload ifunctions in
  let tload = List.fold_left tload_function tload functions in
  match tload with
    None | Some Field -> 0.0
  | Some (Value v) -> v
