(*
Voici le new de Weak-WC sur le wiki

    On new {
	    thread p = e.target;

	    p.vruntime = ticks_to_time(0);
	    p.quanta = 0;
	    p.priority = 1;
	    p.load = 0;

	    e.target => ready;
    }

Je veux:

let new (cores:cores) (tgt:thread) =
  requires {valid_cores cores}
  ensures {valid_cores cores}
  let p = tgt in
  p.vruntime <- ticks_to_time(0);  // <- parce que c'est un champ, sinon :=
  p.quanta <- 0;
  p.priority <- 1;
  p.load <- 0;
  move(cores,tgt,cores.ready[coreof tgt])

Il faudrait que je reflechisse pour les foreach, alors tu peu just les
afficher comme ils sont.

Il faut aussi generer du code pour les functions (eg update_thread).

Ne fais rien pour le steal.

Pour les threads, on peut generer un structure:

  thread = {
    int quanta;
    time vruntime;	// time spent running on CPUs
    int priority;	  // niceness
    int load;		  // "load" metric for load balancing
  }

devient:

type time = int
type thread = { quanta : int; vruntime : time; priority : int;
                load : int }

Pareil pour les core (type core = { ... }).  Pour l'instant, prends juste
les attributs avec type int ou time.

Pour threads et domains, on peut laisser tomber pour l'instant.

S'il y a quelquechose que je n'ai pas pris en compte, peut etre que tu peux
juste le mettre sous commentaires, ie (* *).

Si ca prend beaucoup trop de ton temps, n'hesite pas a le signaller.  Ca
peut aussi etre fait a la main, mais ca semble un peu penible...

*)

module B = Objects
module CS = Class_state
module G = Genaux

open Format

let compile_decl decl =
  let Ast.VARDECL(ty, id, _, _, _, _, _) = decl in
  if ty = B.INT
    || ty = B.TIME
    || ty = B.BOOL then
    begin
      print_string (B.id2c id);
      print_string " : ";
      G.print_nl_string (B.type2c ty)
    end

let compile_thread tvs =
     open_vbox 2;
     G.print_nl_string "type thread = {";
     List.iter  compile_decl tvs;
     close_box ();
     G.print_nl_string "}"

let compile_core core =
  match core with
    None -> ()
  | Some cvs ->
     open_vbox 2;
     G.print_nl_string "type core = {";
     List.iter  compile_decl cvs;
     close_box ();
     G.print_nl_string "}"

let rec compile_expr expr =
  match expr with
    Ast.INT (v, _) -> print_string (string_of_int v)
  | Ast.SELF _ -> print_string "self"
    | Ast.VAR(id, _, _) ->
	if B.id2c id = "NULL" then
	  print_string "None"
	else print_string (B.id2c id)
    | Ast.FIELD(e, id, _, a) ->
      if B.ty a = B.PROCESS && B.id2c id = "target" then
	print_string "tgt"
      else
	begin
	  compile_expr e;
	  print_string "->";
	  print_string (B.id2c id)
	end
    | Ast.UNARY(op, e, _) ->
       (match op with
	 NOT -> print_string "!"
       | COMPLEMENT -> print_string "~"
       ); compile_expr e;
    | Ast.BINARY(op, e1, e2, _) ->
      compile_expr e1;
      print_string (" "^(Ast.bop2c op)^" ");
      compile_expr e2
    | Ast.VALID(e,_, _) ->
	print_string "valid(";
	compile_expr e;
	print_string ")"
    | Ast.FIRST(e, crit, _, _) ->
	print_string "first(";
	compile_expr e;
	print_string ")"
    | Ast.PRIM(fn, params, _) ->
      compile_expr fn;
      print_string "(";
      let bt = function _ ->
	print_string ","
      in
      Genaux.print_between bt compile_expr params;
      print_string ")"
    | Ast. SELF _ -> print_string "self"
    | Ast.IN(e1, e2, _, _, _, _, attr) ->
       compile_expr e1;
      print_string (" in ");
      compile_expr e2
    | _ -> Pp.pretty_print_exp expr

let assign e1 e2 =
  print_string "let ";
  compile_expr e1;
  print_string " = ";
  compile_expr e2;
  G.print_nl_string " in"
    
let compile_def def =
  match def with
      Ast.VALDEF(Ast.VARDECL(ty, nm, _,_, _, e_, a), e, _, _) ->
	if ty = B.TIME || ty = B.INT || ty = B.CORE then
	  assign (Ast.VAR(nm, [], a)) e
  | _ -> ()

let rec compile_stmt stmt =
  match stmt with
      Ast.SEQ(decls, stmts, _) ->
	open_vbox 0;
	List.iter compile_def decls;
	open_vbox 2;
	List.iter compile_stmt stmts;
	close_box ();
	close_box ()
    | Ast.FOR(id, states, _, _, stmt, crit, _) ->
      print_string (B.id2c id);
      compile_stmt stmt
    | Ast.MOVE (src, dst, _, _, _, _, _, _) ->
      let cplsrc = fun _ -> compile_expr src in
      let (pre, dststr, post) = match dst with
	  Ast.VAR (id, _, _) ->
	    (let f = fun _ -> () in f, B.id2c id ^ "[coreof ", cplsrc)
	| Ast.FIELD(e, id, _, _) ->
	  let cpl_e = fun _ -> compile_expr e in
	  (cplsrc, B.id2c id ^"[", cpl_e)
      in
      print_string "move(cores,";
      compile_expr src;
      print_string ", cores.";
      pre ();
      print_string dststr;
      post ();
      G.print_nl_string "])"
    | Ast.ASSIGN (left, right, _, _) ->
      assign left right
    | Ast.RETURN (e_, _) ->
      print_string "return ";
      ignore(Aux.app_option compile_expr e_);
      print_cut ()
    | Ast.PRIMSTMT (fn, params, _) ->
      compile_expr fn;
      print_string "(";
      let bt = function _ ->
	print_string ","
      in
      Genaux.print_between bt compile_expr params;
      G.print_nl_string ")"
    | Ast.IF(e, s1, s2, _) ->
      print_string "if ";
      compile_expr e;
      G.print_nl_string " then";
      compile_stmt s1;
      G.print_nl_string " else ";
      compile_stmt s2
    | Ast.BREAK _ -> G.print_nl_string "break"
    | _ -> let a = Ast.get_stm_attr stmt in
	   failwith (__LOC__ ^": "^string_of_int (B.line a))

let compile_handler hdl =
  let Ast.EVENT(Ast.EVENT_NAME(nm,_), params, stmt, _, attr) = hdl in
  open_vbox 0;
  open_vbox 2;
  print_string "let ";
  print_string (B.id2c nm);
  G.print_nl_string " (cores: cores) (tgt:thread) =";
  G.print_nl_string "requires {valid_cores cores}";
  G.print_nl_string "ensures {valid_cores cores}";
  compile_stmt stmt;
  close_box ();
  print_cut ();
  close_box ()

let policy_valid_cores rdq tload =
  G.print_nl_string ("predicate policy_valid_cores (cid:Array.array int) (cload:Array.array int) (current:astate) (ready:mstate) (blocked:astate) =
  mcols ready = "^rdq^" && forall t:thread. load t "^tload)

let count_rdq states =
  string_of_int
    (List.fold_left (fun c s ->
         match s with
           Ast.QUEUE (CS.READY, _,_,_,_,_,_)
         | Ast.PROCESS(CS.READY, _, _,_,_,_) -> c+1
         | _ -> c
       ) 0 states)

let to_why3 dirname
    ((Ast.SCHEDULER(nm,cstdefs,enums, dom, grp, procdecls,
		    fundecls,valdefs,domains,states_,cstates,
		    criteria,trace,handlers,chandlers,ifunctions,functions,
		    attr)) as x) : unit =
  let (states, corev_, steal_) = match states_ with
      Ast.CORE(cv,st, steal) -> (st, Some cv, steal)
    | Ast.PSTATE(st) -> (st, None, None)
  in
  Bossa2c.mkdir_if_necessary dirname;
  let file_ext = ".why3" in
  let ch = open_out (dirname^"/"^nm^file_ext) in
  set_formatter_out_channel ch;
  set_margin 80;
  G.policy_name := nm;
  open_vbox 0;
  G.print_nl_string ("(* Generated Why3 file - "^nm^" *)");
  print_cut ();
  G.print_nl_string ("module "^ nm ^"
use int.Int
use ref.Ref
use list.List
use array.Array
use my_matrix.Matrix
use boolvec2.Boolvec
use astate.Astate
use mstate.Mstate

type time = int");
  print_cut ();
  compile_thread procdecls;
  print_cut ();
  (* FIXME: Constant parameter *)
  let tload = Why3Aux.tload handlers chandlers ifunctions functions in
  let tloads =
    if tload = neg_infinity then "< 0"
    else if tload = infinity || tload = nan then "> 0"
    else "= "^ string_of_float tload
  in
  policy_valid_cores (count_rdq states) tloads;
  print_cut ();
  compile_core corev_;
  print_cut ();
  List.iter compile_handler handlers;
  close_box ()
