(*************************************************************************)
(*                                                                       *)
(*                               Bossa                                   *)
(*                                                                       *)
(*              Julia, Dan , projet Compose, INRIA Rennes                *)
(*                                                                       *)
(*   Copyright (C) 2000 INRIA. All rights reserved.                      *)
(*   Institut National de Recherche en Informatique et en Automatique.   *)
(*                                                                       *)
(*************************************************************************)

(* $Id$ *)

(*open Devilast*)
open Lexing
open Format
open Bossa_lexer	(* to get cr_index and lineno *)
open List

let input_name = ref ([] : string list)	(* no filename by default *)
let nocode = ref false
let dirname = ref "c-code"  (* default output directory *)
let genname = ref None      (* default generated code name *)

exception  Err of string ;;


let fatal_error s = Printf.eprintf "%s: %s\n"
    (Filename.basename Sys.argv.(0)) s;
  exit 2


let version =
  "This Software is (C) copyright INRIA, LIG, LIP6, EPFL, I3S - 2016.\n\
   Alpha 0.0.1\n"


let print_version () =
  print_string version;
  exit 0


let print_pos fname line colfrom colto =
  sprintf "File \"%s\", line %d, characters xxx %d-%d:\n"
    fname line colfrom colto


let pr x y = Printf.sprintf "%s.%s" x y

(* decorator used in the AST operations to provide error handling*)
let check_errors name f =
  Error.init_error();
  let res = f () in
  Error.check_errors(name);
  print_endline "ok";
  res

(* Operations on the AST *)

let transform ast f transform_name abbrev =
  check_errors transform_name ( fun _ ->
                 Printf.printf "%s - %!" transform_name;
                 let transformed = f ast in
                 Pp.pretty_print true (pr !Error.output_name abbrev) transformed;
                 (* Error.check_errors(transform_name); *)
                 transformed
               )

let transform2 ast f transform_name abbrev =
  check_errors transform_name (fun _ ->
                 Printf.printf "%s - %!" transform_name;
                 let transformed = f ast in
		 let (_, _, expanded) = transformed in
                 Pp.pretty_print true (pr !Error.output_name abbrev) expanded;
                 transformed
               )

let generator ast f generation_name =
  ignore (check_errors generation_name (fun _ ->
                 Printf.printf "%s - %!" generation_name;
                 f ast
               ))

let with_file real_input_name f =
  let ch = open_in real_input_name in
  let res = f ch in
  close_in ch;
  res

(* Parsing *)
let parse_from ch ch_name =
  let lexbuf = Lexing.from_channel ch in
  (try
     Misc.init ch_name;
     Bossa_lexer.init();
     printf "Parsing - %!";
     let ast = Bossa_parser.bossa_spec Bossa_lexer.token lexbuf in
     print_endline "ok";
     ast
   with x ->
       (print_endline "ko";
	Parser_error.report_error lexbuf Format.err_formatter x;
	exit 2))

let parse_bossa real_input_name =
  printf "Bossa file: %s\n%!" real_input_name;
  with_file real_input_name
    (fun ch -> parse_from ch (Filename.basename real_input_name))

(* Components of the compilation pipeline *)

let build_ast () =
  match !input_name with
    [] -> parse_from stdin "stdin"
  | [real_input_name] -> parse_bossa real_input_name
  | _ -> failwith "Modular Bossa no longer supported"

let type_check ast =
  transform ast Type.type_check "Type checking" "tc"

let verify type_checked =
  transform type_checked Verifier_new.verify "Verifying" "vr"

let compile (verified : Ast.scheduler) : Ast.scheduler * Ast.scheduler * Ast.scheduler  =
  transform2 verified Compile.compile "Postprocessing" "end"

let generate_c compiled : unit =
  (*FIXME !genname shouldn't use !input_name b/c it breaks generator
                              if Error.any_errors()
                              then
                              (match !genname with
                              Some x -> Some ("error_"^x)
                              | _ -> Some ("error_"^(List.hd !input_name)))
                              else !genname in *)
  (*if (!Objects.hls)
    then Bossa2c_hls.bcpp !dirname !genname compiled
    else*)
  let gen_makefile = function Ast.SCHEDULER(nm,cstdefs,enums, dom, grp, procdecls,
		    fundecls,valdefs,domains,states_,cstates,
		    criteria,trace,handlers,chandlers,ifunctions,functions,
		attr) ->
    Bossa2c.mkdir_if_necessary !dirname;
    let ch = open_out (!dirname^"/"^nm^".mk") in
    set_formatter_out_channel ch;
    set_margin 80;
    print_string "
obj-m += ";
    print_string nm;
    print_string ".o";
    print_newline ();
    print_flush ();
    close_out ch;
    let ch = open_out (!dirname^"/Makefile") in
    set_formatter_out_channel ch;
    set_margin 80;
    print_string "
MODULES_C=$(wildcard *.c)
MODULES=$(MODULES_C:%.c=%.ko)

-include $(PWD)/*.mk
KERNELRELEASE?=$(shell uname -r)
KERNEL?=/lib/modules/$(KERNELRELEASE)/build
UBSAN_SANITIZE := y

-include Makefile.override

$(MODULES):
	make -C $(KERNEL) M=$(PWD) $@

all:
	make -C $(KERNEL) M=$(PWD) modules

clean:
	make -C $(KERNEL) M=$(PWD) clean

install:
	rsync -avcz *.ko /lib/modules/$(KERNELRELEASE)/kernel/kernel/sched/ipanema/
	depmod

distclean: clean
	rm -f *.c *.why3 *.scala *.log *~";
    print_newline ();
    print_flush ();
    close_out ch;
    set_formatter_out_channel stdout;
  in
  generator compiled gen_makefile "Generating Makefile";
  generator compiled (Bossa2c.bcpp !dirname) "Generating C code"

let generate_leon compiled : unit =
  generator compiled (Bossa2Leon.bcpp !dirname) "Generating Leon code"

let generate_leonstate compiled : unit =
  generator compiled (Bossa2LeonState.bcpp !dirname) "Generating Leon state code"

let gen_why3 compiled : unit =
  generator compiled (Why3.to_why3 !dirname) "Generating Why3 code"

(* the 'pipeline' defines how the components are wired together to produce the
   desired result*)
let pipeline build_ast type_check verify compile generate_c generate_leon gen_leonstate =
  Objects.init_id();
  let (ast,ty) = build_ast() in
  Pp.pretty_print true (pr !Error.output_name "pp") ast;
  let type_checked = type_check ast in
  let asts =
    (if !Objects.mutate then Mutate_states.mutate type_checked
     else type_checked::[]) in
  List.iter (fun ast ->
      let (why3, leon, ast) = compile (verify ast) in
      generate_c ast;
      generate_leon leon;
      gen_leonstate leon;
      gen_why3 why3
    ) asts

(* Parsing of command-line arguments *)

exception Found_program_name

let set_output s = Error.output_name := s

let anonymous s = input_name := s :: !input_name

let set_dir s = dirname := s
let set_gen s = genname := Some s

let speclist =
  [ (*"-csharp", Arg.Set Objects.csharp,
      "The code will be generated in C#, not in C";

      "-hls", Arg.Set Objects.hls,
      "The code will be generated for the HLS framework";

      "-blast", Arg.Set Objects.blast,
      "The code will be generated for use with Blast";*)

    "-prod", Arg.Clear Ipanema.debug,
    "Don't generated debugging C code";

    "-dev", Arg.Set Ipanema.debug,
    "Generated debugging C code";

    "-d", Arg.Set Error.debug,
    "Set verbose mode with debugging information";

    "-o", Arg.String set_dir,
    "<dirname>  Set the name of the output directory";

    "-hrt", Arg.Set Objects.high_res,
    "Generate code for high-res timers";

    "-code", Arg.String set_gen ,
    "<name> Set the name of generated code";

    "-debug", Arg.String set_output ,
    "<name> Set the name of debugging files";

    "-os", Arg.String Objects.set_os ,
    "Select OS (eg Linux or Linux2.6)";

    "-version", Arg.Unit print_version,
    "Display version info";

    "-m", Arg.Set Objects.mutate,
    "Perform a random mutation after type checking and before verification";
  ]

let usage =
  Printf.sprintf "Usage: %s [options] <filename> \nOptions are:"
    (Filename.basename Sys.argv.(0))

let main () =
  Arg.parse speclist anonymous usage;
  if !genname == None && !input_name <> [] then
    genname := Some (List.hd !input_name);
  pipeline build_ast type_check verify compile generate_c generate_leon generate_leonstate

let _ =
  try
    Unix.handle_unix_error main ()
  with
    Sys_error(s)     -> fatal_error s
  | Failure(s)       -> fatal_error s
  | Error.Error(s)   -> fatal_error s
