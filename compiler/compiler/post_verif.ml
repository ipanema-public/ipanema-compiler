module B = Objects
module CS = Class_state

(* The purpose of this phase is to reconstruct complex conditionals that
were broken apart to facilitate verification *)

(* Also convert => statements to function calls *)

(* -------------------- Miscellaneous operations -------------------- *)

let error (line,str) =
  (Error.update_error();
   if (line > 0)
   then Printf.printf "line %d: %s\n" line str
   else Printf.printf "%s\n" str)

(* ----------------- Reorganization of conditionals ----------------- *)

let negate attr = function
    Ast.UNARY(Ast.NOT,exp,_) -> exp
  | exp -> Ast.UNARY(Ast.NOT,exp,B.updty attr B.BOOL)

let try_and exp stm1 stm2 attr =
  let bool_attr = B.updty attr B.BOOL in
  match stm1 with
    Ast.IF(expa,stm1a,stm2a,a) ->
      if Ast.equal_except_attributes stm2 stm2a
      then Some(Ast.IF(Ast.BINARY(Ast.AND,exp,expa,bool_attr),
		       stm1a,stm2a,attr))
      else if Ast.equal_except_attributes stm2 stm1a
      then Some(Ast.IF(Ast.BINARY(Ast.AND,exp,negate bool_attr expa,bool_attr),
		       stm2a,stm1a,attr))
      else None
  | _ -> None

let try_or exp stm1 stm2 attr =
  let bool_attr = B.updty attr B.BOOL in
  match stm2 with
    Ast.IF(expa,stm1a,stm2a,_) ->
      if Ast.equal_except_attributes stm1 stm1a
      then Some(Ast.IF(Ast.BINARY(Ast.OR,exp,expa,bool_attr),
		       stm1a,stm2a,attr))
      else if Ast.equal_except_attributes stm1 stm2a
      then Some(Ast.IF(Ast.BINARY(Ast.OR,exp,negate bool_attr expa,bool_attr),
		       stm2a,stm1a,attr))
      else None
  | _ -> None

let try_not exp stm1 stm2 attr =
  match (stm1,stm2) with
    (Ast.SEQ([],[],_),Ast.SEQ([],[],_)) -> None
  | (Ast.SEQ([],[],_),_) ->
      Some(Ast.IF(negate attr exp,stm2,stm1,attr))
  | _ -> None

(* ------------------------- Postprocessing ------------------------- *)

let rec postprocess_stmt = function
    Ast.IF(exp,stm1,stm2,attr) ->
      (* try before processing both branches and after processing both
	 branches.  could try one at a time, but this seems like enough for
	 now *)
      let bool_attr = B.updty attr B.BOOL in
      (match try_and exp stm1 stm2 attr with
	Some(x) -> postprocess_stmt x
      | None ->
	  (match try_or exp stm1 stm2 attr with
	    Some(x) -> postprocess_stmt x
	  | None ->
	      (match try_and (negate bool_attr exp) stm2 stm1 attr with
		Some(x) -> postprocess_stmt x
	      | None ->
		  (match try_or (negate bool_attr exp) stm2 stm1 attr with
		    Some(x) -> postprocess_stmt x
		  | None ->
		      let stm1 = postprocess_stmt stm1
		      and stm2 = postprocess_stmt stm2 in
		      (match try_and exp stm1 stm2 attr with
			Some(x) -> postprocess_stmt x
		      | None ->
			  (match try_or exp stm1 stm2 attr with
			    Some(x) -> postprocess_stmt x
			  | None ->
			      (match try_and (negate bool_attr exp)
				  stm2 stm1 attr with
				Some(x) -> postprocess_stmt x
			      | None ->
				  (match try_or (negate bool_attr exp)
				      stm2 stm1 attr with
				    Some(x) -> postprocess_stmt x
				  | None -> Ast.IF(exp,stm1,stm2,attr)))))))))

  | Ast.FOR(id,stid,l,dir,stm,crit,attr) ->
      Ast.FOR(id,stid,l,dir,postprocess_stmt stm,crit,attr)

  | Ast.FOR_WRAPPER(label,stms,attr) ->
      Ast.FOR_WRAPPER(label,List.map postprocess_stmt stms,attr)

  | Ast.SWITCH(exp,cases,default,attr) ->
      Ast.SWITCH(exp,
		 List.map
		   (function Ast.SEQ_CASE(pat,l,stm,attr) ->
		     Ast.SEQ_CASE(pat,l,postprocess_stmt stm,attr))
		   cases,
		 Aux.app_option postprocess_stmt default,
		 attr)

  | Ast.SEQ(decls,stmts,attr) ->
      Ast.SEQ(decls,List.map postprocess_stmt stmts,attr)

  | stmt -> stmt

(* --------------------- Apply the try not rule --------------------- *)

let rec try_not_stmt = function
    Ast.IF(exp,stm1,stm2,attr) ->
      let stm1 = try_not_stmt stm1
      and stm2 = try_not_stmt stm2 in
      (match try_not exp stm1 stm2 attr with
	Some(x) -> x
      | None -> Ast.IF(exp,stm1,stm2,attr))

  | Ast.FOR(id,stid,l,dir,stm,crit,attr) ->
      Ast.FOR(id,stid,l,dir,try_not_stmt stm,crit,attr)

  | Ast.FOR_WRAPPER(label,stms,attr) ->
      Ast.FOR_WRAPPER(label,List.map try_not_stmt stms,attr)

  | Ast.SWITCH(exp,cases,default,attr) ->
      Ast.SWITCH(exp,
		 List.map
		   (function Ast.SEQ_CASE(pat,l,stm,attr) ->
		     Ast.SEQ_CASE(pat,l,try_not_stmt stm,attr))
		   cases,
		 Aux.app_option try_not_stmt default,
		 attr)

  | Ast.SEQ(decls,stmts,attr) ->
      Ast.SEQ(decls,List.map try_not_stmt stmts,attr)

  | stmt -> stmt

(* ------------------- Add return Scheduler_state ------------------- *)

let return_sched_state attr =
  Ast.mkRETURN(Ast.mkVAR("Scheduler_state",[],B.line attr),B.line attr)

(*let add_return attr = function
    Ast.SEQ(decls,stms,attr) ->
      Ast.SEQ(decls,stms@[return_sched_state attr],attr)
  | stm -> Ast.SEQ([],[stm;return_sched_state attr],attr)*)

let add_return attr s = s

(* --------------------- Handlers and functions --------------------- *)

let postprocess_handlers handlers =
  List.map
    (function (Ast.EVENT(nm,param,stmt,syn,attr)) ->
      Ast.EVENT(nm,param,add_return attr (try_not_stmt (postprocess_stmt stmt)),
		syn,attr))
    handlers

let postprocess_functions functions =
  List.map
    (function (Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr)) ->
      Ast.FUNDEF(tl, ret, nm, params,
		 try_not_stmt (postprocess_stmt stmt),
		 inl, attr))
    functions

let postprocess
    (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		   fundecls,valdefs,domains,states,cstates,criteria,
		   trace,handlers,chandlers,ifunctions,functions,attr)) =
  (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		 fundecls,valdefs,domains,states,cstates,criteria,
		 trace,
		 postprocess_handlers handlers,chandlers,
		 postprocess_functions ifunctions,functions,
		 attr))
