(* $Id$ *)

(* insert at the beginning of each traced handler code to update the trace *)

module B = Objects
module CS = Class_state

(* -------------------------- miscellaneous ------------------------- *)

let mk_fname str attr = Ast.VAR(B.mkId str, [], B.updty attr B.VOID)

(* ----------------------- the trace function ----------------------- *)

(*
if (trace_ctr < trace_max && {trace_exp}) {
  trace_buffer[trace_ctr].event = event;
  trace_buffer[trace_ctr].pid = tgt->attr->pid;
  trace_buffer[trace_ctr].jiffies = jiffies;
  trace_buffer[trace_ctr].fld = exp;
  trace_ctr++;
}
*)

let make_trace_function trace_max test exps procdecls attr =
  let trace_buffer =
    Ast.VAR(B.mkId("trace_buffer"), [],
	    B.updty attr (B.STRUCT(B.mkId "trace_struct"))) in
  let int_attr = B.updty attr B.INT in
  let bool_attr = B.updty attr B.BOOL in
  let proc_attr = B.updty attr B.PROCESS in
  let task_struct_attr = B.updty attr (B.STRUCT(B.mkId("task_struct"))) in
  let trace_ctr = Ast.VAR(B.mkId("trace_ctr"), [], int_attr) in
  let pidref procvar =
    if List.exists
	(function Ast.VARDECL(_,id,_,_,_,_,_) -> B.id2c id = "pid")
	procdecls
    then
      (* in this case, some other pass will later add the call to attr *)
      Ast.FIELD(procvar,B.mkId("pid"), [], int_attr)
    else Ast.FIELD(Ast.PRIM(Ast.VAR(B.mkId "attr", [], attr),[procvar],
			    task_struct_attr),
		   B.mkId("pid"), [], int_attr) in
  let mkassign lhs rhs = Ast.ASSIGN(lhs,rhs,false,attr) in
  let mktbassign lhs rhs =
    match B.ty(Ast.get_exp_attr rhs) with
      B.PROCESS ->
	Ast.IF(rhs,
	       Ast.ASSIGN(Ast.PRIM(mk_fname "array_ref" attr,
				   [trace_buffer;trace_ctr;Ast.VAR(lhs,[],attr)],
				   int_attr),
			  pidref rhs,false,attr),
	       Ast.ASSIGN(Ast.PRIM(mk_fname "array_ref" attr,
				   [trace_buffer;trace_ctr;Ast.VAR(lhs,[],attr)],
				   int_attr),
			  Ast.INT(-1,int_attr),false,attr),
	       attr)
    | ty ->
	Ast.ASSIGN(Ast.PRIM(mk_fname "array_ref" attr,
			    [trace_buffer;trace_ctr;Ast.VAR(lhs,[],attr)],
			    B.updty attr ty),
		   rhs,false,attr) in
  let procid = B.fresh_idx "p" in
  let procvar = Ast.VAR(procid,[],proc_attr) in
  let evid = B.fresh_idx "ev" in
  let evid2 = B.fresh_idx "ev2" in

  let event_assign = mktbassign (B.mkId("_event")) (Ast.VAR(evid,[], int_attr)) in
  let event_assign2 =
    mktbassign (B.mkId("event2")) (Ast.VAR(evid2, [],int_attr)) in
  let pid_assign = mktbassign (B.mkId("pid")) (pidref procvar) in
  let comm_assign =
    Ast.PRIMSTMT(Ast.VAR(B.mkId("strncpy"),[],B.updty attr B.VOID),
		 [Ast.PRIM(mk_fname "array_ref" attr,
			   [trace_buffer;trace_ctr;
			     Ast.VAR(B.mkId("comm"),[],attr)],
			   B.updty attr B.VOID);
		  Ast.FIELD(Ast.PRIM(Ast.VAR(B.mkId "attr", [],attr),
				     [procvar],
				     task_struct_attr),
			     B.mkId("comm"), [], B.updty attr B.VOID);
		   Ast.INT(16,int_attr)],
		 attr) in
  let state_assign =
    mktbassign (B.mkId("state"))
      (Ast.FIELD(procvar,B.mkId("state"),[],int_attr)) in
  let jiffies_assign =
    mktbassign (B.mkId("jiffies"))
      (Ast.VAR(B.mkId("(int)jiffies"), [],int_attr)) in

  let test_exp =
    let test_lim =
      Ast.BINARY(Ast.LT,trace_ctr,Ast.INT(trace_max,int_attr),bool_attr) in
    match test with
      None -> test_lim
    | Some exp -> Ast.BINARY(Ast.AND,test_lim,exp,bool_attr) in

  (* a bit clunky to change the type both here and in mktbassign... *)
  (List.map
     (function
	 Ast.TRACEVAR(fldid,id,attr) ->
	   let ty = match B.ty attr with B.PROCESS -> B.INT | ty -> ty in
	   Ast.TRACEVAR(fldid,id,B.updty attr ty)
       | Ast.TRACEFIELD(id,attr) ->
	   let ty = match B.ty attr with B.PROCESS -> B.INT | ty -> ty in
	   Ast.TRACEFIELD(id,B.updty attr ty)
       | Ast.TRACEQUEUE(fldid,refid,attr) as x -> x)
     exps,
   Ast.FUNDEF(Ast.DFT,B.VOID,B.mkId("trace"),
	      [Ast.VARDECL(B.INT,evid,false,false,CS.UNSHARED,None,attr);
		Ast.VARDECL(B.INT,evid2,false,false,CS.UNSHARED,None,attr);
		Ast.VARDECL(B.PROCESS,
			    procid,false,false,CS.UNSHARED,None,attr)],
	      Ast.IF(test_exp,
		     Ast.mkBLOCK attr
		       ((
			   [event_assign; event_assign2; pid_assign;
			     comm_assign; state_assign; jiffies_assign]
               )
			@ List.map
			    (function
				Ast.TRACEVAR(fldid,id,attr) ->
				  mktbassign fldid (Ast.VAR(id,[],attr))
			      | Ast.TRACEFIELD(id,attr) ->
				  mktbassign id (Ast.FIELD(procvar, id, [],attr))
			      | Ast.TRACEQUEUE(fldid,refid,attr) ->
				  mktbassign fldid
				    (Ast.PRIM(mk_fname "count_queue" attr,
					      [Ast.VAR(refid, [],
						       B.updty attr B.VOID)],
					      B.updty attr B.INT)))
			    exps
			@ [mkassign trace_ctr
			      (Ast.BINARY(Ast.PLUS,trace_ctr,
					  Ast.INT(1,int_attr),
					  int_attr))]),
		     Ast.mkBLOCK attr [],
		     attr),
	      Ast.NO_INLINE, attr))

(* ------------------- calling the trace function ------------------- *)

let call_trace event event_num proc_exp attr =
  let event = Ast.VAR(event,[], B.updty attr B.INT) in
  let event_num = match event_num with None -> event | Some x -> x in
  Ast.PRIMSTMT(mk_fname "TRACE" attr, [event; event_num; proc_exp], attr)

let find_move_to_running count name attr stmt =
  let rec loop = function
      Ast.IF(exp,stmt1,stmt2,attr) ->
      Ast.IF(exp,loop stmt1,loop stmt2,attr)

    | Ast.FOR(id,state_id,state_info,dir,stmt,crit, attr) ->
	Ast.FOR(id,state_id,state_info,dir,loop stmt,crit,attr)

    | Ast.FOR_WRAPPER(label,stmts,attr) ->
	Ast.FOR_WRAPPER(label,List.map loop stmts,attr)

    | Ast.SWITCH(exp,cases,default,attr) ->
	Ast.SWITCH(exp,
		   List.map
		     (function Ast.SEQ_CASE(idlist,state_info,stmt,attr) ->
		       Ast.SEQ_CASE(idlist,state_info,loop stmt,attr))
		   cases,
		   Aux.app_option loop default,
		   attr)

    | Ast.SEQ(decls,stmts,attr) ->
	Ast.SEQ(decls,List.map loop stmts,attr)

    | Ast.MOVE(exp,state,info1,info2,Some(CS.STATE(id,CS.RUNNING,pq,_)),
	       auto_allowed,state_end,mattr)
      as stmt ->
	Ast.mkBLOCK mattr
	  [stmt; call_trace name None state attr]

    | Ast.MOVEFWD(exp,srcs_dsts,state_end,attr)
    | Ast.SAFEMOVEFWD(exp,srcs_dsts,_,state_end,attr)
      as stmt ->
	let proc_attr = B.updty attr B.PROCESS in
	let state = match
	  List.find
	    (function (_,CS.STATE(_,CS.RUNNING,_,_)) -> true | _ -> false)
	    (!CS.state_env) with
		(_,CS.STATE(state,_,_,_)) -> state
	      | _ -> failwith "Unexpected CSTATE"
	in
	Ast.mkBLOCK attr
	  [stmt; call_trace name None (Ast.VAR(state,[],proc_attr)) attr]


    | stmt -> stmt in
  loop stmt

let process_handlers count events handlers =
  List.map
    (function Ast.EVENT(name,param,stmt,syn,attr) as x ->
      (* if List.exists (function nm -> Ast.event_name_prefix nm name) events
      then
	let snm = Ast.event_name2c name in
	let new_stmt =
	  match snm with
	    "bossa.schedule" -> find_move_to_running count name attr stmt
	  | "preempt" ->
	      let proc_attr = B.updty attr B.PROCESS in
	      let running_state =
		match CS.concretize_class CS.RUNNING with
		  [CS.STATE(running,_,_,_)] -> running
		| _ -> raise (Error.Error "compile_trace: internal error") in
	      Ast.mkBLOCK attr
		[call_trace name None (Ast.VAR(running_state,[],proc_attr)) attr;
		  stmt]
	  | _ ->
	      Ast.mkBLOCK attr
		[call_trace name
		    (Some(Ast.VAR(B.mkId "_event", [],B.updty attr B.PEVENT)))
		    (Ast.VAR(B.mkId("tgt"
				      ), [],
			     B.updty attr B.PROCESS))
		    attr;
		  stmt]
	in Ast.EVENT(name,param,new_stmt,attr)
      else *) x)
    handlers

(* -------------------------- entry point --------------------------- *)

let compile_trace = function
    (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		   fundecls,valdefs,domains,states,cstates,criteria,
		   Ast.TRACE(count,events,exps,test,attr),
		   handlers,chandlers,ifunctions,functions,sched_attr)) ->
    let new_handlers =
      process_handlers count events handlers in
    let (new_exps,trace_function) =
      make_trace_function count test exps procdecls attr in
    (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		   fundecls,valdefs,domains,states,cstates,criteria,
		   Ast.TRACE(count,events,new_exps,test,attr),
		   new_handlers,chandlers,ifunctions,
		     trace_function :: functions,
		     sched_attr))
  | (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		   fundecls,valdefs,domains,states,cstates,criteria,
		   _,handlers,chandlers,ifunctions,functions,attr)) as s -> s
