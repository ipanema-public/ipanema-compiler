(* This phase is only used if Objects.high_res is true, implying that the
policy will be used with a kernel that supports high-resolution timers.

This phase implements the function make_time.  If both arguments are
constants, then a global variable is declared that points to a structure
containing these constants.  At most one such variable is declared for each
possible constant.  If at least one of the arguments is not a constant,
then a local time-typed variable is declared and initialized.  These
variables are shared when possible across different statements. *)

module B = Objects
module CS = Class_state

let time_tbl = (Hashtbl.create(500) : (int * int, B.identifier) Hashtbl.t)

type args = NOW of B.identifier | ONE of Ast.expr | TWO of Ast.expr * Ast.expr
| THREE of Ast.expr * Ast.expr

let process_timespec op exp1 exp2 a new_times =
    let mkfld id fld = Ast.FIELD(id,B.mkId fld,[],B.updty a B.INT) in
  let boolattr = B.updty a B.BOOL in
  let intattr = B.updty a B.INT in
  let mknsec op attr =
    Ast.BINARY(op,mkfld exp1 "tv_nsec", mkfld exp2 "tv_nsec",attr) in
  let mksec op attr =
    Ast.BINARY(op,mkfld exp1 "tv_sec", mkfld exp2 "tv_sec",attr) in
  let name = List.length new_times in
  let id = B.mkId(Printf.sprintf "time_local_%d" name) in
  match op with
    Ast.EQ ->
      (Ast.BINARY(Ast.AND, mksec op boolattr, mknsec op boolattr, boolattr),
       new_times)
  | Ast.NEQ ->
      (Ast.BINARY(Ast.OR, mksec op boolattr, mknsec op boolattr, boolattr),
       new_times)
  | Ast.LT | Ast.GT | Ast.GEQ | Ast.LEQ ->
      let op1 =
	(match op with
	  Ast.LT | Ast.LEQ -> Ast.LT
	| _ -> Ast.GT) in
      (Ast.BINARY(Ast.OR, mksec op1 boolattr,
		 Ast.BINARY(Ast.AND, mksec Ast.EQ boolattr,
			    mknsec op boolattr, boolattr),
		 boolattr),
       new_times)
  | Ast.PLUS | Ast.MINUS ->
     let fct = match op with
	 Ast.PLUS -> "timespec_add"
       | Ast.MINUS -> "timespec_sub"
     in
     let new_exp = Ast.PRIM(Ast.VAR(B.mkId(fct),[], B.mkAttr(B.line a)), [exp1;exp2], a) in
       (Ast.VAR(id,[],B.updty a B.TIME),
	ONE(new_exp)::new_times)
  | Ast.TIMES ->
      (match (B.ty(Ast.get_exp_attr exp1), B.ty(Ast.get_exp_attr exp2)) with
	(B.INT,B.TIME) ->
        (Ast.VAR(id,[],B.updty a B.TIME),
	   (TWO(Ast.BINARY(op,exp1,mkfld exp2 "tv_sec",intattr),
		Ast.BINARY(op,exp1,mkfld exp2 "tv_nsec",intattr)))
	   ::new_times)
      |	(B.TIME,B.INT) ->
              (Ast.VAR(id,[],B.updty a B.TIME),
	   (TWO(Ast.BINARY(op,mkfld exp1 "tv_sec",exp2,intattr),
		Ast.BINARY(op,mkfld exp1 "tv_nsec",exp2,intattr)))
	   ::new_times)
      |	_ -> raise (Error.Error "unexpected arguments for *"))
  | op -> raise (Error.Error
		  (Printf.sprintf
		     "%s: %s is not a supported time operator"
		     (B.loc2c a) (Ast.bop2c op)))

let process_ktime op exp1 exp2 a new_times =
  let mkfld id fld = Ast.FIELD(id,B.mkId fld,[],B.updty a B.INT) in
  let boolattr = B.updty a B.BOOL in
  let intattr = B.updty a B.INT in
  let name = List.length new_times in
  let id = B.mkId(Printf.sprintf "time_local_%d" name) in
  match op with
    Ast.EQ | Ast.NEQ | Ast.GEQ | Ast.LEQ ->
      (Ast.BINARY(op, Ast.PRIM(Ast.VAR(B.mkId("ktime_compare"),[],B.mkAttr (-1)), [exp1;exp2], intattr),
		  Ast.INT(0, intattr),
		  boolattr),
       new_times)
     | Ast.LT ->
        (Ast.PRIM(Ast.VAR(B.mkId("ktime_before"),[],B.mkAttr (-1)), [exp1;exp2], intattr),
       new_times)
    | Ast.GT ->
       (Ast.PRIM(Ast.VAR(B.mkId("ktime_after"),[],B.mkAttr (-1)), [exp1;exp2], intattr),
       new_times)
  | Ast.PLUS | Ast.MINUS ->
     let fct = match op with
	 Ast.PLUS -> "ktime_add"
       | Ast.MINUS -> "ktime_sub"
     in
     let new_exp = Ast.PRIM(Ast.VAR(B.mkId(fct),[], B.mkAttr(B.line a)), [exp1;exp2], a) in
     (new_exp, new_times)

    | Ast.DIV as op ->
      (match (B.ty(Ast.get_exp_attr exp1), B.ty(Ast.get_exp_attr exp2)) with
	(B.INT,B.TIME) ->
	  raise (Error.Error
		   (Printf.sprintf
		      "%s: %s: %s is not a supported div operator"
		   __LOC__ (B.loc2c a) (Ast.bop2c op)))
      |	(B.TIME,B.INT) ->
	 let new_exp = Ast.PRIM(Ast.VAR(B.mkId("ktime_divns"),
					[], B.mkAttr(B.line a)), [exp1;exp2], a) in
	 (new_exp, new_times)
      |	_ ->
	 	  raise (Error.Error
		   (Printf.sprintf
		      "%s: %s: %s is not a supported div operator"
		   __LOC__ (B.loc2c a) (Ast.bop2c op))))

  | Ast.TIMES ->
      (match (B.ty(Ast.get_exp_attr exp1), B.ty(Ast.get_exp_attr exp2)) with
	(B.INT,B.TIME) ->
        (Ast.VAR(id,[],B.updty a B.TIME),
	   (TWO(Ast.BINARY(op,exp1,mkfld exp2 "tv_sec",intattr),
		Ast.BINARY(op,exp1,mkfld exp2 "tv_nsec",intattr)))
	   ::new_times)
      |	(B.TIME,B.INT) ->
              (Ast.VAR(id,[],B.updty a B.TIME),
	   (TWO(Ast.BINARY(op,mkfld exp1 "tv_sec",exp2,intattr),
		Ast.BINARY(op,mkfld exp1 "tv_nsec",exp2,intattr)))
	   ::new_times)
      |	_ -> raise (Error.Error "unexpected arguments for *"))

    | op -> raise (Error.Error
		  (Printf.sprintf
		     "%s: %s: %s is not a supported time operator"
		     __LOC__ (B.loc2c a) (Ast.bop2c op)))

let process_times op exp1 exp2 a new_times =
  if true then
    process_ktime op exp1 exp2 a new_times
  else
    process_timespec op exp1 exp2 a new_times

let rec process_exp new_times = function
    Ast.FIELD(exp,fld,ssa,a) ->
      let (exp,new_times) = process_exp new_times exp in
      (Ast.FIELD(exp,fld,ssa,a),new_times)
  | Ast.UNARY(op,exp,a) ->
      let (exp,new_times) = process_exp new_times exp in
      (Ast.UNARY(op,exp,a),new_times)
  | Ast.BINARY(op,exp1,exp2,a) ->
      let (exp1,new_times) = process_exp new_times exp1 in
      let (exp2,new_times) = process_exp new_times exp2 in
      if (B.ty a = B.TIME ||
          B.ty(Ast.get_exp_attr exp1) = B.TIME ||
	  B.ty(Ast.get_exp_attr exp2) = B.TIME)
      then process_times op exp1 exp2 a new_times
      else (Ast.BINARY(op,exp1,exp2,a),new_times)
  | Ast.PBINARY(op,exp1,states1,exp2,states2,a) ->
      let (exp1,new_times) = process_exp new_times exp1 in
      let (exp2,new_times) = process_exp new_times exp2 in
      (Ast.PBINARY(op,exp1,states1,exp2,states2,a),new_times)
  | Ast.INDR(exp,a) ->
      let (exp,new_times) = process_exp new_times exp in
      (Ast.INDR(exp,a),new_times)
  | Ast.PRIM(Ast.VAR(id,_,va),[Ast.INT(c1,ac1);Ast.INT(c2,ac2)],a)
    when (B.id2c id = "make_cycle_time" ||
          (B.id2c id = "make_time" && (c1 = 0 && c2 = 0))) ->
              ((try Ast.VAR(Hashtbl.find time_tbl (c1,c2), [],B.updty va B.TIME)
      with Not_found ->
	let id = B.mkId(Printf.sprintf "time_%d_%d" c1 c2) in
	Hashtbl.add time_tbl (c1,c2) id;
    (Ast.VAR(id,[],B.updty va B.TIME))),
       new_times)
  | Ast.PRIM(Ast.VAR(id,_,va),[e1;e2],a)
    when B.id2c id = "make_cycle_time" ->
      let name = List.length new_times in
      let id = B.mkId(Printf.sprintf "time_local_%d" name) in
      (Ast.VAR(id,[],B.updty va B.TIME),(TWO(e1,e2)::new_times))
  | Ast.PRIM(Ast.VAR(id,_,va),[e1;e2],a)
    when B.id2c id = "make_time" ->
      let name = List.length new_times in
      let id = B.mkId(Printf.sprintf "time_local_%d" name) in
      (Ast.VAR(id,[],B.updty va B.TIME),(THREE(e1,e2)::new_times))
  | Ast.PRIM(Ast.VAR(id,[],va),[e1;e2],a)
    when B.id2c id = "time_to_seconds" || B.id2c id = "time_to_nanoseconds" ->
      raise
	(Error.Error
	   (Printf.sprintf "%s: %s not currently supported"
	      (B.loc2c a) (B.id2c id)))
  | Ast.PRIM(Ast.VAR(id,_,va),[e],a)
    when B.id2c id = "cycles_to_time" ->
      process_exp new_times
    (Ast.PRIM(Ast.VAR(B.mkId "make_time",[],va),
		  [Ast.INT(0,B.updty a B.INT);e],
		  a))

  | Ast.PRIM(Ast.VAR(id,_,va),[],a) as x
    when B.id2c id = "now" ->
     (Ast.PRIM(Ast.VAR(B.mkId "ktime_get", [], va), [], a), (new_times))

  | Ast.PRIM(Ast.VAR(id,_,va),[arg],a) as x
    when B.id2c id = "time_to_ms" ->
     (Ast.PRIM(Ast.VAR(B.mkId "ktime_to_ms", [], va), [arg], a), (new_times))

  | Ast.PRIM(Ast.VAR(id,_,va),[arg],a) as x
    when B.id2c id = "ms_to_time" ->
     (Ast.PRIM(Ast.VAR(B.mkId "ms_to_ktime", [], va), [arg], a), (new_times))

  | Ast.PRIM(fn,args,a) ->
      let (fn,new_times) = process_exp new_times fn in
      let (args,new_times) =
	List.fold_left
	  (function (args,new_times) ->
	    function arg ->
	      let (arg,new_times) = process_exp new_times arg in
	      (arg::args,new_times))
	  ([],new_times) args in
      let args = List.rev args in
      (Ast.PRIM(fn,args,a),new_times)
  | Ast.SCHEDCHILD(exp,x,a) ->
      let (exp,new_times) = process_exp new_times exp in
      (Ast.SCHEDCHILD(exp,x,a),new_times)
  | Ast.IN(exp,x,y,z,m,c,a) ->
      let (exp,new_times) = process_exp new_times exp in
      (Ast.IN(exp,x,y,z,m,c,a),new_times)
  | Ast.MOVEFWDEXP(exp,a,b) ->
      let (exp,new_times) = process_exp new_times exp in
      (Ast.MOVEFWDEXP(exp,a,b),new_times)
  | exp -> (exp,new_times)

let process_new_times n new_times attr s =
  let new_times = List.rev new_times in
  let intattr = B.updty attr B.INT in
  let mkassign id fld vl =
      Ast.ASSIGN(Ast.FIELD(id,B.mkId fld,[],intattr),vl,false,attr) in
  let mkfld id fld = Ast.FIELD(id,B.mkId fld,[],intattr) in
  let mkcall id str =
    mkassign id "tv_nsec"
      (Ast.PRIM(Ast.VAR(B.mkId str,[],attr),
		[mkfld id "tv_sec";mkfld id "tv_nsec";mkfld id "tv_sec"],
		(B.updty attr B.INT))) in
  let (new_n,(d,stms)) =
    List.fold_left
      (function (n,(decls,stms)) ->
	function exp ->
	  let id = Ast.VAR(B.mkId(Printf.sprintf "time_local_%d" n),
               [],B.updty attr B.TIME) in
	   match exp with
	     NOW(ts) ->
	       let vts = Ast.VAR(ts, [], B.mkAttr (-1)) in
	       let ts_decl =
		 Ast.UNINITDEF(
		   Ast.VARDECL(B.STRUCT(B.mkId "timespec"), ts,
			       false, false, CS.UNSHARED,None, B.mkAttr (-1)), B.mkAttr (-1))
	       in
	       (n,
		(ts_decl::decls,
		(* Order is reversed later *)
		 Ast.PRIMSTMT(Ast.VAR(B.mkId "getnstimeofday", [], attr),
			      [Ast.AREF(vts, None, attr)], attr)
		 ::stms))
	   | ONE(exp) ->
	      (n+1,
	       (decls, Ast.ASSIGN(id, exp,false,attr)::stms))
	   | TWO(secs,nsecs) ->
	      (n+1,
	       ([],
		mkcall id "call_normalize_time" ::
		  mkassign id "tv_nsec" nsecs :: mkassign id "tv_sec" secs ::
		  stms))
	   | THREE(secs,nsecs) ->
	      (n+1,
	       ([],
		mkcall id "tstojiffie" ::
		  mkassign id "tv_nsec" nsecs :: mkassign id "tv_sec" secs ::
		  stms)))
      (0,([],[])) new_times in
  match stms with
    [] -> (s, new_n)
  | _ -> (Ast.SEQ(d, List.rev (s :: stms),attr),new_n)

let process_decl = function
    Ast.VALDEF(decl,exp,isconst,attr) ->
      let (exp,new_times) = process_exp [] exp in
      (Ast.VALDEF(decl,exp,isconst,attr),new_times)
  | decl -> (decl,[])

let rec process_stm n = function
    Ast.IF(tst,thn,els,a) ->
      let (tst,new_times) = process_exp [] tst in
      let (thn,n) = process_stm n thn in
      let (els,n) = process_stm n els in
      process_new_times n new_times a (Ast.IF(tst,thn,els,a))
  | Ast.FOR(x,y,z,d,stm,crit,a) ->
      let (stm,n) = process_stm n stm in
      (Ast.FOR(x,y,z,d,stm,crit,a),n)
  | Ast.FOR_WRAPPER(x,stms,a) ->
      let (stms,n) =
	List.fold_left
	  (function (stms,n) ->
	    function stm ->
	      let (cur,n) = process_stm n stm in
	      (cur::stms,n))
	  ([],n) stms in
      let stms = List.rev stms in
      (Ast.FOR_WRAPPER(x,stms,a),n)
  | Ast.SWITCH(exp,cases,dflt,a) ->
      let (exp,new_times) = process_exp [] exp in
      let (cases,n) =
	List.fold_left
	  (function (cases,n) ->
	    function Ast.SEQ_CASE(a,b,stm,c) ->
	      let (stm,n) = process_stm n stm in
	      (Ast.SEQ_CASE(a,b,stm,c)::cases,n))
	  ([],n) cases in
      let cases = List.rev cases in
      let (dflt,n) =
	match dflt with
	  None -> (None,n)
	| Some stm ->
	    let (stm,n) = process_stm n stm in
	    (Some stm,n) in
      process_new_times n new_times a (Ast.SWITCH(exp,cases,dflt,a))
  | Ast.SEQ(decls,stms,a) ->
      let decls_newtimes = List.map process_decl decls in
      let (stms,n) =
	List.fold_left
	  (function (stms,n) ->
	    function stm ->
	      let (stm,n) = process_stm n stm in
	      (stm::stms,n))
	  ([],n) stms in
      let stms = List.rev stms in
      let rec decl_loop acc_decls n = function
	  [] -> (Ast.SEQ(List.rev acc_decls,stms,a),n)
	| ((decl,[])::rest) -> decl_loop (decl::acc_decls) n rest
	| ((decl,new_times)::rest) ->
	    let (rest,n) = decl_loop [] n rest in
	    process_new_times n new_times a
	      (match rest with
		Ast.SEQ(d,s,a1) -> Ast.SEQ((List.rev (decl::acc_decls))@d,s,a1)
	      |	_ -> (Ast.SEQ (List.rev (decl::acc_decls), [rest], a))) in
      decl_loop [] n decls_newtimes
  | Ast.RETURN(Some exp,a) ->
      let (exp,new_times) = process_exp [] exp in
      process_new_times n new_times a (Ast.RETURN(Some exp,a))
  | Ast.MOVE(exp,x,y,z,r,s,b,a) ->
      let (exp,new_times) = process_exp [] exp in
      process_new_times n new_times a (Ast.MOVE(exp,x,y,z,r,s,b,a))
  | Ast.MOVEFWD(exp,x,b,a) ->
      let (exp,new_times) = process_exp [] exp in
      process_new_times n new_times a (Ast.MOVEFWD(exp,x,b,a))
  | Ast.SAFEMOVEFWD(exp,x,s,b,a) ->
      let (exp,new_times) = process_exp [] exp in
      let (s,n) = process_stm n s in
      process_new_times n new_times a (Ast.SAFEMOVEFWD(exp,x,s,b,a))
  | Ast.ASSIGN(lexp,rexp,x,a) ->
      let (lexp,new_times) = process_exp [] lexp in
      let (rexp,new_times) = process_exp new_times rexp in
      process_new_times n new_times a (Ast.ASSIGN(lexp,rexp,x,a))
  | Ast.PRIMSTMT(fn,args,a) ->
      let (fn,new_times) = process_exp [] fn in
      let (args,new_times) =
	List.fold_left
	  (function (args,new_times) ->
	    function arg ->
	      let (arg,new_times) = process_exp new_times arg in
	      (arg::args,new_times))
	  ([],new_times) args in
      let args = List.rev args in
      process_new_times n new_times a (Ast.PRIMSTMT(fn,args,a))
  | Ast.ASSERT(exp,a) ->
      let (exp,new_times) = process_exp [] exp in
      process_new_times n new_times a (Ast.ASSERT(exp,a))
  | stm -> (stm,n)

(* --------------------- Handlers and functions --------------------- *)

let top_decls stm attr = function
    0 -> stm
  | n ->
      (let new_decls =
	let rec loop = function
	    0 -> []
	  | n ->
	      let n = n-1 in
	      let id = B.mkId(Printf.sprintf "time_local_%d" n) in
	      Ast.SYSDEF(Ast.VARDECL(B.TIME,id,false,false,CS.UNSHARED,None,attr),Ast.CONST,attr)
	      :: loop n in
	loop n in
       let new_decls = List.rev new_decls in
      match stm with
	Ast.SEQ(decls,stms,attr) ->
	  Ast.SEQ(decls@new_decls,stms,attr)
      | _ -> Ast.SEQ(new_decls,[stm],attr))

let compile_handlers handlers =
  List.map
    (function (Ast.EVENT(nm,param,stm,syn,attr)) ->
      let (stm,n) = process_stm 0 stm in
      let stm = top_decls stm attr n in
      Ast.EVENT(nm,param,stm,syn,attr))
    handlers

let compile_ifunctions ifunctions =
  List.map
    (function Ast.FUNDEF(tl,ret,nm,params,stm,inl,attr) ->
      let (stm,n) = process_stm 0 stm in
      let stm = top_decls stm attr n in
      Ast.FUNDEF(tl,ret,nm,params,stm,inl,attr))
    ifunctions

let compile_steal_blk (filter, select, migrate) =
  let (v1, v2, v3, v4, stm, expr, upd) = migrate in
  let (stm,n) = process_stm 0 stm in
  let stm = top_decls stm (B.dum __LOC__) n in
  let migrate = (v1, v2, v3, v4, stm, expr, upd) in
  (filter, select, migrate)

let compile_steal_thread steal =
  match steal with
    Ast.FLAT_STEAL_THREAD st -> Ast.FLAT_STEAL_THREAD (compile_steal_blk st)
  | Ast.ITER_STEAL_THREAD (st, until, post) ->
     let (stm,n) = process_stm 0 (Ast.SEQ([], post, B.dum __LOC__)) in
     let post = top_decls stm (B.dum __LOC__) n in
     Ast.ITER_STEAL_THREAD (compile_steal_blk st, until, [post])

let compile_steal steal =
  match steal with
    Ast.STEAL_THREAD s -> Ast.STEAL_THREAD (compile_steal_thread s)
  | Ast.ITER_STEAL_GROUP (sg, st, post, postgrp) ->
      let (stm,n) = process_stm 0 (Ast.SEQ([], post, B.dum __LOC__)) in
      let post = top_decls stm (B.dum __LOC__) n in
      let (stm,n) = process_stm 0 (Ast.SEQ([], postgrp, B.dum __LOC__)) in
      let postgrp = top_decls stm (B.dum __LOC__) n in
      Ast.ITER_STEAL_GROUP (sg, compile_steal_thread st, [post], [postgrp])

  | Ast.FLAT_STEAL_GROUP (sg, st, postgrp) ->
      let (stm,n) = process_stm 0 (Ast.SEQ([], postgrp, B.dum __LOC__)) in
      let postgrp = top_decls stm (B.dum __LOC__) n in
     Ast.FLAT_STEAL_GROUP (sg, compile_steal_thread st, [postgrp])

let compile_steal_param (dom, dstg, dst, steal) =
  (dom, dstg, dst, compile_steal steal)

let compile_states states =
  match states with
    Ast.CORE(cvd, states, Some steal) ->
     Ast.CORE(cvd, states, Some (compile_steal_param steal))
  | _ -> states

let compile_decl (Ast.VARDECL(ty, id, imported, _lazy, sh, eo, a)) =
  match eo with
    None ->
    Ast.VARDECL(ty, id, imported, _lazy, sh, None, a)
  | Some e ->
     Ast.VARDECL(ty, id, imported, _lazy, sh, Some (fst (process_exp [] e)), a)

(* -------------------------- Entry point --------------------------- *)

let compile_time
    ((Ast.SCHEDULER(nm,
		    cstdefs,enums,dom,grp,procdecls,
		    fundecls,valdefs,domains,states,cstates,criteria,
		    trace,handlers,chandlers,ifunctions,functions,
		      attr)) as ast) =
  if !Objects.high_res
  then
    let cstdefs = fst (List.split (List.map process_decl cstdefs)) in
    let dom = List.map compile_decl dom in
    let grp = List.map compile_decl grp in
    let procdecls = List.map compile_decl procdecls in
    let states = compile_states states in
    let handlers = compile_handlers handlers in
    let chandlers = compile_handlers chandlers in
    let ifunctions = compile_ifunctions ifunctions in
    let functions = compile_ifunctions functions in
    let time_defs =
      let intattr = B.updty attr B.INT in
      Hashtbl.fold
	(function (c1,c2) ->
	  function id ->
	    function rest ->
	      Ast.VALDEF(Ast.VARDECL(B.TIME,id,false,false,CS.UNSHARED,None,attr),
             Ast.PRIM(Ast.VAR(B.mkId("make_cycle_time"),[],attr),
				  [Ast.INT(c1,intattr);Ast.INT(c2,intattr)],
				  attr),
			 Ast.CONST,
			 attr)
	      :: rest)
	time_tbl [] in
    let time_defs = List.sort compare time_defs in
    Ast.SCHEDULER(nm,cstdefs@time_defs,
		  enums,dom,grp,procdecls,fundecls,valdefs,domains,states,cstates,
		  criteria,trace,handlers,chandlers,ifunctions,functions,
		  attr)
  else ast
