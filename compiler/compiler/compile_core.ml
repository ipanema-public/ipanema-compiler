module A = Ast
module B = Objects
module T = Type
module CS = Class_state

let opt_apply f o =
  match o with
    Some x -> Some (f x)
  | None -> None

(* ----------------------- Environment lookup ----------------------- *)

exception LookupErrException

let rec lookup_id x = function
    [] -> raise LookupErrException
  | ((y,v)::r) -> if B.ideq(x,y) then v else lookup_id x r

let is_state id =
  try let _ = lookup_id id (!CS.state_env) in true
  with LookupErrException -> false

(* -------------------------- expression and stmt --------------------------- *)
let rec compile_exp4lazy_raw core e =
  match e with
    Ast.VAR(id, ssa, attr) as exp ->
      if is_state id then
	let state =
	  Ast.FIELD(
	    Ast.PRIM(Ast.VAR(B.mkId "per_cpu", [], attr),
                     [Ast.VAR(B.mkId "state_info", [], attr);
		      Ast.FIELD(Ast.VAR(B.mkId core, [], attr),
				B.mkId "id", [], B.updty attr B.INT)],
		     attr),
	    id, ssa, attr)
	in
	(Some state, state)
      else (None, exp)
  | Ast.FIELD(exp, id, ssa, attr) ->
     let (cond, exp) = compile_exp4lazy_raw core exp in
     begin
       match cond with
	 None -> (None, Ast.FIELD(exp, id, ssa, attr))
       | Some cond ->
	  (None, Ast.TERNARY(cond,
			    Ast.FIELD(exp, id, ssa, attr),
			    Ast.INT(0, attr),
			    attr))
     end

  | Ast.TERNARY(e1, e2, e3, a) ->
     let (s, ne) = compile_exp4lazy_raw core e1 in
     (s, Ast.TERNARY(ne,
		     snd (compile_exp4lazy_raw core e2),
		     snd (compile_exp4lazy_raw core e3),
		     a))

  | Ast.VALID(e, state, a) ->
     let (s,ne) = compile_exp4lazy_raw core e in
     (s, Ast.VALID(ne, state, a))

  | Ast.PRIM(Ast.COUNT _, [Ast.VAR(id, _, ida)], a) -> (* Identify as isTerminal in plazy *)
     let p = Ast.AREF(Ast.FIELD(Ast.mkVAR(core, [], -1),
		                id, [], ida),
               None, ida) in
     (None, Ast.PRIM(Ast.mkVAR("cpumask_weight", [], -1), [p], a))
  | _ -> (None, e)

let compile_exp4lazy core e =
  snd (compile_exp4lazy_raw core e)

let rec compile_exp (cv: B.identifier list) (cpu: Ast.expr option) e =
  match e with
    Ast.SELF attr ->
      let line = B.line attr in
      let coreexp = Ast.PRIM(Ast.mkVAR("smp_processor_id", [], line),
		             [], (B.updty (B.mkAttr line) B.INT))
      in
      Ast.AREF(Ast.mkPRIM("per_cpu",
			  [Ast.mkVAR("core",[], line); coreexp], line),
	       None, attr)

  | Ast.LCORE(str, expr, cexpo, attr) ->
      (match cexpo with
	None -> Ast.LCORE(str, expr, cpu, attr)
      | _ -> e)

  | Ast.VAR(id, ssa, attr) as exp ->
      if List.mem id cv then
	  Ast.LCORE("core", exp, cpu, attr)
      else if is_state id then
	Ast.LCORE("state_info", exp, cpu, attr)
      else exp
  | Ast.FIELD(exp, id, ssa, attr)
      when B.ty (Ast.get_exp_attr exp) = B.CORE
      && is_state id ->
     Ast.FIELD(
       Ast.PRIM(Ast.VAR(B.mkId "per_cpu", [], attr),
                [Ast.VAR(B.mkId "state_info", [], attr);
		 Ast.FIELD(compile_exp cv cpu exp, B.mkId "id", [], B.updty attr B.INT)],
		attr),
       id, ssa, attr)

  | Ast.FIELD(exp, id, ssa, attr)
       when B.ty attr = B.CORE && B.id2c id = "cpu" &&
              B.ty (Ast.get_exp_attr exp) = B.PROCESS ->
     Ast.AREF(Ast.PRIM(Ast.VAR(B.mkId "ipanema_core", [], attr),
                       [Ast.PRIM(Ast.VAR(B.mkId "task_cpu", [], attr),
                                 [Ast.FIELD(compile_exp cv cpu exp, B.mkId "task", [], attr)],
	                         attr)], B.updty attr B.INT), None, B.updty attr B.CORE)

  | Ast.FIELD(exp, id, ssa, attr) ->
     Ast.FIELD(compile_exp cv cpu exp, id, ssa, attr)
  | Ast.UNARY(uop,exp,attr) ->
     Ast.UNARY(uop,compile_exp cv cpu exp,attr)

  | Ast.BINARY(bop,exp1,exp2,attr) ->
      let exp1 = compile_exp cv cpu exp1
      and exp2 = compile_exp cv cpu exp2 in
      Ast.BINARY(bop,exp1,exp2,attr)

  | Ast.TERNARY(cond,exp1,exp2,attr) ->
      let cond = compile_exp cv cpu cond
      and exp1 = compile_exp cv cpu exp1
      and exp2 = compile_exp cv cpu exp2 in
      Ast.TERNARY(cond,exp1,exp2,attr)

  | Ast.INDR(exp,attr) -> Ast.INDR(compile_exp cv cpu exp,attr)
  | Ast.AREF(e, oe, a) ->
     Ast.AREF(compile_exp cv cpu e, opt_apply (compile_exp cv cpu) oe, a)

  | Ast.VALID(exp, s, a) -> Ast.VALID(compile_exp cv cpu exp, s, a)

  | Ast.PRIM(fn,pl,a) -> Ast.PRIM(fn, List.map (compile_exp cv cpu) pl ,a)

  | Ast.IN(exp1, exp2, s, t, b, c, a) -> (
     match B.ty (Ast.get_exp_attr exp1) with
       B.CORE -> Ast.PRIM(Ast.VAR(B.mkId "cpumask_test_cpu", [], a),
                          [compile_exp cv cpu (Ast.FIELD(exp1, B.mkId "id", [], a));
                           Ast.AREF(compile_exp cv cpu exp2, None, a)],
                          a)
     | _ -> Ast.IN(compile_exp cv cpu exp1, compile_exp cv cpu exp2, s, t, b, c, a)
  )

  | Ast.FIRST(exp, crit, stopt, attr)
      when B.ty attr = B.CORE && crit = None && stopt = None ->
     let lineattr = B.mkAttr (B.line attr) in
     let ea = Ast.get_exp_attr exp in
     let ty = B.ty ea in
     let exp = match ty with
         B.SET B.CORE -> Ast.AREF(exp, None, B.updty ea (B.INDR ty))
       | _ -> exp
     in
     Ast.AREF(Ast.PRIM(Ast.VAR(B.mkId("per_cpu"), [], lineattr),
		       [Ast.VAR(B.mkId("core"),[],lineattr);
			Ast.PRIM(Ast.VAR(B.mkId("cpumask_first"), [], B.updty attr B.INT),
				 [compile_exp cv cpu exp], attr)], attr),
	      None, lineattr)

  | Ast.FIRST(exp, crit, stopt, attr) -> Ast.FIRST(compile_exp cv cpu exp, crit, stopt, attr)

  | exp -> exp

let compile_decl cv cpu d =
  match d with
    Ast.VALDEF(vd, exp, isc, attr) -> Ast.VALDEF(vd, compile_exp cv cpu exp, isc, attr)
  | decl -> decl

let rec compile_stmt cv cpu s =
  match s with
    Ast.IF(exp,stmt1,stmt2,attr) ->
      Ast.IF(compile_exp cv cpu exp,
	     compile_stmt cv cpu stmt1,
	     compile_stmt cv cpu stmt2,
	     attr)

    | Ast.FOR(id,None,x,dir,stmt,crit,attr) ->
	Ast.FOR(id,None,x,dir,compile_stmt cv cpu stmt,crit,attr)

    | Ast.FOR(id,Some state_id,x,dir,stmt,crit,attr) ->
	Ast.FOR(id,Some (List.map (compile_exp cv cpu) state_id),x,dir,compile_stmt cv cpu stmt,crit,attr)

    | Ast.FOR_WRAPPER(label,stmts,attr) ->
	Ast.FOR_WRAPPER(label,List.map (compile_stmt cv cpu) stmts,attr)

    | Ast.SWITCH(exp,cases,default,attr) ->
       Ast.SWITCH(compile_exp cv cpu exp,List.map
	 (function Ast.SEQ_CASE(idlist,x,stmt,y) ->
	   Ast.SEQ_CASE(idlist,x,compile_stmt cv cpu stmt,y))
	 cases,Aux.app_option (compile_stmt cv cpu) default,attr)
    | Ast.SEQ(decls,stmts,attr) ->
       Ast.SEQ(List.map (compile_decl cv cpu) decls,List.map (compile_stmt cv cpu) stmts,attr)

    | Ast.RETURN(None,attr) -> Ast.RETURN(None,attr)

    | Ast.RETURN(Some(exp),attr) -> Ast.RETURN(Some(compile_exp cv cpu exp),attr)

    | Ast.MOVE(exp,state,x,y,z,a,b,attr) ->
	Ast.MOVE(compile_exp cv cpu exp,compile_exp cv cpu state,x,y,z,a,b,attr)

    | Ast.MOVEFWD(exp,x,state_end,attr) ->
	Ast.MOVEFWD(compile_exp cv cpu exp,x,state_end,attr)

    | Ast.ASSIGN(expl,expr,sorted_fld,attr) -> (
      match expl with
        Ast.VAR(_,_,vattr)
      | Ast.FIELD(_,_,_,vattr) ->
	 (match B.ty vattr with
           B.SET(B.CORE) ->
	     (match expr with
	       Ast.BINARY(Ast.MINUS, lexp, rexp, battr) ->
		 if expl = lexp
		 && B.ty (Ast.get_exp_attr rexp) = B.CORE
		 then
		   Ast.PRIMSTMT(Ast.mkVAR("cpumask_clear_cpu", [], B.line attr),
				    [Ast.FIELD(rexp, B.mkId "id", [], B.updty (B.dum __LOC__) B.INT);
				     lexp], battr)
		 else
		   Ast.PRIMSTMT(A.VAR(B.mkId("cpumask_copy"),[],attr),
				[Ast.AREF(expl, None, attr);
                                 Ast.AREF(expr, None, attr)],
				attr)
	      | _ ->
                 Ast.PRIMSTMT(A.VAR(B.mkId("cpumask_copy"),[],attr),
			      [Ast.AREF(expl, None, attr);
                               Ast.AREF(expr, None, attr)],
			      attr)
	     )
         | _ -> Ast.ASSIGN(compile_exp cv cpu expl,compile_exp cv cpu expr,
                           sorted_fld,attr)
         )
      | _ -> Ast.ASSIGN(compile_exp cv cpu expl,compile_exp cv cpu expr,sorted_fld,attr))

    | Ast.PRIMSTMT(f,args,attr) ->
	Ast.PRIMSTMT(f,List.map (compile_exp cv cpu) args,attr)

    | Ast.ASSERT(exp,attr) -> Ast.ASSERT(compile_exp cv cpu exp,attr)

    | x -> x

         (*
let compile_new sc (defid,defattr) stmt =
  match stmt with
    A.SEQ(lv, stmts, attr) ->
    A.mkSEQ(lv,
            (List.map (fun (A.VARDECL(_,id,_,_,_,_,attr)) ->
                 A.IF(A.PRIM(A.UNARY(A.NOT,
                                     A.VAR(B.mkId("zalloc_cpumask_var"),[],attr),
                                     attr),
                             [A.AREF(A.FIELD(A.VAR(defid,[],defattr),
                                             id,
                                             [],
                                             attr),
                                     None,
                                     attr);
                              A.VAR(B.mkId("GFP_KERNEL"),[],attr)],
                             attr),
                      A.RETURN(Some (A.mkINT(-1,B.line attr)), attr),
                      A.mkSEQ([],[],-1),
                      attr))
                      sc)@stmts,
            B.line attr)
  | stmt ->
     let attr = B.mkAttr (-1) in
     A.mkSEQ([],
             (List.map (fun (A.VARDECL(_,id,_,_,_,_,attr)) ->
                  A.IF(A.PRIM(A.UNARY(A.NOT,
                                      A.VAR(B.mkId("zalloc_cpumask_var"),[],attr),
                                      attr),
                              [A.AREF(A.FIELD(A.VAR(defid,[],defattr),
                                              id,
                                              [],
                                              attr),
                                      None,
                                      attr);
                               A.VAR(B.mkId("GFP_KERNEL"),[],attr)],
                              attr),
                       A.RETURN(Some (A.mkINT(-1,B.line attr)), attr),
                       A.mkSEQ([],[],-1),
                       attr))
                       sc)@[stmt],
             (-1))
          *)
         
let compile_detach sc (A.SEQ(lv, stmts, attr)) =
  (* let A.VALDEF(,_,_,defattr) = List.hd lv in *)
  let stmts =
    match stmts with
      [] -> stmts
    | hd::[] -> (match hd with
                   A.RETURN(_,_) -> []
                 | _ -> [hd])
    | hd::r -> (match List.hd (List.rev r) with
                  A.RETURN(_,_) -> hd::(List.tl (List.rev r))
                | _ -> hd::r)
  in
  (* let free =
  @(List.map (fun (A.VARDECL(_,id,_,_,_,_,attr)) ->
                     (A.PRIMSTMT(A.VAR(B.mkId("free_cpumask_var"),[],attr),
                                 [A.FIELD(A.VAR(B.mkId("tgt"),[],(* def *)attr),
                                          id,
                                          [],
                                          attr)],
                                 attr)))
                          (List.rev sc))
 *)
  A.mkSEQ(lv, stmts, B.line attr)

let compile_schedule cpu psl stmt =
  let (vars, nr_tasks) = (* FIXME: Must handle vars for completeness *)
    List.fold_left
      (fun (pl, ql) ps ->
        match ps with
          Ast.QUEUE(CS.READY, _,_,id,_,_,_) -> (pl, id::ql)
        | Ast.PROCESS(CS.READY, _, id, _,_,_) -> (id::pl, ql)
        | _ -> (pl, ql)
      )
      ([],[]) psl
  in
  let dum = B.dum __LOC__ in
  let (check, check_nr_tasks) =
    match nr_tasks with
      [] -> (false, Ast.BOOL(true, B.dum __LOC__))
    | hd::tail ->
       let Some cpu = cpu in
       let rqty = B.updty dum (B.OPAQUE "ipanema_rq") in
       let r = List.fold_left (fun acc nr ->
           Ast.BINARY(Ast.OR,
                      Ast.FIELD(
                          Ast.FIELD(Ast.mkPRIM("ipanema_state", [cpu], -1),
                                    nr, [], rqty),
                          B.mkId "nr_tasks", [], dum),
                      acc, dum))
                 (Ast.FIELD(
                      Ast.FIELD(Ast.mkPRIM("ipanema_state", [cpu], -1),
                                hd, [], rqty),
                      B.mkId "nr_tasks", [], dum))
                 tail
       in (true, r)

  in
  let emptytest =
    let ret = Ast.RETURN(None, dum) in
    Ast.IF(Ast.UNARY(Ast.NOT, check_nr_tasks, B.updty (dum) BOOL),
           Ast.SEQ([],[ret],dum),
           Ast.SEQ([],[],dum),dum)
  in
  if check then
    match stmt with
    | Ast.SEQ(decls, stmts,attr) ->
       Ast.SEQ(decls, emptytest::stmts,attr)
    | x -> Ast.SEQ([], [emptytest;x], dum)
  else stmt


(* -------------------------- top-level blocks --------------------------- *)
(* cv: core variables *)
(* sc: sets of core (used to add alloc and free on cpumask_var_t) *)
let compile_handler psl cv sc (A.EVENT(nm, v, stmt, syn,attr)) =
  let A.EVENT_NAME(eid,eattr) = nm in
  let cpu =
    let dum = B.mkAttr (-1) in
    match B.id2c eid with
      "schedule" -> Some (Ast.VAR(B.mkId "cpu",[],B.updty dum B.INT))
    | _ ->
       let Ast.VARDECL(_,eparam,_,_,_,_,epattr) = v in
       Some (Ast.FIELD(Ast.VAR(eparam,[], epattr),
		       B.mkId "cpu", [], B.updty dum B.INT))
  in
  let new_stmt =
    match B.id2c eid with
      "detach" -> compile_detach sc stmt
(*    | "new_prepare" ->
       let A.VARDECL(_,p,_,_,_,_,pattr) = v in
       compile_new sc (B.mkId("tgt"), pattr) stmt *)
    | "schedule" -> compile_schedule cpu psl stmt
    | _ -> stmt
  in
  A.EVENT(nm, v, compile_stmt cv cpu new_stmt, syn, attr)

let compile_function cv sc (A.FUNDEF(tl, ty, nm, pl, stmt, inl, attr)) =
  A.FUNDEF(tl, ty, nm, pl, compile_stmt cv None stmt, inl, attr)

let compile_steal_thread_blk env = function
((self, fv, fe), (sv, se1, se2), (mv1, mv2, mv3, mv4, ms, se, ms2)) ->
  ((self, fv, compile_stmt env None fe),
   (sv, se1, compile_stmt env None se2),
   (mv1, mv2, mv3, mv4, compile_stmt env None ms, compile_exp env None se, ms2))

let compile_steal_group env = function
  ((self, fv, fe), (sv1, sv2, se1, se2), se) ->
   ((self,fv, compile_stmt env None fe),
    (sv1, sv2,se1, compile_stmt env None se2),
    compile_exp env None se
   )

let compile_steal_thread env steal =
  match steal with
    Ast.FLAT_STEAL_THREAD st ->
      Ast.FLAT_STEAL_THREAD (compile_steal_thread_blk env st)
  | Ast.ITER_STEAL_THREAD (st, until, post) ->
     Ast.ITER_STEAL_THREAD (compile_steal_thread_blk env st,
			    compile_exp env None until,
			    List.map (compile_stmt env None) post)


let compile_steal env steal =
  match steal with
    Ast.STEAL_THREAD s -> Ast.STEAL_THREAD (compile_steal_thread env s)
  | Ast.ITER_STEAL_GROUP (sg, st, post, postgrp) ->
     Ast.ITER_STEAL_GROUP (compile_steal_group env sg,
			   compile_steal_thread env st,
			   List.map (compile_stmt env None) post,
			   List.map (compile_stmt env None) postgrp)
  | Ast.FLAT_STEAL_GROUP (sg, st, postgrp) ->
     Ast.FLAT_STEAL_GROUP (compile_steal_group env sg,
			    compile_steal_thread env st,
			   List.map (compile_stmt env None) postgrp)

let compile_steal_param env (dom, dstg, dst, steal) =
  (dom, dstg, dst, compile_steal env steal)

(* -------------------------- entry point --------------------------- *)

let compile_core
      (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		     fundecls,valdefs,domains,pcstate,cstates,criteria,
		     trace,handlers,chandlers,ifunctions,functions,attr)) =
  let (new_pcstate,new_handlers, new_chandlers, new_ifunctions, new_functions) =
    let sc = List.filter (fun (A.VARDECL(typ,_,_,_,_,_,_)) ->
                 match typ with
                   B.SET(B.CORE) -> true
                 | _ -> false)
                         procdecls
    in
    match pcstate with
      Ast.CORE(cvd, psl, steal) ->
      let cv = List.map (fun (A.VARDECL(_, id, _, _, _, _, _)) -> id) cvd in
      (Ast.CORE(cvd,psl,
		Aux.option_apply (compile_steal_param cv) steal),
       List.map (compile_handler psl cv sc) handlers,
       List.map (compile_handler psl cv []) chandlers,
       List.map (compile_function cv sc) ifunctions,
       List.map (compile_function cv []) functions)
    | Ast.PSTATE psl ->
       (pcstate,
	List.map (compile_handler psl [] sc) handlers,
        chandlers,
        List.map (compile_function [] sc) ifunctions,
        functions)
  in
  Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		fundecls,valdefs,domains,new_pcstate,cstates,criteria,
		trace,new_handlers,new_chandlers,new_ifunctions,new_functions,attr)
