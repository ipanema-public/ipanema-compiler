(* in this file we will move initgalization to the new handler *)
module B = Objects
module CS = Class_state

let rec error ln str =(
Error.update_error();
Printf.printf "%s: %s \n" (B.loc2c ln) str);;
let rec warning ln str =( Printf.printf "warning: %s: %s \n" (B.loc2c ln) str);;
let rec pr = (Printf.sprintf);;


let rec compile_statment prodeclNew stmt handlerID=(
  let stmtsNew = List.map (function (Ast.VARDECL(ty , id , imported , b2 , csh , expr , attr1) as vdecl) ->
    let ex = match expr with
    | None -> raise (Error.Error "Unexpected expression")
    | Some x -> x
    in
      Ast.mkASSIGN(Ast.FIELD(Ast.mkVAR("tgt" , [],B.line attr1 ), id  ,[],B.updty attr1 ty) , ex ,  B.line attr1 )
  ) prodeclNew
  in
    match stmt with
    | Ast.SEQ(decls,stmtList,attr) ->
      Ast.SEQ( decls , (stmtsNew@stmtList) ,attr)
    | stmt ->  Ast.mkSEQ ( [] ,(stmtsNew@[stmt]), 0)

    );;

let compile_handlers prodeclNew handlers =(
  List.map
    (
      function (Ast.EVENT(Ast.EVENT_NAME(nm, eattr), (Ast.VARDECL(ty,id,_,_,_,_,_) as vd) ,stmt,syn,attr) as e) ->
      if (B.id2c nm = "new") then (*for the new handler we need to add the declarations*)
        let newstmtList = (compile_statment prodeclNew stmt (B.id2c id)) in
        Ast.EVENT(Ast.EVENT_NAME(nm , eattr ),vd, newstmtList, syn, attr)
      else
        Ast.EVENT(Ast.EVENT_NAME( nm, eattr ),vd, stmt, syn, attr)
    )
    handlers
    )

let rec move_initialization (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
  fundecls,valdefs, domains,pcstate,cstates, criteria,trace,handlers,chandlers,
  ifunctions,functions,attr)) =

    let prodeclNew = List.filter (
      function Ast.VARDECL(ty , id , imported , _ , _ , expr , attr)  ->
        ((imported = false) && (not (expr = None))) ) procdecls in

        (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
          fundecls,valdefs, domains,pcstate,cstates, criteria,trace,
          (compile_handlers prodeclNew handlers ),
          chandlers, ifunctions,functions,attr))
