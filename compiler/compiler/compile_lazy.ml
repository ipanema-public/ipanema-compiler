module A = Ast
module B = Objects
module T = Type
module CS = Class_state

(* ----------------------- Environment lookup ----------------------- *)

exception LookupErrException

let rec lookup_id x = function
    [] -> raise LookupErrException
  | ((y,v)::r) -> if B.ideq(x,y) then v else lookup_id x r

let is_state id =
  try let _ = lookup_id id (!CS.state_env) in true
  with LookupErrException -> false

let lazyfns = ref []

(* -------------------------- expression and stmt --------------------------- *)
let is_lazy ty lv id =
  let (dv,gv,pv,cv) = lv in
  match ty with
    B.PROCESS -> List.mem id pv
  | B.CORE -> List.mem id cv
  | B.DOMAIN -> List.mem id dv
  | B.GROUP -> List.mem id gv
  | _ -> false

let get_lazy_fname ty id =
  let res = match ty with
    (* Name must be in sync with the ones generated in Plazy
       and defined in bossa2c.ml *)
      B.PROCESS -> "ipa_task_"^B.id2c id
    | B.CORE -> "ipa_core_"^B.id2c id
    | B.DOMAIN -> "ipa_dom_"^B.id2c id
    | B.GROUP -> "ipa_grp_"^B.id2c id
    | _ -> failwith (__LOC__ ^ "unexpected type for using lazy")
  in
  let fn = Ast.FUNDECL(Ast.DFT, B.INT, B.mkId res, [B.CONST ty], B.mkAttr (-1)) in
  if not (List.mem_assoc res !lazyfns) then
    lazyfns := (res, fn):: !lazyfns;
  res

let rec compile_exp lv e =
  match e with
    Ast.VAR(id, ssa, attr) as exp -> exp
  | Ast.FIELD(exp, id, ssa, attr) ->
     let ty = B.ty (Ast.get_exp_attr exp) in
     if is_lazy ty lv id
     && not (B.id2c id = "cload" && ty = B.CORE) then
       let fn = get_lazy_fname ty id in
       Ast.PRIM(Ast.VAR(B.mkId fn, [], attr), [compile_exp lv exp], attr)
     else
       Ast.FIELD(compile_exp lv exp, id, ssa, attr)
  | Ast.LCORE("core", Ast.VAR(id, _, attr), lrc, _) ->
     let ty = B.CORE in
     if is_lazy ty lv id then
       let fn = get_lazy_fname ty id in
       let line = B.line attr in
       let coreexp = match lrc with
	   None -> Ast.PRIM(Ast.mkVAR("smp_processor_id", [], line),
		            [], (B.updty (B.mkAttr line) B.INT))
	 | Some e -> e
       in
       let core =
         Ast.mkPRIM("per_cpu",
                    [Ast.mkVAR("core",[], line); coreexp], line)
       in
       Ast.PRIM(Ast.mkVAR(fn, [], line), [core], attr)
     else
       e

  | Ast.UNARY(uop,exp,attr) ->
     Ast.UNARY(uop,compile_exp lv exp,attr)

  | Ast.BINARY(bop,exp1,exp2,attr) ->
      let exp1 = compile_exp lv exp1
      and exp2 = compile_exp lv exp2 in
      Ast.BINARY(bop,exp1,exp2,attr)

  | Ast.INDR(exp,attr) -> Ast.INDR(compile_exp lv exp,attr)

  | Ast.VALID(exp, s, a) -> Ast.VALID(compile_exp lv exp, s, a)

  | Ast.PRIM(fn,pl,a) -> Ast.PRIM(fn, List.map (compile_exp lv) pl ,a)

  | Ast.IN(exp1, exp2, s, t, b, c, a) ->
     Ast.IN(compile_exp lv exp1, compile_exp lv exp2, s, t, b, c, a)

  | Ast.FIRST(exp, crit, stopt, attr) ->
     Ast.FIRST(compile_exp lv exp,
	       Aux.option_apply (compile_crit lv (B.ty attr)) crit, stopt, attr)

  | exp -> exp

and compile_crit lv ty c =
  match c with
    CRIT_ID _ -> c
  | CRIT_EXPR(key, order, e, id, attr) ->
     let Ast.VAR(cid, ssa, attr0) = e in
     let new_e =
       if is_lazy ty lv cid
	 && not (B.id2c cid = "cload" && ty = B.CORE) then
	 let fn = get_lazy_fname ty cid in
	 Ast.PRIM(Ast.VAR(B.mkId fn, [], attr), [], attr)
       else
	 e
     in CRIT_EXPR(key, order, new_e, id, attr)
  | CRIT_EXPR(key, order, e, id, attr) ->
     CRIT_EXPR(key, order, compile_exp lv e, id, attr)

let compile_decl lv d =
  match d with
    Ast.VALDEF(vd, exp, isc, attr) -> Ast.VALDEF(vd, compile_exp lv exp, isc, attr)
  | decl -> decl

let rec compile_stmt lv s =
  match s with
    Ast.IF(exp,stmt1,stmt2,attr) ->
      Ast.IF(compile_exp lv exp,compile_stmt lv stmt1,compile_stmt lv stmt2,attr)

    | Ast.FOR(id,None,x,dir,stmt,crit,attr) ->
	Ast.FOR(id,None,x,dir,compile_stmt lv stmt,crit,attr)

    | Ast.FOR(id,Some state_id,x,dir,stmt,crit,attr) ->
	Ast.FOR(id,Some (List.map (compile_exp lv) state_id),x,dir,compile_stmt lv stmt,crit,attr)

    | Ast.FOR_WRAPPER(label,stmts,attr) ->
	Ast.FOR_WRAPPER(label,List.map (compile_stmt lv) stmts,attr)

    | Ast.SWITCH(exp,cases,default,attr) ->
       Ast.SWITCH(compile_exp lv exp,List.map
	 (function Ast.SEQ_CASE(idlist,x,stmt,y) ->
	   Ast.SEQ_CASE(idlist,x,compile_stmt lv stmt,y))
	 cases,Aux.app_option (compile_stmt lv) default,attr)
    | Ast.SEQ(decls,stmts,attr) ->
       Ast.SEQ(List.map (compile_decl lv) decls,List.map (compile_stmt lv) stmts,attr)

    | Ast.RETURN(None,attr) -> Ast.RETURN(None,attr)

    | Ast.RETURN(Some(exp),attr) -> Ast.RETURN(Some(compile_exp lv exp),attr)

    | Ast.MOVE(exp,state,x,y,z,a,b,attr) ->
	Ast.MOVE(compile_exp lv exp,compile_exp lv state,x,y,z,a,b,attr)

    | Ast.MOVEFWD(exp,x,state_end,attr) ->
	Ast.MOVEFWD(compile_exp lv exp,x,state_end,attr)

    | Ast.ASSIGN(expl,expr,sorted_fld,attr) ->
      Ast.ASSIGN(compile_exp lv expl,compile_exp lv expr,sorted_fld,attr)

    | Ast.PRIMSTMT(f,args,attr) ->
	Ast.PRIMSTMT(f,List.map (compile_exp lv) args,attr)

    | Ast.ASSERT(exp,attr) -> Ast.ASSERT(compile_exp lv exp,attr)

    | x -> x

(* -------------------------- steal block -------------------------------- *)
let compile_filter lv (self, v, exp) = (self, v, compile_stmt lv exp)

let compile_select_grp lv (v1, v2, v3, exp2) = (v1, v2, v3, compile_stmt lv exp2)
let compile_select lv (v, v2, exp2) = (v, v2, compile_stmt lv exp2)

let compile_migrate lv (v1, v2, v3, v4, stmt1, exp2, stmt2) =
  (v1, v2, v3, v4, compile_stmt lv stmt1, compile_exp lv exp2, stmt2)

let compile_steal_blk lv (filter, select, migrate) =
  (compile_filter lv filter,
   compile_select lv select,
   compile_migrate lv migrate)

let compile_steal_group lv (filter, select, expr) =
  (compile_filter lv filter,
   compile_select_grp lv select,
   compile_exp lv expr)

let compile_steal_thread env steal =
  match steal with
    Ast.FLAT_STEAL_THREAD st ->
      Ast.FLAT_STEAL_THREAD (compile_steal_blk env st)
  | Ast.ITER_STEAL_THREAD (st, until, post) ->
     Ast.ITER_STEAL_THREAD (compile_steal_blk env st,
			    compile_exp env until,
			    List.map (compile_stmt env) post)

let compile_steal env steal =
  match steal with
    Ast.STEAL_THREAD s -> Ast.STEAL_THREAD (compile_steal_thread env s)
  | Ast.ITER_STEAL_GROUP (sg, st, post, postgrp) ->
     Ast.ITER_STEAL_GROUP (compile_steal_group env sg,
			   compile_steal_thread env st,
			   List.map (compile_stmt env) post,
			   List.map (compile_stmt env) postgrp)
  | Ast.FLAT_STEAL_GROUP (sg, st, postgrp) ->
     Ast.FLAT_STEAL_GROUP (compile_steal_group env sg,
			   compile_steal_thread env st,
			   List.map (compile_stmt env) postgrp)

let compile_steal_param env (dom, dstg, dst, steal) =
  (dom, dstg, dst, compile_steal env steal)

(* -------------------------- top-level blocks --------------------------- *)

(* lv: lazy variables *)
let compile_handler lv (A.EVENT(nm, v, stmt, syn, attr)) =
  A.EVENT(nm, v, compile_stmt lv stmt, syn, attr)

let compile_function lv (A.FUNDEF(tl, ty, nm, pl, stmt, inl, attr)) =
  A.FUNDEF(tl, ty, nm, pl, compile_stmt lv stmt, inl, attr)

(* -------------------------- entry point --------------------------- *)

let get_lazy decls =
  List.fold_left (fun acc (A.VARDECL(_,id,_,lzy,_,_,_)) ->
    if lzy then id :: acc else acc) [] decls

let compile_lazy
      (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		     fundecls,valdefs,domains,pcstate,cstates,criteria,
		     trace,handlers,chandlers,ifunctions,functions,attr)) =
  let (pcstate, new_handlers, new_chandlers, new_ifunctions, new_functions) =
    let dv = get_lazy dom in
    let gv = get_lazy grp in
    let pv = get_lazy procdecls in
    match pcstate with
      Ast.CORE(cvd, states, steal) ->
	let cv = get_lazy cvd in
	let lv = (dv, gv, pv, cv) in
      (Ast.CORE(cvd, states, Aux.option_apply (compile_steal_param lv) steal),
       List.map (compile_handler lv) handlers,
       List.map (compile_handler lv) chandlers,
       List.map (compile_function lv) ifunctions,
       List.map (compile_function lv) functions)
    | Ast.PSTATE _ ->
       let lv = (dv, gv, pv, []) in
       (pcstate,
	List.map (compile_handler lv) handlers,
        List.map (compile_handler lv) chandlers,
        List.map (compile_function lv) ifunctions,
        functions)
  in
  let lazys = snd(List.split !lazyfns) in
  Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		lazys@fundecls,valdefs,domains,pcstate,cstates,criteria,
		trace,new_handlers,new_chandlers,new_ifunctions,new_functions,attr)
