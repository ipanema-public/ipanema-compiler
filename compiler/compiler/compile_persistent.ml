(* $Id$ *)

(* management of persistent timers
   - add creation and deletion of process timers to new/end, attach/detach
   - add increment of reference count when end/detach for each running timer
   - add decrement of reference count when unblock.timer if tgt not alive
   - add update of currently_running = true to start_abs/rel_timer
   - add update of currently_running = false to stop_timer
*)

(* do we still want the reference count in the Linux 2.6 module model? *)

module B = Objects

(* -------------------- Miscellaneous operations -------------------- *)

let error attr str =
  raise(Error.Error(Printf.sprintf "%s: %s" (B.loc2c attr) str))

(* --------------------------- Add at end --------------------------- *)

(* For delete_timer, add near the bottom of process.end/detach, after any
starting of timers.  For create_timer add near the beginninf of
process.new/attach, after any errors.  There are no errors in process.new,
and attach has all of the errors at the beginning, so in both cases, this
should ensure that the timer is created before any code to start it. *)

let rec add_in_list end_code in_attach = function
    [] -> (false,[])
  | stmt :: rest ->
      match stmt with
	Ast.IF(tst,thn,els,attr) ->
	  let (placed1,thn) = add_at_end end_code in_attach thn attr in
	  let (placed2,els) = add_at_end end_code in_attach els attr in
	  (match (placed1,placed2) with
	    (true,true) -> (true,Ast.IF(tst,thn,els,attr) :: rest)
	  | (true,false) ->
	      (true,
	       Ast.IF(tst,thn,Ast.mkBLOCK attr [end_code;els],attr) :: rest)
	  | (false,true) ->
	      (true,
	       Ast.IF(tst,Ast.mkBLOCK attr [end_code;thn],els,attr) :: rest)
	  | (false,false) ->
	      let (placed,rest) = add_in_list end_code in_attach rest in
	      (placed, stmt :: rest))
      | Ast.FOR(id,stnm,states,dir,body,crit,attr) ->
	  let (placed,body) = add_at_end end_code in_attach body attr in
	  if placed
	  then (* can't put end_code in a loop *)
	    (true, end_code :: stmt :: rest)
	  else
	    let (placed,rest) = add_in_list end_code in_attach rest in
	    (placed, stmt :: rest)
      | Ast.FOR_WRAPPER(brklbl,stmts,attr) ->
	  let (placed,stmts) =
	    add_in_list end_code in_attach (List.rev stmts) in
	  if placed
	  then (* can't put end_code in a loop *)
	    (true,end_code :: stmt :: rest)
	  else
	    let (placed,rest) = add_in_list end_code in_attach rest in
	    (placed, stmt :: rest)
      | Ast.SWITCH(exp,cases,def,attr) ->
	  let (placed,new_cases) =
	    List.fold_left
	      (function (placed,new_cases) ->
		function Ast.SEQ_CASE(pat,states,stmt,attr) ->
		  let (placed1,new_stmt) = add_at_end end_code in_attach stmt attr in
		  (placed1::placed,
		   Ast.SEQ_CASE(pat,states,new_stmt,attr)::cases))
	      ([],[]) cases in
	  let new_cases = List.rev new_cases in
	  let any_placed = List.exists (function x -> x) placed in
	  let mk_newer_cases _ =
	    List.map2
	      (function Ast.SEQ_CASE(pat,states,stmt,attr) ->
		function
		    true -> Ast.SEQ_CASE(pat,states,stmt,attr)
		  | false ->
		      Ast.SEQ_CASE(pat,states,
				   Ast.SEQ([],[end_code;stmt],attr),
				   attr))
	      new_cases placed in
	  (match def with
	    None ->
	      if any_placed
	      then (true,Ast.SWITCH(exp,mk_newer_cases (),def,attr) :: rest)
	      else 
		let (placed,rest) = add_in_list end_code in_attach rest in
		(placed, stmt :: rest)
	  | Some stm ->
	      let (placed1,stm) = add_at_end end_code in_attach stm attr in
	      let any_placed = placed1 || any_placed in
	      if any_placed
	      then (true,Ast.SWITCH(exp,mk_newer_cases (),
				    (if placed1
				    then Some stm
				    else
				      Some(Ast.SEQ([],[end_code;stm],attr))),
				    attr) :: rest)
	      else 
		let (placed,rest) = add_in_list end_code in_attach rest in
		(placed, stmt :: rest))
      | Ast.SEQ(ldecls,stmts,attr) ->
	  let (placed1,stmts1) =
	    add_in_list end_code in_attach (List.rev stmts) in
	  if placed1
	  then (placed1, Ast.SEQ(ldecls,List.rev stmts1,attr) :: rest)
	  else
	    let (placed,rest) = add_in_list end_code in_attach rest in
	    (placed, stmt :: rest)
      | Ast.PRIMSTMT(Ast.VAR(f,_,_),_,_)
	when (not in_attach) &&
	  List.mem (B.id2c f)
	    ["start_relative_timer";"start_absolute_timer";"stop_timer"] ->
	  (* stuff we can't move past *)
	  (true,(end_code :: stmt :: rest))
      | Ast.PRIMSTMT(_,_,_)
      | Ast.RETURN(_,_)
      | Ast.DEFER(_)
      | Ast.MOVE(_,_,_,_,_,_,_,_)
      | Ast.MOVEFWD(_,_,_,_)
      | Ast.SAFEMOVEFWD(_,_,_,_,_)
      | Ast.ASSIGN(_,_,_,_)
      | Ast.BREAK(_)
      | Ast.CONTINUE(_)
      | Ast.ASSERT(_,_) ->
	  (* stuff we can move past *)
	  let (placed,rest) = add_in_list end_code in_attach rest in
	  (placed, stmt :: rest)
      | Ast.ERROR(str,attr) ->
	  if in_attach
	  then (true,(stmt :: rest))
	  else error attr "unexpected error code"

and add_at_end end_code in_attach stmt attr =
  let (placed,stmts) = add_in_list end_code in_attach [stmt] in
  (placed,Ast.mkBLOCK attr (List.rev stmts))

let enter_add_at_end end_code in_attach stmt attr =
  match add_at_end end_code in_attach stmt attr with
    (true,s) -> s
  (* didn't find anywhere it has to be, so put it at the beginning *)
  | (false,s) -> Ast.addBLOCK attr end_code s

(* ----------- Starting and stopping of persistent timers ----------- *)

let persistent_event timer =
  "EVENT_UNBLOCK_TIMER_PERSISTENT_"^String.uppercase(B.id2c(timer))

let get_persistent_timer = function
    (a1::rest) ->
      let attr = Ast.get_exp_attr a1 in
      (match B.ty attr with
	B.TIMER(B.PERSISTENT(_)) ->
	  (match a1 with
	    Ast.FIELD(tgt,timer,_,attr) ->
            Some(tgt,Ast.VAR(B.mkId(persistent_event timer),[],attr))
	  | _ -> error attr "unsupported persistent timer reference")
      |	_ -> None)
  | _ -> None

(* Event indices are left shifted by 8.  Here we want small numbers *)
let rs8 timer_event =
  Ast.BINARY(Ast.RSHIFT,timer_event,Ast.INT(8,B.mkAttr 0),B.mkAttr 0)

let mk_cr_update tgt index attr vl =
  Ast.ASSIGN(
      Ast.PRIM(Ast.VAR(B.mkId "field_array_ref",[],attr),(*a1.a2[a3]*)
       [tgt;Ast.VAR(B.mkId "currently_running",[],attr);
	     rs8 index],attr),
  Ast.BOOL(vl,attr),false,attr)

let manage_persistent_timer nm param timer_procdecls body attr =
  body
(*  match Ast.event_name2c2 nm with
    Some "unblock.timer" ->
      (match nm with
	Ast.EVENT_NAME(_::_::timer::_,_,_) ->
	  let is_persistent =
	    List.exists
	      (function
		  (id,B.PERSISTENT(_)) -> timer = id
		| _ -> false)
	      timer_procdecls in
	  if is_persistent
	  then
	    let get_tgt _ =
	      let ps = B.PROCESS in
	      let Ast.VARDECL(_,p,_,_,_,_) = param in
          Ast.FIELD(Ast.VAR(p,[],B.updty attr B.PEVENT),B.mkId("target"),
            [],B.updty attr ps) in
        let index = Ast.VAR(B.mkId(persistent_event timer),[],attr) in
	    let off =
	      Ast.IF(Ast.ALIVE(B.BOTH,false,B.updty attr B.BOOL),
		     mk_cr_update (get_tgt()) index attr false,
		     (match !B.config with
		       B.LINUX -> 
			 Ast.PRIMSTMT(Ast.VAR(B.mkId("decrement_ref_count"),
                          [],attr),
				      [],attr)
		     | B.LINUX26 -> Ast.SEQ([],[],attr)),
		     attr) in
	    Ast.addBLOCK attr off body
	  else body
      |	_ -> body)
  | _ -> body
 *)

(* -------------------- Create and delete timers -------------------- *)

(* only works if there are separate handlers for new and end, but the event
types rather require this... *)

let collect_timer_process_fields procdecls =
  let check_vardecl = function
      Ast.VARDECL(B.TIMER(ty),id,_,_,_,_,_) -> Some (id,ty)
    | _ -> None in
  Aux.option_filter check_vardecl procdecls

let make_create_timer tgt attr (timer,ty) =
  let timer_ty = B.TIMER(ty) in
  match ty with
    B.NOT_PERSISTENT ->
        Ast.ASSIGN(Ast.FIELD(tgt,timer,[],B.updty attr timer_ty),
         Ast.PRIM(Ast.VAR(B.mkId("create_bossa_timer"),[],attr),
		      [tgt;Ast.VAR(B.mkId("EXPORTS.next_list.next->next"),
                   [],attr);
			Ast.VAR(B.mkId("EVENT_UNBLOCK_TIMER_TARGET_"^
				       String.uppercase(B.id2c(timer))),
                [],attr)],
		      attr),
	     false,
	     attr)
  | B.PERSISTENT(str) ->
      let sz =
	match str with
	  None -> Ast.INT(0,B.updty attr B.INT)
	| Some(id) ->
            Ast.PRIM(Ast.VAR(B.mkId "sizeof_struct",[],attr),
             [Ast.VAR(id,[],B.updty attr timer_ty)],
		     attr) in
      let timer_fld = Ast.FIELD(tgt,timer,[],B.updty attr timer_ty) in
      let cur_event = Ast.VAR(B.mkId(persistent_event timer),[],attr) in
      let s1 =
	Ast.ASSIGN(timer_fld,
		   Ast.PRIM(Ast.VAR(B.mkId("create_persistent_bossa_timer"),
                    [],attr),
			    [tgt;Ast.VAR(B.mkId("EXPORTS.next_list.next->next"),
                     [],attr);
			      cur_event;sz],
			    attr),
		   false,
		   attr) in
      let s2 = mk_cr_update tgt cur_event attr false in
      Ast.SEQ([],[s1;s2],attr)

let make_delete_timer tgt attr (timer,ty) =
  let timer_ty = B.TIMER(ty) in
  let del =
      Ast.PRIMSTMT(Ast.VAR(B.mkId("delete_bossa_timer"),[],attr),
         [Ast.FIELD(tgt,timer,[],B.updty attr timer_ty)],
		 attr) in
  match ty with
    B.NOT_PERSISTENT -> del
  | B.PERSISTENT(_) ->
      (* this makes the timer global if it is running and destroys it
	 otherwise *)
let index = Ast.VAR(B.mkId(persistent_event timer),[],attr) in
      Ast.IF(
          Ast.PRIM(Ast.VAR(B.mkId "field_array_ref",[],attr),(*a1.a2[a3]*)
           [tgt;Ast.VAR(B.mkId "currently_running",[],attr);
		 rs8 index],attr),
      Ast.SEQ([],
	      (match !B.config with
(*		B.LINUX ->
            [Ast.PRIMSTMT(Ast.VAR(B.mkId("increment_ref_count"),[],attr),[],
				attr)]
	      | B.LINUX26 *) B.LINUX45 -> []) @
          [Ast.PRIMSTMT(Ast.VAR(B.mkId("globalize_bossa_timer"),[],attr),
                [Ast.FIELD(tgt,timer,[],B.updty attr timer_ty)],
			    attr)],
	      attr),
      del,
      attr)

let manage_timer_hd nm param timer_procdecls stmt attr =
  let get_tgt _ =
    let ps = B.PROCESS in
    let Ast.VARDECL(_,p,_,_,_,_,_) = param in
    Ast.FIELD(Ast.VAR(p,[],B.updty attr B.PEVENT),B.mkId("target"),
              [],B.updty attr ps) in
  stmt
(*
      (match (Ast.event_name2c2 nm,timer_procdecls) with
	(Some "process.new",_::_) ->
	  let tgt = get_tgt() in
	  let new_stmts =
	    List.map (make_create_timer tgt attr) timer_procdecls in
	  enter_add_at_end (Ast.mkBLOCK attr new_stmts) true stmt attr
      | (Some "process.end",_::_) ->
	  let tgt = get_tgt() in
	  let new_stmts =
	    List.map (make_delete_timer tgt attr) timer_procdecls in
	  enter_add_at_end (Ast.mkBLOCK attr new_stmts) false stmt attr
      | _ -> stmt)
 *)

let manage_timer_fn nm params timer_procdecls stmt attr =
  let get_tgt f =
    let ps = B.PROCESS in
    match params with
      (Ast.VARDECL(ty,p,_,_,_,_,_)::_) when ty = ps ->
          Ast.VAR(p,[],B.updty attr ps)
    | _ -> error attr ("unexpected arglist for "^f) in
  match (B.id2c nm,timer_procdecls) with
    ("attach",_::_) ->
      let tgt = get_tgt "attach" in
      let new_stmts = List.map (make_create_timer tgt attr) timer_procdecls in
      enter_add_at_end (Ast.mkBLOCK attr new_stmts) true stmt attr
  | ("detach",_::_) ->
      let tgt = get_tgt "detach" in
      let new_stmts = List.map (make_delete_timer tgt attr) timer_procdecls in
      enter_add_at_end (Ast.mkBLOCK attr new_stmts) false stmt attr
  | _ -> stmt

(* --------------------------- Statements -------------------------- *)

let rec compile_stmt = function
    Ast.IF(exp,stmt1,stmt2,attr) ->
      Ast.IF(exp,compile_stmt stmt1,compile_stmt stmt2,attr)
	
    | Ast.FOR(id,state_id,x,dir,stmt,crit, attr) ->
	Ast.FOR(id,state_id,x,dir,compile_stmt stmt,crit,attr)
	  
    | Ast.FOR_WRAPPER(label,stmts,attr) ->
	Ast.FOR_WRAPPER(label,List.map compile_stmt stmts,attr)
	  
    | Ast.SWITCH(exp,cases,default,attr) ->
	Ast.SWITCH(exp,
		   List.map
		     (function Ast.SEQ_CASE(idlist,x,stmt,y) ->
		       Ast.SEQ_CASE(idlist,x,compile_stmt stmt,y))
		     cases,
		   Aux.app_option compile_stmt default,attr)

    | Ast.SEQ(decls,stmts,attr) ->
	Ast.SEQ(decls,List.map compile_stmt stmts,attr)

    | Ast.PRIMSTMT(f,args,attr) as call ->
	(* update bitmap for start/stop timer *)
	(match f with
	  Ast.VAR(f,_,_) ->
	    (match B.id2c f with
	      "start_relative_timer" | "start_absolute_timer" ->
		(match get_persistent_timer args with
		  None -> call
		| Some (tgt,index) ->
		    let s1 = mk_cr_update tgt index attr true in
		    Ast.addBLOCK attr s1 call)
	    | "stop_timer" ->
		(match get_persistent_timer args with
		  None -> call
		| Some (tgt,index) ->
		    let s1 = mk_cr_update tgt index attr false in
		    Ast.addBLOCK attr s1 call)
	    | _ -> call)
	| _ -> call)

    | x -> x

(* --------------------- Handlers and functions --------------------- *)

(* parser should check that event names are ok *)
let compile_handlers handlers timer_procdecls =
  List.map
    (function (Ast.EVENT(nm,param,stmt,syn,attr)) ->
      Ast.EVENT(nm,param,
		manage_persistent_timer nm param timer_procdecls
		  (manage_timer_hd nm param timer_procdecls (compile_stmt stmt)
		     attr)
		  attr,
		syn, attr))
    handlers

let compile_ifunctions ifunctions timer_procdecls =
  List.map
    (function Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr) ->
      Ast.FUNDEF(tl,ret,nm,params,
		 manage_timer_fn nm params timer_procdecls (compile_stmt stmt)
		   attr,
		 inl,attr))
    ifunctions

(* -------------------------- entry point --------------------------- *)

let compile_persistent
    (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		   fundecls,valdefs,domains,states,cstates,criteria,
		   trace,handlers,chandlers,ifunctions,functions,attr)) =
  let timer_procdecls = collect_timer_process_fields procdecls in
  let newhandlers = compile_handlers handlers timer_procdecls in
  let newifunctions = compile_ifunctions ifunctions timer_procdecls in
  Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		fundecls,valdefs,domains,states,cstates,criteria,
		trace,newhandlers,chandlers,newifunctions,
		functions,attr)
