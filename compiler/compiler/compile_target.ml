module A = Ast
module B = Objects
module T = Type
module CS = Class_state

let policy_name = ref ""
let pv = ref []

(* -------------------------- expression and stmt --------------------------- *)

let rec check_exp = function
  Ast.FIELD(exp, id, ssa, attr)
    when B.id2c id = "target"
    && ((B.ty attr = B.PROCESS && B.ty (Ast.get_exp_attr exp) = B.PEVENT)
	|| B.ty attr = B.CORE &&  B.ty (Ast.get_exp_attr exp) = B.CEVENT)
    -> true
  | Ast.FIELD(exp, id, ssa, attr) -> check_exp exp

  | Ast.UNARY(uop,exp,attr) -> check_exp exp

  | Ast.BINARY(bop,exp1,exp2,attr) ->
      let exp1 = check_exp exp1
      and exp2 = check_exp exp2 in
      exp1 || exp2

  | Ast.INDR(exp,attr) -> check_exp exp
  | Ast.VALID(exp, s, a) -> check_exp exp
  | Ast.PRIM(fn,pl,a) -> List.exists check_exp pl
  | Ast.IN(exp1, exp2, s, t, b, c, a) -> check_exp exp1 || check_exp exp2
  | Ast.FIRST (exp, _, _, a) when B.ty a = B.CORE -> true

  | exp -> false

let check_decl = function
    Ast.VALDEF(vd, exp, isc, attr) -> check_exp exp
  | _ -> false

let rec check_stmt = function
    Ast.IF(exp,stmt1,stmt2,attr) ->
      check_exp exp || check_stmt stmt1 ||check_stmt stmt2

    | Ast.FOR(id,state_id,x,dir,stmt,crit,attr) -> check_stmt stmt

    | Ast.FOR_WRAPPER(label,stmts,attr) ->
       List.exists check_stmt stmts

    | Ast.SWITCH(exp,cases,default,attr) ->
       check_exp exp
       || List.exists (function Ast.SEQ_CASE(idlist,x,stmt,y) -> check_stmt stmt) cases
       || (match default with
	 None -> false
	 | Some stmt -> check_stmt stmt)

    | Ast.SEQ(decls,stmts,attr) ->
       List.exists check_decl decls || List.exists check_stmt stmts

    | Ast.RETURN(Some(exp),attr) -> check_exp exp

    | Ast.STEAL(exp, attr) -> check_exp exp

    | Ast.MOVE(exp,state,x,y,z,a,b,attr) -> check_exp exp || check_exp state

    | Ast.MOVEFWD(exp,x,state_end,attr) -> check_exp exp

    | Ast.ASSIGN(expl,expr,sorted_fld,attr) ->
       check_exp expl || check_exp expr

    | Ast.PRIMSTMT(f,args,attr) -> List.exists check_exp args

    | Ast.ASSERT(exp,attr) -> check_exp exp

    | x -> false

(* -------------------------- top-level blocks --------------------------- *)
let need_ppid pdecls =
  List.fold_left
    (fun acc  pdecl ->
      match pdecl with
	Ast.VARDECL(B.PROCESS,vid, true, false,_,None,_) when B.id2c vid = "parent" -> true
      | _ -> acc) false pdecls

let compile_handler is_core (A.EVENT(nm, param, stmt, syn, attr)) =
  let A.EVENT_NAME(B.ID(enm, _),_) = nm in
  if ((not (check_stmt stmt) || enm = "new_end" || enm = "unblock_end")
     && enm <> "new_prepare" && enm <> "yield" && enm <> "tick" && enm <> "block") then
    A.EVENT(nm, param, stmt, syn, attr)
  else
    let stmt = Compile_tgt.rename stmt in
    let (paramstr, paramid) = match param with
	Ast.VARDECL(_,id,_,_,_,_,_) -> (B.id2c id, id)
      | _ -> failwith "handler list must not be empty, only one param allowed"
    in
    let Ast.EVENT_NAME(nmstr, _) = nm in
    let line = B.line attr in
    let tgt = "tgt" in
    let (tgt, decl) = if is_core then
	(tgt, Ast.mkVARDECL(CS.UNSHARED, B.CORE, tgt, line))
      else
	(tgt, Ast.mkVARDECL(CS.UNSHARED, B.PROCESS, tgt, line)) in
    let def =
      if is_core
      then
	[Ast.mkVALDEF(decl,
		     Ast.AREF(
			 Ast.mkPRIM("per_cpu",
				    [Ast.mkVAR("core",[], line);
				     Ast.FIELD(Ast.mkVAR(paramstr, [], line),
                                               B.mkId "target",
                                               [],
                                               B.updty (B.mkAttr line) B.INT)
                                    ], line),
			 None, B.mkAttr line),
		     line)]
      else if enm = "new_prepare" then
        [Ast.UNINITDEF(decl, B.mkAttr line)]
      else
        let old_load = 
          match enm with
            "detach" | "tick" | "yield" | "block" ->
            [Ast.mkVALDEF(Ast.mkVARDECL(CS.UNSHARED, B.INT, "old_load", line),
                          Ast.FIELD(Ast.VAR(B.mkId tgt, [], B.dum __LOC__),
                                    B.mkId "load", [], B.dum __LOC__), __LINE__)
            ]
          | _ -> []
        in
        Ast.mkVALDEF(decl,
		     Ast.mkPRIM("policy_metadata",
				[Ast.mkFIELD(Ast.mkVAR(paramstr, [], line),
					     "target", [], line)],
				line), line)::old_load
    in
    let new_stmt =
      if enm = "new_prepare" then
	let dum = B.updgen (B.mkAttr line) __LOC__ in
	let tasktype = B.STRUCT (B.mkId "task_struct") in
	let taskdum = B.updty dum tasktype in
	let procdum = B.updty dum B.PROCESS in
	let nop = Ast.mkSEQ([],[],-1) in
	let taskid = B.fresh_idx "task" in
	let task = B.id2c taskid in
	let decl = Ast.mkVARDECL(CS.UNSHARED, B.INDR (tasktype), task, line) in
        let deftask = Ast.mkVALDEF(decl,Ast.mkFIELD(Ast.mkVAR(paramstr, [], line),
						    "target", [], line),line) in
        let proc = Ast.VAR(B.mkId tgt, [], procdum) in
	let task = Ast.VAR(taskid, [], taskdum) in
	let meta = Ast.PRIM(Ast.VAR(B.mkId "policy_metadata",[], dum),
			    [task],procdum) in
        let allocsize = "sizeof(struct "^ !policy_name ^"_ipa_process)" in
        let alloc = Ast.PRIM(Ast.VAR(B.mkId "kzalloc", [], dum),
			     [Ast.mkVAR(allocsize, [], line);
			      Ast.mkVAR("GFP_ATOMIC",[],line)],
			     procdum)
        in
        let assignalloc =	Ast.mkASSIGN(proc, alloc, line) in
        let memcheck =
	  Ast.mkIF(Ast.mkUNARY(Ast.NOT,proc,line), Ast.mkRETURN (Ast.mkINT(-1,line), line), nop, line)
        in
        let assigntgt =
	  Ast.ASSIGN(meta, proc, false, procdum)
        in
        let inittask =
	  Ast.ASSIGN(Ast.FIELD(proc,B.mkId "task",[], taskdum),
		     task, false, taskdum)
        in
        let stmtstub = [stmt] in
        let stmtstub =
	  if need_ppid !pv then
	    let ppid = Ast.FIELD(task, B.mkId "parent", [], taskdum) in
	    let meta = Ast.PRIM(Ast.VAR(B.mkId "policy_metadata", [], dum),
			        [ppid],procdum) in
            let isIPANEMA = Ast.BINARY(Ast.NEQ,
                                       Ast.FIELD(ppid, B.mkId "policy", [], dum),
                                       Ast.mkVAR("SCHED_IPANEMA", [], -1), dum) in
            let check = Ast.TERNARY(isIPANEMA, Ast.INT_NULL (B.dum __LOC__), meta, procdum) in
	    let initppid =
	      Ast.mkASSIGN(Ast.FIELD(proc,B.mkId "parent",[], procdum), check, line)
            in
            initppid::stmtstub
	  else
            stmtstub
        in
	Ast.SEQ(def@[deftask], [assignalloc;memcheck;assigntgt;
				  inittask]@stmtstub, attr)
      else
	match stmt with
	  Ast.SEQ(decls, stmts, attr) -> Ast.SEQ(def@decls, stmts, attr)
	| _ ->  Ast.SEQ(def, [stmt], attr)
    in
    A.EVENT(nm, param, new_stmt, syn, attr)

let compile_function (A.FUNDEF(tl, ty, nm, pl, stmt, inl, attr)) =
  let (new_pl, new_stmt) =
    match B.strideq("attach",nm) with
      true ->
      begin
        let line = B.line attr in
        let (pl_tail, paramid) = match pl with
            Ast.VARDECL(_,paramid,_,_,_,_,_)::pl_tail -> (pl_tail,paramid)
          | _ -> failwith "Unexpected"
        in
	let new_id = B.fresh_id () in
        (*
	 let decl = Ast.VARDECL(B.PROCESS, paramid, false, false, None, B.mkAttr line) in
        let def = Ast.UNINITDEF(decl, B.mkAttr line) in
        let task = Ast.VAR(new_id, [], B.mkAttr line) in
        let proc = Ast.VAR(paramid, [], B.mkAttr line) in
	*)
        let new_pl =
          Ast.VARDECL(B.INDR(B.STRUCT (B.mkId "task_struct")),
                      new_id,false,false,CS.UNSHARED,None,B.mkAttr line)::pl_tail
        in
	(new_pl, stmt)
      end
    | false -> (pl, stmt)
  in
  A.FUNDEF(tl, ty, nm, new_pl, new_stmt, inl, attr)

(* -------------------------- entry point --------------------------- *)

let compile_target
      (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		     fundecls,valdefs,domains,pcstate,cstates,criteria,
		     trace,handlers,chandlers,ifunctions,functions,attr)) =
  policy_name := nm;
  pv := procdecls;
  Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		fundecls,valdefs,domains,pcstate,cstates,criteria,
		trace,
		List.map (compile_handler false) handlers,
		List.map (compile_handler true) chandlers,
		List.map compile_function ifunctions,
		functions,
		attr)
