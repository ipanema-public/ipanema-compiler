module A = Ast
module B = Objects
module T = Type

(* -------------------------- expression and stmt --------------------------- *)

let rec compile_exp e =
  match e with
    Ast.VAR(id, ssa, attr) as exp -> exp
  | Ast.FIELD(exp, id, ssa, attr) ->
     Ast.FIELD(compile_exp exp, id, ssa, attr)
  | Ast.UNARY(uop,exp,attr) ->
     Ast.UNARY(uop,compile_exp exp,attr)

  | Ast.BINARY(bop,exp1,exp2,attr) ->
      let exp1 = compile_exp exp1
      and exp2 = compile_exp exp2 in
      Ast.BINARY(bop,exp1,exp2,attr)

  | Ast.INDR(exp,attr) -> Ast.INDR(compile_exp exp,attr)

  | Ast.VALID(exp, s, a) -> Ast.VALID(compile_exp exp, s, a)

  | Ast.PRIM(fn,pl,a) -> Ast.PRIM(fn, List.map (compile_exp) pl ,a)

  | Ast.IN(exp1, exp2, s, t, b, c, a) ->
     Ast.IN(compile_exp exp1, compile_exp exp2, s, t, b, c, a)

  | Ast.FIRST(exp, crit, stopt, attr)
      when B.ty attr = B.DOMAIN && crit = None && stopt = None ->
     let lineattr = B.mkAttr (B.line attr) in
     Ast.AREF(compile_exp exp, Some(Ast.INT(0,lineattr)), lineattr)

  | Ast.FIRST(exp, crit, stopt, attr)
      when B.ty attr = B.GROUP && crit = None && stopt = None ->
     let lineattr = B.mkAttr (B.line attr) in
     Ast.AREF(compile_exp exp, Some(Ast.INT(0,lineattr)), lineattr)

  | Ast.FIRST(exp, crit, stopt, attr) -> Ast.FIRST(compile_exp exp, crit, stopt, attr)

  | Ast. AREF(exp1, None, attr) -> Ast.AREF(compile_exp exp1, None, attr)
  | Ast. AREF(exp1, Some exp2, attr) -> Ast.AREF(compile_exp exp1, Some (compile_exp exp2), attr)

  | exp -> exp

let compile_decl d =
  match d with
    Ast.VALDEF(vd, exp, isc, attr) -> Ast.VALDEF(vd, compile_exp exp, isc, attr)
  | decl -> decl

let rec compile_stmt s =
  match s with
    Ast.IF(exp,stmt1,stmt2,attr) ->
      Ast.IF(compile_exp exp,compile_stmt stmt1,compile_stmt stmt2,attr)

    | Ast.FOR(id,state_id,x,dir,stmt,crit,attr) ->
	Ast.FOR(id,state_id,x,dir,compile_stmt stmt,crit,attr)

    | Ast.FOR_WRAPPER(label,stmts,attr) ->
	Ast.FOR_WRAPPER(label,List.map (compile_stmt) stmts,attr)

    | Ast.SWITCH(exp,cases,default,attr) ->
       Ast.SWITCH(compile_exp exp,List.map
	 (function Ast.SEQ_CASE(idlist,x,stmt,y) ->
	   Ast.SEQ_CASE(idlist,x,compile_stmt stmt,y))
	 cases,Aux.app_option (compile_stmt) default,attr)
    | Ast.SEQ(decls,stmts,attr) ->
       Ast.SEQ(List.map (compile_decl) decls,List.map (compile_stmt) stmts,attr)

    | Ast.RETURN(None,attr) -> Ast.RETURN(None,attr)

    | Ast.RETURN(Some(exp),attr) -> Ast.RETURN(Some(compile_exp exp),attr)

    | Ast.MOVE(exp,state,x,y,z,a,b,attr) ->
	Ast.MOVE(compile_exp exp,compile_exp state,x,y,z,a,b,attr)

    | Ast.MOVEFWD(exp,x,state_end,attr) ->
	Ast.MOVEFWD(compile_exp exp,x,state_end,attr)

    | Ast.ASSIGN(expl,expr,sorted_fld,attr) ->
       begin
	 let rexp = compile_exp expr in
	 let rewrite =
	   match rexp with
	     Ast.BINARY(Ast.MINUS, lexp, rexp, battr) ->
	       if expl = lexp
		 && B.ty (Ast.get_exp_attr lexp) = B.SET B.GROUP
		   && B.ty (Ast.get_exp_attr rexp) = B.GROUP
	       then
		 Some (Ast.PRIMSTMT(Ast.mkVAR("bitmap_clear", [], B.line attr),
				    [lexp;
				     Ast.FIELD(rexp, B.mkId "id", [], B.updty (B.dum __LOC__) B.INT);
				     Ast.INT (1, B.updty (B.dum __LOC__) B.INT)], battr))
	       else None
	   | _ -> None
	 in
	 match rewrite with
	   Some result -> result
	 | None -> Ast.ASSIGN(compile_exp expl,rexp,sorted_fld,attr)
       end

    | Ast.PRIMSTMT(f,args,attr) ->
	Ast.PRIMSTMT(f,List.map (compile_exp) args,attr)

    | Ast.ASSERT(exp,attr) -> Ast.ASSERT(compile_exp exp,attr)

    | x -> x

(* -------------------------- top-level blocks --------------------------- *)
(*: core variables *)
(* sc: sets of core (used to add alloc and free on cpumask_var_t) *)
let compile_handler (A.EVENT(nm, v, stmt, syn, attr)) =
  A.EVENT(nm, v, compile_stmt stmt, syn, attr)

let compile_function (A.FUNDEF(tl, ty, nm, pl, stmt, inl, attr)) =
  A.FUNDEF(tl, ty, nm, pl, compile_stmt stmt, inl, attr)


let compile_steal_thread steal =
  match steal with
    Ast.FLAT_STEAL_THREAD st as is-> is
  | Ast.ITER_STEAL_THREAD (st, until, post) ->
     Ast.ITER_STEAL_THREAD (st, until, List.map compile_stmt post)

let compile_steal steal =
  match steal with
    Ast.STEAL_THREAD s -> Ast.STEAL_THREAD (compile_steal_thread s)
  | Ast.ITER_STEAL_GROUP (sg, st, post, postgrp) ->
     Ast.ITER_STEAL_GROUP (sg,
			   compile_steal_thread st,
			   List.map compile_stmt post,
			   List.map compile_stmt postgrp)
  | Ast.FLAT_STEAL_GROUP (sg, st, postgrp) ->
     Ast.FLAT_STEAL_GROUP (sg,
			   compile_steal_thread st,
			   List.map compile_stmt postgrp)

let compile_steal_param (dom, dstg, dst, steal) =
  (dom, dstg, dst, compile_steal steal)

(* -------------------------- entry point --------------------------- *)

let compile_domgrp
      (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		     fundecls,valdefs,domains,pcstate,cstates,criteria,
		     trace,handlers,chandlers,ifunctions,functions,attr)) =
  let (new_handlers, new_chandlers, new_ifunctions, new_functions) =
    (List.map (compile_handler) handlers,
     List.map (compile_handler) chandlers,
     List.map (compile_function) ifunctions,
     List.map (compile_function) functions)
  in
  let new_pcstate =
    match pcstate with
      Ast.CORE(cv,st, steal) -> Ast.CORE(cv, st, Aux.option_apply compile_steal_param steal)
    | Ast.PSTATE(st) as is -> is
  in
  Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		fundecls,valdefs,domains,new_pcstate,cstates,criteria,
		trace,new_handlers,new_chandlers,new_ifunctions,new_functions,attr)
