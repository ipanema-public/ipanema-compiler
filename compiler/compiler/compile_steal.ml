module B = Objects
module CS = Class_state

let errpos_exp loc exp=
  loc^": "^string_of_int (B.line (Ast.get_exp_attr exp))

(* ----------------------- Environment lookup ----------------------- *)

exception LookupErrException

let rec lookup_id x = function
    [] -> raise LookupErrException
  | ((y,v)::r) -> if B.ideq(x,y) then v else lookup_id x r

let is_state id =
  try let _ = lookup_id id (!CS.state_env) in true
  with LookupErrException -> false

(* ---------------- expand in filter expression -------------- *)
let get_varid (Ast.VARDECL(_, id,_,_,_,_,_)) = id

let is_terminal = function
    Ast.VAR _ | Ast.FIELD _ | Ast.INT _ -> true
  | _ -> false

let rec check_until set until =
  match until with
    Ast.BINARY(bop, lexp, rexp, attr) ->
      begin
	match bop with
	  Ast.OR -> check_until set lexp || check_until set rexp
	| _ -> false
      end
  | Ast.EMPTY(id, _, _, _, _) -> set = id
  | _ -> false

let rec cpl_until_exp dom coreset = function
    Ast.PRIM(Ast.VAR(fnid, ssa, vattr), args, attr) ->
     let args =
       let newargs = List.map (cpl_until_exp dom coreset) args in
       if Progtypes.is_user_fct (B.id2c fnid)
       then Ast.mkVAR("policy", [], -1)::newargs
       else newargs
     in
     Ast.PRIM(Ast.VAR(fnid, ssa, vattr), args, attr)
  | Ast.BINARY(bop, exp1, exp2, attr) ->
     Ast.BINARY(bop, cpl_until_exp dom coreset exp1, cpl_until_exp dom coreset exp2, attr)
  | Ast.VAR(id, ssa, att) as e ->
     e
  | Ast.EMPTY(id, _, a, b, attr) when B.ty attr = B.SET B.GROUP ->
     Ast.EMPTY(id, [CS.COMP(B.mkId dom, B.mkId "")], a, b, attr)
  | _ as until -> until

let compile_until dom ty set until =
  let Ast.VARDECL(_, setid,_,_,_,_,_) = set in
  let until = cpl_until_exp dom setid until in
  if check_until setid until then until
  else Ast.mkBINARY(Ast.OR,
		    Ast.EMPTY(setid, [CS.COMP(B.mkId dom, B.mkId "")],
                              B.UNK, true,
			      B.updty (B.dum __LOC__) (B.SET ty)),
		    until, -1)

let rec cpl_filter_exp self stolen (decls, stmts) cexp =
  let ty = B.ty (Ast.get_exp_attr cexp) in
  match cexp with
    Ast.BOOL _ | Ast. INT _ | Ast.VAR  _ | Ast.FIELD _ ->  (cexp, decls, stmts)
    | Ast.VALID(exp, _, attr) -> cpl_filter_exp self stolen (decls, stmts) exp
    | Ast.UNARY(op, exp, attr) ->
       let (exp, decls, stmts) = cpl_filter_exp self stolen (decls, stmts) exp in
       (Ast.UNARY(op, exp, attr), decls, stmts)

  | Ast.BINARY(bop, exp1, exp2, attr) ->
     if is_terminal exp1 || is_terminal exp2 then
       let (exp1, decls, stmts) = cpl_filter_exp self stolen (decls, stmts) exp1 in
       let (exp2, decls, stmts) = cpl_filter_exp self stolen (decls, stmts) exp2 in
       (Ast.BINARY(bop,exp1,exp2,attr), decls, stmts)
     else
       begin
	 let fid = B.fresh_id () in
	 let fv = Ast.VAR(fid, [],B.updty (B.dum __LOC__) ty) in
	 let fdecl = Ast.mkVALDEF(Ast.mkVARDECL(CS.UNSHARED,ty, B.id2c fid, -1), Ast.get_init ty, -1) in
	 let (exp1, decls, stmts) = cpl_filter_exp self stolen (decls, stmts) exp1 in
	 let (exp2, decls, stmts) = cpl_filter_exp self stolen (decls, stmts) exp2 in
	 let new_exp = Ast.BINARY(bop, exp1, exp2, attr) in
	 (fv, fdecl::decls, Ast.mkASSIGN(fv, new_exp, -1)::stmts)
       end

  | Ast.TERNARY(cond,exp1,exp2,attr) ->
     let fid = B.fresh_id () in
     let fv = Ast.VAR(fid, [],B.updty (B.dum __LOC__) ty) in
     let fdecl = Ast.mkVALDEF(Ast.mkVARDECL(CS.UNSHARED,ty, B.id2c fid, -1), Ast.get_init ty, -1) in
     let (cond, decls, stmts) = cpl_filter_exp self stolen (decls, stmts) cond in
     let (exp1, decls, stmts) = cpl_filter_exp self stolen (decls, stmts) exp1 in
     let (exp2, decls, stmts) = cpl_filter_exp self stolen (decls, stmts) exp2 in
     (fv, fdecl::decls, Ast.mkASSIGN(fv, Ast.TERNARY(cond,exp1,exp2,attr),-1)::stmts)

  | Ast.PRIM(Ast.MIN fatt, [Ast.FIELD(queue, critid, _, _)], attr)
      when B.ty (Ast.get_exp_attr queue) = B.QUEUE B.PROCESS ->
     begin
       let minid = B.fresh_idx ("min_"^B.id2c critid) in
       let mindecl =
	 Ast.mkVALDEF(Ast.mkVARDECL(CS.UNSHARED,ty, B.id2c minid, -1),
		      Ast.mkVAR("INT_MAX",[],-1), -1) in
       let minv = Ast.VAR(minid, [],B.updty (B.dum __LOC__) ty) in
       let pid = B.fresh_idx "p" in
       let pv = Ast.VAR(pid, [], (B.dum __LOC__)) in
       let getcall = Ast.mkPRIMSTMT("get_task_struct",[Ast.mkFIELD(pv, "task", [], -1)], -1) in
       let putcall = Ast.mkPRIMSTMT("put_task_struct",[Ast.mkFIELD(pv, "task", [], -1)], -1) in
       let assign = Ast.mkASSIGN(minv,
				 Ast.FIELD(
				   Ast.VAR(pid,[],B.updty (B.dum __LOC__) B.PROCESS),
				   critid,[],B.updty (B.dum __LOC__) ty), -1) in
       let test = Ast.IF(
	 Ast.BINARY(Ast.LT,
		    Ast.FIELD(
		      Ast.VAR(pid,[],B.updty (B.dum __LOC__) B.PROCESS),
		      critid,[],B.updty (B.dum __LOC__) ty),
		    minv, (B.dum __LOC__)),
	 Ast.SEQ([],[assign],(B.dum __LOC__)),Ast.SEQ([],[],(B.dum __LOC__)),(B.dum __LOC__)) in
       let forstmt = Ast.FOR(pid,Some [queue],[],Ast.INC,
			     Ast.SEQ([],getcall::test::[putcall],(B.dum __LOC__)),None, (B.dum __LOC__)) in
       (minv, mindecl::decls, forstmt::stmts)
     end

  | Ast.PRIM(Ast.SUM fatt, [Ast.FIELD(queue, critid, _, _)], attr)
      when B.ty (Ast.get_exp_attr queue) = B.QUEUE B.PROCESS ->
     begin
       let sumid = B.fresh_idx ("sum_"^B.id2c critid) in
       let sumdecl =
	 Ast.mkVALDEF(Ast.mkVARDECL(CS.UNSHARED,ty, B.id2c sumid, -1),
		      Ast.INT(0,B.mkAttr (-1)), -1) in
       let sumv = Ast.VAR(sumid, [],B.updty (B.dum __LOC__) ty) in
       let pid = B.fresh_idx "p" in
       let pv = Ast.VAR(pid, [], (B.dum __LOC__)) in
       let getcall = Ast.mkPRIMSTMT("get_task_struct",[Ast.mkFIELD(pv, "task", [], -1)], -1) in
       let putcall = Ast.mkPRIMSTMT("put_task_struct",[Ast.mkFIELD(pv, "task", [], -1)], -1) in
       let assign =
	 Ast.mkASSIGN(sumv,
		      Ast.BINARY(Ast.PLUS,
				 sumv,
				 Ast.FIELD(
				   Ast.VAR(pid,[],B.updty (B.dum __LOC__) B.PROCESS),
				   critid,[],B.updty (B.dum __LOC__) ty), B.updty (B.dum __LOC__) B.INT),-1) in
       let forstmt = Ast.FOR(pid,Some [queue],[],Ast.INC,
			     Ast.SEQ([],getcall::assign::[putcall],(B.dum __LOC__)),None, (B.dum __LOC__)) in
       (sumv, sumdecl::decls, forstmt::stmts)
     end

  | Ast.PRIM(Ast.COUNT fatt, [Ast.FIELD(queue, critid, _, setattr) as set], attr)
      when B.ty (Ast.get_exp_attr queue) = B.QUEUE B.PROCESS ->
     let count = Ast.FIELD(Ast.FIELD(queue, critid, [], B.updty setattr (B.OPAQUE "ipanema_rq")),
			   B.mkId "nr_tasks", [], attr) in
     (count, decls, stmts)

  | Ast.PRIM(Ast.VAR(fnid, ssa, vattr), args, attr) ->
     let (args, decls, stmts) =
       let (newargs, decls, stmts) =
         List.fold_left
           (fun (acc, decls, stmts) a ->
             let (r, d, s) = cpl_filter_exp self stolen (decls, stmts) a in
             (r::acc, d, s)
           ) ([], decls, stmts) args in
       let newargs = List.rev newargs in
       if Progtypes.is_user_fct (B.id2c fnid)
       then (Ast.mkVAR("policy", [], -1)::newargs, decls, stmts)
       else (newargs, decls, stmts)
     in
     let fncall = Ast.PRIM(Ast.VAR(fnid, ssa, vattr), args, attr) in
     (fncall, decls, stmts)

  | Ast.AREF(exp1, exp2_, attr) ->
     let (exp1, decls, stmts) = cpl_filter_exp self stolen (decls, stmts) exp1 in
     let (exp2_, decls, stmts) =
       match Aux.option_apply (cpl_filter_exp self stolen (decls, stmts)) exp2_ with
         None -> (None, decls, stmts)
       | Some (exp2, decls, stmts) -> (Some exp2, decls, stmts)
     in
     (Ast.AREF(exp1, exp2_, attr), decls, stmts)

  | _ ->
     Pp.pretty_print_exp cexp;
    failwith ((errpos_exp __LOC__ cexp)
	      ^": Unsupported filter expression")

let compile_filter_exp self v exp =
  match cpl_filter_exp self v ([], []) exp with
    (exp, (d::decls as adecls),(s::stmts as astmts)) ->
      begin
	match exp, s with
	  Ast.VAR(eid, _, _), Ast.ASSIGN(Ast.VAR(lid, _,_), exp, _, _) when
	      B.idnameq (eid, lid) ->
		Ast.SEQ(List.rev decls, List.rev (Ast.mkRETURN(exp,-1)::stmts), (B.dum __LOC__))
	| _ ->
	   Ast.SEQ(List.rev adecls, List.rev (Ast.mkRETURN(exp,-1)::astmts), (B.dum __LOC__))
      end
  | (exp, [],[]) -> Ast.SEQ([], [Ast.mkRETURN(exp, -1)], (B.dum __LOC__))

(* ---------------- expand possible ordering in select -------------- *)
let get_crit = function
    Ast.CRIT_ID _ -> failwith __LOC__
  | Ast.CRIT_EXPR(_, order, e, _, a) ->
     match e with
       Ast.VAR(id, _, _) -> (order, B.ty a, id, e)
     | _ -> failwith (__LOC__ ^": "^ string_of_int (B.line (Ast.get_exp_attr e)))

let cpl_select_exp iter (busiest, busiestid, busiestv) ret = function
  Ast.FIRST(exp, crit_, si_, a) ->
    begin
      match crit_ with
	None -> ([],[ret])
      | Some crit ->
	 let core = Ast.mkVAR("core", [], -1) in
	 let Ast.VARDECL(iterty, iterid,_,_,_,_,_) = iter
	 and (order, critty, critid, critvar) = get_crit crit
	 and coreid = B.fresh_idx "core" in
	 let (op, init) = match order with
	     Ast.HIGHEST -> (Ast.GT, Ast.INT (0, B.dum __LOC__))
	   | Ast. LOWEST -> (Ast.LT, Ast.INT (100, B.dum __LOC__))
	 in
	 let tmp = B.fresh_idx (B.id2c critid) in
	 let tmpv = Ast.VAR(tmp, [], B.updty (B.dum __LOC__) critty) in
	 let citerdecl =
	   Ast.mkVALDEF(Ast.mkVARDECL(CS.UNSHARED,critty, B.id2c tmp, -1),
			init, -1) in
	 let tmpassign =
	   Ast.mkASSIGN(tmpv,
			Ast.FIELD(
			  Ast.VAR(coreid,[],B.updty (B.dum __LOC__) B.CORE),
			  critid,[],B.updty (B.dum __LOC__) critty), -1) in
	 let busiestassign =
	   Ast.mkASSIGN(busiestv, Ast.FIELD(Ast.VAR(coreid,[],B.updty (B.dum __LOC__) B.CORE),
					    B.mkId "id",[],B.updty (B.dum __LOC__) B.INT), -1) in
	 let test = Ast.IF(Ast.BINARY(op,
				      Ast.FIELD(
					Ast.VAR(coreid,[],B.updty (B.dum __LOC__) B.CORE),
					critid,[],B.updty (B.dum __LOC__) critty),
				      tmpv,
				      (B.dum __LOC__)),
			   Ast.SEQ([],[tmpassign;busiestassign],(B.dum __LOC__)),Ast.SEQ([],[],(B.dum __LOC__)),(B.dum __LOC__)) in
	 let forstmt = Ast.FOR(coreid,Some [Ast.VAR(iterid,[],B.updty (B.dum __LOC__) (iterty))],[],Ast.INC,
			       Ast.SEQ([],[test],(B.dum __LOC__)),None, (B.dum __LOC__)) in
	 ([citerdecl],[forstmt;ret])
    end
  | _ as exp -> failwith ((errpos_exp __LOC__ exp)
		   ^": Unexpected expression for select expression !")

let get_top_steal_exp = function
    Ast.FIRST(exp, crit_, si_, a) -> Ast.FIRST(exp, None, si_, a)
  | _  as exp-> failwith ((errpos_exp __LOC__ exp) ^": Unexpected expression for select expression !")

let compile_select_core_exp v exp =
  let busiestid = B.fresh_idx "busiest" in
  let busiest = B.id2c busiestid
  and busiestv = Ast.VAR(busiestid, [], B.updty (B.dum __LOC__) B.INT) in
  let busiestdecl =
    Ast.mkVALDEF(Ast.mkVARDECL(CS.UNSHARED,B.INT, busiest, -1),
		 Ast.INT(-1,B.updty (B.dum __LOC__) B.INT), -1) in
  let retcore =
    Ast.AREF(Ast.mkPRIM("per_cpu",[Ast.mkVAR("core", [], -1);
				   busiestv],-1),None,(B.dum __LOC__)) in
  let ret = Ast.mkRETURN(
                Ast.TERNARY(
                    Ast.BINARY(Ast.EQ,
                               busiestv,
                               Ast.INT(-1,B.updty (B.dum __LOC__) B.INT),
                               B.updty (B.dum __LOC__) B.BOOL),
                    Ast.INT_NULL (B.dum __LOC__),
                    retcore, B.dum __LOC__), -1) in
  let (decls, body) = cpl_select_exp v (busiest, busiestid, busiestv) ret exp in
  Ast.SEQ(busiestdecl::decls, body, (B.dum __LOC__))

let compile_select_grp_exp domselect grpsetselect exp =
  let Ast.VARDECL(iterty, iterid,_,_,_,_,_) = domselect in
  let Ast.VARDECL(_, grpid,_,_,_,_,_) = grpsetselect in
  let exp = match exp with
      Ast.FIRST(exp, crit_, si_, a) ->
	Ast.FIRST(exp, crit_, Some(CS.COMP(iterid, grpid)), a)
    | _  as exp-> failwith ((errpos_exp __LOC__ exp) ^": Unexpected expression for select expression !")
  in
  let ret = Ast.mkRETURN(exp,-1) in
  (*  let (decls, body) = cpl_select_exp grpsetselect core (busiest, busiestid, busiestv) ret exp in*)
  let (decls, body) = Compile_first.compile_stmt ([],[]) ret in
  Ast.SEQ(List.rev decls, List.rev body, (B.dum __LOC__))

let rec check_post ((coreset, busiestdef) as up) check post =
  match post with
    Ast.SEQ(decls, stmts, a) ->
     List.fold_left (check_post up) check stmts
  | Ast.PRIMSTMT(Ast.VAR(id, _, _), [busiest;set], a)
       when B.id2c id = "cpumask_clear_cpu" ->
     begin
       match (busiest, set) with
         (Ast.FIELD(Ast.VAR(busiestid, _, _), idid, _,  _), Ast.VAR(setid, _,_))
            when B.idnameq(setid,get_varid coreset)
                 && B.id2c idid = "id"
                 && B.idnameq (busiestid,get_varid busiestdef)
         -> true
       | _ -> check
     end
  | _ -> check

let compile_migrcond miscf self migrcond =
  let (busiest, thief, group, task, stmt, stop, _) = migrcond in
  let Ast.VARDECL(B.CORE, self, _,_, _, _, _) = thief in
  let (new_stmt, upddecls, updstmts) = Compile_migration.compile_migrstmt self stmt in
  let (new_stop, upddecls, updstmts) = Compile_migration.filter_attr_exp false self (upddecls, updstmts) stop in
  let new_update = Ast.mkSEQ(upddecls, updstmts, -1) in
  (busiest, (busiest, thief, group, task, (fst miscf) new_stmt, (snd miscf) new_stop, new_update))

let compile_steal_blk miscf ((thief, vfilter, sfilter), (vselect, e, sselect), migrcond) =
  let Ast.VARDECL(B.CORE, thiefid,_,_,_,_,_) = thief in
  let new_filter = match sfilter with
      Ast.RETURN(Some exp, a) -> compile_filter_exp thiefid vfilter exp
    | _ -> failwith (__LOC__ ^": Unexpected filter statement !") in
  let new_select = match sselect with
      Ast.RETURN(Some exp, a) -> compile_select_core_exp vselect exp
    | _ -> failwith (__LOC__ ^": Unexpected select statement !") in
  let (busiest, new_migrcond) = compile_migrcond miscf thiefid migrcond in
  ((vselect, busiest), ((thief, vfilter,new_filter), (vselect, e, new_select), new_migrcond))

let compile_steal_group dom ((thief, vfilter, sfilter), (domselect, grpselect, grpsetselect, sselect), until) =
  let Ast.VARDECL(B.GROUP, thiefid,_,_,_,_,_) = thief in
  let new_filter = match sfilter with
      Ast.RETURN(Some exp, a) -> compile_filter_exp thiefid vfilter exp
    | _ -> failwith (__LOC__ ^": Unexpected filter statement !") in
  let new_select = match sselect with
      Ast.RETURN(Some exp, a) -> compile_select_grp_exp domselect grpselect exp
    | _ -> failwith (__LOC__ ^": Unexpected select statement !") in
  ((thief, vfilter,new_filter), (domselect, grpselect, grpsetselect, new_select),
   compile_until dom B.GROUP grpsetselect until)

let compile_steal_thread dom miscf steal =
  match steal with
    Ast.FLAT_STEAL_THREAD st ->
     Ast.FLAT_STEAL_THREAD (snd (compile_steal_blk miscf st))
  | Ast.ITER_STEAL_THREAD (st, until, post) ->
     let ((coreset, busiest) as up, block) = compile_steal_blk miscf st in
     let newpost =
       if List.fold_left (check_post up) false post then
         post
       else
         let dum = B.dum __LOC__ in
         let coresetty = B.updty dum (B.SET B.CORE) in
         let clear =
           Ast.PRIMSTMT(Ast.VAR(B.mkId"cpumask_clear_cpu", [], B.updty dum B.VOID),
                        [Ast.FIELD(Ast.VAR(get_varid busiest, [], B.updty dum B.CORE),
                                   B.mkId "id", [], B.updty dum B.INT);
                         Ast.VAR(get_varid coreset, [], coresetty)],
                        coresetty)
         in
         let [Ast.SEQ(decls, stmts, attr)] = post in
         [Ast.SEQ(decls, clear::stmts, attr)]
     in
     Ast.ITER_STEAL_THREAD (block,
			    compile_until dom B.CORE coreset until,
                            newpost)

let compile_steal dom miscf steal =
  match steal with
    Ast.STEAL_THREAD s ->
     Ast.STEAL_THREAD (compile_steal_thread dom miscf s)
  | Ast.ITER_STEAL_GROUP (sg, st, post, postgrp) ->
     Ast.ITER_STEAL_GROUP (compile_steal_group dom sg,
			   compile_steal_thread dom miscf st,
			   post, postgrp)
  | Ast.FLAT_STEAL_GROUP (sg, st, postgrp) ->
     Ast.FLAT_STEAL_GROUP (compile_steal_group dom sg,
			   compile_steal_thread dom miscf st,
                           postgrp)

let compile_steal_param miscf (dom, dstg, dst, steal) =
  (dom, dstg, dst, compile_steal dom miscf steal)
