(* $Id$ *)

(* various simple compilation things:
   - Convert class references to state references
   - Compile in expressions to state_of(expr) == states, where states is
     the | of constants
   - Compile p1 > p2 etc to a call to greater
   - Call to empty converted to a call to empty_proc or empty_queue
   - Drop the nowhere state
   - Convert references to system process fields to ->attr->field_name
   - adjust switches where the test expression is a process/schedule
   - convert next expression to appropriate field access
   - add reference count information to new, end, attach, and detach
*)

module B = Objects
module CS = Class_state
module S = Compile_steal

(* -------------------- Miscellaneous operations -------------------- *)

let rec lookup_id x = function
    [] -> raise (Error.Error "internal error 1")
  | ((y,v)::r) -> if B.ideq(x,y) then v else lookup_id x r

let error attr str =
  raise(Error.Error(Printf.sprintf "%s: %s" (B.loc2c attr) str))

(* ------------------------- State constants ------------------------ *)

let make_state_cst id =
  B.mkId(Printf.sprintf "%s_STATE" (String.uppercase_ascii (B.id2c id)))

let make_decl id n attr loc =
  let attr = B.updty attr B.INT in
  let cattr = B.add_comment attr loc in
  Ast.VALDEF(Ast.VARDECL(B.EMPTY,id,false,false,CS.UNSHARED,None,attr),
	     Ast.BINARY(Ast.LSHIFT,Ast.INT(1,attr),Ast.INT(n,attr),cattr),
	     Ast.DEFCONST,attr)

(* ------------------------- Process State constants ------------------------ *)
(*
(* needed because a process in an unknown state might be allowed to be
in NOWHERE *)
let make_zdecl id n attr loc =
  let attr = B.updty attr B.INT in
  let cattr = B.add_comment attr loc in
  Ast.VALDEF(Ast.VARDECL(B.EMPTY,id,false,false,CS.UNSHARED,None,attr),Ast.INT(0,cattr),Ast.DEFCONST,
	     attr)

let rec make_pstate_csts n = function
    [] ->
    let (rt_state, rt_loc) = (make_state_cst (B.mkId "READY_TICK"), __LOC__)
    and (migr_state, migr_loc) = (make_state_cst (B.mkId "MIGRATING"), __LOC__) in
	[make_decl rt_state n (B.mkAttr (-1)) rt_loc;
	 make_decl migr_state (n+1) (B.mkAttr (-1)) migr_loc]
  | Ast.QUEUE(CS.NOWHERE,shared,queue_typ,id,vis,po,attr)::xs ->
     (make_zdecl (make_state_cst id) n attr __LOC__)
     :: (make_pstate_csts n xs)
  | Ast.PROCESS(CS.NOWHERE,shared,id,previd,vis,attr)::xs ->
     (make_zdecl (make_state_cst id) n attr __LOC__)
     :: (make_pstate_csts n xs)
  | Ast.QUEUE(clsname,shared,queue_typ,id,vis,po,attr)::xs ->
     (make_decl (make_state_cst id) n attr __LOC__)
     :: (make_pstate_csts (n+1) xs)
  | Ast.PROCESS(clsname,shared,id,previd,vis,attr)::xs ->
     (make_decl (make_state_cst id) n attr __LOC__)
     :: (make_pstate_csts (n+1) xs)
   *)

let drop_nowhere states =
  List.fold_left
    (function prev ->
      function
	  Ast.QUEUE(CS.NOWHERE,_,queue_typ,id,vis,po,attr) -> prev
	| Ast.PROCESS(CS.NOWHERE,_,id,previd,vis,attr) -> prev
	| state -> state::prev)
    [] states

(* ------------------------- Core State constants ------------------------ *)
let make_cstate_cst n = function
   Ast.CSTATE(cl, id, attr) | Ast.CSET(cl, id, attr) ->
     make_decl (make_state_cst id) n attr __LOC__

let make_cstate_csts cstates =
  List.mapi make_cstate_cst cstates

(* --------------------------- Expressions -------------------------- *)

let choose_gtr_prim states1 states2 =
  let common_states = Aux.intersect states1 states2 in
  if List.exists
      (function
	  (_,_,CS.QUEUE(CS.FIFO),_)
	| (_,_,CS.QUEUE(CS.SELECT(CS.LIFODEFAULT)),_)
	| (_,_,CS.QUEUE(CS.SELECT(CS.FIFODEFAULT)),_) -> true
	| _ -> false)
      common_states
  then "greater_lifo_fifo"
  else "greater"

let mk_greater exp1 states1 exp2 states2 attr =
  let battr = B.updty attr B.BOOL in
  let vattr = B.updty attr B.VOID in
  if (!B.hls)
  then
    let iattr = B.updty attr B.SCHEDULER_INST in
    let sched_inst = Ast.VAR(B.mkId("scheduler_instance"),[],iattr) in
    Ast.PRIM(Ast.VAR(B.mkId("greater"),[],vattr),[sched_inst;exp1;exp2],battr)
  else
    (* check if both can be in the same queue and this queue can be fifo or
       lifo *)
    let prim = choose_gtr_prim states1 states2 in
    Ast.PRIM(Ast.VAR(B.mkId(prim),[],vattr),[exp1;exp2],battr)

let mk_geq exp1 states1 exp2 states2 attr =
  let battr = B.updty attr B.BOOL in
  let vattr = B.updty attr B.VOID in
  if (!B.hls)
  then
    let iattr = B.updty attr B.SCHEDULER_INST in
    let sched_inst = Ast.VAR(B.mkId("scheduler_instance"),[],iattr) in
    Ast.UNARY(Ast.NOT,Ast.PRIM(Ast.VAR(B.mkId("greater"),[],vattr),
			       [sched_inst;exp2;exp1],
			       attr),
	      battr)
  else
    let prim = choose_gtr_prim states1 states2 in
    Ast.UNARY(Ast.NOT,Ast.PRIM(Ast.VAR(B.mkId(prim),[],vattr),[exp2;exp1],
			       attr),
	      battr)

let rec orify op attr = function
    [] -> Printf.printf "%s: " (B.loc2c attr);
	raise (Error.Error "internal error 2")
  | [x] -> x
  | x::xs -> Ast.BINARY(op,x,orify op attr xs,attr)

let sysdecls = ref ([] : B.identifier list)

let collect_sys_fields procdecls =
  List.fold_left
    (function prev ->
      function
	  Ast.VARDECL(_,id,true,_,_,_,_) -> id :: prev
	| _ -> prev)
    [] procdecls

let rec compile_exp = function
    Ast.FIELD(exp,fld,ssa,attr) ->
	  (*
	    Add attr wrapper for system attribut

	    Exceptions:
	    - cpu attribut is handled internally as 'core'
	    - ppid attribut is handled internally as 'thread'
	  *)
      if (Aux.member fld (!sysdecls)) &&
	not (B.ty attr = B.CORE && B.strideq ("cpu", fld)
	    && B.ty (Ast.get_exp_attr exp) = B.PROCESS) &&
	not (B.ty attr = B.PROCESS && B.strideq ("parent", fld)
	    && B.ty (Ast.get_exp_attr exp) = B.PROCESS) &&
	not (B.strideq ("parent", fld) && match B.ty attr with
	  B.STRUCT (B.ID ("task_struct", _)) -> true
	| _ -> false)
      then
          Ast.FIELD(Ast.PRIM(Ast.VAR(B.mkId "attr",[],attr),[compile_exp exp],attr),
		  fld,[], attr)
      else Ast.FIELD(compile_exp exp,fld,[],attr)

  | Ast.UNARY(uop,exp,attr) -> Ast.UNARY(uop,compile_exp exp,attr)

  | Ast.BINARY(bop,exp1,exp2,attr) ->
      let exp1 = compile_exp exp1
      and exp2 = compile_exp exp2 in
      if (B.ty(Ast.get_exp_attr exp1)) = B.PROCESS
	  && List.mem bop [Ast.LT;Ast.GT;Ast.LEQ;Ast.GEQ]
      then error attr "unexpected binary operator - expected pbinary"
      else Ast.BINARY(bop,exp1,exp2,attr)

  | Ast.PBINARY(bop,exp1,states1,exp2,states2,attr) ->
      let exp1 = compile_exp exp1
      and exp2 = compile_exp exp2 in
      if (B.ty(Ast.get_exp_attr exp1)) = B.PROCESS
      then
	(match bop with
	  Ast.LT -> mk_greater exp2 states2 exp1 states1 attr
	| Ast.GT -> mk_greater exp1 states1 exp2 states2 attr
	| Ast.LEQ -> mk_geq exp2 states2 exp1 states1 attr
	| Ast.GEQ -> mk_geq exp1 states1 exp2 states2 attr
	| _ -> error attr "unexpected binary operator - expected <,>,>=,>=")
      else
	error attr
	  "unexpected pbinary operator - expected proc/sched comparison"

    | Ast.INDR(exp,attr) -> Ast.INDR(compile_exp exp,attr)

    | Ast.EMPTY(id,states,x,y,attr) ->
            let mk_fname str = Ast.VAR(B.mkId str, [], B.updty attr B.VOID) in
	orify Ast.AND attr
	  (List.map
	     (function
		 CS.STATE(id,_,CS.PROC,_) ->
             Ast.PRIM(mk_fname "empty_proc",[Ast.VAR(id,[],attr)],attr)
	       | CS.STATE(id,_,CS.QUEUE(CS.SELECT(_)),_) ->
		   let sz = !CS.selected_size in
		   if sz > 1
		   then Ast.PRIM(mk_fname "empty_array",[],attr)
		   else Ast.PRIM(mk_fname "empty_queue",[Ast.VAR(id,[],attr)],
				 attr)
	       | CS.STATE(id,_,CS.QUEUE(_),_) ->
                 Ast.PRIM(mk_fname "empty_queue",[Ast.VAR(id,[],attr)],attr)
	       | CS.CSTATE(id, coremode) ->
                 Ast.PRIM(mk_fname "empty_core",[Ast.VAR(id,[],attr)],attr)
	     )
	     states)

    | Ast.PRIM(Ast.VAR(id,_,va),[],a)
	when (B.id2c id) = "system_cores" ->
       (*
	 See http://lxr.free-electrons.com/source/include/linux/cpumask.h#L93
	 and description at http://lxr.free-electrons.com/source/include/linux/cpumask.h#L48
       *)
       Ast.INDR(Ast.VAR((B.mkId "cpu_online_mask"),[],a), a)

    | Ast.PRIM(f,args,attr) ->
       let args =
         let newargs = List.map compile_exp args in
         match f with
           Ast.VAR(fid, _, _) ->
           if Progtypes.is_user_fct (B.id2c fid) then
             Ast.mkVAR("policy", [], -1)::newargs
           else newargs
         | _ -> newargs
       in
	 Ast.PRIM(f,args,attr)

    | Ast.SCHEDCHILD(arg,procs,attr) ->
	Ast.SCHEDCHILD(compile_exp arg,procs,attr)

    | Ast.IN(_,id,[states],_,_,_,attr) as exp -> exp

    | Ast.IN(exp,id,states,_,_,_,attr) ->
            let mk_fname str = Ast.VAR(B.mkId str, [],B.updty attr B.VOID) in
	Ast.BINARY(Ast.BITAND,
		   Ast.PRIM(mk_fname "state_of",[exp],attr),
		   orify Ast.BITOR attr
		     (List.map
			(function CS.STATE(id,_,_,_) ->
			  Ast.VAR(make_state_cst id,[],B.updty attr B.INT)
			  | CS.CSTATE _ -> failwith "Unimplemented CSTATE" )
			states),
		   attr)

    | Ast.AREF(exp1, exp2_, attr) ->
       Ast.AREF(compile_exp exp1, Aux.option_apply compile_exp exp2_, attr)
    | exp -> exp

(* -------------------------- Declarations ------------------------- *)

let compile_decl = function
    Ast.VALDEF(decl,exp,isconst,attr) ->
      Ast.VALDEF(decl,compile_exp exp,isconst,attr)

  | decl -> decl

(* --------------------------- Statements -------------------------- *)

let state_id = function
    CS.STATE(id,_,_,_) -> id
  | CS.CSTATE(id,_) -> failwith "Unexpected CSTATE"

let rec compile_stmt = function
    Ast.IF(exp,stmt1,stmt2,attr) ->
      Ast.IF(compile_exp exp,compile_stmt stmt1,compile_stmt stmt2,attr)

    | Ast.FOR(id,state_id,x,dir,stmt,crit,attr) ->
	Ast.FOR(id,state_id,x,dir,compile_stmt stmt,crit,attr)

    | Ast.FOR_WRAPPER(label,stmts,attr) ->
	Ast.FOR_WRAPPER(label,List.map compile_stmt stmts,attr)

    | Ast.SWITCH(exp,cases,default,attr) ->
	let expty = B.ty (Ast.get_exp_attr exp) in
	let is_proc_sched =
	  (match expty with
	    B.PROCESS -> true
	  | _ -> false) in
	if is_proc_sched
	then
        let mk_fname str = Ast.VAR(B.mkId str, [], B.updty attr B.VOID) in
	  Ast.SWITCH(Ast.PRIM(mk_fname "state_of",[compile_exp exp],
			      B.updty attr expty),
		     List.map
		       (function Ast.SEQ_CASE(idlist,x,stmt,y) ->
			 Ast.SEQ_CASE(List.map state_id x,x,compile_stmt stmt,
				      y))
		       cases,Aux.app_option compile_stmt default,attr)
	else Ast.SWITCH(exp,List.map
			  (function Ast.SEQ_CASE(idlist,x,stmt,y) ->
			    Ast.SEQ_CASE(idlist,x,compile_stmt stmt,y))
			  cases,Aux.app_option compile_stmt default,attr)
    | Ast.SEQ(decls,stmts,attr) ->
	Ast.SEQ(List.map compile_decl decls,List.map compile_stmt stmts,attr)

    | Ast.RETURN(None,attr) -> Ast.RETURN(None,attr)

    | Ast.RETURN(Some(exp),attr) -> Ast.RETURN(Some(compile_exp exp),attr)

    | Ast.MOVE(exp,state,x,y,z,a,b,attr) ->
	Ast.MOVE(compile_exp exp,compile_exp state,x,y,z,a,b,attr)

    | Ast.MOVEFWD(exp,x,state_end,attr) ->
	Ast.MOVEFWD(compile_exp exp,x,state_end,attr)

    | Ast.SAFEMOVEFWD(exp,x,stm,state_end,attr) ->
	raise
	  (Error.Error
	     "compile_misc: unexpected safe move forward statement\n")

    | Ast.ASSIGN(expl,expr,sorted_fld,attr) ->
       Ast.ASSIGN(compile_exp expl,compile_exp expr,sorted_fld,attr)

    | Ast.PRIMSTMT(f,args,attr) ->
       let args =
         let newargs = List.map compile_exp args in
         match f with
           Ast.VAR(fid, _, _) ->
           if Progtypes.is_user_fct (B.id2c fid) then
             Ast.mkVAR("policy", [], -1)::newargs
           else newargs
         | _ -> newargs
       in
	 Ast.PRIMSTMT(f,args,attr)

    | Ast.ASSERT(exp,attr) -> Ast.ASSERT(compile_exp exp,attr)

    | x -> x

(* ------------------- Check for the initial event ------------------ *)

let check_initial attr nm stmt =
  (*
  if List.exists (Ast.event_name_equal nm) (!Events.init_events)
  then
    let new_id = B.fresh_id ()
    and battr = B.updty attr B.BOOL in
    let set = Ast.ASSIGN(Ast.VAR(new_id,[],battr),
			 Ast.BOOL(true,battr),false,attr) in
    Some(Ast.VALDEF(Ast.VARDECL(B.BOOL,new_id,false,false,None,battr),
		    Ast.BOOL(false,battr),
		    Ast.VARIABLE,battr),
	 Ast.SEQ([],
       [Ast.IF(Ast.VAR(new_id,[],battr),
		   Ast.SEQ([],
			   [Ast.ERROR("initial event called more than once",
				      attr)],
			   attr),
		   Ast.addBLOCK attr set stmt,
		   attr)],
		 attr))
  else *) None

(* ------------------------- Reference count ------------------------ *)

let call s attr = Ast.PRIMSTMT(Ast.VAR(B.mkId(s),[],attr),[],attr)

(* only works if there are separate handlers for new and end, but the event
types rather require this... *)
let check_ref_ct_hd (Ast.EVENT_NAME(nm,_)) stmt attr =
  match B.id2c nm with
    "detach" ->
      (match stmt with
	 Ast.SEQ(decls, stmts, attr) ->
         let tgt = Ast.VAR(B.mkId "tgt", [], attr) in
         let cload = Ast.FIELD(
                         Ast.AREF(
                             Ast.PRIM(Ast.VAR(B.mkId "ipanema_core", [], attr),
	                              [Ast.PRIM(Ast.VAR(B.mkId "task_cpu", [], attr),
                                                [Ast.FIELD(tgt, B.mkId "task", [], attr)], attr)],
                                      attr),
                             None, attr),
                         B.mkId "cload", [], attr)
         in
	  Ast.SEQ(decls,
                  stmts@
	            [Ast.ASSIGN(cload,
                                Ast.BINARY(Ast.MINUS, cload,
	                                   Ast.VAR(B.mkId Ipanema.oldload, [], attr), attr),
                                true, attr);
                     Compile_wmb.smp_wmb (B.dum __LOC__);
                     Ast.PRIMSTMT(
	                 Ast.VAR(B.mkId "kfree", [], attr),
	                 [tgt], attr)], attr)
      )
  | _ -> stmt

let check_ref_ct_fn nm stmt attr = stmt

let compile_init stmt =
  match stmt with
    Ast.SEQ(d, s, attr) ->
      let return = [Ast.mkRETURN((Ast.mkINT (0, -1)), -1)] in
      Ast.SEQ(d, s@return, attr)
  | Ast.RETURN(None, attr) -> Ast.RETURN(Some(Ast.mkINT(0, B.line attr)), attr)
  | _ ->
     let return = [Ast.mkRETURN((Ast.mkINT (0, -1)), -1)] in
     Ast.mkSEQ([], stmt::return, -1)

(* --------------------- Handlers and functions --------------------- *)

(* parser should check that event names are ok *)
let compile_handlers handlers =
  List.map
    (function (Ast.EVENT(nm,param,stmt,syn,attr)) ->
      Ast.EVENT(nm,param,check_ref_ct_hd nm (compile_stmt stmt) attr,
		syn,attr))
    handlers

let compile_ifunctions sched_intf ifunctions =
  List.map
    (function Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr) ->
      let new_stmt = check_ref_ct_fn nm (compile_stmt stmt) attr in
      let policy =
	Ast.mkVARDECL(CS.UNSHARED,B.INDR(B.STRUCT(B.mkId("ipanema_policy"))),
		      "policy",
		      B.line attr)
      in
      let (new_ty, new_nm, new_inl, new_params, new_stmt) = match (ret, sched_intf, B.id2c nm) with
	  B.VOID, Some sched_nm, "init" ->
	    (B.INT, B.mkId ("ipanema_"^sched_nm^"_init"), Ast.NO_INLINE, policy::params, compile_init new_stmt)
	| B.BOOL, Some sched_nm, "attach" ->
	   if List.length params = 1 then (* Does attach event have any optional argument ? *)
	     let cmd =
	       Ast.mkVARDECL(CS.UNSHARED,B.INDR(B.CLASS(B.mkId("char"))),
			     "command",
			     B.line attr)
	     in
	     let new_params = policy::params@[cmd] in
	     (B.BOOL, B.mkId ("ipanema_"^sched_nm^"_attach"), Ast.NO_INLINE, new_params, new_stmt)
	   else
	     (B.BOOL, B.mkId ("ipanema_"^sched_nm^"_attach"), Ast.NO_INLINE, policy::params, new_stmt)
	| _ ->
           if Progtypes.is_user_fct (B.id2c nm)
           then (ret, nm, inl, policy::params, new_stmt)
           else (ret, nm, inl, params, new_stmt)
      in
      Ast.FUNDEF(tl,new_ty,new_nm,new_params, new_stmt, new_inl, attr))
    ifunctions

let compile_fundecl = function
    Ast.FUNDECL(tl,ret,nm,params,attr) ->
     let params =
       if Progtypes.is_user_fct (B.id2c nm) then
         B.INDR (B.STRUCT (B.mkId "ipanema_policy"))::params
       else params
     in
     Ast.FUNDECL(tl,ret,nm,params,attr)

(* -------------------------- entry point --------------------------- *)

(* FIXME: handle local/global
 * process states correctly *)
let compile_misc
    (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		   fundecls,valdefs,domains,pcstate,cstates,criteria,
		   trace,handlers,chandlers,ifunctions,functions,attr)) =
  let fundecls = List.map compile_fundecl fundecls in
  let new_states =
    match pcstate with
      Ast.CORE(_, states,_) |
      Ast.PSTATE(states) ->
       List.rev (drop_nowhere states)
  in
  let new_pcstate =
    match pcstate with
      Ast.CORE(cv, _, steal) ->
       Ast.CORE(cv, new_states,
                Aux.option_apply (S.compile_steal_param (compile_stmt, compile_exp)) steal)
    | Ast.PSTATE _ -> Ast.PSTATE(new_states) in
  let cstate_csts = make_cstate_csts cstates in
  sysdecls := collect_sys_fields procdecls;
  let newhandlers = compile_handlers handlers in
  let newchandlers = compile_handlers chandlers in
  Ast.SCHEDULER(nm,cstdefs@cstate_csts,enums,dom,grp,procdecls,
		fundecls,valdefs,domains,
		new_pcstate,cstates,
		criteria,trace,newhandlers,newchandlers,
		compile_ifunctions (Some nm) ifunctions,
		compile_ifunctions None functions, attr)
