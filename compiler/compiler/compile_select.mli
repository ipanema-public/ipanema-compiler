type sort_result = SORTED | UNSORTED | PARTIALLY_SORTED
type criteria_class = DYNCRIT | STATCRIT | KEYCRIT
val compile_select :
    Ast.scheduler -> sort_result * criteria_class list * Ast.scheduler

(* used to indicate whether select functions checking first_fifo/first_lifo
are necessary *)
val used_lifo_fifo : bool ref
