let compile ast =
  let ast_opt = Optim.optimize ast in
  let ast = Compile_wmb.compile ast_opt in
  let ast = Compile_mvInitialization.move_initialization ast in
  let ast = Compile_new.compile_new ast in
  let ast = Compile_unblock.compile_unblock ast in
  let ast = Compile_trace.compile_trace ast in
  let ast = Compile_forward.compile_forward ast in
  if !Error.debug then Pp.pretty_print false "out.fwd.ipa" ast;
  let ast = Compile_select.compile_select ast in
  let ast = Check_foreach.check_foreach ast in
  let ast = Compile_move.compile_move ast in
  let ast = Compile_ordering.compile_ordering ast in
  let ast = Compile_schedchild.compile_schedchild ast in
  let ast = Compile_time.compile_time ast in
  let ast = Compile_persistent.compile_persistent ast in
  let ast = Compile_core.compile_core ast in
  let ast = Compile_domgrp.compile_domgrp ast in
  (* Skip target for leon *)
  if !Error.debug then Pp.pretty_print false "out.domgrp.ipa" ast;
  let (leon, ast) = (ast, Compile_target.compile_target ast) in
  if !Error.debug then Pp.pretty_print false "out.target.ipa" ast;
  (* FIXME: Move compile first before is need for leon *)
  let (leon, ast) = (leon, Compile_first.compile_first ast) in
  if !Error.debug then Pp.pretty_print false "out.first.ipa" ast;
  let (leon, ast) = (Compile_lazy.compile_lazy leon,
                     Compile_lazy.compile_lazy ast) in
  let (leon, ast) = (Compile_prim.compile_prim leon,
                     Compile_prim.compile_prim ast) in
  if !Error.debug then Pp.pretty_print false "out.prim.ipa" ast;
  (* Skip misc for leon *)
  let (leon, ast) = (leon, Compile_misc.compile_misc ast) in
  if !Error.debug then Pp.pretty_print false "out.misc.ipa" ast;
  (ast_opt, Post_verif.postprocess leon, Post_verif.postprocess ast)
