(* $Id$ *)

module B = Objects
module CS = Class_state

let new_s = ([],[])

let get_crit = function
    Ast.CRIT_ID(_, order, id, _, a) ->
     (false, order, B.ty a, id, fun _ -> Ast.VAR(id, [], a))
  | Ast.CRIT_EXPR(_, order, e, _, a) ->
     match e with
       Ast.VAR(id, _, _) -> (false, order, B.ty a, id, fun _ -> e)
     | Ast.PRIM(Ast.VAR(id, ssa, vattr), _, attr) ->
	(true, order, B.ty a, id,
	 fun args ->
	   Ast.PRIM(Ast.VAR(id, ssa, vattr), args, attr))
     | _ -> failwith __LOC__

let rec compile_exp ((decls, stmts) as acc) = function
    Ast.BOOL _ | Ast.VAR _ | Ast.INT _
  | Ast.INT_STRING _ | Ast.INT_NULL _ as e -> (e, acc)
  | Ast.CAST(ty, exp, attr) ->
      let (exp, acc) = compile_exp acc exp in
      (Ast.CAST(ty, exp, attr), acc)
  | Ast.FIELD(exp,fld,ssa,attr) ->
      let (exp, acc) = compile_exp acc exp in
      (Ast.FIELD(exp,fld,[],attr), acc)

  | Ast.LCORE(s, exp, exp_, attr) ->
     let (exp, acc) = compile_exp acc exp in
     let (exp_, acc) = match Aux.app_option (compile_exp acc) exp_ with
	 None -> (None, acc)
       | Some (new_exp, acc) -> (Some new_exp, acc)
     in (Ast.LCORE(s, exp, exp_, attr), acc)

  | Ast.VALID(exp, si,attr) ->
      let (exp, acc) = compile_exp acc exp in
     (Ast.VALID(exp, si, attr), acc)

  | Ast.UNARY(uop,exp,attr) ->
      let (exp, acc) = compile_exp acc exp in
     (Ast.UNARY(uop, exp,attr), acc)

  | Ast.BINARY(bop,exp1,exp2,attr) ->
      let (exp1, acc) = compile_exp acc exp1 in
      let (exp2, acc) = compile_exp acc exp2 in
      (Ast.BINARY(bop,exp1,exp2,attr), acc)

  | Ast.PBINARY(bop,exp1,states1,exp2,states2,attr) ->
      let (exp1, acc) = compile_exp acc exp1 in
      let (exp2, acc) = compile_exp acc exp2 in
      (Ast.PBINARY(bop,exp1,states1,exp2,states2,attr), acc)

  | Ast.TERNARY(cond,exp1,exp2,attr) ->
      let (cond, acc) = compile_exp acc cond in
      let (exp1, acc) = compile_exp acc exp1 in
      let (exp2, acc) = compile_exp acc exp2 in
      (Ast.TERNARY(cond,exp1,exp2,attr), acc)

  | Ast.INDR(exp,attr) ->
      let (exp, acc) = compile_exp acc exp in
     (Ast.INDR(exp,attr), acc)

  | Ast.AREF(exp,x,attr) ->
      let (exp, acc) = compile_exp acc exp in
      (Ast.AREF(exp,x,attr), acc)

  | Ast.PRIM(f,args,attr) ->
     let (args, accl) = List.split (List.map (compile_exp new_s) args) in
     let (declsl, stmtsl) = List.split accl in
     (Ast.PRIM(f,args,attr),
      ((List.concat declsl)@decls, (List.concat stmtsl)@stmts))

    | Ast.FIRST(Ast.VAR(B.ID("READY", _), _, rattr),crit,_,attr) as f
	when B.ty attr = B.PROCESS ->
       begin
	 match B.ty rattr with
	   B.ENUM (B.ID("SchedState", _)) ->
	     (* ipanema_state(cpu).XXX *)
	     let dum = B.dum __LOC__ in
	     let cpu = Ast.mkVAR("cpu", [], -1) in
	     let exp = Ast.FIELD(Ast.mkPRIM("ipanema_state", [cpu], -1),
				 List.hd !CS.ready_st, [],
				 B.updty dum (B.QUEUE(B.PROCESS))) in
	     let (exp, acc) = compile_exp acc exp in
	     let tasktype = B.INDR (B.STRUCT (B.mkId "task_struct")) in
	     let taskid = B.fresh_idx "task" in
	     let task = B.id2c taskid
	     and taskvar = Ast.VAR(taskid,[],B.updty dum tasktype) in
             let taskv =
               Ast.TERNARY(taskvar,
                           Ast.PRIM(Ast.VAR(B.mkId "policy_metadata", [], B.updty dum B.PROCESS),
				    [taskvar],
				    B.updty dum B.PROCESS),
                           Ast.INT_NULL dum,
                           B.updty dum B.PROCESS)
             in
	     let init = Ast.mkPRIM("ipanema_first_task", [Ast.AREF(exp,None, dum)], -1) in
	     let taskdecl =
	       Ast.mkVALDEF(Ast.mkVARDECL(CS.UNSHARED, tasktype, task, -1), init, -1) in
	     let tests = List.map
	       (fun rst ->
		 let exp =
		   Ast.FIELD(Ast.mkPRIM("ipanema_state", [cpu], -1),
			     rst, [], B.updty dum (B.QUEUE(B.PROCESS))) in
		 Ast.ASSIGN(
		     Ast.VAR(taskid, [], B.updty dum tasktype),
                     Ast.TERNARY(Ast.VAR(taskid,[],B.updty dum tasktype),
                                 Ast.PRIM(Ast.mkVAR("ipanema_first_task",[],-1),
			                  [Ast.AREF(exp,None, dum)],
			                  B.updty dum tasktype),
                                 Ast.INT_NULL dum,
                                 B.updty dum tasktype),
                     false,
		     dum
		   )
	       )
	       (List.tl !CS.ready_st)
	     in
             (taskv, (taskdecl::decls, tests@stmts))
	 | _ -> failwith (__LOC__ ^": Unexpected !")
       end

    | Ast.FIRST(exp,crit,_,attr)
      when B.ty attr = B.PROCESS ->
       let (exp, (decls, stmts)) = compile_exp acc exp in
       let dum = B.dum __LOC__ in
       let tasktype = B.INDR (B.STRUCT (B.mkId "task_struct")) in
       let taskid = B.fresh_idx "task" in
       let task = B.id2c taskid
       and taskvar = Ast.VAR(taskid,[],B.updty dum tasktype) in
       let taskv =
	 Ast.TERNARY(taskvar,
		     Ast.PRIM(Ast.VAR(B.mkId "policy_metadata", [], B.updty dum B.PROCESS),
			      [taskvar],
			      B.updty dum B.PROCESS),
		     Ast.INT_NULL dum, B.updty dum B.PROCESS)
       in
       let init = Ast.mkPRIM("ipanema_first_task", [Ast.AREF(exp,None, dum)], -1) in
       let taskdecl =
	 Ast.mkVALDEF(Ast.mkVARDECL(CS.UNSHARED, tasktype, task, -1), init, -1) in
       (taskv, (taskdecl::decls, stmts))
       (*
       print_string "policy_metadata(ipanema_first_task(&"; pp_exp exp; print_string "))"
       *)

    | Ast.FIRST(exp, None, si, a) ->
       let (exp, acc) = compile_exp acc exp in
       let ea = Ast.get_exp_attr exp in
       let ty = B.ty ea in
       let exp = match ty with
           B.SET B.CORE -> Ast.AREF(exp, None, B.updty ea (B.INDR ty))
         | _ -> exp
       in
       (Ast.FIRST(exp, None, si, a), acc)

    | Ast.FIRST(exp, Some crit, si, a)
      when B.ty a = B.CORE ->
       let (exp, acc) = compile_exp acc exp in
     let dum = B.mkAttr (-1) in
     let iterty = B.ty (Ast.get_exp_attr exp)
     and (lazzy, order, critty, critid, critfn) = get_crit crit
     and core = Ast.VAR(B.mkId "core", [], dum)
     and coreid = B.fresh_idx "core" in
     let op = match order with
	 Ast.HIGHEST -> Ast.GT
       | Ast. LOWEST -> Ast.LT
     in
     let busiestid = B.fresh_idx "elected" in
     let busiest = B.id2c busiestid
     and busiestv = Ast.VAR(busiestid, [], B.updty dum B.CORE) in
     let expty = B.updty dum (B.INDR iterty) in
     let exp = match exp with
         Ast.PRIM(Ast.VAR(id, _, _), [], _)
            when (B.id2c id) = "system_cores" ->
          Ast.VAR((B.mkId "cpu_possible_mask"),[], B.updty dum iterty)
       | _ -> Ast.AREF(exp, None, expty)
     in
     let elected = Ast.FIRST(exp, None, si, a) in
     let initcore = Ast.AREF(Ast.PRIM(Ast.mkVAR("per_cpu",[],-1),[core;elected],dum),
			     None,
			     B.updty dum B.CORE) in
     let busiestdecl =
       Ast.UNINITDEF(Ast.mkVARDECL(CS.UNSHARED, B.CORE, busiest, -1), dum) in
     let busiest1stassign =
       Ast.mkASSIGN(busiestv, initcore, -1) in
     let busiestassign =
       Ast.mkASSIGN(busiestv, Ast.VAR(coreid,[],B.updty dum B.CORE), -1) in
     let test = Ast.IF(Ast.BINARY(op,
				  Ast.FIELD(
				    Ast.VAR(coreid,[],B.updty dum B.CORE),
				    critid,[],B.updty dum critty),
				  Ast.FIELD(busiestv,critid, [],B.updty dum critty),
				  dum),
		       Ast.SEQ([],[busiestassign],dum),Ast.SEQ([],[],dum),dum) in
     let forstmt = Ast.FOR(coreid,Some [exp],[],Ast.INC,
			   Ast.SEQ([],[test],dum),None, dum) in
     (busiestv, (busiestdecl::decls, forstmt::busiest1stassign::stmts))

    | Ast.FIRST(exp, Some crit, si, a)
      when B.ty a = B.GROUP ->
     let (exp, acc) = compile_exp acc exp in
     let dum = B.mkAttr (-1) in
     let iterty = B.ty (Ast.get_exp_attr exp)
     and Ast.VAR(iterid, _, _) = exp
     and (lazzy, order, critty, critid, critfn) = get_crit crit
     and group = Ast.VAR(B.mkId "group", [], dum)
     and groupid = B.fresh_idx "group"
     and Some(CS.COMP(domid, grpset)) = si in
     let op = match order with
	 Ast.HIGHEST -> Ast.GT
       | Ast. LOWEST -> Ast.LT
     in
     let busiestid = B.fresh_idx "elected" in
     let busiest = B.id2c busiestid
     and busiestv = Ast.VAR(busiestid, [], B.updty dum B.GROUP) in
     let initelected = Ast.INT_NULL(B.updty dum B.GROUP) in
     let busiestdecl =
       Ast.mkVALDEF(Ast.mkVARDECL(CS.UNSHARED, B.GROUP, busiest, -1),
                    initelected, -1) in
     let busiestassign =
       Ast.mkASSIGN(busiestv, Ast.VAR(groupid,[],B.updty dum B.GROUP), -1) in
     let (left, right) =
       if lazzy then
	 (critfn [Ast.VAR(groupid,[],B.updty dum B.GROUP)],
	  critfn [busiestv])
       else
	 (Ast.FIELD(Ast.VAR(groupid,[],B.updty dum B.GROUP),
		    critid,[],B.updty dum critty),
	  Ast.FIELD(busiestv,critid, [],B.updty dum critty))
     in
     let test = Ast.IF(Ast.BINARY(op, left, right, dum),
		       Ast.SEQ([],[busiestassign],dum),Ast.SEQ([],[],dum),dum) in
     let forstmt = Ast.FOR(groupid,
			   Some [Ast.VAR(iterid,[],B.updty dum iterty)],
			   [CS.COMP(domid,grpset)],
			   Ast.INC,
			   Ast.SEQ([],[test],dum),None, dum) in
     (busiestv, (busiestdecl::decls, forstmt::stmts))

    | Ast.FIRST(exp, Some crit, si, a)
      when B.ty a = B.DOMAIN ->
     let (exp, acc) = compile_exp acc exp in
     let dum = B.updty (B.mkAttr (-1)) B.DOMAIN
     and (lazzy, order, critty, critid, critfn) = get_crit crit in
     begin
       match B.id2c critid with
         "" ->
          begin
            match order with
	      Ast.HIGHEST ->
               let domid = B.fresh_idx "dom" in
               let dom = B.id2c domid
               and domv = Ast.VAR(domid, [], dum) in
               let domdecl =
                 Ast.UNINITDEF(Ast.mkVARDECL(CS.UNSHARED, B.DOMAIN, dom, -1), dum) in
               let domassign = Ast.ASSIGN(domv, Ast.updty B.DOMAIN exp, false, dum) in
               let forstmt = Ast.FOR(domid,
			             None,
			             [],
			             Ast.INC,
			             Ast.SEQ([],[],dum),None, dum) in
               (domv, (domdecl::decls, forstmt::domassign::stmts))

            | Ast. LOWEST ->
               (Ast.updty B.DOMAIN exp, (decls, stmts))
          end
       | _ -> failwith __LOC__
     end
    | exp ->
       Aux.error (Ast.get_exp_attr exp, "");
       Pp.pretty_print_exp exp;
       failwith (__LOC__ ^": Uncompiled expression while looking for 'first' expression !")

(* -------------------------- Declarations ------------------------- *)

let compile_decl ((decls, stmts) as acc) = function
  Ast.VALDEF(Ast.VARDECL(ty,id,_,_,_,_,_) as decl,exp,isconst,attr) ->
    begin
      let (exp, (new_decls, new_stmts)) = compile_exp new_s exp in
      let assign = Ast.ASSIGN (Ast.VAR(id,[],attr), exp, false, attr) in
      (Ast.UNINITDEF(decl,attr)::new_decls@decls, stmts@(assign::new_stmts))
    end
  | decl -> (decl::decls,stmts)

(* --------------------------- Statements -------------------------- *)

let state_id = function
    CS.STATE(id,_,_,_) -> id
  | CS.CSTATE(id,_) -> failwith "Unexpected CSTATE"

let rec compile_stmt ((decls, stmts) as acc) = function
    Ast.IF(exp,stmt1,stmt2,attr) ->
      let (exp, (decls, stmts)) = compile_exp acc exp in
      let stmt1 = compile_scope new_s stmt1 in
      let stmt2 = compile_scope new_s stmt2 in
      (decls, Ast.IF(exp, stmt1, stmt2, attr)::stmts)

    | Ast.FOR(id,state_id,x,dir,stmt,crit,attr) ->
      let stmt = compile_scope new_s stmt in
       (decls, Ast.FOR(id,state_id,x,dir, stmt,crit,attr)::stmts)

    | Ast.SWITCH(exp,cases,default,attr) ->
       let (exp, (decls, stmts)) = compile_exp acc exp in
       (decls,
	Ast.SWITCH(exp,
		   List.map
		     (function Ast.SEQ_CASE(idlist,x,stmt,y) ->
		       let stmt = compile_scope new_s stmt in
		       Ast.SEQ_CASE(idlist,x, stmt,y))
		     cases,Aux.app_option (compile_scope new_s) default,attr)::stmts)
    | Ast.SEQ(seq_decls,seq_stmts,attr) ->
       let (new_decls, add_stmts) = List.fold_left compile_decl new_s (List.rev seq_decls) in
       let scope = (new_decls, add_stmts) in
       let (seq_decls, seq_stmts) = List.fold_left compile_stmt scope seq_stmts in
       (decls, Ast.SEQ(seq_decls, List.rev seq_stmts,attr)::stmts)

    | Ast.RETURN(Some(exp),attr) ->
       let (exp, (decls, stmts)) = compile_exp acc exp in
       (decls, Ast.RETURN(Some(exp),attr)::stmts)

    | Ast.MOVE(exp,state,x,y,z,a,b,attr) ->
       let (exp, acc) = compile_exp acc exp in
       let (state, (decls, stmts)) = compile_exp acc state in
	(decls, Ast.MOVE(exp, state,x,y,z,a,b,attr)::stmts)

    | Ast.ASSIGN(expl,expr,sorted_fld,attr) ->
       let (expr, acc) = compile_exp acc expr in
       let (expl, (decls, stmts)) = compile_exp acc expl in
       (decls, Ast.ASSIGN(expl, expr,sorted_fld,attr)::stmts)

    | Ast.PRIMSTMT(f,args,attr) ->
       let (args, accl) = List.split (List.map (compile_exp new_s) args) in
       let (declsl, stmtsl) = List.split accl in
       (List.concat declsl@decls, Ast.PRIMSTMT(f,args,attr)::(List.concat stmtsl)@stmts)

    | x -> (decls, x::stmts)

and compile_scope scope stmt =
  match compile_stmt scope stmt with
    ([], [stmt]) -> stmt
  | (decls, stmts) -> Ast.SEQ(decls, stmts, B.mkAttr (-1))

(* --------------------- Handlers and functions --------------------- *)

(* parser should check that event names are ok *)
let compile_handlers handlers =
  List.map
    (function (Ast.EVENT(nm,param,stmt,syn,attr)) ->
      let stmt = compile_scope new_s stmt in
      Ast.EVENT(nm,param, stmt, syn, attr))
    handlers

let compile_func functions =
  List.map
    (function Ast.FUNDEF(tl, ty, nm, pl, stmt, inl, attr) ->
      let (_,stmt::_) = compile_stmt new_s stmt in
      Ast.FUNDEF(tl, ty, nm, pl, stmt, inl, attr)) functions

(* -------------------------- entry point --------------------------- *)

let is_ready acc = function
    Ast.QUEUE(CS.READY, _, _, id, _, _, _) -> id::acc
  | Ast.PROCESS(CS.READY, _, id, _, _, _) -> id::acc
  | _ -> acc

let compile_first
    (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		   fundecls,valdefs,domains,pcstate,cstates,criteria,
		   trace,handlers,chandlers,ifunctions,functions,attr)) =
  let proc_st = match pcstate with
      CORE(_, sts, _) -> sts
    | PSTATE sts -> sts
  in
  CS.ready_st := List.rev (List.fold_left is_ready [] proc_st);
  Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		fundecls,valdefs,domains,
		pcstate,cstates,
		criteria, trace,
		compile_handlers handlers,
		compile_handlers chandlers,
		ifunctions,
		compile_func functions, attr)
