module B = Objects
module CS = Class_state

(* The purpose of this phase is to remove conditionals where the test is always true or always false.  If the conditional was part of the source program, a warning is emitted. *)

(* Also convert => statements to function calls *)

(* -------------------- Miscellaneous operations -------------------- *)

let current_handler = ref ""

let warning (loc,str) =
  match loc with
    Some lstr ->
      Printf.printf "%s: warning: %s: %s\n" lstr (!current_handler) str
  | None -> Printf.printf "warning: %s: %s\n" (!current_handler) str

let pr = Printf.sprintf

(* ------------ Manage instances of specialized handlers ------------ *)

let tbl = (Hashtbl.create(100) :
	     (int, (int ref * Objects.testval list ref)) Hashtbl.t)

let add n v =
  if not(n=0)
  then
    begin
      try
	let (ctr,vl) = Hashtbl.find tbl n in
	ctr := !ctr + 1;
	vl := Aux.union [v] !vl;
      with Not_found -> Hashtbl.add tbl n ((ref 1), ref [v])
    end

let dec n =
  if n=0
  then (0,[])
  else
    begin
      try
	let (ctr,vl) = Hashtbl.find tbl n in
	ctr := !ctr - 1;
	(!ctr,!vl)
      with Not_found -> raise (Error.Error (pr "unknown test %d" n))
    end

let rec collect = function
    Ast.IF(Ast.EMPTY(_,_,vl,_,eattr),stm1,stm2,attr)
  | Ast.IF(Ast.IN(_,_,_,vl,_,_,eattr),stm1,stm2,attr)
  | Ast.IF(Ast.ALIVE(vl,_,eattr),stm1,stm2,attr) ->
      add (B.test_index eattr) vl;
      (match vl with
	B.TRUE -> collect stm1
      |	B.FALSE -> collect stm2
      |	B.BOTH -> collect stm1; collect stm2
      |	B.UNK -> ())
  | Ast.IF(exp,stm1,stm2,attr) ->
      collect stm1; collect stm2

  | Ast.FOR(id,stid,l,dir,stm,crit,attr) -> collect stm

  | Ast.FOR_WRAPPER(label,stms,attr) -> List.iter collect stms

  | Ast.SWITCH(exp,cases,default,attr) ->
      List.iter
	(function Ast.SEQ_CASE(pat,l,stm,attr) -> collect stm)
	cases;
      let _ = Aux.app_option collect default in ()

  | Ast.SEQ(decls,stmts,attr) -> List.iter collect stmts

  | stmt -> ()

(* -------------------------- Optimization -------------------------- *)

let vl2b vl =
  if List.mem B.TRUE vl then "true"
  else if List.mem B.FALSE vl then "false"
  else "unknown"

let check_warning insrc attr state str =
  let index = B.test_index attr in
  if not (index = 0)
  then
    begin
      let (ctr,vl) = dec (B.test_index attr) in
      if ctr = 0
      then
	begin
	  if not(List.mem B.BOTH vl ||
	  (List.mem B.TRUE vl && List.mem B.FALSE vl))
	  then warning(Some(B.loc2c attr),
		       Printf.sprintf "%s %s condition always %s"
			 str (B.id2c state) (vl2b vl))
	end
    end

let rec optimize_stmt = function
    Ast.IF(Ast.EMPTY(_,_,vl,insrc,eattr) as exp,stm1,stm2,attr)
  | Ast.IF(Ast.IN(_,_,_,vl,insrc,_,eattr) as exp,stm1,stm2,attr)
  | Ast.IF(Ast.ALIVE(vl,insrc,eattr) as exp,stm1,stm2,attr) ->
      (match exp with
	Ast.EMPTY(id,states,vl,insrc,eattr) ->
	  check_warning insrc eattr id "empty"
      |	Ast.IN(_,Ast.VAR(id,_,_),states,vl,insrc,_,eattr) ->
	 check_warning insrc eattr id "in"
      (* Could we optimize IN with FIELD too ?*)
      |	Ast.ALIVE(vl,insrc,eattr) ->
	  check_warning insrc eattr (B.mkId "") "alive"
      |	_ -> ());
      (match vl with
	B.TRUE -> optimize_stmt stm1
      |	B.FALSE -> optimize_stmt stm2
      |	B.BOTH ->
	  if insrc
	  then
	    let stm1 = optimize_stmt stm1 in
	    let stm2 = optimize_stmt stm2 in
	    Ast.IF(exp,stm1,stm2,attr)
	  else
	    let stm1 = optimize_stmt stm1 in
	    let stm2 = optimize_stmt stm2 in
	    if stm1 = stm2
	    then stm1
	    else Ast.IF(exp,stm1,stm2,attr)	    
      |	B.UNK -> Ast.SEQ([],[],attr))
  | Ast.IF(exp,stm1,stm2,attr) ->
      Ast.IF(exp,optimize_stmt stm1,optimize_stmt stm2,attr)

  | Ast.FOR(id,stid,l,dir,stm,crit,attr) ->
      Ast.FOR(id,stid,l,dir,optimize_stmt stm,crit,attr)

  | Ast.FOR_WRAPPER(label,stms,attr) ->
      Ast.FOR_WRAPPER(label,List.map optimize_stmt stms,attr)

  | Ast.SWITCH(exp,cases,default,attr) ->
      Ast.SWITCH(exp,
		 List.map
		   (function Ast.SEQ_CASE(pat,l,stm,attr) ->
		     Ast.SEQ_CASE(pat,l,optimize_stmt stm,attr))
		   cases,
		 Aux.app_option optimize_stmt default,
		 attr)

  | Ast.SEQ(decls,stmts,attr) ->
      Ast.SEQ(decls,List.map optimize_stmt stmts,attr)

  | stmt -> stmt

let optimize_handlers handlers =
  List.iter
    (function (Ast.EVENT(nm,param,stmt,syn,attr)) ->
      current_handler := Ast.event_name2c nm;
      collect stmt)
    handlers;
  List.map
    (function (Ast.EVENT(nm,param,stmt,syn,attr)) ->
      current_handler := Ast.event_name2c nm;
      Ast.EVENT(nm,param,optimize_stmt stmt,syn,attr))
    handlers

let optimize_functions functions =
  List.iter
    (function (Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr)) ->
      current_handler := B.id2c nm;
      collect stmt)
    functions;
  List.map
    (function (Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr)) ->
      current_handler := B.id2c nm;
      Ast.FUNDEF(tl,ret,nm,params, optimize_stmt stmt, inl, attr))
    functions

let optimize
    (Ast.SCHEDULER(nm,
		   cstdefs,enums,dom,grp,procdecls,
		   fundecls,valdefs,domains,states,cstates,criteria,
		   trace,handlers,chandlers,ifunctions,functions,attr)) =
  (Ast.SCHEDULER(nm,
		 cstdefs,enums,dom,grp,procdecls,
		 fundecls,valdefs,domains,states,cstates,criteria,
		 trace,
		 optimize_handlers handlers,
		 chandlers,
		 optimize_functions ifunctions,functions,attr))
