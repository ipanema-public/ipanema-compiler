(* $Id$ *)

(*
   - convert next expression to appropriate field access
*)

module B = Objects
module CS = Class_state
module VB = Vobjects

let event_param = ref((B.mkId ""),B.PEVENT)

let rec compile_exp = function

    Ast.FIELD(exp,fld,_,attr) ->
      let (exp,expfn) = compile_exp exp in
      (Ast.FIELD(exp,fld,[],attr),expfn)

  | Ast.UNARY(uop,uexp,attr) ->
      let (uexp,expfn) = compile_exp uexp in
      (Ast.UNARY(uop,uexp,attr),expfn)

  | Ast.BINARY(bop,exp1,exp2,attr) ->
      let (exp1,exp1fn) = compile_exp exp1 in
      let (exp2,exp2fn) = compile_exp exp2 in
      (Ast.BINARY(bop,exp1,exp2,attr),function stm -> exp1fn(exp2fn stm))

  | Ast.PBINARY(bop,exp1,states1,exp2,states2,attr) ->
      let (exp1,exp1fn) = compile_exp exp1 in
      let (exp2,exp2fn) = compile_exp exp2 in
      (Ast.PBINARY(bop,exp1,states1,exp2,states2,attr),
       function stm -> exp1fn(exp2fn stm))

  | Ast.INDR(exp,attr) ->
      let (exp,expfn) = compile_exp exp in
      (Ast.INDR(exp,attr),expfn)

  | Ast.PRIM(f,args,attr) ->
      let (f,ffn) = compile_exp f in
      let (args,argsfn) =
	List.fold_left
	  (function (prevargs,prevfn) ->
	    function exp ->
	      let (exp,expfn) = compile_exp exp in
	      (exp::prevargs,function stm -> prevfn(expfn stm)))
	  ([], ffn) args in
      let args = List.rev args in
      (Ast.PRIM(f,args,attr),argsfn)

  | Ast.AREF(exp1,Some(exp2),attr) ->
      let (exp1,exp1fn) = compile_exp exp1 in
      let (exp2,exp2fn) = compile_exp exp2 in
      (Ast.AREF(exp1,Some(exp2),attr),function stm -> exp1fn(exp2fn stm))

  | Ast.AREF(exp1,None,attr) ->
     let (exp1,exp1fn) = compile_exp exp1 in
     (Ast.AREF(exp1,None,attr), function stm -> exp1fn stm)

  | Ast.IN(exp,id,states,vl,insrc,crit,in_attr) ->
      let (exp,expfn) = compile_exp exp in
      (Ast.IN(exp,id,states,vl,insrc,crit,in_attr),expfn)

  | Ast.MOVEFWDEXP(exp,state_end,attr) ->
      let (exp,expfn) = compile_exp exp in
      (Ast.MOVEFWDEXP(exp,state_end,attr),expfn)

  | Ast.SCHEDCHILD(exp,procs,attr) ->
      let (exp,expfn) = compile_exp exp in
      let mkref str =
          Ast.FIELD(Ast.VAR(B.mkId(str^"_next_list"),[],attr),B.mkId "sched",[],attr)
      in
      (match procs with
	[] -> raise (Error.Error "next: internal error []")
      | [VB.SRC] -> (mkref "src",expfn)
      | [VB.TGT] -> (mkref "tgt",expfn)
      | [x;y] as l ->
	  if (x = VB.SRC && y = VB.TGT) || (x = VB.TGT && y = VB.SRC)
	  then
	    let new_id = B.fresh_id () in
	    let new_attr = B.updty attr B.SCHEDULER in
        let var_ref = Ast.VAR(new_id,[],new_attr) in
	    let var_def =
	      Ast.SYSDEF(Ast.VARDECL(B.SCHEDULER,new_id,false,false,CS.UNSHARED,None,new_attr),
			 Ast.VARIABLE,new_attr) in
	    let (ev_id, ev_typ) = !event_param in
	    let tgt_exp =
            Ast.FIELD(Ast.VAR(ev_id,[],B.updty attr ev_typ),
            B.mkId "target",[],B.updty attr B.PROCESS) in
	    let test_exp =
	      Ast.BINARY(Ast.EQ,exp,tgt_exp,B.updty attr B.BOOL) in
	    let if_stmt =
	      Ast.IF(test_exp,
		     Ast.ASSIGN(var_ref,mkref "tgt",false,attr),
		     Ast.ASSIGN(var_ref,mkref "src",false,attr),
		     attr) in
	    (var_ref,
	     function stm ->
	       Ast.mkDBLOCK attr ([var_def],[if_stmt;expfn stm]))
	  else raise (Error.Error (Printf.sprintf "next: internal error %s"
				     (Aux.set2c(List.map VB.vprocname2c l))))
      | l -> raise (Error.Error (Printf.sprintf "next: internal error %s"
				   (Aux.set2c(List.map VB.vprocname2c l)))))
	
  | exp -> (exp,function stm -> stm)
	
let compile_decl = function
    Ast.VALDEF(decl,exp,is_const,attr) ->
      let (exp,expfn) = compile_exp exp in
      (Ast.VALDEF(decl,exp,is_const,attr),expfn)
  | decl -> (decl,function stm -> stm)
	
let rec compile_stmt = function
    Ast.IF(exp,stmt1,stmt2,attr) ->
      let (exp,expfn) = compile_exp exp in
      expfn (Ast.IF(exp,compile_stmt stmt1,compile_stmt stmt2,attr))
	
  | Ast.FOR(id,state_id,states,dir,stmt,crit, attr) ->
      Ast.FOR(id,state_id,states,dir,compile_stmt stmt,crit, attr)
	
  | Ast.FOR_WRAPPER(label,stmts,attr) ->
      Ast.FOR_WRAPPER(label,List.map compile_stmt stmts,attr)
	
  | Ast.SWITCH(exp,cases,default,attr) ->
      let (exp,expfn) = compile_exp exp in
      expfn (Ast.SWITCH(exp,List.map compile_case cases,
			Aux.app_option compile_stmt default,attr))
	
  | Ast.SEQ(decls,stmts,attr) ->
      let (decls,declsfn) =
	List.fold_left
	  (function (prevdecls,prevfn) ->
	    function decl ->
	      let (decl,declfn) = compile_decl decl in
	      (decl::prevdecls,function stm -> prevfn(declfn stm)))
	  ([], function stm -> stm) decls in
      let decls = List.rev decls in
      declsfn(Ast.SEQ(decls,List.map compile_stmt stmts,attr))
	
  | Ast.RETURN(None,attr) as x -> x
	
  | Ast.RETURN(Some(exp),attr) ->
      let (exp,expfn) = compile_exp exp in
      expfn(Ast.RETURN(Some(exp),attr))
	
  | Ast.MOVE(exp,state,x,y,z,a,b,attr) ->
      let (exp,expfn) = compile_exp exp in
      expfn(Ast.MOVE(exp,state,x,y,z,a,b,attr))
	
  | Ast.MOVEFWD(exp,x,state_end,attr) ->
      let (exp,expfn) = compile_exp exp in
      expfn(Ast.MOVEFWD(exp,x,state_end,attr))
	
  | Ast.SAFEMOVEFWD(exp,x,stm,state_end,attr) ->
      raise
	(Error.Error
	   "compile_next: unexpected safe move forward statement\n")
	
  | Ast.ASSIGN(expl,expr,sorted_fld,attr) ->
      let (expl,explfn) = compile_exp expl in
      let (expr,exprfn) = compile_exp expr in
      explfn(exprfn(Ast.ASSIGN(expl,expr,sorted_fld,attr)))
	
  | Ast.PRIMSTMT(f,args,attr) ->
      let (f,ffn) = compile_exp f in
      let (args,argsfn) =
	List.fold_left
	  (function (prevargs,prevfn) ->
	    function exp ->
	      let (exp,expfn) = compile_exp exp in
	      (exp::prevargs,function stm -> prevfn(expfn stm)))
	  ([], ffn) args in
      let args = List.rev args in
      argsfn(Ast.PRIMSTMT(f,args,attr))

  | Ast.ASSERT(exp,attr) ->
      let (exp,expfn) = compile_exp exp in
      expfn(Ast.ASSERT(exp,attr))

  | x -> x

and compile_case (Ast.SEQ_CASE(idlist,l,stmt,attr)) =
  Ast.SEQ_CASE(idlist,l,compile_stmt stmt,attr)


(* --------------------- Handlers and functions --------------------- *)

let process_handlers handlers =
  List.map
    (function
	(Ast.EVENT(nm,(Ast.VARDECL(ev_type,id,imported,lz,CS.UNSHARED,de,vattr) as param),
		   stmt,syn,attr)) ->
		     event_param := (id, ev_type);
		     Ast.EVENT(nm,param,compile_stmt stmt,syn,attr))
    handlers

let compile_schedchild
    (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		   fundecls,valdefs,domains,states,cstates,
		   criteria,trace,handlers,chandlers,ifunctions,functions,attr)) =
  (Ast.SCHEDULER(nm,
		 cstdefs,enums,dom,grp,procdecls,
		 fundecls,valdefs,domains,states,cstates,criteria,
		 trace,
                 process_handlers handlers,
                 chandlers,
		 ifunctions,functions,attr))
