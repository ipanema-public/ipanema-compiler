(* $Id$ *)

(* among previous_needed, and process_stmt code duplication
is possible.  To fix... *)

module B = Objects
module CS = Class_state
module CSel = Compile_select

let in_handler = ref false
let in_ev = ref ""
let move_core = ref false
let steal_block = ref None

let error ln str =
  Error.update_error();
  Printf.printf "%s: %s \n" (B.loc2c ln) str

(* -------------------------- Useful syntax ------------------------- *)

let state_names states =
  List.map (function CS.STATE(id,_,_,_) -> id) states

let switch_error states attr =
  match Aux.subtract (CS.all_states()) states with
    [] -> []
  | rest ->
      let rest_names = state_names rest in
      [Ast.SEQ_CASE(rest_names,rest,
		    Ast.ERROR(Printf.sprintf
				"unexpected process state: %s"
				(String.concat " or "
				   (List.map B.id2c rest_names)),
			      attr),
		    attr)]

let make_cstate_cst id =
  B.mkId(Printf.sprintf "%s_STATE" (String.uppercase (B.id2c id)))

let make_state_cst id =
  let cls =
    let rec lookup x = function
        [] -> raise (Error.Error ("internal error: no class for state "^ B.id2c id))
      | ((y,CS.STATE(_,cls,_,_))::r) -> if x = y then cls else lookup x r in
    lookup id (!CS.state_env) in
  if !in_ev = "tick" && cls = CS.READY  then
    B.mkId(Printf.sprintf "IPANEMA_READY_TICK")
  else
    B.mkId(CS.class2c cls)

let make_class_cst id =
  let cls =
    let rec lookup x = function
	[] -> raise (Error.Error "internal error: no class for state")
      | ((y,CS.STATE(_,cls,_,_))::r) -> if x = y then cls else lookup x r in
    lookup id (!CS.state_env) in
  B.mkId(CS.class2c cls)

(* ---------------- Reorganization of move statements --------------- *)

type source_type =
    PROC of CS.state_info | KEY of CS.state_info |
    QUEUE of CS.state_info list | NOWHERE of CS.state_info list

let classify_sources key_sorted states dst : source_type list =
  let (procs,key,queues) =
    List.fold_left
      (function (procs,key,queues) ->
	function
	  CS.STATE(_,_,CS.PROC,_) as x -> (x::procs,key,queues)
	| CS.STATE(_,_,CS.QUEUE(CS.SELECT(_)),_) as x ->
	    if key_sorted then (procs,x::key,queues) else (procs,key,x::queues)
	| CS.STATE(_,_,CS.QUEUE(_),_) as x -> (procs,key,x::queues))
      ([],[],[]) states in
  let (queues,nowhere) =
    List.partition
      (function
	  CS.STATE(_,CS.NOWHERE,CS.QUEUE(_),_) -> false
	| _ -> true)
      queues in
  (List.map (function p -> (PROC p)) procs) @
  (List.map (function p -> (KEY p)) key) @
  (if queues = [] then [] else [(QUEUE queues)]) @
  (if nowhere = [] then [] else [(NOWHERE nowhere)])

let classify_dst dynamic_sort state override =
  match (state,override) with
    (CS.STATE(_,_,CS.PROC,_),_) -> "proc"
  | (CS.STATE(_,_,CS.QUEUE(CS.FIFO),_),_) -> "queue"
  | (CS.STATE(_,_,CS.QUEUE(CS.SELECT(_)),_),Ast.HEAD) ->
      (match dynamic_sort with
	CSel.SORTED -> "sorted_lifo"
      |	CSel.UNSORTED -> "lifo"
      |	CSel.PARTIALLY_SORTED -> "partially_sorted_lifo")
  | (CS.STATE(_,_,CS.QUEUE(CS.SELECT(_)),_),Ast.TAIL) ->
      (match dynamic_sort with
	CSel.SORTED -> "sorted_fifo"
      |	CSel.UNSORTED -> "fifo"
      |	CSel.PARTIALLY_SORTED -> "partially_sorted_fifo")
  | (CS.STATE(_,_,CS.QUEUE(CS.SELECT(CS.NODEFAULT)),_),_) ->
      (match dynamic_sort with
	CSel.SORTED -> "sorted"
      |	CSel.UNSORTED -> "queue"
      |	CSel.PARTIALLY_SORTED -> "partially_sorted_lifo") (* lifo is cheaper *)
  | (CS.STATE(_,_,CS.QUEUE(CS.SELECT(CS.FIFODEFAULT)),_),_) ->
      (match dynamic_sort with
	CSel.SORTED -> "sorted_fifo"
      |	CSel.UNSORTED -> "fifo"
      |	CSel.PARTIALLY_SORTED -> "partially_sorted_fifo")
  | (CS.STATE(_,_,CS.QUEUE(CS.SELECT(CS.LIFODEFAULT)),_),_) ->
      (match dynamic_sort with
	CSel.SORTED -> "sorted_lifo"
      |	CSel.UNSORTED -> "lifo"
      |	CSel.PARTIALLY_SORTED -> "partially_sorted_lifo")
  | (CS.STATE(_,_,CS.QUEUE(CS.EMPTY),_),_) -> "nowhere"
  | (CS.CSTATE _, _) -> "core"

let states2str states = Aux.set2c(List.map B.id2c (state_names states))

let regroup (decl,stm) = Ast.SEQ([decl],[stm],Ast.get_stm_attr stm)

let src2dst srcinfo decls srcexp dstid dfname dstexp dstcls
    auto_allowed attr before state_upd fromprocname =
  let (sfname,srcexp,sstates,srcid,srckey) =
    (match srcinfo with
      PROC(CS.STATE(id,_,_,_) as x) ->
          ("proc", Ast.VAR(id,[],attr), [x], Some id, false)
    | KEY(CS.STATE(id,_,_,_) as x) ->
	("queue", srcexp, [x], Some id, true)
    | QUEUE(sstates) ->
	let srcid =
	  match sstates with
	    [CS.STATE(id,_,_,_)] -> Some id
	  | _ -> None in
	("queue", srcexp, sstates, srcid, false)
    | NOWHERE(sstates) -> ("nowhere", srcexp, [], None, false)) in
  let mk_fname str = Ast.VAR(B.mkId str, [],B.updty attr B.VOID) in
  if not auto_allowed && Some(dstid) = srcid
  then None
  else
	(* resched is handled by the runtime. *)
(*    let need_resched =
      	let down_resched_code =
	      Ast.PRIMSTMT(Ast.VAR(B.mkId "set_tsk_need_resched",
                   [], B.updty attr B.VOID),
               [Ast.VAR(B.mkId "current",[],B.updty attr B.VOID)],
			   attr) in
	let up_resched_code =
	  Ast.IF(Ast.BINARY(Ast.EQ,
                Ast.VAR(B.mkId("root_s_state"),[],attr),
                Ast.VAR(B.mkId("BLOCKED"),[],attr),
			    B.updty attr B.BOOL),
		 down_resched_code,
		 Ast.SEQ([],[],attr),attr) in
	let is_blocked = function
	    CS.BLOCKED -> true
	  | _ -> false in
	match srcinfo with
	  PROC(CS.STATE(_,CS.RUNNING,_,_)) -> [down_resched_code]
	| PROC(CS.STATE(_,cls,_,_)) when is_blocked cls ->
             if dstcls = CS.READY then [up_resched_code] else []
	| QUEUE(srcs) ->
	   let (blocked,others) =
	     List.partition
		(function CS.STATE(_,CS.BLOCKED,_,_) -> true | _ -> false)
		srcs in
	   (match (blocked,others,dstcls) with
	      ([],_,_) -> []
	    | (_,[],CS.READY) -> [up_resched_code]
	    | (_,[],_) -> []
	    | (blocked,others,CS.READY) ->
	       [Ast.IF(Ast.IN(srcexp,
			      Ast.VAR(B.mkId("BLOCKED"),[],attr),
			      blocked,B.BOTH,false, None,
			       B.updty attr B.BOOL),
			up_resched_code, Ast.SEQ([],[],attr), attr)]
	    | _ -> [])
      | _ -> [] in *)
    let args = [Ast.add_comment (states2str sstates) srcexp;dstexp] in
    let args =
      if (!B.hls)
      then
	let sched_inst_type = B.SCHEDULER_INST in
    (Ast.VAR(B.mkId "scheduler_instance",[],B.updty attr sched_inst_type))
	:: args
      else args in
    let stmt1 =
      Ast.PRIMSTMT(mk_fname(Printf.sprintf "move_%s_%s" sfname dfname),
		   args,attr) in
    let stmt2 =
      if srckey
      then
	let src =
	  match srcid with
        Some(id) -> Ast.VAR(id,[],attr)
	  | None -> failwith "internal error" in
	[Ast.PRIMSTMT(mk_fname "dec_bitmap",
		      [src;Ast.PRIM(mk_fname "key_index",[srcexp],
				    B.updty attr B.INT)],
		      attr)]
      else []
    in
    Some
      (Ast.mkBLOCK attr ((* need_resched@*)(stmt1::stmt2)))

let mkifin (CS.STATE(id,_,_,_) as test_state)
    new_src_exp srcinfo1 srcinfo2 dstid dfname dstexp dstcls auto_allowed attr
    before state_upd decls =
  let after = if before then [] else [state_upd] in
  let locexp = Ast.VAR(id,[],attr) in
  let in_test = Ast.IN(new_src_exp,locexp,[test_state],B.BOTH,false,None,attr) in
  let thn =
    src2dst srcinfo1 decls new_src_exp dstid dfname dstexp dstcls
      auto_allowed attr before state_upd false in
  let els =
    src2dst srcinfo2 decls new_src_exp dstid dfname dstexp dstcls
      auto_allowed attr before state_upd false in
  match (thn,els) with
    (None,None) -> []
  | (None,Some els) ->
      [Ast.IF(in_test,Ast.mkBLOCK attr [],Ast.postBLOCK attr els after,attr)]
  | (Some thn,None) ->
      [Ast.IF(in_test,Ast.postBLOCK attr thn after,Ast.mkBLOCK attr [],attr)]
  | (Some thn,Some els) -> Ast.IF(in_test,thn,els,attr) :: after

let srcs2dst srcexp srcs dstexp state auto_allowed attr
             dst override dynamic_sort key decls =
  let dfname = classify_dst dynamic_sort dst override in
  let CS.STATE(dstid,dstcls,state_type,_) = dst in
  let before =
    match state_type with
      CS.QUEUE(CS.EMPTY) -> true
    | _ -> false in
  let auto_allowed =
    match state_type with
      CS.QUEUE(CS.SELECT(_)) -> auto_allowed
    | _ -> false in
  let mk_fname str = Ast.VAR(B.mkId str, [],B.updty attr B.VOID) in
  let state_upd =
    Ast.PRIMSTMT(mk_fname "ipa_change_raw",
                 [srcexp;Ast.VAR(make_state_cst dstid,[], attr);
                  Ast.VAR(make_class_cst dstid,[],attr)],
		 attr) in
  match classify_sources key srcs dst with
    (* FIXME: Update the verifier to set up the srcs !!! *)
    []| [_] ->
      (let intty = B.updty attr B.INT in
       match dst with
	 CS.STATE(_,CS.RUNNING,CS.PROC, _) ->
         begin
           match dstexp with
             Ast.FIELD(exp,_,_,_) ->
	      [Ast.PRIMSTMT(mk_fname "ipa_change_curr",
			    [Ast.AREF(Ast.LCORE("core", Ast.INT_NULL attr, Some exp, attr), None, attr);
                             srcexp],
			    attr)]
           | Ast.VAR(_,_,_) ->
	      [Ast.PRIMSTMT(mk_fname "ipa_change_curr",
			    [Ast.AREF(Ast.LCORE("core", Ast.INT_NULL attr, None, attr), None, attr);
                             srcexp],
			    attr)]
         end
      | CS.STATE(_,_,CS.QUEUE _, _) ->
         begin
           match dstexp with
            Ast.FIELD(Ast.AREF(Ast.PRIM(_, _::[destcore],_),_,_) as exp,_,_,_) ->
	      [Ast.PRIMSTMT(mk_fname "ipa_change_queue",
			    [srcexp;
			     Ast.AREF(
			       Ast.LCORE("state_info", Ast.VAR(dstid, [], attr), Some destcore, attr),
			       None, attr);
			     Ast.VAR(make_state_cst dstid,[], attr);
                             exp],
			    attr)]
	   | Ast.FIELD(exp,_,_,_) ->
	      begin
		match !steal_block with
		  Some busiest_id ->
		    let intty = B.updty attr B.INT in
		    let corety = B.updty attr B.CORE in
		    let busiest = Ast.VAR(busiest_id, [], corety) in
		    let dbg_cpt = Ast.VAR(B.mkId "dbg_cpt", [], intty) in
		    [Ast.PRIMSTMT(mk_fname "list_add",
				  [Ast.AREF(
				    Ast.FIELD(
				      Ast.FIELD(mk_fname "pos",
						B.mkId "ipanema", [],
						B.updty attr (B.OPAQUE "ipa_metadata")),
				      B.mkId "ipa_tasks",[],attr), None, attr);
				   Ast.AREF(mk_fname (B.id2c dstid ^"_tasks"),None,attr)], attr);
		     Ast.PRIMSTMT(mk_fname "ipa_change_queue",
				  [srcexp;Ast.VAR(B.mkId "NULL", [], attr);
				   Ast.VAR(B.mkId "IPANEMA_MIGRATING",[], attr);
				   Ast.FIELD(exp, B.mkId "id", [], intty)],
				  attr);
                     Compile_wmb.smp_wmb attr;
		     Ast.ASSIGN(Ast.FIELD(busiest, B.mkId Ipanema.cload, [], intty),
				Ast.BINARY(Ast.MINUS,
					   Ast.FIELD(busiest, B.mkId Ipanema.cload, [], intty),
					   Ast.FIELD(srcexp, B.mkId Ipanema.load, [], intty),
					   intty),
				false, intty);
                     Compile_wmb.smp_wmb attr;
                     Ast.ASSIGN(Ast.FIELD(exp, B.mkId Ipanema.cload, [], intty),
				Ast.BINARY(Ast.PLUS,
					   Ast.FIELD(exp, B.mkId Ipanema.cload, [], intty),
					   Ast.FIELD(srcexp, B.mkId Ipanema.load, [], intty),
					   intty),
				false, intty);
		     Ast.ASSIGN(dbg_cpt,
				Ast.BINARY(Ast.PLUS, dbg_cpt, Ast.INT(1, intty), intty),
				false, intty)]
		| None ->
                   let newcpu_id = Ast.FIELD(exp, B.mkId "id",[],(B.dum __LOC__)) in
		   [Ast.PRIMSTMT(mk_fname "ipa_change_queue",
				 [srcexp;
				  Ast.AREF(
				      Ast.LCORE("state_info", Ast.VAR(dstid, [], attr), Some newcpu_id, attr),
				      None, attr);
				  Ast.VAR(make_state_cst dstid,[], attr);
				  Ast.FIELD(exp, B.mkId "id", [], intty)],
				 attr);
                    Compile_wmb.smp_wmb attr

                   (*;
                    Compile_wmb.smp_wmb attr;
		    Ast.ASSIGN(Ast.FIELD(Ast.SELF attr, B.mkId Ipanema.cload, [], intty),
			       Ast.BINARY(Ast.MINUS,
					  Ast.FIELD(Ast.SELF attr, B.mkId Ipanema.cload, [], intty),
					  Ast.FIELD(srcexp, B.mkId Ipanema.load, [], intty),
					  intty),
			       false, intty);
		    Ast.ASSIGN(Ast.FIELD(newcpu_id, B.mkId Ipanema.cload, [], intty),
			       Ast.BINARY(Ast.PLUS,
					  Ast.FIELD(newcpu_id, B.mkId Ipanema.cload, [], intty),
					  Ast.FIELD(srcexp, B.mkId Ipanema.load, [], intty),
					  intty),
			       false, intty);*)
                   ]
	      end
           | Ast.VAR(_,_,_) ->
              let cload_update =
                let cload = match !in_ev with
                    (* FIXME : Better choose the two alternatives *)
                    "tick" | "yield" | "block" -> Ast.LCORE("core", Ast.VAR(B.mkId Ipanema.cload, [], intty), None, intty)
                  | _ -> Ast.LCORE("core", Ast.VAR(B.mkId Ipanema.cload, [], intty), 
				   Some (Ast.PRIM(Ast.VAR(B.mkId "task_cpu", [], attr),
						  [srcexp], attr)), intty)
                in
                match !in_ev with
                  "tick" | "yield" ->
                   [Ast.ASSIGN(cload,
			       Ast.BINARY(Ast.PLUS,
					  cload,
			                  Ast.BINARY(Ast.MINUS,
					             Ast.FIELD(srcexp, B.mkId Ipanema.load, [], intty),
					             Ast.VAR(B.mkId Ipanema.oldload, [], intty),
					             intty),
                                          intty),
			       false, intty);
                    Compile_wmb.smp_wmb (B.dum __LOC__)]
                  | "block" ->
                   [Ast.ASSIGN(cload,
			       Ast.BINARY(Ast.MINUS,
					  cload,
					  Ast.VAR(B.mkId Ipanema.oldload, [], intty),
                                          intty),
			       false, intty);
                   Compile_wmb.smp_wmb (B.dum __LOC__)]
                  | _ -> []
              in
              let move =
                Ast.PRIMSTMT(mk_fname "ipa_change_queue",
			     [srcexp;
			      if dstcls = CS.TERMINATED
			         || dstcls = CS.BLOCKED then
			        Ast.INT_NULL attr
			      else
			        Ast.AREF(
				    Ast.LCORE("state_info", Ast.VAR(dstid, [], attr),
					      Some (Ast.PRIM(Ast.VAR(B.mkId "task_cpu", [], attr),
							     [Ast.FIELD(srcexp,B.mkId "task",[],attr)], attr)), attr),
				    None, attr);
			      Ast.VAR(make_state_cst dstid,[], attr);
                              Ast.mkPRIM("task_cpu",
                                         [Ast.FIELD(Ast.VAR(B.mkId "tgt", [], attr),
                                                    B.mkId "task",[],attr)
                                         ], -1)
                             ],
			     attr)
              in
              match !in_ev with
                "block" -> move :: Compile_wmb.smp_wmb (B.dum __LOC__) :: cload_update
              | _ ->cload_update @ [move ; Compile_wmb.smp_wmb (B.dum __LOC__) ]
         end
      )
    (* this is the only one that might be a process variable *)
  |  [srcinfo] ->
    (match src2dst srcinfo decls srcexp dstid dfname dstexp dstcls
	           auto_allowed attr true state_upd true with
       None -> []
      |	Some s -> [s])
  | [(PROC(s1)) as srcinfo1;srcinfo2]
    | [(KEY(s1)) as srcinfo1;srcinfo2] -> (* use if where possible *)
     mkifin s1 srcexp srcinfo1 srcinfo2 dstid dfname dstexp dstcls
	    auto_allowed attr before state_upd decls
  | [srcinfo1;(PROC(s2)) as srcinfo2]
    | [srcinfo1;(KEY(s2)) as srcinfo2] ->
     mkifin s2 srcexp srcinfo2 srcinfo1 dstid dfname dstexp dstcls
	    auto_allowed attr before state_upd decls
  | [(QUEUE([s1])) as srcinfo1;srcinfo2] ->
     mkifin s1 srcexp srcinfo1 srcinfo2 dstid dfname dstexp dstcls
	    auto_allowed attr before state_upd decls
  | [srcinfo1; (QUEUE([s2])) as srcinfo2] ->
     mkifin s2 srcexp srcinfo2 srcinfo1 dstid dfname dstexp dstcls
	    auto_allowed attr before state_upd decls
  | srcinfos -> (* more than two possibilities, so use switch *)
     let branches =
       List.map
	 (function srcinfo ->
	           let sstates =
	             (match srcinfo with
		        PROC(state) | KEY(state) -> [state]
	                | QUEUE(sstates) -> sstates
	                | NOWHERE(sstates) -> sstates) in
	           Ast.SEQ_CASE(state_names sstates,sstates,
			        (match src2dst srcinfo decls srcexp
			                       dstid dfname dstexp dstcls
			                       auto_allowed attr before
                                               state_upd false with
			           None -> Ast.mkBLOCK attr []
			         | Some s -> s),
			        attr))
	 srcinfos in
     [state_upd]
     (* [Ast.SWITCH(srcexp,branches @ switch_error srcs attr,None,attr)]@ *)
     (*   (if before then [] else [state_upd]) *)

(* ------------------------- Previous variable ---------------------- *)

let save_previous previd procid attr =
    Ast.ASSIGN(Ast.VAR(previd,[],attr),Ast.VAR(procid,[],attr),false,attr)

let rec proc_lookup id = function
    [] -> raise (Error.Error "internal error")
  | Ast.PROCESS(_,_,pid,_,_,_) as x :: xs ->
      if B.ideq(id,pid) then x else proc_lookup id xs
  | Ast.QUEUE(_,_,_,qid,_,_,_) as x :: xs ->
      if B.ideq(id,qid) then x else proc_lookup id xs

let previous_needed states process srcs attr =
  let prevprocs_others =
    List.fold_left
      (function (prevprocs,others) ->
	function
	    CS.STATE(id,_,CS.PROC,_) as st ->
	      (match proc_lookup id states with
		Ast.PROCESS(_,_,_,Ast.BOSSA_PREV(previd),_,_) ->
		  ((st,previd)::prevprocs,others)
	      |	_ -> (prevprocs,st::others))
	  | st -> (prevprocs,st::others))
      ([],[]) srcs in
  match prevprocs_others with
    ([],_) -> []
  | ([(CS.STATE(procid,_,_,_),previd)],[]) ->
      [save_previous previd procid attr]
  | ([(CS.STATE(procid,_,_,_) as st,previd)],x::xs) ->
     let locexp = Ast.VAR(procid,[],attr) in
      [Ast.IF(Ast.IN(process,locexp,[st],B.BOTH,false,None, attr),
	      save_previous previd procid attr,
	      Ast.SEQ([],[],attr),
	      attr)]
  | (prevprocs,others) ->
      let (procs,_) = List.split prevprocs
      and otherids = List.map (function CS.STATE(id,_,_,_) -> id) others in
      [Ast.SWITCH(process,
		  List.map
		    (function (CS.STATE(procid,_,_,_) as procstate,previd) ->
		      Ast.SEQ_CASE([procid],[procstate],
				   save_previous previd procid attr,
				   attr))
		    prevprocs
		  @ [Ast.SEQ_CASE(otherids,others,Ast.SEQ([],[],attr),attr)]
		  @ switch_error (procs@others) attr,
		  None,
		  attr)]

(* ---------------- if dest is RUNNING update running --------------- *)

(*
let running_dst attr = function
    CS.STATE(id,CS.RUNNING,CS.PROC,_) ->
	[Ast.ASSIGN(Ast.VAR(B.mkId "running",
                [],B.updty attr (B.STRUCT (B.mkId "bossa_struct"))),
            Ast.VAR(id,[],attr),false,
		    attr)]
  | _ -> []
*)

(* --- convert the destination of move to an array ref, if needed --- *)

let check_for_key dstexp srcexp attr = function
    (true,CS.STATE(_,_,CS.QUEUE(CS.SELECT(_)),_)) ->
      let new_index_id = B.fresh_id() in
      let new_id = B.fresh_id() in
      let ptype = B.STRUCT (B.mkId "list_head") in
      let mk_fname str = Ast.VAR(B.mkId str,[], B.updty attr B.VOID) in
      let mk_id str = Ast.VAR(str, [],B.updty attr B.INT) in
      ([Ast.VALDEF(Ast.VARDECL(B.INT,new_index_id,false,false,CS.UNSHARED,None,attr),
		   Ast.PRIM(mk_fname "key_index", [srcexp], attr),
		   Ast.VARIABLE,attr);
	 Ast.VALDEF(Ast.VARDECL(ptype,new_id,false,false,CS.UNSHARED,None,attr),
		    Ast.AREF(dstexp,Some(mk_id new_index_id),attr),
		    Ast.VARIABLE,attr)],
       [Ast.PRIMSTMT(mk_fname "inc_bitmap",[dstexp;mk_id new_index_id],attr)],
       Ast.VAR(new_id,[],attr))
  | _ -> ([],[],dstexp)

(* --------------------------- Processing --------------------------- *)

let rec process_stmt decls dynamic_sort key states = function
    Ast.IF(exp,stm1,stm2,attr) ->
      Ast.IF(exp,
	     process_stmt decls dynamic_sort key states stm1,
	     process_stmt decls dynamic_sort key states stm2,
	     attr)

  | Ast.FOR(id,stid,l,dir,stm,crit,attr) ->
      Ast.FOR(id,stid,l,dir,
	      process_stmt (id::decls) dynamic_sort key states stm, crit,
	      attr)

  | Ast.FOR_WRAPPER(label,stms,attr) ->
      Ast.FOR_WRAPPER(label,
		      List.map
			(process_stmt decls dynamic_sort key states)
			stms,
		      attr)

  | Ast.SWITCH(exp,cases,default,attr) ->
      Ast.SWITCH(exp,
		 List.map
		   (function Ast.SEQ_CASE(pat,l,stm,attr) ->
		     Ast.SEQ_CASE(pat,l,
				  process_stmt decls dynamic_sort key
				    states stm,
				  attr))
		   cases,
		 Aux.app_option
		   (process_stmt decls dynamic_sort key states)
		   default,
		 attr)

  | Ast.SEQ(ldecls,stmts,attr) ->
      let new_ids =
	List.map
	  (function
	Ast.VALDEF(Ast.VARDECL(ty,id,sys,lz,_,de,ln),exp,const,a) -> id
	  | Ast.UNINITDEF(Ast.VARDECL(ty,id,sys,lz,_,de,ln),a) -> id
	  | _ -> raise (Error.Error "unexpected local decl"))
	  ldecls in
      Ast.SEQ(ldecls,
	      List.map
		(process_stmt (new_ids @ decls) dynamic_sort
		   key states)
		stmts,
	      attr)

  | Ast.MOVE(exp,state,_,srcs,Some(dst),auto_allowed,state_end,attr) ->
    if !move_core then (* FIXME: Update to the appropriate function name *)
	Ast.PRIMSTMT(Ast.VAR(B.mkId "core_change_state_FIXME",
                   [], B.updty attr B.VOID), exp::[state], attr)
    else
      let (decls1,new_src_exp) = Ast.name_expr exp attr in
      let (decls2,stmt2,new_dst_exp) =
        check_for_key state new_src_exp attr (key,dst) in
      let save_old = previous_needed states new_src_exp srcs attr in
      let move =
	srcs2dst new_src_exp srcs new_dst_exp state auto_allowed attr
	  dst state_end dynamic_sort key decls in
      let check = match dst with
	  CS.STATE(_, CS.RUNNING, _, _) ->
	    let dum = B.updgen (B.mkAttr (-1)) __LOC__ in
	    let ret = Ast.RETURN(None, dum) in
	    let test = Ast.IF(Ast.UNARY(Ast.NOT, new_src_exp, B.updty dum BOOL),
		       Ast.SEQ([],[ret],dum),
		       Ast.SEQ([],[],dum),dum) in
	    [test]
	| _ -> []
      in
      (* let post_move = running_dst attr dst in *)
      Ast.mkDBLOCK attr (decls1 @ decls2, check @ stmt2 @ save_old @ move (* @ post_move *))

  | stmt -> stmt

(* --------------------- Handlers and functions --------------------- *)

let process_handlers dynamic_sort key states handlers =
  in_handler := true;
  List.map
    (function (Ast.EVENT(nm,(Ast.VARDECL(_,id,_,_,_,_,_) as param),
			 stmt,syn,attr)) ->
       in_ev := Ast.event_name2c nm;
       Ast.EVENT(nm,param,
		 process_stmt [id] dynamic_sort key states stmt,
		 syn,attr))
    handlers

let process_chandlers dynamic_sort key states handlers =
  in_handler := true;
  List.map
    (function (Ast.EVENT(nm,(Ast.VARDECL(_,id,_,_,_,_,_) as param),
			 stmt,syn,attr)) ->
      let Ast.EVENT_NAME(nmid, _) = nm in
      match B.id2c nmid with
	"newly_idle" | "balancing_select" ->
              Ast.EVENT(nm,param,
		        process_stmt [id] dynamic_sort key states stmt,
		        syn,attr)
      | _ -> Ast.EVENT(nm,param, stmt, syn, attr))
    handlers

let process_ifunctions dynamic_sort key states ifunctions =
  in_handler := false;
  List.map
    (function Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr) ->
      if B.id2c nm = "init" then
	move_core := true
      else
	move_core := false;
      let init_decls =
	List.map (function Ast.VARDECL(_,id,_,_,_,_,_) -> id) params in
      Ast.FUNDEF(tl,ret,nm,params,
		 process_stmt init_decls dynamic_sort key states stmt,
		 inl,attr))
    ifunctions

let compile_steal_blk ctxt (filter, select, migrate) =
  let (cv, dynamic_sort, key, states) = ctxt in
  let (v1, v2, v3, v4, stmt, expr, upd) = migrate in
  let Ast.VARDECL(_, stolen_migr_id, _,_,_,_,_) = v1 in
  steal_block := Some stolen_migr_id;
  let new_migrate = (v1, v2, v3, v4, process_stmt cv dynamic_sort key states stmt, expr, upd) in
  steal_block := None;
  (filter, select, new_migrate)

let compile_steal_thread env steal =
  match steal with
    Ast.FLAT_STEAL_THREAD st ->
      Ast.FLAT_STEAL_THREAD (compile_steal_blk env st)
  | Ast.ITER_STEAL_THREAD (st, until, post) ->
     let (cv, dynamic_sort, key, states) = env in
     Ast.ITER_STEAL_THREAD (compile_steal_blk env st,
			    until,
			    List.map (process_stmt cv dynamic_sort key states) post)

let compile_steal cv dynamic_sort key states steal =
  let env = (cv, dynamic_sort, key, states) in
  match steal with
    Ast.STEAL_THREAD s -> Ast.STEAL_THREAD (compile_steal_thread env s)
  | Ast.ITER_STEAL_GROUP (sg, st, post, postgrp) ->
     Ast.ITER_STEAL_GROUP (sg,
			   compile_steal_thread env st,
			   List.map (process_stmt cv dynamic_sort key states) post,
			   List.map (process_stmt cv dynamic_sort key states) postgrp)
  | Ast.FLAT_STEAL_GROUP (sg, st, postgrp) ->
     Ast.FLAT_STEAL_GROUP (sg, compile_steal_thread env st,
			   List.map (process_stmt cv dynamic_sort key states) postgrp)


let compile_steal_param cv dynamic_sort key states (dom, dstg, dst, steal) =
  (dom, dstg, dst, compile_steal cv dynamic_sort key states steal)

(* -------------------------- entry point --------------------------- *)

let compile_move
    (dynamic_sort,crit_info,
     (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		    fundecls,valdefs,domains,pcstate,cstates,
		    criteria,trace,handlers,chandlers,ifunctions,functions,
		      attr))) =
  let key = Aux.member CSel.KEYCRIT crit_info in
  let (states, new_pcstate) =
      match pcstate with
        Ast.CORE(cvd,states,Some steal) ->
	  let cv = List.map (fun (Ast.VARDECL(_, id, _, _, _, _, _)) -> id) cvd in
	  (states, Ast.CORE(cvd, states, Some (compile_steal_param cv dynamic_sort key states steal)))
      | Ast.CORE(cvd,states,None) -> (states, pcstate)
      | Ast.PSTATE(states) -> (states, pcstate)
  in
  let new_ast = (
    Ast.SCHEDULER(nm,
		  cstdefs,enums,dom,grp,procdecls,
		  fundecls,valdefs,domains,new_pcstate,cstates,criteria,
		  trace,
		  process_handlers dynamic_sort key states handlers,
		  process_chandlers dynamic_sort key states chandlers, (*FIXME PROCESSING?*)
		  process_ifunctions dynamic_sort key states ifunctions,
                  process_ifunctions dynamic_sort key states functions,
                  attr)) in
  (crit_info,Braces.braces new_ast)
