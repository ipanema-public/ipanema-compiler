(* $Id$ *)

(* for the moment, forwardImmediate not allowed in interface functions *)

module B = Objects
module CS = Class_state

(* ------------------------------ Misc ------------------------------ *)

let handler_name = ref ""
let in_unblock_wakeup = ref false
let fwdres = "fwdres"
let need_fwdres =
  ref false (* do we need to declare fwdres in the current handler *)

let cur_spec_start = "_current_specialized_start"

(* the first string below is the name of a variable derived from the
function at the start of the specialization sequence *)
type isp_info = (* collected at definition of fwd_info *)
    NOT_AN_ENTRY_POINT |
    SPECIALIZED_START of
      string * string(* specd function_name *) * Ast.expr list |
    SPECIALIZED_NOT_START of
      string * string(* specd function_name *) *
	string(* unspecd function_name *) * Ast.expr list |
    NOT_SPECIALIZED of string(* function_name *) * Ast.expr list

let error (loc,str) =
  (Error.update_error();
   match loc with
     Some lstr -> Printf.printf "%s: %s%s\n" lstr (!handler_name) str
   | None -> Printf.printf "%s%s\n" (!handler_name) str)

let internal_error n =
  raise (Error.Error (Printf.sprintf "compile_forward: internal error %d" n))

let state_name (CS.STATE(id,_,_,_)|CS.CSTATE(id,_)) = id

let states2c states =
  Aux.set2c (List.map (function st -> B.id2c(state_name st)) states)

(* -- Determine whether there is at least one forward on every path - *)

let rec find_forward = function
    Ast.IF(exp,stm1,stm2,attr) -> find_forward stm1 && find_forward stm2

  | Ast.SWITCH(exp,cases,default,attr) ->
      (List.for_all
	(function Ast.SEQ_CASE(pat,l,stm,attr) -> find_forward stm)
	cases)
	&&
      (match default with
	None -> true
      |	Some default -> find_forward default)

  | Ast.SEQ(decls,stmts,attr) ->
      (* the following wouldn't work if one of the statements were break
    or continue ,
	 but since we skip loops, as they are not guaranteed to be executed,
	 none of the statements can be break or continue *)
      List.exists find_forward stmts

  | Ast.MOVEFWD(exp,srcs_dsts,state_end,attr) -> true

  | Ast.SAFEMOVEFWD(exp,srcs_dsts,stm,state_end,attr) -> true

  | stmt -> false

(* -------------------------- HLS forward --------------------------- *)

let hls_move_fwd nm new_src_exp srcs_dsts attr =
  match nm with
    "block.*" | "unblock.preemptive" | "unblock.timer" | "system.clocktick" ->
      ([],
       Ast.FIELD(Ast.FIELD(new_src_exp,B.mkId "udvp",[],attr),
         B.mkId "State",[],attr),
       Ast.SEQ([],[],attr))
  | "bossa.schedule" | "preempt" ->
      ([Ast.PRIMSTMT(Ast.FIELD(Ast.FIELD(Ast.FIELD(Ast.FIELD(new_src_exp,
							     B.mkId "udvp",
                                 [],attr),
                           B.mkId "BottomSched",[],attr),
                     B.mkId "CB",[],attr),
                   B.mkId "callee",[],attr),
		     [],
		     attr)],
       Ast.FIELD(Ast.FIELD(new_src_exp,B.mkId "udvp",[],attr),
         B.mkId "State",[],attr),
       Ast.SEQ([],[],attr))
  | _ -> raise (Error.Error ("unknown event "^nm))

(* --------------------------- Processing --------------------------- *)

let rec process_stmt fwd_info fwd_allowed = function
    Ast.IF(exp,stm1,stm2,attr) ->
      Ast.IF(exp,
	     process_stmt fwd_info fwd_allowed stm1,
	     process_stmt fwd_info fwd_allowed stm2,
	     attr)

  | Ast.FOR(id,stid,l,dir,stm,crit, attr) ->
      Ast.FOR(id,stid,l,dir,process_stmt fwd_info fwd_allowed stm,crit, attr)

  | Ast.FOR_WRAPPER(label,stms,attr) ->
      Ast.FOR_WRAPPER(label,List.map (process_stmt fwd_info fwd_allowed) stms,
		      attr)

  | Ast.SWITCH(exp,cases,default,attr) ->
      Ast.SWITCH(exp,
		 List.map
		   (function Ast.SEQ_CASE(pat,l,stm,attr) ->
		     Ast.SEQ_CASE(pat,l,
				  process_stmt fwd_info fwd_allowed
				    stm,
				  attr))
		   cases,
		 Aux.app_option (process_stmt fwd_info fwd_allowed) default,
		 attr)

  | Ast.SEQ(decls,stmts,attr) ->
      Ast.SEQ(decls,List.map (process_stmt fwd_info fwd_allowed) stmts,attr)

  | Ast.MOVE(exp,state,tcsrc,srcs,(Some(CS.STATE(_,CS.READY,_,_)) as dst),
	     auto_allowed,state_end,attr) as x ->
      let (running_srcs,other_srcs) =
	List.partition
	  (function CS.STATE(_,CS.RUNNING,_,_) -> true
	    | _ -> false)
	  srcs in
      let mk_preempt exp =
	let child_fn =
        Ast.FIELD(Ast.FIELD(exp,B.mkId("ops"),[],attr),
            B.mkId "preempt",[],attr) in
	Ast.PRIMSTMT(child_fn,[],B.updty attr B.VOID) in
      (match (running_srcs,other_srcs) with
	([],_) -> x
      |	(_,[]) ->
	  let (decls,new_src_exp) = Ast.name_expr exp attr in
	  (* assumes that preempt is not specialized *)
	  Ast.mkDBLOCK attr
	    (decls,
	     [mk_preempt new_src_exp;
	       Ast.MOVE(new_src_exp,state,tcsrc,srcs,dst,
			auto_allowed,state_end,attr)])
      |	_ ->
	  let (decls,new_src_exp) = Ast.name_expr exp attr in
	  let running_state =
	    match running_srcs with
	      [running_state] -> running_state
	    | _ ->
		raise
		  (Error.Error
		     "a policy must have exactly one RUNNING state") in
	  Ast.IF(Ast.IN(new_src_exp,
			Ast.VAR(B.mkId (CS.state2c running_state),[],attr),
			running_srcs,B.BOTH,false,None,B.updty attr B.BOOL),
		 Ast.mkDBLOCK attr
		   (decls,
		    [mk_preempt new_src_exp;
		      Ast.MOVE(new_src_exp,state,tcsrc,running_srcs,
			       dst,auto_allowed,state_end,attr)]),
		 Ast.MOVE(exp,state,tcsrc,other_srcs,dst,auto_allowed,
			  state_end,attr),
		 attr))

  | Ast.MOVEFWD(exp,srcs_dsts,state_end,attr) ->
      process_move_forward fwd_info fwd_allowed exp
	srcs_dsts state_end attr

  | Ast.SAFEMOVEFWD(exp,srcs_dsts,stm,state_end,attr) ->
      Ast.IF(exp,
	     process_move_forward fwd_info fwd_allowed exp srcs_dsts
	       state_end attr,
	     process_stmt fwd_info fwd_allowed stm,
	     attr)

  | stmt -> stmt

and process_move_forward fwd_info fwd_allowed exp srcs_dsts
    state_end attr =
  if not fwd_allowed
  then error(Some(B.loc2c attr),"forward not allowed in interface function");
  let (decls,new_src_exp) = Ast.name_expr exp attr in
  let int_attr = B.updty attr B.INT in
  let (need_frexp,need_frstm,pre_fwd,fwd,fwds) =
    let basic_translation sched_preempt_name args name_needed =
      (*if (!B.hls)
      then hls_move_fwd sched_preempt_name new_src_exp srcs_dsts attr
      else*)
	let child_fn =
        Ast.FIELD(Ast.FIELD(new_src_exp,B.mkId("ops"),[],attr),
            B.mkId(sched_preempt_name),[],attr) in
	if !in_unblock_wakeup
	then
	  (* The first is code to run before a forward expression, the second
	     is the forward as an expression, and the third is the forward
	     as a statement.  Either the first two are used or the third is
	     used *)
	  begin
	    (true,true,
         [Ast.ASSIGN(Ast.VAR(B.mkId fwdres,[],int_attr),
			 Ast.PRIM(child_fn,args,int_attr),
			 false,
			 attr)],
         Ast.PRIM(Ast.VAR(B.mkId "drop_unblock_res", [],attr),
              [Ast.VAR(B.mkId fwdres,[],int_attr)],
		      attr),
         Ast.ASSIGN(Ast.VAR(B.mkId fwdres,[],int_attr),
			Ast.PRIM(child_fn,args,int_attr),
			false,
		      attr))
	  end
	else if name_needed
	then
	  begin
	    (true,false,
         [Ast.ASSIGN(Ast.VAR(B.mkId fwdres,[],int_attr),
			 Ast.PRIM(child_fn,args,int_attr),
			 false,
			 attr)],
         Ast.VAR(B.mkId fwdres,[],int_attr),
	     Ast.PRIMSTMT(child_fn,args,int_attr))
	  end
	else
	  (false,false,[],Ast.PRIM(child_fn,args,int_attr),
	   Ast.PRIMSTMT(child_fn,args,attr)) in
    (* idea: we can only continue to use the specialized version if we
       are using the same child scheduler.  So store the identify of the
       child scheduler on the first function in the specialized sequence,
       and then compare against the stored child on subsequent functions.
       If the test fails, we change the stored child to 0, to be sure that
       it will not match against any subsequent element of the specialization
       and use the generic version *)
    match fwd_info with
      NOT_AN_ENTRY_POINT ->
	if !in_unblock_wakeup
	then
	  (true,false,
       [Ast.ASSIGN(Ast.VAR(B.mkId fwdres,[],int_attr),
		       Ast.MOVEFWDEXP(new_src_exp,state_end,int_attr),
		       false,
		       attr)],
       Ast.PRIM(Ast.VAR(B.mkId "drop_unblock_res", [],attr),
            [Ast.VAR(B.mkId fwdres,[],int_attr)],
		    attr),
	   Ast.MOVEFWD(new_src_exp,srcs_dsts,state_end,attr))
	else
	  (false,false,[],Ast.MOVEFWDEXP(new_src_exp,state_end,int_attr),
	   Ast.MOVEFWD(new_src_exp,srcs_dsts,state_end,attr))
    | SPECIALIZED_START(flag,sched_preempt_name,args) ->
	let (need_frexp,need_frstm,before_exp,exp,stmt) =
	  basic_translation sched_preempt_name args false in
	let assign_flag =
        Ast.ASSIGN(Ast.VAR(B.mkId flag,[],B.updty attr B.SCHEDULER),
		     new_src_exp,false,attr) in
	(need_frexp,need_frstm,
	 assign_flag::before_exp,exp,Ast.SEQ([],[assign_flag;stmt],attr))
    | SPECIALIZED_NOT_START(flag,sched_preempt_name,unspecd_name,args) ->
            let flag_id = Ast.VAR(B.mkId flag,[],B.updty attr B.SCHEDULER) in
	let (need_frexp,need_frstm,before_exp,exp,stmt) =
	  basic_translation sched_preempt_name args true in
	let (_,_,ubefore_exp,_,ustmt) =
	  basic_translation unspecd_name args true in
	let clear_flag =
	  Ast.ASSIGN(flag_id,Ast.INT(0,int_attr),false,attr) in
	(need_frexp,need_frstm,
	 [Ast.IF(Ast.BINARY(Ast.EQ,new_src_exp,flag_id,
			     B.updty attr B.BOOL),
		 Ast.SEQ([],before_exp,attr),
		 Ast.SEQ([],clear_flag::ubefore_exp,attr),
		 attr)],
	 exp,
	 Ast.IF(Ast.BINARY(Ast.EQ,new_src_exp,flag_id,
			    B.updty attr B.BOOL),
		 Ast.SEQ([],[stmt],attr),
		 Ast.SEQ([],[clear_flag;ustmt],attr),
		 attr))
    | NOT_SPECIALIZED(sched_preempt_name,args) ->
	basic_translation sched_preempt_name args false in
  let mk_move srcs dst =
    let dst_state_id = state_name dst in
    Ast.MOVE(new_src_exp,Ast.VAR(dst_state_id,[],attr),None,srcs,
	     Some(dst),false,state_end,attr) in
  let classified_by_dest =
    let info =
      List.sort
	(function (_,CS.STATE(id1,cls1,_,_)) ->
	  function (_,CS.STATE(id2,cls2,_,_)) ->
	    let tmp = compare cls1 cls2 in
	    if tmp = 0 then compare id1 id2 else tmp)
	srcs_dsts in
    let rec loop = function
	[] -> []
      | (s,(CS.STATE(_,cls,_,_) as d))::rest ->
	  (match loop rest with
	    [] -> [(cls,[(d,[s])])]
	  |	((cls1,d1_ss)::rest_res) as x ->
	      if cls = cls1
	      then
		match d1_ss with
		  [] -> internal_error 1
		| (d1,ss)::cls_rest as y ->
		    if d = d1
		    then (cls,(d1,s::ss)::cls_rest)::rest_res
		    else (cls,(d,[s])::y)::rest_res
	      else (cls,[(d,[s])])::x) in
    loop info in
  let compile_one_case = function
      [] -> internal_error 2
    | [(dst,[src])] ->
	if dst = src
	then None
	else Some(mk_move [src] dst)
    | [(dst,srcs)] -> Some(mk_move srcs dst)
    | dst_srcs ->
	let (same,diff) =
	  List.partition
	    (function
		(d,[s]) -> d = s |
		(d,s) -> false)
	    dst_srcs in
	let same = List.map (function (d,_) -> d) same in
	(match (same,diff) with
	  ([],_) -> internal_error 3
	| (_,[]) -> None
	| ((l1::ls),[(dst,srcs)]) ->
	    let mkin state =
	      Ast.IN(new_src_exp,
		     Ast.VAR(B.mkId (CS.state2c state),[],attr),
		     [state],B.BOTH,false,None,B.updty attr B.BOOL) in
	    let ins =
	      List.fold_left
		(function rest ->
		  function state ->
		    Ast.BINARY(Ast.OR,rest,mkin state,B.updty attr B.BOOL))
		(mkin l1) ls in
	    Some(Ast.IF(ins,Ast.SEQ([],[],attr),mk_move srcs dst,attr))
	| _ ->
	    internal_error 4) in
  let drop_empties =
    let compiled =
      List.map
	(function (cls,dst_srcs) -> (cls,compile_one_case dst_srcs))
	classified_by_dest in
    let (same,diff) =
      List.partition (function (_,None) -> true | _ -> false) compiled in
    (same = [],
     List.map (function (x,Some(y)) -> (x,y) | _ -> internal_error 5)
       diff) in
  (match drop_empties with
    (true,[]) ->
      error(Some(B.loc2c attr),"no destinations for forwardImmediate");
      fwds
  | (false,[]) ->
      if need_frstm then need_fwdres := true;
      Ast.mkDBLOCK attr (decls,[fwds])
  | (true,[(cls,dst_srcs)]) ->
      if need_frstm then need_fwdres := true;
      Ast.mkDBLOCK attr (decls, [fwds;dst_srcs])
  | (false,[(cls,dst_srcs)]) ->
      let moves =
	Ast.IF(Ast.BINARY(Ast.EQ,fwd,
              Ast.VAR(B.mkId(CS.class2state cls),[],attr),
			  attr),
	       dst_srcs,
	       Ast.SEQ([],[],attr),
	       attr) in
      if need_frexp then need_fwdres := true;
      Ast.mkDBLOCK attr (decls,pre_fwd@[moves])
  | (true,[(cls1,dst_srcs1);(cls2,dst_srcs2)]) ->
      let moves =
	Ast.IF(Ast.BINARY(Ast.EQ,fwd,
              Ast.VAR(B.mkId(CS.class2state cls1),[],attr),
			  attr),
	       dst_srcs1,
	       dst_srcs2,
	       attr) in
      if need_frexp then need_fwdres := true;
      Ast.mkDBLOCK attr (decls,pre_fwd@[moves])
  | (_,classified_by_dest) ->
      let moves =
	Ast.SWITCH(fwd,
		   List.map
		     (function
			 (cls,dst_srcs) ->
			   Ast.SEQ_CASE([B.mkId(CS.class2state cls)],[],
					dst_srcs, attr))
		     classified_by_dest,
		   Some(Ast.SEQ([],[],attr)),
		   attr) in
      if need_frexp then need_fwdres := true;
      Ast.mkDBLOCK attr (decls,pre_fwd@[moves]))

(* --------------------- Handlers and functions --------------------- *)

let process_handlers handlers =
  List.map
    (function (Ast.EVENT(nm,param,stmt,syn,attr)) ->
      let name = Ast.event_name2c nm in
      (*      let event = Ast.mkVAR("_event",[],-1) in *)
      let tgt = Ast.mkVAR("tgt",[],-1) in
      let tgt_next_list = Ast.mkFIELD(Ast.mkVAR("tgt_next_list",[],-1),"next",[],-1)
      in
      handler_name := Printf.sprintf "%s: " name;
      in_unblock_wakeup := false;
      need_fwdres := false;
      let in_sched_preempt =
	(match name with
	  "preempt" -> NOT_SPECIALIZED ("preempt",[])
	| "compute_state" -> NOT_SPECIALIZED ("compute_state", [tgt_next_list])
	| "attach" -> NOT_SPECIALIZED ("ps_attach", [tgt_next_list])
	| "detach" -> NOT_SPECIALIZED ("ps_detach",[tgt_next_list;tgt])
	| _ -> NOT_AN_ENTRY_POINT
(*	    let find_specd =
	      let rec loop = function
		  ([],[]) -> (function args -> NOT_AN_ENTRY_POINT)
		| ((prev,last)::rest1,hname::rest2) ->
		    if List.mem nm
			(Verifier_new.handler_name_lookup
			   (!Verifier_new.phandler_environment)
			   last)
		    then
		      match prev with
			[] ->
			(function args ->
				  NOT_SPECIALIZED(Ast.event_name2c_ hname,args))
		       |	_ ->
			  (function args ->
			    SPECIALIZED_NOT_START(cur_spec_start,
						  Ast.event_name2c_ hname,
						  Ast.event_name2c_ nm,
						  args))
		    else loop (rest1, rest2)
		| _ ->
		    raise
		      (Error.Error
			 "incompatible specialized event descriptions") in
	      loop (!Events.specialized_events,
		    !Events.top_level_event_names) in
	    (if Ast.event_name2c3 nm = Some "unblock.timer.persistent"
	     then
	       let self = Ast.mkVAR("self",[],-1) in
	       let local_data = Ast.mkVAR("local_data",[],-1) in
	       let target_alive = Ast.mkVAR("target_alive",[],-1) in
	       find_specd
		 [event;self;local_data;target_alive;tgt;tgt_next_list]
	     else
	       find_specd [event;tgt;tgt_next_list]
 *)
	) in
      let body = process_stmt in_sched_preempt true stmt in
      let init =
	match in_sched_preempt with
	  SPECIALIZED_START(flag,_,_) ->
	    let flag_id = Ast.VAR(B.mkId flag,[],B.updty attr B.SCHEDULER) in
	    if find_forward stmt
	    then None
	    else Some(Ast.ASSIGN(flag_id,Ast.INT(0,B.updty attr B.INT),
				 false,attr))
	| _ -> None in
      let def =
	if !need_fwdres
	then
	  let fr = Ast.VARDECL(B.INT,B.mkId fwdres,false,false,CS.UNSHARED,None,attr) in
	  let def = Ast.VALDEF(fr,Ast.INT(0,B.updty attr B.INT),
			       Ast.VARIABLE,attr) in
	  Some def
	else None in
      let body =
	match (def,init) with
	  (Some def,Some init) -> Ast.mkDBLOCK attr ([def],[init;body])
	| (None,Some init) -> Ast.mkDBLOCK attr ([],[init;body])
	| (Some def,None) -> Ast.mkDBLOCK attr ([def],[body])
	| (None,None) -> body in
      Ast.EVENT(nm,param,body,syn,attr))
    handlers

let process_ifunctions ifunctions =
  List.map
    (function Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr) ->
      handler_name := Printf.sprintf "%s: " (B.id2c nm);
      Ast.FUNDEF(tl,ret,nm,params,
		 process_stmt NOT_AN_ENTRY_POINT false stmt,inl,
		 attr))
  ifunctions

let compile_forward
    ((Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		    fundecls,valdefs,domains,pstates,cstates,
		    criteria,trace,handlers,chandlers,ifunctions,functions,
		      attr)) as x) =
     x
