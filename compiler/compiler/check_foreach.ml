(* A foreach that is increasing or decreasing should only be applied to the
sorted state, and only when this state is SORTED, not partially sorted or
dynamically sorted *)

module B = Objects
module CS = Class_state

let error (loc,str) =
  (Error.update_error();
   match loc with
     Some lstr -> Printf.printf "%s: %s\n" lstr str
   | None -> Printf.printf "%s\n" str)

let rec do_check dir_allowed = function
    Ast.IF(tst,thn,els,a) -> do_check dir_allowed thn; do_check dir_allowed els
  | Ast.FOR_WRAPPER(label,stmts,attr) ->
      List.iter (do_check dir_allowed) stmts
  | Ast.FOR(id,state_id,_,Ast.RANDOM,stmt,crit, attr) -> do_check dir_allowed stmt
  | Ast.FOR(id,state_id,states,_,stmt,crit,attr) ->
      if dir_allowed
      then
	if List.for_all
	    (function state_info -> Some state_info = !CS.selected)
	    states
	then do_check dir_allowed stmt
	else error (Some(B.loc2c attr), "ordered for must be on sorted state")
      else
	error
	  (Some(B.loc2c attr),
	   "ordered for requires statically sorted select queue")
  | Ast.SWITCH(exp,cases,default,attr) ->
      List.iter
	(function Ast.SEQ_CASE(pat,_,stmt,attr) -> do_check dir_allowed stmt)
	cases;
      (match default with None -> () | Some stmt -> do_check dir_allowed stmt)
  | Ast.SEQ(decls,stmts,attr) -> List.iter (do_check dir_allowed) stmts
  | Ast.SAFEMOVEFWD(exp,_,stm,state_end,attr) -> do_check dir_allowed stm
  | _ -> ()

(* --------------------- Handlers and functions --------------------- *)

let process_handlers dir_allowed handlers =
  List.iter
    (function (Ast.EVENT(nm,param,stmt, _, attr)) ->
      do_check dir_allowed stmt)
    handlers

let process_ifunctions dir_allowed ifunctions =
  List.iter
    (function Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr) ->
      do_check dir_allowed stmt)
    ifunctions

let check_foreach
    ((dynamic_sort,crit_info,
     (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		    fundecls,valdefs,domains,states,cstates,
		    criteria,trace,handlers,chandlers,ifunctions,functions,
		    attr))) as x)=
  let dir_allowed = dynamic_sort = Compile_select.SORTED in
  process_handlers dir_allowed handlers;
  process_ifunctions dir_allowed ifunctions;
  x
