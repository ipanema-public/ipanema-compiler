module B = Objects

let smp_wmb attr =
  Ast.PRIMSTMT(Ast.mkVAR("smp_wmb", [], B.line attr), [], (B.dum __LOC__))

let rec compile_wrap_stmt sv stack s =
  let ret = compile_stmt sv stack s in
  match (s, ret) with
    Ast.ASSIGN _, x::[y] ->
      Ast.SEQ([], List.rev ret, (B.dum __LOC__))
  | _, [x] -> x

and compile_stmt sv stack s =
  match s with
    Ast.IF(exp,stmt1,stmt2,attr) ->
      Ast.IF(exp,
	     compile_wrap_stmt sv [] stmt1,
	     compile_wrap_stmt sv [] stmt2,
	     attr):: stack

    | Ast.FOR(id,state,x,dir,stmt,crit,attr) ->
	Ast.FOR(id,state,x,dir,compile_wrap_stmt sv [] stmt,crit,attr):: stack

    | Ast.FOR_WRAPPER(label,stmts,attr) ->
	Ast.FOR_WRAPPER(label,List.rev (List.fold_left (compile_stmt sv) [] stmts),attr):: stack

    | Ast.SWITCH(exp,cases,default,attr) ->
       Ast.SWITCH(exp,List.map
	 (function Ast.SEQ_CASE(idlist,x,stmt,y) ->
	   Ast.SEQ_CASE(idlist,x,compile_wrap_stmt sv [] stmt,y))
	 cases,Aux.app_option (compile_wrap_stmt sv []) default,attr):: stack
	 
    | Ast.SEQ(decls,stmts,attr) ->
       Ast.SEQ(decls, List.rev (List.fold_left (compile_stmt sv) [] stmts),attr):: stack

    | Ast.ASSIGN(expl,expr,sorted_fld,attr) -> (
      match expl with
        Ast.FIELD(e,_,_,vattr) ->
	  if B.ty (Ast.get_exp_attr e) = B.CORE (*
	  || B.ty (Ast.get_exp_attr e) = B.PROCESS *) then
	    smp_wmb attr::s::stack
	  else
	    s:: stack
      | _ -> s:: stack)
    | x -> x::stack

let compile_handler events =
  List.map
    (function (Ast.EVENT(nm,param,stmt,syn,attr)) ->
      Ast.EVENT(nm, param, compile_wrap_stmt [] [] stmt, syn, attr))
    events

let compile_function functions =
  List.map
    (function Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr) ->
      Ast.FUNDEF(tl,ret,nm,params, compile_wrap_stmt [] [] stmt, inl, attr))
    functions

(* -------------------------- entry point --------------------------- *)

(* FIXME: handle local/global
 * process states correctly *)
let compile
    (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		   fundecls,valdefs,domains,pcstate,cstates,criteria,
		   trace,handlers,chandlers,ifunctions,functions,attr)) =
  let newhandlers = compile_handler handlers in
  let newchandlers = compile_handler chandlers in
  let newfunctions = compile_function functions in
  Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		fundecls,valdefs,domains, pcstate,cstates,
		criteria, trace, newhandlers, newchandlers,
		ifunctions, newfunctions, attr)
