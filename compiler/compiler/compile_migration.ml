module A = Ast
module B = Objects
module CS = Class_state

let bind = ref (Hashtbl.create(500) : (B.identifier, B.identifier) Hashtbl.t)

let filter_attr_lists f self (decls, stmts) ls =
  let (ls, decls, stmts) = List.fold_left (
    fun (acc, decls, stmts) elem ->
      let (st, decls, stmts) = f self (decls, stmts) elem in
      (st::acc, decls, stmts)
  ) ([], decls, stmts) ls in
  (List.rev ls, decls, stmts)

let upd_fresh fid ty cexp =
  let fv = Ast.VAR(fid, [],B.updty (B.dum __LOC__) ty) in
  Ast.ASSIGN(cexp, fv, true, (B.dum __LOC__))

let make_fresh ty cexp =
  let fid = B.fresh_id () in
  let fv = Ast.VAR(fid, [],B.updty (B.dum __LOC__) ty) in
  let fdecl = Ast.mkVALDEF(Ast.mkVARDECL(CS.UNSHARED,ty, B.id2c fid, -1), cexp, -1) in
  let upd = Ast.ASSIGN(cexp, fv, true, (B.dum __LOC__)) in
  (fid, fdecl, upd)

let rec filter_attr_exp assign self (decls, stmts) cexp =
  match cexp with
    Ast.BOOL _ | Ast.INT _ | Ast.VAR  _ ->  (cexp, decls, stmts)
  | Ast.FIELD(exp, field, ssa, attr) ->
     begin
       match exp with
	 Ast.VAR(varid, _,varty) ->
	   if varid = self then
	     let ty = B.ty attr in
	     let (fid, decls, stmts) =
	       try
                 let fid = Hashtbl.find (!bind) field in
                 if assign
                    && not (B.strideq ("cload", field)
                            && B.ty varty = B.CORE
                            && ty = B.INT) then
                   let upd = upd_fresh fid ty cexp in
                   (fid, decls, upd::stmts)
                 else
		   (fid, decls, stmts)
	       with Not_found ->
                 let (fid, fdecl, upd) = make_fresh ty cexp in
		 Hashtbl.add (!bind) field fid;
                 if assign
                    && not (B.strideq ("cload", field)
                            && B.ty varty = B.CORE
                            && ty = B.INT) then
		   (fid, fdecl::decls, upd::stmts)
                 else
                   (fid, fdecl::decls, stmts)
	     in
	     (Ast.VAR(fid, [], attr), decls, stmts)
	   else
	     let (exp, decls, stmts) = filter_attr_exp assign self (decls, stmts) exp in
	     (Ast.FIELD(exp, field, ssa, attr), decls, stmts)
       | _ ->
	  let (exp, decls, stmts) = filter_attr_exp assign self (decls, stmts) exp in
	  (Ast.FIELD(exp, field, ssa, attr), decls, stmts)
     end

  | Ast.VALID(exp, _, attr) -> filter_attr_exp assign self (decls, stmts) exp
  | Ast.UNARY(op, exp, attr) ->
     let (exp, decls, stmts) = filter_attr_exp assign self (decls, stmts) exp in
     (Ast.UNARY(op,exp,attr), decls, stmts)

  | Ast.BINARY(bop, exp1, exp2, attr) ->
     let (exp1, decls, stmts) = filter_attr_exp assign self (decls, stmts) exp1 in
     let (exp2, decls, stmts) = filter_attr_exp assign self (decls, stmts) exp2 in
     (Ast.BINARY(bop,exp1,exp2,attr), decls, stmts)

  | Ast.TERNARY(cond,exp1,exp2,attr) ->
     let (cond, decls, stmts) = filter_attr_exp assign self (decls, stmts) cond in
     let (exp1, decls, stmts) = filter_attr_exp assign self (decls, stmts) exp1 in
     let (exp2, decls, stmts) = filter_attr_exp assign self (decls, stmts) exp2 in
     (Ast.TERNARY(cond,exp1,exp2,attr), decls, stmts)

  | Ast.AREF(exp1, None, attr) ->
     let (exp1, decls, stmts) = filter_attr_exp assign self (decls, stmts) exp1 in
     (Ast.AREF(exp1, None, attr), decls, stmts)

  | Ast.AREF(exp1, Some exp2, attr) ->
     let (exp1, decls, stmts) = filter_attr_exp assign self (decls, stmts) exp1 in
     let (exp2, decls, stmts) = filter_attr_exp assign self (decls, stmts) exp2 in
     (Ast.AREF(exp1, Some exp2, attr), decls, stmts)

  | Ast.PRIM(fn, args, attr) ->
     let (args, decls, stmts) = filter_attr_lists (filter_attr_exp assign) self (decls, stmts) args in
     (Ast.PRIM(fn, args, attr), decls, stmts)

  | _ ->
     Pp.pretty_print_exp cexp;
     failwith (__LOC__^": "^string_of_int (B.line (Ast.get_exp_attr cexp)) ^": Unsupported steal_thread expression")

let compile_decl d = d

let rec filter_attr_seq self (decls, stmts) seq =
  let Ast.SEQ_CASE(idlist,x,stmt,y) = seq in
  let (stmt, decls, stmts) = filter_attr_stmt self (decls, stmts) stmt in
  (Ast.SEQ_CASE(idlist,x,stmt,y), decls, stmts)

and filter_attr_stmt self (decls, stmts) stmt =
  match stmt with
    Ast.IF(exp,stmt1,stmt2,attr) ->
      let (exp, decls, stmts) = filter_attr_exp false self (decls, stmts) exp in
      let (stmt1, decls, stmts) = filter_attr_stmt self (decls, stmts) stmt1 in
      let (stmt2, decls, stmts) = filter_attr_stmt self (decls, stmts) stmt2 in
      (Ast.IF(exp, stmt1, stmt2, attr), decls, stmts)

    | Ast.FOR(id,None,x,dir,stmt,crit,attr) ->
       let (stmt, decls, stmts) = filter_attr_stmt self (decls, stmts) stmt in
       (Ast.FOR(id,None,x,dir, stmt,crit,attr), decls, stmts)

    | Ast.FOR(id,Some state_id,x,dir,stmt,crit,attr) ->
       let (new_state, decls, stmts) =
	 filter_attr_lists (filter_attr_exp false) self (decls, stmts) state_id in
       let (stmt, decls, stmts) = filter_attr_stmt self (decls, stmts) stmt in
       (Ast.FOR(id,Some new_state, x,dir, stmt,crit,attr), decls, stmts)

    | Ast.FOR_WRAPPER(label,stmts2,attr) ->
       let (stmts2, decls, stmts) =
	 filter_attr_lists filter_attr_stmt self (decls, stmts) stmts2 in
       (Ast.FOR_WRAPPER(label, stmts2, attr), decls, stmts)

    | Ast.SWITCH(exp,cases,default,attr) ->
       let (exp, decls, stmts) = filter_attr_exp false self (decls, stmts) exp in
       let (cases, decls, stmts) = filter_attr_lists filter_attr_seq self (decls, stmts) cases in
       let (default, decls, stmts) = match Aux.app_option (filter_attr_stmt self (decls, stmts)) default with
	   None -> (None, decls, stmts)
	 | Some (default, decls, stmts) -> (Some default, decls, stmts)
       in
       (Ast.SWITCH(exp, cases, default, attr), decls, stmts)

    | Ast.SEQ(ldecls,lstmts,attr) ->
       let (lstmts, updecls, upstmts) = filter_attr_lists filter_attr_stmt self (decls, stmts) lstmts in
       (Ast.SEQ(List.map (compile_decl) ldecls, lstmts,attr), updecls, upstmts)

    | Ast.RETURN(None,attr) ->
       (Ast.RETURN(None,attr), decls, stmts)

    | Ast.RETURN(Some(exp),attr) ->
       let (exp, decls, stmts) = filter_attr_exp false self (decls, stmts) exp in
       (Ast.RETURN(Some exp,attr), decls, stmts)

    | Ast.MOVE(exp,state,x,y,z,a,b,attr) ->
       let (exp, decls, stmts) = filter_attr_exp false self (decls, stmts) exp in
       let (state, decls, stmts) = filter_attr_exp false self (decls, stmts) state in
       (Ast.MOVE(exp, state, x,y,z,a,b,attr), decls, stmts)

    | Ast.MOVEFWD(exp,x,state_end,attr) ->
       let (exp, decls, stmts) = filter_attr_exp false self (decls, stmts) exp in
       (Ast.MOVEFWD(exp,x,state_end,attr), decls, stmts)

    | Ast.ASSIGN(expl,expr,sorted_fld,attr) ->
       let (expl, decls, stmts) = filter_attr_exp true self (decls, stmts) expl in
       let (expr, decls, stmts) = filter_attr_exp false self (decls, stmts) expr in
       (Ast.ASSIGN(expl,expr, sorted_fld,attr), decls, stmts)

    | Ast.PRIMSTMT(f,args,attr) ->
       let (args, decls, stmts) = filter_attr_lists (filter_attr_exp false) self (decls, stmts) args in
       (Ast.PRIMSTMT(f, args, attr), decls, stmts)

    | Ast.ASSERT(exp,attr) ->
       let (exp, decls, stmts) = filter_attr_exp false self (decls, stmts) exp in
       (Ast.ASSERT(exp,attr), decls, stmts)

    | x -> (x, decls, stmts)

let compile_migrstmt (self: B.identifier) stmt =
  let (stmt, decls, stmts) = filter_attr_stmt self ([], []) stmt in
  (stmt, decls, stmts)
