(* $Id$ *)

(* First, classify each case in the ordering criteria as static or dynamic.
   For each criteria, we first check whether it depends on a "previous"
   variable or on a "system" variable or field.  If so, it must be dynamic.
   If the criteria does not depend on a "previous" variable or on a
   "system" variable or field, this phase collects the variables and fields
   that are referred to.  In the second step, this information is used to
   determine whether these variables are assigned anywhere, and whether the
   fields are assigned when associated to a process in the sorted queue.
   Finally, replace each call to select by a call to
   select_sorted(sorted_queue), if there are not dynamic criteria,
   select_sort(sorted_queue), if the first criterion is dynamic, and
   select_partially_sorted(sorted_queue) if there is a non-empty prefix of
   static criteria. *)

module B = Objects
module CS = Class_state

let used_lifo_fifo = ref false

(* -------------------- Miscellaneous operations -------------------- *)

let rec id_member x = function
    [] -> false
  | (y::r) -> B.ideq(x,y) || id_member x r

let error (loc,str) =
  (Error.update_error();
   match loc with
     Some lstr -> Printf.printf "%s: %s\n" lstr str
   | None -> Printf.printf "%s\n" str)

let warning (loc,str) =
  match loc with
    Some lstr -> Printf.printf "warning: %s: %s\n" lstr str
  | None -> Printf.printf "warning: %s\n" str

(* -------------- Step 1: analyze the ordering criteria ------------- *)

let collect_previous state_decl =
  List.fold_left
    (function prev ->
      function
	  Ast.PROCESS(clsname,shared,id,Ast.BOSSA_PREV previd,vis,attr) ->
	    previd::prev
	| Ast.PROCESS(clsname,shared,id,Ast.RTS_PREV previd,vis,attr) ->
	    previd::prev
	| _ -> prev)
    [] state_decl

let collect_sys_globals valdefs =
  List.fold_left
    (function prev ->
      function
	  Ast.SYSDEF(Ast.VARDECL(_,id,_,_,_,_,_),_,_) -> id :: prev
	| _ -> prev)
    [] valdefs

let collect_sys_fields procdecls =
  List.fold_left
    (function prev ->
      function
	  (* used to also consider the type, but that doesn't seem to matter *)
	  Ast.VARDECL(_,id,true,_,_,_,_) -> id :: prev
	| _ -> prev)
    [] procdecls

exception Prevref

(* all cases included for completeness, but most make no sense... *)
let rec prevref volatile_ids = function
    Ast.VAR(id, _,attr) ->
      if id_member id volatile_ids
      then raise Prevref
      else [id]

    | Ast.FIELD(exp,fld,_,attr) -> prevref volatile_ids exp

    | Ast.UNARY(uop,exp,attr) -> prevref volatile_ids exp

    | Ast.BINARY(bop,exp1,exp2,attr) ->
	(prevref volatile_ids exp1) @ (prevref volatile_ids exp2)

    | Ast.PBINARY(bop,exp1,states1,exp2,states2,attr) ->
	(prevref volatile_ids exp1) @ (prevref volatile_ids exp2)

    | Ast.INDR(exp,attr) -> prevref volatile_ids exp
	  
    | Ast.PRIM(f,args,attr) ->
	(prevref volatile_ids f) @
	List.fold_left
	  (function prev ->
	    function arg ->
	      (prevref volatile_ids arg) @ prev)
	  [] args

    | Ast.IN(exp,id,_,_,_,_,attr) -> prevref volatile_ids exp

    | _ -> []

type prevref_res = PREVREF | NOPREVREF of Ast.key * B.identifier list

let dynamic_ordering state_decl valdefs procdecls ordering_criteria =
  let volatile_ids =
    (collect_previous state_decl @ collect_sys_globals valdefs @
     collect_sys_fields procdecls) in
  let check_key attr = function
      Ast.NOKEY -> ()
    | _ ->
	error(Some(B.loc2c attr), "key not allowed with dynamic criteria") in
  List.map
    (function
	Ast.CRIT_ID(key,order,id,fnid,attr) ->
	  if id_member id volatile_ids
	  then (check_key attr key; PREVREF)
	  else NOPREVREF(key,[id])
      | Ast.CRIT_EXPR(key, order, expr, fnid, attr) ->
	 error(Some(B.loc2c attr), "TODO: key with expr and dynamic criteria");
	 NOPREVREF(key, prevref [] expr)
    )
    ordering_criteria

(* ----------------- Step 2: analyze the assignments ---------------- *)

type criteria_class = DYNCRIT | STATCRIT | KEYCRIT

let rec sorted_modified ordering_flds_glbls = function
    Ast.IF(exp,stmt1,stmt2,attr) ->
      (sorted_modified ordering_flds_glbls stmt1 ||
      sorted_modified ordering_flds_glbls stmt2)

  | Ast.FOR(id,state_id,_,dir,stmt,crit,attr) ->
      sorted_modified ordering_flds_glbls stmt
	  
  | Ast.FOR_WRAPPER(label,stmts,attr) ->
      List.exists (sorted_modified ordering_flds_glbls) stmts
	  
  | Ast.SWITCH(exp,cases,default,attr) ->
      (List.exists
	(function Ast.SEQ_CASE(idlist,_,stmt,_) ->
	  sorted_modified ordering_flds_glbls stmt)
	cases)
    ||
       (match default with
	 None -> false
       | Some x -> sorted_modified ordering_flds_glbls x)

  | Ast.SEQ(decls,stmts,attr) ->
      List.exists (sorted_modified ordering_flds_glbls) stmts

  | Ast.ASSIGN(Ast.VAR(id,_,attr),expr,_,attr1) ->
      id_member id ordering_flds_glbls
	  
  | Ast.ASSIGN(Ast.FIELD(exp,fld,_,attr),expr,true,attr1) ->
      id_member fld ordering_flds_glbls

  | stmt -> false

(* ----------------- Step 3: rewrite calls to select ---------------- *)

let negate exp attr = Ast.UNARY(Ast.NOT,exp,attr)

let pick call lifo_fifo_call other_exp other_states attr =
  if List.exists
      (function
	  (_,_,CS.QUEUE(CS.SELECT(CS.FIFODEFAULT)),_)
	| (_,_,CS.QUEUE(CS.SELECT(CS.LIFODEFAULT)),_) ->
	    true
	| _ -> false)
      other_states
  then
    begin
      used_lifo_fifo := true;
      lifo_fifo_call other_exp attr
    end
  else call other_exp attr

let rec rew_exp calls = function
    Ast.FIELD(exp,fld,_,attr) -> Ast.FIELD(rew_exp calls exp,fld,[],attr)

    | Ast.UNARY(uop,exp,attr) -> Ast.UNARY(uop,rew_exp calls exp,attr)

    | Ast.BINARY(bop,exp1,exp2,attr) ->
	Ast.BINARY(bop,rew_exp calls exp1,rew_exp calls exp2,attr)

    | Ast.PBINARY(Ast.GT as bop,exp1,states1,exp2,states2,attr) ->
	(match (exp1,exp2) with
	  (Ast.SELECT(_,_),Ast.SELECT(_,_)) ->
	    warning(Some(B.loc2c attr), "comparison always false");
	    Ast.BOOL(false,attr)
	| (Ast.SELECT(_,_),_) ->
	    let (_,greater_call,greater_lifo_fifo_call,_,_) = calls in
	    pick greater_call greater_lifo_fifo_call exp2 states2 attr
	| (_,Ast.SELECT(_,_)) ->
	    let (_,_,_,lesser_call,lesser_lifo_fifo_call) = calls in
	    pick lesser_call lesser_lifo_fifo_call exp1 states1 attr
	| (_,_) ->
	    Ast.PBINARY(bop,rew_exp calls exp1,states1,
			rew_exp calls exp2,states2,attr))

    | Ast.PBINARY(Ast.LT as bop,exp1,states1,exp2,states2,attr) ->
	(match (exp1,exp2) with
	  (Ast.SELECT(_,_),Ast.SELECT(_,_)) ->
	    warning(Some(B.loc2c attr), "comparison always false");
	    Ast.BOOL(false,attr)
	| (Ast.SELECT(_,_),_) ->
	    let (_,_,_,lesser_call,lesser_lifo_fifo_call) = calls in
	    pick lesser_call lesser_lifo_fifo_call exp2 states2 attr
	| (_,Ast.SELECT(_,_)) ->
	    let (_,greater_call,greater_lifo_fifo_call,_,_) = calls in
	    pick greater_call greater_lifo_fifo_call exp1 states1 attr
	| (_,_) ->
	    Ast.PBINARY(bop,rew_exp calls exp1,states1,
			rew_exp calls exp2,states2,attr))

    | Ast.PBINARY(Ast.GEQ as bop,exp1,states1,exp2,states2,attr) ->
	(match (exp1,exp2) with
	  (Ast.SELECT(_,_),Ast.SELECT(_,_)) ->
	    warning(Some(B.loc2c attr), "comparison always true");
	    Ast.BOOL(true,attr)
	| (Ast.SELECT(_,_),_) ->
	    let (_,_,_,lesser_call,lesser_lifo_fifo_call) = calls in
	    negate (pick lesser_call lesser_lifo_fifo_call exp2 states2 attr)
	      attr
	| (_,Ast.SELECT(_,_)) ->
	    let (_,greater_call,greater_lifo_fifo_call,_,_) = calls in
	    negate (pick greater_call greater_lifo_fifo_call exp1 states1 attr)
	      attr
	| (_,_) ->
	    Ast.PBINARY(bop,rew_exp calls exp1,states1,
			rew_exp calls exp2,states2,attr))

    | Ast.PBINARY(Ast.LEQ as bop,exp1,states1,exp2,states2,attr) ->
	(match (exp1,exp2) with
	  (Ast.SELECT(_,_),Ast.SELECT(_,_)) ->
	    warning(Some(B.loc2c attr), "comparison always true");
	    Ast.BOOL(true,attr)
	| (Ast.SELECT(_,_),_) ->
	    let (_,greater_call,greater_lifo_fifo_call,_,_) = calls in
	    negate (pick greater_call greater_lifo_fifo_call exp2 states2 attr)
	      attr
	| (_,Ast.SELECT(_,_)) ->
	    let (_,_,_,lesser_call,lesser_lifo_fifo_call) = calls in
	    negate (pick lesser_call lesser_lifo_fifo_call exp1 states1 attr)
	      attr
	| (_,_) ->
	    Ast.PBINARY(bop,rew_exp calls exp1,states1,
			rew_exp calls exp2,states2,attr))

    | Ast.PBINARY(bop,exp1,states1,exp2,states2,attr) as x ->
	error(Some(B.loc2c attr), "unexpected operator for pbinary");
	x

    | Ast.INDR(exp,attr) -> Ast.INDR(rew_exp calls exp,attr)
	  
    | Ast.SELECT(_,attr) ->
	let (select_call,_,_,_,_) = calls in
	select_call attr

    | Ast.PRIM(f,args,attr) ->
	Ast.PRIM(rew_exp calls f,List.map (rew_exp calls) args,attr)

    | Ast.IN(exp,id,x,y,z,crit,attr) -> Ast.IN(rew_exp calls exp,id,x,y,z,crit,attr)

    | exp -> exp

let rew_decl calls = function
    Ast.VALDEF(decl,exp,is_const,attr) ->
      Ast.VALDEF(decl,rew_exp calls exp,is_const,attr)
  | decl -> decl

let rec rew_stmt calls = function
    Ast.IF(exp,stmt1,stmt2,attr) ->
      Ast.IF(rew_exp calls exp,rew_stmt calls stmt1,rew_stmt calls stmt2,attr)
	
    | Ast.FOR(id,state_id,x,dir,stmt,crit, attr) ->
	Ast.FOR(id,state_id,x,dir,rew_stmt calls stmt, crit,attr)
	  
    | Ast.SWITCH(exp,cases,default,attr) ->
	Ast.SWITCH(rew_exp calls exp,
		   List.map
		     (function Ast.SEQ_CASE(idlist,x,stmt,attr) ->
		       Ast.SEQ_CASE(idlist,x,rew_stmt calls stmt,attr))
		     cases,
		   Aux.app_option (rew_stmt calls) default,
		   attr)
	  
    | Ast.SEQ(decls,stmts,attr) ->
	Ast.SEQ(List.map (rew_decl calls) decls,
		List.map (rew_stmt calls) stmts,attr)
	  
    | Ast.RETURN(Some(exp),attr) ->
	Ast.RETURN(Some(rew_exp calls exp),attr)
	  
    | Ast.MOVE(exp,state,x,y,z,a,b,attr) ->
	Ast.MOVE(rew_exp calls exp,state,x,y,z,a,b,attr)
	       
    | Ast.MOVEFWD(exp,x,state_end,attr) ->
	Ast.MOVEFWD(rew_exp calls exp,x,state_end,attr)

    | Ast.SAFEMOVEFWD(exp,x,stm,state_end,attr) ->
	raise
	  (Error.Error
	     "compile_select: unexpected safe move forward statement\n")
	       
    | Ast.ASSIGN(expl,expr,sorted_fld,attr) ->
	Ast.ASSIGN(rew_exp calls expl,rew_exp calls expr,sorted_fld,attr)

    | Ast.PRIMSTMT(f,args,attr) ->
	Ast.PRIMSTMT(rew_exp calls f,List.map (rew_exp calls) args,attr)

    | stmt -> stmt

(* --------- classify the criteria as key, static or dynamic -------- *)

let classify_criteria states valdefs procdecls criteria handlers ifunctions
    attr =
  let do_handlers handlers ids =
    List.exists
      (function Ast.EVENT(_,_,stmt,_,_) -> sorted_modified ids stmt)
      handlers in
  let do_ifunctions ifunctions ids =
    List.exists
      (function Ast.FUNDEF(_,_,_,_,stmt,_,_) -> sorted_modified ids stmt)
    ifunctions in
  List.map
    (function
	PREVREF -> DYNCRIT
      | NOPREVREF(Ast.NOKEY, ids) ->
	  if (do_handlers handlers ids) || (do_ifunctions ifunctions ids)
	  then DYNCRIT
	  else STATCRIT
      | NOPREVREF(_, ids) ->
	  if (do_handlers handlers ids) || (do_ifunctions ifunctions ids)
	  then
	    error(Some(B.loc2c attr), "key not allowed with dynamic criteria");
	  KEYCRIT)
    (dynamic_ordering states valdefs procdecls criteria)
    
(* ------------------- information about the queue ------------------ *)

let get_queue_info key = function
    Some(CS.STATE(id,_,CS.QUEUE(CS.SELECT(default)),_)) ->
      ((function attr ->
          let mk_fname str = Ast.VAR(B.mkId str, [], B.updty attr B.VOID) in
	if key
	then Ast.PRIM(mk_fname "first_non_empty", [Ast.VAR(id, [],attr)], attr)
	else Ast.VAR(id, [],attr)),
       match default with
	 CS.NODEFAULT -> ""
       | CS.LIFODEFAULT -> "_lifo"
       | CS.FIFODEFAULT -> "_fifo")
  | Some(CS.CSTATE(id, _))
  | Some(CS.STATE(id,_, _,_)) ->
     let nm = B.id2c id in
     raise (Error.Error (__LOC__ ^"internal error with "^nm))
  | None -> raise (Error.Error (__LOC__^"internal error"))

(* -------------------------- entry point --------------------------- *)
    
type sort_result = SORTED | UNSORTED | PARTIALLY_SORTED

(* FIXME: handle distinction between local and global core
 * correctly *)
let compile_select
    (Ast.SCHEDULER(nm,
		   cstdefs,enums,dom,grp,procdecls,
		   fundecls,valdefs,domains,pcstate,cstates,criteria,
		   trace,handlers,chandlers,ifunctions,functions,attr)) =
  let classified_criteria = (
    match pcstate with 
      Ast.CORE(cv, states,_) ->
      classify_criteria states valdefs procdecls criteria handlers
			ifunctions attr
    | Ast.PSTATE(states) ->
       classify_criteria states valdefs procdecls criteria handlers ifunctions attr)
  in
  if classified_criteria <> []
  then
    let key  = Aux.member KEYCRIT  classified_criteria
    and stat = Aux.member STATCRIT classified_criteria
    and dyn  = Aux.member DYNCRIT  classified_criteria in
    let (queue_name,default) = get_queue_info key (!CS.selected) in
    let (sort_needed,select_name,greater_name,lesser_name) =
      if criteria = []
      then
	(* nothing to test, so pretend it's unsorted for inserts, and sorted
	 for removal *)
	(UNSORTED,Printf.sprintf "select%s_sorted" default,"","")
      else
	if dyn
	then
	  if stat
	  then (* both static and dynamic *)
	    (PARTIALLY_SORTED,
	     Printf.sprintf "select%s_partially_sorted" default,
	     "select_greater_partially_sorted",
	     "select_lesser_partially_sorted")
	  else (* all dynamic *)
	    (UNSORTED,Printf.sprintf "select%s_sort" default, "select_greater",
	     "")
	else (* all static *)
	  (SORTED,Printf.sprintf "select%s_sorted" default,"","") in
    let (greater_lifo_fifo_name,lesser_lifo_fifo_name) =
      (Printf.sprintf "%s_lifo_fifo" greater_name,
       Printf.sprintf "%s_lifo_fifo" lesser_name) in
    let mk_fname str = Ast.VAR(B.mkId str, [], B.updty attr B.VOID) in
    let args = [queue_name attr] in
    let args =
      if (!B.hls)
      then
	let sched_inst_type = B.SCHEDULER_INST in
	(Ast.VAR(B.mkId "scheduler_instance", [],B.updty attr sched_inst_type))
	:: args
      else args in
    let select_call =
      function attr -> Ast.PRIM(mk_fname select_name,args,
				B.updty attr
					(B.PROCESS )) in
    let greater_call =
      function p ->
	       function attr ->
			if sort_needed = SORTED
			then Ast.BINARY(Ast.GT,select_call attr,p,attr)
			else Ast.PRIM(mk_fname greater_name,[p;queue_name attr],attr) in
    let greater_lifo_fifo_call =
      function p ->
	       function attr ->
			if sort_needed = SORTED
			then Ast.BINARY(Ast.GT,select_call attr,p,attr)
			else Ast.PRIM(mk_fname greater_lifo_fifo_name,[p;queue_name attr],
				      attr) in
    let lesser_call =
      function p ->
	       function attr ->
			if sort_needed = PARTIALLY_SORTED
			then Ast.PRIM(mk_fname lesser_name,[p;queue_name attr],attr)
			else Ast.BINARY(Ast.GT,p,select_call attr,attr) in
    let lesser_lifo_fifo_call =
      function p ->
	       function attr ->
			if sort_needed = PARTIALLY_SORTED
			then Ast.PRIM(mk_fname lesser_lifo_fifo_name,[p;queue_name attr],attr)
			else Ast.BINARY(Ast.GT,p,select_call attr,attr) in
    let calls =
      (select_call, greater_call, greater_lifo_fifo_call,
       lesser_call, lesser_lifo_fifo_call) in
    let handlers =
      List.map
	(function Ast.EVENT(nm,param,stmt,syn,attr) ->
		  Ast.EVENT(nm,param,rew_stmt calls stmt,syn,attr))
	handlers in
    let ifunctions =
      List.map
	(function Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr) ->
		  Ast.FUNDEF(tl,ret,nm,params,rew_stmt calls stmt,inl,attr))
	ifunctions in
    (sort_needed,classified_criteria,
     Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		   fundecls,valdefs,domains,pcstate,cstates,criteria,
		   trace,handlers,chandlers,ifunctions,functions,attr))
  else
    (UNSORTED,[],
     Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		   fundecls,valdefs,domains,pcstate,cstates,criteria,
		   trace,handlers,chandlers,ifunctions,functions,attr))
