module A = Ast
module B = Objects
module G = Genaux

let opt_apply f a =
  match a with
    None -> None
  | Some x -> Some (f x)

let rec compile_stmt = function
    A.IF(tst,thn,els,attr) ->
     A.IF(compile_exp None tst, compile_stmt thn, compile_stmt els,attr)
  | A.FOR(id,Some exprs,si,dir,stmt,crit,attr) ->
     A.FOR(id, Some (List.map (function e -> compile_exp None e) exprs),si,dir,
           compile_stmt stmt,crit,attr)
  | A.FOR(id,None,si,dir,stmt,crit,attr) ->
     A.FOR(id,None,si,dir,compile_stmt stmt,crit,attr)
  | A.FOR_WRAPPER(id,stmts,attr) ->
     A.FOR_WRAPPER(id,List.map (function s -> compile_stmt s) stmts,attr)
  | A.SWITCH(tst,cases,Some stmt,attr) ->
     A.SWITCH(compile_exp None tst,
              List.map (function A.SEQ_CASE(id,si,stmt,attr) ->
                                 A.SEQ_CASE(id,si,compile_stmt stmt,attr)) cases,
              Some (compile_stmt stmt), attr)
  | A.SWITCH(tst,cases,None,attr) ->
     A.SWITCH(compile_exp None tst,
              List.map (function A.SEQ_CASE(id,si,stmt,attr) ->
                                 A.SEQ_CASE(id,si,compile_stmt stmt,attr)) cases,
              None, attr)
  | A.SEQ(vdefs,stmts,attr) -> A.SEQ(List.map compile_decl vdefs,
                                     List.map compile_stmt stmts, attr)
  | A.RETURN(Some exp,attr) -> A.RETURN(Some (compile_exp None exp), attr)
  | A.RETURN(None,attr) -> A.RETURN(None, attr)
  | A.MOVE(e1,e2,si1,si2,si3,b,se,attr) ->
     A.MOVE(compile_exp None e1, compile_exp None e2, si1,si2,si3,b,se,attr)
  | A.MOVEFWD(exp,si,se,attr) -> A.MOVEFWD(compile_exp None exp, si,se,attr)
  | A.SAFEMOVEFWD(exp,si,stmt,se,attr) ->
     A.SAFEMOVEFWD(compile_exp None exp,si,compile_stmt stmt,se,attr)
  | A.ASSIGN(e1,e2,b,attr) -> A.ASSIGN(compile_exp None e1, compile_exp None e2,b,attr)
  | A.PRIMSTMT(e,args,attr) ->
     (match e with
       Ast.VAR(B.ID(fn, _),_,_) when fn = "steal_for_dom" ->
	 A.PRIMSTMT(compile_exp None e, Ast.mkVAR("policy", [], -1)::(List.map (compile_exp None) args),attr)
     | _ -> A.PRIMSTMT(compile_exp None e, List.map (compile_exp None) args,attr)
     )
  | A.ASSERT(e,attr) -> A.ASSERT(compile_exp None e, attr)
  | s -> s

and compile_exp rewrite = function
    A.CAST(ty,exp,attr) -> A.CAST(ty,compile_exp rewrite exp,attr)
  | A.LCORE(str,exp,core,attr) -> A.LCORE(str,compile_exp rewrite exp, opt_apply (compile_exp rewrite) core, attr)
  | A.UNARY(op,exp,attr) -> A.UNARY(op,compile_exp rewrite exp,attr)
  | A.BINARY(op,e1,e2,attr) -> A.BINARY(op,compile_exp rewrite e1,compile_exp rewrite e2,attr)
  | A.PBINARY(op,e1,pi1,e2,pi2,attr) ->
     A.PBINARY(op,compile_exp rewrite e1,pi1,compile_exp rewrite e2,pi2,attr)
  | A.INDR(e,attr) -> A.INDR(compile_exp rewrite e,attr)
  | A.FIRST(e,c,si,a) -> A.FIRST(compile_exp rewrite e,c,si,a)
  | A.PFIRST(e,c,si,a) -> A.PFIRST(compile_exp rewrite e,c,si,a)
  | A.CFIRST(e,c,si,a) -> A.CFIRST(compile_exp rewrite e,c,si,a)
  | A.VALID(e,si,a) -> A.VALID(compile_exp rewrite e,si,a)
  | A.PRIM(fn,args,a) ->
     (match fn with
       A.COUNT(a) -> compile_count rewrite a args
     | A.FNOR(a) -> compile_or a args
     (*   | A.SUM(a) -> compile_sum a args *)
     | _ -> A.PRIM(fn,List.map (compile_exp rewrite) args,a))
  | A.SCHEDCHILD(e,vp,a) -> A.SCHEDCHILD(compile_exp rewrite e,vp,a)
  | A.AREF(e1,Some e2,a) -> A.AREF(compile_exp rewrite e1,Some (compile_exp rewrite e2),a)
  | A.AREF(e1,None,a) -> A.AREF(compile_exp rewrite e1,None,a)
  | A.IN(e1,e2,si,t,b,c,a) -> A.IN(compile_exp rewrite e1,compile_exp rewrite e2,si,t,b,c,a)
  | A.MOVEFWDEXP(e,se,a) -> A.MOVEFWDEXP(compile_exp rewrite e,se,a)
  | A.VAR(id, ssa, a) as e ->
     (match rewrite with
       None -> e
     | Some coreset ->
        if B.idnameq (coreset, id) then
          A.VAR(id, ssa, B.updty a (B.INDR (B.SET B.CORE)))
        else
          e
     )
  | A.FIELD(exp,id,l,attr) -> A.FIELD(compile_exp rewrite exp,id,l,attr)
  | e -> e

and compile_decl = function
    A.VALDEF(v,exp,c,a) -> A.VALDEF(v,compile_exp None exp,c,a)
  | d -> d
(*
and compile_sum a = function
x::[] -> (
  let ty = (
    match B.ty (A.get_exp_attr x) with
      B.SET B.CORE -> (B.CORE, "cpumask_weight")
    | B.QUEUE B.CORE -> (B.CORE, "cpumask_weight")
    | B.QUEUE B.PROCESS -> (B.PROCESS, "nr_tasks")
    | B.SET B.PROCESS -> (B.PROCESS, "nr_tasks")
    | t -> raise (Failure ("count() primitive accepts set<core> or set<process> ("^(B.type2c t)^" given)")))
  in
  let intty = B.updty a B.INT in
  begin
    match ty with
      B.CORE -> A.PRIM(A.VAR(B.mkId count_fn, [], intty),
		       [compile_exp x], intty)
    | B.PROCESS -> A.FIELD(A.updty (B.OPAQUE "ipanema_rq") (compile_exp x),
			   B.mkId count_fn, [], intty)
  end)
  | x::r -> A.BINARY(A.PLUS,compile_sum a [x],compile_sum a r,B.updty a B.INT)
  | [] -> raise (Failure "Compile_prim.compile_count: empty argument list")
*)
and compile_count rewrite a = function
x::[] -> (
  let (ty, count_fn) = (
    match B.ty (A.get_exp_attr x) with
      B.SET B.CORE -> (B.CORE, "cpumask_weight")
    | B.QUEUE B.CORE -> (B.CORE, "cpumask_weight")
    | B.QUEUE B.PROCESS -> (B.PROCESS, "nr_tasks")
    | B.SET B.PROCESS -> (B.PROCESS, "nr_tasks")
    | B.SET B.GROUP -> (B.GROUP, "bitmap_weight")
    | t -> raise (Failure ("count() primitive accepts set<core> or set<process> ("^(B.type2c t)^" given)")))
  in
  let intty = B.updty a B.INT in
  begin
    match ty with
      B.CORE ->
       let exp = compile_exp rewrite x in
       let ea = Ast.get_exp_attr exp in
       let ty = B.ty ea in
       let exp = match ty with
           B.SET B.CORE -> Ast.AREF(exp, None, B.updty ea (B.INDR ty))
         | _ -> exp
       in
       A.PRIM(A.VAR(B.mkId count_fn, [], intty),
		       [exp], intty)
    | B.GROUP -> A.PRIM(A.VAR(B.mkId count_fn, [], intty),
		        [compile_exp None x;
                         Ast.mkFIELD(Ast.mkVAR("sd", [], -1),
                                     "___sched_group_idx", [], -1)], intty)
      | B.PROCESS -> A.FIELD(A.updty (B.OPAQUE "ipanema_rq") (compile_exp None x),
			   B.mkId count_fn, [], intty)
  end)
  | x::r -> A.BINARY(A.PLUS,
                     compile_count rewrite a [x],
                     compile_count rewrite a r,
                     B.updty a B.INT)
  | [] -> raise (Failure "Compile_prim.compile_count: empty argument list")

and compile_or a [set;fld] =
  let or_fn = (
      match B.ty (A.get_exp_attr set) with
        B.SET B.CORE -> A.VAR(B.mkId "or_cores",[],B.updty a B.BOOL)
      | B.QUEUE B.CORE -> A.VAR(B.mkId "or_cores",[],B.updty a B.BOOL)
      | t -> raise
               (Failure ("or() primitive accepts set<core> ("^(B.type2c t)^" given)")))
  in
  let offset_fn = A.VAR(B.mkId "offsetof",[],B.updty a B.INT) in
  A.PRIM(or_fn,[compile_exp None set;
                A.PRIM(offset_fn,
                       [A.TYPE(B.CORE, B.updty a B.CORE);
                        A.VAR(A.get_exp_id fld,[],A.get_exp_attr fld)],
                       B.updty a B.INT)],
         B.updty a B.BOOL)

let compile_handler = function
    A.EVENT(nm, v, stmt, syn, attr) -> A.EVENT(nm, v, compile_stmt stmt, syn, attr)

let compile_function = function
    A.FUNDEF(tl, ty, nm, pl, stmt, inl, attr) ->
    A.FUNDEF(tl, ty, nm, pl, compile_stmt stmt, inl, attr)

(* -------------------------- steal block --------------------------- *)
let get_coreset ((thief, vfilter, sfilter), (vselect, e, sselect), migrcond) =
  let Ast.VARDECL(B.INDR (B.SET B.CORE), coresetid, _,_,_,_,_) = vselect in
  coresetid

let compile_migrcond migrcond =
  let (busiest, thief, group, task, stmt, stop, update) = migrcond in
  let new_stmt = compile_stmt stmt
  and new_stop = compile_exp None stop in
  (busiest, thief, group, task, new_stmt, new_stop, update)

let compile_steal_blk ((thief, vfilter, sfilter), (vselect, e, sselect), migrcond) =
  let new_filter = match sfilter with
      Ast.RETURN(Some exp, a) -> Ast.RETURN(Some (compile_exp None exp), a)
    | _ -> failwith (__LOC__ ^": Unexpected filter statement !") in
  let new_select = match sselect with
      Ast.RETURN(Some exp, a) -> Ast.RETURN(Some (compile_exp None exp), a)
    | _ -> failwith (__LOC__ ^": Unexpected select statement !") in
  let new_migrcond = compile_migrcond  migrcond in
  ((thief, vfilter,new_filter), (vselect, e, new_select), new_migrcond)

let compile_steal_group ((thief, vfilter, sfilter), (vs1, vs2, e, sselect), until) =
  let Ast.VARDECL(B.GROUP, thiefid, _,_,_,_,_) = thief in
  let new_filter = match sfilter with
      Ast.RETURN(Some exp, a) -> Ast.RETURN(Some (compile_exp None exp), a)
    | _ -> failwith (__LOC__ ^": Unexpected filter statement !") in
  let new_select = match sselect with
      Ast.RETURN(Some exp, a) -> Ast.RETURN(Some (compile_exp None exp), a)
    | _ -> failwith (__LOC__ ^": Unexpected select statement !") in
  (* FIXME: None -> vs1 / vs2 ? *)
  ((thief, vfilter,new_filter), (vs1, vs2, e, new_select), compile_exp None until)

let compile_steal_thread steal =
  match steal with
    Ast.FLAT_STEAL_THREAD st ->
      Ast.FLAT_STEAL_THREAD (compile_steal_blk st)
  | Ast.ITER_STEAL_THREAD (st, until, post) ->
     Ast.ITER_STEAL_THREAD (compile_steal_blk st,
			    compile_exp (Some (get_coreset st)) until,
			    List.map compile_stmt post)

let compile_steal steal =
  match steal with
    Ast.STEAL_THREAD s -> Ast.STEAL_THREAD (compile_steal_thread s)
  | Ast.ITER_STEAL_GROUP (sg, st, post, postgrp) ->
     Ast.ITER_STEAL_GROUP (compile_steal_group sg,
			   compile_steal_thread st,
			    List.map compile_stmt post,
			    List.map compile_stmt postgrp)
  | Ast.FLAT_STEAL_GROUP (sg, st, postgrp) ->
     Ast.FLAT_STEAL_GROUP (compile_steal_group sg,
			   compile_steal_thread st,
			    List.map compile_stmt postgrp)

let compile_steal_param (dom, dstg, dst, steal) =
  (dom, dstg, dst, compile_steal steal)

let compile_state pcstate =
    match pcstate with
      Ast.CORE(cvd,states, steal) ->
	Ast.CORE(cvd, states, opt_apply compile_steal_param steal)
    | Ast.PSTATE(states) -> pcstate

(* -------------------------- entry point --------------------------- *)
let compile_prim (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		                fundecls,valdefs,domains,states,cstates,criteria,
		                trace,handlers,chandlers,ifunctions,functions,attr)) =
  let states = compile_state states in
  let handlers = List.map compile_handler handlers in
  let chandlers = List.map compile_handler chandlers in
  let functions = List.map compile_function functions in
  let ifunctions = List.map compile_function ifunctions in
  Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		fundecls,valdefs,domains,states,cstates,
		criteria,trace,handlers,chandlers,ifunctions,
		functions,attr)
