val make_cstate_cst : Objects.identifier -> Objects.identifier
val make_state_cst : Objects.identifier -> Objects.identifier
val make_class_cst : Objects.identifier -> Objects.identifier

val compile_move :
  Compile_select.sort_result *
    Compile_select.criteria_class list * Ast.scheduler ->
      Compile_select.criteria_class list * Ast.scheduler
