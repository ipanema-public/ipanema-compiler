module A = Ast
module B = Objects
module T = Type

(* --------------------------- Expressions -------------------------- *)

let rec rename_exp env = function
      Ast.VAR(id,ssa,attr) -> Ast.VAR(id, ssa,attr)

  | Ast.FIELD(exp,fld,ssa,attr) ->
     if (B.ty attr = B.PROCESS
	&& B.ty (Ast.get_exp_attr exp) = B.PEVENT)
       || (B.ty attr = B.CORE
	 && B.ty (Ast.get_exp_attr exp) = B.CEVENT)
     then
       Ast.VAR(B.mkId "tgt", [], attr)
     else
       Ast.FIELD(rename_exp env exp, fld, ssa,attr)

  | Ast.UNARY(uop,exp,attr) -> Ast.UNARY(uop,rename_exp env exp,attr)

  | Ast.BINARY(bop,exp1,exp2,attr) ->
     Ast.BINARY(bop,rename_exp env exp1,rename_exp env exp2,attr)
       
  | Ast.TERNARY(exp1,exp2,exp3,attr) ->
     Ast.TERNARY(rename_exp env exp1,rename_exp env exp2, rename_exp env exp3,attr)
       
    | Ast.PBINARY(bop,exp1,states1,exp2,states2,attr) ->
       Ast.PBINARY(bop,rename_exp env exp1,states1,rename_exp env exp2,states2,attr)

    | Ast.INDR(exp,attr) -> Ast.INDR(rename_exp env exp,attr)

    | Ast.PRIM(f,exps,attr) ->
	Ast.PRIM(rename_exp env f,List.map (rename_exp env) exps,attr)

    | Ast.IN(exp,locexp,x,y,z,crit,attr) ->
	Ast.IN(rename_exp env exp,
	       rename_exp env locexp,x,y,z,crit,attr)

    | Ast.VALID(exp, o, attr) -> Ast.VALID(rename_exp env exp, o, attr)

    | Ast.AREF(exp, None, attr) -> Ast.AREF(rename_exp env exp, None, attr)
    | Ast.AREF(exp, Some exp2, attr) -> Ast.AREF(rename_exp env exp, Some( rename_exp env exp2), attr)

    | Ast.LCORE("state_info", exp, exp2_, attr) ->
       Ast.LCORE("state_info",
		 rename_exp env exp,
		 Aux.option_apply (rename_exp env) exp2_, attr)
	
    | exp -> exp

(* -------------------------- Declarations -------------------------- *)

(* A global is only renamed if its name is reserved and if the type is not a system type *)
let rename_decl env = function
  Ast.VARDECL(ty,id,imported,laazy,shared,dexp,attr) ->
  let dexp1 = match dexp with
      None -> None
    | Some de_ -> Some (rename_exp env de_)
  in
  Ast.VARDECL(ty, id,imported,laazy,shared, dexp1,attr)

(* A local variable is renamed if it is of type process or if its name is reserved *)
let rename_decls decls env =
  List.fold_left
    (function (env,prev_decls) ->
      function
	  Ast.VALDEF(Ast.VARDECL(B.PROCESS,id,imported,laazy,shared,dexp,attr),exp,cst,attr1) ->
	    (env,
	     Ast.VALDEF(Ast.VARDECL(B.PROCESS,id,imported,laazy,shared,dexp,attr),
			rename_exp env exp,cst,attr1) :: prev_decls)
	| Ast.VALDEF(vdecl,exp,cst,attr1) ->
	    (env, Ast.VALDEF(rename_decl env vdecl, rename_exp env exp,cst,attr1) :: prev_decls)
	| Ast.UNINITDEF(Ast.VARDECL(B.PROCESS,id,imported,laazy,shared,dexp,attr),attr1) ->
	    (env,
	     Ast.UNINITDEF(Ast.VARDECL(B.PROCESS,id,imported,laazy,shared,dexp,attr),
			   attr1) :: prev_decls)
	| Ast.UNINITDEF(Ast.VARDECL(ty,id,imported,laazy,shared,dexp,attr),attr1) ->
	    (env,
	     Ast.UNINITDEF(Ast.VARDECL(ty, id,imported,laazy,shared,
				       Aux.option_apply (rename_exp env) dexp,attr),
			   attr1) :: prev_decls)
	| Ast.SYSDEF(Ast.VARDECL(ty,id,imported,laazy,shared,dexp,attr),isconst,attr1) as x ->
	   (env,Ast.SYSDEF(Ast.VARDECL(ty,id,imported,laazy,shared,
				       Aux.option_apply (rename_exp env) dexp,attr),isconst,
			      attr1)
	       :: prev_decls)
	| Ast.DUMMYDEF(id,exp,attr) -> (* never of type process *)
	   (env,Ast.DUMMYDEF(id,rename_exp env exp,attr) :: prev_decls)
    )
    (env,[])
    decls

(* --------------------------- Statements --------------------------- *)

let rec rename_stmt env = function
    Ast.IF(exp,stm1,stm2,attr) ->
      Ast.IF(rename_exp env exp,
	     rename_stmt env stm1,rename_stmt env stm2,attr)

  | Ast.FOR(id,stid,l,dir,stm,crit,attr) ->
      Ast.FOR(id,stid,l,dir,
	      rename_stmt env stm,
	      crit,attr)

  | Ast.FOR_WRAPPER(label,stms,attr) -> raise (Error.Error "not generated yet")

  | Ast.SWITCH(exp,cases,default,attr) ->
      Ast.SWITCH(rename_exp env exp,
		 List.map
		   (function Ast.SEQ_CASE(pat,l,stm,attr) ->
		     Ast.SEQ_CASE(pat,l,rename_stmt env stm,attr))
		   cases,
		 Aux.app_option (rename_stmt env) default,
		 attr)

  | Ast.SEQ(decls,stmts,attr) ->
     let (env, decls) = rename_decls decls env in
      Ast.SEQ(List.rev decls,List.map (rename_stmt env) stmts,attr)

  | Ast.RETURN(Some(e),attr) -> Ast.RETURN(Some(rename_exp env e),attr)

  | Ast.MOVE(exp,dst_exp,src,srcs,dst,auto_allowed,state_end,attr) ->
      let id = rename_exp env dst_exp in
      Ast.MOVE(rename_exp env exp,id,src,srcs,dst,auto_allowed,state_end,attr)

  | Ast.MOVEFWD(exp,srcs_dsts,state_end,attr) ->
      Ast.MOVEFWD(rename_exp env exp,srcs_dsts,state_end,attr)

  | Ast.SAFEMOVEFWD(exp,srcs_dsts,stm,state_end,attr) ->
      Ast.SAFEMOVEFWD(rename_exp env exp,srcs_dsts,rename_stmt env stm,
		      state_end,attr)

  | Ast.ASSIGN(exp1,exp2,sorted_fld,attr) ->
      Ast.ASSIGN(rename_exp env exp1,rename_exp env exp2,sorted_fld,attr)

  | Ast.PRIMSTMT(f,args,attr) ->
      Ast.PRIMSTMT(rename_exp env f,List.map (rename_exp env) args,attr)

  | Ast.STEAL (exp, attr) -> Ast.STEAL (rename_exp env exp, attr)

  | stmt -> stmt

let rename s = rename_stmt [] s
