module A = Ast
module B = Objects
module T = Type
module CS = Class_state
  
(* ----------------------- Environment lookup ----------------------- *)

exception LookupErrException

let rec lookup_id x = function
    [] -> raise LookupErrException
  | ((y,v)::r) -> if B.ideq(x,y) then v else lookup_id x r

let is_state id =
  try let _ = lookup_id id (!CS.state_env) in true
  with LookupErrException -> false

(* -------------------------- expression and stmt --------------------------- *)

let rec compile_exp (cv: B.identifier list) e =
  match e with
    Ast.VAR(id, ssa, attr) as exp ->
      if List.mem id cv then
	  Ast.LCORE(exp, attr)
      else exp
  | Ast.FIELD(exp, id, ssa, attr)
      when B.ty (Ast.get_exp_attr exp) = B.CORE
      && is_state id ->
     Ast.PRIM(Ast.VAR(B.mkId "per_cpu", [], attr),
                          [Ast.VAR(id, [], attr);
			   Ast.FIELD(compile_exp cv exp, B.mkId "id", [], B.updty attr B.INT)],
              attr)
       
  | Ast.FIELD(exp, id, ssa, attr) ->
     Ast.FIELD(compile_exp cv exp, id, ssa, attr)
  | Ast.UNARY(uop,exp,attr) ->
     Ast.UNARY(uop,compile_exp cv exp,attr)

  | Ast.BINARY(bop,exp1,exp2,attr) ->
      let exp1 = compile_exp cv exp1
      and exp2 = compile_exp cv exp2 in
      Ast.BINARY(bop,exp1,exp2,attr)

  | Ast.INDR(exp,attr) -> Ast.INDR(compile_exp cv exp,attr)

  | Ast.VALID(exp, s, a) -> Ast.VALID(compile_exp cv exp, s, a)

  | Ast.PRIM(fn,pl,a) -> Ast.PRIM(fn, List.map (compile_exp cv) pl ,a)

  | Ast.IN(exp1, exp2, s, t, b, c, a) -> (
     match B.ty (Ast.get_exp_attr exp1) with
       B.CORE -> Ast.PRIM(Ast.VAR(B.mkId "cpumask_test_cpu", [], a),
                          [compile_exp cv (Ast.FIELD(exp1, B.mkId "id", [], a));
                           Ast.AREF(compile_exp cv exp2, None, a)],
                          a)
     | _ -> Ast.IN(compile_exp cv exp1, compile_exp cv exp2, s, t, b, c, a)
  )

  | Ast.FIRST(exp, crit, stopt, attr)
      when B.ty attr = B.CORE && crit = None && stopt = None ->
     let lineattr = B.mkAttr (B.line attr) in
     Ast.AREF(Ast.PRIM(Ast.VAR(B.mkId("per_cpu"), [], lineattr),
		       [Ast.VAR(B.mkId("core"),[],lineattr);
			Ast.PRIM(Ast.VAR(B.mkId("cpumask_first"), [], B.updty attr B.INT),
				 [Ast.AREF(compile_exp cv exp, None, attr)], attr)], attr),
	      None, lineattr)

  | Ast.FIRST(exp, crit, stopt, attr) -> Ast.FIRST(compile_exp cv exp, crit, stopt, attr)

  | exp -> exp

let compile_decl cv d =
  match d with
    Ast.VALDEF(vd, exp, isc, attr) -> Ast.VALDEF(vd, compile_exp cv exp, isc, attr)
  | decl -> decl

let rec compile_stmt cv s =
  match s with
    Ast.IF(exp,stmt1,stmt2,attr) ->
      Ast.IF(compile_exp cv exp,compile_stmt cv stmt1,compile_stmt cv stmt2,attr)

    | Ast.FOR(id,None,x,dir,stmt,crit,attr) ->
	Ast.FOR(id,None,x,dir,compile_stmt cv stmt,crit,attr)

    | Ast.FOR(id,Some state_id,x,dir,stmt,crit,attr) ->
	Ast.FOR(id,Some (List.map (compile_exp cv) state_id),x,dir,compile_stmt cv stmt,crit,attr)

    | Ast.FOR_WRAPPER(label,stmts,attr) ->
	Ast.FOR_WRAPPER(label,List.map (compile_stmt cv) stmts,attr)

    | Ast.SWITCH(exp,cases,default,attr) ->
       Ast.SWITCH(compile_exp cv exp,List.map
	 (function Ast.SEQ_CASE(idlist,x,stmt,y) ->
	   Ast.SEQ_CASE(idlist,x,compile_stmt cv stmt,y))
	 cases,Aux.app_option (compile_stmt cv) default,attr)
    | Ast.SEQ(decls,stmts,attr) ->
       Ast.SEQ(List.map (compile_decl cv) decls,List.map (compile_stmt cv) stmts,attr)

    | Ast.RETURN(None,attr) -> Ast.RETURN(None,attr)

    | Ast.RETURN(Some(exp),attr) -> Ast.RETURN(Some(compile_exp cv exp),attr)

    | Ast.MOVE(exp,state,x,y,z,a,b,attr) ->
	Ast.MOVE(compile_exp cv exp,compile_exp cv state,x,y,z,a,b,attr)

    | Ast.MOVEFWD(exp,x,state_end,attr) ->
	Ast.MOVEFWD(compile_exp cv exp,x,state_end,attr)

    | Ast.ASSIGN(expl,expr,sorted_fld,attr) -> (
      match expl with
        Ast.VAR(_,_,vattr)
      | Ast.FIELD(_,_,_,vattr) -> (match B.ty vattr with
                                     SET(CORE) ->
                                     Ast.PRIMSTMT(A.VAR(B.mkId("cpumask_copy"),[],attr),
                                                  [Ast.AREF(expl, None, attr);
                                                   Ast.AREF(expr, None, attr)],
                                                  attr)
                                   | _ -> Ast.ASSIGN(compile_exp cv expl,compile_exp cv expr,
                                                     sorted_fld,attr)
                                  )
      | _ -> Ast.ASSIGN(compile_exp cv expl,compile_exp cv expr,sorted_fld,attr))

    | Ast.PRIMSTMT(f,args,attr) ->
	Ast.PRIMSTMT(f,List.map (compile_exp cv) args,attr)

    | Ast.ASSERT(exp,attr) -> Ast.ASSERT(compile_exp cv exp,attr)

    | x -> x

         (*
let compile_attach sc (A.SEQ(lv, stmts, attr)) =
  let A.VALDEF(A.VARDECL(_,defid,_,_,_,_),_,_,defattr) = List.hd lv in
  A.mkSEQ(lv,
          (List.map (fun (A.VARDECL(_,id,_,_,_,attr)) ->
               A.IF(A.PRIM(A.UNARY(A.NOT,
                                   A.VAR(B.mkId("zalloc_cpumask_var"),[],attr),
                                   attr),
                           [A.AREF(A.FIELD(A.VAR(defid,[],defattr),
                                           id,
                                           [],
                                           attr),
                                   None,
                                   attr);
                            A.VAR(B.mkId("GFP_KERNEL"),[],attr)],
                           attr),
                    A.RETURN(Some (A.mkBOOL(false,B.line attr)), attr),
                    A.mkSEQ([],[],-1),
                    attr))
                    sc)@stmts,
          B.line attr)
          *)
         
let compile_detach sc (A.SEQ(lv, stmts, attr)) =
  (* let A.VALDEF(A.VARDECL(_,defid,_,_,_,_),_,_,defattr) = List.hd lv in *)
  let stmts =
    match stmts with
      [] -> stmts
    | hd::[] -> (match hd with
                   A.RETURN(_,_) -> []
                 | _ -> [hd])
    | hd::r -> (match List.hd (List.rev r) with
                  A.RETURN(_,_) -> hd::(List.tl (List.rev r))
                | _ -> hd::r)
  in
  A.mkSEQ(lv,
          stmts
          (*@(List.map (fun (A.VARDECL(_,id,_,_,_,attr)) ->
                     (A.PRIMSTMT(A.VAR(B.mkId("free_cpumask_var"),[],attr),
                                 [A.FIELD(A.VAR(B.mkId("tgt"),[],(* def *)attr),
                                          id,
                                          [],
                                          attr)],
                                 attr)))
                          (List.rev sc))*),
          B.line attr)


(* -------------------------- top-level blocks --------------------------- *)
(* cv: core variables *)
(* sc: sets of core (used to add alloc and free on cpumask_var_t) *)
let compile_handler cv sc (A.EVENT(nm, v, stmt, attr)) =
  let new_stmt =
    let A.EVENT_NAME(eid,eattr) = nm in
    match B.strideq("detach",eid) with
      true -> compile_detach sc stmt
    | false -> stmt
  in
  A.EVENT(nm, v, compile_stmt cv new_stmt, attr)

let compile_function cv sc (A.FUNDEF(tl, ty, nm, pl, stmt, inl, attr)) =
  let new_stmt = stmt
(*    match B.strideq("attach",nm) with
      true -> compile_attach sc stmt
    | false -> stmt
 *)
  in
  A.FUNDEF(tl, ty, nm, pl, compile_stmt cv new_stmt, inl, attr)

(* -------------------------- entry point --------------------------- *)

let compile_core
      (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		     fundecls,valdefs,domains,pcstate,cstates,criteria,
		     trace,handlers,chandlers,ifunctions,functions,attr)) =
  let (new_pcstate, new_handlers, new_chandlers, new_ifunctions, new_functions) =
    let sc = List.filter (fun (A.VARDECL(typ,_,_,_,_,_)) ->
                 match typ with
                   B.SET(B.CORE) -> true
                 | _ -> false)
                         procdecls
    in
    match pcstate with
      Ast.CORE(cvd, x) ->
	let cv = List.map (fun (A.VARDECL(_, id, _, _, _, _)) -> id) cvd in
	let new_cvd = List.map (fun d -> compile_decl cv d) cvd in
      (Ast.CORE(new_cvd, x),
       List.map (compile_handler cv sc) handlers,
       List.map (compile_handler cv []) chandlers,
       List.map (compile_function cv sc) ifunctions,
       List.map (compile_function cv []) functions)
    | Ast.PSTATE _ ->
       (pcstate, List.map (compile_handler [] sc) handlers,
        chandlers,
        List.map (compile_function [] sc) ifunctions,
        functions)
  in
  Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		fundecls,valdefs,domains,new_pcstate,cstates,criteria,
		trace,new_handlers,new_chandlers,new_ifunctions,new_functions,attr)
