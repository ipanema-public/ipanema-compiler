(* $Id$ *)

(* convert the ordering_criteria to the definition of a "greater" function *)

module B = Objects
module CS = Class_state
module CSel = Compile_select
open Format

let debug = ref true

(* -------------------- Miscellaneous operations -------------------- *)

let rec id_member x = function
    [] -> false
  | (y::r) -> B.ideq(x,y) || id_member x r

let mkif test tr fl attr =
  Ast.IF(test,Ast.SEQ([],tr,attr),Ast.SEQ([],fl,attr),attr)

(* ------------ Tests derived from the ordering criteria ------------ *)

let sysdecls = ref ([] : B.identifier list)

let collect_sys_fields procdecls =
  List.fold_left
    (function prev ->
      function
	  Ast.VARDECL(_,id,true,_,_,_,_) -> id :: prev
	| _ -> prev)
    [] procdecls

let compile_exp procfields vr exp =
  let rec loop = function
      Ast.VAR(id, ssa, attr) -> (* Nico: Axel must review *)
	if id_member id (!sysdecls)
	then Ast.FIELD(vr,id, ssa, attr)
	else if id_member id procfields
	then Ast.FIELD(vr,id, ssa, attr)
	else Ast.VAR(id, ssa, attr)

    | Ast.FIELD(exp,fld, ssa, attr) -> Ast.FIELD(loop exp,fld,ssa, attr)
    | Ast.UNARY(uop,exp,attr) ->
	Ast.UNARY(uop,loop exp,attr)

    | Ast.BINARY(bop,exp1,exp2,attr) ->
	Ast.BINARY(bop, loop exp1, loop exp2, attr)

    | Ast.PBINARY(bop,exp1,states1,exp2,states2,attr) ->
	raise (Error.Error "compile_ordering: unexpected pbinary")

    | Ast.INDR(exp,attr) -> Ast.INDR(loop exp,attr)
	  
    | Ast.PRIM(f,args,attr) ->
	Ast.PRIM(loop f,List.map loop args,attr)

    | exp -> exp (* make no sense in a criteria or ok as is *)
  in loop exp

let order2op = function
    Ast.LOWEST -> Ast.LT
  | Ast.HIGHEST -> Ast.GT

let order2notop = function
    Ast.LOWEST -> Ast.GT
  | Ast.HIGHEST -> Ast.LT

let compile_test tst thn els p pv procfields attr =
  mkif (compile_exp procfields p tst)
    [Ast.ASSIGN(pv,compile_exp procfields p thn,false,attr)]
    [Ast.ASSIGN(pv,compile_exp procfields p els,false,attr)]
    attr

let compare_pvid ty op pv1id pv2id attr =
  Ast.BINARY(op,pv1id,pv2id,attr)

let compile_one p1 p2 pv1 pv2 procfields = function
    Ast.CRIT_ID(_,order,id,_,attr) ->
      let ty = B.ty attr in
      (* Nico: Is ssa = [] here ? *)
      let pv1id = Ast.VAR(pv1, [], B.updty attr ty) in
      let pv2id = Ast.VAR(pv2, [], B.updty attr ty) in
      (ty,
       [Ast.ASSIGN(pv1id,Ast.FIELD(p1,id, [], attr),false,attr);
       Ast.ASSIGN(pv2id,Ast.FIELD(p2,id, [], attr),false,attr)],
       compare_pvid ty (order2op order) pv1id pv2id attr,
       compare_pvid ty (order2notop order) pv1id pv2id attr)
  (* | _ -> FIXME Not handled CRIT_EXPR  *)
          
          
(* ------- Tests derived from the ordering of the sorted queue ------ *)

let check_sorted lifo_fifo =
  match (lifo_fifo,!CS.selected) with
    (false,_) | (true, None) -> None
  | (true,Some(CS.STATE(id,_,CS.QUEUE(CS.SELECT(default)),_))) ->
      (match default with
	CS.NODEFAULT -> None
      | _ -> Some(id,default))
  | _ -> raise (Error.Error "internal error:compile_ordering:check_sorted")

let queue_order_test id default p1 p2 attr =
  let order =
    (match default with
      CS.FIFODEFAULT -> "first_fifo"
    | CS.LIFODEFAULT -> "first_lifo"
    | CS.NODEFAULT -> raise (Error.Error "internal error:compile_ordering:queue_order_test")) in
  let order = if (!CS.selected_size) > 1 then order ^"_ar" else order in
  let info =
    match !CS.selected with
      Some x -> x
    | None -> raise (Error.Error "internal error:compile_ordering:queue_order_test(2)") in
  let locexp = Ast.VAR(id, [], attr) in
  mkif
    (Ast.IN(p1,locexp,[info],B.BOTH,false,None,attr))
    [mkif
	(Ast.IN(p2,locexp,[info],B.BOTH,false,None,attr))
	[Ast.RETURN(Some(Ast.PRIM(Ast.VAR(B.mkId(order), [],B.updty attr B.VOID),
				  [Ast.VAR(id,[], attr);p1;p2],
				  attr)),
		    attr)]
	[Ast.RETURN(Some(Ast.BOOL(true,attr)),attr)]
	attr]
    [Ast.RETURN(Some(Ast.BOOL(false,attr)),attr)]
    attr

(* --------------------- Select an array element -------------------- *)

let key_index procdecls key_crit =
  let attr = B.mkAttr (-1) in
  let attr = B.updty attr B.INT in
  let pid = B.fresh_idx "p" in
  let p = Ast.VAR(pid, [], attr) in
  let accid = B.fresh_idx "acc" in
  let acc = Ast.VAR(accid, [], attr) in
  let ptype = B.PROCESS in
  let reorder mx exp attr = function
      Ast.HIGHEST -> Ast.BINARY(Ast.MINUS,Ast.INT(mx,attr),exp,attr)
    | Ast.LOWEST -> exp in
  let addend exp = function
      0 -> exp
    | mn -> Ast.BINARY(Ast.MINUS,exp,Ast.INT(mn,attr),attr) in
  let mult_pattern exp mn = function
      0 -> Ast.ASSIGN(acc,addend exp mn,false,attr)
    | offset ->
	Ast.ASSIGN(acc,Ast.BINARY(Ast.PLUS,
				  Ast.BINARY(Ast.TIMES,acc,
					     Ast.INT(offset,attr),
					     attr),
				  addend exp mn,attr),
		   false,attr) in
  let (_,code) =
    List.fold_left
      (function (offset,code) ->
	function
	    Ast.CRIT_ID(Ast.KEY(mn,mx),order,id,_,attr) ->
	      (true,
	       (mult_pattern (reorder mx (Ast.FIELD(p,id,[],attr)) attr order) mn
		  (if offset then (mx-mn+1) else 0))
	       :: code)
	  | _ -> raise (Error.Error "internal error"))
      (false,[]) key_crit in
  Ast.FUNDEF(Ast.DFT,B.INT,B.mkId("key_index"),
	     [Ast.VARDECL(ptype,pid,false,false,CS.UNSHARED,None,attr)],
	     Ast.SEQ([Ast.VALDEF(Ast.VARDECL(B.INT,accid,false,false,CS.UNSHARED,None,attr),
				 Ast.INT(0,attr),
				 Ast.VARIABLE,attr)],
		     List.rev (Ast.RETURN(Some(acc),attr) :: code), attr),
	     (if !debug then Ast.NO_INLINE else Ast.INLINE),attr)

(* ------------------ Making the complete function ------------------ *)

let mkdecl nm ty attr =
  Ast.SYSDEF(Ast.VARDECL(ty,nm,false,false,CS.UNSHARED,None,attr),Ast.CONST,attr)

let rec make_ifs tr fls fail pv1 pv2 attr = function
    ([],None,_) -> Ast.SEQ([],[Ast.RETURN(Some(fail),attr)],attr)
  | ([],Some(stmt),_) -> Ast.SEQ([],[stmt],attr)
  | ([(ty,stmts,test,ntest)],None,B.BOOL) ->
      Ast.SEQ([mkdecl pv1 ty attr; mkdecl pv2 ty attr],
	      stmts @ [Ast.RETURN(Some(test),attr)],attr)
  | ((ty,stmts,test,ntest)::rest,queue_order_test_stmt,ty1) ->
      let new_stmts =
	stmts @
	[mkif test
	    [Ast.RETURN(Some(tr),attr)]
	    [mkif ntest [Ast.RETURN(Some(fls),attr)] [] attr]
	    attr] in
      (match
	make_ifs tr fls fail pv1 pv2 attr (rest,queue_order_test_stmt,ty1)
      with
	Ast.SEQ([],rest_stmts,rest_attr) ->
	  Ast.SEQ([],
		  Ast.SEQ([mkdecl pv1 ty attr; mkdecl pv2 ty attr],
			  new_stmts,attr)
		    :: rest_stmts,
		  attr)
      |	rest_stmts ->
	  Ast.SEQ([],
		  [Ast.SEQ([mkdecl pv1 ty attr; mkdecl pv2 ty attr],
			   new_stmts,attr);
		    rest_stmts],
		  attr))

type critbody = SORTED of B.identifier option |
                FALL_THROUGH of B.identifier | NORMAL

let compile_crit name procdecls criteria prevname ty tr fls fail lifo_fifo =
  let ptype = B.PROCESS in
  let attr = B.mkAttr (-1) in
  let p1id = B.fresh_idx "p1" in
  let p1 = Ast.VAR(p1id, [], B.updty attr ptype) in
  let p2id = B.fresh_idx "p2" in
  let p2 = Ast.VAR(p2id, [], B.updty attr ptype) in
  let pv1 = B.fresh_idx "pv1" in
  let pv2 = B.fresh_idx "pv2" in
  let procfields =
    List.map (function Ast.VARDECL(ty,id,imported,lz,_,de,attr) -> id) procdecls in
  let stmts_tests = List.map (compile_one p1 p2 pv1 pv2 procfields) criteria in
  let args =
    [Ast.VARDECL(ptype,p1id,false,false,CS.UNSHARED, None,B.updty attr ptype);
     Ast.VARDECL(ptype,p2id,false,false,CS.UNSHARED, None,B.updty attr ptype)] in
  let args =
    if (!B.hls)
    then
      let sched_inst_type = B.SCHEDULER_INST in
      (Ast.VARDECL(sched_inst_type,B.mkId "scheduler_instance",false,false,
                   CS.UNSHARED,None,
		   B.updty attr sched_inst_type))
      :: args
    else args in
  let mktest queue_order_test_stmt =
    let body =
      make_ifs tr fls fail pv1 pv2 attr
	(stmts_tests, queue_order_test_stmt, ty) in
    Ast.FUNDEF(Ast.DFT,ty,B.mkId(name),args,body,(if !debug then Ast.NO_INLINE else Ast.INLINE),attr) in
  let si_args =
    let si_arg = Ast.VAR(B.mkId "scheduler_instance", [],
			 B.updty attr B.SCHEDULER_INST) in
    let args = [Ast.VAR(p1id,[],attr);Ast.VAR(p2id,[],attr)] in
    if (!B.hls) then si_arg :: args else args in
  match prevname with
    SORTED info ->
      (match (check_sorted lifo_fifo,info) with
	(None,Some previous_name) ->
	  (* no extra sorting and a previous function has already been defined.
	     just make a call to it, and hope for inlining *)
	  Ast.FUNDEF(Ast.DFT,B.BOOL,B.mkId(name),args,
		     Ast.SEQ([],
			     [Ast.RETURN(
			       Some(Ast.PRIM(
				 Ast.VAR(previous_name, [],
					 B.updty attr
					   B.VOID),
				 si_args,attr)),
			       attr)],
			     attr),
		     (if !debug then Ast.NO_INLINE else Ast.INLINE),attr)
      |	(None,None) -> mktest None
      |	(Some(id,default),_) ->
	  mktest (Some (queue_order_test id default p1 p2 attr)))
  | FALL_THROUGH previous_name ->
    mktest (Some (Ast.RETURN(
      Some(Ast.PRIM(
	Ast.VAR(previous_name, [],
		B.updty attr B.VOID),
	si_args,attr)),
      attr)))
  | NORMAL -> mktest None

let dummy_function name =
  let ptype = B.PROCESS in
  let attr = B.mkAttr (-1) in
  let p1id = B.fresh_idx "p1" in
  let p2id = B.fresh_idx "p2" in
  let args =
    [Ast.VARDECL(ptype,p1id,false,false,CS.UNSHARED, None,B.updty attr ptype);
     Ast.VARDECL(ptype,p2id,false,false,CS.UNSHARED, None,B.updty attr ptype)] in
  let args =
    if (!B.hls)
    then
      let sched_inst_type = B.SCHEDULER_INST in
      (Ast.VARDECL(sched_inst_type,B.mkId "scheduler_instance",false,false,
                   CS.UNSHARED,None,
		   B.updty attr sched_inst_type))
      :: args
    else args in
  Ast.FUNDEF(Ast.DFT,B.BOOL,B.mkId(name),args,
	     Ast.SEQ([],[Ast.RETURN(Some(Ast.BOOL(false,attr)),attr)],attr),
	     (if !debug then Ast.NO_INLINE else Ast.INLINE),attr)

let compile_sd_crit crit_info procdecls criteria attr =
  let rec loop = function
      ([],[]) -> ([],[],[])
    | (CSel.DYNCRIT::_,criteria) -> ([],[],criteria)
    | (CSel.STATCRIT::rest_info,crit::rest_crit) ->
	let (key_crit,stat_crit,dyn_crit) = loop (rest_info,rest_crit) in
	(key_crit,crit::stat_crit,dyn_crit) 
    | (CSel.KEYCRIT::rest_info,crit::rest_crit) ->
	let (key_crit,stat_crit,dyn_crit) = loop (rest_info,rest_crit) in
	(crit::key_crit,stat_crit,dyn_crit) 
    | (_,_) -> raise (Error.Error "internal error") in
  let (key_crit,stat_crit,dyn_crit) = loop (crit_info,criteria) in
  [key_index procdecls key_crit;
    compile_crit "static_compare" procdecls stat_crit NORMAL B.INT
      (Ast.INT(1,attr)) (Ast.INT(-1,attr)) (Ast.INT(0,attr)) false;
    (* should call static_greater is key criteria run out *)
    compile_crit "static_select_compare" procdecls key_crit
      (FALL_THROUGH(B.mkId "static_compare")) B.INT
      (Ast.INT(1,attr)) (Ast.INT(-1,attr)) (Ast.INT(0,attr)) false;
    compile_crit "dynamic_select_greater" procdecls dyn_crit NORMAL B.BOOL
      (Ast.BOOL(true,attr)) (Ast.BOOL(false,attr)) (Ast.BOOL(false,attr))
      false;
    compile_crit "dynamic_greater" procdecls dyn_crit
      (SORTED(Some(B.mkId "dynamic_select_greater"))) B.BOOL
      (Ast.BOOL(true,attr)) (Ast.BOOL(false,attr)) (Ast.BOOL(false,attr))
      false]
  @
  if !Compile_select.used_lifo_fifo
  then
    (* not sure to understand NORMAL, SORTED and fallthrough, and why static
       and dynamic are different in this regard *)
    [compile_crit "static_compare_lifo_fifo" procdecls stat_crit NORMAL B.INT
	(Ast.INT(1,attr)) (Ast.INT(-1,attr)) (Ast.INT(0,attr)) true;
    (* should call static_greater is key criteria run out *)
      compile_crit "static_select_compare_lifo_fifo" procdecls key_crit
	(FALL_THROUGH(B.mkId "static_compare_lifo_fifo")) B.INT
	(Ast.INT(1,attr)) (Ast.INT(-1,attr)) (Ast.INT(0,attr)) true;
      compile_crit "dynamic_select_greater_lifo_fifo" procdecls dyn_crit
	NORMAL B.BOOL
	(Ast.BOOL(true,attr)) (Ast.BOOL(false,attr)) (Ast.BOOL(false,attr))
	true;
      compile_crit "dynamic_greater_lifo_fifo" procdecls dyn_crit
	(SORTED(Some(B.mkId "dynamic_select_greater_lifo_fifo"))) B.BOOL
	(Ast.BOOL(true,attr)) (Ast.BOOL(false,attr)) (Ast.BOOL(false,attr))
	true]
  else List.map dummy_function
      ["static_compare_lifo_fifo";"static_select_compare_lifo_fifo";
	"dynamic_select_greater_lifo_fifo"; "dynamic_greater_lifo_fifo"]

(* ------------------- the size of the key array -------------------- *)

let key_size criteria =
  List.fold_left
    (function sz ->
      function
	  Ast.CRIT_ID(Ast.KEY(mn,mx),order,id,_,attr) ->
	    sz * (mx - mn + 1)
	| _ -> sz)
    1 criteria

(* ------------------- ordering swap/compare ------------------------ *)

let print_order = function
    Ast.HIGHEST -> "highest"
  | Ast.LOWEST -> "lowest"

let print_criteria (ty, crit) = match crit with
    Ast.CRIT_ID(key,ord,id,fnid,attr) ->
    begin
      let fnid = match fnid with None -> "" | Some fnid -> (B.id2c fnid) in
      match key with
        Ast.KEY(low,high) ->
        Printf.printf "%s: [id] (%d,%d) %s %s -> %s\n"
                      (B.type2c ty) low high (print_order ord) (B.id2c id) fnid
      | Ast.NOKEY ->
         Printf.printf "%s: [id] %s %s -> %s\n"
                       (B.type2c ty) (print_order ord) (B.id2c id) fnid
    end
  | Ast.CRIT_EXPR(key,ord,exp,fnid,attr) ->
     begin
       let fnid = match fnid with None -> "" | Some fnid -> (B.id2c fnid) in
       match key with
         Ast.KEY(low,high) ->
         Printf.printf "%s: (%d,%d) %s %s -> %s\n"
                       (B.type2c ty) low high (print_order ord) (Ast.exp2c exp) fnid
       | Ast.NOKEY ->
          Printf.printf "%s: %s %s -> %s\n"
                        (B.type2c ty) (print_order ord) (Ast.exp2c exp) fnid
     end

let rec collect_criteria_stmt  = function
    Ast.IF(_,thn,els,_) -> (collect_criteria_stmt thn)@(collect_criteria_stmt els)
  | Ast.FOR(_,state_ids,states,_,stmt,Some crit,attr) ->
     (if states = [] then
        match state_ids with
          None -> failwith "Unsupported for-iteration !"
        | Some [set_id] -> (match B.ty (Ast.get_exp_attr set_id) with
                              B.SET B.DOMAIN ->
                              (B.DOMAIN,crit)::(collect_criteria_stmt stmt)
                            | B.SET B.GROUP ->
                               (B.GROUP,crit)::(collect_criteria_stmt stmt)
                            | B.SET B.CORE ->
                               (B.CORE,crit)::(collect_criteria_stmt stmt)
                            | B.QUEUE B.PROCESS ->
                               (B.PROCESS,crit)::(collect_criteria_stmt stmt))
      else (B.ty attr,crit)::(collect_criteria_stmt stmt)
     )  
  | Ast.FOR(_,_,_,_,stmt,None,_) -> collect_criteria_stmt stmt
  | Ast.FOR_WRAPPER(_,stmts,_) -> List.flatten (List.map collect_criteria_stmt stmts)
  | Ast.SWITCH(_,sc,Some def,_) -> (collect_criteria_stmt def)@
                                     (List.flatten
                                        (List.map collect_criteria_seq_case sc))
  | Ast.SWITCH(_,sc,None,_) -> List.flatten (List.map collect_criteria_seq_case sc)
  | Ast.SEQ(_,stmts,_) -> List.flatten (List.map collect_criteria_stmt stmts)
  | Ast.SAFEMOVEFWD(_,_,stmt,_,_) -> collect_criteria_stmt stmt
  | _ -> []

and collect_criteria_seq_case = function
    Ast.SEQ_CASE(_,_,stmt,_) -> collect_criteria_stmt stmt

let collect_criteria_handler = function
    Ast.EVENT(_,_,stmt,_,_) -> collect_criteria_stmt stmt

let collect_criteria_function = function
    Ast.FUNDEF(_,_,_,_,stmt,_,_) -> collect_criteria_stmt stmt

let remove_elt e l =
  let rec go l acc = match l with
    | [] -> List.rev acc
    | x::xs when e = x -> go xs acc
    | x::xs -> go xs (x::acc)
  in go l []
   
let remove_duplicates l =
  let rec go l acc = match l with
    | [] -> List.rev acc
    | x :: xs -> go (remove_elt x xs) (x::acc)
  in go l []

let rec compile_exp v = function
    Ast.VAR(id,s,attr) -> Ast.FIELD(v, id, s, attr)
  | Ast.UNARY(uop,exp,attr) -> Ast.UNARY(uop,compile_exp v exp,attr)
  | Ast.BINARY(bop,e1,e2,attr) -> Ast.BINARY(bop,compile_exp v e1,
                                             compile_exp v e2,attr)
  | exp -> exp

let generate_cmp_function_core_id = function
    Ast.CRIT_ID(_,ord,id,_,attr) as crit ->
    let params = [Ast.VARDECL(B.CONST (B.INDR (B.VOID)), B.mkId "p1", false, false, CS.UNSHARED, None, attr);
                  Ast.VARDECL(B.CONST (B.INDR (B.VOID)), B.mkId "p2", false, false, CS.UNSHARED, None, attr)] in
    let vdecls = [Ast.VALDEF(Ast.VARDECL(B.CORE,B.mkId "c1",false,false,CS.UNSHARED,None,attr),
                             Ast.INDR(Ast.CAST(B.INDR(B.CORE), Ast.VAR(B.mkId "p1",[],attr),attr),attr),
                             Ast.CONSTVAR, attr);
                  Ast.VALDEF(Ast.VARDECL(B.CORE,B.mkId "c2",false,false,CS.UNSHARED,None,attr),
                             Ast.INDR(Ast.CAST(B.INDR(B.CORE), Ast.VAR(B.mkId "p2",[],attr),attr),attr),
                             Ast.CONSTVAR, attr)] in
    let stmts = [Ast.IF(Ast.BINARY(order2notop ord,
                                   Ast.FIELD(Ast.VAR(B.mkId "c1",[],B.updty attr B.CORE),
                                             id,[],attr),
                                   Ast.FIELD(Ast.VAR(B.mkId "c2",[], B.updty attr B.CORE),
                                             id,[],B.updty attr B.INT),attr),
                        Ast.RETURN(Some(Ast.INT(1,attr)), attr), Ast.SEQ([],[],attr),
                        attr);
                 Ast.IF(Ast.BINARY(order2op ord,
                                   Ast.FIELD(Ast.VAR(B.mkId "c1",[],B.updty attr B.CORE),
                                             id,[],attr),
                                   Ast.FIELD(Ast.VAR(B.mkId "c2",[],B.updty attr B.CORE),
                                             id,[],attr),attr),
                        Ast.RETURN(Some(Ast.INT(-1,attr)), attr), Ast.SEQ([],[],attr),
                        attr);
                 Ast.RETURN(Some(Ast.INT(0,attr)),attr)] in
    let fn_id = B.mkId ("cmp_core_"^(B.id2c id)) in
    Progtypes.add_internal (B.id2c fn_id);
    (B.CORE,crit,Ast.FUNDEF(Ast.DFT,B.INT,fn_id,params,Ast.SEQ(vdecls,stmts,attr),(if !debug then Ast.NO_INLINE else Ast.INLINE),attr))

let generate_cmp_function_core_exp = function
    Ast.CRIT_EXPR(_,ord,exp,_,attr) as crit ->
    let params = [Ast.VARDECL(B.CONST (B.INDR (B.VOID)), B.mkId "p1", false, false, CS.UNSHARED, None, attr);
                  Ast.VARDECL(B.CONST (B.INDR (B.VOID)), B.mkId "p2", false, false, CS.UNSHARED, None, attr)] in
    let vdecls = [Ast.VALDEF(Ast.VARDECL(B.CORE,B.mkId "c1",false,false,CS.UNSHARED,None,B.updty attr B.CORE),
                             Ast.INDR(Ast.CAST(B.INDR(B.CORE), Ast.VAR(B.mkId "p1",[],attr),attr),attr),
                             Ast.CONSTVAR, attr);
                  Ast.VALDEF(Ast.VARDECL(B.CORE,B.mkId "c2",false,false,CS.UNSHARED,None,B.updty attr B.CORE),
                             Ast.INDR(Ast.CAST(B.INDR(B.CORE), Ast.VAR(B.mkId "p2",[],attr),attr),attr),
                             Ast.CONSTVAR, attr);
                  Ast.VALDEF(Ast.VARDECL(B.INT,B.mkId "e1",false,false,CS.UNSHARED,None,B.updty attr B.INT),
                             compile_exp (Ast.VAR(B.mkId "c1",[],B.updty attr B.CORE)) exp,
                             Ast.VARIABLE, attr);
                  Ast.VALDEF(Ast.VARDECL(B.INT,B.mkId "e2",false,false,CS.UNSHARED,None,B.updty attr B.INT),
                             compile_exp (Ast.VAR(B.mkId "c2",[],B.updty attr B.CORE)) exp,
                             Ast.VARIABLE, attr)] in
    let stmts = [Ast.IF(Ast.BINARY(order2notop ord,
                                   Ast.VAR(B.mkId "e1",[],attr),
                                   Ast.VAR(B.mkId "e2",[],attr),attr),
                        Ast.RETURN(Some(Ast.INT(1,attr)), attr), Ast.SEQ([],[],attr),
                        attr);
                 Ast.IF(Ast.BINARY(order2op ord,
                                   Ast.VAR(B.mkId "e1",[],attr),
                                   Ast.VAR(B.mkId "e2",[],attr),attr),
                        Ast.RETURN(Some(Ast.INT(-1,attr)), attr), Ast.SEQ([],[],attr),
                        attr);
                 Ast.RETURN(Some(Ast.INT(0,attr)),attr)] in
    let fn_id = B.fresh_idx "cmp_core" in
    (B.CORE,crit,Ast.FUNDEF(Ast.DFT,B.INT,fn_id,params,Ast.SEQ(vdecls,stmts,attr),(if !debug then Ast.NO_INLINE else Ast.INLINE),attr))

let generate_cmp_function_group_id = function
    Ast.CRIT_ID(_,ord,id,_,attr) as crit ->
    let params = [Ast.VARDECL(B.CONST (B.INDR (B.VOID)), B.mkId "p1", false, false, CS.UNSHARED, None, attr);
                  Ast.VARDECL(B.CONST (B.INDR (B.VOID)), B.mkId "p2", false, false, CS.UNSHARED, None, attr)] in
    let vdecls = [Ast.VALDEF(Ast.VARDECL(B.GROUP,B.mkId "g1",false,false,CS.UNSHARED,None,B.updty attr B.GROUP),
                             Ast.INDR(Ast.CAST(B.INDR(B.GROUP), Ast.VAR(B.mkId "p1",[],attr),attr),attr),
                             Ast.CONSTVAR, attr);
                  Ast.VALDEF(Ast.VARDECL(B.GROUP,B.mkId "g2",false,false,CS.UNSHARED,None,B.updty attr B.GROUP),
                             Ast.INDR(Ast.CAST(B.INDR(B.GROUP), Ast.VAR(B.mkId "p2",[],attr),attr),attr),
                             Ast.CONSTVAR, attr)] in
    let stmts = [Ast.IF(Ast.BINARY(order2notop ord,
                                   Ast.FIELD(Ast.VAR(B.mkId "g1",[],B.updty attr B.GROUP),
                                             id,[],attr),
                                   Ast.FIELD(Ast.VAR(B.mkId "g2",[],B.updty attr B.GROUP),
                                             id,[],attr),attr),
                        Ast.RETURN(Some(Ast.INT(1,attr)), attr), Ast.SEQ([],[],attr),
                        attr);
                 Ast.IF(Ast.BINARY(order2op ord,
                                   Ast.FIELD(Ast.VAR(B.mkId "g1",[],B.updty attr B.GROUP),
                                             id,[],attr),
                                   Ast.FIELD(Ast.VAR(B.mkId "g2",[],B.updty attr B.GROUP),
                                             id,[],attr),attr),
                        Ast.RETURN(Some(Ast.INT(-1,attr)), attr), Ast.SEQ([],[],attr),
                        attr);
                 Ast.RETURN(Some(Ast.INT(0,attr)),attr)] in
    let fn_id = B.mkId ("cmp_group_"^(B.id2c id)) in
    (B.GROUP,crit,Ast.FUNDEF(Ast.DFT,B.INT,fn_id,params,Ast.SEQ(vdecls,stmts,attr),(if !debug then Ast.NO_INLINE else Ast.INLINE),attr))

let generate_cmp_function_group_exp = function
    Ast.CRIT_EXPR(_,ord,exp,_,attr) as crit ->
    let params = [Ast.VARDECL(B.CONST (B.INDR (B.VOID)), B.mkId "p1", false, false, CS.UNSHARED, None, attr);
                  Ast.VARDECL(B.CONST (B.INDR (B.VOID)), B.mkId "p2", false, false, CS.UNSHARED, None, attr)] in
    let vdecls = [Ast.VALDEF(Ast.VARDECL(B.GROUP,B.mkId "g1",false,false,CS.UNSHARED,None,B.updty attr B.GROUP),
                             Ast.INDR(Ast.CAST(B.INDR(B.GROUP), Ast.VAR(B.mkId "p1",[],attr),attr),attr),
                             Ast.CONSTVAR, attr);
                  Ast.VALDEF(Ast.VARDECL(B.GROUP,B.mkId "g2",false,false,CS.UNSHARED,None,B.updty attr B.GROUP),
                             Ast.INDR(Ast.CAST(B.INDR(B.GROUP), Ast.VAR(B.mkId "p2",[],attr),attr),attr),
                             Ast.CONSTVAR, attr);
                  Ast.VALDEF(Ast.VARDECL(B.INT,B.mkId "e1",false,false,CS.UNSHARED,None,attr),
                             compile_exp (Ast.VAR(B.mkId "g1",[],B.updty attr B.GROUP)) exp,
                             Ast.VARIABLE, attr);
                  Ast.VALDEF(Ast.VARDECL(B.INT,B.mkId "e2",false,false,CS.UNSHARED,None,attr),
                             compile_exp (Ast.VAR(B.mkId "g2",[],B.updty attr B.GROUP)) exp,
                             Ast.VARIABLE, attr)] in
    let stmts = [Ast.IF(Ast.BINARY(order2notop ord,
                                   Ast.VAR(B.mkId "e1",[],attr),
                                   Ast.VAR(B.mkId "e2",[],attr),attr),
                        Ast.RETURN(Some(Ast.INT(1,attr)), attr), Ast.SEQ([],[],attr),
                        attr);
                 Ast.IF(Ast.BINARY(order2op ord,
                                   Ast.VAR(B.mkId "e1",[],attr),
                                   Ast.VAR(B.mkId "e2",[],attr),attr),
                        Ast.RETURN(Some(Ast.INT(-1,attr)), attr), Ast.SEQ([],[],attr),
                        attr);
                 Ast.RETURN(Some(Ast.INT(0,attr)),attr)] in
    let fn_id = B.fresh_idx "cmp_group" in
    (B.GROUP,crit,Ast.FUNDEF(Ast.DFT,B.INT,fn_id,params,Ast.SEQ(vdecls,stmts,attr),(if !debug then Ast.NO_INLINE else Ast.INLINE),attr))

let generate_cmp_function_core = function
    Ast.CRIT_ID(_,_,_,_,_) as crit -> generate_cmp_function_core_id crit
  | Ast.CRIT_EXPR(_,_,_,_,_) as crit ->  generate_cmp_function_core_exp crit

let generate_cmp_function_group = function
    Ast.CRIT_ID(_,_,_,_,_) as crit -> generate_cmp_function_group_id crit
  | Ast.CRIT_EXPR(_,_,_,_,_) as crit ->  generate_cmp_function_group_exp crit

let generate_cmp_function = function
    (B.CORE,crit) -> generate_cmp_function_core crit
  | (B.GROUP,crit) -> generate_cmp_function_group crit
  | (_,crit) -> failwith "Compare function generation support only CORE and GROUP"

(* ------------------------- Compile criteria ----------------------- *)

let rec compile_criteria_stmt crit2fn_list = function
    Ast.IF(tst,thn,els,a) -> Ast.IF(tst,compile_criteria_stmt crit2fn_list thn,
                                    compile_criteria_stmt crit2fn_list els,a)
  | Ast.FOR(id,state_ids,states,d,stmt,Some crit,attr) as forloop ->
     begin
       let typ = 
         if states = [] then
           match state_ids with
             None -> failwith "Unsupported for-iteration !"
           | Some [set_id] ->
              begin
                match B.ty (Ast.get_exp_attr set_id) with
                  B.SET B.DOMAIN -> B.DOMAIN
                | B.SET B.GROUP ->B.GROUP
                | B.SET B.CORE -> B.CORE
                | B.QUEUE B.PROCESS -> B.PROCESS
              end
         else B.ty attr in
       let (ty,cri,cmp_fn) = try List.find
                                   (function (ty,cri,fn) -> ty = typ && cri = crit)
                                   crit2fn_list
                             with Not_found -> failwith "compare function for ordering not found" in
       let Ast.FUNDEF(_,_,fnid,_,_,_,_) = cmp_fn
       and Ast.CRIT_EXPR(a,b,c,_,e) = crit in
       Ast.FOR(id,state_ids,states,d,compile_criteria_stmt crit2fn_list stmt,
               Some (Ast.CRIT_EXPR(a,b,c,Some fnid,e)),attr)
     end
  (* let typ = (if states = [] then *)
  (*              match state_ids with *)
  (*                None -> failwith "Unsupported for-iteration !" *)
  (*              | Some [set_id] -> (match B.ty (Ast.get_exp_attr set_id) with *)
  (*                                    B.SET B.DOMAIN -> B.DOMAIN *)
  (*                                  | B.SET B.GROUP -> B.GROUP *)
  (*                                  | B.SET B.CORE -> B.CORE *)
  (*                                  | B.QUEUE B.PROCESS -> B.PROCESS) *)
  (*            else failwith "Unsupported for-iteration !" *)
  (*           ) in *)
  (* let Ast.SEQ(vd,st,aa) as sort_seq = gen_wrap_seq_criteria forloop crit2fn_list (typ,crit) in *)
  (* Ast.SEQ(vd,st,aa) *)
  | Ast.FOR(id,sc,si,d,stmt,None,a) ->
     begin
       Ast.FOR(id,sc,si,d,
               compile_criteria_stmt crit2fn_list stmt,None,a)
     end
  | Ast.FOR_WRAPPER(id,stmts,a) -> Ast.FOR_WRAPPER(id,
                                                   List.map (function s -> compile_criteria_stmt
                                                                             crit2fn_list s)
                                                            stmts,
                                                   a)
  | Ast.SWITCH(tst,sc,Some def,a) -> Ast.SWITCH(tst,
                                                List.map (function s -> compile_criteria_seq_case
                                                                          crit2fn_list s)
                                                         sc,
                                                Some(compile_criteria_stmt crit2fn_list def),a)
  | Ast.SWITCH(tst,sc,None,a) -> Ast.SWITCH(tst,
                                            List.map (function s -> compile_criteria_seq_case
                                                                      crit2fn_list s)
                                                     sc,
                                            None,a)
  | Ast.SEQ(lv,stmts,a) -> Ast.SEQ(lv,List.map (function s -> compile_criteria_stmt
                                                                crit2fn_list s)
                                               stmts,a)
  | Ast.SAFEMOVEFWD(e,si,stmt,se,a) -> Ast.SAFEMOVEFWD(e,si,
                                                       compile_criteria_stmt crit2fn_list stmt,
                                                       se,a)
  | s -> s

and compile_criteria_seq_case crit2fn_list = function
    Ast.SEQ_CASE(id,si,stmt,a) -> Ast.SEQ_CASE(id,si,compile_criteria_stmt crit2fn_list stmt,a)

and get_size set_id =
  match set_id with
    Ast.FIELD(v, fld, ssa, attr) ->
    begin
      match B.ty attr with
	B.SET B.DOMAIN -> Ast.FIELD(v, B.mkId ("___sched_domains_idx"), ssa,B.updty attr B.INT)
      | B.SET B.GROUP -> Ast.FIELD(v, B.mkId ("___sched_group_idx"), ssa,B.updty attr B.INT)
      | B.SET B.CORE -> set_id (* FIXME: Handle *)
      | ty -> failwith ("Need to get size of "^ B.type2c ty ^" at " ^__LOC__)
    end
  | _ -> set_id (* FIXME *)

(* and gen_wrap_seq_criteria forloop crit2fn_list (typ,crit) = *)
(*   let (ty,cri,cmp_fn) = try List.find (function (ty,cri,fn) -> ty = typ && cri = crit) crit2fn_list *)
(*                         with Not_found -> failwith "compare function for ordering not found" in *)
(*   let Ast.FUNDEF(_,_,id,_,_,_,a) = cmp_fn in *)
(*   let new_id = B.fresh_idx "tmp_array" in *)
(*   let (parm,swp_fnid) = match typ with *)
(*       B.CORE -> (Ast.UNINITDEF(Ast.VARDECL(B.CORE,new_id,false,false,None,a),a), *)
(*                  B.mkId "swap_cores") *)
(*     | B.GROUP-> (Ast.UNINITDEF(Ast.VARDECL(B.GROUP,new_id,false,false,None,a),a), *)
(*                   B.mkId "swap_groups") in *)
(*   let Ast.FOR(id,Some(state_ids),states,d,stmt,Some crit,attr) = forloop in *)
(*   let iter_id = B.fresh_idx "i" in *)
(*   Ast.SEQ([parm;Ast.UNINITDEF(Ast.VARDECL(B.INT,iter_id,false,false,None,a),a)], *)
(*           [Ast.FOR(id,Some (state_ids(\* @[Ast.ASSIGN(Ast.VAR(iter_id,[],attr), *\) *)
(*                                      (\*              Ast.BINARY(Ast.PLUS, *\) *)
(*                                      (\*                         Ast.VAR(iter_id,[],attr), *\) *)
(*                                      (\*                         Ast.INT(1,attr)), *\) *)
(*                                      (\*              attr)] *\)), *)
(*                    states,d,Ast.ASSIGN(Ast.INDR(Ast.AREF(Ast.VAR(new_id,[],attr), *)
(*                                                          Some (Ast.VAR(iter_id,[],attr)), *)
(*                                                          attr), *)
(*                                                 attr),Ast.VAR(id,[],attr),false,attr), *)
(*                    None,attr); *)
(*            Ast.FOR(id,Some state_ids,states,d,stmt,None,attr)], *)
(*           a) *)

let compile_criteria_handler crit2fn_list = function
  Ast.EVENT(nm,params,stmt,syn,attr) ->
    Ast.EVENT(nm,params,compile_criteria_stmt crit2fn_list stmt,syn,attr)

let compile_criteria_function crit2fn_list = function
  Ast.FUNDEF(tl,ty, nm,params,stmt,inl,attr) ->
    Ast.FUNDEF(tl, ty, nm,params,compile_criteria_stmt crit2fn_list stmt,inl,attr)

(* -------------------------- entry point --------------------------- *)

let compile_ordering
      (crit_info,
       Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		     fundecls,valdefs,domains,states,cstates,criteria,
		     trace,handlers,chandlers,ifunctions,functions,attr)) =
  let for_criteria_chdl = remove_duplicates
    (List.flatten (List.map collect_criteria_handler (handlers @ chandlers))) in
  let for_criteria_fct = remove_duplicates
    (List.flatten (List.map collect_criteria_function functions)) in
  let for_criteria = for_criteria_chdl @ for_criteria_fct in
  (* List.iter print_criteria for_criteria; *)
  let crit2fn_list = List.map generate_cmp_function for_criteria in
  let critfunctions = List.map (function (ty,crit,fn) -> fn) crit2fn_list in
  let handlers = List.map (function h -> compile_criteria_handler crit2fn_list h) handlers in
  let chandlers = List.map (function h -> compile_criteria_handler crit2fn_list h) chandlers in
  let functions = List.map (function h -> compile_criteria_function crit2fn_list h) functions in
  (* CS.selected_size := key_size criteria; *)
  (* sysdecls := collect_sys_fields procdecls; *)
  (*  let greater =
    compile_crit "greater" procdecls criteria (SORTED None) B.BOOL
      (Ast.BOOL(true,attr)) (Ast.BOOL(false,attr)) (Ast.BOOL(false,attr))
      false in
  let greater_lifo_fifo =
    match check_sorted true with
      None -> dummy_function "greater_lifo_fifo"
    | Some (_,_) ->
	compile_crit "greater_lifo_fifo" procdecls criteria (SORTED None)
	  B.BOOL
	  (Ast.BOOL(true,attr)) (Ast.BOOL(false,attr)) (Ast.BOOL(false,attr))
	  true in *)
  Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		fundecls,valdefs,domains,states,cstates,
		criteria,trace,handlers,chandlers,ifunctions,
		(* FIXME: Revisit ordering *)
		(* greater :: greater_lifo_fifo ::
		(compile_sd_crit crit_info procdecls criteria attr) @ *)
		critfunctions @ functions,attr)
