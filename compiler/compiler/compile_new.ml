
module B = Objects
module CS = Class_state

let rec prepare_stmt count = function
    Ast.MOVE (e, ds, x, srcs, dst, auto_allowed, state_end, a) ->
      (match ds with
	Ast.VAR  _ ->
	 (count+1,
          Ast.RETURN (Some (Ast.PRIM(Ast.VAR(B.mkId "smp_processor_id", [], a),
				     [], a)), a))
       | Ast.FIELD(core, _, _, _) ->
          (count +1,
           Ast.SEQ([],
                   [Ast.ASSIGN(Ast.FIELD(e, B.mkId Ipanema.cont, [], (B.dum __LOC__)),
                               Ast.INT(count, (B.dum __LOC__)), false, (B.dum __LOC__));
	            Ast.RETURN (Some (Ast.FIELD(core, B.mkId "id", [], a)), a)],
                   a
          ))
      )
  | Ast.SEQ(d, s, a) ->
     let (count, s) = List.fold_left
                        (fun (count, acc) s ->
                          let (count, s) = prepare_stmt count s in
                          (count, s::acc)
                        )
                        (count, []) s in
     (count, Ast.SEQ(d, List.rev s, a))
  | Ast.IF(e, s1, s2, a) ->
     let (count, s1) = prepare_stmt count s1 in
     let (count, s2) = prepare_stmt count s2 in
     (count, Ast.IF(e, s1, s2, a))
  | x -> (count, x)

let rec get_move r = function
    Ast.MOVE _ as m -> m::r
  | Ast.SEQ(_, s, _) -> List.fold_left get_move r s
  | Ast.IF(_, s1, s2, _) ->
     let r = get_move r s1 in
     get_move r s2
  | _ -> r

let place_stmt e stmt =
  let dumpos = B.mkAttr (-1) in
  let target = Ast.FIELD(Ast.VAR(e, [], B.updty dumpos B.PEVENT),
			   B.mkId "target", [],B.updty dumpos B.PROCESS)
  in
  let idlecore = B.fresh_idx "idlecore"
  and taskcpu = Ast.mkPRIM("task_cpu", [Ast.FIELD(target, B.mkId "task", [],
						  B.updty dumpos (B.STRUCT (B.mkId "task")))],-1) in
  let declcore = Ast.VALDEF(Ast.VARDECL(B.CORE, idlecore, false, false, CS.UNSHARED, None, dumpos),
			    Ast.AREF(
                                Ast.mkPRIM(Ipanema.policy_core, [taskcpu], -1),
                                None, dumpos), Ast.VARIABLE, dumpos)
  and idlecorevar = Ast.VAR(idlecore, [], B.updty dumpos B.CORE) in
  let updatecload = Ast.ASSIGN(
                        Ast.FIELD(idlecorevar, B.mkId Ipanema.cload, [], dumpos),
                        Ast.BINARY(Ast.PLUS,
                                   Ast.FIELD(idlecorevar, B.mkId Ipanema.cload, [], dumpos),
                                   Ast.FIELD(target, B.mkId Ipanema.load, [], dumpos),
                                   dumpos),
                        false,
                        dumpos)
  in
  let moves = get_move [] stmt in
  let newmoves =
    List.fold_left (
        fun (count, acc) (Ast.MOVE (e, ds, x, srcs, dst, auto_allowed, state_end, a)) ->
        let dsid = match ds with
            Ast.VAR(dsid, _, _) -> dsid
          | Ast.FIELD(_, dsid, _, _) -> dsid
        in
        let newmove = Ast.MOVE (e,
			        Ast.FIELD(idlecorevar, dsid, [], dumpos),
			        x, srcs, dst, auto_allowed, state_end, a) in
        (count-1, Ast.SEQ_CASE([B.mkId (string_of_int count)],[],newmove, (B.dum __LOC__))::acc)
      ) (-1 + List.length moves, []) moves
  in
  let cont = Ast.SWITCH(Ast.FIELD(target, B.mkId Ipanema.cont, [], (B.dum __LOC__)),
                        snd newmoves, None, (B.dum __LOC__)) in
  let wmb = Compile_wmb.smp_wmb (B.dum __LOC__) in
  Ast.SEQ([declcore], [updatecload;wmb;cont],dumpos)

let end_stmt e =
  let dumpos = B.mkAttr (-1) in
  let target = Ast.FIELD(Ast.VAR(e, [], B.updty dumpos B.PEVENT),
			   B.mkId "target", [],B.updty dumpos (B.STRUCT (B.mkId "task_struct")))
  in
  let pid = Ast.FIELD(target, B.mkId "pid", [], B.updty dumpos B.INT) in
  let cpu = Ast.PRIM(Ast.VAR(B.mkId "task_cpu",[],dumpos), [target], B.updty dumpos B.INT) in
  Ast.mkPRIMSTMT("pr_info",
		 [(Ast.INT_STRING ("[%d] post new on core %d\\n", dumpos));
		  pid; cpu],
		 -1)

(* -------------------------- edit new event --------------------------- *)

let compile_handler events =
  function (Ast.EVENT(Ast.EVENT_NAME(nm, eattr),param,stmt,syn,attr) as e) ->
    if B.id2c nm = "new" then
      let Ast.VARDECL(_, e, _, _, _, _, _) = param in
      let (count, prep_stmt) = prepare_stmt 0 stmt in
      if count > 0 then Ipanema.usecont := true;
      let new_prepare = Ast.EVENT(Ast.EVENT_NAME(B.mkId "new_prepare", eattr),
				  param, prep_stmt, syn, attr)
      and new_place = Ast.EVENT(Ast.EVENT_NAME(B.mkId "new_place",eattr),
				param, place_stmt e stmt, syn, attr)
      and new_end = Ast.EVENT(Ast.EVENT_NAME(B.mkId "new_end",eattr),
			      param, end_stmt e, syn, attr)
      in
	new_end :: new_place :: new_prepare :: events
    else
      e :: events

(* -------------------------- entry point --------------------------- *)

(* FIXME: handle local/global
 * process states correctly *)
let compile_new
    (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		   fundecls,valdefs,domains,pcstate,cstates,criteria,
		   trace,handlers,chandlers,ifunctions,functions,attr)) =
  let newhandlers = List.fold_left compile_handler [] handlers in
  Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		fundecls,valdefs,domains, pcstate,cstates,
		criteria, trace, newhandlers, chandlers,
		ifunctions, functions, attr)
