val rename_module : (Objects.identifier * Objects.identifier) list -> (*vars*)
  (Objects.identifier * Objects.identifier) list -> (*fields*)
    (Objects.identifier * Objects.identifier) list -> (*states*)
      Mast.module_def -> Mast.module_def

val rename_stmt : (Objects.identifier * Objects.identifier) list -> (*vars*)
  (Objects.identifier * Objects.identifier) list -> (*fields*)
    (Objects.identifier * Objects.identifier) list -> (*states*)
      Mast.stmt -> Mast.stmt
