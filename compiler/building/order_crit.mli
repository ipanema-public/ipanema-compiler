val verif_order_crit_param :
    Mast.criteria list -> Mast.module_def list -> Mast.criteria list
val extract_module_process : Objects.identifier -> Mast.vardecl list ->
  (Objects.identifier * Objects.identifier * Mast.requires * Objects.typ *
     Objects.attribute) list
val extract_module_global : Objects.identifier -> Mast.valdef list ->
  (Objects.identifier * Objects.identifier * Mast.requires * Objects.typ *
     Objects.attribute) list
