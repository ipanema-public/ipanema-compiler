(* Renaming as may be useful in the module combining process.  Renaming of
a field is assumed to apply to the process structure, and thus "source" and
"target" are not accepted for renaming. *)

module B = Objects

let check env id =
  try List.assoc id env with Not_found -> id

let check2 env1 env2 id =
  try List.assoc id env1
  with Not_found -> (try List.assoc id env2 with Not_found -> id)

let rec rename_expr vars fields states = function
    Mast.VAR(id,a) -> Mast.VAR(check2 vars states id,a)
  | Mast.FIELD_OC(mn,nm,a) ->
      raise (Error.Error
	       (Printf.sprintf "%s: should not occur in a module" (B.loc2c a)))
  | Mast.FIELD(exp,fld,a) ->
      Mast.FIELD(rename_expr vars fields states exp,
		 check fields fld,a)
  | Mast.UNARY(uop,exp,a) ->
      Mast.UNARY(uop,rename_expr vars fields states exp,a)
  | Mast.BINARY(bop,exp1,exp2,a) ->
      Mast.BINARY(bop,rename_expr vars fields states exp1,
		  rename_expr vars fields states exp2,a)
  | Mast.INDR(exp,a) -> Mast.INDR(rename_expr vars fields states exp,a)
  | Mast.EMPTY(st,a) -> Mast.EMPTY(check states st,a)
  | Mast.PRIM(fn,args,a) ->
      Mast.PRIM(rename_expr vars fields states fn,
		List.map (rename_expr vars fields states) args,
		a)
  | Mast.SCHEDCHILD(exp,a) ->
      Mast.SCHEDCHILD(rename_expr vars fields states exp,a)
  | Mast.IN(exp,st,a) ->
      Mast.IN(rename_expr vars fields states exp,check states st,a)
  | e -> e (* nothing to rename *)

let rename_decls vars fields states decls =
  let (vars,decls) =
    List.fold_left
      (function (vars,decls) ->
	function
	    Mast.VALDEF(Mast.VARDECL(req,ty,id,imp,a),exp,cst,a1) ->
	      let new_exp = rename_expr vars fields states exp in
	      ((id,id)::vars,
	       (Mast.VALDEF(Mast.VARDECL(req,ty,id,imp,a),new_exp,cst,a1))
	       ::decls)
	  | Mast.SYSDEF(Mast.VARDECL(req,ty,id,imp,a),cst,a1) as x ->
	      ((id,id)::vars,x::decls)
	  | _ -> raise (Error.Error "unexpected declaration"))
      (vars,[]) decls in
  (vars,List.rev decls)

let rec rename_stmt vars fields states = function
    Mast.IF(tst,thn,els,a) ->
      Mast.IF(rename_expr vars fields states tst,
	      rename_stmt vars fields states thn,
	      rename_stmt vars fields states els,
	      a)
  | Mast.FOR(id,st,dir,body,a) ->
      let new_st =
	match st with
	  None -> None
	| Some st -> Some(List.map (check states) st) in
      Mast.FOR(id,new_st,dir,
	       rename_stmt ((id,id)::vars) fields states body,
	       a)
  | Mast.SWITCH(exp,cases,def,a) ->
      Mast.SWITCH(rename_expr vars fields states exp,
		  List.map
		    (function Mast.SEQ_CASE(sts,info,body,a) ->
		      Mast.SEQ_CASE(List.map (check states) sts,info,
				    rename_stmt vars fields states body,a))
		    cases,
		  (match def with
		    None -> None
		  | Some s -> Some(rename_stmt vars fields states s)),a)
  | Mast.SEQ(decls,stms,a) ->
      let (new_vars,new_decls) = rename_decls vars fields states decls in
      Mast.SEQ(new_decls,
	       List.map (rename_stmt new_vars fields states) stms,
	       a)
  | Mast.RETURN(Some exp,a) ->
      Mast.RETURN(Some (rename_expr vars fields states exp),a)
  | Mast.MOVE(proc,dst,auto,state_end,a) ->
      Mast.MOVE(rename_expr vars fields states proc,
		check states dst,auto,state_end,a)
  | Mast.MOVEFWD(proc,state_end,a) ->
      Mast.MOVEFWD(rename_expr vars fields states proc,
		   state_end,a)
  | Mast.ASSIGN(lexp,rexp,a) ->
      Mast.ASSIGN(rename_expr vars fields states lexp,
		  rename_expr vars fields states rexp,
		  a)
  | Mast.PRIMSTMT(fn,args,a) ->
      Mast.PRIMSTMT(rename_expr vars fields states fn,
		    List.map (rename_expr vars fields states) args,
		    a)
  | s -> s (* nothing to rename *)

(* ------------------------ Rename a module ------------------------- *)
(* To be used for the state params and for requires *)

let rename_vars vars vardecls =
  List.map
    (function
	Mast.VALDEF(Mast.VARDECL(Mast.NOT_REQUIRES,ty,id,imp,a),exp,cst,a1) ->
	  Mast.VALDEF(Mast.VARDECL(Mast.NOT_REQUIRES,ty,check vars id,imp,a),
		      rename_expr vars [] [] exp,
		      cst,a1)
      | Mast.SYSDEF(Mast.VARDECL(Mast.NOT_REQUIRES,ty,id,imp,a),cst,a1) ->
	  Mast.SYSDEF(Mast.VARDECL(Mast.NOT_REQUIRES,ty,check vars id,imp,a),
		      cst,a1)
      | d -> d)
    vardecls

let rename_fields fields field_decls =
  List.map
    (function
	Mast.VARDECL(Mast.NOT_REQUIRES,ty,id,imp,a) ->
	  Mast.VARDECL(Mast.NOT_REQUIRES,ty,check fields id,imp,a)
      |	d -> d)
    field_decls

let rename_mod_ordering vars fields states oc =
  List.map
    (function
	Mast.MOD_CRIT_ID(key,order,id,a) ->
	  (* fields seem to be favored over vars by type.ml *)
	  let new_id = check2 fields vars id in
	  Mast.MOD_CRIT_ID(key,order,new_id,a)
      | Mast.MOD_CRIT_TEST(key,order,tst,thn,els,a) ->
	  Mast.MOD_CRIT_TEST(key,order,
			     rename_expr vars fields states tst,
			     rename_expr vars fields states thn,
			     rename_expr vars fields states els,a))
    oc

let rename_admit vars fields states = function
    None -> None
  | Some(Mast.MOD_ADMIT(locals,crit_params,crit,att,det,a)) ->
      let new_vars =
	(List.map (function id -> (id,id)) (Mast.valdefs2ids locals))@vars in
      let new_crit =
	rename_expr
	  ((List.map (function id -> (id,id)) (Mast.params2ids crit_params))@
	   new_vars)
	  fields states crit in
      let attach_detach = function
	  None -> None
	| Some ((Mast.VARDECL(_,_,id,_,_) as param),code) ->
	    Some(param,rename_stmt ((id,id)::new_vars) fields states code) in
      Some(Mast.MOD_ADMIT(locals,crit_params,new_crit,
			  attach_detach att,attach_detach det,a))

let rename_aspects vars fields states aspects =
  List.map
    (function
	Mast.ASPECT(srcs,dsts,(Mast.VARDECL(_,_,id,_,_) as param),pos,body,a)->
	  Mast.ASPECT(srcs,dsts,param,pos,
		      rename_stmt ((id,id)::vars) fields states body,
		      a))
    aspects

let rename_handlers vars fields states handlers =
  List.map
    (function
	Mast.EVENT(nm,spcnms,(Mast.VARDECL(_,_,id,_,_) as param),overrideable,
		   body,a) ->
      Mast.EVENT(nm,spcnms,param,overrideable,
		 rename_stmt ((id,id)::vars) fields states body,
		 a))
    handlers

let rename_functions vars fields states functions =
  List.map
    (function Mast.FUNDEF(ty,nm,params,body,inl,a) ->
      let new_vars =
	(List.map (function id -> (id,id)) (Mast.params2ids params)) @ vars in
      Mast.FUNDEF(ty,nm,params,rename_stmt new_vars fields states body,inl,a))
    functions

(* no need to rename states,consts,globals,fields, because no one will ever
observe these things *)
let rename_module vars fields states
    (Mast.MODULE(nm,mstates,consts,types,globals,mfields,funs,
		 ordering,admit,handlers,ifunctions,functions,
		   aspects,attr)) =
  Mast.MODULE(nm,mstates,rename_vars vars consts,types,
	      rename_vars vars globals,rename_fields fields mfields,funs,
	      rename_mod_ordering vars fields states ordering,
	      rename_admit vars fields states admit,
	      rename_handlers vars fields states handlers,
	      rename_functions vars fields states ifunctions,
		rename_functions vars fields states functions,
		rename_aspects vars fields states aspects,
		attr)

