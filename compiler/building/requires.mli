val global_requires_function :
    Mast.requiresdef list -> Mast.requiresdef list -> Mast.module_def list
      -> Mast.module_def list
