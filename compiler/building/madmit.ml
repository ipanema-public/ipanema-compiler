(* Combine module admission criteria into a single admission criteria. *)

(* locals : concatenate; rename.ml will check for duplicates
   crit_params : take those that are not requires, type.ml will check for dups
   crits : collect all that are provided into a list in the order specified
           by the scheduler attach declaration
   attach/detach : pick a fresh parameter name, and rename all of the
                   code accordingly.  collect into a sequence according to
                   the order provided in the scheduler *)

module B = Objects

let error attr str =
   Error.update_error();
   Printf.printf "%s: %s\n" (B.loc2c attr) str

let nl_error str =
   Error.update_error();
   Printf.printf "%s\n" str

(* ------------------ Process individual elements ------------------ *)

let process_crit_params params =
  (* make a list of all parameters *)
  (*let all_params =
    Aux.flat_map
      (function one_params ->
	List.filter
	  (function
	      Mast.VARDECL(Mast.NOT_REQUIRES,ty,id,sys,ln) -> true
	    | Mast.VARDECL(Mast.REQUIRES,ty,id,sys,ln) -> false)
	  one_params)
      params in  -- not needed? *)
  let all_params = List.concat params in
  (* remove duplicates, ignoring where they come from *)
  let rec loop prev = function
      [] -> []
    | (Mast.VARDECL(req,ty,id,sys,ln))::rest ->
	if List.mem (ty,id) prev
	then loop prev rest
	else
	  (Mast.VARDECL(Mast.NOT_REQUIRES,ty,id,sys,ln)) ::
	  (loop ((ty,id)::prev) rest) in
  loop [] all_params

let process_attach_detach lst =
  match Aux.option_filter (function x -> x) lst with
    [] -> None
  | ((Mast.VARDECL(req,ty,id,sys,ln),s)::_) as params_code ->
      let new_id = B.fresh_idx "p" in
      let new_codes =
	List.map
	  (function
	      (Mast.VARDECL(Mast.NOT_REQUIRES,ty,id,sys,ln),code) ->
		Mrename.rename_stmt [(id,new_id)] [] [] code
	    | (Mast.VARDECL(Mast.REQUIRES,ty,id,sys,ln),code) ->
		error ln "unexpected attach/detach parameter";
		code)
	  params_code in
      Some(Mast.VARDECL(req,ty,new_id,sys,ln),Mast.SEQ([],new_codes,ln))

(* -- Check that everything that has an admit is in the scheduler -- *)

let check_admits_used modules used_admits =
  List.iter
    (function
	Mast.MODULE(nm,states,consts,types,globals,fields,funs,ordering,
		    Some(Mast.MOD_ADMIT(_,_,_,_,_,attr)),
		    handlers,ifunctions,functions,aspects,_) ->
			if not (List.mem nm used_admits)
			then error attr "unexpected admit"
      | _ -> ())
    modules

(* --- Collect elements in the order indicated in the scheduler ---- *)

let collect_elements module_names modules attr =
  check_admits_used modules module_names;
  let relevant_modules =
    Aux.option_filter
      (function mn ->
	try
	  Some
	    (List.find
	       (function
		   Mast.MODULE(nm,states,consts,types,globals,fields,funs,
			       ordering,admit,handlers,ifunctions,functions,
				 aspects,attr) -> B.ideq(mn,nm))
	       modules)
	with Not_found ->
	  (nl_error (Printf.sprintf
		      "unknown module %s mentioned in admission criteria"
		      (B.id2c mn));
	  None))
      module_names in
  let (locals,crit_params,crit,att,det) =
    List.fold_left
      (function (locals,crit_params,crit,att,det) ->
	function
	    Mast.MODULE(nm,states,consts,types,globals,fields,funs,ordering,
			admit,handlers,ifunctions,functions,aspects,_) ->
	      match admit with
		None ->
		  error attr
		    (Printf.sprintf
		       "scheduler admission criteria refers to a module %s\nthat has no admission criteria"
		       (B.id2c nm));
		  (locals,crit_params,crit,att,det)
	      | Some(Mast.MOD_ADMIT(locals1,crit_params1,crit1,att1,det1,a)) ->
		  (locals1::locals,crit_params1::crit_params,crit1::crit,
		   att1::att,det1::det))
      ([],[],[],[],[]) relevant_modules in
  let locals = List.concat(List.rev locals) in
  let crit_params = process_crit_params(List.rev crit_params) in
  let crit = List.rev crit in
  let att = process_attach_detach(List.rev att) in
  let det = process_attach_detach(List.rev det) in
  Mast.ADMIT(locals,crit_params,crit,att,det,attr)

(* -------------------------- Entry point --------------------------- *)

let process modules = function
    Mast.IMPORT_ADMIT(mns,a) -> collect_elements mns modules a
  | Mast.NO_ADMIT(a) ->
      check_admits_used modules [];
      Mast.NO_ADMIT(a)
  | _ -> raise (Error.Error "madmit: unexpected admit")
