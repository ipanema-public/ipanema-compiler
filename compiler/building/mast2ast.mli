val state : Mast.state -> Ast.state
val cstate : Mast.cstate -> Ast.cstate
val valdef : Mast.valdef -> Ast.valdef
val typedef : Mast.typedef -> Ast.typedef
val extfun : Mast.fundecl -> Ast.fundecl
val vardecl : Mast.vardecl -> Ast.vardecl
val stmt : Mast.stmt -> Ast.stmt
val event_decl : Mast.event_decl -> Ast.event_decl
val function_decl : Mast.fundef -> Ast.fundef
val oc : Mast.criteria -> Ast.criteria
val aspect : Mast.aspect_decl -> Ast.aspect_decl
val admit : Mast.admit -> Ast.admit option

