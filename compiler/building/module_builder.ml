(********************************************************************
*  builder prend en param�tre le scheduler et une liste de modules 
*********************************************************************)
let builder modules = 
  let sched =
    Aux.option_filter
      (function Mast.SCHED(s) -> Some s | _ -> None) modules in
  let modules =
    Aux.option_filter
      (function Mast.MODL(m) -> Some m | _ -> None) modules in
  match sched with
    [Mast.SCHED_MODULE(n,d,s,cs,u,g,p,c,adm,h,f,ln)] ->
(*      (Order_crit.extract_oc c); *)
      let (handler_fv_env,interface_fv_env) = Variables.process modules in
      List.iter Timercheck.timer_check modules;
      let modules = Signature.verif_signature u s modules in
      let modules = Requires.global_requires_function g p modules in
      let criteria = Order_crit.verif_order_crit_param c modules in
      let admit = Madmit.process modules adm in
      let aspects =
	Aux.flat_map
	  (function
	      Mast.MODULE(nm,states,constants,types,glbls,field,extfuns,
			  criteria,admit,handlers,inter,fns,aspects,a) ->
			    aspects)
	  modules in
      let constants =
	Aux.flat_map
	  (function
	      Mast.MODULE(nm,states,constants,types,glbls,field,extfuns,
			  criteria,admit,handlers,inter,fns,aspects,a) ->
			    List.map Mast2ast.valdef constants)
	  modules in
      let types = Types.collect_types modules in
      let external_functions =
	Aux.union_list
	  (Aux.flat_map
	     (function
		 Mast.MODULE(nm,states,constants,types,glbls,field,extfuns,
			     criteria,admit,handlers,inter,fns,aspects,a) ->
			       List.map Mast2ast.extfun extfuns)
	     modules) in
      let globals =
	let tmp =
	  Aux.flat_map
	    (function
		Mast.MODULE(nm,states,constants,types,glbls,field,extfuns,
			    criteria,admit,handlers,inter,fns,aspects,a) ->
		  List.filter
		    (function
			Mast.VALDEF(Mast.VARDECL(Mast.NOT_REQUIRES,_,_,_,_),
				    _,ic,a)
		      | Mast.SYSDEF(Mast.VARDECL(Mast.NOT_REQUIRES,_,_,_,_),
				    ic,a) -> true
		      | _ -> false)
		    glbls)
	    modules in
	List.map Mast2ast.valdef tmp in
      let attributes =
	let tmp =
	  Aux.flat_map
	    (function
		Mast.MODULE(nm,states,constants,types,glbls,field,extfuns,
			    criteria,admit,handlers,inter,fns,aspects,a) ->
		  List.filter
		    (function
			Mast.VARDECL(Mast.NOT_REQUIRES,_,_,_,_) -> true
		      | _ -> false)
		    field) 
	    modules in
	List.map Mast2ast.vardecl tmp in
      Ast.SCHEDULER(Objects.id2c n,d,constants (* constants *),
		    List.map Mast2ast.typedef types,
		    [] (* importing fns from parent not yet supported *),
		    attributes,external_functions,globals,
            List.map Mast2ast.state s,
            List.map Mast2ast.cstate cs,
		    List.map Mast2ast.oc criteria,
		    Mast2ast.admit admit, Ast.NOTRACE,
		    List.map Mast2ast.event_decl
		      (Super.verif_sched_hd_construction h
			 handler_fv_env modules),
		    List.map Mast2ast.function_decl
		      (Super.verif_sched_fn_construction f
			 interface_fv_env modules),
		    [],
		    Ast.INITASPECTS(List.map Mast2ast.aspect aspects),
		    ln)
  | _ -> raise (Error.Error "exactly one scheduler should be provided")
