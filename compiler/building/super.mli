val verif_sched_hd_construction :
    Mast.handlerdef list -> Variables.handler_fv_env ->
      Mast.module_def list -> Mast.event_decl list
val verif_sched_fn_construction :
    Mast.functiondef list -> Variables.interface_fv_env ->
      Mast.module_def list -> Mast.fundef list
