(***************************
*  Traitement des requires 
*****************************)

module B = Objects

let error attr str =
  Error.update_error();
  Printf.printf "%s: %s\n" (B.loc2c attr) str

let pr = Printf.sprintf

(* ------------------------------------------------------------------ *)

(* step 1 verification que toutes les variables requises sont affect�es et que
* toutes les variables affect�es sont requises
* *)

(* ENTRE : liste de MODULES
*  SORTIE: liste de couple [(n,v,r,t,ln),...] pour les variables de types
*  process
*  ou n = nom de module 
*  et v = nom de varible
*  et r = REQUIRES | NOT_REQUIRES
*  et t = type de la variable
*  et ln = num�ro de ligne
*)
let lmodsp modules = 
  List.flatten
    (List.map
       (function 
	   Mast.MODULE(n,_,_,_,_,lp,_,_,_,_,_,_,_,_) -> 
             (Order_crit.extract_module_process n lp))
       modules)
    
(* ENTRE : liste de MODULES
*  SORTIE: liste de couple [(n,v,t,ln),...] pour les variables de types
*  process qui sont requises
*  ou n = nom de module 
*  et v = nom de varible
*  et t = type de la variable
*  et ln = num�ro de ligne
*)
let extract_requires_module_process modules =
  Aux.option_filter (function 
      (n,v,Mast.REQUIRES,t,ln) -> Some (n,v,t,ln)
    | _ -> None) (lmodsp modules) (*[ (m,v,r,t,ln),(m,v,r,t,ln)...]*)
    

(* ENTRE : liste de MODULES
*  SORTIE: liste de couple [(n,v,r,t,ln),...] pour les variables globales 
*  ou n = nom de module 
*  et v = nom de varible
*  et r = REQUIRES | NOT_REQUIRES
*  et t = type de la variable
*  et ln = num�ro de ligne
* *)
let lmodsg modules =
    List.flatten
    (List.map
       (function 
	   Mast.MODULE(n,_,_,_,lg,_,_,_,_,_,_,_,_,_) -> 
             (Order_crit.extract_module_global n lg))
       modules)
    
(* ENTRE : liste de MODULES
*  SORTIE: liste de couple [(n,v,r,t,ln),...] pour les variables globales qui
*  sont requisent 
*  ou n = nom de module 
*  et v = nom de varible
*  et t = type de la variable
*  et ln = num�ro de ligne
* *)
let extract_requires_module_global modules =
  Aux.option_filter (function
      (n,v,Mast.REQUIRES,t,ln) -> Some (n,v,t,ln)
    | _ -> None)
    (lmodsg modules) (*[ (m,v,r,t,ln),(m,v,r,t,ln)...]*)


(* V�rifie que toutes les variables requises dans les modules sont
affect�es dans le scheduler (with) * *)
let all_rq_affected lrg lrp modules =
  let process_requires scheduler_inits required_names str =
    List.iter (function (n,v,_,ln) -> 
      if not(List.exists
	       (function Mast.REQUIRESDEF(nr,vr,_,_,_) -> nr=n && vr=v)
	       scheduler_inits)
      then
	error ln
	  (Printf.sprintf
	     "required %s %s is not provided in the scheduler declaration"
	     str (Objects.id2c v)))
      required_names in
  process_requires lrg (extract_requires_module_global modules) "global";
  process_requires lrp (extract_requires_module_process modules) "attribute"

(* V�rifie que toutes les variables affect�es dans le scheduler (with)
 * sont requises
 *  *)
let all_affected_rq lrg lrp modules =
  let module_names =
    List.map
      (function Mast.MODULE(n,_,_,_,_,_,_,_,_,_,_,_,_,_) -> n)
      modules in
  let process_affected required_names scheduler_inits str =
    List.filter
      (function Mast.REQUIRESDEF(nr,vr,_,_,ln) -> 
	if List.mem nr module_names
	then
          if List.exists (function (n,v,_,_)-> nr=n && vr=v) required_names
	  then true
          else
	    begin
	      error ln
		(Printf.sprintf
		   "variable %s provided by the scheduler is not required in the %s declaration of the module"
		   (Objects.id2c vr) str);
	      false
	    end
	else
	  begin
	    error ln
	      (Printf.sprintf
		 "unknown module %s used in requires declaration"
		 (Objects.id2c nr));
	    false
	  end)
      scheduler_inits in
  (process_affected (extract_requires_module_global modules) lrg "global",
   process_affected (extract_requires_module_process modules) lrp "attribute")

(* ------------------------------------------------------------------ *)

(* step 2 v�rification que toutes les variables requises et affect�es sont du
* m�me type 
* *)

exception Not_declared (* rhs of reads not declared *)

(* Extrait le type de la variable process 'va' d�finie dans le module 'na'*)
let extract_type_p na va modules =
  try
    let (_,_,_,rt,_) =
      List.find (function (mn,vn,_,_,_) -> (mn = na && vn = va) )
	(lmodsp modules) in
    rt
  with Not_found -> raise Not_declared

(* Extrait le type de la variable global 'va' d�finie dans le module 'na'*)
let extract_type_g na va modules =
  try
    let (_,_,_,rt,_) =
      List.find (function (mn,vn,_,_,_) -> (mn = na && vn = va) )
	(lmodsg modules) in
    rt
  with Not_found -> raise Not_declared
 
(* Extrait le type de la variable process 'v' requise et d�finie dans
le module 'n'*)
let find_type_p n v modules =
  let (_,_,rt,_)=
    List.find (function (mn,vn,_,_) -> (mn = n && vn = v) )
              (extract_requires_module_process modules) in rt


(* Extrait le type de la variable globale 'v' requise et d�finie dans
le module 'n'*)
let find_type_g n v modules =
  let (_,_,rt,_)=
    List.find (function (mn,vn,_,_) -> (mn = n && vn = v) )
      (extract_requires_module_global modules) in rt
          
(*V�rifie la concordance de type entre les variables de types process et global
* 'vr' et 'va'
* *)       
let verif_type_p lrg lrp modules =
  let process extract_type find_type decls str =
    List.iter
      (function Mast.REQUIRESDEF(nr,vr,na,va,ln) ->
	try
	  if not((extract_type na va modules) = (find_type nr vr modules))
	  then
	    error ln
	      (Printf.sprintf
		 "the type of the %s affected %s does not match the type of %s %s"
		 str (Objects.id2c nr) str (Objects.id2c na))
	with Not_declared ->
	  error ln
	    (pr "%s.%s not declared" (B.id2c na) (B.id2c va)))
      decls in
  process extract_type_g find_type_g lrg "global";
  process extract_type_p find_type_p lrp "attribute"

(* ------------------------------------------------------------------ *)

(* step 3 modification de la valeur du champs requires dans le module par sa
* valeur affect� dans le sched
* *)

(* For each module, first extract the relevant requires declarations.  If
there is any overlap among the variable names on the right-hand sides, then
give an error.  If there is any overlap with the non-requires variables of
the current module, then rename the variables of the current module in the
overlap.  Finally, rename the requires variables to the right-hand side
variables.  Do the same for fields. *)

let rename lrg lrp modules =
  List.map
    (function
	(Mast.MODULE(nm,states,consts,types,globals,fields,funs,ordering,
		    admit,handlers,ifunctions,functions,aspects,attr) as m) ->
      (* requires definitions relevant to this module *)
      let collect_relevant lst =
	List.filter
	  (function Mast.REQUIRESDEF(lmn,lvn (*left*),rmn,rvn (*right*),a) ->
	    B.ideq(nm,lmn))
	  lst in
      let relevant_g = collect_relevant lrg in
      let relevant_p = collect_relevant lrp in
      (* check that right-hand side names form a set *)
      let get_new_names lst =
	List.map (function Mast.REQUIRESDEF(_,_,_,rvn,_) -> rvn) lst in
      let new_g_ids = get_new_names relevant_g in
      let new_p_ids = get_new_names relevant_p in
      let check_set lst str =
	if not(Aux.is_set lst)
	then
	  error attr
	    (Printf.sprintf
	       "variables provided for %s requires are not disjoint" str) in
      check_set new_g_ids "global";
      check_set new_p_ids "attribute";
      (* detect overlap with existing module declarations *)
      let current_globals =
	Aux.option_filter
	  (function
	      Mast.VALDEF(Mast.VARDECL(Mast.NOT_REQUIRES,t,v,_,ln),_,_,_)
	    | Mast.SYSDEF(Mast.VARDECL(Mast.NOT_REQUIRES,t,v,_,ln),_,_) ->
		Some v
	    | _ -> None)
	  (consts @ globals) in
      let current_fields =
	Aux.option_filter
	  (function
	      Mast.VARDECL(Mast.NOT_REQUIRES,t,v,_,ln) -> Some v
	    | _ -> None)
	  fields in
      let mk_local_rename new_ids locals =
	List.map (function id -> (id,B.fresh_idx (B.id2c id)))
	  (Aux.intersect new_ids locals) in
      (* create environments to deal with any overlap *)
      let local_var_replace = mk_local_rename new_g_ids current_globals in
      let local_field_replace = mk_local_rename new_p_ids current_fields in
      (* make environments based on the requires *)
      let mk_requires_env requires =
	List.map
	  (function Mast.REQUIRESDEF(lmn,lvn (*left*),rmn,rvn (*right*),a) ->
	    (lvn,rvn))
	  requires in
      let var_requires_env = mk_requires_env relevant_g in
      let field_requires_env = mk_requires_env relevant_p in
      (* perform the substitutions *)
      Mrename.rename_module var_requires_env field_requires_env [] m)
    modules 

(* -------------------------- Entry point --------------------------- *)

let global_requires_function lrg (* globals *) lrp (* process *) modules =
  all_rq_affected lrg lrp modules;
  let (lrg,lrp) = all_affected_rq lrg lrp modules in
  verif_type_p lrg lrp modules;
  rename lrg lrp modules
