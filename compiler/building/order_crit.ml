(************************************************************************
*  V�rifier l'existance des param�tres pour ordering criteria dans le
* sched
**************************************************************************) 

(* fonction r�cursive qui extrait les noms module et variable d'une structure
* FIELD_OC.
* Elle en param�tre une variable de type Mast.expr
* Elle retourne une liste [(mn,vn),(mn1,vn1)...]
* *)

module B = Objects

let pr = Printf.sprintf

let error str =
  Error.update_error();
  Printf.printf "%s \n" str

(* ---------------- Resolve imported ordering criteria --------------- *)

(* The following treats expressions like field that probably can't occur 
in an ordering criteria, but there is another phase to check for this... *)
let rec convert_crit_exp mn = function
    Mast.VAR(i,a) -> Mast.FIELD_OC(mn,i,a)
  | Mast.FIELD(exp,i,a) -> Mast.FIELD(convert_crit_exp mn exp,i,a)
  | Mast.UNARY(u,exp,a) -> Mast.UNARY(u,convert_crit_exp mn exp,a)
  | Mast.BINARY(b,exp1,exp2,a) ->
      Mast.BINARY(b,convert_crit_exp mn exp1,convert_crit_exp mn exp2,a)
  | Mast.INDR(exp,a) -> Mast.INDR(convert_crit_exp mn exp,a)
  | Mast.PRIM(f,args,a) ->
      Mast.PRIM(convert_crit_exp mn f,List.map (convert_crit_exp mn) args,a)
  | Mast.SCHEDCHILD(exp,a) -> Mast.SCHEDCHILD(convert_crit_exp mn exp,a)
  | Mast.IN(exp,i,a) -> Mast.IN(convert_crit_exp mn exp,i,a)
  | e -> e (* either no subexps, or can't arise *)

let convert_module_crit mn ln = function
    [] -> raise (Error.Error (pr "%s: module %s has no ordering criteria"
				(B.loc2c ln) (B.id2c mn)))
  | oc ->
      List.map
	(function
	    Mast.MOD_CRIT_ID(key,order,vn,ln) ->
	      Mast.CRIT_ID(key,order,mn,vn,ln)
	  | Mast.MOD_CRIT_TEST(key,order,expr1,expr2,expr3,ln) ->
	      Mast.CRIT_TEST(key,order,
			     convert_crit_exp mn expr1,
			     convert_crit_exp mn expr2,
			     convert_crit_exp mn expr3,
			     ln))
	oc

let resolve_oc oc oc_env =
  List.iter
    (function (mn,[]) -> ()
      |	(mn,crit) ->
	  if not(List.exists
		   (function
		       Mast.IMPORT(mn1,ln) -> mn = mn1
		     | _ -> false)
		   oc)
	  then
	    error
	      (Printf.sprintf
		 "module %s has an ordering criteria, but is not mentioned in the scheduler" (B.id2c mn)))
    oc_env;
  Aux.flat_map
    (function
	Mast.IMPORT(mn,ln) ->
	  (try convert_module_crit mn ln (List.assoc mn oc_env)
	  with Not_found ->
	    raise (Error.Error (pr "%s: unknown module %s"
				  (B.loc2c ln) (B.id2c mn))))
      | x -> raise (Error.Error (pr "unexpected ordering criteria")))
    oc

let make_oc_env modules =
  List.map
    (function Mast.MODULE(nm,states,constants,types,glbls,field,extfuns,
			  criteria,admit,handlers,inter,fns,aspects,a) ->
      (nm,criteria))
    modules

(* ------------------- Check the ordering criteria ------------------- *)

let rec extract = function
    Mast.BINARY(_,exp1,exp2,_) -> (extract exp1)@(extract exp2)
  | Mast.UNARY(_,exp,_) -> extract exp
  | Mast.IN(exp,_,_) -> extract exp
  | Mast.FIELD(exp,_,_) -> extract exp
  | Mast.INDR(exp,_) -> extract exp
  | Mast.PRIM(exp1,expl,_) -> (extract exp1)@(Aux.flat_map extract expl)
  | Mast.SCHEDCHILD(exp,_) -> extract exp
  | Mast.FIELD_OC(mn,vn,_) -> [(mn,vn)]
  | _ -> []
	
(* Cette fonction extrait pour tout type d'ordering criteria les couples
   * (nom_module,nom_variable) exprim�s en son sein.
   * elle prend en entr� une liste d'ordering_criteria (Mast.criteria list)
   * elle retourne une liste sans doublon de type:
   * [(nm,vn),(nm,vn),(nm,vn),...]
   * *)
let extract_oc loc =
  Aux.union_list( 
  Aux.flat_map
    (function 
	Mast.CRIT_ID(_,_,mn,vn,_) -> [(mn,vn)]
      | Mast.CRIT_TEST(_,_,expr1,expr2,expr3,_)-> 
          (extract expr1)@(extract expr2)@(extract expr3)
      |	Mast.IMPORT(_,_) ->
	  raise (Error.Error "unexpected import"))
    loc)
    
(* Cette fonctionne formate � partir d'un nom de module et de sa liste de
   * vardecl les d�clarations des variables du types process sous forme
   * d'une liste:
   * [(mn,v,r),(mn,v,r),...]
   * ou mn = nom du module ou se trouve la d�claration (type id)
   * et v = nom de la variable
   * et r = REQUIRES | NOT_REQUIRES
   * *)
let extract_module_process mn lvd = 
  List.map ( function Mast.VARDECL(r,t,v,_,ln) -> (mn,v,r,t,ln) )
    lvd
(* Cette fonctionne formate � partir d'un nom de module et de sa liste de
   * valdef les d�clarations des variables globales sous forme d'une liste:
   * [(mn,v,r),(mn,v,r),...]
   * ou mn = nom du module ou se trouve la d�claration (type id)
   * et v = nom de la variable
   * et r = REQUIRES | NOT_REQUIRES
   * *)           
let extract_module_global mn lvd = 
  List.map ( function 
      Mast.VALDEF(Mast.VARDECL(r,t,v,_,ln),_,_,_) -> (mn,v,r,t,ln)
    | Mast.SYSDEF(Mast.VARDECL(r,t,v,_,ln),_,_) -> (mn,v,r,t,ln)
    | Mast.DUMMYDEF(_,_,ln) ->
	raise (Error.Error
		 (pr "%s: you can not use this expression here"
		    (B.loc2c ln))))
    lvd
					
					
let final_extract_process (Mast.MODULE(n,_,_,_,_,lp,_,_,_,_,_,_,_,_)) =
  (extract_module_process n lp)
	
let final_extract_global (Mast.MODULE(n,_,_,_,lg,_,_,_,_,_,_,_,_,_)) =
  (extract_module_global n lg)
	
(* Cette fonction extrait pour tout chaque module toutes les variables
   * globales et de process exprim�es en son sein.
   * elle prend en entr� une liste de modules
   * elle retourne une liste sans doublon de type:
   * [(nm,vn,r),(nm,vn,r),(nm,vn,r),...]
   * ou mn = nom de module
   * et vn = nom de la variable
   * et r = REQUIRES | NOT_REQUIRES
   * *)
let final_extract_module modules =
  Aux.union_list(
  List.flatten (
  List.map (function mods -> 
    ((final_extract_process mods)@(final_extract_global mods)))
    modules))

(* ----------------------- Check for duplicates ---------------------- *)
(* only looks at crit_id, not at expressions *)

let check_for_duplicates loc =
  let variables =
    Aux.option_filter
      (function
	  Mast.CRIT_ID(_,_,mn,vn,ln) -> Some ((mn,vn),ln)
	| _ -> None)
      loc in
  let rec loop = function
      [] -> ()
    | ((_,vn) as mnvn,ln)::rest ->
	(try
	  let ln1 = List.assoc mnvn rest in
	  raise (Error.Error (pr "duplicate criteria %s at %s and %s"
				(B.id2c vn) (B.loc2c ln) (B.loc2c ln1)))
	with Not_found -> ());
	loop rest in
  loop variables

(* --------------------------- Entry point --------------------------- *)
    
(* fonction principale qui renvoie TRUE si tous les arguements de
   * l'ordering_crit�ria faisant reference � un module existent 
   * *)
    
let verif_order_crit_param loc modules =
  let new_oc = resolve_oc loc (make_oc_env modules) in
  check_for_duplicates new_oc;
  new_oc
