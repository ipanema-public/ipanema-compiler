(* have to check whether the use of a queue is compatible with respect to
its local declaration and not with respect to its global declaration.  This
is for sorted lifo or sorted fifo and the use of such a queue with the
.head or .tail field *)

(********************************************************** 
* V�rification des signatures dans la d�claration uses
***********************************************************
*
* En entr�: uses et state pour le sched et string et state pour chaque module
*
* En sortie: Concordance ou pas 
*
**********************************************************)

module B = Objects
module CS = Class_state

let error attr str =
  Error.update_error();
  Printf.printf "%s: %s\n" (B.loc2c attr) str

(* ------------------------ Process uses list ----------------------- *)

(* Transformation du uses en state � partir
* des states d�clar� dans le scheduler
* avec en plus conservation du nom de module et v�rification de la concordance
* entre les variable utilis�es (dans uses) et celle d�clar�es (dans state)
* en sorti j'ai une tuple du type:
* (mn,[state,state,state])
*  *)
let signature (Mast.USES(n,lsn,ln)) states =
  let state_list =
    List.map
      (function id -> 
	try
	  List.find
	    (function 
		Mast.QUEUE(_,_,n,_,_,_) -> id = n
	      | Mast.PROCESS(_,n,_,_,_,_) -> id = n)
            states
	with Not_found -> 
	  raise (Error.Error 
		   (Printf.sprintf "%s: unknown state in uses argument"
		      (B.loc2c ln))))
      lsn in
  if not(Aux.is_set state_list)
  then error ln "uses list passes a state more than once";
  (n,state_list)
        
        
(* Cette fonction renvoie une liste du type:
   *[ (nm,[state,state,state]),(nm,[state,...]),...]
   * *)
let res_signature uses state_list = 
  List.map
    (function u -> (signature u state_list) )
    uses

(* ---------------- Compare uses to module definition --------------- *)
		      
let verif_signature uses state modules =
  let env = res_signature uses state in
  List.map
    (function (Mast.MODULE(mn,ls,_,_,_,_,_,_,_,_,_,_,_,a) as m) ->
      let provided =
	try
	  List.assoc mn env
	with Not_found ->
	  raise (Error.Error
		   (Printf.sprintf "No information about module %s"
		      (B.id2c mn))) in
      if not(List.length ls = List.length provided)
      then raise (Error.Error
		    (Printf.sprintf "%s: wrong number of args" (B.loc2c a)));
      let new_env =
	Aux.option_filter
	  (function
	      (Mast.QUEUE(cls,order,id,vis,share,attr),
	       Mast.QUEUE(cls1,order1,id1,vis1,share1,attr1)) ->
		 let compatible_order =
		   match (order(*declared*),order1(*provided*)) with
		     (CS.SELECT(CS.NODEFAULT),CS.SELECT(_)) -> true
		   | (_,CS.SELECT(_)) ->
		       (* actually anything matches select; the module
			  just can't use "select()" on the state if select
			  is not declared *)
		       true
		   | _ -> order = order1 in
		 if cls = cls1 && compatible_order && vis = vis1
		 then
		   if B.ideq(id,id1)
		   then None
		   else Some (id,id1)
		 else
		   (error attr
		      (Printf.sprintf "parameter mismatch on %s" (B.id2c id));
		    None)
	    | (Mast.PROCESS(cls,id,prev,vis,share,attr),
	       Mast.PROCESS(cls1,id1,prev1,vis1,share1,attr1)) ->
		 if cls = cls1 && prev = prev1 && vis = vis1
		 then
		   if B.ideq(id,id1)
		   then None
		   else Some (id,id1)
		 else
		   (error attr
		      (Printf.sprintf "parameter mismatch on %s" (B.id2c id));
		    None)
	    | (Mast.QUEUE(cls,order,id,vis,share,attr),
	       Mast.PROCESS(cls1,id1,prev1,vis1,share1,attr1)) ->
		 (error attr "queue parameter declared, process provided";
		  None)
	    | (Mast.PROCESS(cls,id,prev,vis,share,attr),
	       Mast.QUEUE(cls1,order1,id1,vis1,share1,attr1)) ->
		 (error attr "process parameter declared, queue provided";
		  None))
	  (List.combine ls provided) in
      match new_env with
	[] -> m
      |	_ -> Mrename.rename_module [] [] new_env m)
    modules
