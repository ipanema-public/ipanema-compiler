(* For every unblock.timer.X handler, X has to be declared either as an
attribute or as a global variable *)

module B = Objects

let error mn str =
  Error.update_error();
  Printf.printf "%s: %s \n" (B.id2c mn) str

let pr = Printf.sprintf

let check_handlers mn valdefs procdecls handlers =
  let timer_handlers =
    Aux.option_filter
      (function
	  Mast.EVENT(Mast.EVENT_NAME([unblock;timer;timer_name],false,a),
		    specd,param,overrideable,stm,attr)
	  when B.id2c unblock = "unblock" && B.id2c timer = "timer" ->
	    Some timer_name
	| _ -> None)
      handlers in
  let timers_with_targets =
    Aux.option_filter
      (function
	  Mast.VARDECL(_,B.TIMER(_),id,_,_) -> Some id
	| _ -> None)
      procdecls in
  let timers_without_targets =
    Aux.option_filter
      (function
	  Mast.SYSDEF(Mast.VARDECL(_,B.TIMER(_),id,_,_),_,_) -> Some id
	| _ -> None)
      valdefs in
  let timers = Aux.union timers_with_targets timers_without_targets in
  List.iter
    (function timer ->
      if not (List.mem timer timer_handlers)
      then error mn (pr "no handler provided for timer %s" (B.id2c timer)))
    timers;
  List.iter
    (function timer_handler ->
      if not (List.mem timer_handler timers)
      then error mn (pr "no timer declaration provided for timer handler %s"
		       (B.id2c timer_handler)))
    timer_handlers
  
let timer_check
    (Mast.MODULE(mn,_,_,_,globals,attributes,_,_,_,handlers,_,_,_,_)) =
  check_handlers mn globals attributes handlers
