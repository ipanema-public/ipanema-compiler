(* Ensure that references to non-local variables actually refer to global
variables defined in the module and that references to fields actually
refer to fields defined in the module.  For each handler and interface
function (ie thing that can be included using super, collect a list of the
non-local variables referenced by the handler or interface function.  These
will be used in super.ml to ensure that such variables are not captured by
local declarations created by the including handler or interface
function.

This analysis is repeated in the type checking phase, but it has to be done
here separately for the modules so that we can detect that the module in
which a reference occurs is the same as the module in which the declaration
occurs. *)

module B = Objects
module CS = Class_state

(* ----- Collect read and written variables, fields, and states ---- *)

let error ln str =
  Error.update_error();
  Printf.printf "%s: %s \n" (B.loc2c ln) str

(* class env maps classes to local states only in this phase, so unbound
states are mapped to [] *)
let make_class_env states =
  let init_env = List.map (function cls -> (cls,[])) (CS.all_classes false) in
  let env =
    List.fold_left
      (function env ->
	function
	    Mast.QUEUE(cls,_,id,_,_,_)
	  | Mast.PROCESS(cls,id,_,_,_,_) ->
	      let rec loop = function
		  [] -> [(cls,[id])]
		| ((cls1,idlst) as cur)::rest ->
		    if cls = cls1
		    then (cls1,id::idlst)::(loop rest)
		    else cur::(loop rest) in
	      loop env)
      init_env states in
  List.map
    (function (cls,idlist) -> (B.mkId(CS.safe_class2c cls),idlist))
    env

let idunion l1 l2 =
  let names2 = List.map (function (id,_) -> id) l2 in
  let rec loop = function
      [] -> l2
    | ((id,ln) as cur)::rest ->
	if List.mem id names2 then loop rest else cur::(loop rest) in
  loop l1

(* may raise not_found, in which case it's not a state *)
let get_states states clsenv id =
  if List.mem id states
  then [id]
  else List.assoc id clsenv

(* collects variables and fields read by an expression *)
let rec collect_exp env (states : B.identifier list) clsenv = function
    Mast.VAR(id,ln) ->
      if List.mem id env
      then ([],[],[])
      else
	(try
	  let expanded = get_states states clsenv id in
	  ([],[],List.map (function id -> (id,ln)) expanded)
	with Not_found -> ([(id,ln)],[],[]))
  | Mast.FIELD(exp,id,ln) ->
      let (read_var,read_field,read_state) =
	collect_exp env states clsenv exp in
      (read_var, idunion [(id,ln)] read_field, read_state)
  | Mast.UNARY(uop,exp,ln) -> collect_exp env states clsenv exp
  | Mast.BINARY(bop,exp1,exp2,ln) ->
      let (read_var1,read_field1,read_state1) =
	collect_exp env states clsenv exp1 in
      let (read_var2,read_field2,read_state2) =
	collect_exp env states clsenv exp2 in
      (idunion read_var1 read_var2,
       idunion read_field1 read_field2,
       idunion read_state1 read_state2)
  | Mast.INDR(exp,ln) -> collect_exp env states clsenv exp
  | Mast.PRIM(fn,args,ln) ->
      List.fold_left
	(function (read_var,read_field,read_state) ->
	  function x ->
	    let (read_var1,read_field1,read_state1) =
	      collect_exp env states clsenv x in
	    (idunion read_var1 read_var,
	     idunion read_field1 read_field,
	     idunion read_state1 read_state))
	(collect_exp env states clsenv fn) args
  | Mast.SCHEDCHILD(exp,ln) -> collect_exp env states clsenv exp
  | Mast.IN(exp,st,ln) ->
      let instates =
	try List.map (function id -> (id,ln)) (get_states states clsenv st)
	with Not_found -> error ln "bad state for in operation"; [] in
      let (read_var,read_field,read_state) =
	collect_exp env states clsenv exp in
      (read_var,read_field,idunion instates read_state)
  | Mast.EMPTY(st,ln) ->
      let estates =
	try List.map (function id -> (id,ln)) (get_states states clsenv st)
	with Not_found -> error ln "bad state for empty operation"; [] in
      ([],[],estates)
  | _ -> ([],[],[])

let collect_decl env states clsenv decls ln =
  List.fold_left
    (function (read_var,read_field,read_state,env) ->
      function
	  Mast.VALDEF(Mast.VARDECL(req,ty,id,sys,ln),exp,const,a) ->
	    let (read_var1,read_field1,read_state1) =
	      collect_exp env states clsenv exp in
	    (idunion read_var1 read_var,
	     idunion read_field1 read_field,
	     idunion read_state1 read_state,
	     Aux.union [id] env)
	| _ ->
	    error ln "unexpected local decl";
	    (read_var,read_field,read_state,env))
    ([],[],[],env) decls

(* collects variables and fields read or written by a statement *)
let rec collect_stm env (states : B.identifier list) clsenv = function
    Mast.IF(exp,st1,st2,ln) ->
      let (read_var1,read_field1,read_state1) =
	collect_exp env states clsenv exp in
      let (read_var2,read_field2,read_state2,write_var2,write_field2) =
	collect_stm env states clsenv st1 in
      let (read_var3,read_field3,read_state3,write_var3,write_field3) =
	collect_stm env states clsenv st2 in
      (idunion read_var1 (idunion read_var2 read_var3),
       idunion read_field1 (idunion read_field2 read_field3),
       idunion read_state1 (idunion read_state2 read_state3),
       idunion write_var2 write_var3,
       idunion write_field2 write_field3)
  | Mast.FOR(id,sts,_,stmt,ln) ->
      let fstates =
	List.map (function id -> (id,ln))
	  (match sts with
	    None -> states
	  | Some sts ->
	      try Aux.union_list(Aux.flat_map (get_states states clsenv) sts)
	      with Not_found -> error ln "bad state for in operation"; []) in
      let (read_var,read_field,read_state,write_var,write_field) =
	collect_stm (id::env) states clsenv stmt in
      (read_var,read_field,idunion fstates read_state,write_var,write_field)
  | Mast.SWITCH(exp,lsc,st2,ln) ->
      let (read_var0,read_field0,read_state0) =
	collect_exp env states clsenv exp in
      let sstates = List.map (function id -> (id,ln)) states in
      let (read_var,read_field,_,write_var,write_field) =
	(* drop read_state, because all states are read by the switch test *)
	List.fold_left
	  (function (read_var,read_field,read_state,write_var,write_field) ->
	    function Mast.SEQ_CASE(_,_,st1,_) ->
	      let (read_var1,read_field1,read_state1,write_var1,write_field1) =
		collect_stm env states clsenv st1 in
	      (idunion read_var1 read_var,
	       idunion read_field1 read_field,
	       idunion read_state1 read_state,
	       idunion write_var1 write_var,
	       idunion write_field1 write_field))
	  (match st2 with
	    None -> ([],[],[],[],[])
	  | Some st -> collect_stm env states clsenv st)
	  lsc in
      (idunion read_var0 read_var,idunion read_field0 read_field,
       sstates,write_var,write_field)
    | Mast.SEQ(decls,lstmt,ln) ->
      let (read_var,read_field,read_state,env) =
	collect_decl env states clsenv decls ln in
      List.fold_left
	(function (read_var,read_field,read_state,write_var,write_field) ->
	  function x ->
	    let (read_var1,read_field1,read_state1,write_var1,write_field1) =
	      collect_stm env states clsenv x in
	    (idunion read_var1 read_var,
	     idunion read_field1 read_field,
	     idunion read_state1 read_state,
	     idunion write_var1 write_var,
	     idunion write_field1 write_field))
	(read_var,read_field,read_state,[],[]) lstmt
  | Mast.RETURN(Some exp,ln) ->
      let (read_var,read_field,read_state) =
	collect_exp env states clsenv exp in
      (read_var,read_field,read_state,[],[])
  | Mast.ASSIGN(lexp,rexp,ln) ->
      let (read_var1,read_field1,read_state1) =
	collect_exp env states clsenv rexp in
      let (read_var,read_field,read_state,write_var,write_field) =
	match lexp with
	  Mast.VAR(id,a) ->
	    if List.mem id env
	    then ([],[],[],[],[])
	    else ([],[],[],[(id,a)],[])
	| Mast.FIELD(exp,id,a) ->
	    let (read_var2,read_field2,read_state2) =
	      collect_exp env states clsenv exp in
	    (read_var2,read_field2,read_state2,[],[(id,a)])
	| _ -> error ln "unexpected left-hand side expression";
	    ([],[],[],[],[]) in
      (idunion read_var read_var1,idunion read_field read_field1,
       idunion read_state read_state,write_var,write_field)
  | Mast.PRIMSTMT(fn,args,ln) ->
      let (read_var,read_field,read_state) =
	List.fold_left
	  (function (read_var,read_field,read_state) ->
	    function x ->
	      let (read_var1,read_field1,read_state1) =
		collect_exp env states clsenv x in
	      (idunion read_var1 read_var,
	       idunion read_field1 read_field,
	       idunion read_state1 read_state))
	  (collect_exp env states clsenv fn) args in
      (read_var,read_field,read_state,[],[])
  | Mast.MOVE(src,dst,a,b,attr) ->
      let (read_var,read_field,read_state) =
	collect_exp env states clsenv src in
      (read_var,read_field,read_state,[],[])
  | Mast.MOVEFWD(src,y,a) ->
      let (read_var,read_field,read_state) =
	collect_exp env states clsenv src in
      (read_var,read_field,read_state,[],[])
  | _ -> ([],[],[],[],[]) (* can't occur or no expression subterms *)

(* ------ Check read and written variables, fields, and states ----- *)

(* free vars for handlers *)
type handler_fv_env =
    ((Objects.identifier * Mast.event_name) *
       (Objects.identifier * Objects.attribute) list) list

(* free vars for interface functions *)
type interface_fv_env =
    ((Objects.identifier * Objects.identifier) *
       (Objects.identifier * Objects.attribute) list) list

let params2env params =
  List.map (function Mast.VARDECL(req,ty,id,sys,ln) -> id) params

let check vars fields states free_vars free_fields free_states =
    Printf.printf "Check" ;
    let check_one str declared referenced =
    List.iter
      (function (id,ln) ->
	if not(List.mem id declared)
	then error ln ("unknown "^str^" "^(B.id2c id)))
      referenced in
  check_one "variable" vars free_vars;
  check_one "attribute" fields free_fields;
  check_one "state" states free_states

let check_handler mn vars fields states clsenv
    (Mast.EVENT(nm,spcnms,param,overrideable,body,a)) =
  let (read_var,read_field,read_state,write_var,write_field) =
    collect_stm (params2env [param]) states clsenv body in
  let free_vars = idunion read_var write_var in
  check vars fields states free_vars (idunion read_field write_field)
    read_state;
  ((mn,nm),free_vars)

(* The free variable information collected here is only used for interface
functions.  Free variables are collected again on the Ast for ordinary
functions, which might not have passed through the module system, to check
inlining *)
let check_function mn vars fields states clsenv
    (Mast.FUNDEF(ty,nm,params,body,inl,a)) =
  let (read_var,read_field,read_state,write_var,write_field) =
    collect_stm (params2env params) states clsenv body in
  let free_vars = idunion read_var write_var in
  check vars fields states free_vars (idunion read_field write_field)
    read_state;
  ((mn,nm),free_vars)

let check_aspect vars fields states clsenv
    (Mast.ASPECT(srcs,dsts,param,pos,body,a)) =
  let (read_var,read_field,read_state,write_var,write_field) =
    collect_stm (params2env [param]) states clsenv body in
  check vars fields states
    (idunion read_var write_var) (idunion read_field write_field)
    read_state

(* check that all of the non-local variables, states referenced by the
admission criteria are actually defined *)
let check_admit vars fields states clsenv = function
    Some(Mast.MOD_ADMIT(locals,crit_params,crit,att,det,a)) ->
      let local_ids_with_vars =
	List.fold_left
	  (function local_ids ->
	    function
		Mast.VALDEF(Mast.VARDECL(req,ty,id,sys,ln),exp,const,a) ->
		  let (read_var,read_field,read_state) =
		    collect_exp local_ids states clsenv exp in
		  check local_ids fields states
		    read_var read_field read_state;
		  id::local_ids
	      |	_ -> raise (Error.Error "unexpected declaration in admit"))
	  vars locals in
      let process = function
	  None -> ()
	| Some(param,stm) ->
	    let param_env = params2env [param] in
	    let (read_var1,read_field1,read_state1,write_var1,write_field1) =
	      collect_stm param_env states clsenv stm in
	    check local_ids_with_vars fields states
	      (idunion read_var1 write_var1)
	      (idunion read_field1 write_field1)
	      read_state1 in
      let (read_var,read_field,read_state) =
	let param_env = params2env crit_params in
	collect_exp param_env states clsenv crit in
      check local_ids_with_vars fields states read_var read_field read_state;
      process att;
      process det
  | None -> ()

(* -------------------------- Entry point -------------------------- *)

let process modules =
  let envs =
    List.map
      (function
	  (Mast.MODULE(nm,states,consts,types,globals,fields,funs,
		       ordering,admit,handlers,ifunctions,functions,
			 aspects,attr)) ->
	    let clsenv = make_class_env states in
	    let fields =
	      List.map (function Mast.VARDECL(req,ty,id,sys,ln) -> id)
		fields in
	    let src = B.mkId "source" in
	    let tgt = B.mkId "target" in
	    (* The problem is that to check for source and target, we have to
	       properly, we have to do type checking.  Since we would rather
	       just defer the type checking to the verifier phase, we just
	       disallow these as process attributes.  Another solution would
	       be to do the cheated analysis here, where we accept source
	       and target here but check that the module names are the same
	       when we get to the type checker. *)
	    if List.mem src fields || List.mem tgt fields
	    then
	      error attr
		("the module system currently does not allow source or "^
		 "target as a process attribute");
	    let fields = src::tgt::fields in
	    let states =
	      List.map
		(function
		    Mast.QUEUE(cls,_,id,_,_,_)
		  | Mast.PROCESS(cls,id,_,_,_,_) -> id)
		states in
	    let enums =
	      List.concat
		(Aux.option_filter
		   (function
		       Mast.ENUMDEF(ty,vals,a) -> Some vals
		     | _ -> None)
		   types) in
	    let function_names =
	      List.map (function Mast.FUNDECL(ret,nm,argtys,a) -> nm) funs in
	    let prims = List.map B.mkId (B.prim_names()) in
	    let vars =
	      List.fold_left
		(function vars ->
		  function
		      Mast.VALDEF(Mast.VARDECL(req,ty,id,sys,ln),exp,const,a)->
			(match collect_exp vars states clsenv exp with
			  (read_var,[],[]) -> check vars [] [] read_var [] []
			| _ -> error a "unexpected field or state references");
			Aux.union [id] vars
		    | Mast.SYSDEF(Mast.VARDECL(req,ty,id,sys,ln),const,a) ->
			Aux.union [id] vars
		    | _ -> error attr "unexpected declaration"; vars)
		(function_names @ enums @ prims) (consts @ globals) in
	    let handler_env =
	      List.map (check_handler nm vars fields states clsenv)
		handlers in
	    let ifunction_env =
	      List.map (check_function nm vars fields states clsenv)
	      ifunctions in
	    let _ =
	      List.map (check_function nm vars fields states clsenv)
		functions in
	    List.iter (check_aspect vars fields states clsenv) aspects;
	    check_admit vars fields states clsenv admit;
	    (handler_env,ifunction_env))
      modules in
  let (hfv,ifv) = List.split envs in
  (List.flatten hfv,List.flatten ifv)
