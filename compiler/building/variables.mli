(* free vars for handlers *)
type handler_fv_env =
    ((Objects.identifier * Mast.event_name) *
       (Objects.identifier * Objects.attribute) list) list

(* free vars for interface functions *)
type interface_fv_env =
    ((Objects.identifier * Objects.identifier) *
       (Objects.identifier * Objects.attribute) list) list

val process : Mast.module_def list -> handler_fv_env * interface_fv_env
