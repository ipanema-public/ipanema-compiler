(************************
*  Traitement des super 
**************************)

module B = Objects

let error ln str =
  Error.update_error();
  Printf.printf "%s: %s \n" (B.loc2c ln) str

let serror ln str =
  Error.update_error();
  Printf.printf "%s: %s \n" ln str

let pr = Printf.sprintf

type ('event, 'event_name, 'param) info =
    {get_name : 'event -> 'event_name;
      get_params : 'event -> 'param list;
	get_body : 'event -> Mast.stmt;
	  get_overrideable : 'event -> Mast.overrideable;
	  insert_body : 'event * 'param list * Mast.stmt -> 'event;
	    compatible : 'event_name -> 'event_name -> bool;
	      get_line : 'event_name -> string;
		comp_ext : (B.identifier * 'event_name) ->
		  (B.identifier * 'event_name) -> bool;
		    print_name : 'event_name -> string}

let event : (Mast.event_decl, Mast.event_name, Mast.vardecl) info =
  {get_name = (function (Mast.EVENT(nm,_,_,_,_,_)) -> nm);
    get_params = (function (Mast.EVENT(nm,_,params,_,_,_)) -> [params]);
    get_body = (function (Mast.EVENT(_,_,_,_,stmt,_)) -> stmt);
    get_overrideable =
    (function (Mast.EVENT(_,_,_,overrideable,_,_)) -> overrideable);
    insert_body = (function (Mast.EVENT(a,b,c,d,_,e),_,stmt) ->
      Mast.EVENT(a,b,c,d,stmt,e));
    compatible = Mast.event_name_equal;
  get_line = (function (Mast.EVENT_NAME(_,_,ln)) -> B.loc2c ln);
  comp_ext =
    (function (mn1,nm1) -> function (mn2,nm2) ->
      mn1 = mn2 && Mast.event_name_equal nm1 nm2);
  print_name = Mast.event_name2c}

let fn : (Mast.fundef, B.identifier, Mast.vardecl) info =
  {get_name = (function (Mast.FUNDEF(_,nm,_,_,_,_)) -> nm);
    get_params =
    (function (Mast.FUNDEF(_,_,params,_,_,_)) -> params);
    get_body = (function (Mast.FUNDEF(_,_,_,stmt,_,_)) -> stmt);
    get_overrideable = (function _ -> Mast.NOT_OVERRIDEABLE);
    insert_body = (function (Mast.FUNDEF(a,b,c,_,d,e), params, stmt) ->
      Mast.FUNDEF(a,b,params,stmt,d,e));
    compatible = (function x -> function y -> x = y);
  get_line = (function id -> "function "^(B.id2c id));
  comp_ext = (function mnnm1 -> function mnnm2 -> mnnm1 = mnnm2);
  print_name = B.id2c}

let find_event events event_name info =
  List.find
    (function event -> info.compatible (info.get_name event) event_name)
    events

let exists_event events event_name info other_events =
  try
    let _ = find_event events event_name info in true
  with Not_found ->
    (* search for the handler in the other list to be sure it is defined
       somewhere *)
    let _ = find_event other_events event_name info in false

(* v�rifie dans une liste de stmt 
*  si un super existe qu'il n'y en a pas d'autre apr�s...
*    si c'est le cas la fonction retourne true 
*    sinon elle soul�ve une erreur.
*  Sinon si aucun super n'est trouv� elle retourne false
*  *)
let rec test_stmt overrideable mn loc = function
  [] -> false
  | x::xs ->
      if (extract_super overrideable mn x) 
      then 
        if List.for_all extract_no_super xs
        then true
        else
	  raise
	    (Error.Error 
               (pr "%s: No more than one next() in event" loc))
      else test_stmt overrideable mn loc xs
 

and seq_case_control overrideable mns bl loc = (*function*)
  if (List.for_all (function x -> x = true) bl)
  then true
  else if (List.for_all (function x -> x = false) bl) 
  then false
  else if overrideable = Mast.OVERRIDEABLE
  then true
  else raise (Error.Error (pr "%s: next() missing in some switch cases" loc))

(* identification des supers,
* prend comme argument un stmt
* retourne true si super est trouv� false sinon
* *)
and extract_super overrideable mn = function
  Mast.IF(_,st1,st2,ln) ->
    let if1 = (extract_super overrideable mn st1) in
    let if2 = (extract_super overrideable mn st2) in 
    if (if1 = if2) 
    then if1
    else
      if overrideable = Mast.OVERRIDEABLE
      then true
      else
	raise (Error.Error 
                 (pr "%s: next() missing in some if branches" (B.loc2c ln)))

  | Mast.FOR(_,_,_,stmt,ln) ->
      if (extract_no_super stmt)
      then false
      else raise (Error.Error 
                    (pr "%s: next() not allowed in loop"
		       (B.loc2c ln)))
  | Mast.SWITCH(_,lsc,st2,ln) -> 
      let sl =
	List.map
	  (function Mast.SEQ_CASE(_,_,st1,_) ->
	    (extract_super overrideable mn st1))
	  lsc in
      let sl2 =
	(match st2 with
	  None -> sl
	| Some st -> (extract_super overrideable mn st)::sl) in
      (seq_case_control overrideable (B.id2c mn) sl2 (B.loc2c ln))
         
  | Mast.SEQ(_,lstmt,ln) -> (test_stmt overrideable mn (B.loc2c ln) lstmt) 
  | Mast.NEXT(_) -> true
  | _ -> false
  
(* identification de non pr�sence de supers,
* prend comme argument un stmt
* retourne false si super est trouv� true sinon
* *)
and extract_no_super = function
    Mast.IF(_,st1,st2,ln) ->
      let if1 = (extract_no_super st1) in
      let if2 = (extract_no_super st2) in 
      if (if1 = if2) 
      then if1
      else raise (Error.Error 
                    (pr "%s: Not super in all
                       'if' branches" (B.loc2c ln) ))
  | Mast.FOR(_,_,_,stmt,_) -> extract_no_super stmt
  | Mast.SWITCH(_,lsc,st2,_) ->
      ( List.for_all
	 (function Mast.SEQ_CASE(_,_,st1,_) -> (extract_no_super st1))
         lsc
         && 
       ( match st2 with None -> true
       | Some st -> (extract_no_super st)))
  | Mast.SEQ(_,lstmt,_) ->
      List.for_all (function x -> (extract_no_super x)) lstmt
  | Mast.NEXT(_) -> false
  | _ -> true
 

(* traitement de la liste d'handler d'un modules
* prend comme argument une liste d'handler
* retourne un tuple de 2 listes d'event, 
* la 1er contenant les events contenant NEXT [e1,e2,...]
* la 2sd contenant les events ne contenant pas NEXT
* *) 

(* the following doesn't work because hl is the list of all handlers, not
the sequence of handlers for a given event.  should rather have
extract_super classify handlers in three ways: super on all control flow
paths, super on some control flow paths, and super on no control flow
paths, and then use that information later. *)
let extract_handlers_info hl mn info = 
  let rec rest_overrideable = function
      [] -> (true,[])
    | handler::rest ->
	let (ok,l) = rest_overrideable rest in
	if ok
	then (info.get_overrideable handler = Mast.OVERRIDEABLE,
	      Mast.OVERRIDEABLE::l)
	else (false,Mast.NOT_OVERRIDEABLE::l) in
  let (_,overrideable_info) = rest_overrideable hl in
  let (with_super,with_no_super) =
    List.fold_left2
      (function (with_super,with_no_super) ->
	function x ->
	  function rest_overrideable ->
	    if extract_super rest_overrideable mn (info.get_body x)
	    then (x::with_super,with_no_super)
	    else (with_super,x::with_no_super))
      ([],[]) hl overrideable_info in
  (List.rev with_super,List.rev with_no_super)

  
(* transformation d'une liste de modules en une liste de nom de module et
* 2 listes d'evenement, l'une contenant les events NEXT et l'autre les autre
* Prend comme param�tre une liste de modules
* Retourne une liste de type:
* [(nm1,([e11,e13,...],[e12,..]) ),(nm2,([e21],[]) ),(n3,([],[e31]) )]
* ou nm = nom du module
* et eXX 1er liste est un evenement contenant NEXT
* et eXX 2sd listes est un evenement ne contenant NEXT
* *)    
let extract_handlers modules =
  List.map
    (function Mast.MODULE(mn,_,_,_,_,_,_,_,_,hl,_,_,_,_) ->
      (mn,(extract_handlers_info hl mn event)))
    modules

let extract_functions modules =
  List.map
    (function Mast.MODULE(mn,_,_,_,_,_,_,_,_,_,fl,_,_,_) ->
      (mn,(extract_handlers_info fl mn fn)))
    modules

(* Controle la concordance entre les noms d'evenement lors de la composition
* d'un event_handler. Ainsi tout nom d'event utilis� lors de la
* composition doit correspondre au nom de l'event_handler compos�.
* prend comme param�tre: la liste du scheduler des event_handlers a composer
* retourne une liste de liste du type:
* [[(nm,en),(nm,en)...],[(nm,en)],...] ou chaque sous liste repr�sente la
* composition d'un event_handler dans l'ordre.
* *)
                
let controle_handlerdef lhd =
  List.map
    (function Mast.HANDLERDEF(en,ebl,_) ->
      List.map
	(function Mast.EVENT_BODY(mn,evn,ln) ->
          if Mast.event_name_equal evn en
          then (mn,evn)
          else raise (Error.Error 
			(pr
                           "%s: Event name missmatch in Scheduler handler composition" (B.loc2c ln))))
	ebl)
    lhd

let controle_functiondef lhd =
  List.map
    (function Mast.FUNCTIONDEF(en,ebl,_) ->
      List.map
	(function Mast.FUNCTION_BODY(mn,evn,ln) ->
          if evn = en
          then (mn,evn)
          else raise (Error.Error 
                        (pr
			   "%s: Event name mismatch in Scheduler handler composition" (B.loc2c ln))))
	ebl)
    lhd

(* Controle a partir de la liste ml 
* g�n�rer a partir de l'ensemble des modules et de type:
* [(nm1,([e11,e13,...],[e12,..]) ),(nm2,(ls,lns) )...]
* ou ls est la liste des events contenant NEXT
* et lns est la liste des events ne contenant pas NEXT
* et nm = nom du module
* S'il existe un module qui a dans sa liste ls un event dont
* le nom match avec celui pass� en param�tre.
* Si oui alors elle retourne true
* Sinon elle retourne false
*)
let controle_exist_super n en m info =
  List.exists
    (function (nm,(ls(*list super, the one we want*),lns)) -> 
      if (nm = n)
      then exists_event ls en info lns
      else false)
    m

(* Controle a partir de la liste ml 
* g�n�rer a partir de l'ensemble des modules et de type:
* [(nm1,([e11,e13,...],[e12,..]) ),(nm2,(ls,lns) )...]
* ou ls est la liste des events contenant NEXT
* et lns est la liste des events ne contenant pas NEXT
* et nm = nom du module
* S'il existe un module qui a dans sa liste lns un event dont
* le nom match avec celui pass� en param�tre.
* Si oui alors elle retourne true
* Sinon elle retourne false
*)
let controle_not_exist_super n en m info =
  List.exists
    (function (nm,(ls,lns(*list no super, the one we want*))) -> 
      if (nm = n)
      then exists_event lns en info ls
      else false)
    m

             
(* 2sde phase de d�sempilage de la liste s du type:
* [[(nm,en),(nm,en)...],[(nm,en)],...]
* je dispose maintenant d'une sous liste de type:
* [(nm,en),(nm,en)...]
* Pour tous les tuples de cette liste sauf le dernier,
* je v�rifie que l'�v�nement du tuples contient bien un NEXT
* je v�rifie pour le dernier tuple que l'�v�nement ne contient pas de NEXT
* *)
let rec cxs info m = function
    [(n,en)] -> 
      (try
	if (controle_not_exist_super n en m info)
	then true
	else
	  raise (Error.Error (pr
				"%s.bossa %s: This event does not have next()"
				(B.id2c n) (info.get_line en)))
      with Not_found ->
	raise (Error.Error (pr
			      "%s.bossa %s: module does not define %s"
			      (B.id2c n) (info.get_line en)
			      (info.print_name en))))
			      
  | (n,en)::xs -> 
      (try
	if (controle_exist_super n en m info)
	then (cxs info m xs)
	else raise (Error.Error (pr
				   "%s.bossa %s: This event must have next()"
				   (B.id2c n) (info.get_line en)) )
      with Not_found ->
	raise (Error.Error (pr
			      "%s.bossa %s: module does not define %s"
			      (B.id2c n) (info.get_line en)
			      (info.print_name en))))
			      
  | [] -> true
  
(* 1�re phase de d�sempilage de la liste s du type:
* [[(nm,en),(nm,en)...],[(nm,en)],...]
* Pour chaque sous liste je v�rifie qu'il existe bien 
* une Concordance entre les eXtands et les Super => cxs 
* *)
let rec controle info m = function
    x::xs -> (cxs info m x) && (controle info m xs)
  | [] -> true

(* m is an environment mapping each module to a pair of lists ls and lns,
where ls is the list of handlers containing super and lns is the list of
handlers not containing super.  The lns handlers must be disjoint.  This is
a bit complicated because of compatibility.  We only give an error if the
names are identical.  If one is more specific than the other that seems ok,
since having two such handlers is allowed in a single policy. *)
let lns_disjoint m info =
  let rec loop prev = function
      [] -> ()
    | (nm,(ls,lns))::rest ->
	let lns = List.map info.get_name lns in
	List.iter
	  (function en ->
	    try
	      let other =
		List.find
		  (function en1 ->
	            (* the following call to info.compatible should be
		       testing equality *)
		    info.compatible en en1)
		  prev in
	      serror (pr "%s, %s" (info.get_line en) (info.get_line other))
		(pr "multiple definitions for %s with no next()"
		   (info.print_name en))
	    with Not_found -> ())
	  lns;
	loop (lns@prev) rest in
  loop [] m

(* m is as described above.  s is the information from the scheduler.  it
consists of a list of lists where each element has the form module name x
event name.  For each ls element of m, there has to be some module name x
event name in s such that module name is the same as that associated with
the ls element and the ls element is compatible with event name *)
let ls_in_scheduler m s info =
  List.iter
    (function (nm,(ls,lns)) ->
      List.iter
	(function en ->
	  let enn = info.get_name en in
	  let in_sched =
	    List.exists
	      (function composition ->
		List.exists
		  (function (nm1,enn1) ->
		    (* info.compatible should let enn be more general than
		       enn1? *)
		    nm = nm1 && info.compatible enn enn1)
		  composition)
	      s in
	  if not in_sched
	  then
	    serror (info.get_line enn)
	      (pr "handler %s with next() not listed in scheduler"
		 (info.print_name enn)))
	ls)
    m

let rec concat_ls = function
    [(_,(ls,_))] -> ls
  | (_,(ls,_))::xs -> ls@(concat_ls xs)
  | [] -> []

let rec concat_lns = function
    [(_,(_,lns))] -> lns
  | (_,(_,lns))::xs -> lns@(concat_lns xs)
  | [] -> []

(* retourne tous les event n'intervenant pas dans une construction de super
*  elle prend la liste des event contenant des super (ls) et 
*  celle des events ne contenant pas de super (lns).
*  Ce nettoyage est n�cessaire pour retirer de lns les event terminaux (ne
*  poss�dant pas de super) intervenant dans une construction d'event.
*  *)
let clean_lns ls lns info =
  List.filter
    (function no_super ->
      let ens = info.get_name no_super in
      not (exists_event (Aux.union_list ls) ens info lns))
    lns

(*extrait a partir de m les events dont les noms concorde avec les noms de
* module et d'event de la liste hss, �l�ment de la liste s.
* Cette fonction retourne une liste de type:
* [e,e,e,e,...] qui repr�sente le contruction 
* d'un event dans le scheduler
* *)
let rec extract_event m info = function
  [(mn,en)] ->
    let (_,(_,elt)) = (List.find (function (n,(_,_)) -> n = mn) m) in
    [find_event elt en info]
  | (mn,en)::xs ->
      let (_,(elt,_)) = (List.find (function (n,(_,_)) -> n = mn) m) in
      (find_event elt en info)::(extract_event m info xs)
  | [] -> []


(* Like List.assoc, but with with a function for making the comparison *)
let rec fnassoc f key = function
    [] -> raise Not_found
  | (key1,vl)::rest -> if f key key1 then vl else fnassoc f key rest

(* fonction auxilaire*)
let rec slsc xs decl_env fv_env ext_seq comp_ext lsc = 
  List.map
    (function Mast.SEQ_CASE(a,b,st1,c) -> 
      Mast.SEQ_CASE(a,b,(find_super_replace xs decl_env fv_env ext_seq comp_ext st1),c))
    lsc
(* reconstruit le statement de chaque event ayant un super
* prend en entr�e une liste d'event et un statement
* retourne le statement d'entr�e dans lequel super() a �t� remplac� par le code
* correspondant
*  *)
and find_super_replace xs decl_env fv_env ext_seq comp_ext = function
  Mast.IF(a,st1,st2,b) ->
    Mast.IF(a,(find_super_replace xs decl_env fv_env ext_seq comp_ext st1),
	    (find_super_replace xs decl_env fv_env ext_seq comp_ext st2),b)
  | Mast.SWITCH(a,lsc,st2,b) ->
      Mast.SWITCH(a,(slsc xs decl_env fv_env ext_seq comp_ext lsc),
		  ( match st2 with None -> None
                  | Some st -> Some(find_super_replace xs decl_env
				      fv_env ext_seq comp_ext st)),
		  b)
  | Mast.SEQ(a,lstmt,b) -> 
      let new_decl_env =
	(List.map
	   (function
	       Mast.VALDEF(Mast.VARDECL(req,ty,id,sys,ln),exp,const,a) -> id
	     | _ -> raise (Error.Error "unexpected local decl"))
	   a)
	  @ decl_env in
      Mast.SEQ(a,(List.map
		    (function stmt ->
		      find_super_replace xs new_decl_env fv_env ext_seq comp_ext stmt)
		    lstmt),
	       b)
  | Mast.NEXT(_) -> super_replace decl_env fv_env ext_seq comp_ext xs
  | s -> s (* NB: super not allowed in for *)

(* fonction principale pour la reconstruction d'un event
 * prend en entr�e une liste de statement et retourne un statement*)
and super_replace decl_env fv_env ext_seq comp_ext stmts =
  match (ext_seq,stmts) with
   ([(mn,nm)],[stmt]) -> stmt
   | ((mn,nm)::exts,stmt::xs) ->
       let handler_fvs = fnassoc comp_ext (mn,nm) fv_env in
       B.capture_check handler_fvs decl_env (error (Mast.get_stm_attr stmt));
       find_super_replace xs decl_env fv_env exts comp_ext stmt
   | _ ->
       raise
	 (Error.Error 
            (pr "Do not build satement without statement list"))


let make_event_disjoint params name = List.hd params

let make_function_disjoint params name =
  (* start with the parameters of the extended module, not the extending
     module *)
  let params = List.rev params in
  let new_params =
    List.fold_left
      (function collect_params ->
	function current_params ->
	  let new_collect_params =
	    Aux.option_filter
	      (function 
		  Mast.VARDECL(Mast.REQUIRES,ty,i,imp,ln) ->
		    (try
		      let (defty,defimp,def) = List.assoc i collect_params in
		      if not (defty = ty) || not (defimp == imp)
		      then 
			raise
			  (Error.Error
			     (pr
				"%s: %s: incompatible properties of requires parameter %s"
				(B.loc2c ln) (B.id2c name) (B.id2c i)))
		      else None
		    with Not_found ->
		      raise
			(Error.Error
			   (pr
			      "%s: %s: undeclared requires parameter %s"
			      (B.loc2c ln) (B.id2c name) (B.id2c i))))
		| Mast.VARDECL(Mast.NOT_REQUIRES,ty,i,imp,ln) as x ->
		    try
		      let _ = List.assoc i collect_params in
		      raise
			(Error.Error
			   (pr "%s: %s: duplicated parameter %s"
			      (B.loc2c ln) (B.id2c name) (B.id2c i)))
		    with Not_found -> Some (i,(ty,imp,x)))
	      current_params in
	  (List.rev new_collect_params) @ collect_params)
      [] params in
  List.map (function (_,(_,_,decl)) -> decl) (List.rev new_params)
    
let build_super_event s m info make_disjoint fv_env comp_ext =
  let list_list_event =
    (List.map (function hss -> (extract_event m info hss)) s) in
  (* [ [e,e,e],[e,e],...]*)
  let list_list_stmt =
    (List.map
       (function list_event -> 
         ((List.hd list_event),
          (List.map info.get_params list_event),
          (List.map info.get_body list_event)))
       list_list_event) in
    (* [ (e,[param,param],[stmt,stmt,stmt]),(e,[param],[stmt,stmt]),...]*)
  let list_final_event =
    (List.map2
       (function extend_sequence ->
	 function (e,list_param,list_stmt) ->
	   let name = info.get_name e in
	   (*let new_params = List.flatten list_param in   -- not needed?*)
	   (e,make_disjoint list_param name,
	    super_replace [] fv_env extend_sequence comp_ext list_stmt))
       s list_list_stmt) in
      (* [(e,stmt),(e,stmt),...]*)
  List.map info.insert_body list_final_event
      (* [ne,ne,...] ne = new event*)



(* Fonction principale qui prend une liste d'handlerdef de sched_module et la
* liste des modules. Elle retourne une liste d'event_decl *)      
let verif_sched_construction lhd fv_env modules info
    controle_handlerdef extract_handlers check_disjoint =

  let s = (controle_handlerdef lhd) in
  (* s est une liste contenant la liste de tout les handlers compos�
   * au niveau du sch�duler.
   * type de s:
   * [[(nm,en),(nm,en),(nm,en)],[hss],[hss]...]
   * ou nm = nom de module
   * et en = nom de l'event
   * *)
    let m = (extract_handlers modules) in
    (* m est une liste contenant pour chaque module, son nom et la liste des
    * events contenant super et celle de ceux ne ls contenant pas
    * type de m:
    * [(nm,([e,e,e..],[e,e,e..])),(nm,(ls,lns),...]
    * ou nm = nom du module
    * et ls = liste des events contenant super
    * et lns = liste des event ne contenant pas super
    * et e = Mast.EVENT
    * *)
    (* check that there is at most one non-next handler for each event *)
      lns_disjoint m info;
    (* check that each handler that contains next() appears in the scheduler
       list *)
      ls_in_scheduler m s info;
      if (controle info m s)
      (* A ce stade je peux commencer l'AST car la phase de verification est ok*)
      then 
        (*event_decl du sech final :=  *)
        (clean_lns (concat_ls m) (concat_lns m) info)@
	(build_super_event s m info check_disjoint fv_env info.comp_ext)
      else raise (Error.Error "Do not build final event/function")

let verif_sched_hd_construction lhd hd_fv_env modules =
  verif_sched_construction lhd hd_fv_env modules event
    controle_handlerdef extract_handlers make_event_disjoint
let verif_sched_fn_construction lhd fn_fv_env modules =
  verif_sched_construction lhd fn_fv_env modules fn
    controle_functiondef extract_functions make_function_disjoint
