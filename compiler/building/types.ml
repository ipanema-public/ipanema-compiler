(* A given type has to be defined in the same way in each modules that it
appears.  Types are not imported, so that it is clear how imported
information can be used (and the complete value of imported information can
be accessed for reading).  Other checks will happen as normal in the
verification of the generated scheduler. *)

module B = Objects

let incompatible nm ln1 ln2 =
  Error.update_error();
  Printf.printf "Incompatible definitions for %s at %s and %s\n"
    (B.id2c nm) (B.loc2c ln1) (B.loc2c ln2)

let type_name = function
    Mast.ENUMDEF(nm,vals,a) -> nm
  | Mast.RANGEDEF(nm,first,last,a) -> nm

let rec check_sorted_types = function
    [] -> []
  | [x] -> [x]
  | ((Mast.ENUMDEF(nm,vals,a)) as x)::((ty::_) as rest) ->
      if nm = type_name ty
      then
	begin
	  (match ty with
	    Mast.ENUMDEF(nm,vals1,a1) ->
	      if not(List.for_all2 (function a -> function b -> B.ideq(a,b))
		       vals vals1)
	      then incompatible nm a a1
	  | Mast.RANGEDEF(_,_,_,a1) -> incompatible nm a a1);
	  check_sorted_types rest
	end
      else x :: check_sorted_types rest
  | ((Mast.RANGEDEF(nm,first,last,a)) as x)::((ty::_) as rest) ->
      if nm = type_name ty
      then
	begin
	  (match ty with
	    Mast.RANGEDEF(nm,first1,last1,a1) -> 
	      if not(Mast.equal_except_attributes first first1 &&
		     Mast.equal_except_attributes last last1)
	      then incompatible nm a a1
	  | Mast.ENUMDEF(_,_,a1) -> incompatible nm a a1);
	  check_sorted_types rest
	end
      else x :: check_sorted_types rest

let collect_types modules =
  let types =
    Aux.flat_map
      (function Mast.MODULE(n,_,_,tydefs,_,_,_,_,_,_,_,_,_,_) -> tydefs)
      modules in
  let sorted_types =
    List.sort
      (function ty1 -> function ty2 -> compare (type_name ty1) (type_name ty2))
      types in
  check_sorted_types sorted_types
