(* Explore a collection of states generatable by an automaton.  Collect the
possible states as well as a single path from the initial state that leads
to each one.

A state is a mapping of Bossa classes/states to contents descriptions.
The module is parameterized over the definition of state.

A configuration is a triple of a source state, a rule name, and a target
state.

The first step is to pick a state that has not yet been treated and apply
all applicable rules to it, producing a set of target states.  The
resulting configurations are by definition all fresh, because the source
state has not yet been treated. 

There are then several questions:
1. Should the target state be added to the set of new states?
2. Should the configuration be added to the set of transitions?

The target state should only be added to the set of new states if it has
not been seen before.

Whether the configuration should be added to the set of transitions depends
on the user of this module.
1.  When computing the automaton, we are interested in all transitions that
    can occur at all points.  Thus, every configuration should be added to the
    set of transitions.
2.  When computing the set of inputs derived from quasi-sequences, we are
    only interested in one approach to reaching a given state.  Thus
    nothing should be added if there is already a transition to the target
    state.
The module is thus parameterized over this decision.

A transition (tbl) is a maximal unique sequence of configurations.
extensible_transitions (exttbl) consists of prefixes of the transitions.
*)

type 'a sequence = START of 'a
  | TRANSITION of 'a sequence * Ast.event_name * 'a

type 'a ftres =
    NOTHING_NEW | NOT_FOUND | PREVTRANS of 'a sequence * 'a sequence list

let rec iterate current_states seen_states tbl exttbl
    get_output_states find_transition print_state =
  match current_states with
    [] ->
      let transitions =
	Hashtbl.fold
	  (function key -> function bind -> function collect -> bind::collect)
	  tbl [] in
      (seen_states,transitions)
  | (current_state::remaining_states) ->
      (* just process one state per iteration *)
      let edges_targets = get_output_states current_state in
      (* collect new states *)
      let new_states =
	Aux.option_filter
	  (function (nm,target) ->
	    if Aux.member target seen_states
	    then None
	    else Some(target))
	  edges_targets in
      (* collect new transitions *)
      let new_configs =
	List.map (function (nm,st) -> (current_state,nm,st))
	  edges_targets in
      List.iter
	(function ((src,nm,tgt) as config) ->
	  match find_transition config tbl with
	    NOTHING_NEW -> ()
	  | NOT_FOUND ->
	      (match find_transition config exttbl with
		NOTHING_NEW -> ()
	      | NOT_FOUND ->
		  raise (Error.Error "no transition found")
	      | PREVTRANS(prev,rest) ->
		  Hashtbl.add tbl tgt (TRANSITION(prev,nm,tgt)))
	  | PREVTRANS(prev,rest) ->
	      let moving_config =
		match prev with
		  START a -> a
		| TRANSITION(_,_,a) -> a in
	      Hashtbl.remove tbl moving_config;
	      Hashtbl.add tbl tgt (TRANSITION(prev,nm,tgt));
	      Hashtbl.add exttbl moving_config prev)
	  new_configs;
      iterate (new_states @ remaining_states) (new_states @ seen_states)
	tbl exttbl get_output_states find_transition print_state

let model init_states get_output_states find_transition tbl exttbl
    print_state =
  List.iter (function st -> Hashtbl.add tbl st (START(st))) init_states;
  iterate init_states init_states tbl exttbl
    get_output_states find_transition print_state
