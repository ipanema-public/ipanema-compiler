type 'a sequence = START of 'a
  | TRANSITION of 'a sequence * Ast.event_name * 'a

type 'a ftres =
    NOTHING_NEW | NOT_FOUND | PREVTRANS of 'a sequence * 'a sequence list

val model : 'a list ->                       (* starting states *)
  ('a -> (Ast.event_name * 'a) list) ->      (* get_output_states *)
    (('a * Ast.event_name * 'a) -> ('a,'a sequence) Hashtbl.t ->
      'a ftres) ->
	('a,'a sequence) Hashtbl.t ->
	  ('a,'a sequence) Hashtbl.t ->
                                             (* find_transition *)
	    ('a -> unit) ->                  (* state printer function *)
	      ('a list * 'a sequence list)
