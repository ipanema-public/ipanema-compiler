#(*************************************************************************)
#(*                    Bossa types                                      *)
#(*									 *)
#(*                Julia ,                                       *)
#(*   Copyright (C) 2001-2002 INRIA. All rights reserved.                 *)
#(*   Institut National de Recherche en Informatique et en Automatique.   *)
#(*                                                                       *)
#(*************************************************************************)

SOURCES = model.ml
TARGET = model

# The Caml compilers.
OPT		= .opt
CAMLC           = ocamlc -g
CAML 		= ocamlopt
CAMLP4		= camlp4o
CAMLOPT         = $(CAML) -pp "$(CAMLP4)"
CAMLLEX 	= ocamllex
CAMLYACC 	= ocamlyacc
CAMLDEP 	= ocamldep

LIB=$(TARGET).cma
OPTLIB=$(LIB:.cma=.cmxa)

OBJS = $(SOURCES:.ml=.cmo)
OPTOBJS = $(SOURCES:.ml=.cmx)

INCLUDE_PATH = -I ../types

all: $(LIB)

all.opt: $(OPTLIB)


$(LIB): $(OBJS)
	$(CAMLC) -a -o $(LIB) $(OBJS)

# clean rule for LIB
clean::
	rm -f $(LIB)


$(OPTLIB): $(OPTOBJS)
	$(CAMLOPT) -a -o $(OPTLIB) $(OPTOBJS)

# clean rule for LIB.opt
clean::
	rm -f $(OPTLIB) $(LIB:.cma=.a) 	


.SUFFIXES:
.SUFFIXES: .ml .mli .cmo .cmi .cmx

.ml.cmo:
	$(CAMLC) $(INCLUDE_PATH) -c $<

.mli.cmi:
	$(CAMLC) $(INCLUDE_PATH) -c $<

.ml.cmx:
	$(CAMLOPT) $(INCLUDE_PATH) -c $<

# clean rule for others files
clean::
	rm -f *.cm[iox] *.o 
	rm -f *~ .*~ #*# 

depend: 
	$(CAMLDEP) *.mli *.ml > .depend

clean::
	rm -f .depend

.depend:
	$(CAMLDEP) *.mli *.ml > .depend

-include .depend
