(* --------------------------- Verification ------------------------- *)

type vprocname = SRC | TGT | P of int | Q of int | UNKNOWNPROC | BOTPROC |
                 S of int * string | ARG of int
and vproc = VP of vprocname * Class_state.state_info list |
            EQUAL of vproc list (* to be kept sorted *)
(* the list argument to MEMBER is a set and is never empty *)
type vtype =
    BOT | UNKNOWN | EMPTY | MEMBER of vproc list
val lub_vtype : vtype -> vtype -> vtype
val lub_vproc : vproc -> vproc -> vproc
val compatible : vtype * vtype -> bool
val vp2c : vproc -> string
val vp_member : vprocname -> vproc list -> bool
val vp_replace : vprocname -> vprocname -> vproc -> vproc
val vp_remove : vprocname -> vproc list -> vproc list
val collect_ps : vproc -> vprocname list
val collect_vps : vproc -> vproc list
val collect_sources : vproc -> Class_state.state_info list
val vp2c_all : vproc -> string
val vprocname2c : vprocname -> string
val vt2c : vtype -> string
val vt2c_all : vtype -> string
val known_contents : vtype -> vproc list
val contains_unknown : Class_state.state_type -> vtype -> bool
val drop_vp : (vprocname -> bool) -> vproc list -> vproc list

val add_proc
    : vproc list -> vtype -> Class_state.state_info list ->
      vtype * Class_state.state_info list
val add_queue
    : vproc list -> vtype -> Class_state.state_info list ->
      vtype * Class_state.state_info list

(* in each case, the first component of the result is the thing that was
actually removed, the second component is what was left, and the third
component is the sources information *)
val remove_proc_possible
    : vproc list -> vtype -> Class_state.state_info list ->
      vproc list * vtype * Class_state.state_info list
val remove_proc_definite
    : vproc list -> vtype -> Class_state.state_info list ->
      vproc list * vtype * Class_state.state_info list
val remove_queue
    : vproc list -> vtype -> Class_state.state_info list ->
      vproc list * vtype * Class_state.state_info list

val mk_min_seq : Class_state.state_info list -> int -> vtype

(* --------------- Drop unknown proc, for explication --------------- *)

val drop_unknownproc : vtype -> vtype

(* -------------------- Modification information -------------------- *)

type modified = MODIFIED | UNMODIFIED | BOTMOD

val lub_modified : modified * modified -> modified
val modified2c : modified -> string
