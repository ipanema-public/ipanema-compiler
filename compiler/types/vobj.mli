val handler_name : string ref

(* ---------------------- process descriptions ---------------------- *)

type pname = SRC | TGT | SRCANDTGT | ARG of int | X
type pd = FROMSTATE of pname * Class_state.state_info
        | FROMCLASS of Class_state.classname
        | UNKNOWN (* (X,bot) *)
val unique   : pd -> bool
val arg_list : pname list ref (* numbers used in arg variables for current fn*)
val max_arg  : int ref (* numbers used in arg variables for current fn*)
val alpha    : pd -> pd list
val pdlub    : pd -> pd -> pd
val pdluball : int -> pd list -> pd
val pdluball_fresh : int -> pd list -> pd
val pdunfresh : pd -> pd
val discard_fresh : pd -> unit
val pd2c     : pd -> string
val is_src_tgt : pd -> bool
val pd_max_union : pd -> pd list -> pd list

(* ---------------------- contents descriptions --------------------- *)

type cd = EMPTY | MM of pd list (* min *) * pd list (* max *)
val init_cd : Class_state.state_info -> cd
val get_mm  : cd -> (pd list * pd list)
val cdlub   : cd -> cd -> cd
val cd2c    : cd -> string
val cd2c_complete : cd -> string

(* ------------------------ state environment ----------------------- *)

type state_env = (Class_state.state_info (* state *) * cd (* contents *)) list
val lookup : pd -> state_env -> state_env * state_env
val lookup_src_tgt : pname -> state_env -> pd
val state_lookup : Objects.identifier -> state_env -> cd
val state_info_lookup : int -> Class_state.state_info -> state_env -> cd
val combine_extensions : state_env -> state_env -> state_env
val state_env_leq : state_env -> state_env -> bool
val state_env_lub_all : state_env list -> state_env
val state_env2c : state_env -> string
val state_env2c_complete : state_env -> string
val compare_state_info :
    (Class_state.state_info * cd) -> (Class_state.state_info * cd) -> int
val state_env_replace : pd -> pd -> state_env -> state_env

(* ---------------------- variable environment ---------------------- *)

type var_env = (Objects.identifier * pd) list
val var_env_lookup : Objects.identifier -> var_env -> Objects.attribute -> pd
val update_var_env : Objects.identifier -> pd -> var_env -> var_env
val drop_var_env : Objects.identifier -> var_env -> var_env
val var_env_leq : var_env -> var_env -> bool
val var_env_lub_all : var_env list -> var_env

(* ----- check a state environment against an event type output ----- *)

val make_input_envs :
    (Class_state.classname * Vobjects.vtype * Vobjects.modified) list ->
      (state_env * (Vobjects.vproc * Class_state.classname) list *
	 (unit -> unit)) list
val nullstpfn : unit -> unit
val event_type2c :
    (Class_state.classname * Vobjects.vtype * Vobjects.modified) list -> string
val check_output : (Vobjects.vproc * Class_state.classname) list ->
  state_env ->
  (Class_state.classname * Vobjects.vtype * Vobjects.modified) list ->
    string option
