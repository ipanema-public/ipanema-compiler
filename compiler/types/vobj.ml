(* $Id$ *)

(* Objects used in the verification phase *)

module CS = Class_state
module B = Objects
module VB = Vobjects

let handler_name = ref ""

(* ---------------------- process descriptions ---------------------- *)

let srcandtgt_present = ref false
let tgt_present = ref false

type pname = SRC | TGT | SRCANDTGT | ARG of int | X

type pd = FROMSTATE of pname * CS.state_info | FROMCLASS of CS.classname
        | UNKNOWN (* (X,bot) *)

let unique = function
    FROMSTATE(SRC,_) | FROMSTATE(TGT,_) | FROMSTATE(ARG(_),_) -> true
  | FROMSTATE(SRCANDTGT,_) -> true
  | FROMSTATE(_,CS.STATE(_,_,CS.PROC,_)) -> true
  | _ -> false

(* numbers used in arg variables for current fn *)
let arg_list = ref ([] : pname list)
let max_arg = ref (-1)

let alpha = function
    FROMSTATE(SRC,state) ->
      [FROMSTATE(SRC,state);FROMSTATE(X,state);FROMCLASS(CS.state2class state);
	UNKNOWN]
  | FROMSTATE(TGT,state) ->
      [FROMSTATE(TGT,state);FROMSTATE(X,state);FROMCLASS(CS.state2class state);
	UNKNOWN]
  | FROMSTATE(SRCANDTGT,state) ->
      [FROMSTATE(SRCANDTGT,state);FROMSTATE(X,state);
	FROMCLASS(CS.state2class state);UNKNOWN]
  | FROMSTATE(ARG(n),state) ->
      [FROMSTATE(ARG(n),state);FROMSTATE(X,state);
	FROMCLASS(CS.state2class state);UNKNOWN]
  | FROMSTATE(X,state)   ->
      [FROMSTATE(X,state);FROMCLASS(CS.state2class state);UNKNOWN]
  | FROMCLASS(cls)       ->
      let all_states = CS.concretize_class cls in
      let states =
	Aux.flat_map
	  (function pname ->
	    (List.map
	       (function state ->
		 FROMSTATE(pname,state))
	       all_states))
	  ((!arg_list)@
	   (if !srcandtgt_present
	   then [SRCANDTGT;X]
	   else [SRC;TGT;X])) in
      UNKNOWN::FROMCLASS(cls)::states
  | UNKNOWN              ->
      let all_states = CS.all_states() in
      let states =
	Aux.flat_map
	  (function pname ->
	    (List.map
	       (function state ->
		 FROMSTATE(pname,state))
	       all_states))
	  ((!arg_list)@
	   (if !srcandtgt_present
	   then [SRCANDTGT;X]
	   else [SRC;TGT;X])) in
      let classes =
	List.map (function cls -> FROMCLASS cls) (CS.all_classes false) in
      UNKNOWN::classes@states

let pdlub pd1 pd2 =
  if pd1 = pd2
  then pd1
  else
    match (pd1,pd2) with
      (UNKNOWN,_) -> UNKNOWN
    | (_,UNKNOWN) -> UNKNOWN
    | (FROMCLASS(c1),FROMCLASS(c2)) -> UNKNOWN
    | (FROMSTATE(p1,s1),FROMCLASS(c2)) ->
	if CS.state2class s1 = c2 then FROMCLASS(c2) else UNKNOWN
    | (FROMCLASS(c1),FROMSTATE(p2,s2)) ->
	if CS.state2class s2 = c1 then FROMCLASS(c1) else UNKNOWN
    | (FROMSTATE(p1,s1),FROMSTATE(p2,s2)) ->
	if s1 = s2
	then FROMSTATE(X,s1)
	else if CS.state2class s1 = CS.state2class s2
	then FROMCLASS(CS.state2class s1)
	else UNKNOWN

let pdluball n = function
    [] -> raise (Error.Error (Printf.sprintf "pdluball %d: no information" n))
  | (x::xs) ->
      List.fold_left
	(function lub ->
	  function elem ->
	    pdlub elem lub)
	x xs

let pdluball_fresh n maxx =
  match pdluball n maxx with
    FROMSTATE(X,state) ->
      let arg = (!max_arg) + 1 in
      max_arg := arg;
      arg_list := (ARG(arg)) :: (!arg_list);
      FROMSTATE(ARG(arg),state)
  | x -> x

let pdunfresh = function 
    FROMSTATE(ARG(n) as x,state) as y ->
      if !max_arg = n (* should be true for loop variable *)
      then
	(arg_list := Aux.remove x (!arg_list);
	 FROMSTATE(X,state))
      else y
  | x -> x

let discard_fresh = function 
    FROMSTATE(ARG(n),state) -> if !max_arg = n then max_arg := n-1
  | _ -> ()

let pname2c state = function
    SRC    -> "src"
  | TGT    -> "tgt"
  | SRCANDTGT    -> "src/tgt"
  | ARG(n) -> "arg"^(string_of_int n)
  | X      -> CS.state2c state

let pd2c = function
    FROMSTATE(pname,state) -> pname2c state pname
  | FROMCLASS(cls)         -> CS.class2c cls
  | UNKNOWN                -> "?"

let is_src_tgt pd =
  match pd with
    FROMSTATE(SRC,_) | FROMSTATE(TGT,_) | FROMSTATE(SRCANDTGT,_) -> true
  | _ -> false

let pd_max_union pd max = (* U+ in the gpce paper *)
  let drop_lower =
    Aux.option_filter
      (function pd1 ->
	if pdlub pd pd1 = pd then None else Some pd1)
      max in
  if List.exists (function pd1 -> pdlub pd pd1 = pd1) drop_lower
  then max
  else pd::max

(* ---------------------- contents descriptions --------------------- *)

type cd = EMPTY | MM of pd list (* min *) * pd list (* max *)

let init_cd state = MM([],[FROMSTATE(X,state)])

let get_mm = function
    EMPTY -> ([],[])
  | MM(min,max) -> (min,max)

let normalize_cd = function
    EMPTY -> EMPTY
  | MM(min,max) -> MM(List.sort compare min,List.sort compare max)

let cdlub cd1 cd2 =
  match (cd1,cd2) with
    (EMPTY,EMPTY) -> EMPTY
  | _ ->
      let (min1,max1) = get_mm cd1 in
      let (min2,max2) = get_mm cd2 in
      let alpha = Aux.intersect min1 min2 in
      let min1p =
	let sub12 = Aux.subtract min1 min2 in
	Aux.union_list
	  (List.map
	     (function
		 FROMSTATE(name,state) -> FROMSTATE(X,state)
	       | FROMCLASS(cls) -> FROMCLASS(cls)
	       | UNKNOWN -> UNKNOWN)
	     sub12) in
      let min2p =
	let sub21 = Aux.subtract min2 min1 in
	Aux.union_list
	  (List.map
	     (function
		 FROMSTATE(name,state) -> FROMSTATE(X,state)
	       | FROMCLASS(cls) -> FROMCLASS(cls)
	       | UNKNOWN -> UNKNOWN)
	     sub21) in
      let beta = Aux.intersect min1p min2p in
      let gamma =
	let aub = List.length (Aux.union alpha beta) in
	let min_lens = min (List.length min1) (List.length min2) in
	if aub < min_lens
	then [UNKNOWN]
	else [] in
      normalize_cd(MM(Aux.union alpha (Aux.union beta gamma),
		      Aux.union max1 max2))

let cd2c = function
    EMPTY         -> "[]"
  | MM([min],max) -> pd2c min
  | MM(min,max)   -> Aux.set2c(List.map pd2c min)

let cd2c_complete = function
    EMPTY         -> "[]"
  | MM(min,max)   ->
      Printf.sprintf "(%s,%s)"
	(Aux.set2c(List.map pd2c min)) (Aux.set2c(List.map pd2c max))

(* ------------------------ state environment ----------------------- *)

type state_env =
    (CS.state_info (* state *) * cd (* contents *)) list

let state_env2c state_env =
  Aux.set2c
    (Aux.option_filter
       (function (si,cd) ->
	 match cd with
	   MM([],_) -> None
	 | _ -> Some (Printf.sprintf "%s: %s" (CS.state2c si) (cd2c cd)))
       state_env)

let state_env2c_complete state_env =
  Aux.set2c
    (List.map
       (function (si,cd) ->
	 Printf.sprintf "%s -> %s" (CS.state2c si) (cd2c_complete cd))
       state_env)

(* the following function both looks up a process description in a state
environment and returns an update to the state environment that accounts
for removing the process from its current state *)

let lookup pd state_env =
  let remove_all_traces _ =
    let apd = alpha pd in
    let states =
      List.filter
	(function (state_info,cd) ->
	  let (_,max) = get_mm cd in
	  List.exists (function pd -> List.mem pd max) apd)
    state_env in
    (states,
    List.map
      (function (state_info,cd) ->
	let (min,max) = get_mm cd in
	(state_info,MM(Aux.subtract min apd, max)))
      states) in
  if unique pd
  then
    let states =
      List.filter
	(function (state_info, cd) ->
	  let (min,_) = get_mm cd in
	  List.mem pd min)
	state_env in
    match states with
      [(CS.STATE(_,_,CS.PROC,_) as state,_)] -> (states,[(state,EMPTY)])
    | [(state,cd)] ->
	let (min,max) = get_mm cd in
	(* it's unfortunate that we cannot remove pd from max; the need for
	   this has something to do with monotonicity *)
	(states,[(state,MM(Aux.subtract min [pd], max))])
    | [] -> remove_all_traces()
    | _ ->
	raise
	  (Error.Error("vobj: a unique process should not appear in "^
		       "more than one min set\n"^
		       (Printf.sprintf "%sprocess %s in %s"
			  (!handler_name) (pd2c pd) (state_env2c state_env))))
  else remove_all_traces()

let lookup_src_tgt pname state_env =
  let all_processes =
    Aux.map_union
      (function (state_info,cd) ->
	match cd with
	  EMPTY -> []
	| MM(min,max) -> Aux.union min max)
      state_env in
  try
    List.find
      (function
	  FROMSTATE(pname1,_) -> pname = pname1 || pname1 = SRCANDTGT
	| _ -> false)
      all_processes
  with Not_found ->
    raise (Error.Error(Printf.sprintf "%s cannot find source or target"
			 (!handler_name)))

let state_lookup state state_env =
  try
    let (_,cd) =
      List.find (function (CS.STATE(id,_,_,_),_) -> B.ideq(state,id))
	state_env in
    cd
  with Not_found ->
    raise
      (Error.Error(Printf.sprintf "vobj, state_lookup: unknown state %s"
		     (B.id2c state)))

let state_info_lookup n state_info state_env =
  try
    let (_,cd) =
      List.find (function (si,_) -> si = state_info)
	state_env in
    cd
  with Not_found ->
    raise
      (Error.Error(Printf.sprintf
		     "vobj, state_info_lookup %d: unknown state %s, among %s"
		     n (CS.state2c state_info)
		     (state_env2c_complete state_env)))

let combine_extensions new_env old_env =
  let all =
    Aux.union_list (List.map (function (x,_) -> x) (new_env @ old_env)) in
  List.map
    (function state_info ->
      try
	List.find (function (x,_) -> x = state_info) new_env
      with Not_found -> List.find (function (x,_) -> x = state_info) old_env)
    all

let state_env_leq state_env1 state_env2 =
  List.for_all
    (function (si,cd) ->
      try
	let (_,cd1) = List.find (function (si1,_) -> si1 = si) state_env2 in
	normalize_cd cd1 = normalize_cd (cdlub cd cd1)
      with Not_found -> raise (Error.Error "unknown state"))
    state_env1

let state_env_lub state_env1 state_env2 =
  List.map
    (function (si,cd) ->
      let (_,cd1) =
	try
	  List.find (function (si1,_) -> si1 = si) state_env1
	with
	  Not_found ->
	    raise (Error.Error "incomaptible envs in state_env_lub") in
      (si, cdlub cd cd1))
    state_env2

let state_env_lub_all = function
    [] -> [] (* no envs implies dead code, so return nothing *)
  | l ->
      let rev = List.rev l in
      List.fold_left
	(function state_env ->
	  function cur ->
	    state_env_lub cur state_env)
	(List.hd rev) (List.tl rev)

let compare_state_info
    ((CS.STATE(id1,cls1,_,_)),_) ((CS.STATE(id2,cls2,_,_)),_) =
  if cls1 = cls2
  then compare id1 id2
  else if CS.less_than(cls1,cls2) then 1 else -1

let state_env_replace elem new_elem state_env =
  let process l =
    Aux.union_list
      (List.map (function pd -> if pd = elem then new_elem else pd) l) in
  List.map
    (function
	(si,EMPTY) as x  -> x
      |	(si,MM(min,max)) -> (si,MM(process min, process max)))
    state_env

(* ---------------------- variable environment ---------------------- *)

type var_env = (B.identifier * pd) list

let var_env_lookup id var_env attr =
  try
    let (_,pd) = List.find (function (id1,_) -> id1 = id) var_env in
    pd
  with
    Not_found ->
      raise
	(Error.Error(Printf.sprintf "%s: vobj: unknown variable %s"
		       (B.loc2c attr) (B.id2c id)))

let update_var_env id pd var_env =
  let rec loop = function
      [] -> raise Not_found
    | ((x,_) as cur) :: rest ->
	if B.ideq(id,x) then (x,pd) :: rest else cur :: (loop rest) in
  try loop var_env
  with Not_found -> (id,pd) :: var_env

let drop_var_env id var_env =
  let rec loop = function
      [] -> raise Not_found
    | ((x,_) as cur) :: rest ->
	if B.ideq(id,x) then rest else cur :: (loop rest) in
  try loop var_env
  with Not_found -> var_env

(* computes var_env1 <= var_env2.  var_env1 can contain variables not bound
in var_env2 *)
let var_env_leq var_env1 var_env2 =
  List.for_all
    (function (id,pd) ->
      try
	let (_,pd1) = List.find (function (id1,_) -> id1 = id) var_env2 in
	pd1 = pdlub pd pd1
      with Not_found -> true)
    var_env1

(* computes var_env1 lub var_env2.  var_env1 can contain variables not bound
in var_env2.  These variables are ignored. *)
let var_env_lub var_env1 var_env2 =
  List.map
    (function (id,pd) ->
      let (_,pd1) =
	try
	  List.find (function (id1,_) -> id1 = id) var_env1
	with
	  Not_found ->
	    raise (Error.Error "incomaptible envs in var_env_lub") in
      (id, pdlub pd pd1))
    var_env2

let var_env_lub_all = function
    [] -> [] (* no envs implies dead code, so return nothing *)
  | l ->
      let rev = List.rev l in
      List.fold_left
	(function var_env ->
	  function cur ->
	    var_env_lub cur var_env)
	(List.hd rev) (List.tl rev)

(* ------ generate state environments from an event type input ------ *)

let class2state_options (cls,desc,_) =
  let states = CS.concretize_class cls in
  match desc with
    VB.UNKNOWN ->
      [(List.map (function s -> (s,MM([],[FROMSTATE(X,s)]))) states,[])]
  | VB.EMPTY -> [(List.map (function s -> (s,EMPTY)) states,[])]
  | VB.MEMBER(l) ->
      let n = List.length states in
      let partitions = Aux.partition l n in
      List.map
	(function partition ->
	  let (state_env,var_info) =
	    List.split
	      (List.map2
		 (function state ->
		   function elems ->
		     let (min,var_info) =
		       List.split
			 (List.map
			    (function
				VB.VP(VB.SRC,_)    ->
				  if !srcandtgt_present
				  then
				    raise
				      (Error.Error
					 "cannot have both src and src=tgt")
				  else
				    begin
				      tgt_present := true;
				      (FROMSTATE(SRC,state),[])
				    end
			      | VB.VP(VB.TGT,_)    ->
				  if !srcandtgt_present
				  then
				    raise
				      (Error.Error
					 "cannot have both tgt and src=tgt")
				  else
				    begin
				      tgt_present := true;
				      (FROMSTATE(TGT,state),[])
				    end
			      | VB.VP(VB.ARG(n),_) ->
				  (FROMSTATE(ARG(n),state),[])
			      | VB.VP(VB.UNKNOWNPROC,_) ->
				  (FROMSTATE(X,state),[])
			      | VB.VP(VB.BOTPROC,_) ->
				  raise (Error.Error "internal error")
			      | VB.VP(_,_) as p     ->
				  (FROMSTATE(X,state),[(p,cls)])
			      (* equal is only used in a VS, to indicate that
				 the src an tgt are on the same child
				 scheduler *)
			      | VB.EQUAL([VB.VP(VB.SRC,_);VB.VP(VB.TGT,_)])
			      | VB.EQUAL([VB.VP(VB.TGT,_);VB.VP(VB.SRC,_)])
				when not (!tgt_present) ->
				  srcandtgt_present := true;
				  (FROMSTATE(SRCANDTGT,state),[])
			      | VB.EQUAL(_) ->
				  raise (Error.Error
					   "nonsensical equal pattern"))
			    elems) in
		     let var_info = List.concat var_info in
		     let max = Aux.union [FROMSTATE(X,state)] min in
		     ((state,MM(min,max)),var_info))
		 states partition) in
	  (state_env,List.concat var_info))
	partitions
  | VB.BOT -> raise (Error.Error "internal error")
	
let rec crossproduct = function
    [] -> [([],[])]
  | x::xs ->
      let rest = crossproduct xs in
      List.concat
	(List.map
	   (function (cur,info) ->
	     List.map
	       (function (another,another_info) ->
		 (cur @ another, info @ another_info))
	       rest)
	   x)

let make_input_envs input =
  srcandtgt_present := false;
  tgt_present := false;
  let options = List.map class2state_options input in
  let stp = !srcandtgt_present in
  let tp = !tgt_present in
  List.map
    (function (config,info) ->
      (List.sort compare_state_info config,info,
       function _ ->
	 srcandtgt_present := stp;
	 tgt_present := tp))
    (crossproduct options)

let nullstpfn _ = tgt_present := true

(* ----- check a state environment against an event type output ----- *)

let event_type2c et =
  Aux.set2c
    (Aux.option_filter
       (function
	   (_,VB.BOT,_) -> raise (Error.Error "internal error")
	 | (cls,VB.UNKNOWN,VB.UNMODIFIED) -> None
	 | (cls,VB.UNKNOWN,VB.MODIFIED) -> Some ((CS.class2c cls)^"!")
	 | (cls,VB.EMPTY,_) -> Some ((CS.class2c cls)^" = []")
	 | (cls,VB.MEMBER([l]),VB.UNMODIFIED) ->
	     Some(Printf.sprintf "%s in %s" (VB.vp2c l) (CS.class2c cls))
	 | (cls,VB.MEMBER(l),VB.UNMODIFIED) ->
	     Some(Printf.sprintf "%s in %s"
		    (Aux.set2c (List.map VB.vp2c l))
		    (CS.class2c cls))
	 | (cls,VB.MEMBER([l]),VB.MODIFIED) ->
	     Some(Printf.sprintf "%s in %s!" (VB.vp2c l) (CS.class2c cls))
	 | (cls,VB.MEMBER(l),VB.MODIFIED) ->
	     Some(Printf.sprintf "%s in %s!"
		    (Aux.set2c (List.map VB.vp2c l))
		    (CS.class2c cls))
	 | (_,_,VB.BOTMOD) -> raise (Error.Error "internal error"))
       et)

exception Check_fail of string

let fail1 cls s state =
  raise
    (Check_fail
       (Printf.sprintf
	  "Change of state (from %s to %s) not allowed in class %s."
	  (CS.state2c s) (CS.state2c state) (CS.class2c cls)))

let fail2 cls state =
  raise
    (Check_fail
       (Printf.sprintf
	  "Change of state not allowed in class %s.\nUnknown process in state %s."
	  (CS.class2c cls) (CS.state2c state)))

let fail3 cls state =
  raise
    (Check_fail
       (Printf.sprintf
	  "Change of state only allowed within class %s.\nError in state %s."
	  (CS.class2c cls) (CS.state2c state)))

let fail4 cls state =
  raise
    (Check_fail
       (Printf.sprintf
	  "Change of state only allowed within class %s.\nUnknown process in state %s."
	  (CS.class2c cls) (CS.state2c state)))

let fail5 cls state =
  raise
    (Check_fail
       (Printf.sprintf
	  "Change of state only allowed for listed processes in %s.\nError in state %s."
	  (CS.class2c cls) (CS.state2c state)))

let fail6 cls state =
  raise
    (Check_fail
       (Printf.sprintf
	  "Change of state only allowed for listed processes %s.\nUnknown process in state %s."
	  (CS.class2c cls) (CS.state2c state)))

let fail7 cls state =
  raise
    (Check_fail
       (Printf.sprintf
	  "Change of class only allowed for listed processes in %s.\nError in state %s."
	  (CS.class2c cls) (CS.state2c state)))

let fail8 cls state =
  raise
    (Check_fail
       (Printf.sprintf
	  "Change of class only allowed for listed processes %s.\nUnknown process in state %s."
	  (CS.class2c cls) (CS.state2c state)))

let rec check_one_output var_info state_env (cls,desc,modd) =
  let states = CS.concretize_class cls in
  match (desc,modd) with
    (VB.BOT,_) | (_,VB.BOTMOD) -> raise (Error.Error "internal error")
  | (VB.UNKNOWN,VB.UNMODIFIED) ->
      List.iter
	(function state ->
	  let cd = state_info_lookup 20 state state_env in
	  let (min,max) = get_mm cd in
	  List.iter
	    (function
		FROMSTATE(_,s) ->
		  if not (s = state) then fail1 cls s state
	      |	_ -> fail2 cls state)
	    max)
	states
  | (VB.UNKNOWN,VB.MODIFIED) ->
      List.iter
	(function state ->
	  let cd = state_info_lookup 21 state state_env in
	  let (min,max) = get_mm cd in
	  List.iter
	    (function
		FROMSTATE(_,s) ->
		  if not(List.mem s states) then fail3 cls state
	      |	FROMCLASS(cls1) ->
		  if not(cls = cls1) then fail3 cls state
	      |	_ -> fail4 cls state)
	    max)
	states
  | (VB.EMPTY,_) ->
      (* this case is not allowed by the esop paper, but is generated by
	 the current set of event types *)
      List.iter
	(function state ->
	  match state_info_lookup 22 state state_env with
	    EMPTY -> ()
	  | _ ->
	      raise
		(Check_fail
		   (Printf.sprintf "state %s must be empty"
		      (CS.state2c state))))
	states
  | (VB.MEMBER(l),VB.UNMODIFIED) ->
      check_mins l cls states var_info state_env;
      (* check maxes *)
      let all_starting_classes =
	Aux.map_union
	  (function
	      VB.VP(VB.SRC,_) | VB.VP(VB.TGT,_) | VB.VP(VB.ARG(_),_)
	    | VB.EQUAL([VB.VP(VB.SRC,_);VB.VP(VB.TGT,_)])
	    | VB.EQUAL([VB.VP(VB.TGT,_);VB.VP(VB.SRC,_)]) -> []
	    | var ->
		let (_,cls) =
		  Aux.list_find "check maxes"
		    (function (v,_) -> v = var) var_info in
		[cls])
	  l in
      List.iter
	(function state ->
	  let cd = state_info_lookup 23 state state_env in
	  let (_,max) = get_mm cd in
	  List.iter
	    (function
	      	FROMSTATE(SRC,s) | FROMSTATE(TGT,s) | FROMSTATE(ARG(_),s)
	      | FROMSTATE(SRCANDTGT,s) -> ()
	      |	FROMSTATE(_,s) ->
		  if not(s = state ||
		         List.mem (CS.state2class s) all_starting_classes)
		  then fail5 cls state
	      |	FROMCLASS(cls1) ->
		  if not(List.mem cls1 all_starting_classes)
		  then fail5 cls state
	      | _ -> fail6 cls state)
	    max)
	states
  | (VB.MEMBER(l),VB.MODIFIED) ->
      check_mins l cls states var_info state_env;
      (* check maxes *)
      let all_starting_classes =
	Aux.map_union
	  (function
	      VB.VP(VB.SRC,_) | VB.VP(VB.TGT,_) | VB.VP(VB.ARG(_),_)
	    | VB.EQUAL([VB.VP(VB.SRC,_);VB.VP(VB.TGT,_)])
	    | VB.EQUAL([VB.VP(VB.TGT,_);VB.VP(VB.SRC,_)]) -> []
	    | var ->
		let (_,cls) =
		  Aux.list_find "check maxes"
		    (function (v,_) -> v = var) var_info in
		[cls])
	  l in
      List.iter
	(function state ->
	  let cd = state_info_lookup 24 state state_env in
	  let (_,max) = get_mm cd in
	  List.iter
	    (function
		(* for the src, etc case, should also check that the pd is
		   also in the min; otherwise it should be handled by the
		   other cases *)
	      	FROMSTATE(SRC,s) | FROMSTATE(TGT,s) | FROMSTATE(ARG(_),s)
	      | FROMSTATE(SRCANDTGT,s) -> ()
	      |	FROMSTATE(_,s) ->
		  if not(List.mem s states ||
		         List.mem (CS.state2class s) all_starting_classes)
		  then fail7 cls state
	      |	FROMCLASS(cls1) ->
		  if not(cls = cls1 || List.mem cls1 all_starting_classes)
		  then fail7 cls state
	      | _ -> fail8 cls state)
	    max)
	states

and check_mins l cls states var_info state_env =
  let all_mins =
    Aux.map_union
      (function state ->
	let cd = state_info_lookup 25 state state_env in
	let (min,_) = get_mm cd in
	min)
      states in
  (* check mins *)
  let _ =
    List.fold_left
      (function mins ->
	function 
	    VB.VP(VB.SRC,_) ->
	      search_for_return_rest mins cls "src"
		(function FROMSTATE(SRC,_) -> true | _ -> false)
	  | VB.VP(VB.TGT,_) ->
	      search_for_return_rest mins cls "tgt"
		(function FROMSTATE(TGT,_) -> true | _ -> false)
	  | VB.EQUAL([VB.VP(VB.SRC,_);VB.VP(VB.TGT,_)])
	  | VB.EQUAL([VB.VP(VB.TGT,_);VB.VP(VB.SRC,_)]) ->
	      search_for_return_rest mins cls "src/tgt"
		(function FROMSTATE(SRCANDTGT,_) -> true | _ -> false)
	  | VB.VP(VB.ARG(n),_) ->
	      search_for_return_rest mins cls ("arg-" ^ (string_of_int n))
		(function FROMSTATE(ARG(n1),_) -> n1 = n | _ -> false)
	  | var ->
	      let (_,cls) =
		Aux.list_find "check mins"
		  (function (v,_) -> v = var) var_info in
	      search_for_return_rest mins cls (VB.vp2c var)
		(function
		    FROMSTATE(_,s)  -> CS.state2class s = cls
		  | FROMCLASS(cls1) -> cls1 = cls
		  | _ -> false))
      all_mins l in
  ()

(* This looks a bit wrong: the thing that is removed from the list that
matches might be the only thing that matches some later variable *)
and search_for_return_rest l cls name pred =
  let (from_state,rest) = List.partition pred l in
  match from_state with
    [] ->
      raise
	(Check_fail
	   (Printf.sprintf "In class %s, no process matching %s"
	      (CS.class2c cls) name))
  | x::xs -> xs@rest

let check_output var_info state_env output =
  try
    List.iter (check_one_output var_info state_env) output;
    None
  with Check_fail s -> Some s
