module B = Objects
module VB = Vobjects
module CS = Class_state

(* -------------------- The scheduler structure ------------------- *)

type scheduler =
  SCHEDULER of
    string                (* name *)
    * bool                (* default scheduler *)
    * valdef list         (* constant declarations *)
    * typedef list        (* programmer declared types *)
    * vardecl list        (* fields of the process structure *)
    * fundecl list        (* external function declarations *)
    * valdef list         (* variable declarations *)
    * state list          (* states *)
    * cstate list          (*core states *)
    * criteria list       (* ordering criteria *)
    * trace               (* tracing declaration *)
    * event_decl list     (* event handlers *)
    * fundef list         (* interface functions *)
    * fundef list         (* verifier-defined functions *)
    * B.attribute
	
and typedef =
  ENUMDEF of
    B.identifier          (* the name of the declared type *)
    * B.identifier list   (* the list of possible values *)
    * B.attribute
  | RANGEDEF of
    B.identifier          (* the name of the declared type *)
    * expr                      (* the lower bound *)
    * expr                      (* the upper bound *)
    * B.attribute

and vardecl =
  VARDECL of
    requires
    * B.typ                 (* the type of the declared identifier *)
    * B.identifier        (* the name of the declared identifier *)
    * bool                (* true if imported from the system *)
    * B.attribute
    and requires = REQUIRES | NOT_REQUIRES 

(* useful to avoid a parser problem *)
and pre_valdef = FN of fundecl | VL of valdef

and fundecl =
  FUNDECL of
    B.typ                 (* the return type *)
    * B.identifier        (* the name of the declared function *)
    * B.typ list          (* the types of the arguments *)
    * B.attribute

and valdef =
  VALDEF of
    vardecl               (* the type and name of the declared identifier *)
    * expr                (* the initial value *)
    * is_const            (* CONST if constant, VARIABLE otherwise *)
    * B.attribute
  | SYSDEF of
    vardecl               (* the type and name of the declared identifier *)
    * is_const            (* CONST if constant, VARIABLE otherwise *)
    * B.attribute
  | DUMMYDEF of       
    B.identifier          (* declared identifier *)
    * expr                (* inital value *)
    * B.attribute

  and is_const = CONST | VARIABLE | DEFCONST

and state =
  QUEUE of
    CS.classname          (* the class of the state *)
    * CS.queue_order      (* FIFO, or SORTED *)
    * B.identifier        (* the name of the state *)
    * CS.visibility       (* the visibility of the queue *)
    * CS.share_state       (* The share of the queue with different module*)
    * B.attribute
  | PROCESS of
  (* SURCHARGE POUR UNSHARE *)
    CS.classname          (* the class of the state *)
    * B.identifier        (* the name of the state *)
    * prev                (* the name of the previous value of the state,
			   if any *)
    * CS.visibility       (* the visibility of the process variable *)
    * CS.share_state       (* the share of the process with different module*)
    * B.attribute

and prev = NO_PREV | RTS_PREV of B.identifier | BOSSA_PREV of B.identifier

and cstate =
  CSET of
    Class_state.corename          (* the class of the state *)
    * B.identifier        (* the name of the state *)
    * B.attribute

and criteria =
  CRIT_ID of 
    key
    * crit_order
    * B.identifier (* module name *)
    * B.identifier (* variable *)
    * B.attribute
  | CRIT_TEST of
      key
      * crit_order
      * expr
      * expr
      * expr
      * B.attribute
  | IMPORT of
      B.identifier (* module name *)
      * B.attribute

and module_criteria = (* criteria in a module; no module name for vars *)
  MOD_CRIT_ID of 
    key
    * crit_order
    * B.identifier (* variable *)
    * B.attribute
  | MOD_CRIT_TEST of
      key
      * crit_order
      * expr
      * expr
      * expr
      * B.attribute

and key = 
  KEY of 
    int (* low *)
    * int (* high *)
  | NOKEY

and crit_order = HIGHEST | LOWEST

and admit =
  IMPORT_ADMIT of
    B.identifier list
    * B.attribute
  | NO_ADMIT of
    B.attribute
  | ADMIT of
    valdef list               (* encapsulated variables *)
    * vardecl list            (* criteria parameters - should be read only *)
    * expr list               (* criteria *)
    * (vardecl * stmt) option (* attach/new code *)
    * (vardecl * stmt) option (* detach/end code *)
    * B.attribute

and mod_admit =
  MOD_ADMIT of
    valdef list             (* encapsulated variables *)
    * vardecl list          (* criteria parameters - should be read only *)
    * expr                  (* criteria *)
    * (vardecl * stmt) option (* attach/new code *)
    * (vardecl * stmt) option (* detach/end code *)
    * B.attribute

and trace =
    NOTRACE
  | PRETRACE of int * trace_info list * B.attribute
  | TRACE of int * event_name list * classified_exp list * expr option *
	B.attribute

  and trace_info =
    TRACEEVENTS of event_name list
  | TRACEEXPS of classified_exp list
  | TRACETEST of expr

  and classified_exp =
    TRACEVAR of B.identifier * B.identifier * B.attribute
  | TRACEFIELD of B.identifier * B.attribute
  | TRACEQUEUE of B.identifier * B.identifier * B.attribute

and event_decl =
  EVENT of
    event_name            (* the name of the event *)
    * event_name list     (* peut etre supprim�*)
    * vardecl             (* the event parameters (always the same) *)
    * overrideable        (* is the handler overrideable *)
    * stmt                (* the body of the event handler *)
    * B.attribute

  and event_name =
    EVENT_NAME of
      B.identifier list   (* hierarchical event names *)
      * bool              (* true the name ends with * *)
      * B.attribute

and overrideable = OVERRIDEABLE | NOT_OVERRIDEABLE

and aspect_decl =
  ASPECT of
    B.identifier list     (* initial states/classes *)
    * B.identifier list   (* final states/classes *)
    * vardecl             (* the process parameters (always the same) *)
    * position
    * stmt                (* the body of the event handler *)
    * B.attribute

and position = ON | BEFORE | AFTER

and fundef =
  FUNDEF of
    B.typ                 (* the return type of the function *)
    * B.identifier        (* the name of the function *)
    * vardecl list        (* the parameter list of the function *)
    * stmt                (* the body of the function *)
    * inl                 (* INLINE if the function should be inlined *)
    * B.attribute

    and inl = INLINE | NO_INLINE
(* ------------------- The Module Structure ------------------------*)
and module_def =
    MODULE of
      B.identifier
        * state list        (* module parameters (module signature) *)
	* valdef list       (* constant declarations *)
	* typedef list      (* programmer declared types *)
        * valdef list       (* variable/global declarations *)
        * vardecl list      (* fields of the process structure *)
	* fundecl list      (* external function declarations *)
	* module_criteria list (* ordering criteria *)
	* mod_admit option  (* admission criteria *)
        * event_decl list   (* event handlers *)
        * fundef list       (* interface functions *)
        * fundef list       (* module local functions *)
        * aspect_decl list  (* aspects *)
        * B.attribute

and sched_module_def =
    SCHED_MODULE of
      B.identifier
        * bool              (* default scheduler *)
        * state list        (* states *)
        * cstate list        (* core states *)
        * uses list
        * requiresdef list  (* for globaldef *)
        * requiresdef list  (* for processdef *)
        * criteria list
	* admit
        * handlerdef list
        * functiondef list
        * B.attribute

and super_sched = MODL of module_def | SCHED of sched_module_def

and handlerdef =
    HANDLERDEF of
      event_name (* event name *)
    * event_body list
    * B.attribute
and event_body =
    EVENT_BODY of
      B.identifier (* module name *)
    * event_name (* event name *)
    * B.attribute

and functiondef =
    FUNCTIONDEF of
      B.identifier (* function name *)
    * function_body list
    * B.attribute
and function_body =
    FUNCTION_BODY of
      B.identifier (* module name *)
    * B.identifier (* function name *)
    * B.attribute

and requiresdef =
    REQUIRESDEF of
      B.identifier   (* module name *)
    * B.identifier (* variable name *)
    * B.identifier (* second module name *)
    * B.identifier (* second variable name *)
    * B.attribute
and uses =
    USES of
      B.identifier          (* module name *)
    * B.identifier list   (* module signature *)
    * B.attribute
	    
(* ------------------- Statements and expressions ----------------- *)
	    
and stmt =
    IF of
      expr                  (* test expression *)
    * stmt                (* then branch *)
    * stmt                (* else branch *)
    * B.attribute
    | NEXT of
	B.attribute
    | FOR of
	B.identifier          (* index variable *)
      * B.identifier list option (* state or class name, if any *)
      * direction           (* order in which elements are chosen *)    
      * stmt                (* loop body *)
      * B.attribute
    | SWITCH of
	expr                  (* test expression *)
      * seq_case list       (* list of cases *)
      * stmt option         (* default *)
      * B.attribute
    | SEQ of
	valdef list           (* list of local variables *)
      * stmt list           (* list of statements *)
      * B.attribute
    | RETURN of
	expr option           (* returned expression, if any *)
      * B.attribute
    | MOVE of
	expr                  (* moved process *)
      * B.identifier        (* destination state *)
      * bool                (* true if move from a state to itself allowed *)
      * state_end           (* head, tail, or anywhere *)
      * B.attribute
    | MOVEFWD of
	expr                  (* moved process *)
      * state_end           (* head, tail, or anywhere *)
      * B.attribute
  | ASSIGN of
    expr                  (* assigned location *)
    * expr                (* assigned value *)
    * B.attribute
  | DEFER of
    B.attribute
  | PRIMSTMT of           (* not in source language *)
    expr                  (* called function *)
    * expr list           (* arguments *)
    * B.attribute
  | ERROR of
      string              (* the error message *)
    * B.attribute
  | BREAK of              (* break *)
    B.attribute
  | CONTINUE of           (*continue *)
    B.attribute


  and direction = INC | DEC | RANDOM

  and state_end = HEAD | TAIL | EITHER

  and seq_case =
    SEQ_CASE of
      B.identifier list   (* list of matching state/class names *)
      * CS.state_info list(* information about all possible states *)
      * stmt              (* case statement *)
      * B.attribute

and expr =
  INT        of int * B.attribute
  | VAR      of B.identifier * B.attribute
  | FIELD_OC of B.identifier * B.identifier * B.attribute
  | FIELD    of expr * B.identifier * B.attribute
  | BOOL     of bool * B.attribute
  | UNARY    of uop * expr * B.attribute
  | BINARY   of bop * expr * expr * B.attribute
  | INDR     of expr * B.attribute
  | SELECT   of B.attribute
  | EMPTY    of B.identifier * B.attribute
  | PRIM     of expr * expr list * B.attribute
  | SCHEDCHILD of expr * B.attribute
  | SRCONSCHED of B.attribute
 (* | AREF     of expr * expr * B.attribute (* only in output *)*)
  | IN       of expr * B.identifier * B.attribute
  (*| MOVEFWDEXP of           (* not in source language *)
      expr                  (* moved process *)
      * state_end
      * B.attribute*)


  and uop = NOT | COMPLEMENT
  and bop = PLUS | MINUS | TIMES | DIV | MOD | AND | OR | BITAND | BITOR
    | EQ | NEQ | LT | GT | LEQ | GEQ | LSHIFT | RSHIFT

  and ename = PROCESSNEW | PROCESSEND |
  PROCESSYIELD | CLOCKTICK |SCHEDULE |
  PROCESSBLOCK | PROCESSUNBLOCK

(* --------------------- Constructor functions -------------------- *)

let mkSCHEDULER(nm,default,csts,enums,procflds,vars,states,cstates,crits,trace,
		events,functions,line) =
  let (funs,vars) =
    List.fold_left
      (function (funs,vars) ->
	function
	    FN(x) -> (x :: funs, vars)
	  | VL(x) -> (funs, x :: vars))
      ([],[]) vars in
  let funs = List.rev funs in
  let vars = List.rev vars in
  SCHEDULER(nm,default,csts,enums,procflds,funs,vars,states,cstates,crits,trace,
	    events,functions,[],B.mkAttr(line))

let mkENUMDEF(id,ids,line) =
  ENUMDEF(B.mkId(id),List.map B.mkId ids,B.mkAttr(line))
let mkRANGEDEF(id,lo,hi,line) =
  RANGEDEF(B.mkId(id),lo,hi,B.mkAttr(line))

let mkFUNDECL(ret,id,params,line) =
  FUNDECL(ret,B.mkId(id),params,B.mkAttr(line))

let mkVARDECL(rq,t,id,line) = VARDECL(rq,t,B.mkId(id),false,B.mkAttr(line))
let mkSYSVARDECL(rq,t,id,line) = VARDECL(rq,t,B.mkId(id),true,B.mkAttr(line))

let mkCONSTDEF(decl,exp,line) = VALDEF(decl,exp,CONST,B.mkAttr(line))
let mkVALDEF(decl,exp,line) = VALDEF(decl,exp,VARIABLE,B.mkAttr(line))
let mkSYSDEF(decl,line) = SYSDEF(decl,CONST,B.mkAttr(line))

let mkQUEUE(cls,qtyp,id,vis,share,line) =
  QUEUE(cls,qtyp,B.mkId(id),vis,share,B.mkAttr(line))
let mkPROCESS(cls,id,vis,share,line) =
  PROCESS(cls,B.mkId(id),NO_PREV,vis,share,B.mkAttr(line))
let mkPREVPROCESS(cls,id,vis,share,previd,line) =
  PROCESS(cls,B.mkId(id),BOSSA_PREV(B.mkId(previd)),vis,share,B.mkAttr(line))

let mkCRIT_ID(key,order,mn,vn,line) = 
  CRIT_ID(key,order,B.mkId(mn),B.mkId(vn),B.mkAttr(line))
let mkCRIT_TEST(key,order,tst,thn,els,line) =
  CRIT_TEST(key,order,tst,thn,els,B.mkAttr(line))
let mkIMPORT(mn,line) = IMPORT(B.mkId(mn),B.mkAttr(line))

let mkMOD_CRIT_ID(key,order,vn,line) = 
  MOD_CRIT_ID(key,order,B.mkId(vn),B.mkAttr(line))
let mkMOD_CRIT_TEST(key,order,tst,thn,els,line) =
  MOD_CRIT_TEST(key,order,tst,thn,els,B.mkAttr(line))

let mkIMPORT_ADMIT(mns,line) = IMPORT_ADMIT(List.map B.mkId mns,B.mkAttr(line))
let mkNO_ADMIT(line) = NO_ADMIT(B.mkAttr(line))

let mkMOD_ADMIT(decls,crit_params,crit,attach,detach,line) =
  MOD_ADMIT(decls,crit_params,crit,attach,detach,B.mkAttr(line))

let mkTRACE(max,info,line) = PRETRACE(max,info,B.mkAttr(line))
let mkTRACEEXP(info,line) =
  List.map (function x -> TRACEVAR(B.mkId x,B.mkId x,B.mkAttr line)) info

let mkEVENT(n,param,overrideable,s,l) =
  EVENT(n,[],param,overrideable,s,B.mkAttr l)

let mkEVENT_NAME(ids,star,line) =
  EVENT_NAME(List.map (B.mkId) ids,star,B.mkAttr(line))

let mkASPECT(src,tgt,param,pos,s,l) =
  ASPECT(List.map B.mkId src,List.map B.mkId tgt,param,pos,s,B.mkAttr l)

let mkFUNDEF(t,id,params,stm,line) =
  FUNDEF(t,B.mkId(id),params,stm,INLINE,B.mkAttr(line))

let mkIF(exp,stm1,stm2,line) = IF(exp,stm1,stm2,B.mkAttr(line))
let mkFOR(id,state_ids,stm,line) =
  FOR(B.mkId(id),Some(List.map B.mkId state_ids),RANDOM,stm,B.mkAttr(line))
let mkFORINC(id,state_id,stm,line) =
  FOR(B.mkId(id),Some([B.mkId(state_id)]),INC,stm,B.mkAttr(line))
let mkFORDEC(id,state_id,stm,line) =
  FOR(B.mkId(id),Some([B.mkId(state_id)]),DEC,stm,B.mkAttr(line))
let mkFOR_ANY(id,stm,line) =
  FOR(B.mkId(id),None,RANDOM,stm,B.mkAttr(line))
let mkSWITCH(exp,cases,line) = SWITCH(exp,cases,None,B.mkAttr(line))
let mkSEQ(decls,stms,line) = SEQ(decls,stms,B.mkAttr(line))
let mkRETURN(exp,line) = RETURN(Some(exp),B.mkAttr(line))
let mkRETURNVOID(line) = RETURN(None,B.mkAttr(line))
(******************************************************************************)
let mkNEXT(line) = NEXT(B.mkAttr(line))
let mkMODULE(name,arg,constdef,typedef,globaldef,vardecl,criteria,admit,
	     event,interf,aspect,line) =
  
  let (funs,vars) =
    List.fold_left
      (function (funs,vars) ->
	function
	    FN(x) -> (x :: funs, vars)
	  | VL(x) -> (funs, x :: vars))
      ([],[]) globaldef in
  let funs = List.rev funs in
  let vars = List.rev vars in
  MODULE(B.mkId(name),arg,constdef,typedef,vars,vardecl,funs,criteria,admit,
	 event,interf,[],aspect,B.mkAttr(line))
  
let mkSCHED_MODULE(nm,id_default,state,cstate,uses,global,proc,order,adm,hd,fn,ln) =
  SCHED_MODULE(B.mkId(nm),id_default,state,cstate,uses,global,proc,order,adm,hd,fn,
	       B.mkAttr(ln))
let mkUSES(name, sn,ln) = 
  USES(B.mkId(name),List.map (B.mkId) sn,B.mkAttr(ln))
let mkREQUIRESDEF(nm1, nv1, nm2, nv2, ln) = 
  REQUIRESDEF(B.mkId(nm1), B.mkId(nv1), B.mkId(nm2), B.mkId(nv2), B.mkAttr(ln))

let print2  = function EVENT_NAME(nl,_,_) ->
  List.iter
    (function id ->
      Printf.printf"Event name = %s\n"(Objects.id2c id))
    nl

let print evtn evtbl = 
  (print2 evtn);
  List.iter (function EVENT_BODY(mn,et,_) ->
    Printf.printf"Module name MAST = %s\n"(Objects.id2c mn);(print2 et) )evtbl


  
let mkHANDLERDEF(et,evb,ln) =
  HANDLERDEF(et,evb,B.mkAttr(ln)) 
let mkEVENT_BODY(mn,et,ln) =
  EVENT_BODY(B.mkId(mn),et,B.mkAttr(ln))
let mkFUNCTIONDEF(et,evb,ln) =
  FUNCTIONDEF(B.mkId et,evb,B.mkAttr(ln)) 
let mkFUNCTION_BODY(mn,et,ln) =
  FUNCTION_BODY(B.mkId(mn),B.mkId et,B.mkAttr(ln))
let mkFIELD_OC (mn,vn,ln) =
  FIELD_OC(B.mkId(mn),B.mkId(vn),B.mkAttr(ln))
(******************************************************************************)
let mkMOVE(exp,id,line) =
  MOVE(exp,B.mkId(id),true,EITHER,B.mkAttr(line))
let mkMOVEHD(exp,id,line) =
  MOVE(exp,B.mkId(id),true,HEAD,B.mkAttr(line))
let mkMOVETL(exp,id,line) =
  MOVE(exp,B.mkId(id),true,TAIL,B.mkAttr(line))
let mkMOVEFWD(exp,line) = MOVEFWD(exp,EITHER,B.mkAttr(line))
let mkMOVEHDFWD(exp,line) = MOVEFWD(exp,HEAD,B.mkAttr(line))
let mkMOVETLFWD(exp,line) = MOVEFWD(exp,TAIL,B.mkAttr(line))
let mkASSIGN(loc,exp,line) = ASSIGN(loc,exp,B.mkAttr(line))
let mkDEFER(line) = DEFER(B.mkAttr(line))

let mkSEQ_CASE(ids,stm,line) =
  SEQ_CASE(List.map (B.mkId) ids,[],stm,B.mkAttr(line))

let mkPRIMSTMT(id,exprs,line) =
  PRIMSTMT(VAR(B.mkId(id),B.mkAttr(line)),exprs,B.mkAttr(line))
let mkERROR(str,line) = ERROR(str,B.mkAttr(line))
let mkBREAK(line) = BREAK(B.mkAttr(line))
let mkCONTINUE(line) = CONTINUE(B.mkAttr(line))

let mkINT(vl,line) = INT(vl,B.mkAttr(line))
let mkVAR(id,line) = VAR(B.mkId(id),B.mkAttr(line))
let mkFIELD(exp,id,line) = FIELD(exp,B.mkId(id),B.mkAttr(line))
let mkBOOL(vl,line) = BOOL(vl,B.mkAttr(line))
let mkUNARY(op,exp,line) = UNARY(op,exp,B.mkAttr(line))
let mkBINARY(op,exp1,exp2,line) = BINARY(op,exp1,exp2,B.mkAttr(line))
let mkINDR(exp,line) = INDR(exp,B.mkAttr(line))
let mkSELECT(line) = SELECT(B.mkAttr(line))

let cur_test_index = ref 0
let mk_test_index _ =
  cur_test_index := !cur_test_index + 1;
  !cur_test_index

let mkEMPTY(id,line) = EMPTY(B.mkId(id),B.mkTestAttr(line,mk_test_index()))
let mkEMPTYCLASS(id,line) =
  EMPTY(B.mkId(Class_state.class2c(id)),
	B.mkTestAttr(line,mk_test_index()))
let mkPRIM(id,exprs,line) =
  PRIM(VAR(B.mkId(id),B.mkAttr(line)),exprs,B.mkAttr(line))
let mkSCHEDCHILD(expr,line) = SCHEDCHILD(expr,B.mkAttr(line))
let mkSRCONSCHED(line) = SRCONSCHED(B.mkAttr(line))
let mkIN(exp,id,line) =
  IN(exp,B.mkId(id),B.mkTestAttr(line,mk_test_index()))
let mkINCLASS(exp,id,line) =
  IN(exp,B.mkId(Class_state.class2c(id)),
     B.mkTestAttr(line,mk_test_index()))

let mkCOMPOUND_ASSIGN(exp,op,rarg,line) =
  let rec extract = function
      VAR(id,line) -> None
    | FIELD(exp,id,line) ->
	(match extract exp with
	  None -> None
	| Some(rename,flds) -> Some(rename,id::flds)) 
    | _ -> raise (Error.Error("internal error")) in
  match extract exp with
    None -> mkASSIGN(exp,mkBINARY(op,exp,rarg,line),line)
  | Some(rename,flds) ->
      let newid = B.fresh_id() in
      let newvar = VAR(newid,B.mkAttr(line)) in
      mkSEQ([DUMMYDEF(newid,exp,B.mkAttr(line))],
	    [ASSIGN(newvar,BINARY(op,newvar,rarg,B.mkAttr(line)),
		    B.mkAttr(line))],
	    line)

(* ----------------------- Accessor functions --------------------- *)

let get_exp_attr = function
  INT(_,a) -> a
  | VAR(_,a) -> a
  | FIELD_OC(_,_,a)-> a
  | FIELD(_,_,a) -> a
  | BOOL(_,a) -> a
  | UNARY(_,_,a) -> a
  | BINARY(_,_,_,a) -> a
  | INDR(_,a) -> a
  | SELECT(a) -> a
  | EMPTY(_,a) -> a
  | PRIM(_,_,a) -> a
  | SCHEDCHILD(_,a) -> a
  | SRCONSCHED(a) -> a
  | IN(_,_,a) -> a

let get_stm_attr = function
  IF(_,_,_,a) -> a
  | NEXT(a) -> a
  | FOR(_,_,_,_,a) -> a
  | SWITCH(_,_,_,a) -> a
  | SEQ(_,_,a) -> a
  | RETURN(_,a) -> a
  | MOVE(_,_,_,_,a) -> a
  | MOVEFWD(_,_,a) -> a
  | ASSIGN(_,_,a) -> a
  | DEFER(a) -> a
  | PRIMSTMT(_,_,a) -> a
  | ERROR(_,a) -> a
  | BREAK(a) -> a
  | CONTINUE(a) -> a

(* ------------------------- Useful syntax ------------------------ *)

let mkBLOCK attr = function
    [stm] -> stm
  | stms -> SEQ([],stms,attr)

let mkDBLOCK attr = function
    ([],[stm]) -> stm
  | (decls,stms) -> SEQ(decls,stms,attr)

let addBLOCK attr stm1 = function
    SEQ([],stms,attr) -> SEQ([],stm1::stms,attr)
  | stm2 -> mkBLOCK attr [stm1;stm2]

let postBLOCK attr stm1 end_list =
  match stm1 with
    SEQ([],stms,attr) -> SEQ([],stms@end_list,attr)
  | stm2 -> mkBLOCK attr (stm1::end_list)

let add_decl_block decl = function
    SEQ(d,s,a) -> SEQ(decl::d,s,a)
  | s -> SEQ([decl],[s],get_stm_attr s)

(* ---------------------------- Comments -------------------------- *)

let add_comment comment = function
  INT(x,a) -> INT(x,B.add_comment a comment)
  | VAR(x,a) -> VAR(x,B.add_comment a comment)
  | FIELD_OC(x,y,a)-> FIELD_OC(x,y,B.add_comment a comment) 
  | FIELD(x,y,a) -> FIELD(x,y,B.add_comment a comment)
  | BOOL(x,a) -> BOOL(x,B.add_comment a comment)
  | UNARY(x,y,a) -> UNARY(x,y,B.add_comment a comment)
  | BINARY(x,y,m,a) -> BINARY(x,y,m,B.add_comment a comment)
  | INDR(x,a) -> INDR(x,B.add_comment a comment)
  | SELECT(a) -> SELECT(B.add_comment a comment)
  | EMPTY(x,a) -> EMPTY(x,B.add_comment a comment)
  | PRIM(x,y,a) -> PRIM(x,y,B.add_comment a comment)
  | SCHEDCHILD(x,a) -> SCHEDCHILD(x,B.add_comment a comment)
  | SRCONSCHED(a) -> SRCONSCHED(B.add_comment a comment)
  | IN(x,y,a) -> IN(x,y,B.add_comment a comment)

(* ----------------------- Printing functions --------------------- *)

let event_name2c = function
    EVENT_NAME(ids,true,_) -> (String.concat "." (List.map B.id2c ids))^".*"
  | EVENT_NAME(ids,_,_) ->    (String.concat "." (List.map B.id2c ids))

let event_name2c_ (EVENT_NAME(ids,_,_)) =
  (String.concat "_" (List.map B.id2c ids))

let event_name2c1 = function
    EVENT_NAME(id::_,_,_) -> Some(B.id2c id)
  | EVENT_NAME(ids,_,_) -> None

let event_name2c2 = function
    EVENT_NAME(id1::id2::_,_,_) -> Some((B.id2c id1)^"."^(B.id2c id2))
  | EVENT_NAME(ids,_,_) -> None

let event_name_equal
    (EVENT_NAME(nmlst1,star1,_))
    (EVENT_NAME(nmlst2,star2,_)) =
  nmlst1 = nmlst2 && star1 = star2

let event_names_equal en1 en2 =
  (List.length en1 = List.length en2) &&
  (List.for_all2 event_name_equal en1 en2)

let event_name_prefix
    (EVENT_NAME(nmlst1,star1,_) as e1)
    (EVENT_NAME(nmlst2,star2,_) as e2) =
  if star1
  then
    let rec prefix = function
	([],y) -> true
      | (x::xs,y::ys) -> x = y && prefix (xs, ys)
      |	_ -> false in
    prefix (nmlst1, nmlst2)
  else event_name_equal e1 e2

let rec take = function
    (0,_) -> []
  | (n,[]) -> raise (Failure "take")
  | (n,x::xs) -> x::(take(n-1,xs))


let list_of_const_enum = ref []

let event_name2partial_mask n (EVENT_NAME(l,star,_)) =
  if List.mem
      ("EVENT_"^(String.concat "_" (List.map (function x -> String.uppercase (B.id2c x)) (take(n+1,l)))))
      !list_of_const_enum
  then ()
  else
    list_of_const_enum :=
      !list_of_const_enum @
      [("EVENT_"^(String.concat "_"
		    (List.map (function x -> String.uppercase (B.id2c x))
		       (take(n+1,l)))))];
  Printf.sprintf "EVENT_%s"
    (String.concat "_"
       (List.map (function x -> String.uppercase (B.id2c x)) (take(n+1,l))))

let event_name2mask (EVENT_NAME(l,star,_) as event) =
  event_name2partial_mask ((List.length l)-1) event

and bop2c = function
    PLUS -> "+"
  | MINUS -> "-"
  | TIMES -> "*"
  | DIV -> "/"
  | MOD -> "%"
  | AND -> "&&"
  | OR -> "||"
  | BITAND -> "&"
  | BITOR -> "|"
  | EQ -> "=="
  | NEQ -> "!="
  | LT -> "<"
  | GT -> ">"
  | LEQ -> "<="
  | GEQ -> ">="
  | LSHIFT -> "<<"
  | RSHIFT -> ">>"


let rec equal_except_attributes e1 = function
    INT(v,a) ->
      (match e1 with
	INT(v1,a) -> v = v1
      |	_ -> false)
  | VAR(id,a) ->
      (match e1 with
	VAR(id1,a) -> B.ideq(id,id1)
      |	_ -> false)
  | FIELD_OC(modl,id,a) ->
      (match e1 with
	FIELD_OC(modl1,id1,a) -> B.ideq(modl,modl1) && B.ideq(id,id1)
      |	_ -> false)
  | FIELD(exp,field,a) ->
      (match e1 with
	FIELD(exp1,field1,a) ->
	  equal_except_attributes exp exp1 && B.ideq(field,field1)
      |	_ -> false)
  | BOOL(b,a) ->
      (match e1 with
	BOOL(b1,a) -> b = b1
      |	_ -> false)
  | UNARY(uop,exp,a) ->
      (match e1 with
	UNARY(uop1,exp1,a) -> uop = uop1 && equal_except_attributes exp exp1
      |	_ -> false)
  | BINARY(bop,exp1,exp2,a) ->
      (match e1 with
	BINARY(bopa,exp1a,exp2a,a) ->
	  bop = bopa && equal_except_attributes exp1 exp1a &&
	  equal_except_attributes exp2 exp2a
      |	_ -> false)
  | INDR(exp,a) ->
      (match e1 with
	INDR(exp1,a) -> equal_except_attributes exp exp1
      |	_ -> false)
  | SELECT(a) ->
      (match e1 with
	SELECT(a) -> true
      |	_ -> false)
  | EMPTY(stateid,a) ->
      (match e1 with
	EMPTY(stateid1,a) -> B.ideq(stateid,stateid1)
      |	_ -> false)
  | PRIM(fn,args,a) ->
      (match e1 with
	PRIM(fn1,args1,a) -> equal_except_attributes fn fn1 &&
	  List.for_all2 equal_except_attributes args args1
      |	_ -> false)
  | SCHEDCHILD(child,a) ->
      (match e1 with
	SCHEDCHILD(child1,a) -> equal_except_attributes child child1
      |	_ -> false)
  | SRCONSCHED(a) ->
      (match e1 with
	SRCONSCHED(a) -> true
      |	_ -> false)
  | IN(exp,id,a) ->
      (match e1 with
	IN(exp1,id1,a) ->
	  equal_except_attributes exp exp1 && B.ideq(id,id1)
      |	_ -> false)

(* --------------------- Special process names --------------------- *)

exception LookupErrException

let rec lookup x = function
    [] -> raise LookupErrException
  | ((y,v)::r) -> if x = y then v else lookup x r

let is_src_or_tgt exp fld (srcenv,nonempty_srcs) =
  let nonempty_srcs = List.sort compare nonempty_srcs in
  match B.ty(get_exp_attr(exp)) with
    B.PEVENT | B.CEVENT ->
      let str = B.id2c fld in
      if str = "source"
      then
	(try VB.VP(VB.SRC,lookup VB.SRC srcenv)
	with LookupErrException -> VB.VP(VB.SRC,nonempty_srcs))
      else
	if str = "target"
	then
	  (try VB.VP(VB.TGT,lookup VB.TGT srcenv)
	  with LookupErrException -> VB.VP(VB.TGT,nonempty_srcs))
	else VB.VP(VB.UNKNOWNPROC,nonempty_srcs)
  | _ -> VB.VP(VB.UNKNOWNPROC,nonempty_srcs)

(* ----------------------------- Timers ---------------------------- *)

let collect_timer_names procdecls valdefs =
  let check_vardecl = function
      VARDECL(_,B.TIMER(_),id,_,_) -> Some id
    | _ -> None in
  let timer_procdecls = Aux.option_filter check_vardecl procdecls in
  let timer_valdefs =
    Aux.option_filter
      (function
	  VALDEF(decl,_,_,_) -> check_vardecl decl
	| SYSDEF(decl,_,_) -> check_vardecl decl
	| _ -> None)
      valdefs in
  if not (Aux.intersect timer_procdecls timer_valdefs = [])
  then raise (Error.Error ("timer-typed variables must be distinct from "^
			   "timer-typed process-structure fields"));
  timer_procdecls @ timer_valdefs

(* ---------------------- Variable environments --------------------- *)

let params2ids params = List.map (function VARDECL(_,_,id,_,_) -> id) params

let valdefs2ids valdefs =
  List.map
    (function
	VALDEF(VARDECL(_,_,id,_,_),_,_,_)
      |	SYSDEF(VARDECL(_,_,id,_,_),_,_)
      |	DUMMYDEF(id,_,_) -> id)
    valdefs
