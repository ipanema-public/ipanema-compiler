module B = Objects
module VB = Vobjects
module CS = Class_state

let error attr str =
  Error.update_error();
  Printf.eprintf "%s: %s\n" (B.loc2c attr) str

let stopError attr str =
  error attr str;
  raise (Error.Error str)

(* -------------------- The scheduler structure ------------------- *)
type fcttype = DFT | TRYLOCK | SYSTEM

type scheduler =
  SCHEDULER of
    string                (* name *)
    * valdef list         (* constant declarations *)
    * typedef list        (* programmer declared types *)
    * vardecl list        (* fields of the domain structure *)
    * vardecl list        (* fields of the group structure *)
    * vardecl list        (* fields of the process structure *)
    * fundecl list        (* external function declarations *)
    * valdef list         (* variable declarations *)
    * domains option      (* description of how to build scheduling domains/groups *)
    * pcstate             (* global or per-core process states *)
    * cstate list         (* core states *)
    * criteria list       (* ordering criteria *)
    * trace               (* tracing declaration *)
    * event_decl list     (* event handlers *)
    * event_decl list     (* core event handlers *)
    * fundef list         (* interface functions *)
    * fundef list         (* user-defined functions *)
    * B.attribute

and typedef =
   STRUCTDEF of
    B.identifier          (* the name of the declared type *)
    * vardecl list        (* the list of the fields *)
    * B.attribute
  | ENUMDEF of
    B.identifier          (* the name of the declared type *)
    * B.identifier list   (* the list of possible values *)
    * B.attribute
  | ENUMVALDEF of         (* not from the source program *)
    B.identifier          (* the name of the declared type *)
    * (B.identifier * expr) list   (* the list of possible values *)
    * B.attribute
  | RANGEDEF of
    B.identifier          (* the name of the declared type *)
    * expr                      (* the lower bound *)
    * expr                      (* the upper bound *)
    * B.attribute
  | TIMERDEF of
    B.identifier          (* the name of the declared type *)
    * vardecl list        (* fields of the timer structure *)
    * B.attribute

and vardecl =
    VARDECL of
    B.typ                 (* the type of the declared identifier *)
    * B.identifier        (* the name of the declared identifier *)
    * bool                (* true if imported from the system    *)
    * bool                (* true if lazy computed               *)
    * CS.share_state      (* SHARED if the field is shared       *)
    * expr option         (* the lazy/init expression, if any    *)
    * B.attribute

(* useful to avoid a parser problem *)
and pre_valdef = FN of fundecl | VL of valdef

and fundecl =
  FUNDECL of
    fcttype               (* normal, trylock or system function *)
    * B.typ               (* the return type *)
    * B.identifier        (* the name of the declared function *)
    * B.typ list          (* the types of the arguments *)
    * B.attribute

and valdef =
  VALDEF of
    vardecl               (* the type and name of the declared identifier *)
    * expr                (* the initial value *)
    * is_const            (* CONST if constant, VARIABLE otherwise *)
    * B.attribute
  | UNINITDEF of          (* for temporaries introduced by the compiler *)
    vardecl               (* the type and name of the declared identifier *)
    * B.attribute         (* can't be constant *)
  | SYSDEF of
    vardecl               (* the type and name of the declared identifier *)
    * is_const            (* CONST if constant, VARIABLE otherwise *)
    * B.attribute
  | DUMMYDEF of          (* used by parser, when type not known (a const) *)
    B.identifier          (* declared identifier *)
    * expr                (* inital value *)
    * B.attribute

  and is_const = CONST | CONSTVAR | VARIABLE | DEFCONST

(* Structure capturing the core/process states information,
 * including core variables *)
and pcstate =
   CORE of
        vardecl list
     * state list
     * steal_param option
   | PSTATE of
         state list

and steal_param = string * string * string * steal
and steal =
    FLAT_STEAL_GROUP of steal_group * steal_thread * stmt list
  | ITER_STEAL_GROUP of steal_group * steal_thread * stmt list * stmt list
  | STEAL_THREAD of steal_thread

and steal_group = filter * select_grp * expr
and steal_thread =
    FLAT_STEAL_THREAD of steal_thread_blk
  | ITER_STEAL_THREAD of steal_thread_blk * expr * stmt list

and steal_thread_blk = filter * select * migration

and filter = (vardecl * vardecl * stmt)
and select_grp = (vardecl * vardecl * vardecl * stmt)
and select = (vardecl * vardecl * stmt)
and migration =
  (*
    - stolen core
    - thief core
    - stolen group - optional
    - stolen thread
    - migration condition
    - stopping condition
    - internal statement to split migration in 2 phases
  *)
  (vardecl * vardecl * vardecl option * vardecl * stmt * expr * stmt)

and state =
  QUEUE of
    CS.classname          (* the class of the state *)
    * CS.share_state      (* whether state is shared *)
    * CS.queue_order      (* FIFO, or SORTED *)
    * B.identifier        (* the name of the state *)
    * CS.visibility       (* the visibility of the queue *)
    * criteria  option    (* sorting key of the queue, optionally*)
    * B.attribute
  | PROCESS of
    CS.classname          (* the class of the state *)
    * CS.share_state      (* whether state is shared *)
    * B.identifier        (* the name of the state *)
    * prev                (* the name of the previous value of the state,
			   if any *)
    * CS.visibility       (* the visibility of the process variable *)
    * B.attribute

and cstate =
    CSTATE of
      Class_state.corename   (* the class of the core *)
      * Objects.identifier   (* the name of the core *)
      * Objects.attribute
    | CSET of
	Class_state.corename (* the class of the core *)
	* Objects.identifier (* the name of the core *)
	* Objects.attribute

and domains = string * string * domain list
and domain =
    DOMFOR of string * int option * domain list
  | DOMDOM of string * expr * string * domgrp
and domgrp = string * string * expr * string

and prev = NO_PREV | RTS_PREV of B.identifier | BOSSA_PREV of B.identifier

and criteria =
  | CRIT_ID of
      key
      * crit_order
      * B.identifier            (* field used as criteria *)
      * B.identifier option     (* id of comparison function (generated) *)
      * B.attribute
  | CRIT_EXPR of
      key
      * crit_order
      * expr                    (* expr used as criteria *)
      * B.identifier option     (* id of comparison function (generated) *)
      * B.attribute

and key = KEY of int (* low *) * int (* high *) | NOKEY

and crit_order = HIGHEST | LOWEST

and trace =
    NOTRACE
  | PRETRACE of int * trace_info list * B.attribute
  | TRACE of int * event_name list * classified_exp list * expr option *
	B.attribute

  and trace_info =
    TRACEEVENTS of event_name list
  | TRACEEXPS of classified_exp list
  | TRACETEST of expr

  and classified_exp =
    TRACEVAR of B.identifier * B.identifier * B.attribute
  | TRACEFIELD of B.identifier * B.attribute
  | TRACEQUEUE of B.identifier * B.identifier * B.attribute

and event_decl =
  EVENT of
    event_name            (* the name of the event *)
    * vardecl             (* the event parameters (always the same) *)
    * stmt                (* the body of the event handler *)
    * bool                (* is the event called with mutex *)
    * B.attribute

  and event_name =
    EVENT_NAME of
      B.identifier (* hierarchical event names *)
      * B.attribute

and fundef =
  FUNDEF of
    fcttype               (* normal, trylock or system function *)
    * B.typ               (* the return type of the function *)
    * B.identifier        (* the name of the function *)
    * vardecl list        (* the parameter list of the function *)
    * stmt                (* the body of the function *)
    * inl                 (* INLINE if the function should be inlined *)
    * B.attribute

    (* EXPORTED is not inlined and visible to parents and children *)
    and inl = INLINE | NO_INLINE | EXPORTED

(* ------------------- Statements and expressions ----------------- *)

and stmt =
  IF of
    expr                  (* test expression *)
    * stmt                (* then branch *)
    * stmt                (* else branch *)
    * B.attribute
  | FOR of
    B.identifier          (* index variable *)
    * expr list option    (* state or class name, if any *)
    * CS.state_info list  (* information about possible states *)
    * direction           (* order in which elements are chosen *)
    * stmt                (* loop body *)
    * criteria option     (* optional ordering (expression, highest/lowest FIXME: merge with direction? *)
    * B.attribute
  | FOR_WRAPPER of        (* used to group fors sharing the same break *)
      B.identifier        (* break label *)
      * stmt list         (* list of for loops *)
      * B.attribute
  | SWITCH of
    expr                  (* test expression *)
    * seq_case list       (* list of cases *)
    * stmt option         (* default *)
    * B.attribute
  | SEQ of
    valdef list           (* list of local variables *)
    * stmt list           (* list of statements *)
    * B.attribute
  | RETURN of
    expr option           (* returned expression, if any *)
    * B.attribute
  | STEAL of
    expr                  (* core expression of the thief *)
    * B.attribute
  | MOVE of
      expr                (* moved process *)
    * expr                (* destination state *)
    * CS.state_info option(* info about state of source, if known *)
    * CS.state_info list  (* information about the source collected in the
			     verifier *)
    * CS.state_info option(* info about state of target, if known *)
    * bool                (* true if move from a state to itself allowed *)
    * state_end           (* head, tail, or anywhere *)
    * B.attribute
  | MOVEFWD of
    expr                  (* moved process *)
    * (CS.state_info * CS.state_info) list
                          (* information about the sources and dests collected
			     in the verifier *)
    * state_end           (* head, tail, or anywhere *)
    * B.attribute
  (* The following is because a VS may request that some thing (eg the
     scheduler state) might be reprocessed up the hierarchy.  In this case,
     a next list is sent that doesn't terminate with a process scheduler.
     So we have to check in the function whether the next list really
     continues.  It is not really very nice to combine this with forward,
     but it seemed like the easiest solution.  Creating a new primitive would
     require redoing the analysis that figures out what next means.  At the
     moment, this is only used in the implicitly defined function
     compute_state, where doing the test with the forward suffices. *)
  | SAFEMOVEFWD of
    expr                  (* moved process *)
    * (CS.state_info * CS.state_info) list
                          (* information about the sources and dests collected
			     in the verifier *)
    * stmt                (* default action *)
    * state_end           (* head, tail, or anywhere *)
    * B.attribute
  | ASSIGN of
    expr                  (* assigned location *)
    * expr                (* assigned value *)
    * bool                (* can the assignment affect a field of something
			     on a sorted queue *)
    * B.attribute
  | DEFER of
    B.attribute
  | PRIMSTMT of           (* not in source language *)
    expr                  (* called function *)
    * expr list           (* arguments *)
    * B.attribute
  | ERROR of
      string              (* the error message *)
    * B.attribute
  | BREAK of              (* break *)
    B.attribute
  | CONTINUE of              (* continue *)
    B.attribute
  | ASSERT of             (* not in source language *)
    expr                  (* expression asserted to be true *)
    * B.attribute

  and direction = INC | DEC | RANDOM

  and state_end = HEAD | TAIL | EITHER

  and seq_case =
    SEQ_CASE of
        B.identifier list (* list of matching state/class names *)
      * CS.state_info list(* information about all possible states *)
      * stmt              (* case statement *)
      * B.attribute

  and expr =
    SELF     of B.attribute
  | SUM      of B.attribute
  | MIN	     of B.attribute
  | MAX	     of B.attribute
  | COUNT    of B.attribute
  | FNOR     of B.attribute
  | DISTANCE of B.attribute
  | INT      of int * B.attribute
  | VAR      of B.identifier * (string list) * B.attribute
  | FIELD    of expr * B.identifier * (string list) * B.attribute
  | CAST     of B.typ * expr * B.attribute
  | TYPE     of B.typ * B.attribute
  | LCORE    of string * expr * expr option * B.attribute
  | BOOL     of bool * B.attribute
  | PARENT   of B.attribute
  | UNARY    of uop * expr * B.attribute
  | BINARY   of bop * expr * expr * B.attribute
  | TERNARY  of expr * expr * expr * B.attribute
  | PBINARY  of bop * expr * CS.pstate_info list * expr * CS.pstate_info list
    * B.attribute
  | CONDOP   of expr * expr * expr * B.attribute
  | INDR     of expr * B.attribute
  | SELECT   of CS.state_info option * B.attribute
  | FIRST    of expr * criteria option * CS.state_info option * B.attribute
  | PFIRST   of expr * criteria option * CS.pstate_info option * B.attribute
  | CFIRST   of expr * criteria option * CS.cstate_info option * B.attribute
  | EMPTY    of B.identifier * CS.state_info list * B.testval * bool *
	        B.attribute
  | VALID    of expr * CS.state_info option * B.attribute
  | PRIM     of expr * expr list * B.attribute
  | SCHEDCHILD of expr * VB.vprocname list * B.attribute
  | SRCONSCHED of B.attribute
  | ALIVE    of B.testval * bool (* insrc *) * B.attribute
  | AREF     of expr * expr option * B.attribute (* only in output *)
  | IN       of expr * expr * CS.state_info list * B.testval *
	        bool (* true if from the src program *) * criteria option * B.attribute
  | MOVEFWDEXP of           (* not in source language *)
      expr                  (* moved process *)
      * state_end
    * B.attribute
  | INT_STRING of string * B.attribute
  | INT_NULL of B.attribute

  and uop = NOT | COMPLEMENT
  and bop = PLUS | MINUS | TIMES | DIV | MOD | AND | OR | BITAND | BITOR
    | EQ | NEQ | LT | GT | LEQ | GEQ | LSHIFT | RSHIFT

  and ename = PROCESSNEW | PROCESSEND |
  PROCESSYIELD | CLOCKTICK |SCHEDULE |
  PROCESSBLOCK | PROCESSUNBLOCK

(* --------------------- Helper function -------------------- 
 For:
   - Compile_migration
   - Compile_steal
*)

let get_init = function
    B.BOOL -> BOOL(false, B.dum __LOC__)
  | B.INT -> INT(0, B.dum __LOC__)
  | B.PROCESS -> INT(0, B.dum __LOC__)
  | _ -> failwith (__LOC__^": Add initial value for more types...")
   
(* --------------------- Constructor functions -------------------- *)

let mkSCHEDULER(nm,csts,enums,domgrp,procflds,vars,domains,pstates,cstates,crits,
		trace,events,cevents,ifunctions,functions,line) =
  let (funs,vars) =
    List.fold_left
      (function (funs,vars) ->
	function
	    FN(x) -> (x :: funs, vars)
	  | VL(x) -> (funs, x :: vars))
      ([],[]) vars in
  let funs = List.rev funs in
  let vars = List.rev vars in
  let doms = List.filter (fun (dom, body) -> dom) domgrp
  and grps = List.filter (fun (dom, body) -> not dom) domgrp in
  if List.length doms > 1 then
    error (B.mkAttr line) "More than a single scheduling domain definition !";
  if List.length grps > 1 then
    error (B.mkAttr line) "More than a single scheduling group definition !";
  if List.length doms = 0 then
    stopError (B.mkAttr line) "No scheduling domain definition !";
  if List.length grps = 0 then
    stopError (B.mkAttr line) "No scheduling group definition !";
  let dom:vardecl list = snd(List.hd doms)
  and grp:vardecl list = snd(List.hd grps) in
  SCHEDULER(nm,csts,enums,dom, grp,
	    procflds,funs,vars,domains,pstates,cstates,
	    crits,trace,events,cevents,ifunctions,functions,B.mkAttr(line))

let mkSTRUCTDEF(id,flds,line) =
  STRUCTDEF(B.mkId(id), flds,B.mkAttr(line))
let mkENUMDEF(id,ids,line) =
  ENUMDEF(B.mkId(id),List.map B.mkId ids,B.mkAttr(line))
let mkRANGEDEF(id,lo,hi,line) =
  RANGEDEF(B.mkId(id),lo,hi,B.mkAttr(line))
let mkTIMERDEF(id,decls,line) = TIMERDEF(B.mkId(id),decls,B.mkAttr(line))

let mkFUNDECL(kind,ret,id,params,line) =
  FUNDECL(kind,ret,B.mkId(id),params,B.mkAttr(line))

let mkVARDECL(s,t,id,line) = VARDECL(t,B.mkId(id),false, false, s, None, B.mkAttr(line))
let mkSYSVARDECL(s,t,id,line) = VARDECL(t,B.mkId(id),true, false, s, None, B.mkAttr(line))
let mkLAZYVARDECL(s,t,id,exp,line) =
  VARDECL(t,B.mkId(id), false, true, s, Some exp, B.mkAttr(line))
let mkINITVARDECL(s,t,id,exp,line) =
  VARDECL(t,B.mkId(id), false, false, s,Some exp, B.mkAttr(line))

let mkCONSTDEF(decl,exp,line) = VALDEF(decl,exp,DEFCONST,B.mkAttr(line))
let mkVALDEF(decl,exp,line) = VALDEF(decl,exp,VARIABLE,B.mkAttr(line))
let mkSYSDEF(decl,line) = SYSDEF(decl,CONST,B.mkAttr(line))


let mkQUEUE(cls,shared,qtyp,id,vis,crit,line) = QUEUE(cls,shared,qtyp,B.mkId(id),vis,crit,B.mkAttr(line))
let mkPROCESS(cls,shared,id,vis,line) =
  PROCESS(cls,shared,B.mkId(id),NO_PREV,vis,B.mkAttr(line))
let mkPREVPROCESS(cls,id,vis,previd,line) =
  PROCESS(cls,CS.UNSHARED, B.mkId(id),BOSSA_PREV(B.mkId(previd)),vis,B.mkAttr(line))

let mkCORE(vardecl_list, state_list, steal) = CORE(vardecl_list, state_list, steal)
let mkPSTATE(state_list) = PSTATE(state_list)

let mkCSTATE(cls,id,line) =
  CSTATE(cls,B.mkId(id),B.mkAttr(line))
let mkCSET(cls,id,line) =
  CSET(cls,B.mkId(id),B.mkAttr(line))

let mkCRIT_ID(key, order,id,line) = CRIT_ID(key, order,B.mkId(id),None,B.mkAttr(line))
let mkCRIT_EXPR(key, order,expr,line) = CRIT_EXPR(key, order, expr, None,B.mkAttr(line))

let mkTRACE(max,info,line) = PRETRACE(max,info,B.mkAttr(line))
let mkTRACEEXP(info,line) =
  List.map (function x -> TRACEVAR(B.mkId x,B.mkId x,B.mkAttr line)) info

let mkEVENT(n,param,s,b,l) = EVENT(n,param,s,b,B.mkAttr l)

let mkEVENT_NAME(id,line) =
  EVENT_NAME(B.mkId id,B.mkAttr(line))

let mkFUNDEF(kind,t,id,params,stm,line) =
  FUNDEF(kind,t,B.mkId(id),params,stm,INLINE,B.mkAttr(line))
let mkEXPORT_FUNDEF(t,id,params,stm,line) =
  FUNDEF(DFT,t,B.mkId(id),params,stm,EXPORTED,B.mkAttr(line))

let mkSELF(line) = SELF(B.mkAttr(line))
let mkINT(vl,line) = INT(vl,B.mkAttr(line))
let mkVAR(id,ssa,line) = VAR(B.mkId(id),ssa,B.mkAttr(line))
let mkFIELD(exp,id,ssa,line) = FIELD(exp,B.mkId(id),ssa,B.mkAttr(line))
let mkBOOL(vl,line) = BOOL(vl,B.mkAttr(line))
let mkPARENT(line) = PARENT(B.mkAttr(line))
let mkUNARY(op,exp,line) = UNARY(op,exp,B.mkAttr(line))
let mkBINARY(op,exp1,exp2,line) = BINARY(op,exp1,exp2,B.mkAttr(line))
let mkTERNARY(exp1,exp2, exp3,line) = TERNARY(exp1,exp2,exp3,B.mkAttr(line))
let mkINDR(exp,line) = INDR(exp,B.mkAttr(line))
let mkSELECT(line) = SELECT(None,B.mkAttr(line))
let mkFIRST(exp, criteria, line) = FIRST(exp, criteria, None, B.mkAttr(line))

let mkIF(exp,stm1,stm2,line) = IF(exp,stm1,stm2,B.mkAttr(line))
let mkFOR(id,state_ids,stm,crit,line) =
  FOR(B.mkId(id),Some(state_ids),[],RANDOM,stm,crit,B.mkAttr(line))
let mkFOR_ANY(id,stm,line) =
  FOR(B.mkId(id),None,[],RANDOM,stm,None,B.mkAttr(line))
let mkSWITCH(exp,cases,line) = SWITCH(exp,cases,None,B.mkAttr(line))
let mkSEQ(decls,stms,line) = SEQ(decls,stms,B.mkAttr(line))
let mkRETURN(exp,line) = RETURN(Some(exp),B.mkAttr(line))
let mkRETURNVOID(line) = RETURN(None,B.mkAttr(line))
let mkSTEAL(exp,line) = STEAL(exp,B.mkAttr(line))
let mkMOVE(src,dst,line) =
  MOVE(src,dst,None,[],None,true,EITHER,B.mkAttr(line))
let mkASSIGN(loc,exp,line) = ASSIGN(loc,exp,false,B.mkAttr(line))
let mkDEFER(line) = DEFER(B.mkAttr(line))

let mkSEQ_CASE(ids,stm,line) =
  SEQ_CASE(List.map (B.mkId) ids,[],stm,B.mkAttr(line))

let mkPRIMSTMT(id,exprs,line) =
  PRIMSTMT(VAR(B.mkId(id),[],B.mkAttr(line)),exprs,B.mkAttr(line))
let mkPRIMSTMTEXP(exp,exprs,line) = PRIMSTMT(exp,exprs,B.mkAttr(line))
let mkERROR(str,line) = ERROR(str,B.mkAttr(line))
let mkBREAK(line) = BREAK(B.mkAttr(line))
let mkCONTINUE(line) = CONTINUE(B.mkAttr(line))
let cur_test_index = ref 0
let mk_test_index _ =
  cur_test_index := !cur_test_index + 1;
  !cur_test_index

let mkEMPTY(id,line) = EMPTY(B.mkId(id),[],B.UNK,true,
			     B.mkTestAttr(line,mk_test_index()))
let mkVALID(exp, line) = VALID(exp, None, B.mkAttr(line))
let mkEMPTYCLASS(id,line) =
  EMPTY(B.mkId(Class_state.class2idc(id)),[],B.UNK,true,
	B.mkTestAttr(line,mk_test_index()))
let mkPRIM(id,exprs,line) =
  PRIM(VAR(B.mkId(id),[],B.mkAttr(line)),exprs,B.mkAttr(line))
let mkPRIMEXP(exp,exprs,line) = PRIM(exp,exprs,B.mkAttr(line))
let mkSCHEDCHILD(expr,line) = SCHEDCHILD(expr,[],B.mkAttr(line))
let mkSRCONSCHED(line) = SRCONSCHED(B.mkAttr(line))
let mkALIVE(line) = ALIVE(B.UNK,true,B.mkAttr(line))
let mkIN(exp,locexp,crit,line) =
  IN(exp,locexp,[],B.BOTH,true,crit,B.mkTestAttr(line,mk_test_index()))
let mkINCLASS(exp,id,crit,line) =
  IN(exp,VAR(B.mkId(Class_state.class2idc(id)),[],B.mkAttr(line)),[],B.UNK,true,
     crit, B.mkTestAttr(line,mk_test_index()))

let mkCOMPOUND_ASSIGN(exp,op,rarg,line) =
  mkASSIGN(exp,mkBINARY(op,exp,rarg,line),line)

let mkDOMFOR(id, opt_bound, stmts) = DOMFOR(id, opt_bound, stmts)
let mkDOMDOM(id, exp, target, group_def) = DOMDOM(id, exp, target, group_def)

(* ----------------------- Accessor functions --------------------- *)

let get_exp_attr = function
    COUNT(a) -> a
  | FNOR(a) -> a
  | SUM(a) -> a
  | MIN(a) -> a
  | MAX(a) -> a
  | DISTANCE(a) -> a
  | SELF(a) -> a
  | INT(_,a) -> a
  | VAR(_,_,a) -> a
  | FIELD(_,_,_,a) -> a
  | CAST(_,_,a) -> a
  | TYPE(_,a) -> a
  | LCORE(_, _, _, a) -> a
  | BOOL(_,a) -> a
  | PARENT(a) -> a
  | UNARY(_,_,a) -> a
  | BINARY(_,_,_,a) -> a
  | TERNARY(_,_,_,a) -> a
  | PBINARY(_,_,_,_,_,a) -> a
  | CONDOP(_,_,_,a) -> a
  | INDR(_,a) -> a
  | SELECT(_,a) -> a
  | FIRST(_,_,_,a) -> a
  | EMPTY(_,_,_,_,a) -> a
  | VALID(_,_,a) -> a
  | PRIM(_,_,a) -> a
  | SCHEDCHILD(_,_,a) -> a
  | ALIVE(_,_,a) -> a
  | SRCONSCHED(a) -> a
  | AREF(_,_,a) -> a
  | IN(_,_,_,_,_,_,a) -> a
  | MOVEFWDEXP(_,_,a) -> a
  | INT_STRING (_, a) -> a
  | INT_NULL a -> a

let rec get_exp_id = function
  | VAR(id,_,_) -> id
  | FIELD(_,id,_,_) -> id
  | LCORE(_,e,_,_) -> get_exp_id e
  | INDR(e,_) -> get_exp_id e
  | FIRST(e,_,_,_) -> get_exp_id e
  | EMPTY(id,_,_,_,a) -> id
  | VALID(e,_,_) -> get_exp_id e
  | PRIM(e,_,_) -> get_exp_id e
  | SCHEDCHILD(e,_,_) -> get_exp_id e
  | AREF(e,_,_) -> get_exp_id e
  | MOVEFWDEXP(e,_,_) -> get_exp_id e
  | _ -> raise (Failure (__LOC__ ^": expression does not have an id"))

let get_stm_attr = function
  IF(_,_,_,a) -> a
  | FOR(_,_,_,_,_,_,a) -> a
  | FOR_WRAPPER(_,_,a) -> a
  | SWITCH(_,_,_,a) -> a
  | SEQ(_,_,a) -> a
  | RETURN(_,a) -> a
  | STEAL(_,a) -> a
  | MOVE(_,_,_,_,_,_,_,a) -> a
  | MOVEFWD(_,_,_,a) -> a
  | SAFEMOVEFWD(_,_,_,_,a) -> a
  | ASSIGN(_,_,_,a) -> a
  | DEFER(a) -> a
  | PRIMSTMT(_,_,a) -> a
  | ERROR(_,a) -> a
  | BREAK(a) -> a
  | CONTINUE(a) -> a
  | ASSERT(_,a) -> a

(* ------------------------- Useful syntax ------------------------ *)

let mkBLOCK attr = function
    [stm] -> stm
  | stms -> SEQ([],stms,attr)

let mkDBLOCK attr = function
    ([],[stm]) -> stm
  | (decls,stms) -> SEQ(decls,stms,attr)

let addBLOCK attr stm1 = function
    SEQ([],stms,attr) -> SEQ([],stm1::stms,attr)
  | stm2 -> mkBLOCK attr [stm1;stm2]

let postBLOCK attr stm1 end_list =
  match stm1 with
    SEQ([],stms,attr) -> SEQ([],stms@end_list,attr)
  | stm2 -> mkBLOCK attr (stm1::end_list)

let add_decl_block decl = function
    SEQ(d,s,a) -> SEQ(decl::d,s,a)
  | s -> SEQ([decl],[s],get_stm_attr s)
(* ---------------------------- Comments -------------------------- *)

let add_comment comment = function
    SUM(a) -> SUM(B.add_comment a comment)
  | MIN(a) -> MIN(B.add_comment a comment)
  | MAX(a) -> MAX(B.add_comment a comment)
  | COUNT(a) -> COUNT(B.add_comment a comment)
  | FNOR(a) -> FNOR(B.add_comment a comment)
  | DISTANCE(a) -> DISTANCE(B.add_comment a comment)
  | INT(x,a) -> INT(x,B.add_comment a comment)
  | SELF(a) -> SELF(B.add_comment a comment)
  | VAR(x,_,a) -> VAR(x,[],B.add_comment a comment)
  | FIELD(x,y,_,a) -> FIELD(x,y,[],B.add_comment a comment)
  | BOOL(x,a) -> BOOL(x,B.add_comment a comment)
  | PARENT(a) -> PARENT(B.add_comment a comment)
  | UNARY(x,y,a) -> UNARY(x,y,B.add_comment a comment)
  | BINARY(o,x,y,a) -> BINARY(o,x,y,B.add_comment a comment)
  | TERNARY(x,y,z,a) -> TERNARY(x,y,z,B.add_comment a comment)
  | PBINARY(x,y,m,n,o,a) -> PBINARY(x,y,m,n,o,B.add_comment a comment)
  | INDR(x,a) -> INDR(x,B.add_comment a comment)
  | SELECT(x,a) -> SELECT(x,B.add_comment a comment)
  | FIRST(x,y,z,a) -> FIRST(x,y,z,B.add_comment a comment)
  | EMPTY(x,y,m,n,a) -> EMPTY(x,y,m,n,B.add_comment a comment)
  | VALID(x,y,a) -> VALID(x,y,B.add_comment a comment)
  | PRIM(x,y,a) -> PRIM(x,y,B.add_comment a comment)
  | SCHEDCHILD(x,y,a) -> SCHEDCHILD(x,y,B.add_comment a comment)
  | ALIVE(x,y,a) -> ALIVE(x,y,B.add_comment a comment)
  | SRCONSCHED(a) -> SRCONSCHED(B.add_comment a comment)
  | AREF(x,y,a) -> AREF(x,y,B.add_comment a comment)
  | IN(x,y,z,m,n,c,a) -> IN(x,y,z,m,n,c,B.add_comment a comment)
  | MOVEFWDEXP(x,y,a) -> MOVEFWDEXP(x,y,B.add_comment a comment)

(* ---------------------------- Tys -------------------------- *)

let updty ty = function
    SUM(a) -> SUM(B.updty a ty)
  | MIN(a) -> MIN(B.updty a ty)
  | MAX(a) -> MAX(B.updty a ty)
  | COUNT(a) -> COUNT(B.updty a ty)
  | FNOR(a) -> FNOR(B.updty a ty)
  | DISTANCE(a) -> DISTANCE(B.updty a ty)
  | INT(x,a) -> INT(x,B.updty a ty)
  | SELF(a) -> SELF(B.updty a ty)
  | VAR(x,_,a) -> VAR(x,[],B.updty a ty)
  | FIELD(x,y,_,a) -> FIELD(x,y,[],B.updty a ty)
  | BOOL(x,a) -> BOOL(x,B.updty a ty)
  | PARENT(a) -> PARENT(B.updty a ty)
  | UNARY(x,y,a) -> UNARY(x,y,B.updty a ty)
  | BINARY(o,x,y,a) -> BINARY(o,x,y,B.updty a ty)
  | TERNARY(x,y,z,a) -> TERNARY(x,y,z,B.updty a ty)
  | PBINARY(x,y,m,n,o,a) -> PBINARY(x,y,m,n,o,B.updty a ty)
  | INDR(x,a) -> INDR(x,B.updty a ty)
  | SELECT(x,a) -> SELECT(x,B.updty a ty)
  | FIRST(x,y,z,a) -> FIRST(x,y,z,B.updty a ty)
  | EMPTY(x,y,m,n,a) -> EMPTY(x,y,m,n,B.updty a ty)
  | VALID(x,y,a) -> VALID(x,y,B.updty a ty)
  | PRIM(x,y,a) -> PRIM(x,y,B.updty a ty)
  | SCHEDCHILD(x,y,a) -> SCHEDCHILD(x,y,B.updty a ty)
  | ALIVE(x,y,a) -> ALIVE(x,y,B.updty a ty)
  | SRCONSCHED(a) -> SRCONSCHED(B.updty a ty)
  | AREF(x,y,a) -> AREF(x,y,B.updty a ty)
  | IN(x,y,z,m,n,c,a) -> IN(x,y,z,m,n,c,B.updty a ty)
  | MOVEFWDEXP(x,y,a) -> MOVEFWDEXP(x,y,B.updty a ty)

(* ----------------------- Printing functions --------------------- *)
let event_name2partial_mask (EVENT_NAME(l,_)) =
  ("EVENT_"^(String.uppercase (B.id2c l)))

let event_name2c = function
    EVENT_NAME(id,_) -> B.id2c id

let event_name_equal
    (EVENT_NAME(nmlst1,_))
    (EVENT_NAME(nmlst2,_)) =
  nmlst1 = nmlst2

let list_of_const_enum = ref []

and bop2c = function
    PLUS -> "+"
  | MINUS -> "-"
  | TIMES -> "*"
  | DIV -> "/"
  | MOD -> "%"
  | AND -> "&&"
  | OR -> "||"
  | BITAND -> "&"
  | BITOR -> "|"
  | EQ -> "=="
  | NEQ -> "!="
  | LT -> "<"
  | GT -> ">"
  | LEQ -> "<="
  | GEQ -> ">="
  | LSHIFT -> "<<"
  | RSHIFT -> ">>"

(* ------------------------ Comparing terms ------------------------ *)

let rec exp_equal_except_attributes e1 = function
    SUM(_) ->
      (match e1 with
	SUM(_) -> true
      |	_ -> false)
  | COUNT(_) ->
      (match e1 with
	 COUNT(_) -> true
       |	_ -> false)
  | FNOR(_) ->
     (match e1 with
        FNOR(_) -> true
      | _-> false)
  | MIN(_) ->
      (match e1 with
	MIN(_) -> true
      |	_ -> false)
  | MAX(_) ->
      (match e1 with
	MAX(_) -> true
      |	_ -> false)
  | DISTANCE(_) ->
      (match e1 with
	DISTANCE(_) -> true
      |	_ -> false)
  | INT(n,_) ->
      (match e1 with
	INT(n1,_) -> n = n1
      |	_ -> false)
  | VAR(id,_,_) ->
      (match e1 with
	VAR(id1,_,_) -> B.ideq(id,id1)
      |	_ -> false)
  | FIELD(exp,fld,_,_) ->
      (match e1 with
	FIELD(exp1,fld1,_,_) ->
	  exp_equal_except_attributes exp exp1 && B.ideq(fld,fld1)
      |	_ -> false)
  | BOOL(b,_) ->
      (match e1 with
	BOOL(b1,_) -> b = b1
      |	_ -> false)
  | PARENT(_) ->
      (match e1 with
	PARENT(_) -> true
      |	_ -> false)
  | UNARY(uop,exp,_) ->
      (match e1 with
	UNARY(uop1,exp1,_) ->
	  uop = uop1 && exp_equal_except_attributes exp exp1
      |	_ -> false)
  | BINARY(bop,exp1,exp2,_) ->
      (match e1 with
	BINARY(bopa,exp1a,exp2a,_) ->
	  bop = bopa &&
	  exp_equal_except_attributes exp1 exp1a &&
	  exp_equal_except_attributes exp2 exp2a
      |	_ -> false)
  | TERNARY(exp1,exp2, exp3,_) ->
      (match e1 with
	TERNARY(exp1a,exp2a, exp3a,_) ->
	  exp_equal_except_attributes exp1 exp1a &&
	  exp_equal_except_attributes exp2 exp2a &&
	  exp_equal_except_attributes exp3 exp3a
      |	_ -> false)
  | PBINARY(bop,exp1,_,exp2,_,_) ->
      (match e1 with
	PBINARY(bopa,exp1a,_,exp2a,_,_) ->
	  bop = bopa &&
	  exp_equal_except_attributes exp1 exp1a &&
	  exp_equal_except_attributes exp2 exp2a
      |	_ -> false)
  | INDR(exp,_) ->
      (match e1 with
	INDR(exp1,_) -> exp_equal_except_attributes exp exp1
      |	_ -> false)
  | SELECT(sti,_) ->
      (match e1 with
	SELECT(sti1,_) -> sti = sti1
      |	_ -> false)
  | FIRST(exp,crit,sti,_) ->
      (match e1 with
	FIRST(exp1,crit1,sti1, _) -> sti = sti1 && exp_equal_except_attributes exp exp1 && crit = crit1
      |	_ -> false)
  | EMPTY(st,sti,tv,insrc,_) ->
      (match e1 with
	EMPTY(st1,sti1,tv1,insrc1,_) ->
	  st = st1 && Aux.set_equal sti sti1 && tv = tv1 && insrc = insrc1
      |	_ -> false)
  | VALID(exp,si,_) ->
      (match e1 with
	VALID(exp1,si1,_) ->
	  exp_equal_except_attributes exp exp1 && si = si1
      |	_ -> false)
  | PRIM(fn,args,_) ->
      (match e1 with
	PRIM(fn1,args1,_) ->
	  exp_equal_except_attributes fn fn1 &&
	  List.length args = List.length args1 &&
	  List.for_all2 exp_equal_except_attributes args args1
      |	_ -> false)
  | SCHEDCHILD(proc,vp,_) ->
      (match e1 with
	SCHEDCHILD(proc1,vp1,_) ->
	  exp_equal_except_attributes proc proc1 && Aux.set_equal vp vp1
      |	_ -> false)
  | ALIVE(tv1,insrc1,_) ->
      (match e1 with
	ALIVE(tv2,insrc2,_) -> tv1 = tv2 && insrc1 = insrc2
      |	_ -> false)
  | SRCONSCHED(_) ->
      (match e1 with
	SRCONSCHED(_) -> true
      |	_ -> false)
  | AREF(exp1,Some(exp2),_) ->
     (match e1 with
	AREF(exp1a,Some(exp2a),_) ->
	exp_equal_except_attributes exp1 exp1a &&
	  exp_equal_except_attributes exp2 exp2a
      | _ -> false)
  | AREF(exp1,None,_) ->
     (match e1 with
        AREF(exp1a,None,_) ->
        exp_equal_except_attributes exp1 exp1a
      | _ -> false)
  | IN(proc,st,sti,tv,insrc,crit,_) ->
      (match e1 with
	IN(proc1,st1,sti1,tv1,insrc1,crit1,_) ->
	  exp_equal_except_attributes proc proc1 && st = st1 &&
	  Aux.set_equal sti sti1 && tv = tv1 && insrc = insrc1
      && crit == crit1
      |	_ -> false)
  | MOVEFWDEXP(proc,state_end,_) ->
      (match e1 with
	MOVEFWDEXP(proc1,state_end1,_) ->
	  exp_equal_except_attributes proc proc1 && state_end = state_end1
      |	_ -> false)

let decl_equal_except_attributes
    (VARDECL(ty,id,sys,laazy,shared,exp,_)) (VARDECL(ty1,id1,sys1,laazy1,shared1,exp1,_)) =
  ty = ty1 && id = id1 && sys = sys1 && laazy = laazy1
  && exp = exp1 && shared = shared1

let def_equal_except_attributes d1 = function
    VALDEF(decl,exp,cst,_) ->
      (match d1 with
	VALDEF(decl1,exp1,cst1,_) ->
	  decl_equal_except_attributes decl decl1 &&
	  exp_equal_except_attributes exp exp1 && cst = cst1
      |	_ -> false)
  | UNINITDEF(decl,_) ->
      (match d1 with
	UNINITDEF(decl1,_) -> decl_equal_except_attributes decl decl1
      |	_ -> false)
  | SYSDEF(decl,cst,_) ->
      (match d1 with
	SYSDEF(decl1,cst1,_) ->
	  decl_equal_except_attributes decl decl1 && cst = cst1
      |	_ -> false)
  | DUMMYDEF(id,exp,_) ->
      (match d1 with
	DUMMYDEF(id1,exp1,_) ->
	  id = id1 && exp_equal_except_attributes exp exp1
      |	_ -> false)

let rec equal_except_attributes s1 = function
    IF(tst,thn,els,_) ->
      (match s1 with
	IF(tst1,thn1,els1,_) ->
	  exp_equal_except_attributes tst tst1 &&
	  equal_except_attributes thn thn1 &&
	  equal_except_attributes els els1
      |	_ -> false)
  | FOR(id,st,sti,dir,body,crit,_) ->
      (match s1 with
	FOR(id1,st1,sti1,dir1,body1,crit1,_) ->
	  id = id1 && st = st1 && Aux.set_equal sti sti1 && dir = dir1 && crit == crit1 (*FIXME: should ignore attributes here too *) &&
	  equal_except_attributes body body1
      |	_ -> false)
  | FOR_WRAPPER(brklbl,loops,_) ->
      (match s1 with
	FOR_WRAPPER(brklbl1,loops1,_) ->
	  brklbl = brklbl1 &&
	  List.length loops = List.length loops1 &&
	  List.for_all2 equal_except_attributes loops loops1
      |	_ -> false)
  | SWITCH(tst,cases,def,_) ->
      (match s1 with
	SWITCH(tst1,cases1,def1,_) ->
	  exp_equal_except_attributes tst tst1 &&
	  List.length cases = List.length cases1 &&
	  List.for_all2
	    (function SEQ_CASE(sts,stis,stmt,_) ->
	      function SEQ_CASE(sts1,stis1,stmt1,_) ->
		Aux.set_equal sts sts1 && Aux.set_equal stis stis1 &&
		equal_except_attributes stmt stmt1)
	    cases cases1 &&
	  (match (def,def1) with
	    (None,None) -> true
	  | (Some s, Some s1) -> equal_except_attributes s s1
	  | _ -> false)
      |	_ -> false)
  | SEQ(decls,stmts,_) ->
      (match s1 with
	SEQ(decls1,stmts1,_) ->
	  List.length decls = List.length decls1 &&
	  List.length stmts = List.length stmts1 &&
	  List.for_all2 def_equal_except_attributes decls decls1 &&
	  List.for_all2 equal_except_attributes stmts stmts1
      |	_ -> false)
  | RETURN(None,_) ->
      (match s1 with
	RETURN(None,_) -> true
      |	_ -> false)
  | RETURN(Some exp,_) ->
      (match s1 with
	RETURN(Some exp1,_) -> exp_equal_except_attributes exp exp1
      |	_ -> false)
  | MOVE(proc,dst,srcinfo,vsrcinfo,dstinfo,auto,state_end,_) ->
      (match s1 with
	MOVE(proc1,dst1,srcinfo1,vsrcinfo1,dstinfo1,auto1,state_end1,_) ->
	  exp_equal_except_attributes proc proc1 &&
	  exp_equal_except_attributes dst dst1 &&
	  Aux.set_equal vsrcinfo vsrcinfo1 && dstinfo = dstinfo1 &&
	  auto = auto1 && state_end = state_end1
      |	_ -> false)
  | MOVEFWD(proc,srcdst,state_end,_) ->
      (match s1 with
	MOVEFWD(proc1,srcdst1,state_end1,_) ->
	  exp_equal_except_attributes proc proc1 &&
	  Aux.set_equal srcdst srcdst1 &&
	  state_end = state_end1
      |	_ -> false)
  | SAFEMOVEFWD(proc,srcdst,def,state_end,_) ->
      (match s1 with
	SAFEMOVEFWD(proc1,srcdst1,def1,state_end1,_) ->
	  exp_equal_except_attributes proc proc1 &&
	  Aux.set_equal srcdst srcdst1 &&
	  equal_except_attributes def def1 &&
	  state_end = state_end1
      |	_ -> false)
  | ASSIGN(lexp,rexp,srtd,_) ->
      (match s1 with
	ASSIGN(lexp1,rexp1,srtd1,_) ->
	  exp_equal_except_attributes lexp lexp1 &&
	  exp_equal_except_attributes rexp rexp1
      |	_ -> false)
  | DEFER(_) ->
      (match s1 with
	DEFER(_) -> true
      |	_ -> false)
  | PRIMSTMT(fn,args,_) ->
      (match s1 with
	PRIMSTMT(fn1,args1,_) ->
	  exp_equal_except_attributes fn fn1 &&
	  List.length args = List.length args1 &&
	  List.for_all2 exp_equal_except_attributes args args1
      |	_ -> false)
  | ERROR(str,_) ->
      (match s1 with
	ERROR(str1,_) -> str = str1
      |	_ -> false)
  | BREAK(_) ->
      (match s1 with
	BREAK(_) -> true
      |	_ -> false)
  | CONTINUE(_) ->
      (match s1 with
	CONTINUE(_) -> true
      |	_ -> false)
  | ASSERT(exp,_) ->
      (match s1 with
	ASSERT(exp1,_) -> exp_equal_except_attributes exp exp1
      |	_ -> false)

(* --------------------- Special process names --------------------- *)

exception LookupErrException

let rec lookup x = function
    [] -> raise LookupErrException
  | ((y,v)::r) -> if x = y then v else lookup x r

let is_src_or_tgt exp fld (srcenv,nonempty_srcs) =
  let nonempty_srcs = List.sort compare nonempty_srcs in
  match B.ty(get_exp_attr(exp)) with
    B.PEVENT | B.CEVENT ->
      let str = B.id2c fld in
      if str = "source"
      then
	(try VB.VP(VB.SRC,lookup VB.SRC srcenv)
	with LookupErrException -> VB.VP(VB.SRC,nonempty_srcs))
      else
	if str = "target"
	then
	  (try VB.VP(VB.TGT,lookup VB.TGT srcenv)
	  with LookupErrException -> VB.VP(VB.TGT,nonempty_srcs))
	else VB.VP(VB.UNKNOWNPROC,nonempty_srcs)
  | _ -> VB.VP(VB.UNKNOWNPROC,nonempty_srcs)

(* ------------------ Naming nontrivial expressions ---------------- *)

let dummy_env = [(VB.SRC,[]);(VB.TGT,[])]

let do_name_expr new_id exp attr =
  let rename exp =
    let exp_type = B.ty (get_exp_attr exp) in
    let new_id = match new_id with Some x -> x | None -> B.fresh_id()
    in ([VALDEF(VARDECL(exp_type,new_id,false, false, CS.UNSHARED,None,B.updty attr exp_type),
		    exp,VARIABLE,B.updty attr exp_type)],
	VAR(new_id,[],B.updty attr exp_type)) in
  match exp with
    VAR(_,_,_) -> ([],exp)
  | FIELD(exp1,fld,_,attr) ->
      (match is_src_or_tgt exp1 fld (dummy_env,[]) with
	VB.VP(VB.SRC,_) -> rename exp
      | VB.VP(VB.TGT,_) -> ([],exp)
      | _ -> rename exp)
  | _ -> rename exp


let name_expr exp attr = do_name_expr None exp attr

let pick_name_expr str exp attr =
  do_name_expr (Some (B.fresh_idx(str))) exp attr

(* ----------------------------- Timers ---------------------------- *)

let collect_timer_names procdecls valdefs =
  let check_vardecl = function
      VARDECL(B.TIMER(_),id,_,_,_,_,_) -> Some id
    | _ -> None in
  let timer_procdecls = Aux.option_filter check_vardecl procdecls in
  let timer_valdefs =
    Aux.option_filter
      (function
	  VALDEF(decl,_,_,_) -> check_vardecl decl
	| SYSDEF(decl,_,_) -> check_vardecl decl
	| _ -> None)
      valdefs in
  if not (Aux.intersect timer_procdecls timer_valdefs = [])
  then raise (Error.Error ("timer-typed variables must be distinct from "^
			   "timer-typed process-structure fields"));
  timer_procdecls @ timer_valdefs

(* ----------------------- Collect information ---------------------- *)
(* read variables, fields, and states; written variables and fields *)

let make_class_env (state_env : (B.identifier * CS.state_info) list) =
  let states = List.map (function (_,state) -> state) state_env in
  let env =
    List.fold_left
      (function env ->
      function
        (CS.CSTATE(id,cls)) ->
        env
      | (CS.STATE(id,cls,ty,vis)) as s ->
        let rec loop = function
            [] -> [(cls,[s])]
          | ((cls1,stlst) as cur)::rest ->
            if cls = cls1
            then (cls1,s::stlst)::(loop rest)
            else cur::(loop rest)
        in
        loop env)
      [] states in
  List.map
    (function (cls,stlist) -> (B.mkId(CS.safe_class2c cls),stlist))
    env

let idunion l1 l2 =
  let names2 = List.map (function (id,_) -> id) l2 in
  let rec loop = function
      [] -> l2
    | ((id,ln) as cur)::rest ->
	if List.mem id names2 then loop rest else cur::(loop rest) in
  loop l1

(* may raise not_found, in which case it's not a state *)
let get_states clsenv id =
  try [List.assoc id (!CS.state_env)]
  with Not_found -> List.assoc id clsenv

(* collects variables and fields read by an expression *)
let rec collect_exp env clsenv = function
    VAR(id,_,ln) ->
      if List.mem id env
      then ([],[],[])
      else
	(try ([],[],List.map (function x -> (x,ln)) (get_states clsenv id))
	with Not_found -> ([(id,ln)],[],[]))
  | FIELD(exp,id,_,ln) ->
      let (read_var,read_field,read_state) =
	collect_exp env clsenv exp in
      (read_var, idunion [(id,ln)] read_field,read_state)
  | UNARY(uop,exp,ln) -> collect_exp env clsenv exp
  | BINARY(bop,exp1,exp2,ln) ->
      let (read_var1,read_field1,read_state1) = collect_exp env clsenv exp1 in
      let (read_var2,read_field2,read_state2) = collect_exp env clsenv exp2 in
      (idunion read_var1 read_var2,idunion read_field1 read_field2,
       idunion read_state1 read_state2)
  | TERNARY(exp1,exp2,exp3,ln) ->
      let (read_var1,read_field1,read_state1) = collect_exp env clsenv exp1
      and (read_var2,read_field2,read_state2) = collect_exp env clsenv exp2
      and (read_var3,read_field3,read_state3) = collect_exp env clsenv exp3 in
      (idunion (idunion read_var1 read_var2) read_var3,
       idunion (idunion read_field1 read_field2) read_field3,
       idunion (idunion read_state1 read_state2) read_state3)
  | PBINARY(bop,exp1,states1,exp2,states2,ln) ->
      let (read_var1,read_field1,read_state1) = collect_exp env clsenv exp1 in
      let (read_var2,read_field2,read_state2) = collect_exp env clsenv exp2 in
      (idunion read_var1 read_var2,idunion read_field1 read_field2,
       idunion read_state1 read_state2)
  | INDR(exp,ln) -> collect_exp env clsenv exp
  | VALID(exp, si, ln) ->
     let instates = match si with
	 None -> []
       | Some si_ -> [(si_,ln)]
     in
      let (read_var,read_field,read_state) = collect_exp env clsenv exp in
      (read_var,read_field,idunion instates read_state)
  | PRIM(fn,args,ln) ->
      List.fold_left
	(function (read_var,read_field,read_state) ->
	  function x ->
	    let (read_var1,read_field1,read_state1) =
	      collect_exp env clsenv x in
	    (idunion read_var1 read_var,idunion read_field1 read_field,
	     idunion read_state1 read_state))
	(collect_exp env clsenv fn) args
  | SCHEDCHILD(exp,procs,ln) -> collect_exp env clsenv exp
  | IN(exp,st,sti,tv,insrc,ord,ln) ->
      let instates = List.map (function sti -> (sti,ln)) sti in
      let (read_var,read_field,read_state) = collect_exp env clsenv exp in
      (read_var,read_field,idunion instates read_state)
  | EMPTY(st,sti,tv,insrc,ln) ->
      let estates = List.map (function x -> (x,ln)) sti in
      ([],[],estates)
  | _ -> ([],[],[])

(* collects variables and fields read or written by a statement *)
let rec collect_stm (env : B.identifier list) clsenv = function
    IF(exp,st1,st2,ln) ->
      let (read_var1,read_field1,read_state1) = collect_exp env clsenv exp in
      let (read_var2,read_field2,read_state2,write_var2,write_field2) =
	collect_stm env clsenv st1 in
      let (read_var3,read_field3,read_state3,write_var3,write_field3) =
	collect_stm env clsenv st2 in
      (idunion read_var1 (idunion read_var2 read_var3),
       idunion read_field1 (idunion read_field2 read_field3),
       idunion read_state1 (idunion read_state2 read_state3),
       idunion write_var2 write_var3, idunion write_field2 write_field3)
  | FOR(id,_,sts,_,stmt,_,ln) ->
      let fstates = List.map (function x -> (x,ln)) sts in
      let (read_var,read_field,read_state,write_var,write_field) =
	collect_stm (id::env) clsenv stmt in
      (read_var,read_field,idunion fstates read_state,write_var,write_field)
  | FOR_WRAPPER(_,stmts,ln) ->
      List.fold_left
	(function (read_var,read_field,read_state,write_var,write_field) ->
	  function stmt ->
	    let (read_var1,read_field1,read_state1,write_var1,write_field1) =
	      collect_stm env clsenv stmt in
	    (idunion read_var1 read_var,idunion read_field1 read_field,
	     idunion read_state1 read_state,
	     idunion write_var1 write_var,idunion write_field1 write_field))
	([],[],[],[],[]) stmts
  | SWITCH(exp,lsc,st2,ln) ->
      let (read_var0,read_field0,read_state0) = collect_exp env clsenv exp in
      let (read_var,read_field,read_state,write_var,write_field) =
	(* drop read_state, because all states are read by the switch test *)
	List.fold_left
	  (function (read_var,read_field,read_state,write_var,write_field) ->
	    function SEQ_CASE(_,_,st1,_) ->
	      let (read_var1,read_field1,read_state1,write_var1,write_field1) =
		collect_stm env clsenv st1 in
	      (idunion read_var1 read_var,idunion read_field1 read_field,
	       idunion read_state1 read_state,
	       idunion write_var1 write_var,idunion write_field1 write_field))
	  (match st2 with None -> ([],[],[],[],[])
	  | Some st -> collect_stm env clsenv st)
	  lsc in
      (idunion read_var0 read_var,idunion read_field0 read_field,
       idunion read_state0 read_state,write_var,write_field)
  | SEQ(decls,lstmt,ln) ->
      let (read_var,read_field,read_state,env) =
	List.fold_left
	  (function (read_var,read_field,read_state,env) ->
	    function
		VALDEF(VARDECL(_,id,_,_,_,_,ln),exp,const,a) ->
		  let (read_var1,read_field1,read_state1) =
		    collect_exp env clsenv exp in
		  (idunion read_var1 read_var, idunion read_field1 read_field,
		   idunion read_state1 read_state, Aux.union [id] env)
	      |	_ ->
		  raise (Error.Error (Printf.sprintf
					"%s: unexpected local decl"
					(B.loc2c ln))))
	  ([],[],[],env) decls in
      List.fold_left
	(function (read_var,read_field,read_state,write_var,write_field) ->
	  function x ->
	    let (read_var1,read_field1,read_state1,write_var1,write_field1) =
	      collect_stm env clsenv x in
	    (idunion read_var1 read_var,idunion read_field1 read_field,
	     idunion read_state1 read_state,
	     idunion write_var1 write_var,idunion write_field1 write_field))
	(read_var,read_field,[],[],[]) lstmt
  | RETURN(Some exp,ln) ->
      let (read_var,read_field,read_state) = collect_exp env clsenv exp in
      (read_var,read_field,read_state,[],[])
  | MOVE(proc,dst,srcinfo,vsrcinfo,dstinfo,auto,state_end,_) ->
      let (read_var,read_field,read_state) = collect_exp env clsenv proc in
      (read_var,read_field,read_state,[],[])
  | MOVEFWD(proc,srcdst,state_end,_) ->
      let (read_var,read_field,read_state) = collect_exp env clsenv proc in
      (read_var,read_field,read_state,[],[])
  | ASSIGN(lexp,rexp,affectsrtd,ln) ->
      let (read_var1,read_field1,read_state1) = collect_exp env clsenv rexp in
      let (read_var,read_field,read_state,write_var,write_field) =
	match lexp with
	  VAR(id,_,a) ->
	    if List.mem id env
	    then ([],[],[],[],[])
	    else ([],[],[],[(id,a)],[])
	| FIELD(exp,id,_,a) ->
	    let (read_var2,read_field2,read_state2) =
	      collect_exp env clsenv exp in
	    (read_var2,read_field2,read_state2,[],[(id,a)])
	| _ -> raise (Error.Error "unexpected left-hand side expression") in
      (idunion read_var read_var1,idunion read_field read_field1,
       idunion read_state read_state1,write_var,write_field)
  | PRIMSTMT(fn,args,ln) ->
      let (read_var,read_field,read_state) =
	List.fold_left
	  (function (read_var,read_field,read_state) ->
	    function x ->
	      let (read_var1,read_field1,read_state1) =
		collect_exp env clsenv x in
	      (idunion read_var1 read_var,idunion read_field1 read_field,
	       idunion read_state1 read_state))
	  (collect_exp env clsenv fn) args in
      (read_var,read_field,read_state,[],[])
  | ASSERT(exp,ln) ->
      let (read_var,read_field,read_state) = collect_exp env clsenv exp in
      (read_var,read_field,read_state,[],[])
  | _ -> ([],[],[],[],[]) (* can't occur or no expression subterms *)

let collect_exp_all env stm =
  let clsenv = make_class_env (!CS.state_env) in
  collect_exp env clsenv stm

let collect_stm_all env stm =
  let clsenv = make_class_env (!CS.state_env) in
  collect_stm env clsenv stm

let collect_exp_vars env exp =
  let clsenv = make_class_env (!CS.state_env) in
  let (read_var,read_field,read_state) =
    collect_exp env clsenv exp in
  read_var

let collect_stm_vars env stm =
  let clsenv = make_class_env (!CS.state_env) in
  let (read_var,read_field,read_state,write_var,write_field) =
    collect_stm env clsenv stm in
  idunion read_var write_var

let params2ids params = List.map (function VARDECL(_,id,_,_,_,_,_) -> id) params

let valdefs2ids valdefs =
  List.map
    (function
	VALDEF(VARDECL(_,id,_,_,_,_,_),_,_,_)
      |	UNINITDEF(VARDECL(_,id,_,_,_,_,_),_)
      |	SYSDEF(VARDECL(_,id,_,_,_,_,_),_,_)
      |	DUMMYDEF(id,_,_) -> id)
    valdefs

(* ----------------- Duplicate variable declarations ---------------- *)

let no_dup_params params str =
  let _ =
    List.fold_left
      (function ids ->
	function VARDECL(_,id,_,_,_,_,a) ->
	  (try
 	    let other_attr = List.assoc id ids in
	    error a
	      (Printf.sprintf "%s %s already bound at %s" str (B.id2c id)
		 (B.loc2c other_attr))
	  with Not_found -> ());
	  (id,a)::ids)
      [] params in
  ()

let no_dup_valdefs valdefs =
  let decls =
    List.map
      (function
 	  VALDEF(decl,_,_,_)
	| UNINITDEF(decl,_)
	| SYSDEF(decl,_,_) -> decl
	| _ -> raise (Error.Error "expected variable definition"))
      valdefs in
  no_dup_params decls "variable"

(* ---------------------- Convert exp to string --------------------- *)
(* assume the expression will fit on one line *)

let pr = Printf.sprintf

let rec exp2c = function
    SELF _ -> "self"
  | SUM _ -> "sum"
  | MIN _-> "min"
  | MAX _ -> "max"
  | COUNT _ -> "count"
  | FNOR _ -> "or"
  | DISTANCE _ -> "distance"
  | INT(n,attr) -> pr "%d" n
  | VAR(id,_,attr) -> pr "%s" (B.id2c id)
  | FIELD(exp,fld,_,attr) -> pr "%s.%s" (exp2c exp) (B.id2c fld)
  | BOOL(true,attr) -> "true"
  | BOOL(false,attr) -> "false"
  | PARENT(attr) -> "parent"
  | UNARY(NOT,exp,attr) -> pr "!%s" (print_paren exp)
  | UNARY(COMPLEMENT,exp,attr) -> pr "~%s" (print_paren exp)
  | BINARY(bop,exp1,exp2,attr) ->
      pr "%s %s %s" (print_paren exp1) (bop2c bop) (print_paren exp2)
  | TERNARY(exp1,exp2,exp3,attr) ->
      pr "%s ? %s : %s" (print_paren exp1) (print_paren exp2) (print_paren exp3)
  | PBINARY(bop,exp1,_,exp2,_,attr) ->
      pr "%s %s %s" (print_paren exp1) (bop2c bop) (print_paren exp2)
  | INDR(exp,attr) -> pr "!%s" (print_paren exp)
  | SELECT(_,attr) -> "select()"
  | FIRST(exp,_,_,attr) -> pr "first(%s)" (exp2c exp) (* FIXME handle ordering in First corrextly *)
  | EMPTY(id,_,_,insrc,attr) -> pr "empty(%s)" (B.id2c id)
  | VALID(exp,None,attr) ->
      pr "valid(%s)" (print_paren exp)
  | VALID(exp,Some _,attr) ->
      pr "valid(/* UNSUPPORTED */)"
  | PRIM(f,args,attr) ->
      pr "%s(%s)" (print_paren f) (String.concat "," (List.map exp2c args))
  | SCHEDCHILD(arg,procs,attr) -> pr "next(%s)" (exp2c arg)
  | SRCONSCHED(attr) -> "srcOnSched()"
  | ALIVE(tv,_,attr) -> pr "alive()"
  | AREF(exp1,Some(exp2),_) -> pr "%s[%s]" (exp2c exp1) (exp2c exp2)
  | AREF(exp1,None,_) -> pr "%s" (exp2c exp1)
  | IN(exp,locexp,_,_,_,_,attr) -> pr "%s in %s" (print_paren exp) (exp2c locexp)
  | MOVEFWDEXP(exp,_,attr) -> pr "%s => forwardImmediate()" (exp2c exp)

(* eventually, compare the precedence of the expression with the precedence
of the context, as done for devil *)
and print_paren exp =
  match exp with
    BINARY(_,_,_,_)
  | PBINARY(_,_,_,_,_,_)
  | IN(_,_,_,_,_,_,_) -> pr "(%s)" (exp2c exp)
  | exp -> exp2c exp
