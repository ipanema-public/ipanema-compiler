(* ----------------------------- Classes ---------------------------- *)

type classname = RUNNING | READY | READYTICK | MIGRATING |
    BLOCKED | TERMINATED | NOWHERE | ANYWHERE

val all_classes : bool (* true if called from event types and VS needs
			  terminated *) -> classname list
val all_classes_nowhere : bool (* as above *) -> classname list
val class2c : classname -> string
val safe_class2c : classname -> string
val classic_class2c : classname -> string
val class2idc : classname -> string
val class2state : classname -> string
val id2class : Objects.identifier -> classname
val less_than : classname * classname -> bool

(* ----------------------------- Cores ----------------------------- *)
type corename = ACTIVE | SLEEPING | UNK
val safe_coreclass2c : corename -> string

(* ----------------------- State information ------------------------ *)
type state_info = STATE of pstate_info | CSTATE of cstate_info
		  | COMP of Objects.identifier * Objects.identifier
and pstate_info = Objects.identifier * classname * state_type * visibility
and cstate_info = Objects.identifier * corename
and state_type  = PROC | QUEUE of queue_order
and queue_order = FIFO | SELECT of sort_default | EMPTY
and sort_default = LIFODEFAULT | FIFODEFAULT | NODEFAULT
and visibility = PUBLIC | PRIVATE
and share_state = SHARED | UNSHARED

val state2c : state_info -> string
val state2class : state_info -> classname
val cstate2class : state_info -> corename


(* ----------------------------- States ----------------------------- *)
val ready_st : Objects.identifier list ref
val state_env : (Objects.identifier * state_info) list ref
val cstate_env : (Objects.identifier * state_info) list ref
val selected : state_info option ref
val selected_size : int ref
val concretize : Objects.identifier -> state_info list
val concretize_class : classname -> state_info list
val all_states : unit -> state_info list

(* ----------------------------- Cores ----------------------------- *)
val concretize4core : Objects.identifier -> state_info list
val concretize4coreprocess: Objects.identifier -> state_info list

(* ------------------------- States and classes --------------------- *)

val valid_sources : Objects.identifier -> classname -> state_info list
