(* $Id$ *)

(* Objects used in the verification phase *)

module CS = Class_state

(* -------------------- Miscellaneous operations -------------------- *)

let internal_error(n) =
  raise (Error.Error(Printf.sprintf "vobjects: internal error %i" n))

(* --------------------------- Objects ------------------------------ *)

type vprocname = SRC | TGT | P of int | Q of int | UNKNOWNPROC | BOTPROC |
                 S of int * string | ARG of int
(* the list of states must be kept sorted *)
and vproc = VP of vprocname * CS.state_info list |
            EQUAL of vproc list (* to be kept sorted *)
(* the list argument to MEMBER is a set and is never empty *)
type vtype =
    BOT | UNKNOWN | EMPTY | MEMBER of vproc list

(* -------------------------- Lub operation ------------------------- *)

let rec collect_sources = function
    VP(vp,s) -> s
  | EQUAL(vps) -> Aux.map_union collect_sources vps

let rec update_sources s = function
    VP(vp,_) -> VP(vp,s)
  | EQUAL(vps) -> EQUAL(List.map (update_sources s) vps)

let lub_vproc vp1 vp2 =
  let vp =
    (match (vp1,vp2) with
      (VP(BOTPROC,_),x) -> x
    | (x,VP(BOTPROC,_)) -> x
    | (VP(UNKNOWNPROC,x),_) -> VP(UNKNOWNPROC,x)
    | (_,VP(UNKNOWNPROC,x)) -> VP(UNKNOWNPROC,x)
    | (x,y) -> if x = y then x else VP(UNKNOWNPROC,[])) in
  update_sources
    (Aux.sorted_union (collect_sources vp1) (collect_sources vp2))
    vp

let rec lub_vtype vt1 vt2 =
  match (vt1,vt2) with
  | (BOT,x) -> x
  | (x,BOT) -> x
  | (UNKNOWN,x) -> UNKNOWN
  | (x,UNKNOWN) -> UNKNOWN
  | (EMPTY,EMPTY) -> EMPTY
  | (EMPTY,x) -> UNKNOWN
  | (x,EMPTY) -> UNKNOWN
  | (MEMBER(l1),MEMBER(l2)) ->
      let minlen = min (List.length l1) (List.length l2) in
      (match Aux.intersect l1 l2 with
	(* know that it is non-empty, but not contents *)
	[] -> mk_min_seq (procs2srcs (l1@l2)) minlen
	    (* know some of the contents, lose some size information *)
      |	i -> MEMBER(mk_min_set i minlen (procs2srcs (Aux.subtract (l1@l2) i))))

and procs2srcs l =
  List.fold_left
    (function prev -> function x -> Aux.sorted_union (collect_sources x) prev)
    [] l

and mk_min_seq states = function
    0 -> UNKNOWN
  | n ->
      (let rec loop = function
	  0 -> []
        | n -> VP(UNKNOWNPROC,List.sort compare states) :: (loop (n - 1)) in
      MEMBER(loop n))

and mk_min_set l n states =
  let states = List.sort compare states in
  if List.length(l) < n
  then
    (match extract_unknown l with
      Some(old_states,rest) ->
	(VP(UNKNOWNPROC,Aux.sorted_union states old_states)) :: rest
    | None -> (VP(UNKNOWNPROC,states)) :: l)
  else l

and extract_unknown l =
  let split =
    List.partition
      (function (* UNKNOWNPROC shold not appear in an EQUAL pattern *)
	  VP(UNKNOWNPROC,states) -> true
	| _ -> false)
      l in
  match split with
    ([],_) -> None
  | ([VP(UNKNOWNPROC,states)],xs) -> Some(states,xs)
  | _ -> internal_error(1)

and known_contents = function
    UNKNOWN -> []
  | EMPTY -> [] (* perhaps some information is lost here *)
  | MEMBER(l) -> l
  | BOT -> internal_error(2)

and contains_unknown ty = function
    UNKNOWN -> true
  | EMPTY -> false
  | MEMBER(l) ->
      (match ty with
	CS.PROC ->
	  (match extract_unknown l with None -> false | Some(_,_) -> true)
      | CS.QUEUE(_) -> true)
  | BOT -> internal_error(3)

let rec drop_vp f = function
    [] -> []
  | (VP(x,_)::rest) when f x -> drop_vp f rest
  | (((VP(_,_)) as x)::rest) -> x :: drop_vp f rest
  | (EQUAL(vpl)::rest) ->
      match drop_vp f vpl with
	[x] -> x::drop_vp f rest
      |	l -> EQUAL(l)::drop_vp f rest

(* for (x,y) checks that x is at least as informative as y *)
let rec compatible = function
  | (BOT,x) -> true
  | (x,BOT) -> false
  | (x,UNKNOWN) -> true
  | (UNKNOWN,x) -> false
  | (EMPTY,EMPTY) -> true
  | (x,EMPTY) -> false
  | (EMPTY,x) -> false
  | (MEMBER(l1),MEMBER(l2)) -> Aux.subset (drop_sources l2) (drop_sources l1)

and drop_sources (l : vproc list) =
  Aux.flat_map
    (function
	VP(vp,_) -> [vp]
      |	EQUAL(vps) -> drop_sources vps)
    l

let vprocname2c = function
    SRC -> "src"
  | TGT -> "tgt"
  | P(n) -> Printf.sprintf "p%i" n
  | Q(n) -> Printf.sprintf "q%i" n
  | S(n,str) -> Printf.sprintf "s%i_%s" n str
  | ARG(n) -> Printf.sprintf "arg%i" n
  | UNKNOWNPROC -> "x"
  | BOTPROC -> internal_error(4)

let rec vp2c = function
    (VP(vp,srcs)) -> vprocname2c vp
  | EQUAL([]) -> internal_error(15)
  | EQUAL([x]) -> vp2c x
  | EQUAL(x::xs) ->
      List.fold_left
	(function prev ->
	  function vp -> prev ^ "=" ^ (vp2c vp))
	(vp2c x) xs

let rec vp_member x l =
  List.exists
    (function
	VP(v,_) -> x = v
      | EQUAL(l1) -> vp_member x l1)
    l

let rec vp_replace old_name new_name = function
    VP(v,l) as x -> if old_name = v then VP(new_name,l) else x
  | EQUAL(l) -> EQUAL(List.map (vp_replace old_name new_name) l)

let rec vp_remove name l =
  Aux.option_filter
    (function
	VP(v,l) as x -> if name = v then None else Some x
      | EQUAL(l) ->
	  (match vp_remove name l with
	    [] -> internal_error(20)
	  | [x] -> Some x
	  | l -> Some (EQUAL(l))))
    l

let rec collect_ps = function
    EQUAL(l) -> Aux.flat_map collect_ps l
  | VP(UNKNOWNPROC,_) -> []
  | VP(x,_) -> [x]


let rec collect_vps = function
    EQUAL(l) -> Aux.flat_map collect_vps l
  | VP(UNKNOWNPROC,_) -> []
  | x -> [x]

let rec vt2c = function
    UNKNOWN -> "?"
  | EMPTY -> "[]"
  | MEMBER(l) -> Aux.set2c(List.map vp2c l)
  | BOT -> internal_error(5)

let states2c states =
  Aux.set2c (List.map (function CS.STATE(id,_,_,_) -> Objects.id2c id)
	       states)

let rec vp2c_all = function
    VP(vp,l) -> Printf.sprintf "<%s,%s>" (vprocname2c vp) (states2c l)
  | EQUAL(vps) -> "="^(Aux.set2c (List.map vp2c_all vps))

let vt2c_all = function
    UNKNOWN -> "?"
  | EMPTY -> "[]"
  | MEMBER(l) -> Aux.set2c(List.map vp2c_all l)
  | BOT -> internal_error(6)

(* -------------------------- Add operation ------------------------- *)

let add_one_proc p = function
    UNKNOWN -> MEMBER([p])
  | EMPTY -> MEMBER([p])
  | _ -> internal_error(7)

let rec add_one_queue p = function
    UNKNOWN -> MEMBER([p])
  | MEMBER(l) as x ->
      (match p with
	VP(UNKNOWNPROC,states) ->
	  (match extract_unknown l with
	    Some(old_states,rest) ->
	      MEMBER((VP(UNKNOWNPROC,Aux.sorted_union states old_states))
		     :: rest)
	  | None -> MEMBER((VP(UNKNOWNPROC,states)) :: l))
      |	_ ->
	  if (Aux.member p l)
	  then x (* can arise in dstadd because of imprecision in venv *)
	  else MEMBER(p::l))
  | EMPTY -> MEMBER([p])
  | BOT -> internal_error(9)

let add_proc procs dst srcs =
  let p = List.fold_left lub_vproc (VP(BOTPROC,[])) procs in
  (add_one_proc p dst, collect_sources p)

let add_queue procs dst srcs =
  let p = List.fold_left lub_vproc (VP(BOTPROC,[])) procs in
  (add_one_queue p dst, Aux.sorted_union (collect_sources p) srcs)

(* ------------------------ Remove operation ------------------------ *)

(* not known whether the removed process is in the state *)
let rec remove_one_proc_possible p srcs = function
    UNKNOWN -> (p,UNKNOWN,srcs)
  | MEMBER([p1]) ->
      let srcs = List.sort compare srcs in
      (match (p,p1) with
	(VP(UNKNOWNPROC,_),_) -> (p,UNKNOWN,srcs)
      |	(_,VP(UNKNOWNPROC,_)) -> (p,UNKNOWN,srcs)
      |	_ ->
	  let ps = collect_ps p
	  and p1s = collect_ps p1 in
	  if Aux.subset ps p1s then (p1,EMPTY,[]) else internal_error(10))
  | x -> internal_error(11)

(* known that the removed process is in the state *)
let rec remove_one_proc_definite p = function
    UNKNOWN -> (p,EMPTY)
  | MEMBER([p1]) ->
      (match (p,p1) with
	(VP(UNKNOWNPROC,_),p1) -> (p1,EMPTY)
      |	(_,VP(UNKNOWNPROC,_)) -> (p,EMPTY)
      |	_ ->
	  let ps = collect_ps p
	  and p1s = collect_ps p1 in
	  if Aux.subset ps p1s then (p1,EMPTY) else internal_error(12))
  | x ->
      Printf.printf "unexpected proc %s\n" (vt2c x);
      internal_error(13)

let rec remove_one_queue p = function
    UNKNOWN -> (p,UNKNOWN)
  | MEMBER(l) ->
      (match p with
	VP(UNKNOWNPROC,states) ->
	  (p,
	   (match extract_unknown l with
	     None -> MEMBER(l)
	   | Some(old_states,rest) ->
	       (match Aux.intersect states old_states with
		 [] -> MEMBER(l) (* definitely disjoint *)
	       | _ ->
		   (match rest with
		     [] -> UNKNOWN
		   | _ -> MEMBER(rest)))))
      |	_ ->
	  let ps = collect_ps p in
	  let (remd,rest) =
	    List.partition (function p1s -> Aux.subset ps (collect_ps p1s))
	      l in
	  (match remd,rest with
	    ([remd],[]) -> (remd,UNKNOWN)
	  | ([remd],l1) -> (remd,MEMBER(l1))
	  | _ ->
	      Printf.printf "unexpected remd %d\n" (List.length remd);
	      Printf.printf "ps: %s\n" (Aux.set2c(List.map vprocname2c ps));
	      Printf.printf "p1s: %s\n" (Aux.set2c(List.map vp2c l));
	      internal_error(16)))
  | _ -> internal_error(14)

let remove_proc_possible procs dst dsrcs =
  List.fold_left
    (function (remd,pvt,psrcs) ->
      function p ->
	let (remd1,vt,srcs) = remove_one_proc_possible p dsrcs dst in
	(remd1::remd,lub_vtype pvt vt, Aux.sorted_union psrcs srcs))
    ([],BOT,[]) procs

let remove_proc_definite procs dst dsrcs =
  match procs with
    [proc] ->
      let (remd,rest) = remove_one_proc_definite proc dst
      in ([remd],rest,[])
  | _ ->
      List.fold_left
	(function (remd,pvt,psrcs) ->
	  function p ->
	    let (remd1,vt,srcs) = remove_one_proc_possible p dsrcs dst in
	    (remd1::remd,lub_vtype pvt vt, Aux.sorted_union psrcs srcs))
	([],BOT,[]) procs

let remove_queue procs dst dsrcs =
  let (remd,vt) =
    List.fold_left
      (function (remd,prev) ->
	function p ->
	  let (remd1,vt) = remove_one_queue p dst in
	  (remd1::remd, lub_vtype prev vt))
      ([],BOT) procs in
  (remd,vt,dsrcs)

(* --------------- Drop unknown proc, for explication --------------- *)

let drop_unknownproc = function
    MEMBER(l) ->
      (match List.filter (function VP(UNKNOWNPROC,_) -> false | _ -> true) l
      with
	[] -> UNKNOWN
      |	l1 -> MEMBER(l1))
  | vt -> vt

(* -------------------- Modification information -------------------- *)

(* MODIFIED really means "added".  UNMODIFIED really means "nothing added" *)
type modified = MODIFIED | UNMODIFIED | BOTMOD

let lub_modified = function
    (MODIFIED,_) -> MODIFIED
  | (_,MODIFIED) -> MODIFIED
  | _ -> UNMODIFIED

let modified2c = function
    MODIFIED -> "addition allowed"
  | UNMODIFIED -> "addition prohibited"
  | BOTMOD -> "bottom"
