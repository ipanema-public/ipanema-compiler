val debug : bool ref
val output_name : string ref

exception Error of string
val init_error : unit -> unit
val update_error : unit -> unit
val check_errors : string -> unit
val any_errors : unit -> bool
val internal_error : unit -> 'a
