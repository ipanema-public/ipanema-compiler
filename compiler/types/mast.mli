(* -------------------- The scheduler structure ------------------- *)
val list_of_const_enum : string list ref

type scheduler =
  SCHEDULER of
    string                (* name *)
    * bool                (* default scheduler *)
    * valdef list         (* constant declarations *)
    * typedef list        (* programmer declared types *)
    * vardecl list        (* fields of the process structure *)
    * fundecl list        (* external function declarations *)
    * valdef list         (* (non-constant) variable declarations *)
    * state list          (* states *)
    * cstate list          (*core states *)
    * criteria list       (* ordering criteria *)
    * trace               (* tracing declaration *)
    * event_decl list     (* event handlers *)
    * fundef list         (* interface functions *)
    * fundef list         (* verifier-defined functions *)
    * Objects.attribute

and typedef =
  ENUMDEF of
    Objects.identifier          (* the name of the declared type *)
    * Objects.identifier list   (* the list of possible values *)
    * Objects.attribute
  | RANGEDEF of
    Objects.identifier          (* the name of the declared type *)
    * expr                      (* the lower bound *)
    * expr                      (* the upper bound *)
    * Objects.attribute

and vardecl =
  VARDECL of
    requires
    * Objects.typ                 (* the type of the declared identifier *)
    * Objects.identifier        (* the name of the declared identifier *)
    * bool                      (* true if imported from the system *)
    * Objects.attribute
    and requires = REQUIRES | NOT_REQUIRES

(* useful to avoid a parser problem *)
and pre_valdef = FN of fundecl | VL of valdef

and fundecl =
  FUNDECL of
    Objects.typ                 (* the return type *)
    * Objects.identifier        (* the name of the declared function *)
    * Objects.typ list          (* the types of the arguments *)
    * Objects.attribute

and valdef =
  VALDEF of
    vardecl               (* the type and name of the declared identifier *)
    * expr                (* the initial value *)
    * is_const            (* CONST if constant, VARIABLE otherwise *)
    * Objects.attribute
  | SYSDEF of
    vardecl               (* the type and name of the declared identifier *)
    * is_const            (* CONST if constant, VARIABLE otherwise *)
    * Objects.attribute
  | DUMMYDEF of           (* used by parser, when type not known (a const) *)
    Objects.identifier          (* declared identifier *)
    * expr                (* inital value *)
    * Objects.attribute

  and is_const = CONST | VARIABLE | DEFCONST

and state =
  QUEUE of
    Class_state.classname           (* the class of the state *)
    * Class_state.queue_order       (* FIFO, or SORTED *)
    * Objects.identifier        (* the name of the state *)
    * Class_state.visibility       (* the visibility of the queue *)
    * Class_state.share_state   (* the share of the queue with different module*)
    * Objects.attribute
  | PROCESS of
    Class_state.classname           (* the class of the state *)
    * Objects.identifier        (* the name of the state *)
    * prev                      (* the name of the previous value of the state,
			   if any *)
    * Class_state.visibility       (* the visibility of the process variable *)
    * Class_state.share_state   (* the share of the process with different module*)
    * Objects.attribute

and prev =
    NO_PREV | RTS_PREV of Objects.identifier | BOSSA_PREV of Objects.identifier

and cstate =
  CSET of
    Class_state.corename          (* the class of the state *)
    * Objects.identifier        (* the name of the state *)
    * Objects.attribute

and criteria =
  CRIT_ID of key * crit_order * Objects.identifier * Objects.identifier * Objects.attribute
  | CRIT_TEST of key * crit_order * expr * expr * expr * Objects.attribute
  | IMPORT of Objects.identifier (* module name *) * Objects.attribute

and module_criteria = (* criteria in a module; no module name for vars *)
  MOD_CRIT_ID of 
    key
    * crit_order
    * Objects.identifier (* variable *)
    * Objects.attribute
  | MOD_CRIT_TEST of
      key
      * crit_order
      * expr
      * expr
      * expr
      * Objects.attribute

and key = KEY of int (* lower limit *) * int (* upper limit *) | NOKEY

and crit_order = HIGHEST | LOWEST

and admit =
  IMPORT_ADMIT of
    Objects.identifier list
    * Objects.attribute
  | NO_ADMIT of
    Objects.attribute
  | ADMIT of
    valdef list               (* encapsulated variables *)
    * vardecl list            (* criteria parameters - should be read only *)
    * expr list               (* criteria *)
    * (vardecl * stmt) option (* attach/new code *)
    * (vardecl * stmt) option (* detach/end code *)
    * Objects.attribute

and mod_admit =
  MOD_ADMIT of
    valdef list             (* encapsulated variables *)
    * vardecl list          (* criteria parameters - should be read only *)
    * expr                  (* criteria *)
    * (vardecl * stmt) option (* attach/new code *)
    * (vardecl * stmt) option (* detach/end code *)
    * Objects.attribute

and trace =
    NOTRACE
  | PRETRACE of int * trace_info list * Objects.attribute
  | TRACE of int * event_name list * classified_exp list * expr option *
	Objects.attribute

  and trace_info =
    TRACEEVENTS of event_name list
  | TRACEEXPS of classified_exp list
  | TRACETEST of expr

  and classified_exp =
    TRACEVAR of Objects.identifier * Objects.identifier * Objects.attribute
  | TRACEFIELD of Objects.identifier * Objects.attribute
  | TRACEQUEUE of Objects.identifier * Objects.identifier * Objects.attribute

and event_decl =
  EVENT of
    event_name            (* the name of the event *)
    * event_name list     (* predecessors for which this event is specialized*)
    * vardecl             (* the event parameters (always the same) *)
    * overrideable        (* is the handler overrideable *)
    * stmt                (* the body of the event handler *)
    * Objects.attribute

  and event_name =
    EVENT_NAME of
      Objects.identifier list   (* hierarchical event names *)
      * bool                    (* whether the name ends with * *)
      * Objects.attribute

and overrideable = OVERRIDEABLE | NOT_OVERRIDEABLE

and aspect_decl =
  ASPECT of
    Objects.identifier list     (* initial states/classes *)
    * Objects.identifier list   (* final states/classes *)
    * vardecl             (* the event parameters (always the same) *)
    * position
    * stmt                (* the body of the event handler *)
    * Objects.attribute

and position = ON | BEFORE | AFTER

and fundef =
  FUNDEF of
    Objects.typ                 (* the return type of the function *)
    * Objects.identifier        (* the name of the function *)
    * vardecl list        (* the parameter list of the function *)
    * stmt                (* the body of the function *)
    * inl                 (* true if the function should be inlined *)
    * Objects.attribute

    and inl = INLINE | NO_INLINE
    
(* ------------------- The Module Structure ------------------------*)
and module_def =
    MODULE of
      Objects.identifier
        * state list        (* module parameters (module signature) *)
	* valdef list       (* constant declarations *)
	* typedef list      (* programmer declared types *)
        * valdef list       (* variable/global declarations *)
        * vardecl list      (* fields of the process structure *)
	* fundecl list      (* external function declarations *)
	* module_criteria list (* ordering criteria *)
	* mod_admit option  (* admission criteria *)
        * event_decl list   (* event handlers *)
        * fundef list       (* interface functions *)
        * fundef list       (* module defined functions *)
        * aspect_decl list  (* aspects *)
        * Objects.attribute

and sched_module_def =
    SCHED_MODULE of
      Objects.identifier
        * bool              (* default scheduler *)
        * state list        (* states *)
        * cstate list        (* core states *)
        * uses list
        * requiresdef list  (* for globaldef *)
        * requiresdef list  (* for processdef *)
        * criteria list
	* admit
        * handlerdef list
        * functiondef list
        * Objects.attribute

and super_sched = MODL of module_def | SCHED of sched_module_def

and handlerdef =
    HANDLERDEF of
      event_name
        * event_body list
        * Objects.attribute
and event_body =
    EVENT_BODY of
      Objects.identifier
        * event_name
        * Objects.attribute

and functiondef =
    FUNCTIONDEF of
      Objects.identifier (* function name *)
    * function_body list
    * Objects.attribute
and function_body =
    FUNCTION_BODY of
      Objects.identifier (* module name *)
    * Objects.identifier (* function name *)
    * Objects.attribute

    and requiresdef =
        REQUIRESDEF of
          Objects.identifier   (* module name *)
            * Objects.identifier (* variable name *)
            * Objects.identifier (* second module name *)
            * Objects.identifier (* second variable name *)
            * Objects.attribute
        and uses =
          USES of
            Objects.identifier          (* module name *)
            * Objects.identifier list   (* module signature *)
            * Objects.attribute
    
(* ------------------- Statements and expressions ----------------- *)

and stmt =
  IF of
    expr                  (* test expression *)
    * stmt                (* then branch *)
    * stmt                (* else branch *)
    * Objects.attribute
  | NEXT of
    Objects.attribute
  | FOR of
    Objects.identifier          (* index variable *)
    * Objects.identifier list option (* state or class name *)
    * direction           (* order in which elements are chosen *)    
    * stmt                (* loop body *)
    * Objects.attribute
  | SWITCH of
    expr                  (* test expression *)
    * seq_case list       (* list of cases *)
    * stmt option         (* default *)
    * Objects.attribute
  | SEQ of
    valdef list           (* list of local variables *)
    * stmt list           (* list of statements *)
    * Objects.attribute
  | RETURN of
    expr option           (* returned expression, if any *)
    * Objects.attribute
  | MOVE of
    expr                        (* moved process *)
    * Objects.identifier        (* destination state *)
    * bool                (* true if move from a state to itself allowed *)
    * state_end           (* head, tail, or anywhere *)
    * Objects.attribute
  | MOVEFWD of
    expr                  (* moved process *)
    * state_end           (* head, tail, or anywhere *)
    * Objects.attribute
  | ASSIGN of
    expr                  (* assigned location *)
    * expr                (* assigned value *)
    * Objects.attribute
  | DEFER of
    Objects.attribute
  | PRIMSTMT of           (* not in source language *)
    expr                  (* called function *)
    * expr list           (* arguments *)
    * Objects.attribute
  | ERROR of
    string                (* the error message *)
    * Objects.attribute
  | BREAK of              (* break *)
    Objects.attribute
  | CONTINUE of              (* continue *)
    Objects.attribute


  and direction = INC | DEC | RANDOM

  and state_end = HEAD | TAIL | EITHER

  and seq_case =
    SEQ_CASE of
      Objects.identifier list   (* list of matching state names *)
      * Class_state.state_info list (* information about all possible states *)
      * stmt              (* case statement *)
      * Objects.attribute

and expr =
  INT        of int * Objects.attribute
  | VAR      of Objects.identifier * Objects.attribute
  | FIELD_OC of Objects.identifier * Objects.identifier * Objects.attribute
  | FIELD    of expr * Objects.identifier * Objects.attribute
  | BOOL     of bool * Objects.attribute
  | UNARY    of uop * expr * Objects.attribute
  | BINARY   of bop * expr * expr * Objects.attribute
  | INDR     of expr * Objects.attribute
  | SELECT   of Objects.attribute
  | EMPTY    of Objects.identifier * Objects.attribute
  | PRIM     of expr * expr list * Objects.attribute
  | SCHEDCHILD  of expr * Objects.attribute
  | SRCONSCHED of Objects.attribute
 (* | AREF     of expr * expr * Objects.attribute (* only in output *) *)
  | IN       of expr * Objects.identifier * Objects.attribute
  (*| MOVEFWDEXP of
    expr                  (* moved process *)
    * state_end
    * Objects.attribute*)

  and uop = NOT | COMPLEMENT
  and bop = PLUS | MINUS | TIMES | DIV | MOD | AND | OR | BITAND | BITOR
    | EQ | NEQ | LT | GT | LEQ | GEQ | LSHIFT | RSHIFT

(* --------------------- Constructor functions -------------------- *)

val mkSCHEDULER : 
    string * bool * valdef list * typedef list * vardecl list *
    pre_valdef list * state list * cstate list *
    criteria list * trace * event_decl list * fundef list * int -> scheduler
(**************************************************************************)
val mkNEXT : int -> stmt
val mkMODULE : string * state list * valdef list * typedef list *
    pre_valdef list * vardecl list * module_criteria list * mod_admit option *
    event_decl list * fundef list * aspect_decl list * int -> module_def
val mkSCHED_MODULE : string * bool * state list * cstate list * uses list *
    requiresdef list * requiresdef list * criteria list * admit *
    handlerdef list * functiondef list * int -> sched_module_def
val mkHANDLERDEF : event_name * event_body list * int -> handlerdef
val mkEVENT_BODY : string * event_name * int -> event_body
val mkFUNCTIONDEF : string * function_body list * int -> functiondef
val mkFUNCTION_BODY : string * string * int -> function_body
val mkREQUIRESDEF : string * string * string * string * int -> requiresdef
val mkUSES : string * string list * int -> uses
val mkFIELD_OC: string * string * int -> expr
(**************************************************************************)
val mkENUMDEF : string * string list * int -> typedef
val mkRANGEDEF : string * expr * expr * int -> typedef

val mkFUNDECL : Objects.typ * string * Objects.typ list * int -> fundecl

val mkVARDECL : requires * Objects.typ * string * int -> vardecl
val mkSYSVARDECL : requires * Objects.typ * string * int -> vardecl

val mkCONSTDEF : vardecl * expr * int -> valdef
val mkVALDEF : vardecl * expr * int -> valdef
val mkSYSDEF : vardecl * int -> valdef

val mkQUEUE :
    Class_state.classname * Class_state.queue_order * string *
    Class_state.visibility * Class_state.share_state * int -> state
val mkPROCESS :
    Class_state.classname * string * Class_state.visibility * Class_state.share_state * int -> state
val mkPREVPROCESS :
    Class_state.classname * string * 
    Class_state.visibility * Class_state.share_state * string * int -> state

val mkCRIT_ID   : key * crit_order * string * string * int -> criteria
val mkCRIT_TEST : key * crit_order * expr * expr * expr * int -> criteria
val mkIMPORT : string * int -> criteria

val mkMOD_CRIT_ID   : key * crit_order * string * int -> module_criteria
val mkMOD_CRIT_TEST : key * crit_order * expr * expr * expr * int ->
  module_criteria

val mkIMPORT_ADMIT : string list * int -> admit
val mkNO_ADMIT : int -> admit

val mkMOD_ADMIT : valdef list * vardecl list * expr * (vardecl * stmt) option *
    (vardecl * stmt) option * int -> mod_admit

val mkTRACE     : int * trace_info list * int -> trace
val mkTRACEEXP  : string list * int -> classified_exp list

val mkEVENT : event_name * vardecl * overrideable * stmt * int -> event_decl

val mkEVENT_NAME : string list * bool * int -> event_name

val mkASPECT : string list * string list * vardecl * position * stmt * int ->
  aspect_decl

val mkFUNDEF : Objects.typ * string * vardecl list * stmt * int -> fundef

val mkIF         : expr * stmt * stmt * int -> stmt
val mkFOR        : string * string list * stmt * int -> stmt
val mkFORINC     : string * string * stmt * int -> stmt
val mkFORDEC     : string * string * stmt * int -> stmt
val mkFOR_ANY    : string * stmt * int -> stmt
val mkSWITCH     : expr * seq_case list * int -> stmt
val mkSEQ        : valdef list * stmt list * int -> stmt
val mkRETURN     : expr * int -> stmt
val mkRETURNVOID : int -> stmt
val mkMOVE       : expr * string * int -> stmt
val mkMOVEHD     : expr * string * int -> stmt
val mkMOVETL     : expr * string * int -> stmt
val mkMOVEFWD    : expr * int -> stmt
val mkMOVEHDFWD  : expr * int -> stmt
val mkMOVETLFWD  : expr * int -> stmt
val mkASSIGN     : expr * expr * int -> stmt
val mkDEFER      : int -> stmt
val mkPRIMSTMT   : string * expr list * int -> stmt
val mkERROR      : string * int -> stmt
val mkBREAK      : int -> stmt
val mkCONTINUE   : int -> stmt

val mkSEQ_CASE : string list * stmt * int -> seq_case

val mkINT        : int * int -> expr
val mkVAR        : string * int -> expr
val mkFIELD      : expr * string * int -> expr
val mkBOOL       : bool * int -> expr
val mkUNARY      : uop * expr * int -> expr
val mkBINARY     : bop * expr * expr * int -> expr
val mkINDR       : expr * int -> expr
val mkSELECT     : int -> expr
val mkEMPTY      : string * int -> expr
val mkEMPTYCLASS : Class_state.classname * int -> expr
val mkPRIM       : string * expr list * int -> expr
val mkSCHEDCHILD : expr * int -> expr
val mkSRCONSCHED : int -> expr
val mkIN         : expr * string * int -> expr
val mkINCLASS    : expr * Class_state.classname * int -> expr

val mkCOMPOUND_ASSIGN : expr * bop * expr * int -> stmt

(* ------------------------- Useful syntax ------------------------ *)

val mkBLOCK : Objects.attribute -> stmt list -> stmt
val mkDBLOCK : Objects.attribute -> valdef list * stmt list -> stmt
val addBLOCK : Objects.attribute -> stmt -> stmt -> stmt
val postBLOCK : Objects.attribute -> stmt -> stmt list -> stmt
val add_decl_block : valdef -> stmt -> stmt

(* ----------------------- Accessor functions --------------------- *)

val get_exp_attr : expr -> Objects.attribute
val get_stm_attr : stmt -> Objects.attribute

(* ---------------------------- Comments -------------------------- *)

val add_comment : string -> expr -> expr

(* ----------------------- Printing functions --------------------- *)

val event_name2c : event_name -> string
val event_name2c_ : event_name -> string
val event_name2c1 : event_name -> string option
val event_name2c2 : event_name -> string option
val event_name_equal : event_name -> event_name -> bool
val event_names_equal : event_name list -> event_name list -> bool
val event_name_prefix : event_name -> event_name -> bool
val event_name2partial_mask : int -> event_name -> string
val event_name2mask : event_name -> string
val bop2c : bop -> string
val equal_except_attributes : expr -> expr -> bool

(* --------------------- Special process names --------------------- *)

val is_src_or_tgt : expr -> Objects.identifier ->
  (Vobjects.vprocname * Class_state.state_info list) list *
    Class_state.state_info list
  -> Vobjects.vproc

(* ----------------------------- Timers ---------------------------- *)

val collect_timer_names :
    vardecl list -> valdef list -> Objects.identifier list

(* ---------------------- Variable environments --------------------- *)

val params2ids : vardecl list -> Objects.identifier list

val valdefs2ids : valdef list -> Objects.identifier list
