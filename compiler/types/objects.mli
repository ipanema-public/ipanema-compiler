(* ------------- Process scheduler and event version *)

type events = LINUX45 (* Ipanema *)
val config : events ref
val set_os : string -> unit

type sched = PROCSCHED 
(* --------------------------- Identifiers -------------------------- *)

(* string for printing, int for comparison *)
type identifier = ID of string * int
val init_id : unit -> unit
val mkId : string -> identifier
val ideq : (identifier * identifier) -> bool
val idnameq: (identifier * identifier) -> bool
val strideq : (string * identifier) -> bool
val id2c : identifier -> string
val id2n : identifier -> int
val nopid : identifier
val fresh_id : unit -> identifier
val fresh_idxstr : string -> string
val fresh_idx : string -> identifier

val symtab : ((string, identifier) Hashtbl.t) ref
val index : int ref
val fresh_ctr : int ref

(* ------------------------------ Types ----------------------------- *)

type typ =
  ENUM of identifier | RANGE of identifier | SYSTEM of typ | CORE | DISTANCE |
  UINT | INT | BOOL | TIME | CYCLES | PROCESS | SRCPROCESS | SCHEDULER | VOID |
  PEVENT | CEVENT | INDR of typ | STRUCT of identifier | NULL | EMPTY | PORT |
  CLASS of identifier | GROUP | DOMAIN | CONST of typ | OPAQUE of string |
  SCHEDULER_INST | TIMER of timer_persistence | QUEUE of typ | SET of typ

and timer_persistence = NOT_PERSISTENT | PERSISTENT of identifier option

val type2c : typ -> string     (* for debugging *)
val type2ctype : typ -> string (* for C code *)

type locktyp =
    CORESTATE  | GLOBALVAR | SHAREDLOCAL | PRIVATELOCAL
type lockscope =
    EVENTLOCK  | USELOCK | NOLOCK
(* ---------------------------- Attributes -------------------------- *)

(* test_index is used to correlate the occurrences of tests in specialized
handlers, to know when the test is true or false in all instances, to know
when to give an error message *)

type location = MODULE_NAME of identifier | SCHEDULER_NAME of identifier
type attribute = {gen: string ; modl : location ref option; ln : int; test_index : int;
                  ty : typ; com : string list; lck : (identifier*lockscope) list}
val module_name : location ref option ref
val current_comment : string ref
val mkAttr : int -> attribute
val mkTestAttr : int * int -> attribute
val modl : attribute -> location ref option
val line : attribute -> int
val test_index : attribute -> int
val ty : attribute -> typ
val com : attribute -> string option
val updty : attribute -> typ -> attribute
val updtst : attribute -> int -> attribute
val updlck : attribute -> ((identifier*lockscope) list) -> attribute
val updgen : attribute -> string -> attribute
val add_comment : attribute -> string -> attribute
val loc2c : attribute -> string
val dum : string -> attribute

(* ----------------- Optimizing empty and in tests ------------------ *)

type testval = TRUE | FALSE | BOTH | UNK
val lubtestval : testval * testval -> testval
val testval2c : testval -> string

(* -------------------------- For events ---------------------------- *)

type intinfo = INTIBLE | UNINTIBLE

val mutate : bool ref

val csharp : bool ref
val hls : bool ref
val blast : bool ref
val high_res : bool ref

val init_ioctls : string ref

val type_return : string ref

type isquasi = QUASI | NOT_QUASI   (* detect quasi event types *)

(* -------------------------- Primitives ---------------------------- *)

val prim_names : unit -> string list

(* ------------------------ Free variables ------------------------- *)

val capture_check : (identifier * attribute) list ->
    identifier list -> (string -> unit) -> unit
