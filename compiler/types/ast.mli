(* -------------------- The scheduler structure ------------------- *)
val list_of_const_enum : string list ref

type fcttype = DFT | TRYLOCK | SYSTEM

type scheduler =
  SCHEDULER of
    string                (* name *)
    * valdef list         (* constant declarations *)
    * typedef list        (* programmer declared types *)
    * vardecl list        (* fields of the domain structure *)
    * vardecl list        (* fields of the group structure *)
    * vardecl list        (* fields of the process structure *)
    * fundecl list        (* external function declarations *)
    * valdef list         (* (non-constant) variable declarations *)
    * domains option      (* description of how to build scheduling domains/groups *)
    * pcstate             (* global or per-core states *)
    * cstate list         (* core states *)
    * criteria list       (* ordering criteria *)
    * trace               (* tracing declaration *)
    * event_decl list     (* event handlers *)
    * event_decl list     (* core event handlers *)
    * fundef list         (* interface functions *)
    * fundef list         (* verifier-defined functions *)
    * Objects.attribute

and typedef =
   STRUCTDEF of
    Objects.identifier          (* the name of the declared type *)
    * vardecl list              (* the list of the fields *)
    * Objects.attribute
  | ENUMDEF of
    Objects.identifier          (* the name of the declared type *)
    * Objects.identifier list   (* the list of possible values *)
    * Objects.attribute
  | ENUMVALDEF of
    Objects.identifier          (* the name of the declared type *)
    * (Objects.identifier * expr) list (* the list of possible values *)
    * Objects.attribute
  | RANGEDEF of
    Objects.identifier          (* the name of the declared type *)
    * expr                      (* the lower bound *)
    * expr                      (* the upper bound *)
    * Objects.attribute
  | TIMERDEF of
    Objects.identifier          (* the name of the declared type *)
    * vardecl list              (* fields of the timer structure *)
    * Objects.attribute

and vardecl =
  VARDECL of
    Objects.typ                 (* the type of the declared identifier *)
    * Objects.identifier        (* the name of the declared identifier *)
    * bool                      (* true if imported from the system    *)
    * bool                      (* true if lazy computed               *)
    * Class_state.share_state   (* SHARED if the field is shared       *)
    * expr option               (* the lazy/init expression, if any    *)
    * Objects.attribute

(* useful to avoid a parser problem *)
and pre_valdef = FN of fundecl | VL of valdef

and fundecl =
  FUNDECL of
    fcttype               (* normal, trylock or system function *)
    * Objects.typ         (* the return type *)
    * Objects.identifier  (* the name of the declared function *)
    * Objects.typ list    (* the types of the arguments *)
    * Objects.attribute

and valdef =
  VALDEF of
    vardecl               (* the type and name of the declared identifier *)
    * expr                (* the initial value *)
    * is_const            (* CONST if constant, VARIABLE otherwise *)
    * Objects.attribute
  | UNINITDEF of          (* for temporaries introduced by the compiler *)
    vardecl               (* the type and name of the declared identifier *)
    * Objects.attribute         (* can't be constant *)
  | SYSDEF of
    vardecl               (* the type and name of the declared identifier *)
    * is_const            (* CONST if constant, VARIABLE otherwise *)
    * Objects.attribute
  | DUMMYDEF of           (* used by parser, when type not known (a const) *)
    Objects.identifier          (* declared identifier *)
    * expr                (* inital value *)
    * Objects.attribute

  and is_const = CONST | CONSTVAR | VARIABLE | DEFCONST

  and cstate =
    CSTATE of
      Class_state.corename   (* the class of the core *)
      * Objects.identifier   (* the name of the core *)
      * Objects.attribute
    | CSET of
	Class_state.corename (* the class of the core *)
	* Objects.identifier (* the name of the core *)
	* Objects.attribute

  and state =
  QUEUE of
    Class_state.classname           (* the class of the state *)
    * Class_state.share_state       (* shared or not *)
    * Class_state.queue_order       (* FIFO, or SORTED *)
    * Objects.identifier        (* the name of the state *)
    * Class_state.visibility       (* the visibility of the queue *)
    * criteria  option
    * Objects.attribute
  | PROCESS of
    Class_state.classname           (* the class of the state *)
    * Class_state.share_state       (* shared or not *)
    * Objects.identifier        (* the name of the state *)
    * prev                      (* the name of the previous value of the state,
			   if any *)
    * Class_state.visibility       (* the visibility of the process variable *)
    * Objects.attribute

(* Structure capturing the core/process states information,
 * including core variables *)
and pcstate =
   CORE of
       vardecl list
     * state list
     * steal_param option
   | PSTATE of
         state list

and steal_param = string * string * string * steal
and steal =
    FLAT_STEAL_GROUP of steal_group * steal_thread * stmt list
  | ITER_STEAL_GROUP of steal_group * steal_thread * stmt list * stmt list
  | STEAL_THREAD of steal_thread

and steal_group = filter * select_grp * expr
and steal_thread =
    FLAT_STEAL_THREAD of steal_thread_blk
  | ITER_STEAL_THREAD of steal_thread_blk * expr * stmt list

and steal_thread_blk = filter * select * migration

and filter = (vardecl * vardecl * stmt)
and select_grp = (vardecl * vardecl * vardecl * stmt)
and select = (vardecl * vardecl * stmt)
and migration =
  (*
    - stolen core
    - thief core
    - stolen group - optional
    - stolen thread
    - migration condition
    - stopping condition
    - internal statement to split migration in 2 phases
  *)
  (vardecl * vardecl * vardecl option * vardecl * stmt * expr * stmt)

and domains = string * string * domain list
and domain =
    DOMFOR of string * int option * domain list
  | DOMDOM of string * expr * string * domgrp
and domgrp = string * string * expr * string

and prev =
    NO_PREV | RTS_PREV of Objects.identifier | BOSSA_PREV of Objects.identifier

and criteria =
  | CRIT_ID of
      key
      * crit_order
      * Objects.identifier            (* field used as criteria *)
      * Objects.identifier option     (* id of comparison function (generated) *)
      * Objects.attribute
  | CRIT_EXPR of
      key
      * crit_order
      * expr                    (* expr used as criteria *)
      * Objects.identifier option     (* id of comparison function (generated) *)
      * Objects.attribute

and key = KEY of int (* low *) * int (* high *) | NOKEY

and crit_order = HIGHEST | LOWEST

and trace =
    NOTRACE
  | PRETRACE of int * trace_info list * Objects.attribute
  | TRACE of int * event_name list * classified_exp list * expr option *
	Objects.attribute

  and trace_info =
    TRACEEVENTS of event_name list
  | TRACEEXPS of classified_exp list
  | TRACETEST of expr

  and classified_exp =
    TRACEVAR of Objects.identifier * Objects.identifier * Objects.attribute
  | TRACEFIELD of Objects.identifier * Objects.attribute
  | TRACEQUEUE of Objects.identifier * Objects.identifier * Objects.attribute

and event_decl =
  EVENT of
    event_name            (* the name of the event *)
    * vardecl             (* the event parameters (always the same) *)
    * stmt                (* the body of the event handler *)
    * bool                (* is the event called with mutex *)
    * Objects.attribute

  and event_name =
    EVENT_NAME of
      Objects.identifier (* hierarchical event names *)
      * Objects.attribute

and fundef =
  FUNDEF of
    fcttype               (* normal, trylock or system function *)
    * Objects.typ         (* the return type of the function *)
    * Objects.identifier  (* the name of the function *)
    * vardecl list        (* the parameter list of the function *)
    * stmt                (* the body of the function *)
    * inl                 (* true if the function should be inlined *)
    * Objects.attribute

    and inl = INLINE | NO_INLINE | EXPORTED

(* ------------------- Statements and expressions ----------------- *)

and stmt =
  IF of
    expr                  (* test expression *)
    * stmt                (* then branch *)
    * stmt                (* else branch *)
    * Objects.attribute
  | FOR of
    Objects.identifier          (* index variable *)
    * expr list option (* state or class name *)
    * Class_state.state_info list   (* information about possible states *)
    * direction           (* order in which elements are chosen *)
    * stmt                (* loop body *)
    * criteria option     (* optional ordering constraint. FIXME: merge with direction *)
    * Objects.attribute
  | FOR_WRAPPER of
    Objects.identifier        (* break label *)
    * stmt list         (* list of for loops *)
    * Objects.attribute
  | SWITCH of
    expr                  (* test expression *)
    * seq_case list       (* list of cases *)
    * stmt option         (* default *)
    * Objects.attribute
  | SEQ of
    valdef list           (* list of local variables *)
    * stmt list           (* list of statements *)
    * Objects.attribute
  | RETURN of
    expr option           (* returned expression, if any *)
    * Objects.attribute
  | STEAL of
    expr                  (* core expression of the thief *)
    * Objects.attribute
  | MOVE of
      expr                          (* moved process *)
    * expr                          (* destination state *)
    * Class_state.state_info option (* info about state of source, if known *)
    * Class_state.state_info list   (* information about the source collected
				       in the verifier *)
    * Class_state.state_info option (* info about state of target, if known *)
    * bool                (* true if move from a state to itself allowed *)
    * state_end           (* head, tail, or anywhere *)
    * Objects.attribute
  | MOVEFWD of
    expr                  (* moved process *)
    * (Class_state.state_info * Class_state.state_info) list
                          (* information about the sources and dests collected
			     in the verifier *)
    * state_end           (* head, tail, or anywhere *)
    * Objects.attribute
  | SAFEMOVEFWD of        (* if there might be no next scheduler, not in lang*)
    expr                  (* moved process *)
    * (Class_state.state_info * Class_state.state_info) list
                          (* information about the sources and dests collected
			     in the verifier *)
    * stmt                (* default action *)
    * state_end           (* head, tail, or anywhere *)
    * Objects.attribute
  | ASSIGN of
    expr                  (* assigned location *)
    * expr                (* assigned value *)
    * bool                (* can the assignment affect a field of something
			     on a sorted queue *)
    * Objects.attribute
  | DEFER of
    Objects.attribute
  | PRIMSTMT of           (* not in source language *)
    expr                  (* called function *)
    * expr list           (* arguments *)
    * Objects.attribute
  | ERROR of
    string                (* the error message *)
    * Objects.attribute
  | BREAK of              (* break *)
    Objects.attribute
  | CONTINUE of           (* continue *)
    Objects.attribute
  | ASSERT of             (* not in source language *)
    expr                  (* expression asserted to be true *)
    * Objects.attribute

  and direction = INC | DEC | RANDOM

  and state_end = HEAD | TAIL | EITHER

  and seq_case =
    SEQ_CASE of
      Objects.identifier list   (* list of matching state names *)
      * Class_state.state_info list (* information about all possible states *)
      * stmt              (* case statement *)
      * Objects.attribute

and expr =
    SELF     of Objects.attribute
  | SUM      of Objects.attribute
  | MIN	     of Objects.attribute
  | MAX	     of Objects.attribute
  | COUNT    of Objects.attribute
  | FNOR     of Objects.attribute
  | DISTANCE of Objects.attribute
  | INT      of int * Objects.attribute
  | VAR      of Objects.identifier * (string list) * Objects.attribute
  | FIELD    of expr * Objects.identifier * (string list) * Objects.attribute
  | CAST     of Objects.typ * expr * Objects.attribute
  | TYPE     of Objects.typ * Objects.attribute
  | LCORE    of string * expr * expr option * Objects.attribute
  | BOOL     of bool * Objects.attribute
  | PARENT   of Objects.attribute
  | UNARY    of uop * expr * Objects.attribute
  | BINARY   of bop * expr * expr * Objects.attribute
  | TERNARY  of expr * expr * expr * Objects.attribute
  | PBINARY  of bop * expr * Class_state.pstate_info list
	        * expr * Class_state.pstate_info list
    * Objects.attribute (* Introduce by rewrite of BINARY in verifier/pre_verif::pb_exp *)
  | CONDOP   of expr * expr * expr * Objects.attribute
  | INDR     of expr * Objects.attribute
  | SELECT   of Class_state.state_info option * Objects.attribute
  (* For the parser. Independant of the expression type *)
  | FIRST    of expr * criteria option * Class_state.state_info option  * Objects.attribute
  (* To replace FIRST for processes in pre_verif *)
  | PFIRST   of expr * criteria option * Class_state.pstate_info option  * Objects.attribute
  (* To replace FIRST for cores in pre_verif *)
  | CFIRST   of expr * criteria option * Class_state.cstate_info option  * Objects.attribute
  | EMPTY    of Objects.identifier * Class_state.state_info list *
	        Objects.testval * bool * Objects.attribute
  | VALID    of expr * Class_state.state_info option * Objects.attribute
  | PRIM     of expr * expr list * Objects.attribute
  | SCHEDCHILD of expr * Vobjects.vprocname list * Objects.attribute
  | SRCONSCHED of Objects.attribute
  | ALIVE    of Objects.testval * bool (*insrc*) * Objects.attribute
  | AREF     of expr * expr option * Objects.attribute (* only in output *)
  | IN       of expr * expr * Class_state.state_info list *
	        Objects.testval * bool * criteria option * Objects.attribute
  | MOVEFWDEXP of           (* not in source language *)
      expr                  (* moved process *)
      * state_end
    * Objects.attribute
  | INT_STRING of string * Objects.attribute
  | INT_NULL of Objects.attribute

  and uop = NOT | COMPLEMENT
  and bop = PLUS | MINUS | TIMES | DIV | MOD | AND | OR | BITAND | BITOR
    | EQ | NEQ | LT | GT | LEQ | GEQ | LSHIFT | RSHIFT

(* Helper function *)

val get_init: Objects.typ -> expr

(* --------------------- Constructor functions -------------------- *)
	
val mkSCHEDULER :
    string * valdef list * typedef list * (bool * vardecl list) list * vardecl list *
    pre_valdef list * domains option * pcstate * cstate list *
    criteria list * trace * event_decl list * event_decl list *
    fundef list (* interface functions *) *
    fundef list (* functions *) * int -> scheduler

val mkSTRUCTDEF : string * vardecl list * int -> typedef
val mkENUMDEF : string * string list * int -> typedef
val mkRANGEDEF : string * expr * expr * int -> typedef
val mkTIMERDEF : string * vardecl list * int -> typedef

val mkFUNDECL : fcttype * Objects.typ * string * Objects.typ list * int -> fundecl

val mkVARDECL :     Class_state.share_state * Objects.typ * string * int -> vardecl
val mkSYSVARDECL :  Class_state.share_state * Objects.typ * string * int -> vardecl
val mkLAZYVARDECL : Class_state.share_state * Objects.typ * string * expr * int -> vardecl
val mkINITVARDECL : Class_state.share_state * Objects.typ * string * expr * int -> vardecl

val mkCONSTDEF : vardecl * expr * int -> valdef
val mkVALDEF : vardecl * expr * int -> valdef
val mkSYSDEF : vardecl * int -> valdef

val mkQUEUE :
    Class_state.classname * Class_state.share_state * Class_state.queue_order * string *
    Class_state.visibility * criteria option * int -> state
val mkPROCESS :
    Class_state.classname * Class_state.share_state * string * Class_state.visibility * int -> state
val mkPREVPROCESS :
    Class_state.classname * string *
    Class_state.visibility * string * int -> state

val mkCORE: vardecl list * state list * steal_param option -> pcstate
val mkPSTATE: state list -> pcstate

val mkCSTATE : Class_state.corename * string * int -> cstate
val mkCSET : Class_state.corename * string * int -> cstate

val mkCRIT_ID   : key * crit_order * string * int -> criteria
val mkCRIT_EXPR : key * crit_order * expr  * int -> criteria

val mkTRACE     : int * trace_info list * int -> trace
val mkTRACEEXP  : string list * int -> classified_exp list

val mkEVENT     : event_name * vardecl * stmt * bool * int -> event_decl

val mkEVENT_NAME : string * int -> event_name

val mkFUNDEF : fcttype * Objects.typ * string * vardecl list * stmt * int -> fundef
val mkEXPORT_FUNDEF :
    Objects.typ * string * vardecl list * stmt * int -> fundef

val mkIF         : expr * stmt * stmt * int -> stmt
val mkFOR        : string * expr list * stmt * criteria option *int -> stmt
val mkFOR_ANY    : string * stmt * int -> stmt
val mkSWITCH     : expr * seq_case list * int -> stmt
val mkSEQ        : valdef list * stmt list * int -> stmt
val mkRETURN     : expr * int -> stmt
val mkRETURNVOID : int -> stmt
val mkSTEAL      : expr * int -> stmt
val mkMOVE       : expr * expr   * int -> stmt
val mkASSIGN     : expr * expr * int -> stmt
val mkDEFER      : int -> stmt
val mkPRIMSTMT   : string * expr list * int -> stmt
val mkPRIMSTMTEXP : expr * expr list * int -> stmt
val mkERROR      : string * int -> stmt
val mkBREAK      : int -> stmt
val mkCONTINUE   : int -> stmt

val mkSEQ_CASE : string list * stmt * int -> seq_case

val mkSELF       : int -> expr
val mkINT        : int * int -> expr
val mkVAR        : string * string list * int -> expr
val mkFIELD      : expr * string * string  list * int -> expr
val mkBOOL       : bool * int -> expr
val mkPARENT     : int -> expr
val mkUNARY      : uop * expr * int -> expr
val mkBINARY     : bop * expr * expr * int -> expr
val mkTERNARY    : expr * expr * expr * int -> expr
val mkINDR       : expr * int -> expr
val mkSELECT     : int -> expr
val mkFIRST      : expr * criteria option * int -> expr
val mkEMPTY      : string * int -> expr
val mkEMPTYCLASS : Class_state.classname * int -> expr
val mkPRIM       : string * expr list * int -> expr
val mkPRIMEXP    : expr * expr list * int -> expr
val mkSCHEDCHILD : expr * int -> expr
val mkVALID      : expr * int -> expr
val mkSRCONSCHED : int -> expr
val mkALIVE      : int -> expr
val mkIN         : expr * expr * criteria option * int -> expr
val mkINCLASS    : expr * Class_state.classname * criteria option * int -> expr

val mkCOMPOUND_ASSIGN : expr * bop * expr * int -> stmt

val mkDOMFOR : string * int option * domain list -> domain
val mkDOMDOM : string * expr * string * domgrp -> domain
									       
val mk_test_index : unit -> int

(* ------------------------- Useful syntax ------------------------ *)

val mkBLOCK : Objects.attribute -> stmt list -> stmt
val mkDBLOCK : Objects.attribute -> valdef list * stmt list -> stmt
val addBLOCK : Objects.attribute -> stmt -> stmt -> stmt
val postBLOCK : Objects.attribute -> stmt -> stmt list -> stmt
val add_decl_block : valdef -> stmt -> stmt

(* ----------------------- Accessor functions --------------------- *)

val get_exp_attr : expr -> Objects.attribute
val get_exp_id : expr -> Objects.identifier
val get_stm_attr : stmt -> Objects.attribute

(* ---------------------------- Comments -------------------------- *)

val add_comment : string -> expr -> expr

(* ---------------------------- Comments -------------------------- *)

val updty : Objects.typ -> expr -> expr

(* ----------------------- Printing functions --------------------- *)

val event_name2partial_mask : event_name -> string
val event_name2c : event_name -> string
val event_name_equal : event_name -> event_name -> bool
val bop2c : bop -> string
val equal_except_attributes : stmt -> stmt -> bool

(* --------------------- Special process names --------------------- *)

val is_src_or_tgt : expr -> Objects.identifier ->
  (Vobjects.vprocname * Class_state.state_info list) list *
    Class_state.state_info list
  -> Vobjects.vproc

(* ------------------ Naming nontrivial expressions ---------------- *)

val name_expr : expr -> Objects.attribute -> (valdef list * expr)
val pick_name_expr :
    string -> expr -> Objects.attribute -> (valdef list * expr)

(* ----------------------------- Timers ---------------------------- *)

val collect_timer_names :
    vardecl list -> valdef list -> Objects.identifier list

(* ------------------------- Free variables ------------------------ *)

val make_class_env : (Objects.identifier * Class_state.state_info) list ->
  (Objects.identifier * Class_state.state_info list) list

val collect_exp_all : Objects.identifier list (* env *) ->
      expr ->
      (Objects.identifier * Objects.attribute) list *
      (Objects.identifier * Objects.attribute) list *
      (Class_state.state_info * Objects.attribute) list

val collect_stm_all : Objects.identifier list (* env *) ->
      stmt ->
      (Objects.identifier * Objects.attribute) list *
      (Objects.identifier * Objects.attribute) list *
      (Class_state.state_info * Objects.attribute) list *
      (Objects.identifier * Objects.attribute) list *
      (Objects.identifier * Objects.attribute) list

(* both read and written variables, no fields *)
val collect_stm_vars : Objects.identifier list (* env *) ->
  stmt -> (Objects.identifier * Objects.attribute) list

(* both read and written variables, no fields *)
val collect_exp_vars : Objects.identifier list (* env *) ->
  expr -> (Objects.identifier * Objects.attribute) list

val params2ids : vardecl list -> Objects.identifier list
val valdefs2ids : valdef list -> Objects.identifier list
val no_dup_params : vardecl list -> string -> unit
val no_dup_valdefs : valdef list -> unit

(* ---------------------- Convert exp to string --------------------- *)

val exp2c : expr -> string
