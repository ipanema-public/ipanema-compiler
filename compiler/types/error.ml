exception Error of string

let debug = ref true
let output_name = ref "out" (* default output name *)
let error_ctr = ref 0

let init_error _ = error_ctr := 0

let update_error _ = error_ctr := (!error_ctr) + 1

let check_errors str =
  if not((!error_ctr) = 0)
  then
    (if (!error_ctr) = 1
    then
      failwith
	(Printf.sprintf "%i error detected during %s phase\n"
	   (!error_ctr) str)
    else
      failwith
	(Printf.sprintf "%i errors detected during %s phase\n"
	   (!error_ctr) str))

let any_errors _ = not((!error_ctr) = 0)

let internal_error _ =
  raise (Error(Printf.sprintf "The module list contains a scheduler"))
