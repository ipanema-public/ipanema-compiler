module B = Objects

(* -------------------- Miscellaneous operations -------------------- *)

exception LookupErrException

let rec lookup x = function
    [] -> raise LookupErrException
  | ((y,v)::r) -> if x = y then v else lookup x r

let rec lookup_id x = function
    [] -> raise LookupErrException
  | ((y,v)::r) -> if B.ideq(x,y) then v else lookup_id x r

(* ----------------------------- Classes ---------------------------- *)

let ready_st = ref ([] : B.identifier list)

type classname = RUNNING | READY | READYTICK | MIGRATING | BLOCKED | TERMINATED | NOWHERE | ANYWHERE
  
let all_classes in_event_types = [RUNNING; READY ; BLOCKED ; TERMINATED]

let all_classes_nowhere in_event_types = 
  NOWHERE :: (all_classes in_event_types)

(* for communicating with the user *)
let class2c = function
    RUNNING -> "IPANEMA_RUNNING"
  | READY -> "IPANEMA_READY"
  | READYTICK -> "IPANEMA_READY_TICK"
  | MIGRATING -> "IPANEMA_MIGRATING"
  | BLOCKED -> "IPANEMA_BLOCKED"
  | TERMINATED -> "IPANEMA_TERMINATED"
  | NOWHERE -> "IPANEMA_NOWHERE"
  | ANYWHERE -> "IPANEMA_ANYWHERE"

(* no error checking, for error messages *)
let safe_class2c = function
    RUNNING -> "RUNNING"
  | READY -> "READY"
  | READYTICK -> "READY_TICK"
  | MIGRATING -> "MIGRATING"
  | BLOCKED -> "BLOCKED"
  | TERMINATED -> "TERMINATED"
  | NOWHERE -> "NOWHERE"
  | ANYWHERE -> "ANYWHERE"

(* kernel classes, for printing the sched state *)
let classic_class2c = function
    RUNNING -> "RUNNING"
  | READY -> "READY"
  | READYTICK -> "READY_TICK"
  | MIGRATING -> "MIGRATING"
  | BLOCKED -> "BLOCKED"
  | TERMINATED -> "TERMINATED"
  | NOWHERE -> "NOWHERE"
  | ANYWHERE -> "ANYWHERE"

(* string for internal use, eg for making an identifier *)
let class2idc = function
    RUNNING -> "RUNNING"
  | READY -> "READY"
  | READYTICK -> "READY_TICK"
  | MIGRATING -> "MIGRATING"
  | BLOCKED -> "BLOCKED"
  | TERMINATED -> "TERMINATED"
  | NOWHERE -> "NOWHERE"
  | ANYWHERE -> "ANYWHERE"

let class2state = function
    RUNNING -> "RUNNING"
  | READY -> "READY"
  | READYTICK -> "READY_TICK"
  | MIGRATING -> "MIGRATING"
  | BLOCKED -> "BLOCKED"
  | _ -> raise (Error.Error("class does not correspond to a scheduler state"))

let id2class = function
    B.ID("RUNNING",_) -> RUNNING
  | B.ID("READY",_) -> READY
  | B.ID("READY_TICK",_) -> READYTICK
  | B.ID("MIGRATING", _) -> MIGRATING
  | B.ID("BLOCKED",_) -> BLOCKED
  | B.ID("TERMINATED",_) -> TERMINATED
  | B.ID("NOWHERE",_) -> NOWHERE
  | B.ID("ANYWHERE", _) -> ANYWHERE
  | id -> raise (Error.Error(Printf.sprintf "identifier %s not a class name"
			       (B.id2c id)))

(* RUNNING > READY > BLOCKED > TERMINATED > NOWHERE *)

(* A total order, this should just be
used for consistency between two lists, not as having some semantics *)
let less_than = function
    (NOWHERE,_) -> false
  | (_,NOWHERE) -> true
  | (ANYWHERE,_) -> true
  | (_,ANYWHERE) -> false
  | (RUNNING,_) -> false
  | (READY,RUNNING) -> true
  | (READY,_) -> false
  | (READYTICK,RUNNING) -> true
  | (READYTICK,_) -> false
  | (MIGRATING,RUNNING) -> true
  | (MIGRATING,_) -> false
  | (BLOCKED,RUNNING) -> true
  | (BLOCKED,READY) -> true
  | (BLOCKED,_) ->  false
  | (TERMINATED,TERMINATED) -> false
  | (TERMINATED,_) -> true

(* ----------------------------- Cores ----------------------------- *)
type corename = ACTIVE | SLEEPING | UNK

let safe_coreclass2c = function
    ACTIVE -> "IPANEMA_ACTIVE_CORE"
  | SLEEPING -> "IPANEMA_IDLE_CORE"
  | UNK -> "UNKNOWN"

let id2coreclass = function
    B.ID("ACTIVE",_) -> ACTIVE
  | B.ID("SLEEPING",_) -> SLEEPING
  | id -> raise (Error.Error(Printf.sprintf "identifier %s not a core class name"
			       (B.id2c id)))

let iscoreclass = function
    B.ID("ACTIVE",_) -> true
  | B.ID("SLEEPING",_) -> true
  | _ -> false

 (* ----------------------- State information ------------------------ *)

type state_info = STATE of pstate_info | CSTATE of cstate_info
		  | COMP of B.identifier * B.identifier
and pstate_info = B.identifier * classname * state_type * visibility
and cstate_info = B.identifier * corename
and state_type  = PROC | QUEUE of queue_order
and queue_order = FIFO | SELECT of sort_default | EMPTY
and sort_default = LIFODEFAULT | FIFODEFAULT | NODEFAULT
and visibility = PUBLIC | PRIVATE
and share_state = SHARED | UNSHARED

let cstate2class = function
    (CSTATE(_,cls)) -> cls
  | _ -> failwith "Core state expected"

let state2c = function
    (STATE(id,_,_,_)) | CSTATE(id, _) -> B.id2c id
let state2class = function
    (STATE(_,cls,_,_)) -> cls
		| _ -> failwith "Process state expected"

(* ---------------------- Process states and classes --------------------- *)

(* all states except NOWHERE *)
let state_env = (ref [] : (B.identifier * state_info) list ref)
let selected = (ref None : state_info option ref)
let selected_size = ref 0 (* number of key array elements *)

let concretize_class cls =
  let res =
    Aux.option_filter
      (function (_,(STATE(_,state_class,_,_) as x)) ->
	      if cls = state_class then Some x else None
       | (_,CSTATE(_,_)) -> None)

      (!state_env) in
  if res = []   then
    raise (Error.Error("cannot refer to an empty class "^(safe_class2c(cls))))
  else res
  

(* id should be either a state or a class *)
let concretize id =
  try [lookup_id id (!state_env)]
  with LookupErrException -> concretize_class (id2class id)

let all_states _ =
  Aux.option_filter
    (function
	(_,STATE(_,NOWHERE,_,_)) -> None
      | (_,CSTATE(_,_)) -> None
      | (_,x) -> Some x)
    (!state_env)

(* ----------------------- Core states and classes --------------------- *)

let cstate_env = (ref [] : (B.identifier * state_info) list ref)

let concretize_class4core cls =
  let res =
    Aux.option_filter
      (function (_,(STATE(_,_,_,_))) -> None
	      | (_,(CSTATE(_,coreclass) as x)) ->
		 if cls = coreclass then Some x else None)
      (!cstate_env) in
  if res = []   then
    raise (Error.Error("cannot refer to an empty class "^(safe_coreclass2c(cls))))
  else res


(* id should be either a state or a class *)
let concretize4core id =
  try [lookup_id id (!cstate_env)]
  with LookupErrException -> concretize_class4core (id2coreclass id)

let concretize4coreprocess id =
  try [lookup_id id (!cstate_env)]
  with LookupErrException ->
    try [lookup_id id (!state_env)]
    with LookupErrException ->
      if iscoreclass id then
	concretize_class4core (id2coreclass id)
      else
	concretize_class (id2class id)

(* ------------------------- States and classes --------------------- *)

let valid_sources dstid dst =
(* no more class automaton *)
  List.map (function (id,x) -> x) (!state_env)
(*
  List.fold_left
    (function prev ->
      function (src,dstlst) ->
	if Aux.member dst dstlst
	then (List.filter
		 (function STATE(id,_,_) -> not(id = dstid))
		 (concretize_class src))
	      @ prev
	else prev)
    []
    (B.sched_select class_automaton vclass_automaton)
*)
