(* $Id$ *)

let csharp = ref false
let hls = ref false
let blast = ref false
let high_res = ref true

let init_ioctls = ref ""

let type_return = ref ""

(* -------------------------- Linux version ------------------------- *)
type sched = PROCSCHED
type events = LINUX45
let config = ref LINUX45
let set_os = function
   "Linux4.5" -> config  := LINUX45  
  | _ -> raise (Error.Error "unknown OS")

(* --------------------------- Identifiers -------------------------- *)

(* string for printing, int for comparison *)
type identifier = ID of string * int

let symtab = ref (Hashtbl.create(500) : (string, identifier) Hashtbl.t)
let index = ref 0
let fresh_ctr = ref 0

let init_id () =
  symtab := Hashtbl.create(500);
  index := 0;
  fresh_ctr := 0

let mkId str =
  try Hashtbl.find (!symtab) str
  with Not_found ->
    (let n = !index in
    let entry = ID(str,n) in
    (index := n + 1;
     Hashtbl.add (!symtab) str entry;
     entry))

(*let ideq (ID(_,m),ID(_,n)) = m = n*)
let ideq (ID(_,m),ID(_,n)) = m = n
let strideq (m,ID(n,_)) = m = n
let idnameq(ID(m,_),ID(n,_)) = m  = n
let id2c (ID(str,n)) = str
let id2n (ID(_,n)) = n

let nopid = mkId("")

let rec fresh_id _ =
  let cur = !fresh_ctr in
  (fresh_ctr := cur + 1;
   let str = Printf.sprintf "_fresh_%d" cur in
   try (let _ = Hashtbl.find (!symtab) str in fresh_id())
   with Not_found -> mkId str) (* a bit inefficient... *)

let rec fresh_idxstr xstr =
  let cur = !fresh_ctr in
  (fresh_ctr := cur + 1;
   let str = Printf.sprintf "%s_%d" xstr cur in
   try (let _ = Hashtbl.find (!symtab) str in fresh_idxstr xstr)
   with Not_found -> str) (* a bit inefficient... *)

let fresh_idx xstr =
  mkId (fresh_idxstr xstr)

(* ------------------------------ Types ----------------------------- *)

(* SRCPROCESS is the type of the source variable.  Its fields can be
   accessed, but it cannot be use in next, moved, etc *)

type typ =
  ENUM of identifier | RANGE of identifier | SYSTEM of typ | CORE | DISTANCE |
  UINT | INT | BOOL | TIME | CYCLES | PROCESS | SRCPROCESS | SCHEDULER | VOID |
  PEVENT | CEVENT | INDR of typ | STRUCT of identifier | NULL | EMPTY | PORT |
  CLASS of identifier | GROUP | DOMAIN | CONST of typ | OPAQUE of string |
  SCHEDULER_INST | TIMER of timer_persistence | QUEUE of typ | SET of typ

and timer_persistence = NOT_PERSISTENT | PERSISTENT of identifier option

let rec type2c = function
    ENUM(id) -> if (!csharp) then (id2c id) else "enum "^(id2c id)
  | OPAQUE(cty) -> cty
  | RANGE(id) -> "range "^(id2c id)
  | CLASS(id) -> "class "^(id2c id)
  | SYSTEM(typ) -> "system "^(type2c typ)
  | CONST typ -> "const "^(type2c typ)
  | UINT -> "unsigned int"
  | INT -> "int"
  | BOOL -> "bool"
  | CORE -> "core"
  | DISTANCE -> "distance"
  | DOMAIN -> "domain"
  | GROUP -> "group"
  | TIME -> "time"
  | CYCLES -> "cycles"
  | PROCESS -> "process"
  | SRCPROCESS -> "process"
  | SCHEDULER -> "scheduler"
  | SCHEDULER_INST -> "scheduler_instance"
  | VOID -> "void"
  | PEVENT -> "process_event"
  | CEVENT -> "core_event"
  | INDR(t) -> (type2c t)^"*"
  | STRUCT(id) ->
     if (!csharp) then (id2c id)^" *" else "struct "^(id2c id)
  | NULL -> "null"
  | EMPTY -> "" (* hack for #defines *)
  | TIMER(_) -> "struct timer_list *"
  | PORT -> "port"
  | SET typ -> "set<"^ type2c typ ^">"
  | QUEUE typ -> "queue<"^ type2c typ ^">"

let rec type2ctype = function
SYSTEM(typ) -> type2c typ
  | CLASS(id) -> id2c id
  | CYCLES -> "int"
  | TIME ->
      if !high_res
      then "ktime_t"
      else
	if (!csharp)
	then "int"
	else "unsigned long"
  | INDR(t) -> (type2ctype t)^"*"
  | typ -> type2c typ

type locktyp =
    CORESTATE  | GLOBALVAR | SHAREDLOCAL | PRIVATELOCAL
type lockscope =
    EVENTLOCK  | USELOCK | NOLOCK
(* ---------------------------- Attributes -------------------------- *)

type location = MODULE_NAME of identifier | SCHEDULER_NAME of identifier

let module_name = ref (None : location ref option)
let current_comment = ref ""

type attribute = {gen : string ; modl : location ref option; ln : int; test_index : int;
                  ty : typ; com : string list; lck : (identifier*lockscope) list}
let mkAttr n =
  let newcom = match !current_comment with
      "" -> []
    | com -> [com]
  in
  current_comment := "";
  {gen = "" ; modl = !module_name; ln = n; test_index = 0; ty = VOID; com = newcom; lck = []}
let mkTestAttr (n,ind) =
  {gen = "" ; modl = !module_name; ln = n; test_index = ind; ty = VOID; com = []; lck = []}
let modl attr = attr.modl
let line attr = attr.ln
let test_index attr = attr.test_index
let ty attr = attr.ty
let com attr = if attr.com = [] then None else Some(String.concat "\n" attr.com)
let updty attr newty = {attr with ty = newty}
let updtst attr newtst = {attr with test_index = newtst}
let updlck attr newlck = {attr with lck = newlck}
let updgen attr newloc = {attr with gen = newloc}
let add_comment attr newcom = {attr with com = newcom::attr.com}

let dum loc = updgen (mkAttr (-1)) loc

let loc2c a =
  match a.modl with
    Some name ->
      (match !name with
	MODULE_NAME(m) ->
	  Printf.sprintf "module %s, line %d" (id2c m) a.ln
      | SCHEDULER_NAME(s) ->
	  Printf.sprintf "scheduler %s, line %d" (id2c s) a.ln)
  | None -> Printf.sprintf "line %d" a.ln

(* ----------------- Optimizing empty and in tests ------------------ *)

type testval = TRUE | FALSE | BOTH | UNK

let lubtestval = function
    (UNK,x) -> x
  | (x,UNK) -> x
  | (TRUE,TRUE) -> TRUE
  | (FALSE,FALSE) -> FALSE
  | _ -> BOTH

let testval2c = function
    TRUE -> "true"
  | FALSE -> "false"
  | BOTH -> "both"
  | UNK -> "unknown"

(* -------------------------- For events ---------------------------- *)

type intinfo = INTIBLE | UNINTIBLE

let mutate = ref false      (* generate mutations *)

type isquasi = QUASI | NOT_QUASI   (* detect quasi event types *)

(* -------------------------- Primitives ---------------------------- *)

let prim_names _ =
  (if !high_res
  then ["make_time";"make_cycle_time";"make_cycles";"time_to_seconds";
	 "time_to_nanoseconds";"time_to_jiffies";"time_to_subjiffies"]
  else []) @
  ["print_trace_info";"kfree";"select";"empty";
   "now";"start_absolute_timer";"start_relative_timer";"stop_timer";
   "time_to_ticks";"ticks_to_time"]

(* ------------------------ Free variables ------------------------- *)

let capture_check fvs decls error =
  List.iter
    (function (id,ln) ->
      if List.mem id decls
      then
	error
	  (Printf.sprintf
	     "identifier %s used at %s captured by a declaration in\nsome enclosing definition" (id2c id) (loc2c ln)))
    fvs
