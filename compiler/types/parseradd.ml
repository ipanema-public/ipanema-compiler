(*
** Type name hashtable
*)
module HashString =
struct
	type t = string
	let equal (s1 : t) (s2 : t) = s1 = s2
	let hash (s : t) = Hashtbl.hash s
end
module StringHashtbl = Hashtbl.Make(HashString)
let lexicon = StringHashtbl.create 211
let init_lexicon _ =
	StringHashtbl.clear lexicon;
	List.fold_left
	(fun tbl (key, token) -> StringHashtbl.add tbl key token ; tbl)
	lexicon
	[
		("int",           Objects.INT);
		("bool",          Objects.BOOL);
		("time",          Objects.TIME);
		("process",       Objects.PROCESS);
		("void",          Objects.VOID);
		("process_event", Objects.PEVENT);
		("core_event",    Objects.CEVENT)
	]

let add_enum_type name =
  StringHashtbl.add lexicon name (Objects.ENUM (Objects.mkId(name)))

let add_range_type name =
  StringHashtbl.add lexicon name (Objects.RANGE (Objects.mkId(name)))

let lookup_typename name =
  StringHashtbl.find lexicon name
