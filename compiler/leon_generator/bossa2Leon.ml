(* $Id$ *)

module B = Objects
module T = Types
module CS = Class_state
module G = Genaux

open Format

let policy_name = ref ""
let need_comma = ref false

let internal_error str =
  raise (Error.Error (Printf.sprintf "ipanema2leon: %s: internal error" str))

let start_block() =
  (* print_cut (); *)
  print_string "{"; print_cut();
  open_vbox 2; print_cut()

let end_block cut =
  close_box(); print_cut (); print_string "}";
  if cut then print_cut ()

let find_fun nm funs =
  try
    List.find (function Ast.FUNDEF(_,_,id,_,_,_,_) ->
			B.strideq ("ipanema_"^(!policy_name)^"_"^nm, id)) funs
  with
    Not_found -> internal_error ("function "^nm^" not found")


(* ssid is the identifier of the context (SharedState) containing all
   informations about cores, threads, and previous modifications. *)

let rec pp_fct ssid op arg =
  match arg with
    Ast.FIELD(exp,id,_,attr) ->
      let typ = B.ty attr in
      if typ != B.INT && typ != B.TIME
      then raise (Error.Error (Printf.sprintf "%s: sum/min/max wrong
						     arg type"
				 (B.loc2c attr)))
      else begin
	pp_exp ssid op;
	print_string "(";
	pp_exp ssid exp; print_string ".map("; G.pp_id ssid;
	print_string ".procs(_).";
	G.pp_id id; print_string "))";
	print_cut();
      end
  | _ -> failwith "sum: TODO"


and pp_ucexp ssid = function
    Ast.SUM att -> print_string "sum"
  | Ast.SELF att -> print_string "self"
  | Ast.MIN att -> print_string "min"
  | Ast.MAX att -> print_string "max"
  | Ast.COUNT att -> print_string "count"
  | Ast.DISTANCE att -> print_string "distance"
  | Ast.INT(n,attr) -> print_int n
  | Ast.VAR(id,_,attr) ->
     (match B.id2c id with
      | "CURRENT_0_STATE" -> print_string "0"
      | "READY_STATE" -> print_string "1"
      | "BLOCKED_STATE" -> print_string "2"
      | "TERMINATED_STATE" -> print_string "3"
      | "ACTIVE_CORES_STATE" -> print_string "0"
      | "__state_cur" -> print_string (B.id2c ssid) (* internal *)
      | _ -> G.pp_id id)

  (* Cores and threads have BigInt type in Leon.  The data is stored in the
     procs()/states() functions of the context. *)
  | Ast.FIELD(exp,fld,_,attr) ->
     (match (B.ty (Ast.get_exp_attr exp)) with
	B.PROCESS
      | B.SRCPROCESS-> G.pp_id ssid; print_string ".procs(";
		       pp_ucexp ssid exp;
		       print_string ")"
      | B.CORE -> G.pp_id ssid; print_string ".states(";
		  pp_ucexp ssid exp;
		  print_string ")"
      | _ -> pp_ucexp ssid exp);
     print_string "."; G.pp_id fld

  | Ast.CAST(typ, exp,attr) -> pp_ucexp ssid exp

  | Ast.LCORE(obj, exp, core, attr) ->
     (open_vbox 0; (* FIXME core is unused *)
      print_string (obj^".");
      pp_exp ssid exp;
      close_box ())

  | Ast.BOOL(true,attr) -> print_string "true"

  | Ast.BOOL(false,attr) -> print_string "false"

  | Ast.UNARY(Ast.NOT,exp,attr) ->
     (print_string "!"; pp_exp ssid exp)

  | Ast.UNARY(Ast.COMPLEMENT,exp,attr) ->
     (print_string "~"; pp_exp ssid exp)

  | Ast.BINARY(bop,exp1,exp2,attr) ->
     if bop = Ast.PLUS && (B.ty (Ast.get_exp_attr exp1)) = B.PROCESS
     then  (* add process into a runqueue *)
       (print_string "Cons(";
	pp_exp ssid (Ast.CAST(B.INT, exp1, B.mkAttr 0));
	G.pp_comma();
	pp_exp ssid exp2;
	print_string ")";)
     else begin
	 (match bop with
	    Ast.OR | Ast.AND -> open_box 0
	    | _ -> ());
	 print_string "(";
	 pp_exp ssid exp1;
	 pp_bop bop;
	 pp_exp ssid exp2;
	 print_string ")";
	 (match bop with
	    Ast.OR | Ast.AND -> close_box()
	    | _ -> ())
       end
  | Ast.PBINARY(bop,exp1,states1,exp2,states2,attr) ->
     raise (Error.Error "internal error: unexpected pbinary")

  | Ast.TERNARY(exp1,exp2,exp3,attr) ->
     (print_string "(if(";pp_exp ssid exp1; print_string ") ";
      pp_exp ssid exp2; print_string " else ";
      pp_exp ssid exp3; print_string ")")

  | Ast.CONDOP(exp1, exp2, exp3, attr) ->
     begin
       print_string "(if (";
       pp_exp ssid exp1;
       print_string ") { ";
       pp_exp ssid exp2;
       print_string "} else { ";
       pp_exp ssid exp3;
       print_string "})"
     end

  | Ast.INDR(exp,attr) ->
     pp_exp ssid exp;

  | Ast.SELECT(_,attr) ->
     print_string "select()"

  | Ast.FIRST(exp,crit,_,attr) ->
     pp_exp ssid (Ast.mkFIELD(exp, "head", [], 0));

  | Ast.EMPTY(id,_,tv,_,attr) ->
     (print_string "empty("; G.pp_id id; print_string ")")

  | Ast.VALID(exp,sti,attr) ->
     pp_exp ssid exp;
     let ty = B.ty (Ast.get_exp_attr exp) in
     if (ty = B.PROCESS || ty = B.CORE)
     then print_string ".isDefined";

  | Ast.PRIM((Ast.VAR(B.ID("count_processes",_),_,_)),args,attr)
  | Ast.PRIM((Ast.COUNT _),args,attr) ->
     if List.length args != 1 then
       raise (Error.Error (Printf.sprintf "%s: count wrong arguments"
					  (B.loc2c attr)))
     else begin
	 match List.hd args with
	 | Ast.VAR(B.ID("self",_),_,_)
	 | Ast.VAR(B.ID("current",_),_,_)
	 | Ast.FIELD(_,B.ID("self",_),_,_)
	 | Ast.FIELD(_,B.ID("current",_),_,_) ->
	    print_string "option_count(";
	    pp_exp ssid (List.hd args);
	    print_string ")"
	 | _ ->
	    print_string "(";
	    pp_exp ssid (List.hd args);
	    print_string ").size"
       end

  (* Lazy evaluation: call to an ipa_{core,task}_field function. *)
  | Ast.PRIM((Ast.VAR(B.ID(funname,_),_,_)),args,attr)
       when (try (Scanf.sscanf funname "ipa_%s@_%s" (fun s _-> s = "core" ||
								 s = "task"))
	     with Scanf.Scan_failure(_) -> false)
    ->
     let varname = Scanf.sscanf funname "ipa_%s@_%s" (fun _ s -> s) in
     let exp_ty = B.ty (Ast.get_exp_attr (List.hd args)) in
     if (List.length args != 1) || not (exp_ty = B.CORE || exp_ty = B.PROCESS)
     then raise (Error.Error (Printf.sprintf "%s: lazy wrong arguments"
					     (B.loc2c attr)));

     let exp = Ast.mkFIELD(List.hd args,
			   (varname ^"("^(B.id2c ssid)^")"), [], 0);
     in pp_exp ssid exp

  | Ast.PRIM((Ast.VAR(B.ID("ticks_to_time",_),_,_)),args,attr)
  | Ast.PRIM((Ast.VAR(B.ID("time_to_ticks",_),_,_)),args,attr) ->
     if List.length args != 1
     then raise (Error.Error (Printf.sprintf "%s: ticks/time wrong arguments"
					     (B.loc2c attr)))
     else pp_exp ssid (List.hd args);

  | Ast.PRIM(Ast.SUM(_) as op,args,attr)
  | Ast.PRIM(Ast.MIN(_) as op,args,attr)
  | Ast.PRIM(Ast.MAX(_) as op,args,attr) ->
     List.iter (pp_fct ssid op) args

  | Ast.PRIM(f,args,attr) ->
     let print_app _ =
       pp_exp ssid f; print_string "(";
       open_box 0;
       G.print_between G.pp_comma (function exp -> pp_exp ssid exp) args;
       close_box(); print_string ")" in
     print_app()

  | _ -> ()

and pp_exp ssid exp =
  pp_ucexp ssid exp

and pp_bop = function
  | Ast.AND -> print_space(); print_string "&&"; print_space()
  | Ast.OR -> print_space(); print_string "||"; print_space()
  | op -> print_string (" "^(Ast.bop2c op)^" ")


and pp_def ssid = function
    Ast.VALDEF(decl,exp,isconst,attr) ->
    (open_box 0;
     if isconst=Ast.CONST then ((print_string "val"; print_space());
				pp_decl false decl; print_string " = ";
				pp_exp ssid exp; print_string ";")
     else
       (pp_decl true decl;
	print_string " = "; pp_exp ssid exp;
	print_string ";");
     close_box();
     print_cut())
  | Ast.UNINITDEF(decl,attr) ->
     (pp_decl true decl;
      if (!need_comma) then print_string ";"; print_cut())
  | Ast.SYSDEF(decl,isconst,attr) ->
     (pp_decl true decl;
      if (!need_comma) then print_string ";"; print_cut())
  | Ast.DUMMYDEF(id,exp,attr) ->
     (print_string "???"; print_space(); G.pp_id(id);
      print_string " = "; pp_exp ssid exp; print_string ";";
      print_cut())

and pp_defs ssid global defs =
  List.iter (pp_def ssid) defs;
  G.nl_ifnonempty(defs)


and append_stmt st elt =
  match st with
  | Ast.SEQ(decls,stmts,attr) -> Ast.SEQ(decls,stmts@[elt],attr)
  | _ -> Ast.mkSEQ ([], [st;elt], 0)


(* Returns an updated ssid.  As core/thread fields are immutable, we need to
   generate a complete SharedState when one value is updated.  It is done by
   copying the original functions, and set the new return value for the given
   input index.  This is an example:
   c.load = 1
     becomes
   new SharedState(..., (idx: BigInt) => if (idx == c)
                                         then new Core (c with load = 1)
                                         else old_ssid.states(idx),
                   ...)
*)
and update ssid l =
  let l_proc =
    List.filter (fun (exp,_) -> let t = B.ty (Ast.get_exp_attr exp)
				in t = B.PROCESS || t = B.SRCPROCESS) l
  and l_core =
    List.filter (fun (exp,_) -> B.ty (Ast.get_exp_attr exp) = B.CORE) l
  in
  let do_ss = function
      Ast.VARDECL(_,n,_,_,_,_,_) ->
      let gen_new (fieldname, consname, fields, l) =
	let newidx =  B.fresh_id() in
	print_string "("; G.pp_id newidx; print_string ": BigInt) => ";
	List.iter
	  (function (exp, fldlist) ->
		    print_string  "if ("; G.pp_id newidx; print_string " == ";
		    pp_exp ssid (Ast.CAST(B.INT, exp, B.mkAttr 0));
		    print_string ") {"; print_cut();
		    print_string "new "; print_string consname;
		    print_string "("; open_box 0;
		    let print_fields = function
			Ast.VARDECL(_,n,_,_,_,_,_) ->
			try let (_,expr) = List.find (function (id,_) ->
							       B.idnameq(id,n))
						     fldlist in
			    pp_exp ssid expr
			with Not_found ->
			  pp_exp ssid (Ast.mkFIELD(exp, B.id2c n, [], 0));
		    in
		    G.print_between G.pp_comma print_fields fields;
		    print_string ");"; close_box();
		    print_cut(); print_string "} else ")
	  l;
	G.pp_id ssid; print_string "."; print_string fieldname;
	print_string "("; G.pp_id newidx; print_string ")";
	print_cut()
      in
      if (B.id2c n) = "procs" && List.length l_proc > 0
      then gen_new ("procs", "Process", !procfields, l_proc)
      else if (B.id2c n) = "states" && List.length l_core > 0
      then gen_new ("states", "Core", !corefields, l_core)
      else (G.pp_id ssid; print_string "."; G.pp_id n)
  in
  let newssid = B.fresh_id() in
  print_string "val "; G.pp_id newssid;
  print_string " = new SharedState("; open_box 0;
  G.print_between G.pp_comma do_ss !ssfields;
  print_string ");"; close_box(); print_cut();
  newssid


and pp_stmt ssid = function
  | Ast.RETURN(_,_) as stmt -> pp_stmt_r ssid stmt
  | stmt ->
     let instr = B.id2c (B.fresh_idx "instr") in
     print_string ("def "^instr^"(self: BigInt, ");
     G.pp_id ssid;
     print_string ": SharedState): SharedState = ";
     start_block();
     (* let newssid = pp_stmt_r (B.mkId("__state")) stmt in *)
     let newssid = pp_stmt_r ssid stmt in
     G.pp_id newssid; print_string ";";
     end_block true;

     (* ensuring here: ssid => newssid*)

     let newssid = (B.fresh_id()) in
     let snewssid = B.id2c newssid in
     print_string ("val "^snewssid^" = "^instr^"(self, ");
     G.pp_id ssid; print_string ");";
     print_cut();
     newssid


(* Statements may modify some core/threads fields, so return the SharedState
   updated accordingly. *)
and pp_stmt_r ssid = function

  | Ast.IF(exp,stmt1,stmt2,attr) ->

     let newssid = B.fresh_id() in
     print_string "val "; G.pp_id newssid; print_string ": SharedState =";
     print_cut();

     ((match exp with
	 Ast.BINARY(Ast.BITAND,_,_,_) ->
	 (print_string "if (("; pp_exp ssid exp; print_string ")!=0")
       | _ -> (print_string "if ("; pp_exp ssid exp;));
      print_string ") "; start_block();

      let ret = Ast.mkRETURN (Ast.mkVAR("__state_cur",[],0),0) in (* internal *)
      ignore (pp_seq ssid (append_stmt stmt1 ret));

     end_block false; print_string " else ";
      match stmt2 with
      | Ast.SEQ([],[],_) -> ignore (pp_seq ssid ret)
      | _ -> (start_block();
	      ignore (pp_seq ssid (append_stmt stmt2 ret));
	      end_block false));
     print_cut();
     newssid

  | (Ast.SEQ(decls,stmts,attr) as x) -> pp_seq ssid x
  | Ast.RETURN(None,attr) -> ssid
  | Ast.RETURN(Some(exp),attr) ->
     open_box 0; pp_exp ssid exp; print_string ";"; close_box();
     print_cut();
     ssid

  | Ast.ASSIGN(expl,expr,sorted_fld,attr) ->
     open_hbox ();
     let newssid =
       let do_p _ =
	 pp_exp ssid expl; print_space(); print_string "="; print_space();
	 pp_exp ssid expr; print_string ";";
	 ssid;
       in
       (match expl with
	| Ast.FIELD(exp,id, _, attr) ->
	   begin
	     match B.ty (Ast.get_exp_attr exp) with
	     | B.PROCESS | B.SRCPROCESS
	     | B.CORE -> update ssid [(exp,[(id,expr)])]
	     | _ -> do_p();
	   end
	| _ -> do_p())
     in
     close_box(); print_cut();
     newssid

  | Ast.PRIMSTMT((Ast.VAR(B.ID("ipa_change_queue",_),_,_)),args,attr) ->
     if List.length args != 4 then
       raise (Error.Error (Printf.sprintf "%s: ipa_change_queue wrong arguments"
					  (B.loc2c attr)))
     else
       let attr = B.updty attr B.CORE in
       let idrq = Ast.get_exp_id (List.nth args 1) in
       let stealer = Ast.VAR(B.mkId "self", [], attr) in
       (* let stealee = Ast.VAR(B.mkId "stealee", [], attr) in *)
       let newssid =
	 update ssid [(stealer,[idrq,
				Ast.mkBINARY(Ast.PLUS,
					     List.nth args 0,
					     Ast.FIELD(stealer, idrq,
						       [], B.mkAttr 0),
					     0)]);
	      (* (stealee,[idrq, *)
	      (* 		Ast.mkBINARY(Ast.MINUS, *)
	      (* 			     Ast.FIELD(stealee, idrq, *)
	      (* 				       [], B.mkAttr 0), *)
	      (* 		     (Ast.CAST(B.INT, List.nth args 0, *)
	      (* 				       B.mkAttr 0)), *)
	      (* 			     0)]); *)
	      (List.nth args 0, [(B.mkId "state", List.nth args 2)])];
       in
       print_cut();
       newssid

  (* process is set to current *)
  | Ast.PRIMSTMT((Ast.VAR(B.ID("ipa_change_proc",_),_,_)),args,attr) ->
     if List.length args != 3 then
       raise (Error.Error (Printf.sprintf "%s: ipa_change_proc wrong arguments"
  					  (B.loc2c attr)))
     else
       let attr = B.updty attr B.CORE in
       let dst = Ast.VAR(B.mkId "self", [], attr) in
       let idc = Ast.get_exp_id (List.nth args 1) in
       let newssid =
  	 update ssid [(dst,[idc, Ast.mkPRIM("Some",
  					    [List.nth args 0],
					    0)]);
  		      (List.nth args 0, [(B.mkId "state", List.nth args 2)])];
       in
       print_cut();
       newssid

  (* | Ast.PRIMSTMT((Ast.FIELD(Ast.PARENT(_),id,_,fn_attr)),args,attr) -> *)
  (*    G.pp_id id; print_string "("; open_box 0; *)
  (*    G.print_between G.pp_comma *)
  (* 		     (function exp -> pp_exp ssid exp) *)
  (* 		     args; *)
  (*    close_box(); print_string ");"; *)
  (*    ssid *)

  (* | Ast.PRIMSTMT(f,args,attr) -> *)
  (*    open_hbox (); *)
  (*    pp_exp ssid f; print_string "("; *)
  (*    open_box 0; *)
  (*    G.print_between G.pp_comma *)
  (* 		     (function exp -> pp_exp ssid exp) *)
  (* 		     args; *)
  (*    close_box(); *)
  (*    print_string ");"; *)
  (*    close_box(); *)
  (*    ssid *)

  | Ast.MOVE(exp_proc,state,src,_,dst,_,_,attr) ->
     (match dst with
     	| Some(CS.STATE(idq,_,CS.QUEUE(_),_)) -> (

	  (* convert one name into another *)
	  (* from genaux.ml, FIXME *)
	  let to_stat state_exp =
	    match state_exp with
	      Ast.VAR(id, _, _) ->
	      Printf.sprintf "%s_STATE" (String.uppercase (B.id2c id))
	    | Ast.FIELD(_,id, _, _) ->
	       Printf.sprintf "%s_STATE" (String.uppercase (B.id2c id))
	    | _ -> failwith "to_stat: State expected"
	  in

	  let attr = B.updty attr B.CORE in
	  let (dst_core, dst_rq) =
	    match state with
	      Ast.VAR(id,_,_) -> (Ast.VAR(B.mkId "self", [], attr), id)
	    | Ast.FIELD(expc,rq,_,_) -> (expc, rq)
	    | _ -> raise (Error.Error "destination state not supported")
	  in
	  let (src_core, src_rq) =
	    match src with
	      (* we always move tasks from ready queue *)
	      Some(CS.CSTATE(id,_)) -> (Ast.VAR(id, [], attr), B.mkId "ready")
	    | _ -> raise (Error.Error "source not known")
	  in
       	  update ssid [(dst_core,[dst_rq,
       				  Ast.mkBINARY(Ast.PLUS, exp_proc,
       					       Ast.FIELD(dst_core, dst_rq, [],
							 B.mkAttr 0),
       					       0)]);
       		       (src_core,[src_rq,
       			 	  Ast.mkBINARY(Ast.MINUS,
       			 		       Ast.FIELD(src_core, src_rq,
       			 				 [], B.mkAttr 0),
       			 		       (Ast.CAST(B.INT, exp_proc,
       			 				 B.mkAttr 0)),
       			 		       0)]);
       		       (exp_proc, [(B.mkId "state",
				    Ast.mkVAR (to_stat state,[],0))])]
	)

	| Some(CS.CSTATE(_,CS.UNK))
	| None -> raise (Error.Error "destination not known")
	| _ -> raise (Error.Error "destination not supported")
     );

  | _ -> print_string "Warning here: unsupported statement!"; ssid


and pp_top_seq ssid s =
  let (decls,stmts) =
    match s with
      Ast.SEQ(d,s,_) ->
      (match List.hd s with
	 Ast.SEQ(d2,s2,attr) -> (d@d2,s2@(List.tl s))
       | _ -> (d, s))
    | _ -> ([],[s]) in
  let stmts = List.rev(flatten stmts) in
  (start_block();
   (match decls with
      [] -> ()
    | x::xs -> pp_defs ssid false decls);
   ignore (List.fold_left pp_stmt ssid stmts);
   end_block false)

and pp_seq ssid s =
  let (decls,stmts) =
    match s with
      Ast.SEQ(d,s,_) ->
      (if s <> [] || d <> [] then
	 match List.hd s with
	   Ast.SEQ(d2,s2,attr) -> (d@d2,s2@(List.tl s))
	 | _ -> (d, s)
       else ([],[]))
    | _ -> ([],[s]) in
  match (decls, stmts) with
    ([], [s]) ->
    print_cut (); (* print_string "\t"; *)
    let newssid = pp_stmt ssid s in
    print_cut (); newssid
  | _ ->
     let stmts = List.rev(flatten stmts) in
     (match decls with
	[] -> ()
      | x::xs -> pp_defs ssid false decls);
     (* ignore (List.fold_left pp_stmt ssid (stmts@[ret])); *)
     let newssid = List.fold_left pp_stmt ssid stmts in
     (* G.end_block false; G.pp_semic();  *)
     newssid

and flatten stmts =
  List.fold_left
    (function prev ->
	      function
		Ast.SEQ([],s,_) -> (flatten s)@prev
	      | s -> s::prev)
    [] stmts

and pp_decl isvar (Ast.VARDECL(ty,id,imported,lz,_,de,attr)) =
  need_comma := false;
  if not (B.id2c id = "current") then
    begin
      if isvar then print_string "val ";
      G.pp_struct_id(id);
      print_string ": ";
      T.pp_type ty;
      need_comma := true
    end

(* Kernel module specific variables, perhaps no longer useful. *)
and procfields = ref [Ast.mkVARDECL(CS.UNSHARED, B.INT, "state", 0);
		      Ast.mkVARDECL(CS.UNSHARED, B.QUEUE(B.PROCESS), "rq", 0);
		      Ast.mkVARDECL(CS.UNSHARED, B.PROCESS, "task", 0)]


(* Set up thread abstraction by populating procfields and handling lazy
   variables as class methods. *)
and pp_procdecls procdecls =
  let (lzy_procv,procv) =
    List.partition (function Ast.VARDECL(_,_,_,lzy,_,_,_) -> lzy) procdecls
  in
  procfields := !procfields@procv;
  open_box 0;
  print_string "case class Process("; open_box 0;
  G.print_between G.pp_comma (pp_decl true) !procfields;
  close_box(); print_string ")";
  close_box ();
  (match lzy_procv with
     [] -> print_string ";";
   | _ -> (
     print_string " "; start_block();
     List.iter (function Ast.VARDECL(ty,id,im,lz,_,de,attr) ->
			 if de = None then
			   raise (Error.Error "lazy variable
					       without definition");
			 let Some(lzy_exp) = de in
			 let ssid = B.mkId("__state") in
			 print_string "def "; G.pp_id id;
			 print_string "(__state: SharedState): ";
			 T.pp_type ty; print_string " = ";
			 start_block();
			 pp_exp ssid lzy_exp;
			 end_block true;
	       ) lzy_procv;
     end_block true;));
  print_cut ();


and pp_functions functions =
  open_vbox 0;
  G.print_between
    print_cut
    (function Ast.FUNDEF(_,ret,nm,params,stmt,_,attr) ->
	      let ssdecl = Ast.mkVARDECL(CS.UNSHARED, B.CLASS(B.mkId("SharedState")),
					 "__state", B.line attr) in
	      let params =  ssdecl
			    ::Ast.mkVARDECL(CS.UNSHARED, B.CORE, "self", B.line attr)
			    ::params in
	      open_vbox 0;
	      print_string "def ";
	      G.pp_id (nm);
	      (match params with
		 [] -> print_string "()";
	       | _ -> let rparams = match params with
			  Ast.VARDECL(_,B.ID("policy",_),_,_,_,_,_)::tl -> tl
			| _ -> params
		      in
		      print_string "("; open_box 0;
		      G.print_between G.pp_comma (pp_decl false) rparams;
		      close_box(); print_string ")");
	      print_string ": ";
	      (match ret with B.VOID -> print_string "SharedState"
			    | _ -> T.pp_type ret);
	      print_string " = ";

	      let Ast.VARDECL(_,ssid,_,_,_,_,_) = ssdecl in

	      let stmt = match ret with
		| B.VOID ->
		   let sret = Ast.mkRETURN(Ast.mkVAR("__state_cur",[],0),0)
		   in append_stmt stmt sret
		| _ -> stmt
	      in pp_top_seq ssid stmt;

		 print_cut ();
		 close_box ())
    functions;
  close_box ();
  print_cut ()

(* core/context fields to be cloned when updated *)
and corefields = ref [];
and ssfields = ref [];

and pp_declstate = function
    Ast.QUEUE(_,_,_,state,_,_,att)
  | Ast.PROCESS (_,_,state,_,_,att) ->
     let typ = if ((B.id2c state) = "current_0")
	       then "Option[BigInt]"
	       else "List[BigInt]"
     (* else T.pp_type_cs (B.ty att)  -> TODO typechecker*)
     in
     print_string ("val "^(B.id2c state)^": "^typ);
     corefields := !corefields@[Ast.VARDECL(B.ty att, state, false,false,CS.UNSHARED,
					    None, att)]

and pp_coredecls states_ =
  let (states, corev_, steal_) = match states_ with
      Ast.CORE(cv,st, steal) -> (st, Some cv, steal)
    | Ast.PSTATE(st) -> (st, None, None)
  in
  print_string "case class Core(";
  open_box 0;
  G.print_between G.pp_comma pp_declstate states;

  (match corev_ with
     None -> print_string ");" (* No core variables *)
   | Some corev ->
      let (lzy_corev,corev) =
	List.partition (function Ast.VARDECL(_,_,_,lzy,_,_,_) -> lzy) corev
      in
      (match corev with
	 [] -> ()
       | _ ->
	  print_string ", ";
	  corefields := !corefields@corev;
	  G.print_between G.pp_comma (pp_decl true) corev
      );
      print_string ")"; close_box ();
      (match lzy_corev with
	 [] -> print_string ";";
       | _ -> (
	 print_string " "; start_block();
	 List.iter (function Ast.VARDECL(ty,id,im,lz,_,de,attr) ->
			     if de = None then
			       raise (Error.Error "lazy variable
						   without definition");
			     let Some(lzy_exp) = de in
			     let ssid = B.mkId("__state") in
			     print_string "def "; G.pp_id id;
			     print_string "(__state: SharedState): ";
			     T.pp_type ty; print_string " = ";
			     start_block();
			     pp_exp ssid lzy_exp;
			     end_block true;
		   ) lzy_corev;
	 end_block true;))
  );
  print_cut ();

  print_string "case class SharedState("; open_box 0;
  print_string "val progress:BigInt, val cores:List[BigInt],";
  print_cut ();  print_string "val states:BigInt => Core, ";
  print_string "val procs:BigInt => Process";
  print_string ");"; close_box (); print_cut ();

  ssfields := [Ast.mkVARDECL(CS.UNSHARED, B.INT, "progress", 0);
	       Ast.mkVARDECL(CS.UNSHARED, B.VOID, "cores", 0);
	       Ast.mkVARDECL(CS.UNSHARED, B.VOID, "states", 0);
	       Ast.mkVARDECL(CS.UNSHARED, B.VOID, "procs", 0)];


  match steal_ with
    None -> () (* No steal block *)

  | Some (dom, dstg, dst, steal) ->
     let get_steal_blk steal =
       match steal with
	 Ast.FLAT_STEAL_THREAD st -> st
       | Ast.ITER_STEAL_THREAD (st, until, post) -> st
     in
     let (filter, select, migrcond) = match steal with
	 Ast.STEAL_THREAD s -> get_steal_blk s
       | Ast.ITER_STEAL_GROUP (sg, st, post, postgrp) -> get_steal_blk st
       | Ast.FLAT_STEAL_GROUP (sg, st, postgrp) -> get_steal_blk st
     in
     let (self, c, stmt) = filter in
     pp_functions [Ast.mkFUNDEF(Ast.DFT, B.BOOL, "can_steal_core", [c],
				stmt, 0)];

     let (c, exp1, select_exp) = select in
     (* saved arg : (state_info: Core)  *)
     print_string "def select_core(__state: SharedState, ";
     print_string "self: BigInt, ";
     let ssid = B.mkId "__state" in
     pp_decl false c;
     print_string "):"; print_space();
     print_string "BigInt = ";
     start_block();

     pp_stmt ssid select_exp;
     end_block true;

     let (c1, c2, c3, c4, stmt1, exp2, update) = migrcond in

     (* add information about c1 (the source core) in the MOVE(c2,...)
	statements *)
     let rec move_src = function
       | Ast.IF(exp,s1,s2,attr) -> Ast.IF(exp,move_src s1,move_src s2,attr)
       | Ast.SEQ(decls,stmts,attr) ->
	  Ast.SEQ(decls,List.map move_src stmts,attr)
       | Ast.FOR(_,_,_,_,_,_,_) -> raise (Error.Error "move_src: bad statement")
       | Ast.FOR_WRAPPER(_,_,_) -> raise (Error.Error "move_src: bad statement")
       | Ast.SWITCH(_,_,_,_) -> raise (Error.Error "move_src: bad statement")
       | Ast.MOVE(exp,state,x,y,z,a,b,attr) ->
	  (match (exp,c4) with
	   | (Ast.VAR(id1,_,_), Ast.VARDECL(_,id2,_,_,_,_,_))
		when (B.ideq (id1,id2)) ->
	      let Ast.VARDECL(_,idc1,_,_,_,_,_) = c1 in
	      Ast.MOVE(exp,state,
		       Some(CS.CSTATE(idc1,CS.ACTIVE)),
		       y,z,a,b,attr)
	   | _ ->  Ast.MOVE(exp,state,x,y,z,a,b,attr))
       | stmt -> stmt
     in
     let params =
       match c3 with
	 None -> [c1;c2;c4]
       | Some c3 -> [c1;c2;c3;c4]
     in
     pp_functions [Ast.mkFUNDEF(Ast.DFT, B.VOID, "steal_thread",
				params, move_src stmt1, 0)];
     (* exp2: stop condition, unused as long as we consider the theft of one
	task at a time. We can ensure ~exp2 in the steal_thread block. *)

     print_cut()

let includefiles _ =
  "import scala.Any\n"^
    "import leon.collection._\n"^
      "import leon.lang._\n"^
	"import leon.annotation._\n"^
	  "import leon.proof._\n"^
	    "import leon.instrumentation._\n"^
	      "import leon.invariant._\n"^
		"import leon.util.Random\n"


(* -------------------------- entry point --------------------------- *)

let bcpp dirname
	 ((Ast.SCHEDULER(nm,cstdefs,enums, dom, grp, procdecls,
			 fundecls,valdefs,domains,states_,cstates,
			 criteria,trace,handlers,chandlers,ifunctions,functions,
			 attr))) : unit =

  let file_ext = ".scala" in
  let ch = open_out (dirname^"/"^nm^file_ext) in
  let ssid = B.mkId "UNKNOWN!" in
  set_formatter_out_channel ch;
  set_margin 80;
  policy_name := nm;
  G.print_nl_string (includefiles ());
  G.print_nl_string ("object Scheduler ");
  start_block();
  print_string "def option_count(e: Option[BigInt]): BigInt = \
		{ if (e.isDefined) 1 else 0 }";
  print_cut();
  print_string "def min(l: List[BigInt]):BigInt= {
      require(!l.isEmpty)
      l match {
	case Cons(head, Nil()) => head
	case Cons(head, tail) => val m = min(tail); if (m < head) m else head
      }
  }";
  print_cut();
  print_string "def max(l: List[BigInt]):BigInt= {
      require(!l.isEmpty)
      l match {
	case Cons(head, Nil()) => head
	case Cons(head, tail) => val m = max(tail); if (m > head) m else head
      }
  }";
  print_cut();
  print_string "def sum(l: List[BigInt]):BigInt= {
      require(!l.isEmpty)
      l match {
	case Nil() => BigInt(0)
	case Cons(head, tail) => head + sum(tail)
      }
  }";
  print_cut();
  print_cut();

  pp_defs ssid true cstdefs;
  pp_defs ssid true valdefs;
  pp_procdecls procdecls;
  pp_coredecls states_;

  pp_functions functions;
  pp_functions ifunctions;

  end_block false;
  print_flush ();
  close_out ch;
  set_formatter_out_channel stdout;
