module G = Genaux
open Objects
open Format

(* --------------------------- Types for Scala -------------------------- *)

let rec type2scala = function
    ENUM(id) -> "enum "^(id2c id)
  | RANGE(id) -> "range "^(id2c id)
  | CLASS(id) -> "class "^(id2c id)
  | SYSTEM(typ) -> "system "^(type2scala typ)
  | CONST typ -> "const "^(type2scala typ)
  | INT -> "BigInt"
  | OPAQUE "uint32_t" -> "BigInt"
  | BOOL -> "Boolean"
  | CORE -> "BigInt" (* !! *)
  | DISTANCE -> "distance"
  | DOMAIN -> "Domain"
  | GROUP -> "Group"
  | TIME -> "BigInt"
  | CYCLES -> "BigInt"
  | PROCESS -> "BigInt"
  | SRCPROCESS -> "BigInt"
  | SCHEDULER -> "Scheduler"
  | SCHEDULER_INST -> "scheduler_instance"
  | VOID -> "Unit"
  | PEVENT -> "process_event"
  | CEVENT -> "core_event"
  | INDR(t) -> (type2c t)^"*"
  | STRUCT(id) -> id2c id
  | NULL -> "null"
  | EMPTY -> "" (* hack for #defines *)
  | TIMER(_) -> "struct timer_list *"
  | PORT -> "Port"
  | SET typ -> "List["^ type2scala typ ^"]"
  | QUEUE typ -> "List["^ type2scala typ ^"]"

let rec type2scalatype = function
SYSTEM(typ) -> type2scala typ
  | CLASS(id) -> id2c id
  | CYCLES -> "BigInt"
  | TIME -> "BigInt"
  | INDR(t) -> type2scalatype t
  | typ -> type2scala typ

let pp_type_cs = function
    PROCESS -> "BigInt"
  | SRCPROCESS -> "BigInt"
  | SCHEDULER_INST -> "Scheduler"
  | RANGE(id) -> G.pp_id_cs id
  | QUEUE(PROCESS) ->  "List[BigInt]"
  | SET(CORE) -> "List[BigInt]"
  | SET(DOMAIN) -> "List[List[List[BigInt]]]"
  | SET(GROUP) -> "List[List[BigInt]]"
  | CORE -> "BigInt"
  | DOMAIN -> "List[List[BigInt]]"
  | GROUP -> "List[BigInt]"
  | ty -> type2scalatype ty

let pp_type = function
    ty -> print_string (pp_type_cs ty)
