(* $Id$ *)

module B = Objects
module T = Types
module CS = Class_state
module G = Genaux

open Format

let policy_name = ref ""
let need_comma = ref false

let internal_error str =
  raise (Error.Error (Printf.sprintf "ipanema2leon: %s: internal error" str))

let start_block() =
  (* print_cut (); *)
  print_string "{"; print_cut();
  open_vbox 2; print_cut()

let end_block cut =
  close_box(); print_cut (); print_string "}";
  if cut then print_cut ()

let find_fun nm funs =
  try
    List.find (function Ast.FUNDEF(_,_,id,_,_,_,_) ->
			B.strideq ("ipanema_"^(!policy_name)^"_"^nm, id)) funs
  with
    Not_found -> internal_error ("function "^nm^" not found")


(* ssid is the identifier of the context (SharedState) containing all
   informations about cores, threads, and previous modifications. *)
let rec pp_exp ssid = function
Ast.VAR(id,_,attr) ->
     begin
       print_string "s.states(X).";
       print_string (B.id2c id);
       print_string " == res.states(X).";
       print_string (B.id2c id);
       if B.ty attr = B.QUEUE B.PROCESS then
	 begin
	   print_string " && s.states(X).";
	   print_string (B.id2c id);
	   print_string ".forall(e => s.procs(e) == res.procs(e))";
	 end;
       print_string ";";
     end
       
  | Ast.FIELD(Ast.FIELD(Ast.VAR(id,_,ida),target,_,a),fld,_,attr)
      when B.ty ida = B.PEVENT
      && B.id2c target = "target"
	&& B.ty a = B.PROCESS ->
     begin
       print_string "s.procs(p).";
       G.pp_id fld;
       print_string " == res.procs(p).";
       G.pp_id fld;
       print_string ";";
     end
	  
  | Ast.FIELD(exp,fld,_,attr) ->
     (match (B.ty (Ast.get_exp_attr exp)) with
	B.PROCESS
      | B.SRCPROCESS-> G.pp_id ssid; print_string ".procs(";
		       pp_exp ssid exp;
		       print_string ")"
      | B.CORE -> G.pp_id ssid; print_string ".states(";
		  pp_exp ssid exp;
		  print_string ")"
      | _ -> pp_exp ssid exp);
     print_string "."; G.pp_id fld

  | _ as exp -> Pp.pretty_print_exp exp

let pp_hdls handlers =
  (* the main handler function *)
  let param = match handlers with
      (Ast.EVENT(nm,Ast.VARDECL(_,id,_,_,_,_,_),_,_,attr) :: _) ->
	id
    | _ ->
       internal_error
	 "pp_handlers: handler list must not be empty, only one param allowed"
  in
  Ast2c.event_param := param;

  (* the top-level handlers *)
  List.iter
    (function (Ast.EVENT(nm,p,stmts,_,attr)) ->
      let Ast.EVENT_NAME(ename,_) = nm in
      let actual_snm = Ast.event_name2c nm in
      let name = B.id2c ename in
      let all_names = "ipanema_"^ name in
      try
	let exps = List.assoc name !Lock_verif.locks in
	let ssid = B.mkId "__state" in
	open_vbox 0;
	print_string all_names;
	G.start_top ();
	List.iter (fun exp ->
	  pp_exp ssid exp;
	  print_cut ()) exps;
	end_block true;
	print_cut ();
	close_box ()
      with Not_found ->
	()
    )
    handlers

let includefiles _ =
  "import scala.Any\n"^
    "import leon.collection._\n"^
      "import leon.lang._\n"^
	"import leon.annotation._\n"^
	  "import leon.proof._\n"^
	    "import leon.instrumentation._\n"^
	      "import leon.invariant._\n"^
		"import leon.util.Random\n"


(* -------------------------- entry point --------------------------- *)

let bcpp dirname
	 ((Ast.SCHEDULER(nm,cstdefs,enums, dom, grp, procdecls,
			 fundecls,valdefs,domains,states_,cstates,
			 criteria,trace,handlers,chandlers,ifunctions,functions,
			 attr))) : unit =

  let file_ext = ".scala" in
  let ch = open_out (dirname^"/"^nm^"_state"^file_ext) in
  set_formatter_out_channel ch;
  set_margin 80;
  policy_name := nm;
  G.print_nl_string (includefiles ());
  pp_hdls handlers;
  print_flush ();
  close_out ch;
  set_formatter_out_channel stdout;

(*
Par exemple
val toto = coreX.var;
===>  générer :
              s.states(X).var == res.states(X).var
          (var doit être le même dans s et res)

if(p.quanta > 3)
===> générer
               s.procs(p).quanta == res.procs(p).quanta


plus compliqué : certaines lignes introduisent des dépendances sur plusieurs processus :
val first = first(ready order { highest tick });
===> générer
                s.states(X).ready == res.states(X).ready &&
                s.states(X).ready.forall(e => s.procs(e) == res.procs(e))
    (Ready ne change pas et les processus non plus.)



Pour le coeur tu peux supposer qu'on a toujours une variable X qui indique le coeur et une variable p qui indique le processus, ça n'est pas forcement vrai, mais on devrait pouvoir corriger ça facilement :)

*)
