(* --------------------- Program-defined types --------------------- *)

module B = Objects

module HashString =
struct
	type t = string
	let equal (s1 : t) (s2 : t) = s1 = s2
	let hash (s : t) = Hashtbl.hash s
end
module StringHashtbl = Hashtbl.Make(HashString)

type idtype =
    STRUCT_TYPE | ENUM_TYPE | RANGE_TYPE | TIMER_TYPE | IDENT | FN_NAME | STATE_NAME

let lexicon = StringHashtbl.create 211
let internal = ref (StringHashtbl.create 1)

let init_lexicon _ =
  StringHashtbl.clear lexicon;
  (* copy from user *)
  StringHashtbl.add lexicon "get_user_int" FN_NAME;
  (* primitive time-related functions *)
  StringHashtbl.add lexicon "now" FN_NAME;
  StringHashtbl.add lexicon "start_absolute_timer" FN_NAME;
  StringHashtbl.add lexicon "start_relative_timer" FN_NAME;
  StringHashtbl.add lexicon "stop_timer" FN_NAME;
  StringHashtbl.add lexicon "time_to_ticks" FN_NAME;
  StringHashtbl.add lexicon "ticks_to_time" FN_NAME;
  StringHashtbl.add lexicon "ms_to_time" FN_NAME;
  StringHashtbl.add lexicon "time_to_ms" FN_NAME;
  (* primitive scheduling domains and groups functions *)
  StringHashtbl.add lexicon "contains" FN_NAME;
  StringHashtbl.add lexicon "system_cores" FN_NAME;
  StringHashtbl.add lexicon "steal_for_dom" FN_NAME;
  (* primitive set/queue functions *)
  StringHashtbl.add lexicon "add" FN_NAME;
  StringHashtbl.add lexicon "min" FN_NAME;
  StringHashtbl.add lexicon "max" FN_NAME;
  (* primitive debug functions *)
  StringHashtbl.add lexicon "bug" FN_NAME;
  StringHashtbl.add lexicon "assert" FN_NAME;
  if !B.high_res
  then
    begin
      StringHashtbl.add lexicon "make_time" FN_NAME;
      StringHashtbl.add lexicon "make_cycle_time" FN_NAME;
      StringHashtbl.add lexicon "make_cycles" FN_NAME;
      StringHashtbl.add lexicon "time_to_seconds" FN_NAME;
      StringHashtbl.add lexicon "time_to_nanoseconds" FN_NAME;
      StringHashtbl.add lexicon "time_to_jiffies" FN_NAME;
      StringHashtbl.add lexicon "time_to_subjiffies" FN_NAME
    end;
  internal := StringHashtbl.copy lexicon
    
let add_struct_type name =
  StringHashtbl.add lexicon name STRUCT_TYPE

let add_enum_type name =
  StringHashtbl.add lexicon name ENUM_TYPE

let add_range_type name =
  StringHashtbl.add lexicon name RANGE_TYPE

let add_timer_type name =
  StringHashtbl.add lexicon name TIMER_TYPE

let add_fn_name name =
  StringHashtbl.add lexicon name FN_NAME

let add_state_name name =
  StringHashtbl.add lexicon name STATE_NAME

let add_cstate_name name =
  StringHashtbl.add lexicon name STATE_NAME (*FIXME: introduce CSTATE_NAME*)

let scan_ident id =
  try StringHashtbl.find lexicon id
  with Not_found -> IDENT

let is_internal id = StringHashtbl.mem !internal id
let add_internal id = StringHashtbl.add !internal id FN_NAME

let is_user_fct id = not (is_internal id) && (scan_ident id == FN_NAME)
