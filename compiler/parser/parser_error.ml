
(* ---------------------------- parser error ---------------------------- *)

open Lexing
open Format

type t = { loc_start: int; loc_end: int }
      
let symbol_rloc () =
    { loc_start = Parsing.symbol_start();
      loc_end   = Parsing.symbol_end() }
    
let rhs_loc n =
  { loc_start = Parsing.rhs_start n;
    loc_end   = Parsing.rhs_end n }
	  
    
let print ppf loc =
  fprintf ppf "File \"%s\", line %i" 
    (Misc.get_filename()) (Misc.get_linenum loc.loc_start);
  fprintf ppf ", characters %i" 
    (loc.loc_start - (Misc.get_linebeg loc.loc_start));
  fprintf ppf "-%i:\n" 
    (loc.loc_end - (Misc.get_linebeg loc.loc_end))



(* Report an error *)

let report_error lexbuf ppf exn =
  let report ppf = function
    | Bossa_lexer.Error(err, start, stop) ->
	print ppf {loc_start = start; loc_end = stop};
	Bossa_lexer.report_error ppf err

(*    | Syntaxerr.Error(loc, err) ->
	Location.print ppf loc;
	Syntaxerr.report_error ppf err*)

    | Parsing.Parse_error
    | Bossa_parser.Error ->
	print ppf { loc_start = Lexing.lexeme_start lexbuf; 
		    loc_end   = Lexing.lexeme_end lexbuf};
	fprintf ppf "Syntax error on '%s'" (Lexing.lexeme lexbuf)

    | x ->
      print ppf { loc_start = Lexing.lexeme_start lexbuf;
		  loc_end   = Lexing.lexeme_end lexbuf};
      fprintf ppf "Exception: %s%!" (Printexc.to_string x) in
  flush_all ();
  fprintf ppf "@[%a@]@.%!" report exn
