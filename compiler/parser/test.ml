(* Test parsing/AST generation based on Axelle's script *)


(* File Variables *) 

let source_file = ref ""
let ast_file = ref "a.bossa"
let output_folder = ref "test_output"

(* Constants *)
let usage_msg =
    "Incorrect input format"
let current_dir = 
    Sys.getcwd()

let gen_ast source_file =
    let ch = open_in source_file in 
    let lexbuf = Lexing.from_channel ch in
    (
        try
            Bossa_lexer.init();
            let ast = Bossa_parser.bossa_spec Bossa_lexer.token lexbuf in
                print_endline "ok";
            ast
        with x->
            (print_endline "ko";
  	        Parser_error.report_error lexbuf Format.err_formatter x;
  	        exit 2)
    )


(* Updates code file*)
let set_source_file fi =
    source_file := fi

(* Updates AST output file*)
let set_ast_file fi =
    ast_file := fi

(* Set output folder *)
let set_output_folder fo =
    output_folder := fo

(* Main function: takes input parameters,
 * generates DAG, and prints it to specified 
 * output file *)
let _ =
    let speclist =
        [
          ("-source", Arg.String set_source_file, "Target Bossa file");
          ("-ast", Arg.String set_ast_file, "Target Generated AST file");
          ("-output", Arg.String set_output_folder, "Target Output Folder")
        ]
    in
    Arg.parse speclist print_string usage_msg;
    let source_fi = current_dir^"/"^(!source_file) in
    let ast_fi = current_dir^"/"^(!ast_file) in
    (
        print_string (source_fi^"\n");
        print_string (ast_fi^"\n");
        let ast = gen_ast source_fi in
        Ast_printer.print_ast ast ast_fi;
    ) 
