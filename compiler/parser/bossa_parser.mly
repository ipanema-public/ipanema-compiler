%{

(* $Id$ *)

module B = Objects
module CS = Class_state

%}

%token SCHEDULER TYPE ENUM THREAD CORE CONST ORDERING ORDER LOWEST HIGHEST KEY TRACE
%token THREADS CORES READY RUNNING BLOCKED
%token TERMINATED SET QUEUE FIFO LIFO FIRST SYSCORES SYNCHRONIZED
%token HANDLER TEVENT CEVENT ON INTERFACE STEAL STEAL4 FILTER STEALTHREAD UNTIL SELF
%token INT BOOL TIME CYCLES VOID IF ELSE FOREACH IN SWITCH RETURN TIMER PORT
%token <string> CTYPE
%token PERSISTENT (* PARENT *) LAZY SUM MIN MAX COUNT FNOR DISTANCE DISTANCES EXPORTED
%token CASE TRUE FALSE AND OR DOT LPAREN RPAREN LCURLY RCURLY BREAK CONTINUE
%token DOT_DOT LBRACK RBRACK
%token EQUAL_EQUAL NEQ COMMA SEMICOLON COLON EQL PLUS
%token MINUS TIMES DIV GT LT COMPLEMENT LEQ GEQ QM
%token RARROW  BITOR BITAND PLUS_PLUS MINUS_MINUS SELECT EMPTY VALID TARGET
%token DO SELECT_GROUP FILTER_GROUP
%token PLUS_EQUAL MINUS_EQUAL TIMES_EQUAL DIV_EQUAL MOD_EQUAL
%token BITAND_EQUAL BITOR_EQUAL LSHIFT_EQUAL RSHIFT_EQUAL MOD NOT
%token LSHIFT RSHIFT ERROR
%token EVENTS TEST EXPRESSIONS
%token SYSTEM STRUCT SHARED
%token ACTIVE SLEEPING DOMAIN DOMAINS GROUP TRY STARTING TO
%token EOF
%token <int> INTEGER
%token <string> IDENT
%token <string> STRUCT_TYPE
%token <string> ENUM_TYPE
%token <string> RANGE_TYPE
%token <string> TIMER_TYPE
%token <string> FN_NAME
%token <string> STATE_NAME
%token <string> STRING

%start bossa_spec
%type <Ast.scheduler * Objects.sched> bossa_spec

/* specify operator precedence to allow simple single production rule for expressions */
%left OR                /* lowest precedence */
%left AND
%left BITOR
%left BITAND
%left EQUAL_EQUAL NEQ
%left GT LT GEQ LEQ
%left LSHIFT RSHIFT
%left PLUS MINUS
%left TIMES DIV MOD IN
%right NOT COMPLEMENT
//%left PLUS_PLUS MINUS_MINUS
%right ELSE then_
%right loc_exp
%left DOT
//%nonassoc UMINUS                /* highest precedence */

/* start syntax definition */
%%
bossa_spec:
x_setpos
    top_v=globaldef*
    SCHEDULER id=IDENT EQL
    LCURLY sd=sched_decl body=sched_body RCURLY EOF
    { let (c,t,dg,p,v,dom,ps,cs,o,tr) = sd
      and (h,ch,i,f) = body (* FIXME Use Domain/Group dg *)
      in (Ast.mkSCHEDULER(id,c,t,dg,p,top_v@v,dom,ps,cs,o,tr,h,ch,i,f,Misc.getpos ()),
         B.PROCSCHED)
    }
   ;

sched_decl:
  c=constdef t=typedef_decl* dg=domgrpdef* p=processdef
    dps=pstatedef cs=cstatedef o=orderdef tr=tracedef  v=globaldef*
   { let (dom,ps) = dps in (c, t, dg, p, v, dom, ps, cs, o, tr)}
     ;

sched_body:
    phandlerdef chandlerdef interfacedef functiondef*        {($1,$2,$3,$4)}
    ;

typedef_decl:
    struct_def            { $1 }
  | enum_def              { $1 }
  | range_def             { $1 }
  | timer_def             { $1 }
  ;

struct_def:
  x_setpos TYPE IDENT EQL STRUCT LCURLY process_body* RCURLY SEMICOLON
    { Progtypes.add_struct_type $3 ;
      Ast.mkSTRUCTDEF($3, $7, Misc.getpos ())}
;

enum_def:
    x_setpos TYPE IDENT EQL ENUM LCURLY enum_body RCURLY SEMICOLON
    {Progtypes.add_enum_type $3;
      Ast.mkENUMDEF($3, $7, Misc.getpos ())}
;

enum_body:
   IDENT                               { [$1] }
  | IDENT COMMA enum_body             { $1::$3 }
;

range_def:
    x_setpos TYPE IDENT EQL LBRACK expr DOT_DOT expr RBRACK SEMICOLON
    {Progtypes.add_range_type $3;
      Ast.mkRANGEDEF($3, $6, $8, Misc.getpos ())}
;

timer_def:
    x_setpos IDENT TIMER EQL LCURLY timer_def_body RCURLY
    {Progtypes.add_timer_type $2;
     Ast.mkTIMERDEF($2, $6, Misc.getpos ())}
;

timer_def_body:
    non_timer_process_var_decl SEMICOLON                { [$1] }
  | non_timer_process_var_decl SEMICOLON timer_def_body   { $1::$3 }
  ;

/* no nested timers */
non_timer_process_var_decl:
      param_var_decl {$1}
  | x_setpos type_expr SYSTEM IDENT
      { Ast.mkSYSVARDECL(CS.UNSHARED, $2,$4, Misc.getpos ()) }
   ;

domgrpdef:
    DOMAIN EQL LCURLY body=process_body* RCURLY { (true, body)  }
  | GROUP EQL LCURLY body=process_body* RCURLY  { (false, body) }
  ;

processdef:
    THREAD EQL LCURLY pb=process_body* RCURLY   { pb }
  ;

process_body:
  x_setpos pvd=process_var_decl SEMICOLON                { pvd(Misc.getpos ()) }
  ;

valid_var_name:
 | CORES   {"cores"}
 | DOMAINS {"domains"}
 | IDENT   {$1}

process_var_decl:
    s=share t=type_expr id=valid_var_name
      { fun x -> Ast.mkVARDECL(s, t,id,x) }
  | s=share TIMER id=valid_var_name
      { fun x -> Ast.mkVARDECL(s, B.TIMER(B.NOT_PERSISTENT),id, x) }
  | s=share PERSISTENT TIMER id=valid_var_name
      { fun x -> Ast.mkVARDECL(s, B.TIMER(B.PERSISTENT(None)),id, x) }
  | s=share PERSISTENT t=TIMER_TYPE TIMER id=valid_var_name
      { fun x -> Ast.mkVARDECL(s, B.TIMER(B.PERSISTENT(Some(B.mkId t))),id, x) }
  | s=share t=type_expr SYSTEM id=valid_var_name
      { fun x -> Ast.mkSYSVARDECL(s, t,id, x) }
  | s=share SYSTEM t=type_expr id=valid_var_name
      { fun x -> Ast.mkSYSVARDECL(s, t, id, x) }
  | s=share LAZY t=type_expr id=valid_var_name EQL e=expr
      { fun x -> Ast.mkLAZYVARDECL(s, t, id, e, x)}
  | s=share ty=type_expr id=valid_var_name EQL e=expr
      { fun x -> Ast.mkINITVARDECL(s, ty,id, e, x)}
   ;

param_var_decl:
 x_setpos type_expr valid_var_name
      { Ast.mkVARDECL(CS.UNSHARED, $2,$3,Misc.getpos ()) }
   ;

const_var_decl:
  x_setpos base_type valid_var_name
      { Ast.mkVARDECL(CS.UNSHARED, $2,$3, Misc.getpos ()) }
   ;

constdef:
   x_setpos CONST d=const_var_decl EQL e=expr SEMICOLON l=constdef
        { Ast.mkCONSTDEF(d,e,Misc.getpos ()) :: l }
  |    x_setpos SYSTEM CONST d=const_var_decl SEMICOLON l=constdef
        { Ast.mkSYSDEF(d,Misc.getpos ()) :: l }
  |     { [] }
   ;

parameter_types:
  param_type_list        { $1 }
  |                      { [] }
  ;

/* for function prototypes */
param_type_list:
  one_param_type                         { [$1] }
  | one_param_type COMMA param_type_list { $1::$3 }

one_param_type:
  type_expr                          { $1 }
  | TIMER                            { B.TIMER(B.NOT_PERSISTENT) }
  | PERSISTENT TIMER                 { B.TIMER(B.PERSISTENT(None)) }
  | PERSISTENT TIMER_TYPE TIMER      { B.TIMER(B.PERSISTENT(Some(B.mkId $2))) }

globaldef:
  x_setpos base_type valid_var_name EQL expr SEMICOLON
      {let ln = Misc.getpos () in
      Ast.VL(Ast.mkVALDEF(Ast.mkVARDECL(CS.UNSHARED, $2,$3,ln),$5,ln)) }
  | x_setpos LAZY base_type valid_var_name EQL expr SEMICOLON
      { let loc = Misc.getpos () in
	Ast.VL(Ast.mkVALDEF(Ast.mkLAZYVARDECL(CS.UNSHARED, $3,$4, $6, loc),$6,loc)) }
  | x_setpos base_type SYSTEM valid_var_name SEMICOLON
      {let ln = Misc.getpos () in
      Ast.VL(Ast.mkSYSDEF(Ast.mkSYSVARDECL(CS.UNSHARED, $2,$4,ln),ln)) }
  | x_setpos SYSTEM ty=type_or_void id=valid_var_name LPAREN pty=parameter_types RPAREN SEMICOLON
      { Progtypes.add_fn_name id;
	Progtypes.add_internal id;
	Ast.FN(Ast.mkFUNDECL(Ast.SYSTEM,ty,id,pty,Misc.getpos ())) }
  | x_setpos ty=type_or_void id=valid_var_name LPAREN pty=parameter_types RPAREN SEMICOLON
      { Progtypes.add_fn_name id;
	Ast.FN(Ast.mkFUNDECL(Ast.DFT,ty,id,pty,Misc.getpos ())) }
  | x_setpos ty=proc_type id=valid_var_name LPAREN pty=parameter_types RPAREN SEMICOLON
      { Progtypes.add_fn_name id;
	Ast.FN(Ast.mkFUNDECL(Ast.DFT,ty,id,pty,Misc.getpos ())) }
  | x_setpos TRY ty=type_expr id=valid_var_name LPAREN pty=parameter_types RPAREN SEMICOLON
      { Progtypes.add_fn_name id;
	Ast.FN(Ast.mkFUNDECL(Ast.TRYLOCK,ty,id,pty,Misc.getpos ())) }
  | x_setpos VOID valid_var_name LPAREN parameter_types RPAREN SEMICOLON
      { Progtypes.add_fn_name $3;
	Ast.FN(Ast.mkFUNDECL(Ast.DFT, B.VOID,$3,$5,Misc.getpos ())) }
  | x_setpos TIMER valid_var_name SEMICOLON
        { let loc = Misc.getpos () in
	  Ast.VL(Ast.mkSYSDEF(Ast.mkVARDECL(CS.UNSHARED, B.TIMER(B.NOT_PERSISTENT),$3, loc),
			      loc)) }
  ;

valdef:
  x_setpos t=type_expr id=valid_var_name EQL e=expr SEMICOLON
	{let ln = Misc.getpos () in
	 Ast.mkVALDEF(Ast.mkVARDECL(CS.UNSHARED, t,id,ln),e,ln)}
  | x_setpos t=type_expr id=valid_var_name LPAREN RPAREN SEMICOLON
	{let ln = Misc.getpos () in
	 Ast.UNINITDEF(Ast.mkVARDECL(CS.UNSHARED, t,id,ln),B.mkAttr ln)}

orderdef:
   ORDERING EQL LCURLY order_body RCURLY { $4 }
  |                                      { [] }
   ;

order_body:
  | crit_decl   		  { [$1] }
  | crit_decl COMMA order_body    { $1::$3 }
  ;

crit_decl:
   x_setpos key crit_order valid_var_name     { Ast.mkCRIT_ID($2,$3,$4,Misc.getpos ()) } ;

key:
   KEY             { Ast.KEY(0,0) }
  |                { Ast.NOKEY }

crit_order:
   LOWEST          { Ast.LOWEST }
   | HIGHEST       { Ast.HIGHEST }

/* ------------------------------------------------------------------ */
/* trace */

tracedef:
   x_setpos TRACE INTEGER LCURLY trace_events trace_expressions trace_test
            RCURLY
            { let ln = Misc.getpos () in
              Progtypes.add_fn_name "print_trace_info";
	 match $5@$6@$7 with
	 [] -> Ast.NOTRACE
       | all -> Ast.mkTRACE($3, all, ln)}
   |   { Ast.NOTRACE }

trace_events:
   EVENTS EQL LCURLY pevent_types cevent_types RCURLY SEMICOLON
       {[Ast.TRACEEVENTS($4)]}
   | {[]}

trace_expressions:
   x_setpos EXPRESSIONS EQL LCURLY fields RCURLY SEMICOLON
       {[Ast.TRACEEXPS(Ast.mkTRACEEXP($5,Misc.getpos ()))]}
   | {[]}

trace_test:
   TEST EQL LCURLY expr RCURLY SEMICOLON
       {[Ast.TRACETEST($4)]}
   | {[]}

fields:
   valid_var_name                       { [$1] }
   | valid_var_name COMMA fields        { $1 :: $3 }
   | STATE_NAME                { [$1] }
   | STATE_NAME COMMA fields   { $1 :: $3 }

pstatedef:
     THREADS EQL LCURLY pstate_decl* RCURLY { (None, Ast.mkPSTATE($4))}
   | coredef          { let (d,ps_opt, cv, steal) = $1 in
 			match ps_opt with
			  None -> (d,Ast.mkCORE(cv, [], steal))
			| Some ps -> (d,Ast.mkCORE(cv, ps, steal))
                      }
   ;

pstatedef_:
     THREADS EQL LCURLY states=pstate_decl* RCURLY { states }

qtype:
  THREAD                   { }
;

pstate_decl:
     x_setpos pcmodifier class_name valid_var_name SEMICOLON
      { (* Rule for "TERMINATED term;" w/o type. *)
	Progtypes.add_state_name $4;
	Ast.mkQUEUE($3, $2, CS.EMPTY,$4,CS.PRIVATE,None,Misc.getpos ())}
   | x_setpos pcmodifier class_name THREAD valid_var_name SEMICOLON
	      { Progtypes.add_state_name $5;
		Ast.mkPROCESS($3,$2,$5,CS.PUBLIC,Misc.getpos ()) }
   | x_setpos pcmodifier class_name QUEUE LT qtype GT valid_var_name colon SEMICOLON
      { Progtypes.add_state_name $8;
        Ast.mkQUEUE($3, $2, CS.FIFO, $8, CS.PRIVATE, $9, Misc.getpos ()) }
   | x_setpos pcmodifier class_name SET LT qtype GT valid_var_name colon SEMICOLON
      { Progtypes.add_state_name $8;
        Ast.mkQUEUE($3, $2, CS.EMPTY, $8, CS.PRIVATE, $9, Misc.getpos ()) }
   ;

colon:
    COLON ordering {
        match $2 with
            | Some(crit,Some expr,line) -> Some(Ast.mkCRIT_EXPR(Ast.NOKEY, crit,expr,line))
            | Some(crit,None,line) -> Some(Ast.mkCRIT_ID(Ast.NOKEY, crit,"",line))
            | None -> None }
    | {None}

pcmodifier:  share { $1 }

share:
     SHARED  { CS.SHARED   }
   |         { CS.UNSHARED }

class_name:
     READY			{CS.READY}
   | RUNNING			{CS.RUNNING}
   | BLOCKED			{CS.BLOCKED}
   | TERMINATED			{CS.TERMINATED}

queue_type:
  SELECT			{CS.SELECT(CS.NODEFAULT) }
  | SELECT FIFO			{CS.SELECT(CS.FIFODEFAULT) }
  | SELECT LIFO			{CS.SELECT(CS.LIFODEFAULT) }
  |                             {CS.FIFO }
  ;

coredef:
  CORE EQL LCURLY core_body RCURLY { $4 }

core_body:
     pb=process_body cb=core_body { let (d, ps, cv, s) = cb in (d, ps, pb::cv, s)  }
  |  states=pstatedef_      cb=core_body { let (d, ps, cv, s) = cb in
				    match ps with
				      None -> (d, Some states, cv, s)
				    | Some _ -> failwith "Thread states already declared" }
  |  dd=domains      cb=core_body { let (d, ps, cv, s) = cb in
				    match d with
				      None -> (Some dd, ps, cv, s)
				    | Some _ -> failwith "Domains already defined" }
  |  steal=steal     cb=core_body { let (d, ps, cv, s) = cb in
				    match s with
				      None -> (d, ps, cv, Some steal)
				    | Some _ -> failwith "Steal already defined" }
  |                                       { (None, None, [], None) }
;

steal:
  x_setpos STEAL LPAREN DOMAIN dom=valid_var_name COMMA
     GROUP dstg=valid_var_name COMMA
     CORE dst=valid_var_name RPAREN
     EQL LCURLY sg=steal_group RCURLY
     { let ln = Misc.getpos () in (dom, dstg, dst, sg) }
  |  x_setpos STEAL LPAREN GROUP dstg=valid_var_name COMMA CORE dst=valid_var_name RPAREN
     EQL LCURLY sg=steal_group RCURLY
     { let ln = Misc.getpos () in ("__sd", dstg, dst, sg) }
  | x_setpos STEAL LPAREN CORE dst=valid_var_name RPAREN
     EQL LCURLY st=steal_thread RCURLY
     { let ln = Misc.getpos () in ("__sd", "", dst, Ast.STEAL_THREAD st) }
  | x_setpos STEAL EQL LCURLY st=steal_thread RCURLY
     { let ln = Misc.getpos () in ("__sd", "", "", Ast.STEAL_THREAD st) }
;

steal_group:
  fg=filter_group sg=select_group st=steal_thread until=until postgrp=stmt*
     { let (fg2, list) = fg in
       Ast.FLAT_STEAL_GROUP((fg2,sg list,until), st, postgrp) }
  | fg=filter_group DO LCURLY sg=select_group st=steal_thread post=stmt* RCURLY until=until postgrp=stmt*
     { let (fg2, list) = fg in
       Ast.ITER_STEAL_GROUP((fg2,sg list,until), st, post, postgrp) }

steal_thread:
   filter=filter select=select migrcond=migrcond
     { let (fc, list) = filter in
       let (select, core) = select in
       Ast.FLAT_STEAL_THREAD (fc, select list, migrcond core)
     }
  |  filter=filter DO LCURLY select=select migrcond=migrcond post=stmt* RCURLY until=until
     { let (fc, list) = filter in
       let (select, core) = select in
       Ast.ITER_STEAL_THREAD ((fc, select list, migrcond core), until, post)
     };

filter_group:
    x_setpos FILTER_GROUP LPAREN GROUP f=valid_var_name COMMA GROUP self=valid_var_name RPAREN
     LCURLY filter=expr SEMICOLON? RCURLY RARROW list=valid_var_name
     { let ln = Misc.getpos () in
       ((Ast.mkVARDECL(CS.UNSHARED, B.GROUP, self, ln),
	 Ast.mkVARDECL(CS.UNSHARED, B.GROUP, f,ln),
	 Ast.mkRETURN(filter,ln)),
	list) }
;

select_group:
  x_setpos SELECT_GROUP LPAREN RPAREN LCURLY
     stolen=expr SEMICOLON? RCURLY RARROW elected=valid_var_name
     { let ln = Misc.getpos () in
       let sg list =
       (Ast.mkVARDECL(CS.UNSHARED, B.DOMAIN, B.fresh_idxstr "dom", ln),
	Ast.mkVARDECL(CS.UNSHARED, B.GROUP, elected, ln),
	Ast.mkVARDECL(CS.UNSHARED, B.SET B.GROUP, list, ln),
	Ast.mkRETURN(stolen, ln)) in
       sg
     };

filter:
  x_setpos FILTER LPAREN CORE f=valid_var_name RPAREN
     LCURLY filter=expr SEMICOLON? RCURLY RARROW list=valid_var_name
     { let ln = Misc.getpos () in
       ((Ast.mkVARDECL(CS.UNSHARED, B.CORE, "self", -1),
	 Ast.mkVARDECL(CS.UNSHARED, B.CORE, f,ln),
	 Ast.mkRETURN(filter,ln)),
	list)
     }
  |  x_setpos FILTER LPAREN CORE f=valid_var_name COMMA CORE self=valid_var_name RPAREN
     LCURLY filter=expr SEMICOLON? RCURLY RARROW list=valid_var_name
     { let ln = Misc.getpos () in
       ((Ast.mkVARDECL(CS.UNSHARED, B.CORE, self, ln),
	 Ast.mkVARDECL(CS.UNSHARED, B.CORE, f,ln),
	 Ast.mkRETURN(filter,ln)),
	list)
     };

select:
  x_setpos SELECT LPAREN RPAREN LCURLY
     stolen=expr SEMICOLON? RCURLY RARROW core=valid_var_name
     { let ln = Misc.getpos () in
       let sc list =
	 (Ast.mkVARDECL(CS.UNSHARED, B.INDR (B.SET B.CORE), list, ln),
	  Ast.mkVARDECL(CS.UNSHARED, B.CORE, "self",  ln),
	  Ast.mkRETURN(stolen, ln)) in
       (sc, core)
     };

migrcond:
  x_setpos STEALTHREAD LPAREN CORE dstc=valid_var_name
     COMMA THREAD p=valid_var_name RPAREN
      migrcond=seq_stmt until=until
     { let ln = Misc.getpos () in
       let migr srcc =
	 (Ast.mkVARDECL(CS.UNSHARED, B.CORE, srcc, ln),
	  Ast.mkVARDECL(CS.UNSHARED, B.CORE, dstc, ln),
	  None,
	  Ast.mkVARDECL(CS.UNSHARED, B.PROCESS, p, ln), migrcond, until, Ast.mkSEQ([],[],-1))
       in migr
     }
  |  x_setpos STEALTHREAD LPAREN GROUP dstg=valid_var_name
     COMMA CORE dstc=valid_var_name
     COMMA THREAD p=valid_var_name RPAREN
      migrcond=seq_stmt until=until
     { let ln = Misc.getpos () in
       let migr srcc =
	 (Ast.mkVARDECL(CS.UNSHARED, B.CORE, srcc, ln),
	  Ast.mkVARDECL(CS.UNSHARED, B.CORE, dstc, ln),
	  Some (Ast.mkVARDECL(CS.UNSHARED, B.GROUP, dstg, ln)),
	  Ast.mkVARDECL(CS.UNSHARED, B.PROCESS, p, ln), migrcond, until, Ast.mkSEQ([],[],-1))
       in migr
     };

until:
    UNTIL LPAREN stopcond=expr RPAREN   { stopcond }
  | x_setpos { Ast.mkBOOL(true, Misc.getpos ())}
;

domains:
    DOMAINS LPAREN CORE id=valid_var_name RPAREN
	  TO target=valid_var_name EQL LCURLY body=dom_body* RCURLY
      {(id,target,body)}
;

dom_body:
    FOREACH LPAREN id=valid_var_name IN DISTANCES RPAREN LCURLY
          stmts=dom_body+ RCURLY { Ast.mkDOMFOR(id, None, stmts) }
  | FOREACH LPAREN id=valid_var_name IN DISTANCES STARTING i=INTEGER RPAREN LCURLY
          stmts=dom_body+ RCURLY { Ast.mkDOMFOR(id, Some i, stmts) }
  | DOMAIN LPAREN id=valid_var_name BITOR exp=expr RPAREN
	   TO target=valid_var_name EQL LCURLY
       gd=grp_def RCURLY	  { Ast.mkDOMDOM(id,exp,target,gd) }
;

  grp_def:
    GROUP LPAREN id1=valid_var_name COMMA id2=valid_var_name BITOR e=expr RPAREN
    TO target=valid_var_name SEMICOLON  { (id1,id2,e,target) }
;

cstatedef:
   CORES EQL LCURLY cstate_decl+ RCURLY{ $4 }
                          | { [] }
   ;

cstate_decl:
     x_setpos cstate_class CORE valid_var_name SEMICOLON
      { Progtypes.add_cstate_name $4;
        Ast.mkCSTATE($2, $4, Misc.getpos ()) }
   | x_setpos cstate_class  SET LT CORE GT valid_var_name SEMICOLON
      { Progtypes.add_cstate_name $7;
        Ast.mkCSET($2, $7, Misc.getpos ()) }
;

cstate_class:
     ACTIVE   {CS.ACTIVE}
   | SLEEPING {CS.SLEEPING}
;

phandlerdef:
     x_setpos HANDLER LPAREN TEVENT valid_var_name RPAREN LCURLY pevent_body RCURLY
     { let ln = Misc.getpos () in
       List.map
	    (function x -> (x (Ast.mkVARDECL(CS.UNSHARED, B.PEVENT,$5, ln))))
	    ($8) }
;

pevent_body:
   pevent_decl           	  { $1 }
  | pevent_decl pevent_body       { $1@$2 }
  ;

pevent_decl:
  x_setpos ON s=SYNCHRONIZED? e=pevent_types stmts=seq_stmt
		{ let ln = Misc.getpos () in
		  List.map
		  (function nm ->
		    function param ->
		      let s = (s <> None) in
		      Ast.mkEVENT(nm,param,stmts, s,ln))
			    e
		}
  ;

pevent_types:
  x_setpos pevent_type_   { [$2(Misc.getpos ())]}
  | x_setpos pevent_type_ COMMA pevent_types   { ($2(Misc.getpos ()))::$4}
;

pevent_type_:
  pevent_name           { function x ->
                                  Ast.mkEVENT_NAME($1, x) }
  ;

pevent_name:
    valid_var_name              { $1 }
  | STATE_NAME                  { $1 }
  | THREAD                     { "process" }
  | SYSTEM                      { "system" }
  | TIMER                       { "timer" }
;

chandlerdef:
    x_setpos HANDLER LPAREN CEVENT valid_var_name RPAREN LCURLY cevent_body RCURLY
	{ (*FIXME : distinguish between core/process *)
let ln = Misc.getpos () in
    List.map
	    (function x -> (x (Ast.mkVARDECL(CS.UNSHARED, B.CEVENT,$5,ln))))
	    ($8) }
;

cevent_body:
    cevent_decl           	{ $1 }
  | cevent_decl cevent_body     { $1@$2 }
  ;

cevent_decl:
  x_setpos ON s=SYNCHRONIZED? e=cevent_types stmts=seq_stmt
		{ let ln = Misc.getpos () in
		  List.map
		  (function nm ->
		    function param ->
		      let s = (s <> None) in
		      Ast.mkEVENT(nm,param,stmts, s,ln))
			    e
		}
  ;

cevent_types:
  x_setpos cevent_type_   { [$2(Misc.getpos ())]}
  | x_setpos cevent_type_ COMMA cevent_types   { ($2(Misc.getpos ()))::$4}
;

cevent_type_:
  cevent_name { function x ->  Ast.mkEVENT_NAME($1,x) }
  ;

cevent_name:
      valid_var_name { $1 }
    | STATE_NAME     { $1 }
    | FN_NAME        { $1 }

interfacedef:
  INTERFACE EQL LCURLY intdef* RCURLY { $4 }
  ;

intdef:
  x_setpos type_or_void valid_var_name parameters seq_stmt
	{ Ast.mkFUNDEF(Ast.DFT,$2,$3,$4,$5,Misc.getpos ()) }
  ;

functiondef:
  x_setpos ty=type_or_void id=FN_NAME ps=parameters stmts=seq_stmt
	{ Ast.mkFUNDEF(Ast.DFT,ty,id,ps,stmts,Misc.getpos ()) }
| x_setpos TRY ty=type_or_void id=FN_NAME ps=parameters stmts=seq_stmt
	{ Ast.mkFUNDEF(Ast.TRYLOCK,ty,id,ps,stmts,Misc.getpos ()) }
| x_setpos EXPORTED ty=type_or_void id=valid_var_name ps=parameters stmts=seq_stmt
	{ Ast.mkEXPORT_FUNDEF(ty,id,ps,stmts,Misc.getpos ()) }
  ;

parameters:
 LPAREN param_lists_body RPAREN   { $2 }
 | LPAREN RPAREN 	          { [] }
 ;

param_lists_body:
   param_var_decl 	                   { [$1] }
   | param_var_decl COMMA param_lists_body { $1::$3 }
   ;

%inline type_expr:
  base_type    { $1 }
  | proc_type    { $1 }
  | QUEUE LT proc_type GT { B.QUEUE ($3) }
  | SET LT proc_type GT   { B.SET ($3) }

%inline proc_type:
  | THREAD     { B.PROCESS }
  | CORE       { B.CORE}
  ;

base_type:
    INT			  { B.INT }
  | BOOL		  { B.BOOL }
  | TIME 		  { B.TIME }
  | CYCLES 		  { B.CYCLES }
  | PORT		  { B.PORT }
  | DISTANCE		  { B.DISTANCE }
  | DOMAIN		  { B.DOMAIN }
  | GROUP		  { B.GROUP }
  | QUEUE LT base_type GT { B.QUEUE ($3) }
  | SET LT base_type GT   { B.SET ($3) }
  | ENUM_TYPE             { B.ENUM(B.mkId($1)) }
  | RANGE_TYPE 	          { B.RANGE(B.mkId($1)) }
  | STRUCT_TYPE           { B.STRUCT(B.mkId($1)) }
  | SYSTEM STRUCT valid_var_name   { B.SYSTEM(B.STRUCT(B.mkId($3))) }
  | t=CTYPE               { B.OPAQUE(t) }
  ;


%inline type_or_void:
  type_expr		{ $1 }
  | VOID		{B.VOID}
  ;

stmt:
    if_stmt    		{ $1 }
  | for_stmt		{ $1 }
  | return_stmt 	{ $1 }
  | switch_stmt		{ $1 }
  | seq_stmt   		{ $1 }
  | assign_stmt	SEMICOLON	{ $1 }
  (*  | parentfn_stmt       { $1 } *)
  | prim_stmt           { $1 }
  | steal_stmt          { $1 }
  | error_stmt          { $1 }
  | break_stmt          { $1 }
  | continue_stmt       { $1 }
  ;

if_stmt:
    x_setpos if_condition stmt %prec then_
      {Ast.mkIF($2,$3, Ast.mkSEQ([],[],-1),Misc.getpos ()) }
  | x_setpos if_condition stmt ELSE stmt
	{ Ast.mkIF($2,$3,$5,Misc.getpos ()) }
;

if_condition:
  IF LPAREN expr RPAREN  { $3 }
  ;

for_stmt:
    x_setpos FOREACH LPAREN valid_var_name IN SYSCORES LPAREN RPAREN ordering RPAREN seq_stmt
      {
        let ln = Misc.getpos() in
        let sc = [Ast.mkPRIM("system_cores", [], ln)] in
        (* et sc = [Ast.mkVAR("possible_cpu", [], ln)] in *)
        match $9 with
            | None -> Ast.mkFOR($4,sc,$11, None, ln)
            | Some(crit, Some expr,line) -> Ast.mkFOR($4,sc,$11,
            Some(Ast.mkCRIT_EXPR(Ast.NOKEY,crit,expr,line)), ln)
            | Some(crit, None,line) -> Ast.mkFOR($4,sc,$11,
            Some(Ast.mkCRIT_ID(Ast.NOKEY,crit,"",line)), ln)
     }
  | x_setpos FOREACH LPAREN valid_var_name IN state_name_list ordering RPAREN seq_stmt
      {
        let ln = Misc.getpos() in
        match $7 with
        None -> Ast.mkFOR($4,$6,$9, None , ln)
          | Some(crit,Some expr,line) -> Ast.mkFOR($4,$6,$9,Some(Ast.mkCRIT_EXPR(Ast.NOKEY, crit,expr,line)), ln)
          | Some(crit,None,line) -> Ast.mkFOR($4,$6,$9,Some(Ast.mkCRIT_ID(Ast.NOKEY, crit,"",line)), ln)
        }
  | x_setpos FOREACH LPAREN valid_var_name RPAREN seq_stmt
	{Ast.mkFOR_ANY($4,$6,Misc.getpos())}
  ;

state_name_list :
     loc_expr  				{ [$1] }
   | x_setpos class_name		{ [Ast.mkVAR(CS.class2idc $2, [], Misc.getpos())] }
   | loc_expr COMMA state_name_list  	{ $1::$3 }
   | x_setpos class_name COMMA state_name_list
       { (Ast.mkVAR(CS.class2idc $2,[],Misc.getpos ()))::$4 }
   ;

switch_stmt:
  x_setpos SWITCH loc_expr IN seq_case { Ast.mkSWITCH($3,$5,Misc.getpos ())}
  ;

return_stmt:
  x_setpos RETURN expr SEMICOLON		{ Ast.mkRETURN($3,Misc.getpos ()) }
  | x_setpos RETURN SEMICOLON		{ Ast.mkRETURNVOID(Misc.getpos ()) }
  ;

steal_stmt:
  x_setpos STEAL4 LPAREN e=expr RPAREN SEMICOLON
      {Ast.mkSTEAL(e,Misc.getpos ())}

error_stmt:
  x_setpos ERROR LPAREN STRING RPAREN SEMICOLON
      {Ast.mkERROR($4,Misc.getpos ())}

break_stmt:
    x_setpos BREAK SEMICOLON
      {Ast.mkBREAK(Misc.getpos ())}
continue_stmt:
  | x_setpos CONTINUE SEMICOLON
      {Ast.mkCONTINUE(Misc.getpos ())}

seq_case:
  LCURLY case_stmt RCURLY	{ $2 }
  ;

case_stmt:
  x_setpos CASE state_lists COLON seq_stmt case_stmt
      { Ast.mkSEQ_CASE($3,$5,Misc.getpos ()) :: $6 }
  |   { [] }
  ;

state_lists:
  STATE_NAME		         {[$1]}
  | STATE_NAME COMMA state_lists {$1::$3}
  | class_name		         {[CS.class2c($1)]}
  | class_name COMMA state_lists {(CS.class2c($1))::$3}
  ;

field_ref:
    valid_var_name { $1 }
  | STATE_NAME     { $1 (* For remote access to state variables on another core *) }
  | TARGET         { "target" }
  ;

seq_stmt:
    x_setpos LCURLY valdef* stmt* RCURLY {Ast.mkSEQ($3,$4,Misc.getpos ())}
    /* Introduce too much shift/reduce conflicts
   | x_setpos valdef       {Ast.mkSEQ([$2],[],Misc.getpos ())}
   | x_setpos stmt         {Ast.mkSEQ([],[$2],Misc.getpos ())}
     */
   ;

(*
parentfn_stmt:
   x_setpos PARENT DOT valid_var_name LPAREN arglist RPAREN SEMICOLON
      {let x = Misc.getpos () in
      Ast.mkPRIMSTMTEXP(Ast.mkFIELD(Ast.mkPARENT(x),$4,[],x),$6,x)}
*)

prim_stmt:
   x_setpos FN_NAME LPAREN arglist RPAREN SEMICOLON
      {Ast.mkPRIMSTMT($2,$4,Misc.getpos ())}

assign_stmt:
  x_setpos assign_stmt_         { $2(Misc.getpos ()) }

assign_stmt_:
  loc_expr PLUS_PLUS
      {function x -> Ast.mkCOMPOUND_ASSIGN($1,Ast.PLUS,Ast.mkINT(1,x),x)}
  | loc_expr MINUS_MINUS
      {function x -> Ast.mkCOMPOUND_ASSIGN($1,Ast.MINUS,Ast.mkINT(1,x),x)}
  | loc_expr EQL expr {function x ->  Ast.mkASSIGN($1,$3,x)}
  | loc_expr PLUS_EQUAL expr
      {function x -> Ast.mkCOMPOUND_ASSIGN($1,Ast.PLUS,$3,x)}
  | loc_expr MINUS_EQUAL expr
      {function x -> Ast.mkCOMPOUND_ASSIGN($1,Ast.MINUS,$3,x)}
  | loc_expr TIMES_EQUAL expr
      {function x -> Ast.mkCOMPOUND_ASSIGN($1,Ast.TIMES,$3,x)}
  | loc_expr DIV_EQUAL expr
      {function x -> Ast.mkCOMPOUND_ASSIGN($1,Ast.DIV,$3,x)}
  | loc_expr MOD_EQUAL expr
      {function x -> Ast.mkCOMPOUND_ASSIGN($1,Ast.MOD,$3,x)}
  | loc_expr BITAND_EQUAL expr
      {function x -> Ast.mkCOMPOUND_ASSIGN($1,Ast.BITAND,$3,x)}
  | loc_expr BITOR_EQUAL expr
      {function x -> Ast.mkCOMPOUND_ASSIGN($1,Ast.BITOR,$3,x)}
  | loc_expr LSHIFT_EQUAL expr
      {function x -> Ast.mkCOMPOUND_ASSIGN($1,Ast.LSHIFT,$3,x)}
  | loc_expr RSHIFT_EQUAL expr
      {function x -> Ast.mkCOMPOUND_ASSIGN($1,Ast.RSHIFT,$3,x)}
/* This ought to allow an arbitrary expr to the left of the arrow, but
doing so causes parsing problems.  Fortunately it seems that only a loc_expr
or select() can be type correct. */
  | move_expr RARROW loc_expr
      {function x -> Ast.mkMOVE($1,$3,x)}
 ;

expr:
  x_setpos expr_                { $2(Misc.getpos ()) }

expr_:
     SELF      {function x -> Ast.mkSELF(x)}
   | INTEGER   {function x -> Ast.mkINT($1, x)}
   | valid_var_name      {function x -> Ast.mkVAR($1, [], x)}
   | STATE_NAME {function x -> Ast.mkVAR($1, [], x)}
   | READY      {function x -> Ast.mkVAR("READY", [], x)}
   | TRUE	{function x -> Ast.mkBOOL(true, x)}
   | FALSE	{function x -> Ast.mkBOOL(false, x)}
(*   | PARENT DOT valid_var_name LPAREN arglist RPAREN
       {function x ->
     Ast.mkPRIMEXP(Ast.mkFIELD(Ast.mkPARENT(x),$3,[],x),$5,x)} *)
   | PLUS expr_  {function x -> Ast.mkBINARY(Ast.PLUS, Ast.mkINT(0,x), $2 x,
			      x)}
   | MINUS expr_ {function x -> Ast.mkBINARY(Ast.MINUS, Ast.mkINT(0,x), $2 x,
			      x)}
   | TIMES expr_ {function x -> Ast.mkINDR($2 x, x)}
   | NOT expr_ {function x -> Ast.mkUNARY(Ast.NOT, $2 x, x)}
   | COMPLEMENT expr_  {function x -> Ast.mkUNARY(Ast.COMPLEMENT, $2 x, x)}
   | expr_ DOT field_ref  {function x -> Ast.mkFIELD($1 x,$3,[],x)}
   | FIRST LPAREN expr o=ordering RPAREN {
        match o with
            None -> (function x -> Ast.mkFIRST($3, None, x))
            | Some(crit,Some expr,line) -> (function x -> Ast.mkFIRST($3, Some(Ast.mkCRIT_EXPR(Ast.NOKEY, crit,expr,line)), x))
            | Some(crit,None,line) -> (function x -> Ast.mkFIRST($3, Some(Ast.mkCRIT_ID(Ast.NOKEY, crit,"",line)), x))
   }
   | int_fn LPAREN arglist RPAREN {function x -> Ast.mkPRIMEXP($1 (B.mkAttr x), $3, x)}
   | SYSCORES LPAREN RPAREN   { function x -> Ast.mkPRIM("system_cores",[], x) }
   | FN_NAME LPAREN arglist RPAREN {function x -> Ast.mkPRIM($1,$3,x)}
   | EMPTY LPAREN valid_var_name RPAREN {function x -> Ast.mkEMPTY($3,x)}
   | EMPTY LPAREN STATE_NAME RPAREN {function x -> Ast.mkEMPTY($3,x)}
   | EMPTY LPAREN class_name RPAREN {function x -> Ast.mkEMPTYCLASS($3,x)}
   | VALID LPAREN expr RPAREN {function x -> Ast.mkVALID($3,x)  (*FIXME: should be cstate_name*) }
   | expr_ PLUS expr_ {function x -> Ast.mkBINARY(Ast.PLUS, $1 x, $3 x, x)}
   | expr_ MINUS expr_ {function x -> Ast.mkBINARY(Ast.MINUS, $1 x, $3 x, x)}
   | expr_ TIMES expr_ {function x -> Ast.mkBINARY(Ast.TIMES, $1 x, $3 x, x)}
   | expr_ DIV expr_ {function x -> Ast.mkBINARY(Ast.DIV, $1 x, $3 x, x)}
   | expr_ MOD expr_ {function x -> Ast.mkBINARY(Ast.MOD, $1 x, $3 x, x)}
   | expr_ AND expr_ {function x -> Ast.mkBINARY(Ast.AND, $1 x, $3 x, x)}
   | expr_ OR expr_ {function x -> Ast.mkBINARY(Ast.OR, $1 x, $3 x, x)}
   | expr_ BITAND expr_ {function x -> Ast.mkBINARY(Ast.BITAND, $1 x, $3 x, x)}
   | expr_ BITOR expr_ {function x -> Ast.mkBINARY(Ast.BITOR,$1 x, $3 x, x)}
   | expr_ EQUAL_EQUAL expr_ {function x -> Ast.mkBINARY(Ast.EQ,$1 x, $3 x, x)}
 | expr_ NEQ expr_ {function x -> Ast.mkBINARY(Ast.NEQ,$1 x, $3 x, x)}
   | expr_ LT expr_ {function x -> Ast.mkBINARY(Ast.LT, $1 x, $3 x, x)}
   | expr_ GT expr_ {function x -> Ast.mkBINARY(Ast.GT, $1 x, $3 x, x)}
   | expr_ LEQ expr_ {function x -> Ast.mkBINARY(Ast.LEQ,$1 x, $3 x, x)}
   | expr_ GEQ expr_ {function x -> Ast.mkBINARY(Ast.GEQ,$1 x, $3 x, x)}
   | expr_ LSHIFT expr_ {function x -> Ast.mkBINARY(Ast.LSHIFT, $1 x, $3 x, x)}
   | expr_ RSHIFT expr_ {function x -> Ast.mkBINARY(Ast.RSHIFT, $1 x, $3 x, x)}
   | expr_ QM expr_ COLON expr_ {function x -> Ast.mkTERNARY($1 x, $3 x, $5 x, x)}
   | e=expr_ IN le=loc_expr
	   {function x -> Ast.mkIN(e x, le, None, x) }
   | e=expr_ IN cn=class_name {function x -> Ast.mkINCLASS(e x, cn, None, x) }
   | LPAREN expr RPAREN {function x -> $2}
    ;

int_fn:
     DISTANCE { fun x -> Ast.DISTANCE x }
   | SUM      { fun x -> Ast.SUM      x }
   | MIN      { fun x -> Ast.MIN      x }
   | MAX      { fun x -> Ast.MAX      x }
   | COUNT    { fun x -> Ast.COUNT    x }
   | FNOR     { fun x -> Ast.FNOR     x }
   ;

ordering:
    ORDER EQL LCURLY c=crit_order e=expr RCURLY { Some(c, Some e, !Misc.lineno) }
  | ORDER EQL LCURLY c=crit_order RCURLY { Some(c, None, !Misc.lineno) }
  |                                      { None }
   ;

arglist:
   arglist1                      { $1 }
   |                             { [] }
   ;

arglist1:
   expr                          { [$1] }
   | expr COMMA arglist1         { $1 :: $3 }
   ;

loc_expr:
  x_setpos loc_expr_  %prec loc_exp
			      { $2(Misc.getpos ()) }
   ;

loc_expr_:
    valid_var_name     	 {function x -> Ast.mkVAR($1, [], x)}
  | STATE_NAME   	 {function x -> Ast.mkVAR($1, [], x)}
  | SELF         	 {function x -> Ast.mkSELF x}
  | loc_expr_ DOT field_ref  {function x -> Ast.mkFIELD($1 x, $3, [],x)}
  | FIRST LPAREN e=expr o=ordering RPAREN {
      match o with
       | None -> (function x -> Ast.mkFIRST(e, None, x))
       | Some(crit,Some expr,line) -> (function x -> Ast.mkFIRST(e, Some(Ast.mkCRIT_EXPR(Ast.NOKEY, crit,expr,line)), x))
       | Some(crit,None,line) -> (function x -> Ast.mkFIRST(e, Some(Ast.mkCRIT_ID(Ast.NOKEY, crit,"",line)), x))
  }
  ;

move_expr:
    x_setpos SELECT LPAREN RPAREN     { Ast.mkSELECT(Misc.getpos ()) }
  | x_setpos SYSCORES LPAREN RPAREN   { Ast.mkPRIM("system_cores",[],Misc.getpos ()) }
  | loc_expr                          { $1 }
;

/* helper production. Purpose: sideeffect: set lineno to get position
where expr starts. NB: rule makes the balance +1 for x_setpos */
x_setpos:
	  {
	    Stack.push (!Misc.lineno, !Misc.cr_index) Misc.pos_stack;
	  }
	  ;
