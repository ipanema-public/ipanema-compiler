(* $Id$ *)

(* **************************************************************** *)
(* String manipulation *)

(* Rewrite a string into a list of char *)
let string2list (s : string) = 
  let l = ref [] in
  let n = String.length s in
  for i = 0 to n-1 do
    l := s.[i] :: !l
  done;
  List.rev !l


(* rewrite a list of char into a string *)
let list2string (l : char list) =
  String.concat "" (List.map (fun x -> Printf.sprintf "%c" x) l)


(* string_remove c s : remove all characters c in string s *)
let string_remove (c : char) (s : string) =
  list2string (List.filter ((!=) c) (string2list s))


(* string_replace c1 c2 s : replace all occurences of character c1 in
   string s by character c2 *)
 let string_replace (c1 : char) (c2 : char)
(s : string) = list2string (List.map (fun c -> if c = c1 then c2 else
c) (string2list s))


(* return a new string where all spaces are removed *)
let remove_spaces = string_remove ' '



(* given a list, return a new list witout any doublons *)
let rec list_remove_doublon = function
    [] -> []
  | x :: xs -> x :: (list_remove_doublon (List.filter (fun e -> e <> x) xs))


(* replace the i eme elt of list l if exists by y *)
let list_replace i y l = 
  let rec list_replace_ i j = function
    [] -> []
  | x :: xs -> if (i=j) then (y::xs) else x::(list_replace_ i (j+1) xs)
  in list_replace_ i 0 l





(* **************************************************************** *)
(* for lexer and  parser *)
let linebeg = ref []
let incr x =
   linebeg := !linebeg @ [(Lexing.lexeme_start x) + 1]
      (* to skip the '\n' character of the begining of the next line *)

let lineno = ref 1
let cr_index = ref 1    (* index of carriage-return, set upon each CR *)

(* stack of lineno,cr_index *)
let pos_stack = (Stack.create ():(int*int) Stack.t)
let filename = ref ""

(* helper function which fills out a pos structure *)
let getpos _ =
  let lineno, cr_index =
    try
      Stack.pop pos_stack
    with Stack.Empty ->
      Printf.eprintf "Unbalanced x_setpos and getpos in the parser";
      (!lineno, 0)
  in
   lineno

let get_linenum c =
  let rec _get_linenum num = function
      [] -> num
    | x :: xs -> 
	if c < x then num 
	else (_get_linenum (num+1) xs) in
  _get_linenum 1 !linebeg

let get_linebeg c = 
  let rec _get_linebeg = function
      [] -> 0
    | x :: [] -> x
    | x :: y :: ys -> if c < y then x else (_get_linebeg (y::ys)) in
  _get_linebeg !linebeg

let get_filename () =
  !filename

let init fname =
  filename := fname;
  lineno := 1;
  cr_index := 1

