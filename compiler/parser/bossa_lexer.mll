{
(* $Id$ *)
open Bossa_parser
open Format
open Lexing

type error =
  | Illegal_character of char
  | Unterminated_comment

(* Error defines error types and positions*)
exception Error of error * int * int

(* To store the position of the beginning of a comment and bitpat *)
let comment_start_pos = ref 0

(* Report Error  *)

let report_error fmt = function
  | Illegal_character c  -> fprintf fmt "Illegal character (%s)" (Char.escaped c)
  | Unterminated_comment -> fprintf fmt "Comment not terminated"

let scan_escape str =
	match str with
	"n" -> "\n"
	| "r" -> "\r"
	| "t" -> "\t"
	| "b" -> "\b"
	| _ -> str
let get_value chr =
	match chr with
	'0'..'9' -> (Char.code chr) - (Char.code '0')
	| 'a'..'z' -> (Char.code chr) - (Char.code 'a') + 10
	| 'A'..'Z' -> (Char.code chr) - (Char.code 'A') + 10
	| _ -> 0
let scan_hex_escape str =
	String.make 1 (Char.chr (
		(get_value (String.get str 0)) * 16
		+ (get_value (String.get str 1))
	))
let scan_oct_escape str =
	String.make 1 (Char.chr (
		(get_value (String.get str 0)) * 64
		+ (get_value (String.get str 1)) * 8
		+ (get_value (String.get str 2))
	))
}

(* define rules *)

let id = ['_' 'a'-'z' 'A'-'Z'] ['a'-'z' 'A'-'Z' '0'-'9' '_']*
let decimal_literal = ['0'-'9']+

(* for strings *)
let escape = '\\' _
let octdigit = ['0'-'7']
let hexdigit = ['0'-'9' 'a'-'f' 'A'-'F']
let hex_escape = '\\' ['x' 'X'] hexdigit hexdigit
let oct_escape = '\\' octdigit  octdigit octdigit
let blank = [' ' '\t' '\n']

let hex_literal = '0' 'x' hexdigit+

rule token = parse
	|	'#'		{ line lexbuf   }
    	|   	"&&"            { AND	        }
    	|   	"||"            { OR            }
   	|   	"=>"            { RARROW        }
    	|   	"="             { EQL   	}
    	|   	">="            { GEQ   	}
    	|   	"<="            { LEQ    	}
    	|   	"=="            { EQUAL_EQUAL   }
    	|   	"!="            { NEQ  		}
	|	"++"		{ PLUS_PLUS	}
	|	"--"		{ MINUS_MINUS	}
	|	"+="		{ PLUS_EQUAL	}
	|	"-="		{ MINUS_EQUAL	}
	|	"*="		{ TIMES_EQUAL	}
	|	"/="		{ DIV_EQUAL	}
	|	"%="		{ MOD_EQUAL	}
	|	"&="		{ BITAND_EQUAL	}
	|	"|="		{ BITOR_EQUAL	}
	|	"<<="		{ LSHIFT_EQUAL	}
	|	">>="		{ RSHIFT_EQUAL	}
	|	">>"		{ RSHIFT	}
	|	"<<"		{ LSHIFT	}
	|	".."		{ DOT_DOT	}
	|	"~"		{ COMPLEMENT	}
	|	"%"		{ MOD		}
   	|   	'&'             { BITAND       	}
     	|   	"."             { DOT           }
    	|   	'('             { LPAREN        }
    	|   	')'             { RPAREN        }
    	|   	'['             { LBRACK        }
    	|   	']'             { RBRACK        }
    	|   	'{'             { LCURLY        }
    	|   	'}'             { RCURLY        }
 	|	','		{ COMMA		}
    	|   	';'             { SEMICOLON     }
    	|   	':'             { COLON         }
    	|   	'+'             { PLUS          }
    	|   	'-'             { MINUS         }
    	|   	'*'             { TIMES         }
    	|   	'/'             { DIV           }
    	|   	'|'             { BITOR         }
    	|   	'>'             { GT            }
    	|   	'<'             { LT            }
    	|   	'!'             { NOT           }
    	|   	'?'             { QM            }
    	|   eof                 { EOF           }
	|	'"'		{ STRING (str lexbuf) }
    	|   [' ' '\t' ]
        	{ token lexbuf }    (* skip whitespace      *)
    	|   '\n' | '\r' | "\r\n"
		{ 
	  	Misc.lineno := !Misc.lineno+1;
          	Misc.cr_index := (lexeme_start lexbuf) + 1;
	  	Misc.incr lexbuf;
          	token lexbuf  (* skip whitespace *)
		}    
    	|   "//" [^'\n' '\r']*       
		{ token lexbuf }    (* skip // comment      *)
    	|   "/*"
        	{ 
          	comment_start_pos := Lexing.lexeme_start lexbuf;
          	Objects.current_comment := comment lexbuf;
	  	token lexbuf
		}
    	| decimal_literal
		{ INTEGER(int_of_string(lexeme lexbuf)) }
    	| hex_literal
		{ INTEGER(int_of_string(lexeme lexbuf)) }
    	| id                 
		{let str = Lexing.lexeme lexbuf in
		try
                  Hashtbl.find Keywords.keyword_table str
                with Not_found ->
		  (match Progtypes.scan_ident str with
		    Progtypes.IDENT -> IDENT str
		  | Progtypes.STRUCT_TYPE -> STRUCT_TYPE str
		  | Progtypes.ENUM_TYPE -> ENUM_TYPE str
		  | Progtypes.RANGE_TYPE -> RANGE_TYPE str
		  | Progtypes.TIMER_TYPE -> TIMER_TYPE str
		  | Progtypes.FN_NAME -> FN_NAME str
		  | Progtypes.STATE_NAME -> STATE_NAME str)}
    	|   _
        { 
	  raise (Error(Illegal_character ((Lexing.lexeme lexbuf).[0]),
		       Lexing.lexeme_start lexbuf,
		       Lexing.lexeme_end lexbuf))
	}

and comment = parse
    | "*/"                  
	{ "" } 
    | '\n' | '\r' | "\r\n"
        { 
	  Misc.lineno := !Misc.lineno+1; 
          Misc.cr_index := (lexeme_start lexbuf) + 1;
	  Misc.incr lexbuf;
          let cur = Lexing.lexeme lexbuf in
          cur ^ comment lexbuf
	}
    | eof
        { 
          raise (Error (Unterminated_comment, 
			!comment_start_pos,
			!comment_start_pos + 2))
        }
    | _  { let cur = Lexing.lexeme lexbuf in
           cur ^ comment lexbuf }

and str =
  parse	'"' {""}
    | hex_escape
	{let cur = scan_hex_escape (String.sub (Lexing.lexeme lexbuf) 2 2) in
	cur ^ (str lexbuf)}
    | oct_escape
	{let cur = scan_oct_escape (String.sub (Lexing.lexeme lexbuf) 1 3) in
	cur ^ (str lexbuf)}
    | "\\0"
	{(String.make 1 (Char.chr 0)) ^ (str lexbuf)}
    | escape
	{let cur = scan_escape (String.sub (Lexing.lexeme lexbuf) 1 1) in
	cur ^ (str lexbuf)}
    | _	{let cur = Lexing.lexeme lexbuf in cur ^  (str lexbuf)}
(* # <line number> <file name> ... *)
and line =
	parse	'\n'		    {token lexbuf}
	|	blank		    {line lexbuf}
	| decimal_literal           {Misc.lineno :=
				       int_of_string (Lexing.lexeme lexbuf);
				      Misc.cr_index :=
					(lexeme_start lexbuf) + 1;
				      Misc.incr lexbuf;
				     endline lexbuf}
	|	_		    {endline lexbuf}
and endline =
	parse '\n' 		    {token lexbuf}
	|	_		    {endline lexbuf}



{
let init _ = Progtypes.init_lexicon ()
}
