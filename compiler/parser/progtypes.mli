(* --------------------- Program-defined types --------------------- *)

type idtype =
    STRUCT_TYPE | ENUM_TYPE | RANGE_TYPE | TIMER_TYPE | IDENT | FN_NAME | STATE_NAME

val init_lexicon : unit -> unit

val add_struct_type : string -> unit

val add_enum_type : string -> unit

val add_range_type : string -> unit

val add_timer_type : string -> unit

val add_fn_name : string -> unit

val add_state_name : string -> unit

val add_cstate_name : string -> unit

val scan_ident : string -> idtype

val is_internal : string -> bool
val add_internal : string -> unit

val is_user_fct : string -> bool
