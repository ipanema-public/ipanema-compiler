(* $Id$ *)


module B = Objects
module CS = Class_state

(* This phase renames identifiers to avoid
 * collisions with reserved keywords

1. Process and core variables that share name
with reserved keyword generate an error
2. Global variables and constants (that are not system
variables) are renamed if they collide with a reserved
keyword.
3.
The previous variable of the RUNNING state, if any, is designated as RTS_PREV rather than BOSSA_PREV to indicate that it is managed by the RTS
4. Rename parameters of attach to be distinct from existing
identifiers as they are static global
*)

exception LookupErrException

let error (loc,str) =
  (Error.update_error();
   match loc with
     Some lstr -> Printf.printf "%s: %s\n" lstr str
   | None -> Printf.printf "%s\n" str)

let rec lookup_id x = function
    [] -> raise LookupErrException
  | ((y,v)::r) -> if B.ideq(x,y) then v else lookup_id x r

let check env id =
  try lookup_id id env with LookupErrException -> id

let extend_env nm vl env = (nm,vl)::env

let pick_new x env =
  try let _ = lookup_id x env in B.fresh_idx (B.id2c x)
  with LookupErrException -> x

let reserved = (ref [] : B.identifier list ref)

(* ------------------------ Process structure ----------------------- *)

(* Checks whether variable declaration collides
 * with reserved keyword *)
let check_vardecls procdecls =
  List.iter
    (function Ast.VARDECL(ty,id,imported,laazy,_,dexp,attr) ->
      if Aux.member id (!reserved)
      then error(Some(B.loc2c attr),"process field cannot be a reserved word"))
    procdecls

(* --------------------------- Expressions -------------------------- *)

let rec rename env = function
    Ast.VAR(id,ssa,attr) as exp -> Ast.VAR(check env id, ssa,attr)

    | Ast.FIELD(exp,fld,ssa,attr) -> Ast.FIELD(rename env exp,check env fld, ssa,attr)

    | Ast.UNARY(uop,exp,attr) -> Ast.UNARY(uop,rename env exp,attr)

    | Ast.BINARY(bop,exp1,exp2,attr) ->
	Ast.BINARY(bop,rename env exp1,rename env exp2,attr)

    | Ast.TERNARY(exp1,exp2,exp3,attr) ->
	Ast.TERNARY(rename env exp1,rename env exp2, rename env exp3,attr)

    | Ast.PBINARY(bop,exp1,states1,exp2,states2,attr) ->
	Ast.PBINARY(bop,rename env exp1,states1,rename env exp2,states2,attr)

    | Ast.INDR(exp,attr) -> Ast.INDR(rename env exp,attr)

    | Ast.PRIM(f,exps,attr) ->
	Ast.PRIM(rename env f,List.map (rename env) exps,attr)

    | Ast.IN(exp,locexp,x,y,z,crit,attr) ->
	Ast.IN(rename env exp,
	       rename env locexp,x,y,z,crit,attr)

    | Ast.EMPTY(id,l,x,y,attr) ->
	let id = check env id in
	Ast.EMPTY(id,l,x,y,attr)

    | Ast.VALID(exp, o, attr) -> Ast.VALID(rename env exp, o, attr)
    | exp -> exp

(* -------------------------- Declarations -------------------------- *)

(* A global is only renamed if its name is reserved and if the type is not a system type *)
let rename_decl env = function
  Ast.VARDECL(ty,id,imported,laazy,sh,dexp,attr) ->
  let dexp1 = match dexp with
      None -> None
    | Some de_ -> Some (rename env de_)
  in
  Ast.VARDECL(ty,check env id,imported,laazy,sh,dexp1,attr)

let rec rename_global_decls defs env =
  List.map
    (function
	Ast.VALDEF(vdecl,exp,cst,attr1) ->
	  Ast.VALDEF(rename_decl env vdecl, rename env exp,cst,attr1)
      | Ast.UNINITDEF(Ast.VARDECL(ty,id,imported,laazy,sh,dexp,attr) as x,attr1) ->
	  Ast.UNINITDEF(Ast.VARDECL(ty,check env id,imported,laazy,sh,dexp,attr),attr1)
      | Ast.SYSDEF(Ast.VARDECL(ty,id,imported,laazy,sh,dexp,attr),isconst,attr1) as x ->
	  if Aux.member id (!reserved)
	  then error(Some(B.loc2c attr),
		     "system global cannot be a reserved word");
	  x
      | Ast.DUMMYDEF(id,exp,attr) -> (* never of type process *)
	  Ast.DUMMYDEF(id,rename env exp,attr))
   defs

(* A local variable is renamed if it is of type process or if its name is reserved *)
let rename_decls decls env =
  List.fold_left
    (function (env,prev_decls) ->
      function
	  Ast.VALDEF(Ast.VARDECL(B.PROCESS,id,imported,laazy,sh,dexp,attr),exp,cst,attr1) ->
	    let newid = pick_new id env in
	    (extend_env id newid env,
	     Ast.VALDEF(Ast.VARDECL(B.PROCESS,newid,imported,laazy,sh,dexp,attr),
			rename env exp,cst,attr1) :: prev_decls)
	| Ast.VALDEF(vdecl,exp,cst,attr1) ->
	    (env, Ast.VALDEF(rename_decl env vdecl, rename env exp,cst,attr1) :: prev_decls)
	| Ast.UNINITDEF(Ast.VARDECL(B.PROCESS,id,imported,laazy,sh,dexp,attr),attr1) ->
	    let newid = pick_new id env in
	    (extend_env id newid env,
	     Ast.UNINITDEF(Ast.VARDECL(B.PROCESS,newid,imported,laazy,sh,dexp,attr),
			   attr1) :: prev_decls)
	| Ast.UNINITDEF(Ast.VARDECL(ty,id,imported,laazy,sh,dexp,attr),attr1) ->
	    (env,
	     Ast.UNINITDEF(Ast.VARDECL(ty,check env id,imported,laazy,sh,dexp,attr),
			   attr1) :: prev_decls)
	| Ast.SYSDEF(Ast.VARDECL(B.PROCESS,id,imported,laazy,sh,dexp,attr),isconst,attr1) ->
	    let newid = pick_new id env in
	    (extend_env id newid env,
	     Ast.SYSDEF(Ast.VARDECL(B.PROCESS,newid,imported,laazy,sh,dexp,attr),isconst,
			attr1)
	     :: prev_decls)
	| Ast.SYSDEF(Ast.VARDECL(ty,id,imported,laazy,sh,dexp,attr),isconst,attr1) as x ->
	    if Aux.member id (!reserved)
	    then
	      let newid = lookup_id id env in
	      (env,Ast.SYSDEF(Ast.VARDECL(ty,newid,imported,laazy,sh,dexp,attr),isconst,
			      attr1)
	       :: prev_decls)
	    else (env,x::prev_decls)
	| Ast.DUMMYDEF(id,exp,attr) -> (* never of type process *)
	   (env,Ast.DUMMYDEF(id,rename env exp,attr) :: prev_decls)
    )
    (env,[])
    decls

(* ------------------------------ States ---------------------------- *)

let rename_states states env =
  let (env,states) =
    List.fold_left
      (function (newenv,states) ->
	function
	    Ast.PROCESS(CS.RUNNING,shared,id,Ast.BOSSA_PREV previd,vis,attr) ->
	      let id = check newenv id
	      and old_running = B.mkId "old_running" in
	      (extend_env previd old_running newenv,
	       Ast.PROCESS(CS.RUNNING,shared,id,Ast.RTS_PREV old_running,vis,attr)::
	       states)
	  | Ast.PROCESS(cls,shared,id,prev,vis,attr) ->
	      let id = check newenv id in
	      (newenv,Ast.PROCESS(cls,shared,id,prev,vis,attr)::states)
	  | Ast.QUEUE(cls,shared,order,id,vis, po, attr) ->
	      let id = check newenv id in
	      (newenv,Ast.QUEUE(cls,shared,order,id,vis,po, attr)::states))
      (env,[])
      states in
  (env,List.rev states)

(* --------------------------- Statements --------------------------- *)

let rec preprocess_stmt env = function
    Ast.IF(exp,stm1,stm2,attr) ->
      Ast.IF(rename env exp,
	     preprocess_stmt env stm1,preprocess_stmt env stm2,attr)

  | Ast.FOR(id,stid,l,dir,stm,crit,attr) ->
      let new_id = pick_new id env in
      let new_stid =
	match stid with
	  None -> None
	| Some x -> Some (List.map (rename env) x) in
      Ast.FOR(new_id,new_stid,l,dir,
	      preprocess_stmt (extend_env id new_id env) stm,
	      crit,attr)

  | Ast.FOR_WRAPPER(label,stms,attr) -> raise (Error.Error "not generated yet")

  | Ast.SWITCH(exp,cases,default,attr) ->
      Ast.SWITCH(rename env exp,
		 List.map
		   (function Ast.SEQ_CASE(pat,l,stm,attr) ->
		     let pat = List.map (check env) pat in
		     Ast.SEQ_CASE(pat,l,preprocess_stmt env stm,attr))
		   cases,
		 Aux.app_option (preprocess_stmt env) default,
		 attr)

  | Ast.SEQ(decls,stmts,attr) ->
      let (env,new_decls) = rename_decls decls env in
      Ast.SEQ(List.rev new_decls,List.map (preprocess_stmt env) stmts,attr)

  | Ast.RETURN(Some(e),attr) -> Ast.RETURN(Some(rename env e),attr)

  | Ast.MOVE(exp,dst_exp,src,srcs,dst,auto_allowed,state_end,attr) ->
      let id = rename env dst_exp in
      Ast.MOVE(rename env exp,id,src,srcs,dst,auto_allowed,state_end,attr)

  | Ast.MOVEFWD(exp,srcs_dsts,state_end,attr) ->
      Ast.MOVEFWD(rename env exp,srcs_dsts,state_end,attr)

  | Ast.SAFEMOVEFWD(exp,srcs_dsts,stm,state_end,attr) ->
      Ast.SAFEMOVEFWD(rename env exp,srcs_dsts,preprocess_stmt env stm,
		      state_end,attr)

  | Ast.ASSIGN(exp1,exp2,sorted_fld,attr) ->
      Ast.ASSIGN(rename env exp1,rename env exp2,sorted_fld,attr)

  | Ast.PRIMSTMT(f,args,attr) ->
      Ast.PRIMSTMT(rename env f,List.map (rename env) args,attr)

  | stmt -> stmt

(* ------------------------------ Steal ---------------------------- *)

let rename_steal_thread_blk env = function
 ((self, fv, fe), (sv, sv2, se2), (mv1, mv2, mv3, mv4, ms, se, ms2)) ->
   ((self,rename_decl env fv,
     preprocess_stmt env fe),
    (rename_decl env sv,
     rename_decl env sv2,
     preprocess_stmt env se2),
    (rename_decl env mv1,
     rename_decl env mv2,
     Aux.option_apply (rename_decl env) mv3,
     rename_decl env mv4,
     preprocess_stmt env ms,
     rename env se,
     ms2
    ))

let rename_steal_group env = function
  ((self, fv, fe), (sv1, sv2, sv3, se2), se) ->
   ((self,rename_decl env fv,
     preprocess_stmt env fe),
    (rename_decl env sv1,
     rename_decl env sv2,
     rename_decl env sv3,
     preprocess_stmt env se2),
    rename env se
   )

let rename_steal_thread env steal =
  match steal with
    Ast.FLAT_STEAL_THREAD st -> Ast.FLAT_STEAL_THREAD (rename_steal_thread_blk env st)
  | Ast.ITER_STEAL_THREAD (st, until, post) ->
     Ast.ITER_STEAL_THREAD (rename_steal_thread_blk env st,
			    rename env until,
			    List.map (preprocess_stmt env) post)

let rename_steal env steal =
  match steal with
    Ast.STEAL_THREAD s -> Ast.STEAL_THREAD (rename_steal_thread env s)
  | Ast.ITER_STEAL_GROUP (sg, st, post, postgrp) ->
     Ast.ITER_STEAL_GROUP (rename_steal_group env sg,
			   rename_steal_thread env st,
			   List.map (preprocess_stmt env) post,
			   List.map (preprocess_stmt env) postgrp)
  | Ast.FLAT_STEAL_GROUP (sg, st, postgrp) ->
     Ast.FLAT_STEAL_GROUP (rename_steal_group env sg, rename_steal_thread env st,
			   List.map (preprocess_stmt env) postgrp)

let rename_steal_param env (dom, dstg, dst, steal) =
  (* FIXME: Eventually rename dom, dstg and dst ... *)
  (dom, dstg, dst, rename_steal env steal)

(* ------------------------ Ordering criteria ----------------------- *)

let preprocess_criteria criteria env =
  List.map
    (function
	Ast.CRIT_ID(key,order,id,fnid,attr) as crit ->
	  Ast.CRIT_ID(key,order,check env id,fnid,attr)
      )
    criteria

(* ------------------------------ Trace ----------------------------- *)

let preprocess_trace env = function
    Ast.PRETRACE(count,trace_info,attr) ->
      Ast.PRETRACE(count,
		   List.map
		     (function
			 Ast.TRACEEVENTS(events) -> Ast.TRACEEVENTS(events)
		       | Ast.TRACEEXPS(exps) ->
			   let new_exps =
			     List.map
			       (function
				   Ast.TRACEVAR(fldid,id,attr) ->
				     Ast.TRACEVAR(fldid,check env id, attr)
				 | _ -> raise (Error.Error "internal error"))
			       exps in
			   Ast.TRACEEXPS(new_exps)
		       | Ast.TRACETEST(exp) -> Ast.TRACETEST(rename env exp))
		     trace_info,
		   attr)
  | Ast.NOTRACE -> Ast.NOTRACE
  | _ -> raise (Error.Error "internal error")

(* --------------------- Handlers and functions --------------------- *)

let preprocess_handlers handlers env =
  List.map
    (function
	(Ast.EVENT(nm,(Ast.VARDECL(_,id,_,_,_,_,_) as param),stmt,syn,attr)) ->
	  let env = (id,id) :: env
	  in Ast.EVENT(nm,param,preprocess_stmt env stmt,syn,attr))
    handlers

let preprocess_functions functions env =
  let (attach_env,new_functions) =
    List.fold_left
      (function (attach_env,new_functions) ->
	function (Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr)) ->
	  let snm = B.id2c nm in
	  let (new_vars,new_params) =
	    List.split
	      (if snm = "attach"
 	       (* parameters of attach will become static globals, so they
		  need to be disjoint from other variables *)
	      then List.map
		  (function Ast.VARDECL(a,id,b,lz,sh,de,c) as x ->
		    let new_id = B.fresh_idx(B.id2c id) in
		    ((id,new_id),Ast.VARDECL(a,new_id,b,lz,sh,de,c)))
		  params
	      else
		List.map (function Ast.VARDECL(_,id,_,_,_,_,_) as x -> ((id,id),x))
		  params) in
	  let env = new_vars @ env in
	  ((if snm = "attach" then Some new_vars else attach_env),
	   (Ast.FUNDEF(tl,ret,nm,new_params,preprocess_stmt env stmt,inl,attr))::
	   new_functions))
      (None,[]) functions in
  (attach_env,List.rev new_functions)

(* Entry point *)
let preprocess
    (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		   fundecls,valdefs,domains,pcstate,cstates,criteria,
		   trace,handlers,chandlers,ifunctions,functions,attr)) =
  reserved := List.map B.mkId [
                  "old_load";
                "current";
                "_checksum";
                "self";
                "tgt";
		"local_data";
                "fwdres"(* for unblock *);
		"trace_buffer";
                "trace_ctr";
                "trace_wrapped"];
  let env0 = List.map (function x -> (x,B.fresh_idx(B.id2c x))) (!reserved) in
  (* Check no process variables uses reserved keywords *)
  check_vardecls procdecls ;
  (* Check no core variables uses reserved keywords *)
  let vars =  match pcstate with
    Ast.CORE(vars,_,_) -> vars
  | Ast.PSTATE(_) -> [] in
  check_vardecls vars;
  (* Rename constants if they share a name with
   * reserved keyword and they are not a system type *)
  let cstdefs = rename_global_decls cstdefs env0 in
  (* Rename global variables if they share a name with
  * reserved keyword and they are not a system type *)
  let valdefs = rename_global_decls valdefs env0 in
  let (env,states) = (match pcstate with
    Ast.CORE(_, states,_) -> rename_states states env0
    | Ast.PSTATE(states) -> rename_states states env0) in
  (* Rename core states ? *)
  let pcstate_new = (match pcstate with
      Ast.CORE(vars,_, steal) ->
	Ast.CORE(List.map (rename_decl env) vars,states,
		 Aux.option_apply (rename_steal_param env) steal)
  | Ast.PSTATE(_) -> Ast.PSTATE(states)) in
  let (_,functions) = preprocess_functions functions env in
  match preprocess_functions ifunctions env with
    (None,_) -> raise (Error.Error "rename: missing attach")
  | ((Some attach_env),ifunctions) ->
      (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,fundecls,
		     valdefs,domains,pcstate_new,cstates,
		     preprocess_criteria criteria env,
		     preprocess_trace env trace,
		     preprocess_handlers handlers env,
		     preprocess_handlers chandlers env,
		     ifunctions,functions, attr))
