(* Check that functions are not recursive (or mutually recursive).
 * For clarify, this is intentionally pessimistic: it checks all functions, even those that are not currently in use *)

(* TODO: the current approach is very inefficient: instead
 * of computing a global graph, it iterates through every
 * function to check if the function creates a call loop.
 * This means that multiple paths will be explored several
 * times
 *)

module B = Objects
module CS = Class_state

let error ln str =
  Error.update_error();
  Printf.printf "%s: %s \n" (B.loc2c ln) str

let warning ln str =
  Printf.printf "warning: %s: %s \n" (B.loc2c ln) str

let pr = Printf.sprintf

(* Stores mapping from function identifier to function
    * definition *)
let fn_table = (ref [] : (B.identifier * Ast.fundef) list ref)


let check_internals current_calls id =
  let fn_name = (B.id2c id) in
  if Progtypes.is_internal fn_name then
      current_calls
  else
    (* Not an internal function *)
      id::current_calls

(* Returns list of function calls for this
 * expression *)
let rec extract_expcalls current_calls exp =
    match exp with
    Ast.UNARY(_,exp,_) ->
        let inner_calls = extract_expcalls [] exp in
        current_calls@inner_calls
    | Ast.BINARY(_,exp1,exp2,_) ->
            let calls1 = extract_expcalls [] exp1  in
            let calls2 = extract_expcalls [] exp2  in
        current_calls@calls1@calls2
    | Ast.PBINARY(_,exp1,_,exp2,_,_) ->
            let calls1 = extract_expcalls [] exp1  in
            let calls2 = extract_expcalls [] exp2  in
        current_calls@calls1@calls2
    | Ast.INDR(exp,_) ->
            let inner_calls = extract_expcalls [] exp  in
        current_calls@inner_calls
    | Ast.FIRST(exp,_,_,_) ->
            let inner_calls = extract_expcalls [] exp  in
        current_calls@inner_calls
    | Ast.VALID(exp,_,_) ->
            let inner_calls = extract_expcalls [] exp  in
        current_calls@inner_calls
    | Ast.PRIM(exp,parms,attr) ->
            begin match exp with
              Ast.VAR(id,line,_) -> check_internals current_calls id
            | Ast.COUNT(a) | Ast.FNOR(a) ->
               current_calls@(List.flatten
                                (List.map
                                   (function e -> extract_expcalls [] e)
                                   parms))
            | _ -> raise (Error.Error (Printf.sprintf "Incorrect function call %s" (Objects.loc2c attr)))
            end
    | Ast.AREF(exp1,Some(exp2),_) ->
            let calls1 = extract_expcalls [] exp1  in
            let calls2 = extract_expcalls [] exp2  in
            current_calls@calls1@calls2
    | Ast.AREF(exp1,None,_) ->
            let calls1 = extract_expcalls [] exp1  in
            current_calls@calls1
    | Ast.IN(exp,_,_,_,_,_,_) ->
            let inner_calls = extract_expcalls [] exp  in
        current_calls@inner_calls
    | _ -> []

    (* Recursively extracts function calls from a code block *)
let rec extract_fncalls current_calls stmt=
    match stmt with
    Ast.IF(exp,stmt1,stmt2,_) ->
        let expr_calls = extract_expcalls [] exp in
        let stmt1_calls = (extract_fncalls [] stmt1 ) in
        let stmt2_calls = (extract_fncalls [] stmt2 ) in
        current_calls@expr_calls@stmt1_calls@stmt2_calls
    | Ast.FOR(_,exp_list,_,_,stmt,_,_) ->
        let for_exp_calls =
            begin match exp_list with
            Some(exp_list) ->
                let calls =
                    List.fold_left extract_expcalls [] exp_list in
                current_calls@calls
            | None -> []
            end in
        let for_calls = extract_fncalls [] stmt   in
        current_calls@for_exp_calls@for_calls
    | Ast.SEQ(_,stmt,_) ->
            let body_calls = List.fold_left extract_fncalls [] stmt in
        current_calls@body_calls
    | Ast.RETURN(exp,_) ->
        let expr_calls =
            begin match exp with
            Some(exp) ->  extract_expcalls [] exp
            | None -> []
            end in
        current_calls@expr_calls
    | Ast.ASSIGN(exp1,exp2,_,_) ->
            let exp1_calls = (extract_expcalls [] exp1 ) in
            let exp2_calls = (extract_expcalls [] exp2 ) in
        current_calls@exp1_calls@exp2_calls
    | Ast.PRIMSTMT(exp,_,attr) ->
            (match exp with
	       Ast.VAR(id,line,_) -> check_internals current_calls id
	     | _ -> raise (Error.Error (Printf.sprintf "Incorrect function call %s" (Objects.loc2c attr)))
            )
    | _ -> current_calls


(* Computes which functions are reachable from each function *)
let rec reachability env (Ast.FUNDEF(_,ret,nm,params,stmt,inl,attr)) =
    let env = nm::env in
    let fn_calls = extract_fncalls [] stmt in
    (* Now check if environment intersects with call
     * if yes, send error message, and return
     * Otherwise, go through each function call*)
    let intersection = Aux.intersect env fn_calls in
    if (List.length intersection == 0)
    then
       (* Generate list of function defs from function ids *)
      let deref l key  =
	try
	  List.assoc key l
	with e ->
	  failwith
	    (Printf.sprintf "Exception: function '%s' not defined !\n"
			    (B.id2c key))
      in
       let fn_defs = List.map (deref !fn_table) fn_calls  in
        List.fold_left reachability env fn_defs
    else
       (* Recursive call TODO fix error message*)
        let str = "Function: "^B.id2c(nm)  in
        let _ = error attr "Recursive call"  in []


(* Entry point. Checks if functions are mutually
 * recursive by checking whether there are reachable
 * from themselves *)
let recursive
    (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,
		   procdecls,fundecls,valdefs,
		     domains,pcstate,cstates, criteria,trace,
		     phandlers,chandlers,ifunctions,functions,attr))  =
    let all_functions = ifunctions@functions in
    fn_table:= List.map
		 (function (Ast.FUNDEF(_,_,nm,_,_,_,_) as fn) ->
			   (nm,fn)) all_functions ;
    let _ =  List.map (reachability []) all_functions in ();
