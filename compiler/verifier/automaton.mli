(* A state can be a core state or a process state. An automaton can have
 * a single set *)
type states =
    CLASS of Class_state.classname
    | CORE_CLASS of Class_state.corename

(* Edge consists of a source, a destination, and the list
 * of events that can cause that transition *)
type edge =
    EDGE of states * (* source *)
            states * (* destination *)
            string list (* events that can induce the transition *)

type automaton =
    AUT of string   (* Name of automaton *)
        * edge list (* Edges and nodes *)


(************** Environment *****************************************)

(* This automaton describes transitions that are valid only
 * for the target process or core in the event, the exception
 * to that is the schedule event, where a process (we don't know which one,
 * is allowed to be scheduled*)
val process_transitions : automaton ref
val core_transitions : automaton ref

val init_process_automaton: 'a -> unit
val init_core_automaton: 'a -> unit

val verify_consistent : automaton -> bool -> unit

val verify_event: Ast.event_decl -> (bool * edge list * string) option
