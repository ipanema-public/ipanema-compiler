(* $Id$ *)

module B = Objects
module CS = Class_state
open Format

(* -------------------- Miscellaneous operations -------------------- *)

let print_comments = ref true (*  print with analysis comments? *)

let rec print_between between_fn elem_fn = function
    [] -> ()
  | [x] -> elem_fn x
  | x :: xs -> (elem_fn x; between_fn(); print_between between_fn elem_fn xs)

let pp_id id = print_string(B.id2c(id))

let pp_comma _ = (print_string ","; print_space())

let nl_ifnonempty = function
    x::xs -> force_newline()
  | _ -> ()

(* -------------------- Information about states -------------------- *)

let pp_vis = function
    CS.PUBLIC -> ()
  | CS.PRIVATE -> print_string "private "

(*FIXME shared vs not shared *)
let pp_states states =
  (print_string "processes = {"; force_newline(); print_string "  "; open_box 0;
   print_between force_newline
     (function
	 Ast.QUEUE(CS.NOWHERE,_,_,id,_,_,attr) -> ()(* don't print nowhere state *)
       | Ast.QUEUE(clsname,_,CS.EMPTY,id,vis,po,attr) ->
	   (print_string (CS.safe_class2c(clsname)); print_space(); pp_id(id);
	    print_string ";")
       | Ast.QUEUE(clsname,_,queue_typ,id,vis,po,attr) ->
	   (print_string (CS.safe_class2c(clsname)); print_space(); pp_id(id);
	    print_space(); print_string ":"; print_space();
	    pp_vis vis;
	    (match queue_typ with
	    | CS.FIFO -> print_string "fifo "
	    | CS.SELECT(CS.NODEFAULT) -> print_string "select "
	    | CS.SELECT(CS.LIFODEFAULT) -> print_string "select lifo "
	    | CS.SELECT(CS.FIFODEFAULT) -> print_string "select fifo "
	    | _ -> raise (Error.Error("printing internal error")));
	    print_string "queue";
	    print_string ";")
       | Ast.PROCESS(clsname,_,id,previd,vis,attr) ->
	   (print_string (CS.safe_class2c(clsname)); print_space(); pp_id(id);
	    print_space(); print_string ":"; print_space();
	    pp_vis vis;
	    print_string "process";
	    (match previd with
	      Ast.NO_PREV -> ()
	    | Ast.BOSSA_PREV x ->
		pp_comma(); print_string "previous"; print_space(); pp_id(x)
	    | Ast.RTS_PREV x ->
		pp_comma(); print_string "previous"; print_space(); pp_id(x));
	    print_string ";"))
     states; close_box(); force_newline();
   print_string "}")

let pp_cstates cstates =
  (print_string "cores = {"; force_newline(); print_string "  "; open_box 0;
   print_between force_newline
     (function
     Ast.CSET(clsname,id,attr) ->
     (print_string (CS.safe_coreclass2c(clsname)); print_space();
      print_string "set<core>";print_space(); pp_id(id);
      print_string ";")
       | Ast.CSTATE(clsname, id, attr) ->
	  (print_string (CS.safe_coreclass2c(clsname)); print_space();
	   print_string "core";print_space(); pp_id(id);
	   print_string ";")
     )
     cstates; close_box(); force_newline();
   print_string "}"; force_newline(); force_newline())
(* --------------------------- Expressions -------------------------- *)

let pp_id id = print_string (B.id2c(id))

let rec pp_decl (Ast.VARDECL(ty,id,imported,lz,sh,exp,attr)) =
  (if imported then print_string "system ";
   if sh = CS.SHARED then print_string "shared ";
   match exp with
     None -> (print_string (B.type2c(ty)); print_string " "; pp_id(id))
   | Some expr -> (
     (if lz then print_string "lazy ");
     print_string (B.type2c(ty)); print_string " "; pp_id(id));
     print_string " = "; pp_exp(expr)
  );
  if !print_comments then
    let comments = B.com attr in
    match comments with
      None -> ()
    | Some s -> print_string ("/* "^s^" */")

and pp_ucexp = function
    Ast.SELF _ -> print_string "self"
  | Ast.DISTANCE _ -> print_string "distance"
  | Ast.COUNT  _ -> print_string "count"
  | Ast.FNOR _   -> print_string "or"
  | Ast.SUM    _ -> print_string "sum"
  | Ast.MIN    _ -> print_string "min"
  | Ast.MAX    _ -> print_string "max"
  | Ast.INT(n,attr) -> print_int n

  | Ast.VAR(id,_,attr) -> pp_id id

  | Ast.FIELD(exp,fld,_,attr) -> (pp_exp exp; print_string "."; pp_id fld)

  | Ast.CAST(typ,exp,attr) -> pp_exp exp

  | Ast.TYPE(typ,attr) -> print_string (B.type2c typ)

  | Ast.LCORE(obj,exp, cexp, attr) ->
     begin
       match cexp with
	 None -> print_string "local("; print_string obj ; print_string ")." ; pp_exp exp
       | Some c ->
	  print_string "remote("; print_string obj ;
	 print_string ", " ; pp_exp c;
	 print_string ")." ; pp_exp exp
     end

  | Ast.BOOL(true,attr) -> print_string "true"
  | Ast.BOOL(false,attr) -> print_string "false"

  | Ast.PARENT(attr) -> print_string "parent"

  | Ast.UNARY(Ast.NOT,exp,attr) ->
     (print_string "!"; print_paren exp)

  | Ast.UNARY(Ast.COMPLEMENT,exp,attr) ->
     (print_string "~"; print_paren exp)

  | Ast.BINARY(bop,exp1,exp2,attr)
    | Ast.PBINARY(bop,exp1,_,exp2,_,attr) ->
     (print_paren exp1; pp_bop bop; print_paren exp2)

  | Ast.TERNARY(exp1,exp2, exp3,attr) ->
      (print_paren exp1; print_string " ? ";
       print_paren exp2; print_string " : ";
       print_paren exp3)

  | Ast.INDR(exp,attr) ->
     (print_string "*"; print_paren exp)

  | Ast.SELECT(_,attr) ->
     print_string "select()"

  | Ast.FIRST(exp,_,_,attr) -> (*FIXME consider order *)
     print_string "first("; pp_exp exp; print_string ")"

  | Ast.VALID(exp,_,attr) ->
     print_string "valid("; pp_exp exp; print_string ")"

  | Ast.EMPTY(id,_,_,insrc,attr) when not(!print_comments) ->
     print_string "empty("; pp_id id; print_string ")"

  | Ast.EMPTY(id,_,B.TRUE,insrc,attr) ->
     (print_string "empty_true("; pp_id id; print_string ")";
      print_string (Printf.sprintf "/* %s: insrc: %b */"
			           (B.loc2c attr) insrc))

  | Ast.EMPTY(id,_,B.FALSE,insrc,attr) ->
     (print_string "empty_false("; pp_id id; print_string ")";
      print_string (Printf.sprintf "/* %s: insrc: %b */"
			           (B.loc2c attr) insrc))

  | Ast.EMPTY(id,_,B.BOTH,_,attr) ->
     (print_string "empty_both("; pp_id id; print_string ")")

  | Ast.EMPTY(id,_,B.UNK,insrc,attr) ->
     (print_string "empty_unknown("; pp_id id; print_string ")";
      print_string (Printf.sprintf "/* %s: insrc: %b */"
			           (B.loc2c attr) insrc))

  | Ast.PRIM(f,args,attr) ->
     (print_paren f; print_string "(";
      open_box 0; print_between pp_comma pp_exp args;
      close_box(); print_string ")")

  | Ast.SCHEDCHILD(arg,procs,attr) ->
     (print_string "scheduler("; open_box 0; pp_exp arg; close_box();
      print_string ")")

  | Ast.SRCONSCHED(attr) -> print_string "srcOnSched()"

  | Ast.ALIVE(_,_,attr) when not(!print_comments) ->
     print_string "tgtAlive()"

  | Ast.ALIVE(B.TRUE,_,attr) -> print_string "tgtAlive_true()"

  | Ast.ALIVE(B.FALSE,_,attr) -> print_string "tgtAlive_false()"

  | Ast.ALIVE(B.BOTH,_,attr) -> print_string "tgtAlive_both()"

  | Ast.ALIVE(B.UNK,_,attr) -> print_string "tgtAlive_unknown()"

  | Ast.AREF(exp1,Some(exp2),_) ->
     (pp_exp exp1; print_string "["; pp_exp exp2; print_string "]")

  | Ast.AREF(exp1,None,_) ->
     (pp_exp exp1)

  | Ast.IN(exp,locexp,_,_,_,_,attr) when not(!print_comments) ->
     (print_paren exp; print_string " in "; pp_exp locexp)

  | Ast.IN(exp,locexp,_,B.TRUE,_,_,attr) ->
     (print_paren exp; print_string " in_true "; pp_exp locexp)

  | Ast.IN(exp,locexp,_,B.FALSE,_,_,attr) ->
     (print_paren exp; print_string " in_false "; pp_exp locexp)

  | Ast.IN(exp,locexp,_,B.BOTH,_,_,attr) ->
     (print_paren exp; print_string " in_both "; pp_exp locexp)

  | Ast.IN(exp,locexp,_,B.UNK,_,_,attr) ->
     (print_paren exp; print_string " in_unk "; pp_exp locexp)

  | Ast.MOVEFWDEXP(exp,state_end,attr) ->
     (open_box 2; pp_exp exp; print_string " => forwardImmediate()";
      close_box())

  | Ast.INT_STRING (s, a) ->
     print_string "\""; print_string s; print_string "\""

  | Ast.INT_NULL a ->
     print_string "NULL"

  | Ast.PFIRST _
  | Ast.CFIRST _ -> failwith  "Unexpected ast node"

and pp_exp exp =
  pp_ucexp exp;
  match B.com (Ast.get_exp_attr exp) with
    None -> ()
  | Some(str) -> print_string " /* "; print_string str; print_string " */"

(* eventually, compare the precedence of the expression with the precedence
of the context, as done for devil *)
and print_paren exp =
  match exp with
    Ast.BINARY  _
  | Ast.PBINARY _
  | Ast.TERNARY _
  | Ast.IN      _ ->
     (print_string "("; pp_exp exp; print_string ")")

  | _ -> pp_exp exp

and pp_bop op = print_string (" "^(Ast.bop2c op)^" ")

(* ------------------ Information about structures ------------------ *)

let pp_procdecls procdecls =
  (print_string "process = {"; force_newline(); print_string "  "; open_box 0;
   print_between force_newline (function x -> pp_decl x; print_string ";")
     procdecls; close_box(); force_newline();
   print_string "}"; force_newline(); force_newline())

(*  ----------------------- User-declared types ---------------------- *)

let rec pp_typedefs typedefs =
  List.iter
    (function
	Ast.STRUCTDEF(id,decls,attr) ->
	    pp_id id; print_string " struct {";
	    force_newline(); print_string "  "; open_box 0;
	    print_between force_newline
	      (function x -> pp_decl x; print_string ";")
	      decls;
	    close_box(); force_newline();
	    print_string "}"; force_newline()
    | Ast.ENUMDEF(id,idlist,attr) ->
	  (print_string "enum"; print_space(); pp_id(id);
	   print_space(); print_string "=";
	   print_space(); print_string "{";
	     print_between pp_comma pp_id idlist;
	   print_string "}"; force_newline();)
	| Ast.ENUMVALDEF(id,idlist,attr) ->
	  (print_string "enum"; print_space(); pp_id(id);
	   print_space(); print_string "=";
	   print_space(); print_string "{";
	   print_between pp_comma
	     (function (id,exp) -> pp_id id; print_string " = "; pp_exp exp)
	     idlist;
	   print_string "}"; force_newline();)
	| Ast.RANGEDEF(id,exp1,exp2,attr) ->
	    (print_string "enum"; print_space(); pp_id(id);
	     print_space(); print_string "=";
	     print_space(); print_string "[";
	     pp_exp exp1; print_space(); print_string ".."; print_space();
	     pp_exp exp2; print_string "]"; print_string ";";
	     force_newline())
	| Ast.TIMERDEF(id,decls,attr) ->
	    pp_id id; print_string " timer {";
	    force_newline(); print_string "  "; open_box 0;
	    print_between force_newline
	      (function x -> pp_decl x; print_string ";")
	      decls;
	    close_box(); force_newline();
	    print_string "}"; force_newline())
    typedefs; nl_ifnonempty(typedefs)

(* ----------------------- External functions ---------------------- *)

let pp_fundecls fundecls =
  open_hbox ();
  List.iter
    (function
	Ast.FUNDECL(tl,ret,nm,params,attr) ->
	if tl = Ast.TRYLOCK then print_string "try ";
	print_string (B.type2c(ret)); print_string " ";
	print_string (B.id2c(nm)); print_string "(";
	print_between pp_comma (function x -> print_string(B.type2c(x)))
		      params;
	print_string ");"; force_newline())
    fundecls;
  close_box ();nl_ifnonempty(fundecls)

(* -------------------------- Declarations ------------------------- *)

let rec pp_def = function
    Ast.VALDEF(decl,exp,isconst,attr) ->
      (open_box 2;
       if isconst=Ast.CONST then (print_string "const"; print_space());
       pp_decl decl; print_string " = ";
       pp_exp exp; print_string ";"; close_box(); force_newline())
  | Ast.UNINITDEF(decl,attr) ->
      (open_box 2;
       pp_decl decl; print_string ";"; close_box(); force_newline())
  | Ast.SYSDEF(decl,isconst,attr) ->
      (open_box 2;
       pp_decl decl; print_string ";"; close_box(); force_newline())
  | Ast.DUMMYDEF(id,exp,attr) ->
      (open_box 2; print_string "???"; print_space(); pp_id(id);
       print_string " = "; pp_exp exp; print_string ";";
       close_box(); force_newline())

let pp_defs defs = (List.iter pp_def defs; nl_ifnonempty(defs))

(* --------------------------- Statements -------------------------- *)

let rec pp_stmt = function
    Ast.IF(exp,stmt1,stmt2,attr) ->
      (print_string "if ("; pp_exp exp;
       print_string ") "; pp_seq stmt1 attr;
       match stmt2 with
	 Ast.SEQ([],[],_) -> ()
       | _ -> (force_newline();
	       print_string "else"; print_space(); pp_seq stmt2 attr))

    | Ast.FOR(id,state_id,_,dir,stmt,_,attr) ->
	(print_string "foreach ("; pp_id(id);
	 (match state_id with
	   None -> ()
	 | Some(state_ids) ->
	     (print_string " in ";
	      print_between pp_comma pp_exp state_ids));
	 print_string ") "; pp_seq stmt attr)

    | Ast.FOR_WRAPPER(label,stms,attr) ->
	print_string "/* an exploded for loop */"; force_newline();
	print_between force_newline pp_stmt stms; force_newline();
	print_string "/* end of an exploded for loop */"

    | Ast.SWITCH(exp,cases,default,attr) ->
	(print_string "switch "; pp_exp exp;
	 print_string " in "; pp_cases cases default)

    | (Ast.SEQ(decls,stmts,attr) as x) -> pp_seq x attr

    | Ast.RETURN(None,attr) ->
	print_string "return;"

    | Ast.RETURN(Some(exp),attr) ->
	(open_box 2; print_string "return "; pp_exp exp;
	 print_string ";"; close_box())

    | Ast.STEAL(exp,attr) ->
	(open_box 2; print_string "steal_for("; pp_exp exp;
	 print_string ");"; close_box())

    | Ast.MOVE(exp,state,_,srcs,_,_,_,attr) ->
	(open_box 2; pp_exp exp;
	 print_string " /* ";
	 print_between pp_comma
	   (function src ->
	     print_string (CS.state2c src))
	   srcs;
	 print_string " */";
	 print_string " => ";
	 pp_exp state; print_string ";"; close_box())

    | Ast.MOVEFWD(exp,src_dests,_,attr) ->
	(open_box 2; pp_exp exp; print_string " => forwardImmediate();";
	 if !print_comments
	 then
	   begin
	     print_string "/* ";
	     print_between pp_comma
	       (function (src,dst) ->
		 print_string (CS.state2c src);
		 print_string "->";
		 print_string (CS.state2c dst))
	       src_dests;
	     print_string " */"
	   end;
	   close_box())

    | Ast.SAFEMOVEFWD(exp,src_dsts,stm,_,attr) ->
	(open_box 2;
	 print_string "if(";
	 pp_exp exp;
	 print_string ") ";
	 pp_exp exp;
	 print_string " => forwardImmediate();";
	 if !print_comments
	 then
	   begin
	     print_string "/* ";
	     print_between pp_comma
	       (function (src,dst) ->
		 print_string (CS.state2c src);
		 print_string "->";
		 print_string (CS.state2c dst))
	       src_dsts;
	     print_string " */"
	   end;
	 force_newline();
	 print_string "else ";
	 pp_seq stm attr;
	 close_box())

    | Ast.ASSIGN(expl,expr,sorted_fld,attr) ->
	(open_box 2;
	 pp_exp expl; print_space(); print_string "="; print_space();
	 pp_exp expr; print_string ";";
	 if !print_comments
	 then
	   begin
	     print_string "/* ";
	     if sorted_fld then print_string "true" else print_string "false";
	     print_string " */"
	   end;
	 close_box())

    | Ast.DEFER(attr) -> print_string "deferEvent();"
    | Ast.BREAK(attr) -> print_string "break;"
    | Ast.CONTINUE(attr) -> print_string "continue;"
    | Ast.PRIMSTMT(f,args,attr) ->
	(print_paren f; print_string "(";
	 open_box 0;
	 print_between pp_comma pp_exp args; close_box(); print_string ");")

    | Ast.ERROR(str,attr) ->
	print_string "error(\""; print_string str; print_string "\");"

    | Ast.ASSERT(exp,_) ->
	(open_box 2; print_string "assert("; pp_exp exp; print_string ");";
	 close_box())

and pp_seq s attr =
  let (decls,stmts) =
    match s with
      Ast.SEQ(d,s,_) -> (d,s)
    | _ -> ([],[s]) in
  (print_string "{";
   (* print_string "/* "; print_int (B.line attr); print_string " */"; *)
   force_newline(); print_string "  "; open_box 0;
   (match decls with
     [] -> ()
   | x::xs -> pp_defs decls);
   print_between force_newline pp_stmt stmts;
   close_box(); force_newline(); print_string "}")

and pp_cases cases default =
  (print_string "{"; force_newline(); print_string "  "; open_box 0;
   print_between force_newline
     (function Ast.SEQ_CASE(idlist,_,stmt,attr) ->
       (print_string "case ";
	print_between pp_comma pp_id idlist;
	print_string ": "; pp_seq stmt attr;))
     cases;
   close_box(); force_newline();
   (match default with
     None -> ()
   | Some x -> pp_stmt x);
   print_string "}")

(* ------------------------ Steal block ----------------------- *)
let pp_migration (busiest, thief, group, task, stmt, stop, update) =
  print_string "migrate_from_to {";
  print_cut ();
  pp_stmt stmt;
  print_cut (); print_string "-----------"; print_cut ();
  pp_exp stop;
  print_cut (); print_string "-----------"; print_cut ();
  pp_stmt update;
  print_string "}";
  force_newline ()

let pp_steal_blk ((thief, vfilter, sfilter), (vselect, e, sselect), migrcond) =
  (* Fix PP of filter and select phases *)
  pp_migration migrcond

let pp_steal_thread steal =
  match steal with
    Ast.FLAT_STEAL_THREAD st
  | Ast.ITER_STEAL_THREAD (st, _, _) ->
     (* Fix missing arguments: _ * until * post *)
     pp_steal_blk st

(* Fix missing arguments *)
let pp_steal steal =
  match steal with
    Ast.STEAL_THREAD s -> pp_steal_thread s
  | Ast.ITER_STEAL_GROUP (sg, st, post, postgrp) ->
     pp_steal_thread st
  | Ast.FLAT_STEAL_GROUP (sg, st, postgrp) ->
     pp_steal_thread st

let pp_stealparam (dom, dstg, dst, steal) =
  print_string ("Domaine: "^dom); force_newline ();
  print_string ("Destination group: "^dstg); force_newline ();
  print_string ("Destionation core: "^dst); force_newline ();
  pp_steal steal

let pp_pcstate pcstate =
  match pcstate with
      Ast.CORE(cv, pv, _steal) ->
	open_vbox 0;
	print_string "core = {";print_cut();
	open_vbox 2; print_string "  ";
	List.iter (fun v -> pp_decl v; print_string ";"; print_cut()) cv;
	print_cut();
	pp_states pv;
	close_box ();
	print_cut();
        (
          match _steal with
            None -> ()
          | Some steal -> pp_stealparam steal
        );
	print_string "}";
	print_cut();
	close_box ()

    | Ast.PSTATE(pv) ->
	pp_states pv


(* ------------------------ Ordering criteria ----------------------- *)

let rec pp_criteria criteria =
  (print_string "ordering_criteria"; print_space(); print_string "=";
   print_space(); print_string "{";
   print_between pp_comma pp_crit criteria;
   print_string "}"; force_newline(); force_newline())

and pp_crit = function
    Ast.CRIT_ID(key,order,id,_,attr) ->
      (pp_key key; pp_crit_order order; pp_id(id))
  | _ -> print_string "TODO" (*FIXME Add support for pp_crit_expr*)

and pp_key = function
    Ast.KEY(_,_) -> print_string "key "
  | Ast.NOKEY    -> ()

and pp_crit_order = function
    Ast.LOWEST ->  print_string "lowest "
  | Ast.HIGHEST -> print_string "highest "

(* --------------------- Handlers and functions --------------------- *)

(* parser should check that event names are ok *)
let rec pp_handlers handlers =
  let param = match handlers with
    (Ast.EVENT(nm,param,stmt,_,attr) :: _) -> param
  | _ -> raise (Error.Error "internal error: handler list must not be empty")
  in print_string "handler (";
  pp_decl param;
  print_string ") {"; force_newline(); print_string "  "; open_box 0;
  print_between (function _ -> force_newline(); force_newline())
    (function Ast.EVENT(nm,param,stmt,syn,attr) ->
      (print_string "On "; if syn then print_string "synchronized ";
       print_string (Ast.event_name2c(nm));
       print_string " "; pp_seq stmt attr))
    handlers;
  close_box(); force_newline();
  print_string "}"; force_newline(); force_newline()

let pp_ifunctions functions =
  print_string "interface = {";
  force_newline(); print_string "  "; open_box 0;
  print_between (function _ -> force_newline(); force_newline())
    (function Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr) ->
       (if tl =Ast.TRYLOCK then print_string "try ";
	print_string (B.type2c(ret)); print_space (); pp_id(nm);
	print_string "(";
	print_between pp_comma pp_decl params;
	print_string ")"; print_space(); pp_seq stmt attr))
    functions;
  close_box(); force_newline();
  print_string "}"; force_newline(); force_newline()

let pp_functions functions =
  open_box 0;
  print_between (function _ -> force_newline(); force_newline())
    (function Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr) ->
      (if tl = Ast.TRYLOCK then print_string "try ";
       print_string (B.type2c(ret)); print_space (); pp_id(nm);
       print_string "(";
       print_between pp_comma pp_decl params;
       print_string ")"; print_space(); pp_seq stmt attr))
    functions;
  close_box(); force_newline(); force_newline()

(* -------------------------- entry points --------------------------- *)

let pretty_print comments out
    (Ast.SCHEDULER(nm,
		   cstdefs,enums,dom,grp,procdecls,
		   fundecls,valdefs,domains,pcstate,cstates,criteria,
		   trace,handlers,chandlers,ifunctions,functions,attr)) =
  print_comments := comments;
  set_formatter_out_channel (open_out out);
  set_margin 80;
  print_string "scheduler ";
  print_string nm; print_string " = {"; force_newline(); print_string "  ";
  open_vbox 0;
  pp_defs cstdefs;
  pp_typedefs enums;
  pp_procdecls procdecls;
  pp_fundecls fundecls;
  pp_defs valdefs;
  pp_pcstate pcstate;
  print_cut();
  pp_cstates cstates;
  pp_criteria criteria;
  pp_handlers handlers;
  pp_handlers chandlers;
  pp_ifunctions ifunctions;
  pp_functions functions;
  close_box(); force_newline();
  print_string "}";
  print_newline()

let pretty_print_exp expr =
  set_formatter_out_channel stderr;
  set_margin 80;
  print_string "Context:";
  print_newline();
  open_box 2;
  pp_exp expr;
  close_box ();
  print_newline()
