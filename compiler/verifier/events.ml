(* $Id$ *)

module B = Objects
module CS = Class_state
module ET45IP = Events_types45_ip
module VB = Vobjects

exception LookupErrException

type entry = CS.classname * VB.vtype * VB.modified

type type_rule =
    B.isquasi * (entry list * CS.classname list) list * entry list list


let pevents = (ref [] : Ast.event_name list ref)
let cevents = (ref [] : Ast.event_name list ref)

let pevent_specifications() =
 match !B.config with
   B.LINUX45-> ET45IP.pevent_specifications()

let cevent_specifications() =
 match !B.config with
   B.LINUX45-> ET45IP.cevent_specifications()

let interface_specifications _ =
  match !B.config with
   B.LINUX45 -> ET45IP.interface_specifications()

let init_pevents_env() = 
    pevents := pevent_specifications()

let init_cevents_env() = 
    cevents := cevent_specifications()
    
let pevent_specifications() =
 match !B.config with
   B.LINUX45-> ET45IP.pevent_specifications()

let cevent_specifications() =
 match !B.config with
   B.LINUX45-> ET45IP.cevent_specifications()

let interface_specifications _ =
  match !B.config with
   B.LINUX45 -> ET45IP.interface_specifications()
