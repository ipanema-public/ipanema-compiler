(* $Id$ *)

module B = Objects
module CS = Class_state

let error (line,str) =
  (Error.update_error();
   if (line > 0)
   then Printf.printf "line %d: %s\n" line str
   else Printf.printf "%s\n" str)

(* Add code to compute scheduler state to the end of each handler *)

let make_sched_state_code attr mkRET =
  let line = B.line attr in
  let empty_running =
    Ast.EMPTY(B.mkId("RUNNING"),[],B.UNK,false,attr)
  and empty_ready =
    Ast.EMPTY(B.mkId("READY"),[],B.UNK,false,attr) in
  Ast.mkIF(empty_running,
	   Ast.mkIF(empty_ready,mkRET "BLOCKED",mkRET "READY",line),
	   mkRET "RUNNING",line)

let sched_state_code attr =
  let line = B.line attr in
  make_sched_state_code attr
    (function str -> Ast.mkRETURN(Ast.mkVAR(str,[],line),line))

(* ------------------ Code to end persistent timers ----------------- *)

let end_persistent_timer nm persistent_timers stm attr =
  try
    let snm = Ast.event_name2c nm in
    let (timer_id,ty) =
      List.find
	(function (pt,ty) -> snm = "unblock.timer."^(B.id2c pt))
	persistent_timers in
    let start =
      match ty with
	None -> None
      |	Some ty ->
	Some(Ast.VALDEF(Ast.VARDECL(B.STRUCT(ty),timer_id,false,false,CS.UNSHARED,None,attr),
              Ast.VAR(B.mkId "local_data",[],attr),Ast.CONST,attr)) in
    let finish =
      Ast.IF (Ast.UNARY(Ast.NOT,Ast.ALIVE(B.UNK,false,attr),attr),
	      Ast.SEQ([],
              (Ast.PRIMSTMT(Ast.VAR(B.mkId("delete_bossa_timer"),[],attr),
                    [Ast.VAR(B.mkId "self",[],attr)],
                    attr)):: [],attr),
	      Ast.SEQ([],[],attr),
	      attr) in
    Some (start,finish)
  with Not_found -> None

(* --------------------- Add to errors positions -------------------- *)

let rec add_stm f stm =
  match f stm with
    Some s -> s
  | None ->
      (match stm with
	Ast.IF(exp,stm1,stm2,attr) ->
	  Ast.IF(exp,add_stm f stm1,add_stm f stm2,attr)
      | Ast.SWITCH(exp,cases,default,attr) ->
	  Ast.SWITCH(exp,
		     List.map
		       (function Ast.SEQ_CASE(pat,states,stm1,attr) ->
			 Ast.SEQ_CASE(pat,states,add_stm f stm1,attr))
		       cases,
		     Aux.app_option (add_stm f) default,
		     attr)
      | Ast.SEQ(decls,stms,attr) ->
	  Ast.SEQ(decls,List.map (add_stm f) stms,attr)
      | Ast.FOR(index,state,state_info,dir,stm1,crit, attr) ->
	  Ast.FOR(index,state,state_info,dir,add_stm f stm1, crit, attr)
      | Ast.FOR_WRAPPER(label,stm1,attr) ->
	  Ast.FOR_WRAPPER(label,List.map (add_stm f) stm1,attr)
      | stm -> stm)

let add_error stm curstm =
  add_stm
    (function
	Ast.ERROR(str,attr) as stm1 ->
	  Some(Ast.SEQ([],[stm1;stm],attr))
      |	_ -> None)
    curstm

let add_tail stm curstm =
  add_stm
    (function
	Ast.RETURN(None,attr) -> Some(stm)
      |	_ -> None)
    curstm

let add_before_return stm curstm =
  add_stm
    (function
	Ast.RETURN(_,attr) as stm1 -> Some(Ast.SEQ([],[stm;stm1],attr))
      |	_ -> None)
    curstm

(* ----- Ensure there every control-flow path ends with a return ---- *)

let rec drop_past_return tail return_ok = function
    Ast.IF(exp,stm1,stm2,attr) ->
      Ast.IF(exp,
	     drop_past_return tail true stm1,
	     drop_past_return tail true stm2,
	     attr)
  | Ast.SWITCH(exp,cases,default,attr) ->
      Ast.SWITCH(exp,
		 List.map
		   (function Ast.SEQ_CASE(pat,states,stm1,attr) ->
		     Ast.SEQ_CASE(pat,states,drop_past_return tail true stm1,
				  attr))
		   cases,
		 Aux.app_option (drop_past_return tail true) default,
		 attr)
  | Ast.SEQ(decls,stms,attr) ->
      (match List.rev stms with
	[] ->
	  Ast.SEQ(decls,(if tail then [Ast.RETURN(None,attr)] else []),attr)
      | (last::rest) ->
	  Ast.SEQ(decls,
		  List.rev
		    ((drop_past_return tail return_ok last) ::
		     (List.map (drop_past_return false false) rest)),
		  attr))
  | Ast.FOR(index,state,state_info,dir,stm1,crit, attr) ->
      let stm =
	Ast.FOR(index,state,state_info,dir,drop_past_return false true stm1, crit,
		attr) in
      if tail then Ast.SEQ([],[stm;Ast.RETURN(None,attr)],attr) else stm
  | Ast.FOR_WRAPPER(label,stms,attr) ->
      let stm =
	Ast.FOR_WRAPPER(label,List.map (drop_past_return false true) stms,
		attr) in
      if tail then Ast.SEQ([],[stm;Ast.RETURN(None,attr)],attr) else stm
  | Ast.RETURN(_,attr) as x ->
      if not return_ok
      then
	raise (Error.Error (Printf.sprintf "%s: dead code following return"
			      (B.loc2c attr)));
      x
  | Ast.ERROR(_,_) as x -> x
  | stm ->
      let attr = Ast.get_stm_attr stm in
      if tail then Ast.SEQ([],[stm;Ast.RETURN(None,attr)],attr) else stm

(* ----------------------------- States ----------------------------- *)

(* Add a "nowhere" state, so that being not yet in the system is made
explicit.  This state is discarded during postprocessing, in compile_misc.ml *)

let add_nowhere states =
  let nowhere = B.fresh_idx "nowhere" in
  (Ast.QUEUE(CS.NOWHERE,CS.UNSHARED,CS.FIFO,nowhere,CS.PRIVATE,None, B.mkAttr(-1))) :: states (*FIXME: is it shared or unshared?*)

(*FIXME: Remove *)
let add_terminated states = states

(* --------------------- Handlers and functions --------------------- *)

(* Add an additional compute_state event *)
let preprocess_handlers handlers =
(* (* Disabled for ipanema ; To remove later ? *)
  let handlers =
    (match handlers with
      Ast.EVENT(nm,(Ast.VARDECL(ty,id,import,lz,de,vattr) as param),stm,attr)
      :: _ ->
		  Ast.EVENT(Ast.EVENT_NAME(B.mkId "compute_state",attr), param,Ast.SEQ([],[],attr),attr) :: handlers
      | _ -> raise (Error.Error "internal error")) in *)
  List.map
    (function Ast.EVENT(nm,param,stm,syn,attr) as event ->
      let Ast.EVENT_NAME(B.ID(ename, _), _) = nm in
      if ename <> "new" && ename <> "unblock" then
	let stm = drop_past_return true true stm in
	Ast.EVENT(nm,param,stm,syn,attr)
      else
	event
    )
    handlers

let call s attr = Ast.PRIMSTMT(Ast.VAR(B.mkId(s),[],attr),[],attr)

let preprocess_ifunctions ifunctions  =
  List.map
    (function Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr) ->
      let stmt = drop_past_return true true stmt in
      Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr)
    )
  ifunctions

(* --------------------------- Functions ---------------------------- *)

let preprocess_functions functions =
  List.map
    (function Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr) ->
      let stmt = drop_past_return true true stmt in
      Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr))
  functions

(* --------------------------- Entry point -------------------------- *)

(* This function modifies the initial set of functions and variables
 * to include a number of system variables/functions*)
let preprocess
    (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		   fundecls,valdefs,domains,pcstate,cstates,criteria,
		   trace,handlers,chandlers,ifunctions,functions,attr)) =
  let null_attr = B.mkAttr 0 in
   (* Add trace to interface functions *)
  let ifunctions =
    (match trace with
      Ast.NOTRACE -> ifunctions
    | _ ->
	Ast.FUNDEF(Ast.DFT,B.VOID,B.mkId "print_trace",[],
           Ast.PRIMSTMT(Ast.VAR(B.mkId "print_trace_info",[],null_attr),
				[],null_attr),
		   Ast.NO_INLINE,null_attr)
	:: ifunctions) in
  Ast.SCHEDULER(nm,
         cstdefs,
		 enums,dom,grp,
		 procdecls,fundecls, (*
		 Ast.SYSDEF(Ast.mkSYSVARDECL(B.ENUM(B.mkId("SchedState")),
					     "root_s_state",0),
			    Ast.VARIABLE,null_attr) ::
		 Ast.SYSDEF(Ast.mkSYSVARDECL(B.STRUCT(B.mkId("task_struct")),
					     "current",0),
			    Ast.VARIABLE,null_attr) :: *)
		 valdefs,
		 domains,
		 pcstate,
         cstates,
		 criteria, trace,
         preprocess_handlers handlers,
         preprocess_handlers chandlers,
		 preprocess_ifunctions ifunctions,
		 preprocess_functions functions,
		   attr)

