(********* Type definitions **********)

(* FIXME: Add comment *)
type entry = Class_state.classname * Vobjects.vtype * Vobjects.modified
(* FIXME: Add comment *)
type type_rule =
    Objects.isquasi *
      (entry list * Class_state.classname list) list * entry list list

(************ Process and core events definitions **********) 

(* List of process events. These are the process events
 * that a policy should define *)
val pevents: Ast.event_name list ref
(* List of core events. These are the core events
 * that a policy should define *)
val cevents: Ast.event_name list ref

(* Initialises process events enviroment.
 * Indirection necessary to enable support (and
 * possibly different event sets) for different
 * Linux versions *)
val init_pevents_env: unit -> unit
(* Initialises core events enviroment.
 * Indirection necessary to enable support (and possibly
 * different event sets) for different Linux versions *)
val init_cevents_env: unit -> unit

(**********   Automaton rules ***********)
(*
(* Initial states for process automaton.
 * Indirection necessary to enable support (and possibly
 * different event sets) fo different Linux versions *)
val init_process_automaton:
    unit -> (int * Ast.event_name * Objects.intinfo * int) list
 (* Initial states for core automaton.
 * Indirection necessary to enable support (and possibly
 * different event sets) fo different Linux versions *)
val init_core_automaton:
    unit -> (int * Ast.event_name * Objects.intinfo * int) list
val automaton :
    unit -> (int * Ast.event_name * Objects.intinfo * int) list
(* FIXME: not needed? *)
val automaton_start : int

(* Type rule for process events *)
val pevent_specifications :
    unit -> (Ast.event_name * type_rule list) list
(* Type rule for core events *)
val cevent_specifications :
    unit -> (Ast.event_name * type_rule list) list
(* Type rule for interface functions *)
val interface_specifications :
    unit -> (Objects.identifier * type_rule list) list
*)
