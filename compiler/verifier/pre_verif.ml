(* $Id$ *)

module B = Objects
module CS = Class_state

(* The purpose of this phase is to normalize the program into a form that
is convenient for verification.  This includes in particular extracting
expressions that are relevant for verification from compound conditional
tests.  This process can result in code duplication.  A corresponding
post-processing phase eliminates this duplication when the same code
results from the duplicated branches after the analysis. *)

(* This phase converts any binary operatons where the operator is <,
>, <= or >= and where both operands are processes to a pbinary structure,
which contains fields to holds the possible current states of the
processes.  This information is used later to determine whether lifo/fifo
ordering needs to be taken into account when comparing the processes.  This
ordering is only taken into account when both processes are in the same
queue and this queue is specified to be lifo or fifo ordered. *)

(* The following function determines whether an expression can provide any
information for verification *)

let rec verifiable = function
    Ast.UNARY(Ast.NOT,exp,attr) -> verifiable exp
  | Ast.EMPTY(id,l,_,_,attr) -> true
  | Ast.IN(exp,id,l,_,_,_,attr) -> true
  | Ast.BINARY(Ast.AND,exp1,exp2,attr) -> verifiable exp1 || verifiable exp2
  | Ast.BINARY(Ast.OR,exp1,exp2,attr) -> verifiable exp1 || verifiable exp2
  | Ast.BINARY(op,exp1,exp2,attr) ->
      (* for identifying variables that are gtd to be non-zero *)
      (match op with
	Ast.EQ | Ast.NEQ | Ast.LT | Ast.GT | Ast.LEQ | Ast.GEQ -> true
      |	_ -> false)
  | Ast.SRCONSCHED(attr) -> true
  | Ast.ALIVE(tv,insrc,attr) -> true
  | _ -> false

(* -------------------- Rewrite binary as pbinary ------------------- *)

let rec pb_exp = function
    Ast.UNARY(uop,exp,a) -> Ast.UNARY(uop,pb_exp exp,a)
  | Ast.BINARY(bop,exp1,exp2,a) ->
      (match (bop,B.ty (Ast.get_exp_attr exp1)) with
	(Ast.GT,B.PROCESS)
      |	(Ast.GT,B.SCHEDULER)
      |	(Ast.LT,B.PROCESS)
      |	(Ast.LT,B.SCHEDULER)
      |	(Ast.GEQ,B.PROCESS)
      |	(Ast.GEQ,B.SCHEDULER)
      |	(Ast.LEQ,B.PROCESS)
      |	(Ast.LEQ,B.SCHEDULER) ->
	  Ast.PBINARY(bop,pb_exp exp1,[],pb_exp exp2,[],a)
      |	_ -> Ast.BINARY(bop,pb_exp exp1,pb_exp exp2,a))
  | Ast.INDR(exp,a) -> Ast.INDR(pb_exp exp,a)
  | Ast.PRIM(fn,args,a) ->
      Ast.PRIM(pb_exp fn,List.map pb_exp args,a)
  | Ast.SCHEDCHILD(exp,vp,a) -> Ast.SCHEDCHILD(pb_exp exp,vp,a)
  | Ast.IN(exp,st,stl,tv,insrc,crit,a) -> Ast.IN(pb_exp exp,st,stl,tv,insrc,crit,a)
  | e -> e

let rec pb_decl = function
    Ast.VALDEF(vardecl,exp,cst,a) -> Ast.VALDEF(vardecl,pb_exp exp,cst,a)
  | d -> d

let rec pb_stm = function
    Ast.IF(tst,thn,els,a) -> Ast.IF(pb_exp tst,pb_stm thn,pb_stm els,a)
  | Ast.FOR(id,states,state_info,dir,body,crit, a) ->
      Ast.FOR(id,states,state_info,dir,pb_stm body, crit, a)
  | Ast.FOR_WRAPPER(lbl,stmts,a) ->
      Ast.FOR_WRAPPER(lbl,List.map pb_stm stmts,a)
  | Ast.SWITCH(exp,cases,dflt,a) ->
      Ast.SWITCH(pb_exp exp,
		 List.map
		   (function Ast.SEQ_CASE(states,info,stmt,a) ->
		     Ast.SEQ_CASE(states,info,pb_stm stmt,a))
		   cases,
		 (match dflt with
		   None -> None
		 | Some s -> Some (pb_stm s)),
		 a)
  | Ast.SEQ(decls,stmts,a) ->
      Ast.SEQ(List.map pb_decl decls,List.map pb_stm stmts,a)
  | Ast.RETURN(Some exp,a) -> Ast.RETURN(Some (pb_exp exp),a)
  | Ast.MOVE(proc,dest,src,vsrc,vtgt,auto,state_end,a) ->
      Ast.MOVE(pb_exp proc,dest,src,vsrc,vtgt,auto,state_end,a)
  | Ast.MOVEFWD(proc,info,state_end,a) ->
      Ast.MOVEFWD(pb_exp proc,info,state_end,a)
  | Ast.SAFEMOVEFWD(proc,info,dflt,state_end,a) ->
      Ast.SAFEMOVEFWD(pb_exp proc,info,pb_stm dflt,state_end,a)
  | Ast.ASSIGN(lexp,rexp,srtd,a) -> Ast.ASSIGN(pb_exp lexp,pb_exp rexp,srtd,a)
  | Ast.PRIMSTMT(fn,args,a) -> Ast.PRIMSTMT(pb_exp fn,List.map pb_exp args,a)
  | s -> s


(* --------------------- Handlers and functions --------------------- *)

let preprocess_handlers handlers =
  List.map
    (function (Ast.EVENT(nm,param,stmt,attr)) ->
      Ast.EVENT(nm,param,pb_stm stmt,attr))
    handlers

let preprocess_functions functions =
  List.map
    (function (Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr)) ->
      Ast.FUNDEF(tl,ret,nm,params,pb_stm stmt,inl,attr))
    functions

let preprocess
    (Ast.SCHEDULER(nm,
		   cstdefs,enums,dom,grp,procdecls,
		   fundecls,valdefs,domains,states,cstates,criteria,
		   trace,handlers,chandlers,ifunctions,functions,attr)) =
  Preprocess_ifs.preprocess verifiable
    (Ast.SCHEDULER(nm,
		   cstdefs,enums,dom,grp,procdecls,
		   fundecls,valdefs,domains, states,cstates,criteria,
		   trace,
		   preprocess_handlers handlers,
		   preprocess_handlers chandlers,
		   preprocess_functions ifunctions,
           preprocess_functions functions,
		     attr))
