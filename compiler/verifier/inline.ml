(* We just inline everything.  It would be nice to only inline things that
make state changes, but there is also the need to detect assignments that
can affect attributes relevant to the ordering criteria of processes that
are on the ready queue.  Since we assume that any function is likely to
affect process attributes, and it is not evident at the time the inlining
takes place what is the state of any process-typed arguments, we just
assume that everything must be inlined.  Recursion is not allowed.  This
phase happens after type checking and before verification, so that we can
check that the return type of a function corresponds to each of its return
values. *)

module B = Objects
module CS = Class_state

let error ln str =
  Error.update_error();
  Printf.printf "%s: %s \n" (B.loc2c ln) str

type fenv_type =
    (B.identifier *
       (Objects.typ * Ast.vardecl list * Ast.stmt *
	  (B.identifier * B.attribute) list (* free vars *))) list

(* ------------------ Functions returning a value ------------------- *)

(* Replace returns by assignments of return value to the provided variable.
This is only called when the function has a non-void type, and thus there
should always be a return value.  Note that pre_type.ml has ensured that
return appears only in tail position. *)

let rec fix_returns var_ref = function
    Ast.IF(tst,thn,els,a) ->
      Ast.IF(tst,fix_returns var_ref thn,fix_returns var_ref els,a)
  | Ast.SWITCH(exp,cases,dflt,a) ->
      let cases =
	List.map
	  (function Ast.SEQ_CASE(a,b,stm,c) ->
	    Ast.SEQ_CASE(a,b,fix_returns var_ref stm,c))
	  cases in
      let dflt =
	match dflt with
	  None -> None
	| Some stm -> Some (fix_returns var_ref stm) in
      Ast.SWITCH(exp,cases,dflt,a)
  | Ast.SEQ(decls,stms,a) ->
      let stms = List.map (fix_returns var_ref) stms in
      Ast.SEQ(decls,stms,a)
  | Ast.RETURN(Some exp,a) -> Ast.ASSIGN(var_ref,exp,false,a)
  | Ast.RETURN(None,a) ->
      raise
	(Error.Error (Printf.sprintf "%s: unexpected void return" (B.loc2c a)))
  | Ast.SAFEMOVEFWD(exp,x,stm,b,a) ->
      Ast.SAFEMOVEFWD(exp,x,fix_returns var_ref stm,b,a)
  | stm -> stm (* note: no return possible in for loop *)

and process_fn_with_value (fenv : fenv_type) decls inlined
    (ret_type,params,body,fvs) id args new_stms a =
  B.capture_check fvs decls (error a);
  let name = List.length new_stms in
  let retid = B.mkId(Printf.sprintf "%s_return_%d" (B.id2c id) name) in
  let var_ref = Ast.VAR(retid,[],a) in
  let ret_decl = Ast.UNINITDEF(Ast.VARDECL(ret_type,retid,false, false, None,a),a) in
  let processed_body =
    process_stm fenv decls (id::inlined) (fix_returns var_ref body) in
  let body =
    Ast.SEQ(List.map2
	      (function (Ast.VARDECL(ty,nm,imp,lz,de,a)) as decl ->
		function arg -> Ast.VALDEF(decl,arg,Ast.VARIABLE,a))
	      params args,
	    [processed_body], B.updty a B.VOID)
  in (var_ref,(ret_decl,body)::new_stms)

(* ------------------ Functions returning no value ------------------ *)

and drop_returns = function
    Ast.IF(tst,thn,els,a) ->
      Some(Ast.IF(tst,drop_return_stmt thn,drop_return_stmt els,a))
  | Ast.SWITCH(exp,cases,dflt,a) ->
      let cases =
	List.map
	  (function Ast.SEQ_CASE(a,b,stm,c) ->
	    Ast.SEQ_CASE(a,b,drop_return_stmt stm,c))
	  cases in
      let dflt =
	match dflt with
	  None -> None
	| Some stm -> drop_returns stm in
      Some(Ast.SWITCH(exp,cases,dflt,a))
  | Ast.SEQ(decls,stms,a) as stm ->
      (match List.rev stms with
	[] -> Some stm
      |	(last::revrest) ->
	  Some
	    (match drop_returns last with
	      None -> Ast.SEQ(decls,List.rev revrest,a)
	    | Some s -> Ast.SEQ(decls,(List.rev revrest)@[s],a)))
  | Ast.RETURN(Some exp,a) ->
      raise
	(Error.Error (Printf.sprintf "%s: unexpected return value"
			(B.loc2c a)))
  | Ast.RETURN(None,a) -> None
  | stm -> Some stm (* note: no return possible in for loop *)

and drop_return_stmt stm =
  match drop_returns stm with
    None -> Ast.SEQ([],[],Ast.get_stm_attr stm)
  | Some s -> s

and process_fn_with_no_value (fenv : fenv_type) decls inlined
    (ret_type,params,body,fvs) id args new_stms a =
  B.capture_check fvs decls (error a);
  let processed_body =
    process_stm fenv (id::inlined) decls (drop_return_stmt body) in
  let body =
    Ast.SEQ(List.map2
	      (function (Ast.VARDECL(ty,nm,imp,lz,de,a)) as decl ->
		function arg -> Ast.VALDEF(decl,arg,Ast.VARIABLE,a))
	      params args,
	    [processed_body], B.updty a B.VOID)
  in (body,new_stms)

(* -------------------------- Expressions --------------------------- *)

and process_exp (fenv : fenv_type) decls inlined new_stms = function
    Ast.FIELD(exp,fld,ssa,a) ->
      let (exp,new_stms) = process_exp fenv decls inlined new_stms exp in
      (Ast.FIELD(exp,fld,ssa,a),new_stms)
  | Ast.UNARY(op,exp,a) ->
      let (exp,new_stms) = process_exp fenv decls inlined new_stms exp in
      (Ast.UNARY(op,exp,a),new_stms)
  | Ast.BINARY(op,exp1,exp2,a) ->
      let (exp1,new_stms) = process_exp fenv decls inlined new_stms exp1 in
      let (exp2,new_stms) = process_exp fenv decls inlined new_stms exp2 in
      (Ast.BINARY(op,exp1,exp2,a),new_stms)
  | Ast.PBINARY(op,exp1,states1,exp2,states2,a) ->
      let (exp1,new_stms) = process_exp fenv decls inlined new_stms exp1 in
      let (exp2,new_stms) = process_exp fenv decls inlined new_stms exp2 in
      (Ast.PBINARY(op,exp1,states1,exp2,states2,a),new_stms)
  | Ast.INDR(exp,a) ->
      let (exp,new_stms) = process_exp fenv decls inlined new_stms exp in
      (Ast.INDR(exp,a),new_stms)
  | Ast.PRIM(Ast.VAR(id,ssa,va),args,a) ->
      begin
	if List.mem id inlined
	then
	  raise
	    (Error.Error
	       (Printf.sprintf "%s: recursion of %s detected"
		  (B.loc2c va) (B.id2c id)));
	let (args,new_stms) =
	  process_exp_list fenv decls inlined new_stms args in
	try process_fn_with_value fenv decls inlined (List.assoc id fenv)
	    id args new_stms a
    with Not_found -> (Ast.PRIM(Ast.VAR(id,[],va),args,a),new_stms)
      end
  | Ast.PRIM(fn,args,a) ->
      let (fn,new_stms) = process_exp fenv decls inlined new_stms fn in
      let (args,new_stms) =
	process_exp_list fenv decls inlined new_stms args in
      (Ast.PRIM(fn,args,a),new_stms)
  | Ast.SCHEDCHILD(exp,x,a) ->
      let (exp,new_stms) = process_exp fenv decls inlined new_stms exp in
      (Ast.SCHEDCHILD(exp,x,a),new_stms)
  | Ast.IN(exp,x,y,z,m,c,a) ->
      let (exp,new_stms) = process_exp fenv decls inlined new_stms exp in
      (Ast.IN(exp,x,y,z,m,c,a),new_stms)
  | Ast.MOVEFWDEXP(exp,a,b) ->
      let (exp,new_stms) = process_exp fenv decls inlined new_stms exp in
      (Ast.MOVEFWDEXP(exp,a,b),new_stms)
  | exp -> (exp,new_stms)

and process_exp_list (fenv : fenv_type) decls inlined new_stms exps =
  let (exps,new_stms) =
    List.fold_left
      (function (exps,new_stms) ->
	function exp ->
	  let (exp,new_stms) = process_exp fenv decls inlined new_stms exp in
	  (exp::exps,new_stms))
      ([],new_stms) exps in
  (List.rev exps,new_stms)

(* -------------------------- Declarations -------------------------- *)

and process_decl (fenv : fenv_type) decls inlined = function
    Ast.VALDEF(decl,exp,isconst,attr) ->
      let (exp,new_times) = process_exp fenv decls inlined [] exp in
      (Ast.VALDEF(decl,exp,isconst,attr),new_times)
  | decl -> (decl,[])

(* --------------------------- Statements --------------------------- *)

and process_new_stms new_stms a stm =
  match new_stms with
    [] -> stm
  | _ ->
      let (new_decls,new_stms) = List.split (List.rev new_stms) in
      Ast.SEQ(new_decls,new_stms@[stm],a)

and process_stm (fenv : fenv_type) decls inlined = function
    Ast.IF(tst,thn,els,a) ->
      let (tst,new_stms) = process_exp fenv decls inlined [] tst in
      let thn = process_stm fenv decls inlined thn in
      let els = process_stm fenv decls inlined els in
      process_new_stms new_stms a (Ast.IF(tst,thn,els,a))
  | Ast.FOR(x,y,z,d,stm,crit, a) ->
      let stm = process_stm fenv decls inlined stm in
      Ast.FOR(x,y,z,d,stm,crit, a)
  | Ast.FOR_WRAPPER(x,stms,a) ->
      Ast.FOR_WRAPPER(x,List.map (process_stm fenv decls inlined) stms,a)
  | Ast.SWITCH(exp,cases,dflt,a) ->
      let (exp,new_stms) = process_exp fenv decls inlined [] exp in
      let cases =
	List.map
	  (function Ast.SEQ_CASE(a,b,stm,c) ->
	    let stm = process_stm fenv decls inlined stm in
	    Ast.SEQ_CASE(a,b,stm,c))
	  cases in
      let dflt =
	match dflt with
	  None -> None
	| Some stm -> Some (process_stm fenv decls inlined stm) in
      process_new_stms new_stms a (Ast.SWITCH(exp,cases,dflt,a))
  | Ast.SEQ(ldecls,stms,a) ->
      let decls_newtimes = List.map (process_decl fenv decls inlined) ldecls in
      let new_decls = Ast.valdefs2ids ldecls in
      let stms =
	List.map
	  (function stm -> process_stm fenv (new_decls@decls) inlined stm)
	  stms in
      let rec decl_loop acc_decls = function
	  [] -> Ast.SEQ(List.rev acc_decls,stms,a)
	| ((decl,[])::rest) -> decl_loop (decl::acc_decls) rest
	| ((decl,new_stms)::rest) ->
	    let rest = decl_loop [] rest in
	    process_new_stms new_stms a
	      (match rest with
		Ast.SEQ(d,s,a1) -> Ast.SEQ((List.rev (decl::acc_decls))@d,s,a1)
	      |	_ -> (Ast.SEQ (List.rev (decl::acc_decls), [rest], a))) in
      decl_loop [] decls_newtimes
  | Ast.RETURN(Some exp,a) ->
      let (exp,new_stms) = process_exp fenv decls inlined [] exp in
      process_new_stms new_stms a (Ast.RETURN(Some exp,a))
  | Ast.MOVE(exp,x,y,z,r,s,b,a) ->
      let (exp,new_stms) = process_exp fenv decls inlined [] exp in
      process_new_stms new_stms a (Ast.MOVE(exp,x,y,z,r,s,b,a))
  | Ast.MOVEFWD(exp,x,b,a) ->
      let (exp,new_stms) = process_exp fenv decls inlined [] exp in
      process_new_stms new_stms a (Ast.MOVEFWD(exp,x,b,a))
  | Ast.SAFEMOVEFWD(exp,x,s,b,a) ->
      let (exp,new_stms) = process_exp fenv decls inlined [] exp in
      let s = process_stm fenv decls inlined s in
      process_new_stms new_stms a (Ast.SAFEMOVEFWD(exp,x,s,b,a))
  | Ast.ASSIGN(lexp,rexp,x,a) ->
      let (lexp,new_stms) = process_exp fenv decls inlined [] lexp in
      let (rexp,new_stms) = process_exp fenv decls inlined new_stms rexp in
      process_new_stms new_stms a (Ast.ASSIGN(lexp,rexp,x,a))
  | Ast.PRIMSTMT((Ast.VAR(id,[],va)) as fn,args,a) ->
     begin
       if List.mem id inlined
       then
	 raise
	   (Error.Error
	      (Printf.sprintf "%s: recursion of %s detected"
		              (B.loc2c va) (B.id2c id)));
       let (args,new_stms) = process_exp_list fenv decls inlined [] args in
       try
	 let (ret_type,params,body,fvs) as x = List.assoc id fenv in
	 match ret_type with
	   B.VOID ->
	   let (body,new_stms) =
	     process_fn_with_no_value fenv decls inlined x id args
		                      new_stms a in
	   process_new_stms new_stms a body
	 | _ ->
	    (match process_fn_with_value fenv decls inlined x id args
		                         new_stms a
	     with
	       (var_ref,(ret_decl,body)::new_stms) ->
	       Ast.SEQ([ret_decl],
		       [process_new_stms new_stms a body],
		       a)
	     | _ -> raise (Error.Error "inline: internal error"))
       with Not_found -> process_new_stms new_stms a (Ast.PRIMSTMT(fn,args,a))
     end
  | Ast.PRIMSTMT(fn,args,a) ->
     let (fn,new_stms) = process_exp fenv decls inlined [] fn in
     let (args,new_stms) =
       process_exp_list fenv decls inlined new_stms args in
     process_new_stms new_stms a (Ast.PRIMSTMT(fn,args,a))
  | stm -> stm

(* ------------------------ Variable renaming ----------------------- *)
(* We rename variables in inlined functions with unique ids to prevent 
   problems with same name variables after inlining *)
let var_dict = ref (Hashtbl.create(50) : (string,B.identifier) Hashtbl.t)

let rec gather_vars_stmt = function
    Ast.SEQ(defs,stmt,attr) -> 
    List.iter (function
                 Ast.VALDEF(Ast.VARDECL(_,id,_,_,_,_),exp,c,a) ->
                 let new_id = (B.fresh_idx (B.id2c id)) in
                 Hashtbl.add !var_dict (B.id2c id) new_id
               | _ -> raise (Error.Error "gather_vars: definition not VALDEF in SEQ")
              )
              defs;
    List.iter gather_vars_stmt stmt
  | Ast.IF(_,t,e,_) -> gather_vars_stmt t; gather_vars_stmt e
  | Ast.FOR(id,_,_,_,stmt,_,_) -> let new_id = (B.fresh_idx (B.id2c id)) in
                                  Hashtbl.add !var_dict (B.id2c id) new_id;
                                  gather_vars_stmt stmt
  | Ast.FOR_WRAPPER(id,stmt,_) -> let new_id = (B.fresh_idx (B.id2c id)) in
                                  Hashtbl.add !var_dict (B.id2c id) new_id;
                                  List.iter gather_vars_stmt stmt
  | Ast.SWITCH(_,seq,Some stmt,_) -> List.iter (function Ast.SEQ_CASE(_,_,s,_) ->
                                                         gather_vars_stmt s)
                                               seq;
                                     gather_vars_stmt stmt
  | Ast.SWITCH(_,seq,None,_) -> List.iter (function Ast.SEQ_CASE(_,_,s,_) ->
                                                    gather_vars_stmt s)
                                          seq
  | Ast.SAFEMOVEFWD(_,_,stmt,_,_) -> gather_vars_stmt stmt
  | _ -> ()

and gather_vars = function
    Ast.FUNDEF(tl,ty,id,params,stmts,inl,attr) ->
    Hashtbl.reset !var_dict;
    List.iter (function Ast.VARDECL(_,id,_,_,_,_) ->
                        let new_id = (B.fresh_idx (B.id2c id)) in
                        Hashtbl.add !var_dict (B.id2c id) new_id)
              params;
    gather_vars_stmt stmts

let rec rename_exp = function
    Ast.FIELD(exp,fld,ssa,a) -> Ast.FIELD(rename_exp exp,fld,ssa,a)
  | Ast.UNARY(op,exp,a) -> Ast.UNARY(op,rename_exp exp,a)
  | Ast.BINARY(op,exp1,exp2,a) -> Ast.BINARY(op,rename_exp exp1,
                                             rename_exp exp2,a)
  | Ast.PBINARY(op,exp1,states1,exp2,states2,a) ->
     Ast.PBINARY(op,rename_exp exp1,states1,rename_exp exp2,states2,a)
  | Ast.INDR(exp,a) -> Ast.INDR(rename_exp exp,a)
  | Ast.PRIM(exp,args,a) ->  Ast.PRIM(rename_exp exp,
                                      List.map rename_exp args,a)
  | Ast.SCHEDCHILD(exp,x,a) -> Ast.SCHEDCHILD(rename_exp exp,x,a)
  | Ast.IN(exp,x,y,z,m,c,a) -> Ast.IN(rename_exp exp,rename_exp x,
                                      y,z,m,c,a)
  | Ast.MOVEFWDEXP(exp,a,b) -> Ast.MOVEFWDEXP(rename_exp exp,a,b)
  | Ast.VAR(id,ssa,a) -> (let new_id = try Hashtbl.find (!var_dict) (B.id2c id)
                                       with Not_found -> id in
                          Ast.VAR(new_id,ssa,a))
  | Ast.FIRST(exp,cri,s,a) -> Ast.FIRST(rename_exp exp,cri,s,a)
  | Ast.PFIRST(exp,cri,s,a) -> Ast.PFIRST(rename_exp exp,cri,s,a)
  | Ast.CFIRST(exp,cri,s,a) -> Ast.CFIRST(rename_exp exp,cri,s,a)
  | Ast.VALID(exp,s,a) -> Ast.VALID(rename_exp exp,s,a)
  | Ast.LCORE(obj, exp,a) -> Ast.LCORE(obj, rename_exp exp, a)
  | Ast.EMPTY(id,s,t,b,a) -> (let new_id = try Hashtbl.find (!var_dict) (B.id2c id)
                                           with Not_found -> id in
                              Ast.EMPTY(new_id,s,t,b,a))
  | exp -> exp

let rec rename_stmt = function
    Ast.IF(tst,thn,els,a) -> Ast.IF(rename_exp tst,
                                    rename_stmt thn,
                                    rename_stmt els,
                                    a)
  | Ast.FOR(id,Some y,z,d,stm,crit, a) -> Ast.FOR((try Hashtbl.find (!var_dict) (B.id2c id)
                                                   with Not_found -> id),
                                                  Some(List.map rename_exp y),
                                                  z,d,rename_stmt stm,crit, a)
  | Ast.FOR(id,None,z,d,stm,crit, a) -> Ast.FOR((try Hashtbl.find (!var_dict) (B.id2c id)
                                                 with Not_found -> id),
                                                None,z,d,rename_stmt stm,crit, a)
  | Ast.FOR_WRAPPER(x,stms,a) -> Ast.FOR_WRAPPER(x,List.map rename_stmt stms,a)
  | Ast.SWITCH(exp,cases,Some dflt,a) ->
     Ast.SWITCH(rename_exp exp,
                List.map (function Ast.SEQ_CASE(id,s,stms,attr) ->
                                   Ast.SEQ_CASE(id,s,rename_stmt stms,attr))
                         cases,
                Some (rename_stmt dflt),a)
  | Ast.SWITCH(exp,cases,None,a) ->
     Ast.SWITCH(rename_exp exp,
                List.map (function Ast.SEQ_CASE(id,s,stms,attr) ->
                                   Ast.SEQ_CASE(id,s,rename_stmt stms,attr))
                         cases,
                None,a)
  | Ast.SEQ(ldecls,stms,a) -> Ast.SEQ(List.map
                                        (function Ast.VALDEF(decl,exp,c,att) -> 
                                                  let new_decl = match decl with
                                                      Ast.VARDECL(t,id,s,l,Some e,at) ->
                                                      Ast.VARDECL(t,
                                                                  (try Hashtbl.find
                                                                         (!var_dict)
                                                                         (B.id2c id)
                                                                   with Not_found -> id),
                                                                  s,l,
                                                                  Some (rename_exp e),
                                                                  at)
                                                    | Ast.VARDECL(t,id,s,l,None,at) ->
                                                       Ast.VARDECL(t,
                                                                   (try Hashtbl.find
                                                                          (!var_dict)
                                                                          (B.id2c id)
                                                                    with Not_found -> id),
                                                                   s,l,
                                                                   None,
                                                                   at) in
                                                  Ast.VALDEF(new_decl,rename_exp exp,c,att))
                                        ldecls,
                                      List.map rename_stmt stms,a)
  | Ast.RETURN(Some exp,a) -> Ast.RETURN(Some (rename_exp exp), a)
  | Ast.MOVE(exp,x,y,z,r,s,b,a) -> Ast.MOVE(rename_exp exp,
                                            rename_exp x,y,z,r,s,b,a)
  | Ast.MOVEFWD(exp,x,b,a) -> Ast.MOVEFWD(rename_exp exp,x,b,a)
  | Ast.SAFEMOVEFWD(exp,x,s,b,a) -> Ast.SAFEMOVEFWD(rename_exp exp,x,
                                                    rename_stmt s,b,a)
  | Ast.ASSIGN(lexp,rexp,x,a) -> Ast.ASSIGN(rename_exp lexp,
                                            rename_exp rexp,x,a)
  | Ast.PRIMSTMT(fn,args,a) -> Ast.PRIMSTMT(fn,List.map rename_exp args,a)
  | Ast.ASSERT(exp,a) -> Ast.ASSERT(rename_exp exp,a)
  | stm -> stm

(* --------------------------- Entry point -------------------------- *)

let create_fn_env fns : (fenv_type * Ast.fundef list) =
  let (inlined, noninlined) =
    List.partition
      (function
	 Ast.FUNDEF(tl,ret_ty,nm,params,body,Ast.INLINE,a) -> true
       | Ast.FUNDEF(tl,ret_ty,nm,params,body,Ast.EXPORTED,a) -> false
       | _ -> failwith "cannot occur")
      fns in
  (List.map
     (function Ast.FUNDEF(tl,ret_ty,nm,params,body,inl,a) ->
               let clsenv = Ast.make_class_env (!CS.state_env) in
               let init_env = Ast.params2ids params in
               let free_vars = Ast.collect_stm_vars init_env body in
               (nm,(ret_ty,params,body,free_vars)))
     inlined,
   noninlined)

let inline
      (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		     fundecls,valdefs,domains,states,cstates,criteria,
		     trace,handlers,chandlers,ifunctions,functions,attr)) =
  let functions = List.map
                    (function Ast.FUNDEF(tl,ty,id,params,stmts,inl,attr) as f ->
                              (gather_vars f;
                               Ast.FUNDEF(tl,ty,id,
                                          List.map
                                            (function Ast.VARDECL(ty,id,i,lz,e,attr) ->
                                                      Ast.VARDECL(ty,
                                                                  Hashtbl.find
                                                                    !var_dict
                                                                    (B.id2c id),
                                                                  i,lz,e,attr))
                                            params,
                                          rename_stmt stmts,
                                          inl,attr)))
                    functions in
  let (fenv,noninlined) = create_fn_env functions in
  let handlers =
    List.map
      (function
	 Ast.EVENT(nm,(Ast.VARDECL(_,id,_,_,_,_) as param),stm,attr) ->
	 Ast.EVENT(nm,param,process_stm fenv [id] [] stm,attr))
      handlers in
  let chandlers =
    List.map
      (function
	 Ast.EVENT(nm,(Ast.VARDECL(_,id,_,_,_,_) as param),stm,attr) ->
	 Ast.EVENT(nm,param,process_stm fenv [id] [] stm,attr))
      chandlers in
  let ifunctions =
    List.map
      (function Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr) ->
	        let init_decls = Ast.params2ids params in
	        Ast.FUNDEF(tl,ret,nm,params,process_stm fenv init_decls [] stmt,
                           inl,attr))
      ifunctions in
  Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		fundecls,valdefs,domains,states,cstates,criteria,
		trace,handlers,chandlers,ifunctions,noninlined,attr)
