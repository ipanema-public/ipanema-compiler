(* mutates references to states, to all possibilities
   Other ideas: remove state change operations
                for cases where there are a set of states (eg for),
                 remove an element from the set
 *)

module B = Objects
module CS = Class_state

let version = ref 0

(* -------------------------- Configuration ------------------------- *)

(* Events to consider *)

let events =
  ["detach";"block";"unblock";
    "tick"; "schedule"]

(* State references to consider *)

type state_references = FOR | IN | EMPTY | VAR | MOVEDEST

let state_references = [FOR; IN; EMPTY; VAR; MOVEDEST]

let variables_of_type env ty attr =
  let in_type ty =
    Aux.option_filter
      (function (var,var_type) ->
	if var_type = ty
    then Some (Ast.VAR(var,[],attr))
	else None)
      env in
  let var_states ty =
    Aux.option_filter
      (function
          (id,CS.STATE(_,_,CS.PROC,_)) -> Some (Ast.VAR(id,[],attr))
	| _ -> None)
      (!CS.state_env) in
  let event_vars _ =
    let names = in_type B.PEVENT in
    (List.map
      (function id ->
          Ast.FIELD(id,B.mkId "target",[],B.updty attr B.PROCESS))
      names) @
    (List.map
      (function id ->
          Ast.FIELD(id,B.mkId "source",[],B.updty attr B.PROCESS))
      names) in
  match ty with
    B.PROCESS -> (var_states ty) @ (in_type ty) @ (event_vars ())
  | B.SCHEDULER -> (var_states ty) @ (in_type ty)
  | _ -> (in_type ty)

let remove_var id vars =
  List.filter
    (function
	Ast.VAR(id1,_,_) -> not (B.ideq (id, id1))
      |	_ -> true)
    vars

let extend env id ty = (id,ty)::env

(* id is the new value except for var, where the new value might be an
expression *)
let add_comment attr com id =
  version := !version + 1;
  let com = Printf.sprintf "%s %s: %s" com (B.id2c id) (B.loc2c attr) in
  Printf.printf "%d: %s\n" !version com;
  B.add_comment attr com

(* ------------------------ State information ----------------------- *)

(* states + classes *)
let all_states_descriptions = ref ([] : B.identifier list)
(* states only *)
let all_states = ref ([] : B.identifier list)

let remove_state id states =
  List.filter (function state -> not (B.ideq (id, state))) states

let remove_states ids states =
  List.filter
    (function state ->
      not (List.exists (function id -> B.ideq (id, state)) ids))
    states

let extract_state (state_exp:Ast.expr) : B.identifier =
  match state_exp with
    Ast.VAR(id, _, _) -> id
  | Ast.FIELD(_, fld, _, _) -> fld
  | _ -> failwith "Could not determine the state identifier"

let mutate_state state_exp chosen =
  match state_exp with
    Ast.VAR(id, ssa, attr) -> Ast.VAR(chosen, ssa, attr)
  | Ast.FIELD(exp, fld, ssa, attr) -> Ast.FIELD(exp, chosen, ssa, attr)
  | _ -> failwith "Could not determine the state identifier"

(* ------------------- Statements and expressions ------------------- *)

let listify fn env l k q =
  let rec loop k q = function
      [] -> q()
    | x::xs ->
	fn env x
	  (function modified -> k (modified::xs))
	  (function _ ->
	    loop (function modified -> k (x::modified)) q xs) in
  loop k q l

let choose fn env l k q =
  let rec loop = function
      [] -> q()
    | x::xs -> fn env x k (function _ -> loop xs) in
  loop l

let rec decls decs env k q =
  let rec loop env k = function
      [] -> q env
    | Ast.VALDEF(Ast.VARDECL(ty,id,a,lz,sh,de,b),exp,c,d)::rest ->
	expr env exp
	  (function modified ->
	    k (Ast.VALDEF(Ast.VARDECL(ty,id,a,lz,sh,de,b),modified,c,d)::rest))
	  (function _ ->
	    loop ((id,ty)::env)
	      (function modified ->
		k (Ast.VALDEF(Ast.VARDECL(ty,id,a,lz,sh,de,b),exp,c,d)::modified))
	      rest)
    | Ast.SYSDEF(Ast.VARDECL(ty,id,a,lz,sh,de,b),m,n)::rest ->
	loop ((id,ty)::env)
	  (function modified ->
	    k (Ast.SYSDEF(Ast.VARDECL(ty,id,a,lz,sh,de,b),m,n)::modified))
	  rest
    | _ -> raise (Error.Error "unexpected code") in
  loop env k decs

and stmt env s k q =
  match s with
    Ast.IF(exp,stmt1,stmt2,attr) ->
      expr env exp
	(function modified -> k (Ast.IF(modified,stmt1,stmt2,attr)))
	(function _ ->
	  stmt env stmt1
	    (function modified -> k (Ast.IF(exp,modified,stmt2,attr)))
	    (function _ ->
	      stmt env stmt2
		(function modified -> k (Ast.IF(exp,stmt1,modified,attr)))
		q))
  | Ast.FOR(id,stids,states,dir,stm,crit,attr) ->
      (* picks some state that is not in the list at all;
	 alternatively could change the list in one way, haven't taken that
	 option *)
      let q _ =
	let env = extend env id (B.PROCESS) in
	stmt env stm
	  (function modified ->
	    k (Ast.FOR(id,stids,states,dir,modified,crit,attr)))
	  q in
      (match stids with
	  Some exps ->
	    let stids = List.map (fun exp ->
	      match exp with
		  Ast.VAR(id, _, _) -> id
		| Ast.FIELD(exp, id,_, _) -> failwith "FIXME"
		| _ -> failwith "Unexpected expression in for (mutate_states.ml)"
	    ) exps in
	  let options = remove_states stids !all_states_descriptions in
	  choose
	    (function env -> function chosen -> function k -> function q ->
	      let concretized = CS.concretize chosen in
	      let attr = add_comment attr "mutated for to" chosen in
          (k (Ast.FOR(id,Some [Ast.VAR(chosen, [],B.mkAttr(-1))],concretized,dir,stm,crit,attr)))::q())
	    env options k q
      |	None -> q())
  | Ast.FOR_WRAPPER(label,stms,attr) ->
      listify stmt env stms
	(function modified -> k (Ast.FOR_WRAPPER(label,stms,attr)))
	q
  | Ast.SWITCH(exp,cases,default,attr) ->
      expr env exp
	(function modified -> k (Ast.SWITCH(modified,cases,default,attr)))
	(function _ ->
	  listify case env cases
	    (function modified -> k (Ast.SWITCH(exp,modified,default,attr)))
	    (function _ ->
	      match default with
		None -> q()
	      |	Some stm ->
		  stmt env stm
		    (function modified ->
		      k (Ast.SWITCH(exp,cases,Some modified,attr)))
		    q))
  | Ast.CONTINUE _ -> q()
  | Ast.SEQ(decs,stms,attr) ->
      decls decs env
	(function modified -> k (Ast.SEQ(modified,stms,attr)))
	(function _ ->
	  listify stmt env stms
	    (function modified -> k (Ast.SEQ(decs,modified,attr)))
	    q)
  | Ast.RETURN(Some(exp),attr) ->
      expr env exp
	(function modified -> k (Ast.RETURN(Some(modified),attr)))
	q
  | Ast.MOVE(exp,state_exp,tsrc,vsrc,dst,auto_allowed,state_end,attr) ->
     (* TOCHECK: Is the transformation still correct now that
         state is an expression (Ast.VAR or Ast.FIELD) instead of B.identifier *)
      expr env exp
	(function modified ->
	  k (Ast.MOVE(modified,state_exp,tsrc,vsrc,dst,auto_allowed,
		      state_end,attr)))
	(function _ ->
	  let state = extract_state state_exp in
     	  let options = remove_state state !all_states in
	  choose
	    (function env -> function chosen -> function k -> function q ->
	      let concretized = CS.concretize chosen in
	      let attr = add_comment attr "mutated move to" chosen in
	      let new_state = mutate_state state_exp chosen in
	      (k (Ast.MOVE(exp,new_state,tsrc,vsrc,Some (List.hd concretized),
			   auto_allowed,state_end,attr)))::q())
	    env options k q)
  | Ast.ASSIGN(loc,exp,sorted_fld,attr) ->
      expr env loc
	(function modified -> k (Ast.ASSIGN(modified,exp,sorted_fld,attr)))
	(function _ ->
	  expr env exp
	    (function modified -> k (Ast.ASSIGN(loc,modified,sorted_fld,attr)))
	    q)
  | Ast.PRIMSTMT(f,args,attr) ->
      expr env f
	(function modified -> k (Ast.PRIMSTMT(modified,args,attr)))
	(function _ ->
	  listify expr env args
	    (function modified -> k (Ast.PRIMSTMT(f,modified,attr)))
	    q)
  | Ast.RETURN(None,attr) | Ast.ERROR(_,attr) | Ast.BREAK(attr) ->
      q() (* no state subterms *)
  | Ast.ASSERT(exp,attr) ->
      expr env exp (function modified -> k (Ast.ASSERT(modified,attr))) q
  | Ast.MOVEFWD(_,_,_,attr) | Ast.SAFEMOVEFWD(_,_,_,_,attr)
  | Ast.DEFER(attr) ->
      raise (Error.Error "virtual schedulers not handled")

and case env (Ast.SEQ_CASE(id,states,stm,attr)) k q =
  let q _ =
    stmt env stm
      (function modified -> k (Ast.SEQ_CASE(id,states,modified,attr)))
      q in
  if List.mem IN state_references
  then
    (* as for FOR, only take things not in the current specification *)
    let options = remove_states id !all_states_descriptions in
    choose
      (function env -> function chosen -> function k -> function q ->
	let concretized = CS.concretize chosen in
	let attr = add_comment attr "mutated case to" chosen in
	(k (Ast.SEQ_CASE([chosen],concretized,stm,attr)))::q())
      env options k q
  else q()

and expr env e k q =
  match e with
    Ast.SUM _|Ast.MIN _|Ast.MAX _|Ast.COUNT _|Ast.FNOR _|Ast.DISTANCE _ -> q()
  | Ast.INT(n,attr) -> q()
  | Ast.VAR(id,_,attr) ->
      if List.exists (function x -> B.ideq(id,x)) !all_states &&
	List.mem VAR state_references
      then
	(* switch within the same type *)
	let attr = add_comment attr "mutated var" id in
	let vars = remove_var id (variables_of_type env (B.ty attr) attr) in
	choose
	  (function env -> function chosen -> function k -> function q ->
	    (k chosen)::q())
	  env vars k q
      else q()
  | Ast.FIELD(exp,id,ssa,attr) ->
      expr env exp (function modified -> k (Ast.FIELD(modified,id,ssa,attr))) q
  | Ast.BOOL(v,attr) -> q()
  | Ast.PARENT(attr) -> q()
  | Ast.UNARY(uop,exp,attr) ->
      expr env exp (function modified -> k (Ast.UNARY(uop,modified,attr))) q
  | Ast.BINARY(bop,exp1,exp2,attr) ->
      expr env exp1
	(function modified -> k (Ast.BINARY(bop,modified,exp2,attr)))
	(function _ ->
	  expr env exp2
	    (function modified -> k (Ast.BINARY(bop,exp1,modified,attr)))
	    q)
  | Ast.PBINARY(bop,exp1,states1,exp2,states2,attr) ->
      failwith "unexpected pbinary"
  | Ast.INDR(exp,attr) ->
      expr env exp (function modified -> k (Ast.INDR(modified,attr))) q
  | Ast.SELECT(st,attr) -> q()
  | Ast.FIRST(exp,crit,st,attr) ->
     expr env exp (function modified -> k (Ast.FIRST(modified,crit, st, attr))) q
  | Ast.VALID(locexp,states,attr) ->
      if List.mem EMPTY state_references
      then
	let id = match locexp with
	    Ast.VAR(id, _, _) | Ast.FIELD(_, id,_, _) -> id
	  | _ -> failwith "Unexpected expression in for (mutate_states.ml)"
	in
	let options = remove_state id !all_states_descriptions in
	choose
	  (function env -> function chosen -> function k -> function q ->
	    let concretized = List.hd (CS.concretize chosen) in
	    let attr = add_comment attr "mutated empty to" chosen in
	    let chosenexp = mutate_state locexp chosen in
	    (k (Ast.VALID(chosenexp,Some concretized,attr)))::q())
	  env options k q
      else q()
  | Ast.EMPTY(id,states,a,true,attr) ->
      if List.mem EMPTY state_references
      then
	let options = remove_state id !all_states_descriptions in
	choose
	  (function env -> function chosen -> function k -> function q ->
	    let concretized = CS.concretize chosen in
	    let attr = add_comment attr "mutated empty to" chosen in
	    (k (Ast.EMPTY(chosen,concretized,a,true,attr)))::q())
	  env options k q
      else q()
  | Ast.EMPTY(id,states,a,false,attr) -> q()
  | Ast.PRIM(f,exps,attr) ->
      expr env f
	(function modified -> k (Ast.PRIM(modified,exps,attr)))
	(function _ ->
	  listify expr env exps
	    (function modified -> k (Ast.PRIM(f,modified,attr)))
	    q)
  | Ast.SCHEDCHILD(exp1,nprocs,attr) ->
      raise (Error.Error "virtual schedulers not handled")

  | Ast.SRCONSCHED(attr) | Ast.ALIVE(_,_,attr) -> q()

  | Ast.AREF(_,_,attr) ->
      raise (Error.Error "internal error, array reference")

  | Ast.IN(exp,locexp,a,b,true (*in src pgm*),crit,attr) ->
      expr env exp
	(function modified -> k (Ast.IN(modified,locexp,a,b,true,crit,attr)))
	(function _ ->
	  if List.mem IN state_references
	  then
	    let id = match locexp with
		  Ast.VAR(id, _, _) | Ast.FIELD(_, id,_, _) -> id
		  | _ -> failwith "Unexpected expression in for (mutate_states.ml)"
	    in
	    let options = remove_state id !all_states_descriptions in
	    choose
	      (function env -> function chosen -> function k -> function q ->
		let concretized = CS.concretize chosen in
		let attr = add_comment attr "mutated in to" chosen in
		let chosenexp = mutate_state locexp chosen in
		(k (Ast.IN(exp,chosenexp,concretized,b,true,crit,attr)))::q())
	      env options k q
	  else q())
  | Ast.IN(exp,_,a,b,false (*not in src pgm*),crit,attr) -> q()
  | Ast.MOVEFWDEXP(_,_,attr) ->
      raise (Error.Error "virtual schedulers not handled")

(* ---------------------------- Handlers ---------------------------- *)

let process_handlers env handlers k q =
  listify
    (function env -> function Ast.EVENT(nm,param,stm,syn,attr) ->
      function k -> function q ->
	if List.mem (Ast.event_name2c nm) events
	then
	  let Ast.VARDECL(ty,id,imported,lz,_,de,vattr) = param in
	  stmt ((id,ty)::env) stm
	    (function modified ->
	      k (Ast.EVENT(nm,param,modified,syn,attr)))
	    q
	else q())
    env handlers k q

(* --------------------------- Entry point -------------------------- *)

let mutate
    (Ast.SCHEDULER(nm,
		     cstdefs,enums,dom,grp,procdecls,fundecls,valdefs,domains,states,cstates,
		     criteria,trace,handlers,chandlers,ifunctions,functions,attr)) =
  all_states := List.map (function CS.STATE(id,_,_,_) -> id) (CS.all_states());
  all_states_descriptions :=
    (!all_states) @
    (List.map (function x -> B.mkId(CS.class2c x)) (CS.all_classes false));
  process_handlers
    (List.map
       (function
	   Ast.VALDEF(Ast.VARDECL(ty,id,_,_,_,_,_),e,_,_) -> (id,ty)
	 | Ast.SYSDEF(Ast.VARDECL(ty,id,_,_,_,_,_),_,_) -> (id,ty)
	 | Ast.UNINITDEF(_,_) ->
	     raise (Error.Error "unexpected uninit def")
	 | Ast.DUMMYDEF(_,_,_) ->
	     raise (Error.Error "unexpected dummy def"))
       valdefs)
    handlers
    (function modified ->
      Ast.SCHEDULER(nm,
		    cstdefs,enums,dom,grp,procdecls,fundecls,valdefs,domains,
		    states,cstates,criteria,trace,modified,chandlers,ifunctions,
		      functions,attr))
    (function _ -> [])
