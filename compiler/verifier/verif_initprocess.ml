(* this ensures that all the process declarations are all initialized except the system fields   *)
module B = Objects
module CS = Class_state

let rec error ln str =(
Error.update_error();
Printf.printf "%s: %s \n" (B.loc2c ln) str);;
let rec warning ln str =( Printf.printf "warning: %s: %s \n" (B.loc2c ln) str);;
let rec pr = (Printf.sprintf);;


let rec check_expression_Assi expr handlerID proList =(
  match expr with
  |Ast.VAR(id , _ , _) -> (List.mem id proList)
  |Ast.FIELD( VAR(id , _ , _) , id2 , _ , _ )->
    (B.id2c id2 = "target" && B.id2c id = B.id2c handlerID )
  |_ -> true
  );;

let rec check_expression expr handlerID =(
  match expr with
  |Ast.VAR(id , _ , _) -> (B.id2c id = B.id2c handlerID) (*checking if *)
  (* |Ast.FIELD( exp , id2 , _ , _ )-> (B.id2c id2 = "target") *)
  |_ -> true
  );;

  let rec help_append_list l1 l2 =(
      match l1 with
          [] -> l2
          |(h::t) -> if (List.mem h l2) then (help_append_list t l2) else (help_append_list t (h :: l2)) );;

  let append_list x y = ( help_append_list x (help_append_list y []) );;


let rec check_declarations_list prodeclNew stmtList proList handlerID =
  match stmtList with
  |[] -> prodeclNew
  |stmt::rest ->
    let newList = (check_declarations prodeclNew stmt proList handlerID) in
    (check_declarations_list newList rest proList handlerID)

and check_declarations prodeclNew stmt proList handlerID=
  (match stmt with
  | Ast.ASSIGN(Ast.FIELD( exp , id2 , _ , _ ) , _, _ , _ )  ->
    if ((B.ty  (Ast.get_exp_attr exp) = B.PROCESS) &&
    (check_expression_Assi exp handlerID proList) )then
        List.filter (function Ast.VARDECL(ty , id , _ , _ , _ , _ , _)  ->
           not (B.id2c id = B.id2c id2)) prodeclNew
    else
      prodeclNew
  | Ast.IF(exp,stmt1,stmt2,attr) ->
    let newList1 = (check_declarations prodeclNew stmt1 proList handlerID) in
      let newList2 = (check_declarations prodeclNew stmt2 proList handlerID) in
      (* we need to take the union between the 2 outputs *)
        (append_list newList1 newList2)
  | Ast.FOR(id,stid,l,dir,stmt,crit,attr) ->
    (check_declarations prodeclNew stmt proList handlerID)
  | Ast.FOR_WRAPPER(label,stmtList,attr) ->
    (check_declarations_list prodeclNew stmtList proList handlerID)
  | Ast.SEQ(decls,stmtList,attr) ->
    let proListNew1 =
      List.filter(function Ast.VALDEF(Ast.VARDECL(ty,id,_,_,_,_,_),
                           Ast.FIELD(expr,idexpr,_,_),_,_)->
                            ((ty = B.PROCESS ) && (B.id2c idexpr = "target") &&
                            (check_expression expr handlerID))
                            | _->false) decls
        in
        let proListNew2 = List.map (function Ast.VALDEF(
          Ast.VARDECL(ty,id,_,_,_,_,_),Ast.FIELD(expr,idexpr,_,_),_,_) -> id ) proListNew1 in
        (* List.fold_left (function ) is better for doing *)
        (check_declarations_list prodeclNew stmtList (proListNew2@proList) handlerID)
    | Ast.SWITCH(_ , seq, stmtopt ,_) ->
    let def = match stmtopt with
        None -> prodeclNew
      | Some stmt -> (check_declarations prodeclNew stmt proList handlerID)
     in
     let newList = List.map (function Ast.SEQ_CASE(_,_,stmt,_) ->
        stmt) seq in newList;
     List.fold_left (fun def x ->
       (check_declarations def x proList handlerID)) def newList;
  | stmt -> prodeclNew );;


let rec check_handler_new handlers prodeclNew=(
  match handlers with
  | [] ->  raise (Error.Error "Unexpected handle ")
  | Ast.EVENT( Ast.EVENT_NAME( nm, eattr ) , vd  , stmt , _ , attr ) :: restH ->
      if B.id2c nm = "new" then
        match vd with
        |Ast.VARDECL(ty,id,_,_,_,_,_) ->
            (check_declarations prodeclNew stmt [] id)
        |_ -> raise (Error.Error "Unexpected vardeclare ")

      else
        (check_handler_new restH prodeclNew)
);;

(************************ entry point ****************************************)
let rec check_process_init (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
  fundecls,valdefs, domains,pcstate,cstates, criteria,trace,handlers,chandlers,
  ifunctions,functions,attr)) =
let prodeclNew = List.filter (
  function Ast.VARDECL(ty , id , imported , _ , _ , expr , attr)  ->
    ((imported = false) && (expr = None)) ) procdecls in
  (check_handler_new handlers prodeclNew)
