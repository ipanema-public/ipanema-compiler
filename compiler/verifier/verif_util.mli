module B = Objects
module CS = Class_state

exception LookupErrException of string option ;;

type environment =  {
    assigned_values: (B.identifier * Ast.expr) list;
    guessed_values: (B.identifier * Ast.expr) list;
    functions: (B.identifier * Ast.fundef) list
}

type 'a reachability =
            REACH of 'a * environment
            |FORK of Ast.expr list option *  (* branching statement *)
                    ('a reachability) option list  * (* move statements *)
                    environment

val verif_env: environment ref

val updFctEnv: environment -> ((B.identifier * Ast.fundef) list) -> environment
val lookfor_exp: B.identifier -> environment -> Ast.expr

val lookup_id: B.identifier -> (B.identifier *'b) list -> 'b
val list_cartesian: ('a list list) -> ('a list list) -> 'a list list
val id_equal: Ast.expr -> Ast.expr -> bool
val is_contained: Ast.expr -> Ast.expr list -> bool
val intersect: Ast.expr list -> Ast.expr list-> bool
val field_to_id: Ast.expr -> B.identifier
val is_equal: Ast.expr -> Ast.expr -> bool 
val evaluate_reachability: 'a reachability option -> 'a reachability option
val simplify_expression : Ast.expr -> Ast.expr
val extract_move_unique : (Ast.stmt -> bool)-> Ast.stmt -> Ast.stmt reachability option
val extract_return_unique: (Ast.expr -> bool)-> Ast.stmt -> Ast.expr option reachability option
val check_all_paths : 'a reachability option -> ('a option -> environment -> bool) -> bool
val check_some_paths : 'a reachability option -> ('a option -> environment ->  bool) -> bool
val check_no_paths: 'a reachability option -> ('a option-> environment ->  bool) -> bool
val type_pstate: Ast.expr -> CS.classname option
val type_cstate: Ast.expr -> CS.state_info option

val lookup_event: Ast.event_decl list -> string -> Ast.event_decl
