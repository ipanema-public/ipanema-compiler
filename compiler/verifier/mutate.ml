(* $Id$ *)

(* could add something to change the name, to show that an exhaustive set
of handlers is required *)

(* no treatment of interface functions, admit, or aspects *)

module B = Objects
module CS = Class_state

(*
     missing tests (ie include only the true branch or the false branch)
     inverted tests (flip true and false branches)
     missing statements
     incorrect move destination
     select from wrong state (not done)
     incorrect scheduler state computation
     inappropriate current->need_resched (not done)
     ordering criteria (not done)
*)

(* for switch case label: mutation is always a single state or class, not a
set of states *)

(* ------------------------ State information ----------------------- *)

let all_states_descriptions = ref ([] : B.identifier list)

let all_states = ref ([] : B.identifier list)

(* point: we can't say running = ..., even though running is an expression *)
type flag = EXP | LOC

let variables_of_type flag env ty attr =
  let in_type ty =
    Aux.option_filter
      (function (var,var_type) ->
	if var_type = ty
	then Some (Ast.VAR(var,attr))
	else None)
      env in
  let var_states ty =
    match flag with
      EXP ->
	Aux.option_filter
	  (function
	      (id,CS.STATE(_,_,CS.PROC,_)) -> Some (Ast.VAR(id,attr))
	    | _ -> None)
	  (!CS.state_env)
    | LOC -> [] in
  let event_vars _ =
    let names = in_type B.EVENT in
    (List.map
      (function id ->
	Ast.FIELD(id,B.mkId "target",B.updty attr B.PROCESS))
      names) @
    (List.map
      (function id ->
	Ast.FIELD(id,B.mkId "source",B.updty attr B.PROCESS))
      names) in
  match ty with
    B.PROCESS -> (var_states ty) @ (in_type ty) @ (event_vars ())
  | B.SCHEDULER -> (var_states ty) @ (in_type ty)
  | _ -> (in_type ty)

let extend env id ty = (id,ty)::env

(* ------------------------- Select mutation ------------------------ *)

let select n x l =
  let rec loop n = function
      [] -> x
    | (ct,mfunc)::rest ->
	if n < ct
	then mfunc n
	else loop (n-ct) rest in
  if n >= 0 then loop n l else x

let select_list n x cts mfuncs terms context =
  let rec loop n context = function
      ([],[],[]) -> x
    | (ct::cts,mfunc::mfuncs,term::terms) ->
	if n < ct
	then context (mfunc n :: terms)
	else
	  loop (n-ct) (function res -> context (term::res))
	    (cts, mfuncs, terms)
    | _ -> raise (Error.Error "internal error") in
  if n < 0
  then x
  else loop n context (cts, mfuncs, terms)

(* ------------------------- Miscellaneous -------------------------- *)

let sum l = List.fold_left (function x -> function n -> x + n) 0 l

let remove_var id vars =
  List.filter
    (function
	Ast.VAR(id1,_) -> not (B.ideq (id, id1))
      |	_ -> true)
    vars

(* not quite right because it removes everything with the given name *)
let remove_field exp vars =
  match exp with
    Ast.FIELD(exp,fld,attr) ->
      List.filter
	(function
	    Ast.FIELD(exp,fld1,attr) -> not (B.ideq (fld, fld1))
	  | _ -> true)
	vars
  | _ -> raise (Error.Error "internal error")

let remove_state id states =
  List.filter (function state -> not (B.ideq (id, state))) states

let remove_states ids states =
  List.filter
    (function state ->
      not (List.exists (function id -> B.ideq (id, state)) ids))
    states

(* ------------------------- Traverse syntax ------------------------ *)

let rec count_exp flag env = function
   Ast.INT(n,attr) as x -> (0, function n -> x)

  | Ast.VAR(id,attr) as x ->
      (* switch within the same type *)
      let vars =
	remove_var id (variables_of_type flag env (B.ty attr) attr) in
      let cur_count = List.length vars in
      (cur_count,
       function n ->
	 select n x
	   [(cur_count,
	     function _ ->
	       Printf.printf "replacing variable in %s\n" (B.loc2c attr);
	       List.nth vars n)])
	
  | Ast.FIELD(exp,fld,attr) as x ->
      (* switch within the same type *)
      let vars =
	remove_field x (variables_of_type flag env (B.ty attr) attr) in
      let cur_count = List.length vars in
      (cur_count,
       function n ->
	 select n x
	   [(cur_count,
	     function _ ->
	       Printf.printf "replacing field expr in %s\n" (B.loc2c attr);
	       List.nth vars n)])
	
  | Ast.BOOL(v,attr) as x ->
    (* switch the value *)
      (1,
       function n ->
	 select n x
	   [(1,
	     function n ->
	       Printf.printf "replacing boolean in %s with %b\n"
		 (B.loc2c attr) (not v);
	       Ast.BOOL(not v,attr))])
	
  | Ast.UNARY(uop,exp,attr) as x ->
      let (ct,mfunc) = count_exp flag env exp in
      (ct,
       function n ->
	 select n x [(ct, function n -> Ast.UNARY(uop,mfunc n,attr))])
	
  | Ast.BINARY(bop,exp1,exp2,attr) as x ->
      let (ct1,mfunc1) = count_exp flag env exp1 in
      let (ct2,mfunc2) = count_exp flag env exp2 in
      let cur_count = 1 (* switch arguments *) + ct1 + ct2 in
      (cur_count,
       function n ->
	 select n x
	   [(1,
	     function n ->
	       Printf.printf
		 "switching order of binary arguments in %s\n"
		 (B.loc2c attr);
	       Ast.BINARY(bop,exp2,exp1,attr));
	    (ct1,function n -> Ast.BINARY(bop,mfunc1 n,exp2,attr));
	    (ct2,function n -> Ast.BINARY(bop,exp1,mfunc2 n,attr))])
	
  | Ast.PBINARY(bop,exp1,states1,exp2,states2,attr) as x ->
      let (ct1,mfunc1) = count_exp flag env exp1 in
      let (ct2,mfunc2) = count_exp flag env exp2 in
      let cur_count = 1 (* switch arguments *) + ct1 + ct2 in
      (cur_count,
       function n ->
	 select n x
	   [(1,
	     function n ->
	       Printf.printf
		 "switching order of binary arguments in %s\n"
		 (B.loc2c attr);
	       Ast.PBINARY(bop,exp2,states2,exp1,states2,attr));
	    (ct1,
	     function n ->
	       Ast.PBINARY(bop,mfunc1 n,states1,exp2,states2,attr));
	    (ct2,
	     function n ->
	       Ast.PBINARY(bop,exp1,states1,mfunc2 n,states2,attr))])
	
  | Ast.INDR(exp,attr) as x ->
      let (ct,mfunc) = count_exp flag env exp in
      (ct,
       function n -> select n x [(ct,function n -> Ast.INDR(mfunc n,attr))])
	
  | Ast.SELECT(st,attr) as x -> (0, function n -> x)
	
  | Ast.EMPTY(id,states,a,b,attr) as x ->
      let options = remove_state id (!all_states_descriptions) in
      let cur_count = List.length options in
      (cur_count,
       function n ->
	 select n x
	   [(cur_count,
	     function n ->
	       let chosen = List.nth options n in
	       let concretized = CS.concretize chosen in
	       Printf.printf "replacing empty argument in %s with %s\n"
		 (B.loc2c attr) (B.id2c chosen);
	       Ast.EMPTY(chosen,concretized,a,b,attr))])
	
  | Ast.PRIM(f,exps,attr) as x ->
      let (cts,mfuncs) = List.split (List.map (count_exp flag env) exps) in
      let cur_count = sum cts in
      (cur_count,
       function n ->
	 select_list n x cts mfuncs exps
	   (function exps -> Ast.PRIM(f,exps,attr)))
	
  | Ast.SCHEDCHILD(exp1,nprocs,attr) ->
      raise (Error.Error "virtual schedulers not handled")
	
  | Ast.SRCONSCHED(attr) as x -> (0, function n -> x)
	
  | Ast.ALIVE(_,_,attr) as x -> (0, function n -> x)
	
  | Ast.AREF(_,_,attr) ->
      raise (Error.Error "internal error, array reference")
	
  | Ast.IN(exp,id,a,b,c,attr) as x ->
      let (ct,mfunc) = count_exp flag env exp in
      let options = remove_state id (!all_states_descriptions) in
      let olen = List.length options in
      let cur_count = ct + olen in
      (cur_count,
       function n ->
	 select n x
	   [(olen,
	     function n ->
	       let chosen = List.nth options n in
	       let concretized = CS.concretize chosen in
	       Printf.printf "replacing in state argument in %s with %s\n"
		 (B.loc2c attr) (B.id2c chosen);
	       Ast.IN(exp,chosen,concretized,b,c,attr));
	    (ct,function n -> Ast.IN(mfunc n,id,a,b,c,attr))])
	
  | Ast.MOVEFWDEXP(_,_,attr) ->
      raise (Error.Error "virtual schedulers not handled")
	
let rec get_atomic_tests context = function
    Ast.BINARY(Ast.AND,exp1,exp2,attr) ->
      let left =
	get_atomic_tests
	  (function new_exp1 -> Ast.BINARY(Ast.AND,new_exp1,exp2,attr))
	  exp1 in
      let right =
	get_atomic_tests
	  (function new_exp2 -> Ast.BINARY(Ast.AND,exp1,new_exp2,attr))
	  exp2 in
      left @ right
	       
  | Ast.BINARY(Ast.OR,exp1,exp2,attr) ->
      let left =
	get_atomic_tests
	  (function new_exp1 -> Ast.BINARY(Ast.OR,new_exp1,exp2,attr))
	  exp1 in
      let right =
	get_atomic_tests
	  (function new_exp2 -> Ast.BINARY(Ast.OR,exp1,new_exp2,attr))
	  exp2 in
      left @ right
	       
  | exp -> [(exp, context)]
	
let count_decls decls env =
  let (env,cts,mfuncs) =
    List.fold_left
      (function (env,cts,mfuncs) ->
	function
	    Ast.VALDEF(Ast.VARDECL(ty,id,a,b),exp,c,d) ->
	      let (ct,mfunc) = count_exp EXP env exp in
	      ((id,ty)::env,ct::cts,
	       (function n -> Ast.VALDEF(Ast.VARDECL(ty,id,a,b),mfunc n,c,d))::
	       mfuncs)
	  | Ast.UNINITDEF(_,_) -> raise (Error.Error "unexpected uninit def")
	  | Ast.SYSDEF(Ast.VARDECL(ty,id,_,_),_,_) -> ((id,ty)::env,cts,mfuncs)
	  | Ast.DUMMYDEF(_,_,_) -> raise (Error.Error "unexpected dummy def"))
      (env,[],[])
      decls in
  (env,List.rev cts,List.rev mfuncs)
    
let rec count_stmt env = function
    Ast.IF(exp,stmt1,stmt2,attr) as x ->
	(* possibilities:
	   negate each atomic test
	   replace an atomic test by true
	   replace an atomic test by false
	   Thus, multiply the number of atomic tests by 3 *)
      let exps_contexts = get_atomic_tests (function x -> x) exp in
      let test_count = List.length exps_contexts in
      let (ct,mfunc) = count_exp EXP env exp in
      let (ct1,mfunc1) = count_stmt env stmt1 in
      let (ct2,mfunc2) = count_stmt env stmt2 in
      let cur_count = 1 + (test_count * 3) + ct + ct1 + ct2 in
      let bool_attr = B.updty attr B.BOOL in
      let trueval = Ast.BOOL(true,bool_attr) in
      let falseval = Ast.BOOL(false,bool_attr) in
      (cur_count,
       function n ->
	 select n x
	   [(1,
	     function n ->
	       Printf.printf "dropping if statement in %s\n"
		 (B.loc2c attr);
	       Ast.SEQ([],[],attr));
	    (test_count*3,
	     function n ->
	       select n x
		 (List.flatten
		    (List.map
		       (function (exp,context) ->
			 let inif test = Ast.IF(test,stmt1,stmt2,attr) in
			 [(1,
			   function n ->
			     Printf.printf "if test set to true in %s\n"
			       (B.loc2c attr);
			     inif (context trueval));
			  (1,
			   function n ->
			     Printf.printf "if test set to false in %s\n"
			       (B.loc2c attr);
			     inif (context falseval));
			  (1,
			   function n ->
			     Printf.printf "if test negated in %s\n"
			       (B.loc2c attr);
			     inif(context(Ast.UNARY(Ast.NOT,exp,bool_attr))))])
		       exps_contexts)));
	    (ct, function n -> Ast.IF(mfunc n,stmt1,stmt2,attr));
	    (ct1, function n -> Ast.IF(exp,mfunc1 n,stmt2,attr));
	    (ct2, function n -> Ast.IF(exp,stmt1,mfunc2 n,attr))])
	
  | Ast.FOR_WRAPPER(label,stms,attr) as x ->
      let (cts,mfuncs) = List.split (List.map (count_stmt env) stms) in
      let cts_sum = sum cts in
      (cts_sum,
       function n ->
	 select n x
	   [(cts_sum,
	     function n ->
	       select_list n x cts mfuncs stms
		 (function stms -> Ast.FOR_WRAPPER(label,stms,attr)))])
	
  | Ast.FOR(id,stids,states,dir,stmt,crit,attr) as x ->
      let (ct,mfunc) =
	count_stmt (extend env id (B.sched_select B.PROCESS B.SCHEDULER)) stmt
      in
      let options =
	match stids with
	  Some x ->
	    (* considers the for applying to states other than the ones in
	       the list *)
	    List.fold_left
	      (function all ->
		function x ->
		  remove_state x all)
	      (!all_states_descriptions) x
	| None -> !all_states_descriptions in
      let olen = List.length options in
      let cur_count =
	1 (* drop the for statement *) +
	  (match stids with Some _ -> 1 | None -> 0) (* no state *) +
	  olen + ct in
      (cur_count,
       function n ->
	 select n x
	   ((match stids with
	     Some _ ->
	       [(1,
		 function n ->
		   Printf.printf "changed for state in %s to everything\n"
		     (B.loc2c attr);
		   Ast.FOR(id,None,CS.all_states(),dir,stmt,crit,attr))]
	   | None -> []) @
	    [(1,
	      function n ->
		Printf.printf "dropping for statement in %s\n"
		  (B.loc2c attr);
		Ast.SEQ([],[],attr));
	     (olen,
	      function n ->
		let chosen = List.nth options n in
		let concretized = CS.concretize chosen in
		Printf.printf "changed for state in %s to %s\n"
		  (B.loc2c attr) (B.id2c chosen);
		Ast.FOR(id,Some [chosen],concretized,dir,stmt,attr));
	     (ct, function n -> Ast.FOR(id,stids,states,dir,mfunc n,attr))]))

  | Ast.SWITCH(exp,cases,default,attr) as x ->
      let (pcts,pmfuncs) =
	List.split
	  (List.map
	     (function Ast.SEQ_CASE(pat,states,stm,attr) ->
	       let options = remove_states pat (!all_states_descriptions) in
	       let olen = List.length options in
	       (olen,
		function n ->
		  let chosen = List.nth options n in
		  Printf.printf "changed switch pattern in %s to %s\n"
		    (B.loc2c attr) (B.id2c chosen);
		  let concretized = CS.concretize chosen in
		  Ast.SEQ_CASE([chosen],concretized,stm,attr)))
	     cases) in
      let pcts_sum = sum pcts in
      let (cts,mfuncs) =
	List.split
	  (List.map
	     (function Ast.SEQ_CASE(pat,states,stm,attr) ->
	       let (ct,mfunc) = count_stmt env stm in
	       (ct,function n -> Ast.SEQ_CASE(pat,states,mfunc n,attr)))
	     cases) in
      let cts_sum = sum cts in
      let (ct,mfunc) = count_exp EXP env exp in
      let (defct,defmfunc) =
	(match default with
	  None -> (0, function x -> None)
	| Some x -> (* drop the default *)
	    let (dct,dmfunc) = count_stmt env x in
	    (1 + dct, function n -> Some (dmfunc n))) in
      let cur_count =
	1 (* drop the for statement *) + cts_sum + pcts_sum + ct + defct in
      (cur_count,
       function n ->
	 select n x
	   ([(1,
	      function n ->
		Printf.printf "dropping switch statement in %s\n"
		  (B.loc2c attr);
		Ast.SEQ([],[],attr));
	     (ct, function n -> Ast.SWITCH(mfunc n,cases,default,attr));
	     (pcts_sum,
	      function n ->
		select_list n x pcts pmfuncs cases
		  (function cases -> Ast.SWITCH(exp,cases,default,attr)));
	     (cts_sum,
	      function n ->
		select_list n x cts mfuncs cases
		  (function cases -> Ast.SWITCH(exp,cases,default,attr)))] @
	    (if defct > 0
	    then
	      [(1,function n -> Ast.SWITCH(exp,cases,None,attr));
	       (defct-1,
		function n -> Ast.SWITCH(exp,cases,defmfunc n,attr))]
	    else [])))
	
  | Ast.SEQ(decls,stms,attr) as x ->
      let (env,dcts,dmfuncs) = count_decls decls env in
      let dcts_sum = sum dcts in
      let (cts,mfuncs) = List.split (List.map (count_stmt env) stms) in
      let cts_sum = sum cts in
      let cur_count = dcts_sum + cts_sum in
      (cur_count,
       function n ->
	 select n x
	   [(dcts_sum,
	     function n ->
	       select_list n x dcts dmfuncs decls
		 (function decls -> Ast.SEQ(decls,stms,attr)));
	    (cts_sum,
	     function n ->
	       select_list n x cts mfuncs stms
		 (function stms -> Ast.SEQ(decls,stms,attr)))])
	
  | Ast.RETURN(Some(exp),attr) as x ->
      let (ct,mfunc) = count_exp EXP env exp in
      let cur_count = 1 + ct in
      (cur_count,
       function n ->
	 select n x
	   [(1,
	     function n ->
	       Printf.printf "dropping return statement in %s\n"
		 (B.loc2c attr);
	       Ast.SEQ([],[],attr));
	    (ct,function n -> Ast.RETURN(Some(mfunc n),attr))])
	
  | Ast.RETURN(None,attr) as x ->
      (* omit the statement *)
      (1,
       function n ->
	 select n x
	   [(1,
	     function n ->
	       Printf.printf "dropping return statement in %s\n"
		 (B.loc2c attr);
	       Ast.SEQ([],[],attr))])
	
  | Ast.MOVE(exp,state,tsrc,vsrc,dst,auto_allowed,state_end,attr) as x ->
      let (ct,mfunc) = count_exp EXP env exp in
      let options = remove_state state (!all_states) in
      let olen = List.length options in
      let cur_count = 1 + ct + olen in
      (cur_count,
       function n ->
	 select n x
	   [(1,
	     function n ->
	       Printf.printf "dropping move statement in %s\n"
		 (B.loc2c attr);
	       Ast.SEQ([],[],attr));
	    (olen,
	     function n ->
	       let chosen = List.nth options n in
	       Printf.printf "changed move destination in %s to %s\n"
		 (B.loc2c attr) (B.id2c chosen);
	       match CS.concretize chosen with
		 [concretized] ->
		   Ast.MOVE(exp,chosen,tsrc,vsrc,Some concretized,
			    auto_allowed,state_end,attr)
	       | _ -> raise (Error.Error "internal error"));
	    (ct,
	     function n ->
	       Ast.MOVE(mfunc n,state,tsrc,vsrc,dst,auto_allowed,
			state_end,attr))])
	
  | Ast.MOVEFWD(exp,countsrcsdsts,state_end,attr)
  | Ast.SAFEMOVEFWD(exp,countsrcsdsts,_,state_end,attr) ->
      raise (Error.Error "virtual schedulers not handled")
	
  | Ast.ASSIGN(loc,exp,sorted_fld,attr) as x ->
      let (ct1,mfunc1) = count_exp LOC env loc in
      let (ct2,mfunc2) = count_exp EXP env exp in
      let cur_count = 1 (* omit the statement *) + ct1 + ct2 in
      (cur_count,
       function n ->
	 select n x
	   [(1,
	     function n ->
	       Printf.printf "dropping assign statement in %s\n"
		 (B.loc2c attr);
	       Ast.SEQ([],[],attr));
	    (ct1,function n -> Ast.ASSIGN(mfunc1 n,exp,sorted_fld,attr));
	    (ct2,function n -> Ast.ASSIGN(loc,mfunc2 n,sorted_fld,attr))])
	  
  | Ast.DEFER(attr) ->
      raise (Error.Error "virtual schedulers not handled")
	
  | Ast.PRIMSTMT(f,args,attr) as x ->
      let (cts,mfuncs) = List.split (List.map (count_exp EXP env) args) in
      let cur_count = 1 (* omit the statement *) + sum cts in
      (cur_count,
       function n ->
	 select n x
	   [(1,
	     function n ->
	       Printf.printf "dropping prim statement in %s\n"
		 (B.loc2c attr);
	       Ast.SEQ([],[],attr));
	    (cur_count-1,
	     function n ->
	       select_list n x cts mfuncs args
		 (function args -> Ast.PRIMSTMT(f,args,attr)))])

  | Ast.BREAK(attr) as x ->
      (* omit the statement *)
      (1,
       function n ->
	 select n x
	   [(1,
	     function n ->
	       Printf.printf "dropping break statement in %s\n"
		 (B.loc2c attr);
	       Ast.SEQ([],[],attr))])
	
  | Ast.ERROR(str,attr) as x ->
      (* omit the statement *)
      (1,
       function n ->
	 select n x
	   [(1,
	     function n ->
	       Printf.printf "dropping error statement in %s\n"
		 (B.loc2c attr);
	       Ast.SEQ([],[],attr))])
	
  | Ast.ASSERT(expr,attr) as x -> (0, function n -> x)
	
(* ---------------------------- Handlers ---------------------------- *)
	
let process_handlers handlers env =
  let (cts,mfuncs) =
    List.split
      (List.map
	 (function
	     Ast.EVENT(nm,specd,param,stmt,attr) ->
	       let Ast.VARDECL(ty,id,imported,vattr) = param in
	       let (ct,mfunc) = count_stmt ((id,ty)::env) stmt in
	       (ct,function n -> Ast.EVENT(nm,specd,param,mfunc n,attr)))
	 handlers) in
  let cur_count = sum cts in
  let chosen = Random.int cur_count in
  Printf.printf "chose: %d out of %d\n" chosen cur_count;
  let _ =
    List.fold_left2
      (function acc ->
	function Ast.EVENT(nm,specd,param,stmt,attr) ->
	  function ct ->
	    let total = acc + ct in
	    Printf.printf "%s: %d\n" (Ast.event_name2c nm) total;
	    total)
      0 handlers cts in
  select_list chosen handlers cts mfuncs handlers
    (function handlers -> handlers)
    
(* --------------------------- Entry point -------------------------- *)
    
let mutate
    (Ast.SCHEDULER(nm,default,
		   cstdefs,enums,dom,grp,procdecls,fundecls,valdefs,states,
		   criteria,admit,trace,handlers,ifunctions,functions,
		     aspects,attr) as ast) =
  if !B.mutate
  then
    begin
      Random.self_init();
      all_states :=
	List.map (function CS.STATE(id,_,_,_) -> id) (CS.all_states());
      all_states_descriptions :=
	(!all_states) @
	(List.map (function x -> B.mkId(CS.class2c x)) (CS.all_classes false));
      let handlers =
	process_handlers handlers
	  (List.map
	     (function
		 Ast.VALDEF(Ast.VARDECL(ty,id,_,_),e,_,_) -> (id,ty)
	       | Ast.SYSDEF(Ast.VARDECL(ty,id,_,_),_,_) -> (id,ty)
	       | Ast.UNINITDEF(_,_) ->
		   raise (Error.Error "unexpected uninit def")
	       | Ast.DUMMYDEF(_,_,_) ->
		   raise (Error.Error "unexpected dummy def"))
	     valdefs) in
      (Ast.SCHEDULER(nm,default,
		     cstdefs,enums,dom,grp,procdecls,fundecls,valdefs,states,
		     criteria,admit,trace,handlers,ifunctions,functions,
		       aspects,attr))
    end
  else ast
