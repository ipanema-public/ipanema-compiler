(* Gather information about which shared resources are used in which handler *)

module CS = Class_state
module B = Objects

(* --------------------------- Expressions -------------------------- *)

let locks = ref []
  
type lock = B.identifier * B.locktyp

let get_id = function
    Ast.VAR(id, _, attr)
  | Ast.FIELD(_, id, _, attr)
  | Ast.EMPTY(id,_,_,_,attr) -> id

let lock_id
    (phi : lock list)
    (id : B.identifier) =
  List.mem_assoc id phi
  
let lock_term
    (phi : (lock list * lock list * lock list)) (* For global var., core var. and process var. *)
    (lambda : Ast.expr list)
    (exp : Ast.expr) =
  match exp with
    Ast.VAR(id, _, attr) 
  | Ast.FIELD(_, id, _, attr)
  | Ast.EMPTY(id,_,_,_,attr) ->
     begin
       match B.ty attr with
	 B.PROCESS ->
	   let (g, c, p) = phi in
	   if  lock_id p id then
	     Aux.union lambda [exp]
	   else lambda
	     
       | B.CORE | B.QUEUE B.PROCESS ->
	  let (g, c, p) = phi in
	  if  lock_id c id then
	    Aux.union lambda [exp]
	  else lambda
       | _ ->
	  let (g, c, p) = phi in
	  if  lock_id g id then
	    Aux.union lambda [exp]
	  else lambda
     end
  | _ -> Pp.pretty_print_exp exp;
    failwith (__LOC__ ^": Unsupported expression")
          
let rec lock_exp
    (phi : (lock list * lock list * lock list)) (* For global var., core var. and process var. *)
    (lambda : Ast.expr list)
    (exp : Ast.expr)
    : Ast.expr list =
  match exp with
    Ast.SUM _ | Ast.MIN _ | Ast.MAX _ | Ast.COUNT _ | Ast.FNOR _ | Ast.DISTANCE _ ->
                 lambda
    | Ast.INT(n,attr) -> lambda
    | Ast.VAR(id,_,attr) as var -> lock_term phi lambda var
    | Ast.FIELD(exp2,fld,_,attr) as fldaccess ->
       let l1 = lock_exp phi lambda exp2 in 
       lock_term phi l1 fldaccess

    | Ast.BOOL(v,attr) -> lambda
    | Ast.PARENT(attr) -> lambda
    | Ast.UNARY(uop,exp,ln) -> lock_exp phi lambda exp
    | Ast.BINARY(bop,exp1,exp2,ln) | Ast.PBINARY(bop, exp1, _, exp2, _, ln) ->
       let l1 = lock_exp phi lambda exp1 in
       lock_exp phi l1 exp2
    | Ast.TERNARY(exp1,exp2, exp3,ln) ->
       let l1 = lock_exp phi lambda exp1 in
       let l2 = lock_exp phi lambda exp2 in
       lock_exp phi l2 exp3
    | Ast.INDR(exp,attr) -> lock_exp phi lambda exp
    | Ast.SELECT(_,attr) -> lambda
    | Ast.FIRST(exp, crit, _,attr) -> lock_exp phi lambda exp
    | Ast.EMPTY(id,_,vl,insrc,attr) as empty ->
       lock_term phi lambda empty
    | Ast.VALID(exp,_,attr) ->
      lock_exp phi lambda exp
    | Ast.PRIM(fn,exps,attr) ->
       List.fold_left (lock_exp phi) lambda exps
    | Ast.SCHEDCHILD(exp,procs,attr) ->
      lock_exp phi lambda exp
    | Ast.SRCONSCHED(attr) -> lambda
    | Ast.ALIVE(tv,insrc,attr) -> lambda
    | Ast.AREF(_,_,attr) -> lambda

    | Ast.IN(exp,id,_,vl,insrc,crit,attr) ->
      lock_exp phi lambda exp
    | Ast.MOVEFWDEXP(_,_,_) -> lambda
    | Ast.SELF _ -> lambda
    | exp -> Pp.pretty_print_exp exp;
      failwith (__LOC__ ^": Unimplemented expression")

(* --------------------- Local Variable Declarations -------------------------- *)
let rec lock_decl
    (phi : (lock list * lock list * lock list)) (* For global var., core var. and process var. *)
    (lambda : Ast.expr list)
    (decl : Ast.valdef)
    : Ast.expr list =
    match decl with
        Ast.VALDEF(vardecl,exp,isconst,attr) ->
        lock_exp phi lambda exp

(* --------------------------- Statements -------------------------- *)

let rec lock_stmt
    (phi : (lock list * lock list * lock list)) (* For global var., core var. and process var. *)
    (lambda : Ast.expr list)
    (stmt : Ast.stmt)
    : Ast.expr list =
    match stmt with
      Ast.IF(exp,stmt1,stmt2,attr) ->
      let l1 = lock_exp phi lambda exp in
      let l2 = lock_stmt phi l1 stmt1 in
      lock_stmt phi l2 stmt2
    | Ast.FOR_WRAPPER(label,stmts,attr) -> raise (Error.Error "cannot occur")
    | Ast.FOR(id,Some state_ids,_,dir,stmt,crit,attr) ->
      let lambda = List.fold_left (lock_exp phi) lambda state_ids in
      lock_stmt phi lambda stmt
    | Ast.SWITCH(exp,cases,default,attr) ->
      lambda
    | Ast.SEQ(decls,stmts,attr) ->
      let lambda = List.fold_left (lock_decl phi) lambda decls in
      List.fold_left (lock_stmt phi) lambda stmts
    | Ast.RETURN(None,attr) -> lambda
    | Ast.RETURN(Some exp,attr) ->
      lock_exp phi lambda exp
    | Ast.MOVE(exp,state,_,_,_,auto_allowed,state_end,attr) ->
      let lambda = lock_exp phi lambda state in
      lock_exp phi lambda exp
    | Ast.MOVEFWD(exp,_,state_end,attr) ->
	  lock_exp phi lambda exp
    | Ast.SAFEMOVEFWD(exp,_,stm,state_end,attr) ->
	  lock_exp phi lambda exp
    | Ast.ASSIGN(expl,expr,sorted_fld,attr) ->
	  let l1 = lock_exp phi lambda expr in
	  lock_exp phi l1 expl
	    
    | Ast.DEFER(attr) -> lambda

    | Ast.BREAK(attr) -> lambda
    | Ast.CONTINUE(attr) -> lambda

    | Ast.PRIMSTMT(fn,exps,attr)  ->
       List.fold_left (lock_exp phi) lambda exps
	 
    | Ast.ERROR(str,attr) -> lambda
    | Ast.ASSERT(exp,_) ->
	  lock_exp phi lambda exp

    | Ast.STEAL (exp, _) ->
	  lock_exp phi lambda exp

    | _ ->
       failwith (__LOC__ ^": Unimplemented statement !")

(* --------------------------- Handlers -------------------------- *)
let psi
  (typedef : B.locktyp)
  (event : string)
  : B.lockscope =
  match (typedef, event) with
    (B.CORESTATE, "new") -> B.EVENTLOCK
  | (B.GLOBALVAR, "new") -> B.USELOCK
  | (B.SHAREDLOCAL, "new") -> B.EVENTLOCK
  | (B.PRIVATELOCAL, "new") -> B.NOLOCK

  | (B.CORESTATE, "detach") -> B.EVENTLOCK
  | (B.GLOBALVAR, "detach") -> B.USELOCK
  | (B.SHAREDLOCAL, "detach") -> B.EVENTLOCK
  | (B.PRIVATELOCAL, "detach") -> B.NOLOCK

  | (B.CORESTATE, "tick") -> B.EVENTLOCK
  | (B.GLOBALVAR, "tick") -> B.USELOCK
  | (B.SHAREDLOCAL, "tick") -> B.EVENTLOCK
  | (B.PRIVATELOCAL, "tick") -> B.NOLOCK

  | (B.CORESTATE, "yield") -> B.EVENTLOCK
  | (B.GLOBALVAR, "yield") -> B.USELOCK
  | (B.SHAREDLOCAL, "yield") -> B.EVENTLOCK
  | (B.PRIVATELOCAL, "yield") -> B.NOLOCK

  | (B.CORESTATE, "block") -> B.EVENTLOCK
  | (B.GLOBALVAR, "block") -> B.USELOCK
  | (B.SHAREDLOCAL, "block") -> B.EVENTLOCK
  | (B.PRIVATELOCAL, "block") -> B.NOLOCK

  | (B.CORESTATE, "unblock") -> B.EVENTLOCK
  | (B.GLOBALVAR, "unblock") -> B.USELOCK
  | (B.SHAREDLOCAL, "unblock") -> B.EVENTLOCK
  | (B.PRIVATELOCAL, "unblock") -> B.NOLOCK

  | (B.CORESTATE, "schedule") -> B.EVENTLOCK
  | (B.GLOBALVAR, "schedule") -> B.USELOCK
  | (B.SHAREDLOCAL, "schedule") -> B.EVENTLOCK
  | (B.PRIVATELOCAL, "schedule") -> B.NOLOCK

  | (B.CORESTATE, "core_entry") -> B.EVENTLOCK
  | (B.GLOBALVAR, "core_entry") -> B.USELOCK
  | (B.SHAREDLOCAL, "core_entry") -> B.EVENTLOCK
  | (B.PRIVATELOCAL, "core_entry") -> B.NOLOCK

  | (B.CORESTATE, "core_exit") -> B.EVENTLOCK
  | (B.GLOBALVAR, "core_exit") -> B.USELOCK
  | (B.SHAREDLOCAL, "core_exit") -> B.EVENTLOCK
  | (B.PRIVATELOCAL, "core_exit") -> B.NOLOCK

  | (B.CORESTATE, "newly_idle") -> B.EVENTLOCK
  | (B.GLOBALVAR, "newly_idle") -> B.USELOCK
  | (B.SHAREDLOCAL, "newly_idle") -> B.EVENTLOCK
  | (B.PRIVATELOCAL, "newly_idle") -> B.NOLOCK

  | (B.CORESTATE, "enter_idle") -> B.EVENTLOCK
  | (B.GLOBALVAR, "enter_idle") -> B.USELOCK
  | (B.SHAREDLOCAL, "enter_idle") -> B.EVENTLOCK
  | (B.PRIVATELOCAL, "enter_idle") -> B.NOLOCK

  | (B.CORESTATE, "exit_idle") -> B.EVENTLOCK
  | (B.GLOBALVAR, "exit_idle") -> B.USELOCK
  | (B.SHAREDLOCAL, "exit_idle") -> B.EVENTLOCK
  | (B.PRIVATELOCAL, "exit_idle") -> B.NOLOCK

  | (B.CORESTATE, "balancing") -> B.EVENTLOCK
  | (B.GLOBALVAR, "balancing") -> B.USELOCK
  | (B.SHAREDLOCAL, "balancing") -> B.EVENTLOCK
  | (B.PRIVATELOCAL, "balancing") -> B.NOLOCK
 
  |_ -> failwith ("don't know the locking characteristics of the event "^event)

let lock_handlers phi lambda handlers =
  List.map
    (function
	(Ast.EVENT((Ast.EVENT_NAME(ids,_) as nm),param,stmt,syn,attr)) ->
        let id = (B.id2c ids) in
        let var_to_lock = lock_stmt phi lambda stmt in
	let (g, c, p) = phi in
	let flat = g @ c @ p in
        let lambda =
	  List.map
	    (function var ->
	      let varid = get_id var in
	      (varid, (psi (List.assoc varid flat) id))
	    ) var_to_lock in
	locks := (id, var_to_lock)::!locks;
	  Ast.EVENT(nm, param, stmt, syn, B.updlck attr lambda))
    handlers

(* --------------------------- Variable Declarations -------------------------- *)
let lock_shared_local
    (phi : (lock list)) (* Any of global var., core var. and process var. *)
    (state : Ast.state) =
  match state with
    Ast.QUEUE(_,shared,_,id,_,_,_)
  | Ast.PROCESS(_,shared,id,_,_,_) ->
    match shared with
      CS.SHARED -> Aux.union phi [(id, B.SHAREDLOCAL)]
    | CS.UNSHARED -> Aux.union phi [(id, B.PRIVATELOCAL)]

let lock_pcstates
    (phi : (lock list * lock list * lock list)) (* For global var., core var. and process var. *)
    (states : Ast.pcstate) =
  match states with
    Ast.PSTATE(decls) ->
      let (g, c, p) = phi in
      let g = List.fold_left lock_shared_local g decls in
      (g, c, p)
    | Ast.CORE(core_variables, decls, _) ->
      let (g, c, p) = phi in
      let c = List.fold_left lock_shared_local c decls in
      (g, c, p)

let lock_core_states
    (phi : (lock list * lock list * lock list)) (* For global var., core var. and process var. *)
    (cstate : Ast.cstate) =
  match cstate with
    Ast.CSET(_,id,_) | Ast.CSTATE(_,id,_) ->
      let (g, c, p) = phi in
      let g = Aux.union g [(id, B.CORESTATE)] in
      (g, c, p)

     

let lock_local_decl
    (phi : (lock list * lock list * lock list)) (* For global var., core var. and process var. *)
    (vardecl : Ast.vardecl) =
  match vardecl with
    Ast.VARDECL(ty,id,imported,lz,_,dexp,attr1) ->
      let (g, c, p) = phi in
      let g = Aux.union g [(id, B.PRIVATELOCAL)] in
      (g, c, p)
	
let lock_global_vars
    (phi : (lock list * lock list * lock list)) (* For global var., core var. and process var. *)
    (valdef : Ast.valdef) =
  match valdef with
    Ast.VALDEF(Ast.VARDECL(ty,id,imported,lz,_,dexp,attr1),exp,isconst,attr2) ->
      let (g, c, p) = phi in
      let g = Aux.union g [(id, B.GLOBALVAR)] in
      (g, c, p)
  | Ast.UNINITDEF(Ast.VARDECL(_,id,_,_,_,_,_),_)
  | Ast.SYSDEF(Ast.VARDECL(_,id,_,_,_,_,_),_,_)
  | Ast.DUMMYDEF(id,_,_) ->
      let (g, c, p) = phi in
      let g = Aux.union g [(id, B.PRIVATELOCAL)] in
      (g, c, p)

(************************ entry point ****************************************)
let analyze (Ast.SCHEDULER(nm,
		     cstdefs,enums,dom,grp,procdecls,fundecls,valdefs,domains,states,cstates,
		     criteria,trace,handlers,chandlers,ifunctions,functions,
                     attr)) =
  (*phase 1: collect global and shared variables: locking candidates*)
  let phi = lock_pcstates ([],[],[]) states in
  let phi = List.fold_left lock_core_states phi cstates in
  let phi = List.fold_left lock_global_vars phi valdefs in
  let phi = List.fold_left lock_local_decl phi procdecls in
  (*phase 2: associate each handler with the locking candidates it uses*)
  let handlers = lock_handlers phi [] handlers in
  let chandlers = lock_handlers phi [] chandlers in
  (Ast.SCHEDULER(nm,
		 cstdefs,enums,dom,grp,procdecls,fundecls,valdefs,domains,states,cstates,
		 criteria,trace,handlers,chandlers,ifunctions,functions,
                 attr))
