module B = Objects
module VB = Vobj
module VB2 = Vobjects
module CS = Class_state

let event_param = ref((B.mkId ""), B.PEVENT, B.PROCESS)

(* ------------------------- miscellaneous ------------------------- *)

(* Set to true when a new state is the destination of a move from running
in an interrupt event.  In this case, the quasitypes have to be reapplied
to the pending configurations. *)
let redo_quasitypes = ref false
let in_a_system_event = ref false
let idest_states = ref ([] : CS.state_info list)
let idest_redo_name = Ast.mkEVENT_NAME(["idest event"],false,0)
let system_events = ref ([] : Ast.event_name list)
let quasitype_failed = ref false
let quasitype_failed_states =
  ref ([] :
	 (Ast.event_name *
	    (CS.state_info list * Ast.event_name list * int * VB.state_env))
	 list)

let classify_states state_infos state_env =
  let cds =
    List.map (function si -> VB.state_info_lookup 1 si state_env)
      state_infos in
  let prs = List.combine state_infos cds in
  List.fold_left
    (function (empty,nonempty,unknown) ->
      function
	  (si,VB.EMPTY) as x    -> (x::empty,nonempty,unknown)
	| (si,VB.MM([],_)) as x -> (empty,nonempty,x::unknown)
	| (si,_) as x           -> (empty,x::nonempty,unknown))
    ([],[],[]) prs

let src_tgt_handler = ref false
let tgt_only_at_start = ref true (* for srconsched, false if no src *)

let illegal_destinations = ref ([] : CS.state_info list)

let return_state = ref ([] : VB.state_env list)

(* no need for separate break/continue state? *)
let break_or_cont_state =
  ref ([] : (VB.var_env * VB.state_env * B.identifier list) list list)

let forwardImmediateOutputs = ref ([] : CS.classname list)

(* state in app automaton and initial configuration *)
let current_input_config = ref (0,([] : VB.state_env))

let pr = Printf.sprintf

let estrs = ref ([] : string list)

let type_error_lines = ref ([] : string list)

let error (loc,str) =
  let estr =
    match loc with
      Some lstr -> pr "%s: %s%s" lstr (!VB.handler_name) str
    | None -> pr "%s%s" (!VB.handler_name) str in
  if not (List.mem estr (!estrs))
  then
    begin
      Error.update_error();
      estrs := estr :: (!estrs);
      Printf.printf "%s\n" estr
    end
  else ()

let always_error (loc,str) =
  let estr =
    match loc with
      Some lstr -> pr "%s: %s%s" lstr (!VB.handler_name) str
    | None -> pr "%s%s" (!VB.handler_name) str in
  Error.update_error();
  estrs := estr :: (!estrs);
  Printf.printf "%s\n" estr

let wstrs = ref ([] : string list)

let warning(loc,str) =
  let wstr =
    match loc with
      Some lstr -> pr "%s: warning: %s%s" lstr (!VB.handler_name) str
    | None -> pr "warning: %s%s" (!VB.handler_name) str in
  if not (List.mem wstr (!wstrs))
  then (wstrs := wstr :: (!wstrs); Printf.printf "%s\n" wstr)

let internal_error(loc,n) =
  let err_str =
    match loc with
      Some str -> pr "%s: %sinternal error %i" str (!VB.handler_name) n
    | None -> pr "%sinternal error %i" (!VB.handler_name) n in
  raise(Error.Error err_str)

let assert_warning(loc,str) =
  let (automaton_state,config) = !current_input_config in
  (match loc with
    Some lstr ->
      Printf.printf "%s: %s%s assert added\n" lstr (!VB.handler_name) str
  | None -> Printf.printf "%s assert added\n" str);
  if automaton_state >= 0
  then Printf.printf "%s\n" (VB.state_env2c config);
  Printf.printf "\n"

let rec lookup_id x env =
  let (_,v) = List.find (function (y,v) -> B.ideq(x,y)) env in
  v

(* ------------------- Looking up event handlers ------------------- *)

let match_event_prev prev nm events =
  List.filter
    (function (Ast.EVENT(name,specd,param,stmt,attr)) ->
      Ast.event_names_equal prev specd && Ast.event_name_equal nm name)
    events

let handler_name_lookup env nm : Ast.event_name list =
  let all_matches =
    Aux.option_filter
      (function (ev_name,handler_names) ->
	if Ast.event_name_equal ev_name nm
	then Some handler_names
	else None)
      env in
  match all_matches with
    [x] -> x
  | l ->
      error(None,pr "handler %s found %d times"
	      (Ast.event_name2c nm) (List.length l));
      raise (Error.Error "fail")

let lookup_handler nm env prev events orig_events =
  let concrete_handlers = handler_name_lookup env nm in
  List.flatten
    (List.map
       (function nm ->
	 let first_try = match_event_prev prev nm events in
	 match (prev,first_try) with
	   (x::xs,[]) ->
	     List.map
	       (function Ast.EVENT(name,_,param,stmt,attr) ->
		 Ast.EVENT(name,prev,param,stmt,attr))
	       (match_event_prev [] nm orig_events)
	 | (_,res) -> res)
       concrete_handlers)

(* -------------------- management of assertions ------------------- *)

let assertions = ref ([] : Ast.stmt list)

let add_non_empty_assert (CS.STATE(id,_,_,_) as state_info) a =
  assert_warning(Some(B.loc2c a),"non empty");
  assertions :=
    (Ast.ASSERT(Ast.UNARY(Ast.NOT,
			  Ast.EMPTY(id,[state_info],B.BOTH,false,
				    B.updty a B.BOOL),
			  B.updty a B.BOOL),
		a)) ::
    (!assertions)

let add_empty_assert (CS.STATE(id,_,_,_) as state_info) a =
  assert_warning(Some(B.loc2c a),"empty");
  assertions :=
    (Ast.ASSERT(Ast.EMPTY(id,[state_info],B.BOTH,false,B.updty a B.BOOL),a)) ::
    (!assertions)

let add_is_src_or_tgt_assertion exp attr =
  assert_warning(Some(B.loc2c attr),"is source or target");
  let new_attr = B.mkAttr(B.line attr) in
  let bool_attr = B.updty new_attr B.BOOL in
  let (decls,new_exp) = Ast.name_expr exp new_attr in
  let mk_test src_tgt = Ast.BINARY(Ast.EQ,new_exp,src_tgt,bool_attr) in
  let fld_exp field =
    let new_attr = B.mkAttr(B.line attr) in
    let (ev_id, ev_typ, fld_typ) = !event_param in
    Ast.FIELD(Ast.VAR(ev_id,[],B.updty new_attr ev_typ),B.mkId field,
          [],B.updty new_attr fld_typ) in
  let test =
    Ast.BINARY(Ast.OR,mk_test (fld_exp "source"),mk_test (fld_exp "target"),
	       bool_attr) in
  assertions := Ast.ASSERT(test,new_attr) :: (!assertions)

let add_assertions assertions stmt =
  match assertions with
    [] -> stmt
  | _ -> Ast.SEQ([],assertions@[stmt],Ast.get_stm_attr stmt)

(* -------- Expression analysis just to look for assertions -------- *)

let rec verif_exp exp var_env state_env nonzero : Ast.expr * VB.state_env =
  let no_info = (exp,state_env) in
  match exp with
    Ast.INT(n,a)             -> no_info
  | Ast.VAR(id,_,a)            ->
      (try
	let (state_info,cd) =
	  List.find
	    (function (CS.STATE(name,_,_,_),_) -> B.ideq (id, name))
	    state_env in
	(match state_info with
	  CS.STATE(_,_,CS.PROC,_) ->
	    (match cd with
	      VB.EMPTY ->
		error(Some(B.loc2c a),"referenced state must be non-empty");
		no_info
	    | VB.MM([],max) ->
		add_non_empty_assert state_info a;
		(exp,
		 VB.combine_extensions
		   [(state_info,VB.MM([VB.pdluball 1 max],max))]
		   state_env)
	    | VB.MM(min,max) -> no_info)
	| _ ->
	    (error(Some(B.loc2c a),"reference to a non-process variable state");
	    no_info))
      with Not_found -> no_info)
  | Ast.FIELD(exp,fld,_,a)     ->
      (match B.id2c fld with
	"source" -> let _ = VB.lookup_src_tgt VB.SRC state_env in no_info
      |	"target" -> let _ = VB.lookup_src_tgt VB.TGT state_env in no_info
      |	_ ->
	  let (exp,state_env) = verif_exp exp var_env state_env nonzero in
      (Ast.FIELD(exp,fld,[],a),state_env))
  | Ast.BOOL(bval,a)         -> no_info
  | Ast.PARENT(a)         -> no_info
  | Ast.UNARY(uop,exp,a)     ->
      let (exp,state_env) = verif_exp exp var_env state_env nonzero in
      (Ast.UNARY(uop,exp,a),state_env)
  | Ast.BINARY(bop,exp1,exp2,a) ->
      let (exp1,state_env) = verif_exp exp1 var_env state_env nonzero in
      let (exp2,state_env) = verif_exp exp2 var_env state_env nonzero in
      (match (bop,exp2) with
    (_,Ast.VAR(id,[],a)) when List.mem id nonzero -> ()
      |	(Ast.DIV,Ast.INT(0,a)) ->
	  error(Some(B.loc2c a), "denominator should not be 0")
      |	(Ast.MOD,Ast.INT(0,a)) ->
	  error(Some(B.loc2c a), "second argument of % should not be zero")
      |	(_,Ast.INT(_,a)) -> ()
      |	(Ast.DIV,_) ->
	  warning(Some(B.loc2c a), "denominator may be zero")
      | (Ast.MOD,_) ->
	  warning(Some(B.loc2c a), "second argument of % may be zero")
      |	_ -> ());
      (Ast.BINARY(bop,exp1,exp2,a),state_env)
  | Ast.PBINARY(bop,exp1,states1,exp2,states2,a) ->
      let (exp1,state_env) = verif_exp exp1 var_env state_env nonzero in
      let (exp2,state_env) = verif_exp exp2 var_env state_env nonzero in
      let get_states drop_extension =
	List.map (function (s,_) -> s) drop_extension in
      let (pd1,exp1,state_env,drop_pd_extension1) =
	verif_proc_exp exp1 var_env state_env in
      let (pd2,exp2,state_env,drop_pd_extension2) =
	verif_proc_exp exp2 var_env state_env in
      (Ast.PBINARY(bop,exp1,Aux.union (get_states drop_pd_extension1) states1,
		   exp2,Aux.union (get_states drop_pd_extension2) states2,a),
       state_env)
  | Ast.INDR(exp,a)          ->
      let (exp,state_env) = verif_exp exp var_env state_env nonzero in
      (Ast.INDR(exp,a),state_env)
  | Ast.SELECT(state_info,a) -> no_info
  | Ast.FIRST(fexp,crit, state_info,a) ->
    let (_,state_env) = verif_exp fexp var_env state_env nonzero in
    (exp,state_env)
  | Ast.EMPTY(_,state_infos,tvl,insrc,a) -> no_info
  | Ast.PRIM(fexp,args,a)    ->
      let (fexp,state_env) = verif_exp fexp var_env state_env nonzero in
      let (args,state_env) =
	List.fold_left
	  (function (args,state_env) ->
	    function arg ->
	      let (arg,state_env) = verif_exp arg var_env state_env nonzero in
	      (arg::args,state_env))
	  ([],state_env) args in
      let args = List.rev args in
      (Ast.PRIM(fexp,args,a),state_env)
  | Ast.SCHEDCHILD(_,proc,a)     ->
      let (pd,exp,state_env,drop_extension) =
	verif_proc_exp exp var_env state_env in
      (exp,state_env)
  | Ast.SRCONSCHED(a)        -> no_info
  | Ast.ALIVE(_,_,a)           -> no_info
  | Ast.AREF(aexp,iexp,a)    ->
      let (aexp,state_env) = verif_exp aexp var_env state_env nonzero in
      let (iexp,state_env) = verif_exp iexp var_env state_env nonzero in
      (Ast.AREF(aexp,iexp,a),state_env)
  | Ast.IN(proc,state,state_info,tvl,insrc,crit,a) ->
      let (proc,state_env) = verif_exp proc var_env state_env nonzero in
      (Ast.IN(proc,state,state_info,tvl,insrc,None,a),state_env)
  | Ast.MOVEFWDEXP(exp,a,b)    ->
      let (exp,state_env) = verif_exp exp var_env state_env nonzero in
      (Ast.MOVEFWDEXP(exp,a,b),state_env)

and enter_verif_exp exp var_env state_env nonzero =
  assertions := [];
  let res = verif_exp exp var_env state_env nonzero in
  (!assertions,res)

(* ---------------------- Process expressions ---------------------- *)

(* the first state env result is a complete environment where the process is
there (perhaps added information), and the second is a little environment
where the process has been removed. *)

and verif_proc_exp exp var_env state_env :
    VB.pd * Ast.expr * VB.state_env * VB.state_env =
  let notproc a = internal_error(Some(B.loc2c a),1) in
  let f e = VB.combine_extensions e state_env in
  match exp with
    Ast.INT(n,a)  -> notproc a
  | Ast.VAR(id,_,a) ->
      (try
	let (state_info,cd) =
	  List.find
	    (function (CS.STATE(name,_,_,_),_) -> B.ideq(id,name))
	    state_env in
	(match state_info with
	  CS.STATE(_,_,CS.PROC,_) ->
	    (match cd with
	      VB.EMPTY ->
		error(Some(B.loc2c a),
		      "referenced process variable found to be empty");
		(VB.UNKNOWN,exp,f [(state_info,cd)],[(state_info,VB.EMPTY)])
	    | VB.MM([],max) ->
		add_non_empty_assert state_info a;
		let pd = VB.pdluball 2 max in
		(pd,exp,f [(state_info,VB.MM([pd],max))],
		 [(state_info,VB.EMPTY)])
	    | VB.MM([pd],_) ->
		(pd,exp,f [(state_info,cd)],[(state_info,VB.EMPTY)])
	    | _ -> internal_error(Some(B.loc2c a),2))
	| _ -> raise(Error.Error("reference to a non-process variable state")))
      with Not_found ->
	let pd = VB.var_env_lookup id var_env a in
	let (in_env,out_env) = VB.lookup pd state_env in
	(pd,exp,f in_env,out_env))
  | Ast.FIELD(exp,fld,_,a) as x ->
      (match B.id2c fld with
	"source" ->
	  let pd = VB.lookup_src_tgt VB.SRC state_env in
	  let (in_env,out_env) = VB.lookup pd state_env in
	  (pd,x,f in_env,out_env)
      |	"target" ->
	  let pd = VB.lookup_src_tgt VB.TGT state_env in
	  let (in_env,out_env) = VB.lookup pd state_env in
	  (pd,x,f in_env,out_env)
      |	_ -> (* notproc a *)
	  let pd = VB.UNKNOWN in
	  let (in_env,out_env) = VB.lookup pd state_env in
	  (pd,x,f in_env,out_env))
  | Ast.BOOL(bval,a)            -> notproc a
  | Ast.PARENT(a)               -> notproc a
  | Ast.UNARY(uop,exp,a)        -> notproc a
  | Ast.BINARY(bop,exp1,exp2,a) -> notproc a
  | Ast.PBINARY(bop,exp1,states1,exp2,states2,a) -> notproc a
  | Ast.INDR(exp,a)             -> notproc a
  | Ast.SELECT(Some state_info,a) ->
      let cd = VB.state_info_lookup 2 state_info state_env in
      (match cd with
	VB.EMPTY ->
	  error(Some(B.loc2c a),"select from an empty state");
	  (VB.UNKNOWN,exp,f [(state_info,cd)],[(state_info,VB.EMPTY)])
      |	VB.MM(min,max) ->
	  let pd = VB.pdluball 3 max in
	  if List.length min <= 1
	  then (pd, exp, f [(state_info,cd)], [(state_info,VB.MM([],max))])
	  else (pd, exp, f [(state_info,cd)], [(state_info,VB.MM([pd],max))]))
  | Ast.SELECT(None,a)                      -> notproc a
  | Ast.FIRST(_, _,_, a) -> notproc a (* FIXME *)
  | Ast.EMPTY(state,state_info,tvl,insrc,a) -> notproc a
  | Ast.PRIM(fexp,args,a)                   -> notproc a
  | Ast.SCHEDCHILD(exp,procs,a) ->
      let (pd,exp,state_env,drop_extension) =
	verif_proc_exp exp var_env state_env in
      let src_tgt_check = (* next can only be applied to source or target *)
	match pd with
	  VB.FROMSTATE(VB.SRC,_) -> Some VB2.SRC
	| VB.FROMSTATE(VB.TGT,_) -> Some VB2.TGT
	| VB.FROMSTATE(VB.SRCANDTGT,_) -> Some VB2.TGT
	| _ -> None in
      (match src_tgt_check with
	Some x -> (pd,Ast.SCHEDCHILD(exp,Aux.union [x] procs,a),
		   state_env,drop_extension)
      |	None   ->
	  add_is_src_or_tgt_assertion exp a;
	  (pd,Ast.SCHEDCHILD(exp,[VB2.SRC;VB2.TGT],a),state_env,drop_extension))
  | Ast.SRCONSCHED(a)        -> notproc a
  | Ast.ALIVE(_,_,a)           -> notproc a
  | Ast.AREF(aexp,iexp,a)    -> notproc a
  | Ast.IN(proc,state,state_info,tvl,insrc,crit,a) -> notproc a
  | Ast.MOVEFWDEXP(exp,_,a)    -> notproc a

let enter_verif_proc_exp exp var_env state_env =
  assertions := [];
  let res = verif_proc_exp exp var_env state_env in
  (!assertions, res)

(* ---------------------- Boolean expressions ---------------------- *)

type bool_res =
    T | F
  | TF of VB.state_env (*true env*) * B.identifier list (*true nonzero vars*)
	* VB.state_env (*false env*) * B.identifier list (*false nonzero vars*)

let verif_bool_exp exp var_env state_env nonzero =
  let notbool a = internal_error(Some(B.loc2c a),3) in
  let no_info (exp,state_env) = (exp,state_env,TF([],[],[],[])) in
  match exp with
    Ast.INT(n,a)             -> notbool a
  | Ast.BOOL(bval,a)         -> (exp,state_env,if bval then T else F)
  | Ast.PARENT(a)
  | Ast.VAR(_,_,a)
  | Ast.FIELD(_,_,_,a)
  | Ast.UNARY(_,_,a)
  | Ast.INDR(_,a)       -> no_info (verif_exp exp var_env state_env nonzero)
  | Ast.BINARY(op,e1,e2,a) ->
      let (exp,state_env) = verif_exp exp var_env state_env nonzero in
      let id_arg =
	match e1 with
	  Ast.VAR(i,_,a) -> Some i
    | _ -> (match e2 with Ast.VAR(i,_,a) -> Some i | _ -> None) in
      let const_arg =
	match e1 with
	  Ast.INT(n,a) -> Some n
	| _ -> (match e2 with Ast.INT(n,a) -> Some n | _ -> None) in
      (match (op,id_arg,const_arg) with
	(Ast.EQ,Some i,Some 0)  -> (exp,state_env,TF([],[],[],[i]))
      | (Ast.EQ,Some i,Some n)  -> (exp,state_env,TF([],[i],[],[]))
      | (Ast.NEQ,Some i,Some 0) -> (exp,state_env,TF([],[i],[],[]))
      | (Ast.NEQ,Some i,Some n) -> (exp,state_env,TF([],[],[],[i]))
      | (Ast.GT,Some i,Some n)  ->
	  if n >= 0
	  then (exp,state_env,TF([],[i],[],[]))
	  else (exp,state_env,TF([],[],[],[i]))
      | (Ast.LT,Some i,Some n)  ->
	  if n <= 0
	  then (exp,state_env,TF([],[i],[],[]))
	  else (exp,state_env,TF([],[],[],[i]))
      | (Ast.GEQ,Some i,Some n) ->
	  if n > 0
	  then (exp,state_env,TF([],[i],[],[]))
	  else (exp,state_env,TF([],[],[],[i]))
      | (Ast.LEQ,Some i,Some n) ->
	  if n < 0
	  then (exp,state_env,TF([],[i],[],[]))
	  else (exp,state_env,TF([],[],[],[i]))
      | _ -> no_info (verif_exp exp var_env state_env nonzero))
  | Ast.PBINARY(op,e1,s1,e2,s2,a) ->
      no_info (verif_exp exp var_env state_env nonzero)
  | Ast.SELECT(state_info,a) -> notbool a
  | Ast.FIRST(_,_, state_info,a) -> notbool a (* FIXME *)
  | Ast.EMPTY(state,state_infos,tvl,insrc,a) ->
      let (all_empty,any_not_empty) =
	(* all_empty is true if all are known to be empty
	   any_non_empty is true if any is actually known to be non-empty *)
	List.fold_left
	  (function (all_empty,any_not_empty) ->
	    function state_info ->
	      let cd = VB.state_info_lookup 3 state_info state_env in
	      match cd with
		VB.EMPTY -> (all_empty,any_not_empty)
	      | VB.MM(_::_,_) -> (false,true)
	      | _ -> (false,any_not_empty))
	  (true,false) state_infos in
      (match (all_empty,any_not_empty) with
	(true,_) ->
	  (Ast.EMPTY(state,state_infos,B.lubtestval(B.TRUE,tvl),insrc,a),
	   state_env,T)
      |	(false,true) ->
	  (Ast.EMPTY(state,state_infos,B.lubtestval(B.FALSE,tvl),insrc,a),
	   state_env,F)
      |	_ ->
	  (Ast.EMPTY(state,state_infos,B.lubtestval(B.BOTH,tvl),insrc,a),
	   state_env,
	   TF(List.map (function state_info -> (state_info,VB.EMPTY))
		state_infos,
	      [],
	      List.map
		(function state_info ->
		  let cd = VB.state_info_lookup 4 state_info state_env in
		  let (min,max) = VB.get_mm cd in
		  (state_info,VB.MM([VB.pdluball 4 max],max)))
		state_infos,
	       [])))
  | Ast.PRIM(fexp,args,a) -> no_info (verif_exp exp var_env state_env nonzero)
  | Ast.SCHEDCHILD(exp,proc,a)     -> notbool a
  | Ast.SRCONSCHED(a) when !tgt_only_at_start -> (exp,state_env,F)
  | Ast.SRCONSCHED(a) ->
      (* if src is in the nowhere state then surely false *)
      let nowhere_state_cds =
	Aux.option_filter
	  (function
	      (CS.STATE(_,CS.NOWHERE,_,_),cd) -> Some cd
	    | _ -> None)
	  state_env in
      let src_among_mins =
	List.exists
	  (function cd ->
	    let (min,max) = VB.get_mm cd in
	    List.exists
	      (function
		  VB.FROMSTATE(VB.SRC,_)
		| VB.FROMSTATE(VB.SRCANDTGT,_) -> true
		| _ -> false)
	      min)
	  nowhere_state_cds in
      (* if not in nowhere and all are empty (as at the beginning) then false*)
      let all_empty =
	List.for_all
	  (function
	      (CS.STATE(_,CS.NOWHERE,_,_),cd) -> true
	    | (_,VB.EMPTY) -> true
	    | _ -> false)
	  state_env in
      if src_among_mins || all_empty
      then (exp,state_env,F)
      else no_info (exp,state_env)
  | Ast.ALIVE(_,_,a) -> no_info (verif_exp exp var_env state_env nonzero)
  | Ast.AREF(aexp,iexp,a)    -> notbool a
  | Ast.IN(proc,state,state_info,tvl,insrc,crit,a) ->
      let (pd,proc,state_env,drop_pd_extension) =
	verif_proc_exp proc var_env state_env in
      let first_try _ =
	match drop_pd_extension with
	  [(found_state_info,_)] ->
	    (if List.mem found_state_info state_info
	    then
	      let cd = VB.state_info_lookup 5 found_state_info state_env in
	      let (min,max) = VB.get_mm cd in
	      if List.mem pd min
	      then
		let tvl1 = B.lubtestval(B.TRUE,tvl) in
		Some(Ast.IN(proc,state,state_info,tvl1,insrc,crit,a),state_env,T)
	      else None
	    else None)
	| _ -> None in
      let second_try _ =
	if List.exists
	    (function (found_state_info,_) ->
	      List.mem found_state_info state_info)
	    drop_pd_extension
	then None
	else
	  let tvl1 = B.lubtestval(B.FALSE,tvl) in
	  Some (Ast.IN(proc,state,state_info,tvl1,insrc,None,a),state_env,F) in
      let third_try _ =
	match state_info with
	  [state_info] ->
	    let apd = VB.alpha pd in
	    let cd = VB.state_info_lookup 6 state_info state_env in
	    let (min,max) = VB.get_mm cd in
	    (match Aux.intersect apd min with
	      [] ->
		let tvl1 = B.lubtestval(B.BOTH,tvl) in
		Some(Ast.IN(proc,state,[state_info],tvl1,insrc,None,a),
		     state_env,
		     TF([(state_info,
			  VB.MM(Aux.union [pd] min,Aux.union [pd] max))],
			[],[],[]))
	    | _ -> None)
	| _ -> None in
      (match first_try() with
	Some x -> x
      |	None ->
	  (match second_try() with
	    Some x -> x
	  | None ->
	      (match third_try() with
		Some x -> x
	      |	None ->
		  let tvl1 = B.lubtestval(B.BOTH,tvl) in
		  (Ast.IN(proc,state,state_info,tvl1,insrc,None,a),
		   state_env,TF([],[],[],[])))))
  | Ast.MOVEFWDEXP(exp,_,a)    -> notbool a

let enter_verif_bool_exp exp var_env state_env nonzero =
  assertions := [];
  let res = verif_bool_exp exp var_env state_env nonzero in
  (!assertions,res)

(* ------------------------ LHS expressions ------------------------ *)

let enter_verif_lhs_exp exp var_env state_env nonzero :
    Ast.stmt list * Ast.expr * VB.state_env * bool * B.identifier list =
  match exp with
    Ast.VAR(id,_,attr) -> ([],exp,[],false,Aux.remove id nonzero)
  | Ast.FIELD(exp,fld,_,attr) ->
      (match B.ty(Ast.get_exp_attr exp) with
	B.PROCESS | B.SRCPROCESS | B.SCHEDULER ->
	  let (assertions,(pd,exp,state_env,drop_pd_extension)) =
	    enter_verif_proc_exp exp var_env state_env in
      (assertions,Ast.FIELD(exp,fld,[],attr),state_env,
	   List.exists
	     (function
		 (CS.STATE(_,_,CS.QUEUE(CS.SELECT(_)),_),_) -> true
	       | _ -> false)
	     drop_pd_extension,
	   nonzero)
      |	_ ->
	  let (assertions,(exp,state_env)) =
	    enter_verif_exp exp var_env state_env nonzero in
      (assertions,Ast.FIELD(exp,fld,[],attr),state_env,false,nonzero))
  | _ -> internal_error(Some(B.loc2c(Ast.get_exp_attr exp)),4)

(* -------------------------- Declarations ------------------------- *)

let verif_decls decls var_env state_env nonzero :
    Ast.stmt list * Ast.valdef list * VB.var_env * VB.state_env =
  assertions := [];
  let (decls,var_env,state_env) =
    List.fold_left
      (function (decls,var_env,state_env) ->
	function
	    Ast.VALDEF(Ast.VARDECL(ty,id,x,lz,de,y),exp,isconst,attr) ->
	      (match ty with
		B.PROCESS | B.SCHEDULER ->
		  let (pd,exp,state_env,_) =
		    verif_proc_exp exp var_env state_env in
		  let var_env = VB.update_var_env id pd var_env in
		  (Ast.VALDEF(Ast.VARDECL(ty,id,x,lz,de,y),exp,isconst,attr)::decls,
		   var_env,state_env)
	      |	_ ->
		  let (exp,state_env) =
		    verif_exp exp var_env state_env nonzero in
		  (Ast.VALDEF(Ast.VARDECL(ty,id,x,lz,de,y),exp,isconst,attr)::decls,
		   var_env,state_env))
	  | Ast.UNINITDEF(decl,attr) as d -> (d::decls,var_env,state_env)
	  | _ -> internal_error(None,5))
      ([],var_env,state_env) decls in
  let decls = List.rev decls in
  (!assertions,decls,var_env,state_env)

(* ------------ Some functions that are useful for move ------------ *)

let check_empty_dest dest_info state_env attr :
    Ast.stmt list * VB.state_env * CS.state_type =
  let dest_cd = VB.state_info_lookup 7 dest_info state_env in
  assertions := [];
  let (state_env,dsttype) =
    match dest_info with
      CS.STATE(id,_,CS.PROC,_) ->
	(match dest_cd with
	  VB.EMPTY -> (state_env,CS.PROC)
	| VB.MM([],_) ->
	    add_empty_assert dest_info attr;
	    (VB.combine_extensions [(dest_info,VB.EMPTY)] state_env, CS.PROC)
	| _ ->
	    error (Some(B.loc2c attr), (pr "%s should be empty" (B.id2c id)));
	    (state_env,CS.PROC))
    | CS.STATE(_,_,dsttype,_) -> (state_env,dsttype) in
  (!assertions,state_env,dsttype)

let move_to_state srcs pd dest_info state_env =
  (if !in_a_system_event
  then
    begin
      match srcs with
	None -> ()
      | Some srcs ->
	  if List.mem CS.RUNNING (List.map CS.state2class srcs) &&
	    not(List.mem dest_info (!idest_states))
	  then
	    begin
	      redo_quasitypes := true;
	      idest_states := dest_info :: (!idest_states)
	    end
    end);
  let state_env =
    if VB.unique pd
    then
      (* drop pd from all maxes, as in the GPCE paper.
         If max becomes empty, add in an unknown process originally in the
	 current state.  This is not in the GPCE paper... *)
      List.map
	(function
	    (state_info,VB.MM(min,max)) ->
	      let new_max =
		match
		  List.filter (function pd1 -> not(pd1 = pd)) max
		with
		  [] -> [VB.FROMSTATE(VB.X,state_info)]
		| x -> x in
	      (state_info,VB.MM(min,new_max))
	  | entry -> entry)
	state_env
    else state_env in
  let dest_cd = VB.state_info_lookup 8 dest_info state_env in
  let (min,max) = VB.get_mm dest_cd in
  if List.mem pd min
  then
    VB.combine_extensions
      [(dest_info,VB.MM(Aux.union [VB.UNKNOWN] min,VB.pd_max_union pd max))]
      state_env
  else
    VB.combine_extensions
      [(dest_info,VB.MM(Aux.union [pd] min,VB.pd_max_union pd max))]
      state_env

(* ------------------------ ForwardImmediate ----------------------- *)

(* finding the states that are the possible destinations of a
forwardImmediate *)

let forwardImmediateOutputs = ref ([] : CS.classname list)

(* if a possible resulting class is the same as a possible source class:
when there is only one possible source for the forwardimmediate, take that
state rather than the public representitive of the corresponding class.
when there are multiple source classes take those states in addition to the
public representitives. *)
let find_target_states srcs =
  let source_classes =
    Aux.make_set (List.map (function CS.STATE(_,cls,_,_) -> cls) srcs) in
  let replace = (List.length source_classes) = 1 in
  let output_classes = !forwardImmediateOutputs in
  let get_public cls =
    Aux.option_filter
      (function
	  (id,(CS.STATE(_,cls1,_,CS.PUBLIC) as x)) ->
	    if cls1 = cls then Some x else None
	| _ -> None)
      (!CS.state_env) in
  Aux.flat_map
    (function output_class ->
      if Aux.member output_class source_classes
      then
	let sources_in_class =
	  List.filter
	    (function CS.STATE(_,cls,_,_) -> cls = output_class)
	    srcs in
	if replace
	then sources_in_class
	else (get_public output_class) @ sources_in_class
      else get_public output_class)
    output_classes

(* the move statement *)

let verif_move_fwd exp verifsrcsdsts attr var_env state_env rebuild =
  let (passertions,(pd,exp,state_env,drop_pd_extension)) =
    enter_verif_proc_exp exp var_env state_env in
  (* exp must be source or target *)
  let checks =
    if not (!src_tgt_handler) || VB.is_src_tgt pd
    then passertions
    else
      begin
	assertions := passertions;
	add_is_src_or_tgt_assertion exp attr;
	(!assertions)
      end in
  (* problem: we can move to more than one state *)
  let srcs =
    List.map (function (state_info,_) -> state_info) drop_pd_extension in
  let dsts = find_target_states srcs in
  if dsts = [] then error(Some(B.loc2c attr),"no destinations");
  let (_,envs,stm) = (* state_env only; no effect on var_env *)
    List.fold_left
      (function (state_env,acc_envs,acc_stms) ->
	function (CS.STATE(dstid,dstcls,dsttype,_)) as dst ->
		(* do nothing if the process is already known to be in the
		   destination state.  but this will not always work when
		   the source is a variable, and srcs does not accurately
		   reflect what is known about the position of the process *)
	  if [dst] = srcs
	  then
	    (state_env, Aux.union [state_env] acc_envs, acc_stms)
	  else
	    begin
	      let new_state_env =
		VB.combine_extensions drop_pd_extension state_env in
	      let (empty_assertions,state_env,dsttype) =
		check_empty_dest dst state_env attr in
	      let new_state_env =
		move_to_state (Some srcs) pd dst new_state_env in
	      (state_env, Aux.union [new_state_env] acc_envs,
	       add_assertions empty_assertions acc_stms)
	    end)
      (state_env,[],
       rebuild(exp,
	       Aux.union
		 (Aux.flat_map
		    (function s -> List.map (function d -> (s,d)) dsts)
		    srcs)
		 verifsrcsdsts,
	       attr))
      dsts in
  let final_envs = List.map (function env -> (var_env,env)) envs in
  (add_assertions checks stm,final_envs)

(* --------------------------- Statements -------------------------- *)

let rec verif_stmt stmt var_env state_env nonzero :
    Ast.stmt *
    (VB.var_env * VB.state_env * (B.identifier list (* nonzero variables *)))
    list =
  match stmt with
    Ast.IF(tst,thn,els,a) as x when state_env = [] ->
      (x,[]) (* dead code due to a break *)
  | Ast.IF(tst,thn,els,a) ->
      let (assertions,(tst1,state_env,res)) =
	enter_verif_bool_exp tst var_env state_env nonzero in
      let (stmt,envs) =
	match res with
	  T ->
	    let (thn1,envs) = verif_stmt thn var_env state_env nonzero in
	    (Ast.IF(tst1,thn1,els,a),envs)
	| F ->
	    let (els1,envs) = verif_stmt els var_env state_env nonzero in
	    (Ast.IF(tst1,thn,els1,a),envs)
	| TF(true_env,true_nonzero,false_env,false_nonzero) ->
	    let (thn1,envs_then) =
	      verif_stmt thn var_env
		(VB.combine_extensions true_env state_env)
		(Aux.union true_nonzero nonzero) in
	    let (els1,envs_else) =
	      verif_stmt els var_env
		(VB.combine_extensions false_env state_env)
		(Aux.union false_nonzero nonzero) in
	    (Ast.IF(tst1,thn1,els1,a),Aux.union envs_then envs_else) in
      (add_assertions assertions stmt, envs)
  | Ast.FOR_WRAPPER(label,stms,a) ->
      Aux.push [] break_or_cont_state;
      let (stms1,envs) =
	List.split
	  (List.map (function stm -> verif_stmt stm var_env state_env nonzero)
	     stms) in
      let break_or_cont_states = Aux.top break_or_cont_state in
      Aux.pop break_or_cont_state;
      (Ast.FOR_WRAPPER(label,stms1,a),
       List.fold_left Aux.union break_or_cont_states envs)
  | Ast.FOR(id,name,[state_info],dir,body,_,a) as x when state_env = [] ->
      (x,[]) (* dead code due to a break *)
  | Ast.FOR(id,name,[state_info],dir,body,crit,a) as x ->
      (match VB.state_info_lookup 9 state_info state_env with
	VB.EMPTY -> (x,[(var_env,state_env,nonzero)])
      |	VB.MM(min,max) ->
	  Aux.push state_info illegal_destinations;
	  let elem = VB.pdluball_fresh 5 max in
          (* run at least once if min is not empty, could do this as many times
          as there are elements in min, but it's probably not worth it *)
	  let (body,var_env,state_env,nonzero) =
	    if not (min = [])
	    then
	      let (body,envs) =
		verif_stmt body (VB.update_var_env id elem var_env)
		  (move_to_state None elem state_info state_env)
		  nonzero in
	      let (var_envs,state_envs,nonzers) = Aux.split3 envs in
	      let var_envs = List.map (VB.drop_var_env id) var_envs in
	      let state_envs =
		List.map (VB.state_env_replace elem (VB.pdunfresh elem))
		  state_envs in
	      (body,VB.var_env_lub_all var_envs,
	       VB.state_env_lub_all state_envs,
	       Aux.intersect_all nonzers)
	    else (body,var_env,state_env,nonzero) in
          (* the actual fixpoint iteration *)
	  let rec loop body var_env state_env nonzero =
	    if state_env = []
	    then (body,[]) (* dead code *)
	    else
	      begin
		let (body,envs) =
		  verif_stmt body (VB.update_var_env id elem var_env)
		    (move_to_state None elem state_info state_env) nonzero in
		let (var_envs,state_envs,nonzers) = Aux.split3 envs in
		let var_envs = List.map (VB.drop_var_env id) var_envs in
		let unfresh_state_envs =
		  List.map (VB.state_env_replace elem (VB.pdunfresh elem))
		    state_envs in
		if (List.for_all
		      (function env1 -> VB.state_env_leq env1 state_env)
		      unfresh_state_envs &&
		    List.for_all
		      (function env1 -> VB.var_env_leq env1 var_env)
		      var_envs)
		then
		  let new_state_envs =
		    (* can the process be in its original state? *)
		    if (List.exists
			  (function state_env ->
			    let (in_env,_) = VB.lookup elem state_env in
			    let states =
			      List.map (function (si,_) -> si) in_env in
			    List.mem state_info states)
			  state_envs)
		    then unfresh_state_envs (* yes *)
		    else                    (* no, make the state empty *)
		      List.map
			(function state_env ->
			  List.map
			    (function (si,_) as x ->
			      if si = state_info then (si,VB.EMPTY) else x)
			    state_env)
			state_envs in
		  (body,
		   Aux.union_list
		     (Aux.combine3 var_envs new_state_envs nonzers))
		else
		  loop body (VB.var_env_lub_all var_envs)
		    (VB.state_env_lub_all unfresh_state_envs)
		    (Aux.intersect_all nonzers)
	      end in
	  let (body,envs) = loop body var_env state_env nonzero in
	  Aux.pop illegal_destinations;
	  VB.discard_fresh elem;
	  (Ast.FOR(id,name,[state_info],dir,body,crit,a), envs))
  | Ast.FOR(id,name,_,dir,body,_,a) -> internal_error(Some(B.loc2c a),6)
  | Ast.SWITCH(exp,cases,default,a) ->
      let (assertions,(pd,exp,state_env,drop_extension)) =
	enter_verif_proc_exp exp var_env state_env in
      let possible_states = List.map (function (x,_) -> x) drop_extension in
      let (branches,envs,used_states) =
	List.fold_left
	  (function (branches,envs,used_states) ->
	    function Ast.SEQ_CASE(pat,states,stm,attr) as branch ->
	      let not_seen_states = Aux.subtract states used_states in
	      let common_states =
		Aux.intersect not_seen_states possible_states in
	      match classify_states common_states state_env with
		(_,[],[]) -> (branch::branches,envs,used_states)(* all empty *)
	      |	(_,[],[(si,VB.MM([],max))]) ->
		  (* only one possibly non-empty *)
		  let state_env =
		    VB.combine_extensions [(si,VB.MM([VB.pdluball 6 max],max))]
		      state_env in
		  let (stm,stm_envs) =
		    verif_stmt stm var_env state_env nonzero in
		  (Ast.SEQ_CASE(pat,states,stm,attr)::branches,
		   Aux.union stm_envs envs,Aux.union [si] used_states)
	       | (_,nonempty,unknown) ->
		   let get_state (si,_) = si in
		   let common_states =
		     (List.map get_state nonempty) @
		     (List.map get_state unknown) in
		   let (stm,stm_envs) =
		     verif_stmt stm var_env state_env nonzero in
		   (Ast.SEQ_CASE(pat,states,stm,attr)::branches,
		    Aux.union stm_envs envs,
		    Aux.union common_states used_states))
	  ([],[],[]) cases in
      let branches = List.rev branches in
      let default_possible =
	not (List.length possible_states = List.length used_states) in
      (match default with
	None ->
	  if default_possible
	  then internal_error(Some(B.loc2c a),7);
	  (add_assertions assertions (Ast.SWITCH(exp,branches,default,a)),
	   envs)
      |	Some stm ->
	  let (default,new_envs) =
	    verif_stmt stm var_env state_env nonzero in
	  (add_assertions assertions (Ast.SWITCH(exp,branches,Some default,a)),
	   Aux.union new_envs envs))
  | Ast.SEQ(decls,body,a) ->
      let (assertions,decls,var_env1,state_env1) =
	verif_decls decls var_env state_env nonzero in
      let (body1,envs_body) =
	List.fold_left
	  (function (stms,envs) ->
	    function stm ->
	      let (stm1,envs_stm) =
		List.fold_left
		  (function (stm1,envs_stm) ->
		    function (var_env,state_env,nonzero) ->
		      let (stm,envs) =
			verif_stmt stm1 var_env state_env nonzero in
		      (stm,Aux.union envs envs_stm))
		  (stm,[]) envs in
	      (stm1::stms,envs_stm))
	  ([],[(var_env1,state_env1,nonzero)]) body in
      (add_assertions assertions (Ast.SEQ(decls,List.rev body1,a)),envs_body)
  | Ast.RETURN(None,a) ->
      return_state := Aux.union [state_env] (!return_state);
      (stmt,[])
  | Ast.RETURN(Some(exp),a) ->
      let (assertions,(exp,state_env)) =
	enter_verif_exp exp var_env state_env nonzero in
      return_state := Aux.union [state_env] (!return_state);
      (add_assertions assertions (Ast.RETURN(Some(exp),a)),[])
  | Ast.MOVE(proc,dest,pre_src_info,verif_src_info,Some dest_info,
	     self_allowed,state_end,a) ->
      if List.mem dest_info (!illegal_destinations)
      then
	error (Some(B.loc2c a),
	       "cannot move a process into a queue traversed by a for loop");
      let (passertions,(pd,proc,state_env,drop_pd_extension)) =
	enter_verif_proc_exp proc var_env state_env in
      let new_srcs = List.map (function (s,_) -> s) drop_pd_extension in
      let state_env = VB.combine_extensions drop_pd_extension state_env in
      (* check whether the destination is free *)
      let (empty_assertions,state_env,dsttype) =
	check_empty_dest dest_info state_env a in
      let assertions = passertions@empty_assertions in
      (* check whether a move from a state to itself is possible *)
      if List.mem dest_info new_srcs
      then
	(match (self_allowed,dsttype) with
	  (true,CS.QUEUE(CS.SELECT(_))) -> ()
	| (true,_) ->
	    warning(Some(B.loc2c a),
		     pr "possible move from %s to itself (may be ignored)"
		       (CS.state2c dest_info))
	| _ -> error (Some(B.loc2c a),
		      pr "possible move from %s to itself not allowed"
			(CS.state2c dest_info)));
      (* make the move itself *)
      let state_env = move_to_state (Some new_srcs) pd dest_info state_env in
      (* all done *)
      (add_assertions assertions
	 (Ast.MOVE(proc,dest,pre_src_info,
		   List.sort compare (Aux.union new_srcs verif_src_info),
		   Some dest_info,self_allowed,state_end,a)),
       [(var_env,state_env,nonzero)])
  | Ast.MOVE(proc,dest,pre_src_info,verif_src_info,None,self_allowed,state_end,
	     a) ->
      internal_error(Some(B.loc2c a),8)
  | Ast.MOVEFWD(exp,verifsrcsdsts,state_end,a) ->
      let (stmt,envs) =
	verif_move_fwd exp verifsrcsdsts a var_env state_env
	  (function (exp,verifsrcsdsts,attr) ->
	    Ast.MOVEFWD(exp,verifsrcsdsts,state_end,attr)) in
      (stmt,
       List.map
	 (function (var_env,state_env) -> (var_env,state_env,nonzero))
	 envs)
  | Ast.SAFEMOVEFWD(exp,verifsrcsdsts,default,state_end,a) ->
	let (stma,enva_venva) = verif_stmt default var_env state_env nonzero in
	let (stm,envb_venvb) =
	  verif_move_fwd exp verifsrcsdsts a var_env state_env
	    (function (exp,verifsrcsdsts,attr) ->
	      Ast.SAFEMOVEFWD(exp,verifsrcsdsts,stma,state_end,attr)) in
	let envb_venvb =
	  List.map
	    (function (var_env,state_env) -> (var_env,state_env,nonzero))
	    envb_venvb in
	(stm, Aux.union enva_venva envb_venvb)
  | Ast.ASSIGN(lexp,rexp,affect_sorted,a) ->
      (* determine whether the lhs is the field of a sorted queue process *)
      let (assertions,lexp,extension,sorted_exp,nonzero) =
	enter_verif_lhs_exp lexp var_env state_env nonzero in
      let state_env = VB.combine_extensions extension state_env in
      (* determine the pd of the rhs expression and update the var_env *)
      let (assertions1,rexp,extension1,var_env) =
	if (B.ty a = B.PROCESS)
	then
	  let (assertions,(pd,rexp,state_env,_)) =
	    enter_verif_proc_exp rexp var_env state_env in
	  match lexp with
	    Ast.VAR(id,_,a) ->
	      (assertions,rexp,state_env,VB.update_var_env id pd var_env)
	  | _ ->
	      error(Some(B.loc2c a),
		    "only process typed assignments to variables supported");
	      ([],rexp,state_env,var_env)
	else
	  let (assertions,(rexp,state_env)) =
	    enter_verif_exp rexp var_env state_env nonzero in
	  (assertions,rexp,state_env,var_env) in
      (add_assertions (assertions@assertions1)
	 (Ast.ASSIGN(lexp,rexp,sorted_exp||affect_sorted,a)),
       [(var_env,state_env,nonzero)])
  | Ast.DEFER(a) -> (stmt,[(var_env,state_env,nonzero)])
  | Ast.PRIMSTMT(fexp,args,a) ->
      let (assertions,(fexp,state_env)) =
	enter_verif_exp fexp var_env state_env nonzero in
      let (assertions,args,state_env) =
	List.fold_left
	  (function (assertions,args,state_env) ->
	    function arg ->
	      let (assertions1,(arg,state_env)) =
		enter_verif_exp arg var_env state_env nonzero in
	      (assertions1 @ assertions,arg::args,state_env))
	  (assertions, [], state_env) args in
      let args = List.rev args in
      (add_assertions assertions (Ast.PRIMSTMT(fexp,args,a)),
       [(var_env,state_env,nonzero)])
  | Ast.ERROR(str,a) -> (stmt,[])
  | Ast.BREAK(a) ->
      Aux.union_top [(var_env,state_env,nonzero)] break_or_cont_state;
      (stmt,[])
  | Ast.CONTINUE(a) -> 
      Aux.union_top [(var_env,state_env,nonzero)] break_or_cont_state;
      (stmt,[])
  | Ast.ASSERT(exp,a) ->
      (stmt,
       match exp with
	Ast.EMPTY(_,states,_,_,_) ->
	  [(var_env,
	    VB.combine_extensions
	      (List.map (function state -> (state,VB.EMPTY)) states)
	      state_env,
	    nonzero)]
      |	Ast.UNARY(Ast.NOT,Ast.EMPTY(_,states,_,_,_),_) ->
	  [(var_env,
	    VB.combine_extensions
	      (List.map
		 (function state ->
		   match VB.state_info_lookup 10 state state_env with
		     VB.MM([],max) ->
		       (state,VB.MM([VB.pdluball 7 max],max))
		   | VB.MM(_,_) as x -> (state,x)
		   | _ -> internal_error(Some(B.loc2c a),9))
		 states)
	      state_env,
	    nonzero)]
      |	_ -> internal_error(Some(B.loc2c a),10))

(* ----- Analyze one handler with respect to one concrete input ---- *)

let rec check_compat name input phi type_input outputs attr envs =
  List.filter
    (function new_env ->
      let errors =
	Aux.option_filter (VB.check_output phi new_env) outputs in
      if List.length errors = List.length outputs
      then (* everything failed *)
	(let line = B.loc2c attr in
	if not (List.mem line (!type_error_lines))
	then
	  begin
	    type_error_lines := line::(!type_error_lines);
	    always_error(Some(B.loc2c attr),
		  "\nfrom input:\n"^(VB.state_env2c input)^
		  "\nderived from event type input:\n"^
		  (VB.event_type2c type_input)^
		  "\nobtained output:\n"^(VB.state_env2c new_env)^
		  "\nwhich caused the following errors:");
	    List.iter
	      (function x -> Printf.printf "%s" x)
	      (Aux.union_list
		 (List.map2
		    (function output ->
		      function error ->
			Printf.sprintf
			  "%s\n  in output type: %s\n"
			  error (VB.event_type2c output))
		    outputs errors));
	    Printf.printf "\n";
	    flush stdout
	  end;
	false)
      else true)
    envs

(* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *)

(* again, backwards as compared to quasi.ml *)
let check_compatible cur input =
  let contains x l =
    List.exists (function VB.FROMSTATE(y,_) -> x = y | _ -> false) l in
  List.for_all2
    (function (cur_state,cur_vt) ->
      function (input_state,input_vt) ->
	match input_vt with
	  VB.MM([],_) -> true (* unknown *)
	| VB.EMPTY ->
	    (match cur_vt with
	      VB.MM([],_) -> true
	    | VB.EMPTY -> true
	    | VB.MM(_,_) -> false)
	| VB.MM(l,_) ->
	    (match cur_vt with
	      VB.MM([],_) -> (* unknown *)
		not(List.exists VB.is_src_tgt l)
	    | VB.EMPTY -> false
	    | VB.MM(l1,_) ->
		let src_needed =
		  (contains VB.SRC l) || (contains VB.SRCANDTGT l) in
		let src_found =
		  (contains VB.SRC l1) || (contains VB.SRCANDTGT l1) in
		let tgt_needed =
		  (contains VB.TGT l) || (contains VB.SRCANDTGT l) in
		let tgt_found =
		  (contains VB.TGT l1) || (contains VB.SRCANDTGT l1) in
		(not src_needed || src_found) &&
		(not tgt_needed || tgt_found)))
    cur input

(* input is the concrete configuration that was passed through the handler.
we next look at all of the possible types and find the ones for which input
is compatible with the input.  When we find something compatible, we make an
environment mapping the variables used in the type to the variables used in
the input. *)

let types_for_the_current_event normalized_types nm_fn =
  List.flatten
    (Aux.option_filter
       (function (name,q,concretized_inputs,outputs) ->
	 if nm_fn name
	 then
	   Some
	     (List.map
		(function concretized_input ->
		  (concretized_input,outputs))
		concretized_inputs)
	 else None)
       normalized_types)

let get_outputs input possible_types =
  (* all the inputs grouped with all the outputs is not so convenient at this
     point *)
  let compatible_outputs =
    Aux.option_filter
      (function ((type_input,phi,_,_,_),outputs) ->
	if check_compatible input type_input
	then Some (phi,outputs)
	else None)
      possible_types in
  compatible_outputs

let rec check_handler_compat name input phi type_input possible_types
    attr envs =
  (* pairs of phi and a list of outputs *)
  let all_outputs = get_outputs input possible_types in
  let outputs = (* a more convenient form *)
    Aux.flat_map
      (function (phi,outputs) ->
	List.map (function output -> (phi,output)) outputs)
      all_outputs in
  List.filter
    (function new_env ->
      let errors =
	Aux.option_filter
	  (function (phi,output) -> VB.check_output phi new_env output)
	  outputs in
      if List.length errors = List.length outputs
      then (* everything failed *)
	(let line = B.loc2c attr in
	if not (List.mem line (!type_error_lines))
	then
	  begin
	    type_error_lines := line::(!type_error_lines);
	    always_error
	      (Some(B.loc2c attr),
	       "\nfrom input:\n"^(VB.state_env2c input)^
	       "\nderived from event type input:\n"^
	       (VB.event_type2c type_input)^
	       "\nobtained output:\n"^(VB.state_env2c new_env)^
	       "\nwhich caused the following errors:");
	    List.iter
	      (function x -> Printf.printf "%s" x)
	      (Aux.union_list
		 (List.map2
		    (function (phi,output) ->
		      function error ->
			Printf.sprintf
			  "%s\n  in output type: %s\n"
			  error (VB.event_type2c output))
		    outputs errors));
	    Printf.printf "\n";
	    flush stdout
	  end;
	false)
      else true)
    envs

let verif_handler (Ast.EVENT(name,specd,param,stmt,attr)) input phi type_input
    possible_types =
  if Ast.event_name2c name = "unblock.timer.notarget.*" ||
  Ast.event_name2c name = "unblock.timer.persistent.*"
  then ([],Ast.EVENT(name,specd,param,stmt,attr)) (* just ignore it *)
  else
    begin
      (match param with
	 Ast.VARDECL(B.PEVENT,id,imported,lz,de,attr) ->
	 event_param := (id, B.PEVENT, B.PROCESS)
       | Ast.VARDECL(B.CEVENT,id,imported,lz,de,attr) ->
	  event_param := (id, B.CEVENT, B.CORE)
      | _ -> error (Some(B.loc2c attr), "incorrect event argument list"));
      return_state := [];
      in_a_system_event := List.mem name (!system_events);
      tgt_only_at_start :=
	(try let _ = VB.lookup_src_tgt VB.SRC input in false
	with Error.Error _ -> true);
      let (new_stmt,_) = verif_stmt stmt [] input [] in
      tgt_only_at_start := true;
      in_a_system_event := false;
      let new_envs = !return_state in
      let valid_envs =
	check_handler_compat (Ast.event_name2c name) input phi type_input
	  possible_types attr new_envs in
      (valid_envs,Ast.EVENT(name,specd,param,new_stmt,attr))
    end

(* ---------------------- Interface functions ---------------------- *)

(* if the type is the generic interface function type, it contains tgt
rather than the argument name.  *)
let instantiate_type ty = function
  [(id,VB.ARG(n))] ->
    List.map
      (function (cls,vt,modd) ->
	match vt with
	  VB2.MEMBER(l) ->
	    let new_l = List.map (VB2.vp_replace VB2.TGT (VB2.ARG(n))) l in
	    (cls,VB2.MEMBER(new_l),modd)
	| _ -> (cls,vt,modd))
      ty
  | [] -> ty (* no process, so no target *)
  | _ -> internal_error(None,11)

let instantiate_env env = function
  [(id,argproc)] ->
    List.map
      (function
	  (id,VB.MM(min,max)) ->
	    let process l =
	      List.map
		(function
		    VB.FROMSTATE(VB.TGT,s) -> VB.FROMSTATE(argproc,s)
		  | x -> x)
		l in
	    (id,VB.MM(process min,process max))
	| x -> x)
      env
  | [] -> env (* no process, so no target *)
  | _ -> internal_error(None,12)

let rec verif_one_ifunction nm proc_args stmt attr inputs outputs imemotable
   generic possible_types =
  VB.arg_list := List.map (function (_,arg) -> arg) proc_args;
  VB.max_arg := List.length proc_args;
  let inputs =
    if generic
    then
      List.map
	(function (env,phi,stpfn,fwds,type_input) ->
	  (instantiate_env env proc_args,phi,stpfn,fwds,type_input))
	inputs
    else inputs in
  let outputs =
    match (generic,outputs) with
      (true,Some outputs) ->
	Some (List.map (function ty -> instantiate_type ty proc_args) outputs)
    | _ -> outputs in
  let possible_types =
    match possible_types with
      None -> None
    | Some possible_types ->
	Some(List.map
	       (function ((i,phi,stpfn,f,type_input),o) ->
		 ((instantiate_env i proc_args,phi,stpfn,f,type_input),
		  List.map (function ty -> instantiate_type ty proc_args) o))
	       possible_types) in
  let res =
    List.fold_left
      (function (prev_stmt,res_envs) ->
	function (env,phi,stpfn,f,type_input) ->
	  try
	    (prev_stmt, Aux.union (Hashtbl.find imemotable (nm,env)) res_envs)
	  with Not_found ->
	    (let venv =
	      List.map
		(function (id,argproc) ->
		  (id,VB.FROMSTATE(argproc,find_arg argproc env attr)))
		proc_args in
	    return_state := [];
	    stpfn(); (* init some info used by vobj.ml *)
	    let (new_stmt,_) = verif_stmt prev_stmt venv env [] in
	    let new_envs = !return_state in
	    (match outputs with
	      None ->
		Hashtbl.add imemotable (nm,env) new_envs;
		(new_stmt,new_envs@res_envs)
	    | Some outputs ->
		let valid_envs =
		  match possible_types with
		    None ->
		      raise
			(Error.Error "possible_types as None should not occur")
		      (* in the initial attach case, there is no type
			 check_compat (B.id2c nm) env phi type_input outputs
			 attr new_envs *)
		  | Some possible_types ->
		      check_handler_compat (B.id2c nm) env phi type_input
			possible_types attr new_envs in
		Hashtbl.add imemotable (nm,env) valid_envs;
		(new_stmt,valid_envs@res_envs))))
      (stmt,[]) inputs in
  VB.arg_list := [];
  VB.max_arg := -1;
  res

(* find the state containing a process, if any *)
and find_arg p env attr =
  let any =
    Aux.option_filter
      (function
	  (si,VB.MM(min,_)) ->
	    if List.exists
		(function VB.FROMSTATE(x,_) -> x = p | _ -> false)
		min
	    then Some si
	    else None
	| _ -> None)
      env in
  match any with
    [] ->
      raise
	(Error.Error(pr "process argument not found in\n%s"
		       (VB.state_env2c env)))
  | [state_info] -> state_info
  | l ->
      error(Some(B.loc2c attr),
	    pr "process %s in more than one state"
	      (match p with
		VB.ARG(n) -> "arg"^(string_of_int n)
	      | _ -> "?"));
      List.hd l

(* ----- Analyze all handlers with respect to the current state ---- *)

(* if the type is a quasi type, the target process can only be matched to a
state into which interrupts have put processes *)

(* isquasi describes whether the concretized inputs come from a quasi type.
If it is a quasi type, the target process must match against a state into
which processes may have been moved from running on an interrupt This
assumes that all quasitypes relate to a process that ought to be in the
running state.  To be more general would be possible, but would require
collecting information about why the quasitype in the type processing. *)

let match_config_to_type config concretized_inputs isquasi =
  let config = List.sort VB.compare_state_info config in
  Aux.option_filter
    (function (input,phi,stpfn,fwds,type_input) ->
      let matching_input =
	Aux.option_for_all2
	  (function (CS.STATE(_,cls,_,_) as cfg_id,cfg_cd) as c ->
	    function (input_id,input_cd) as x ->
	      if not(cfg_id = input_id)
	      then internal_error(None,13);
	      match (input_cd,cfg_cd) with
		(VB.EMPTY,VB.EMPTY) -> Some x
	      |	(VB.EMPTY,VB.MM([],_)) -> Some x
	      |	(VB.MM([],_),_) -> Some c
	      | (VB.MM(l,_),VB.MM(_,_)) ->
		  if not(cls = CS.NOWHERE) && isquasi = B.QUASI &&
		    List.exists VB.is_src_tgt l
		  then
		    if List.mem cfg_id (!idest_states)
		    then Some x
		    else
		      begin
			quasitype_failed := true;
			None
		      end
		  else Some x
	      | _ -> None)
	  config input in
      match matching_input with
	Some x -> Some (x,phi,stpfn,fwds,type_input)
      |	None -> None)
    concretized_inputs

let normalize config =
  List.sort VB.compare_state_info
    (List.map
       (function ((CS.STATE(_,cls,_,_)) as st,cd) ->
	 match (cls,cd) with
	   (* no rule can read from terminated *)
	   (_,VB.EMPTY) ->
	     (st,VB.EMPTY)
	 | (_,VB.MM([],_)) ->
	     (st,VB.init_cd st)
      	 | (_,VB.MM(_,_)) ->
	     (st,VB.MM([VB.FROMSTATE(VB.X,st)],[VB.FROMSTATE(VB.X,st)])))
       config)

let normalize_all types =
   Aux.flat_map
     (function (nm,types) ->
       List.map
         (function (q,inputs,outputs) ->
           (nm,q,
	    List.map
	      (function (i,phi,stpfn,f,type_input) ->
		(List.sort VB.compare_state_info i,phi,stpfn,f,type_input))
	      inputs,
            List.map (List.sort compare) outputs))
	 types)
    types

let rec replace_function (Ast.FUNDEF(_,nm,_,_,_,_) as new_function) = function
    [] -> internal_error(None,14)
  | ((Ast.FUNDEF(ret,name,params,stmt,inl,attr),procarg) as x)::rest ->
      if nm = name
      then (new_function,procarg) :: rest
      else x :: (replace_function new_function rest)

let analyze_untyped_ifunctions nm ifunctions automaton_tgt
    possible_inputs outputs imemotable interface_env =
  src_tgt_handler := false;
  let possible_types =
    types_for_the_current_event interface_env
      (function nm -> (Ast.event_name2c nm) = "generic_interface") in
  Aux.map_union
    (function (Ast.FUNDEF(ret,fnm,params,stmt,inl,attr),procarg) ->
      VB.handler_name := pr "%s: " (B.id2c fnm);
      let (new_body,possible_outputs) =
	verif_one_ifunction fnm procarg stmt attr
	  possible_inputs (Some outputs) imemotable true
	  (Some possible_types) in
      let new_function = Ast.FUNDEF(ret,fnm,params,new_body,inl,attr) in
      ifunctions := replace_function new_function (!ifunctions);
      List.map (function x -> (nm,(automaton_tgt,normalize x)))
	possible_outputs)
  (!ifunctions)

let analyze_typed_ifunctions nm ifunctions config automaton_tgt imemotable
    interface_env =
  src_tgt_handler := false;
  Aux.map_union
    (function (Ast.FUNDEF(ret,fnm,params,stmt,inl,attr),procarg) ->
      VB.handler_name := pr "%s: " (B.id2c fnm);
      let (new_body,possible_outputs) =
	List.fold_left
	  (function (prev_stmt,prev_outputs) ->
	    function (nm,q,concretized_inputs, outputs) ->
	      if B.ideq(nm,fnm)
	      then
		let possible_inputs =
		  match_config_to_type config concretized_inputs q in
		let possible_types =
		  types_for_the_current_event interface_env
		    (function nm -> B.ideq(nm,fnm)) in
		let (new_stmt,possible_outputs) =
		  verif_one_ifunction fnm procarg prev_stmt attr
		    possible_inputs (Some outputs) imemotable false
		    (Some possible_types) in
		(new_stmt,Aux.union possible_outputs prev_outputs)
	      else (prev_stmt,prev_outputs))
	  (stmt,[]) interface_env in
      let new_function = Ast.FUNDEF(ret,fnm,params,new_body,inl,attr) in
      ifunctions := replace_function new_function (!ifunctions);
      List.map (function x -> (nm,(automaton_tgt,normalize x)))
	possible_outputs)
  (!ifunctions)

let rec replace_handler prev (Ast.EVENT(nm,specd,_,_,_) as new_handler) =
  function
    [] -> [new_handler]
  | (Ast.EVENT(name,specd,param,stmt,attr) as x)::rest ->
      if Ast.event_name_equal nm name && Ast.event_names_equal prev specd
      then new_handler :: rest
      else x :: (replace_handler prev new_handler rest)

let adjust_schedule_inputs possible_inputs =
  let is_empty_unk env id =
    match VB.state_info_lookup 11 id env with
      VB.EMPTY -> true
    | VB.MM([],_) -> true
    | _ -> false in
  List.filter
    (function (env,phi,stpfn,fwds,type_input) ->
      let running =
	match CS.concretize_class CS.RUNNING with
	  [state_info] -> state_info
	| _ -> internal_error(None,15) in
      let readys = CS.concretize_class CS.READY in
      (* if running is non-empty or readys are all empty, then restart *)
      if not (is_empty_unk env running) ||
         List.for_all (is_empty_unk env) readys
      then false
      else true)
    possible_inputs

(* event_type_name is the name for which there is a type.
This might be different than the actual name of the handler *)
let analyze_handlers event_type_name handlers relevant_handlers
    prev automaton_state automaton_tgt possible_inputs possible_types
    memotable =
  let possible_types = (* types for this event *)
    types_for_the_current_event possible_types
      (function name -> event_type_name = name) in
  Aux.map_union
    (function handler ->
      let (Ast.EVENT(nm,_,_,_,_)) = handler in
      let str_nm = Ast.event_name2c nm in
      VB.handler_name := pr "%s: " str_nm;
      src_tgt_handler := not (str_nm = "bossa.schedule" || str_nm = "preempt");
      let possible_inputs =
	if str_nm = "bossa.schedule" && prev = []
	then adjust_schedule_inputs possible_inputs
	else possible_inputs in
      let (new_configs,new_handler) =
	List.fold_left
	  (function (new_configs,new_handler) ->
	    function (input,phi,stpfn,fwds,type_input) ->
	      current_input_config := (automaton_state,input);
	      (let (newer_configs,newer_handler) =
		try (Hashtbl.find memotable (prev,nm,input),new_handler)
		with Not_found ->
		  (forwardImmediateOutputs := fwds;
		   stpfn(); (* init some info used in vobj.ml *)
		   let (newer_configs,newer_handler) =
		     verif_handler new_handler input phi type_input
		       possible_types in
		   Hashtbl.add memotable (prev,nm,input) newer_configs;
		   (newer_configs,newer_handler)) in
	      let configs_to_add =
		List.map
		  (function x -> (nm,([nm],automaton_tgt,normalize x)))
		  newer_configs in
	      (Aux.union configs_to_add new_configs,newer_handler)))
	  ([],handler) possible_inputs in
      handlers := replace_handler prev new_handler (!handlers);
      new_configs)
    relevant_handlers

(* -------------- Preprocessing of interface functions ------------- *)

(* distinguish between those that do not perform a move/in/empty operation,
which are ignored, those for which a type is provided, and those for which
the default interface function type should be used.  For the latter two,
construct an environment mapping parameter names to ARG variables. *)

let rec classify_ifunctions ifunctions interface_env =
  let (ignore,type_provided,no_type_provided) =
    List.fold_left
      (function (ignore,type_provided,no_type_provided) ->
	function Ast.FUNDEF(ret,nm,params,stmt,inl,attr) as x ->
	  if move_possible stmt
	  then
	    let args = construct_arg_env params in
	    try
	      let _ = lookup_id nm interface_env in
	      (if (List.length args > 1)
	      then multi_arg_warning nm attr);
	      (ignore,(x,args)::type_provided,no_type_provided)
	    with
	      Not_found ->
		(if (List.length args > 1)
		then multi_arg_error nm attr);
		(ignore,type_provided,(x,args)::no_type_provided)
	  else (x::ignore,type_provided,no_type_provided))
      ([],[],[]) ifunctions in
  (List.rev ignore,List.rev type_provided,List.rev no_type_provided)

and multi_arg_warning nm attr =
  warning(Some(B.loc2c attr),
	  (Printf.sprintf "interface function %s has\n" (B.id2c nm))
	  ^" multiple process/scheduler typed parameters.\n"
	  ^"They are assumed to be disjoint.\n")

and multi_arg_error nm attr =
  error (Some(B.loc2c attr),
	 (Printf.sprintf "untyped interface function %s has\n" (B.id2c nm))
	 ^" multiple process/scheduler typed parameters, which is not"
	 ^" supported.\n")

and construct_arg_env params =
  let (proc_args,_) =
    List.fold_left
      (function (proc_args,pos) ->
	function Ast.VARDECL(ty,id,imported,lz,de,attr) ->
      if  ty = (B.PROCESS )
	  then ((id,VB.ARG(pos))::proc_args,pos+1)
	  else (proc_args,pos+1))
      ([],1) params in
    proc_args

and move_possible = function
    Ast.IF(Ast.EMPTY(id,_,_,_,eattr),stm1,stm2,attr)
  | Ast.IF(Ast.IN(_,Ast.VAR(id,_,_),_,_,_,_,eattr),stm1,stm2,attr)
  | Ast.IF(Ast.IN(_,Ast.FIELD(_,id,_,_),_,_,_,_,eattr),stm1,stm2,attr) -> true
  | Ast.IF(exp,stm1,stm2,attr) -> move_possible stm1 || move_possible stm2
  | Ast.SWITCH(exp,cases,default,attr) ->
      (List.exists
	(function Ast.SEQ_CASE(pat,states,stm1,attr) -> move_possible stm1)
	cases)
    ||
      (match default with
	None -> false
      |	Some x -> move_possible x)
  | Ast.SEQ(decls,stms,attr) -> List.exists move_possible stms
  | Ast.FOR(index,state,state_info,dir,stm1,crit,attr) -> move_possible stm1
  | Ast.FOR_WRAPPER(_,stms,attr) -> List.exists move_possible stms
  | Ast.MOVE(exp,state,srcs,verifsrcs,dst,auto_allowed,state_end,attr) -> true
  | stm -> false

(* ------------------------- The automaton ------------------------- *)

(* a state is the number of a node in the automaton and a mapping of
   states to their contents *)

let match_with_spec prev nm specialized_events =
  let suffixes = List.rev (Aux.non_empty_suffixes prev) in
  let rec loop = function
      [] -> []
    | (prev::rest) ->
	let matching_events =
	  List.filter
	    (function (specd,name) ->
	      Ast.event_names_equal prev specd &&
	      Ast.event_name_equal nm name)
	    specialized_events in
	(match matching_events with
	  [] -> loop rest
	| [_] -> prev
	| _ -> error(None,"more than one match for spec"); []) in
  loop suffixes

let rec get_output_states handler_env specialized_events handlers
    typed_ifunctions untyped_ifunctions
    concretized_types concretized_itypes
    automaton memotable imemotable
    orig_handlers
    (idest_info,prev,automaton_state,config) =
  (* collect the edges relevant to the current state in the automaton *)
  redo_quasitypes := false;
  quasitype_failed := false;
  let automaton_dests =
    Aux.option_filter
      (function (src,nm,int,tgt) ->
	if src = automaton_state
	then
	  (match int with
	    Objects.INTIBLE -> Some(nm,[],tgt)
	  | Objects.UNINTIBLE ->
	      let prev = match_with_spec prev nm specialized_events in
	      Some(nm,prev,tgt))
	else None)
      automaton in
  (* collect the event types relating to the relevant edges, independent of
     the current configuration *)
  let collect_all nm automaton_dests =
    Aux.option_filter
      (function (name,prev,tgt) -> (* prev is Some x iff x -> name is unint *)
	if Ast.event_name_equal nm name
	then Some (prev,tgt)
	else None)
      automaton_dests in
  let possible_types =
    List.fold_left
      (function types ->
	function (nm,q,inputs,outputs) ->
	  (List.map
	     (function (prev,tgt) ->
	       (nm,prev,tgt,q,inputs,outputs))
	     (collect_all nm automaton_dests))
	  @ types)
      [] concretized_types in
  let res =
    Aux.union
      (match
	Aux.make_set
	  (Aux.option_filter
	     (function (nm,prev,automaton_tgt,_,_,_) ->
	       if Ast.event_name2c nm = "generic_interface"
	       then Some (nm,automaton_tgt)
	       else None)
	     possible_types) with
	[(nm,automaton_tgt)] ->
	  let res =
	    analyze_typed_ifunctions nm typed_ifunctions config automaton_tgt
	      imemotable concretized_itypes in
	  List.map
	    (function (a,(b,x)) -> (a,([nm],b,x)))
	    res
      | _ -> [])
      (Aux.map_union
	 (function (nm,prev,automaton_tgt,q,concretized_inputs,outputs) ->
	   let possible_inputs =
	     match_config_to_type config concretized_inputs q in
	   if Ast.event_name2c nm = "generic_interface"
	   then
	     let res =
	       analyze_untyped_ifunctions nm untyped_ifunctions automaton_tgt
		 possible_inputs outputs imemotable concretized_types in
	     List.map
	       (function (a,(b,x)) -> (a,([nm],b,x)))
	       res
	   else
	     begin
	       let relevant_handlers =
		 lookup_handler nm handler_env prev (!handlers) orig_handlers
	       in
	     (* is the following test needed?  isn't this checking taken
		care of by hierarchy.ml? *)
	       if relevant_handlers = [] &&
		 not (Ast.event_name2c2 nm = Some "unblock.timer")
	       then error(None,
			  pr "no handler matches %s" (Ast.event_name2c nm));
	       analyze_handlers nm handlers relevant_handlers
		 prev automaton_state automaton_tgt
		 possible_inputs(*this config, instantiated according to input*)
		 concretized_types  (* all type rules *)
		 memotable
	     end)
	 possible_types) in
  (if !quasitype_failed
  then quasitype_failed_states :=
    (idest_redo_name,(!idest_states,prev,automaton_state,config))
    :: !quasitype_failed_states);
  let res = List.map (function (nm,(p,a,c)) -> (nm,([],p,a,c))) res in
  if !redo_quasitypes
  then
    begin
      let qtf = !quasitype_failed_states in
      quasitype_failed_states := [];
      qtf@res
    end
  else res

let print_config (_,prev,n,config) =
  Printf.printf "%s%d: %s\n"
    (match prev with
      [] -> ""
    | _ -> (String.concat " " (List.map Ast.event_name2c prev)) ^ " ")
    n (VB.state_env2c config)

let rec print_transitions = function
    Model.START((_,_,_,config)) -> Printf.printf "%s\n" (VB.state_env2c config)
  | Model.TRANSITION(prev,nm,(_,_,_,config)) -> print_transitions prev;
      Printf.printf " -%s-> " (Ast.event_name2c nm);
      Printf.printf "%s\n" (VB.state_env2c config)

let find_transition (current_state,nm,tgt) tbl =
  try ignore(Hashtbl.find tbl tgt); Model.NOTHING_NEW
  with Not_found ->
    try Model.PREVTRANS(Hashtbl.find tbl current_state,[])
    with Not_found -> Model.NOT_FOUND

type automaton_state =
    CS.state_info list * Ast.event_name list * int * VB.state_env

let handler_loop handler_env specialized_events handlers
    typed_functions untyped_functions concretized_types concretized_itypes
    automaton starting_state memotable imemotable =
  let hcell = ref handlers in
  let tcell = ref typed_functions in
  let utcell = ref untyped_functions in
  let tbl =
    (Hashtbl.create(500) :
       (automaton_state, automaton_state Model.sequence) Hashtbl.t) in
  let exttbl =
    (Hashtbl.create(500) :
       (automaton_state, automaton_state Model.sequence) Hashtbl.t) in
  let (configs,transitions) =
    Model.model starting_state
      (get_output_states handler_env specialized_events hcell tcell utcell
	 concretized_types concretized_itypes
	 automaton memotable imemotable
	 handlers)
      find_transition tbl exttbl print_config in
  (configs,(!hcell),(!tcell)@(!utcell))

let rec verif_handlers default handler_env specialized_events event_env
    interface_env handlers typed_ifunctions untyped_ifunctions =
  let memotable =
    (Hashtbl.create(500) :
      (Ast.event_name list * Ast.event_name * VB.state_env, VB.state_env list)
       Hashtbl.t) in
  let imemotable =
    (Hashtbl.create(500) :
      (B.identifier * VB.state_env, VB.state_env list) Hashtbl.t) in

  let (init_configs, init_handlers, init_ifunctions) =
     (* A PS initially runs its new_initial_process event, if it is a default
  scheduler and the first to be loaded, or it runs its attach function *)
	let init_state =
	  List.sort VB.compare_state_info
	    (List.map
	       (function
		   (_,(CS.STATE(id,CS.NOWHERE,ty,_) as st)) ->
		     (st,VB.MM([VB.FROMSTATE(VB.ARG(1),st)],
			       [VB.FROMSTATE(VB.ARG(1),st)]))
		 | (_,st) -> (st,VB.EMPTY))
	       (!CS.state_env)) in
	let (init_configs, init_handlers, _) =
	  if default
	  then
	    handler_loop handler_env specialized_events handlers [] []
	      (normalize_all event_env) (normalize_all interface_env)
	      (Events.init_automaton())
	      [([],[],Events.automaton_start,init_state)]
	      memotable imemotable
	  else ([], handlers, []) in
	let (init_attach_configs, init_ifunctions) =
	  List.fold_left
	    (function (new_configs,new_ifunctions) ->
	      function (Ast.FUNDEF(ret,nm,params,stmt,inl,a),proc_args) as x ->
		if B.id2c nm = "attach"
		then
		  (VB.handler_name := pr "%s: " (B.id2c nm);
		  let (new_stmt,outputs) =
		    verif_one_ifunction nm proc_args stmt a
		      [(init_state,[],VB.nullstpfn,[],[])]
		      None imemotable false None in
		  (outputs@new_configs,
		   ((Ast.FUNDEF(ret,nm,params,new_stmt,inl,a),proc_args)
		   :: new_ifunctions)))
		else (new_configs,x::new_ifunctions))
	    ([],[]) typed_ifunctions in
	let init_ifunctions = List.rev init_ifunctions in
	(init_configs @
	 (List.map (function x -> ([],[],Events.automaton_start,x))
	    init_attach_configs),
	 init_handlers, init_ifunctions) in
  let init_configs =
    Aux.make_set
      (List.map
	 (function (_,_,_,config) ->
	   ([],[],Events.automaton_start,normalize config))
	 init_configs) in
  let (configs,handlers,ifunctions) =
    handler_loop handler_env specialized_events init_handlers
      init_ifunctions untyped_ifunctions (normalize_all event_env)
      (normalize_all interface_env)
      (Events.automaton()) init_configs memotable imemotable in
  (handlers, ifunctions)

(* ------------------- Preprocessing of event and core handlers ------------------- *)

let phandler_environment =
  ref ([] : (Ast.event_name * Ast.event_name list) list)

let chandler_environment =
  ref ([] : (Ast.event_name * Ast.event_name list) list)


let preprocess_handlers events handlers procdecls globaldefs =
  let event_names =
    Aux.option_filter
      (function (nm,_) ->
	match (Ast.event_name2c nm) with
    (*FIXME I don't know what generic_interface corresponds to *) 
	  ("generic_interface") -> None
	| _ -> Some nm)
      events in
  (* let handler_names = *)
    List.map (function Ast.EVENT(name,_,_,_,_) -> name) handlers (* in
  Hierarchy.associate event_names handler_names procdecls globaldefs *)

let preprocess_specialized_events handler_env top_level_events =
  let tlnames =
    Events.top_level_event_names_loop
      (function prev -> function last ->
	function specd_name -> function star -> function attr ->
	let concretized_prev =
	  List.map (handler_name_lookup handler_env) prev in
	let suffixes =
	  List.rev(Aux.non_empty_suffixes concretized_prev) @ [[]] in
	let chosen_prev =
	  List.find
	    (List.for_all (function [x] -> true | _ -> false))
	    suffixes in
	(List.flatten chosen_prev,last))
      (function entry_point -> ())
      top_level_events in
  List.flatten (List.map (function (x,_) -> x) tlnames)

let instantiate_top_level_handlers handlers handler_env top_level_events =
  let entry_points = List.map List.hd top_level_events in
  let (invert_env,new_handler_env) =
    List.fold_left
      (function (invert_env,new_handler_env) ->
	function (Ast.EVENT_NAME(nm,_,_) as x, l) ->
	  if List.mem x entry_points
	  then
	      let current_handlers = handler_name_lookup handler_env x in
	      let (shorter,ok) =
		List.partition
		  (function Ast.EVENT_NAME(nm1,_,_) ->
		    List.length nm1 < List.length nm)
		  current_handlers in
	      match shorter with
		[] -> (invert_env, (x,l)::new_handler_env)
	      |	[y] -> ((y,x) :: invert_env, (x,x::ok)::new_handler_env)
	      |	_ -> internal_error(None,16)
	  else (invert_env,(x,l)::new_handler_env))
      ([],[]) handler_env in
  let new_handlers =
    List.flatten
      (List.map
	 (function Ast.EVENT(name,specd,param,stmt,attr) as x ->
	   x ::
	   (Aux.option_filter
	      (function (hnm,tynm) ->
		if Ast.event_name_equal name hnm
		then Some (Ast.EVENT(tynm,specd,param,stmt,attr))
		else None)
	      invert_env))
	 handlers) in
  (new_handlers,new_handler_env)

(* ------------------ Preprocessing of event types ----------------- *)

let process_specifications printer (nm,types) =
  (nm,
   List.map
     (function (q,inputs,outputs) ->
       (q,
	Aux.flat_map
	  (function (input,fwds) ->
	    List.map
	      (function (env,phi,stpfn) ->
		(env,phi,stpfn,fwds,input))
	      (VB.make_input_envs input))
	  inputs,
	outputs))
     types)

(* Initialises process, core, and interface environment *)
let init_envs default =
  let phandler_env =
    List.map (process_specifications Ast.event_name2c)
      ((
	  Events.event_specifications
	  )()) in
  (*FIXME Event specifications for core handlers *)
  let chandler_env = [] in
  let interface_env =
	List.map (process_specifications B.id2c)
	  (Events.interface_specifications())
    in
  (phandler_env,chandler_env,interface_env)

(* ---- create unblock.timer.notarget from unblock.timer.target ----- *)

(* we can't analyze unblock.timer.notarget because it refers to e.target,
which is not part of its event type *)

let make_unblock_timer_notarget handlers = handlers
  
(* -------------------- Verification of functions ------------------- *)
(* At this point, these are exported functions, as the others have been
inlined.  There are no event types because functions can't change process
states, and anyway, these functions are called from a completely unknown
context. *)

let verif_functions functions =
  List.map
    (function Ast.FUNDEF(ret,nm,params,stmt,inl,attr) ->
      (* envs can't contain anything interesting because an exported
	 function can't include state change operations *)
      let unknown_state_env =
	(List.map (function (_,st) -> (st,VB.init_cd st)) (!CS.state_env)) in
      let (stmt, envs) = verif_stmt stmt [] unknown_state_env [] in
      Ast.FUNDEF(ret,nm,params,stmt,inl,attr))
    functions

(* --------------------- Verification of aspects -------------------- *)
(* The purpose is just to find assignments to fields of processes that
may be on the ready queue.  If the starting point of the aspect is not
the sorted ready queue, we don't do any analysis. *)

let verif_aspect
    ((Ast.ASPECT(src,src_infos,dst,dst_infos,param,pos,body,fvs,a)) as x) =
  let find_sorted infos = (* raises Not_found if not found *)
    List.find
      (function
	  CS.STATE(_,_,CS.QUEUE(CS.SELECT(_)),_) -> true
	| _ -> false)
      infos in
  let process infos new_pos =
    try
      let sorted = find_sorted infos in
      let Ast.VARDECL(_,id,_,_,_,_) = param in
      let init_state =
	List.sort VB.compare_state_info
	  (List.map
	     (function (_,st) ->
	       if st = sorted
	       then (st,VB.MM([VB.UNKNOWN],[VB.UNKNOWN]))
	       else (st,VB.MM([],[VB.UNKNOWN])))
	     (!CS.state_env)) in
      let (new_body,_) = verif_stmt body [(id,VB.UNKNOWN)] init_state [] in
      Ast.ASPECT(src,src_infos,dst,dst_infos,param,new_pos,new_body,fvs,a)
    with Not_found ->
      Ast.ASPECT(src,src_infos,dst,dst_infos,param,new_pos,body,fvs,a) in
  match pos with
    Ast.BEFORE -> process src_infos pos
  | Ast.AFTER  -> process dst_infos pos
  | Ast.ON ->
      (try
	let _ = find_sorted src_infos in
	process dst_infos Ast.AFTER
      with Not_found ->
	(try
	  let _ = find_sorted dst_infos in
	  Ast.ASPECT(src,src_infos,dst,dst_infos,param,Ast.BEFORE,body,fvs,a)
	with Not_found -> x))

(* -------------------------- entry point --------------------------- *)

let verify ast =
  let ast = Lock_verif.analyze ast in
  let (Ast.SCHEDULER(nm,default,
		     cstdefs,enums,dom,grp,procdecls,fundecls,valdefs,states,cstates,
		     criteria,admit,trace,phandlers,chandlers,ifunctions,functions,
		       aspects,attr)) =
    Pre_verif.preprocess ast in
  (* Initialises environment *)
  Printf.printf "Start init env";
  (* FIXME: handle core events properly *)
  let (pevents, cevents, interface_env) = init_envs default in
  Printf.printf "Init Env";
  (* Sets Linux version *)
  let top_level_events = Events.top_level_events() in
  Printf.printf "Preprocess handlers";
   let phandler_env = List.map (function Ast.EVENT(name,_,_,_,_) -> name) phandlers in
  let chandler_env = List.map (function Ast.EVENT(name,_,_,_,_) -> name) chandlers in 
  (* Collect event names *)
  phandler_environment := phandler_env;
  chandler_environment := chandler_env;
  system_events :=
    Aux.flat_map
      (function nm -> List.assoc nm phandler_env)
      (Events.system_events());
  let (ignore,type_provided,no_type_provided) =
    classify_ifunctions ifunctions interface_env in
  let (new_handlers,new_ifunctions) =
    verif_handlers default phandler_env (Aux.make_set specialized_events)
      pevents interface_env phandlers type_provided no_type_provided in
  let new_handlers = make_unblock_timer_notarget new_handlers in
  let new_ifunctions = List.map (function (f,_) -> f) new_ifunctions in
  let new_functions = verif_functions functions in
  let new_aspects =
    match aspects with
      Ast.ASPECTINFO(aspects) ->
      Ast.ASPECTINFO(
      List.map
	(function (transition,(bef,aft)) ->
	  (transition,(List.map verif_aspect bef,List.map verif_aspect aft)))
	aspects)
    | _ -> failwith "verifier: unexpected aspects" in
  Ast.SCHEDULER(nm,default,
		cstdefs,enums,dom,grp,procdecls,
		fundecls,valdefs,states,cstates,criteria,
		admit,trace,new_handlers,chandlers,ignore@new_ifunctions,new_functions,
		new_aspects,attr)
