(* Generate locks for shared resources *)

module A = Ast
module B = Objects
module CS = Class_state

(* Functions *)
let process_function = function
    A.FUNDEF(tl,typ,id,params,A.SEQ(vdef,stmts,satt),inl,attr) as f ->
    if tl = Ast.TRYLOCK
    then (
      match params with
        A.VARDECL(_,c1,_,_,_,_,a1)::A.VARDECL(_,c2,_,_,_,_,a2)::r -> (
        let unlock = A.PRIMSTMT(A.VAR(B.mkId "ipanema_unlock_cores",[],attr),
                                [A.FIELD(A.VAR(c1,[],a1),
                                         B.mkId "id", [], a1);
                                 A.FIELD(A.VAR(c2,[],a2),
                                         B.mkId "id", [], a2)],
                                attr) in
        let lock = A.IF(A.UNARY(A.NOT,
                                A.PRIM(A.VAR(B.mkId "ipanema_trylock_cores",[],attr),
                                       [A.FIELD(A.VAR(c1,[],a1),
                                                B.mkId "id", [], a1);
                                        A.FIELD(A.VAR(c2,[],a2),
                                                B.mkId "id", [], a2)],
                                       attr),
                                attr),
                        A.RETURN(Some (A.BOOL(false, attr)), attr),
                        A.SEQ([],stmts@[unlock],attr), attr) in
        A.FUNDEF(tl,typ,id,params,A.SEQ(vdef,[lock],satt),inl,attr)
      )
      | _ -> raise (Error.Error "try functions should have 2 core parameters")
    )
    else
      f

(* --------------------------- Entry point -------------------------- *)
let generate_lock
      (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		     fundecls,valdefs,domains,states,cstates,criteria,
		     trace,handlers,chandlers,ifunctions,functions,attr)) =
  let functions = (*List.map process_function*) functions in
  Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		fundecls,valdefs,domains,states,cstates,criteria,
		trace,handlers,chandlers,ifunctions,functions,attr)
