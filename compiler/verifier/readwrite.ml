(* Check that global variables and fields are written before they are read. This will generate an error.
 * Approximate control flow, ie nothing done about control flow from the bottom of a loop to the top.
* Lazy variables, system variables and timers are assumed
* to always be initialised.
* Currently assumes that all variable names are unique. There is no notion of scope in the code logic *)

module B = Objects
module CS = Class_state

let error ln str =
  Error.update_error();
  Printf.printf "%s: %s \n" (B.loc2c ln) str

let warning ln str =
  Printf.printf "warning: %s: %s \n" (B.loc2c ln) str

let pr = Printf.sprintf

let in_attach = ref false

let class_ids = List.map CS.class2c (CS.all_classes false)

let is_state id =
  try let _ = List.assoc id (!CS.state_env) in true
  with Not_found -> List.mem (B.id2c id) class_ids

let idunion l1 l2 =
  let names2 = List.map (function (id,_) -> id) l2 in
  let rec loop = function
      [] -> l2
    | ((id,ln) as cur)::rest ->
	if List.mem id names2 then loop rest else cur::(loop rest) in
  loop l1

(* checks variables and fields read by an expression *)
let rec check_exp write_field env field_env = function
    Ast.VAR(id,_,ln) ->
      if List.mem id env || is_state id
      then ([],[])
      else ([(id,ln)],[])
  | Ast.FIELD(exp,id,_,ln) ->
      let (read_var,read_field) =
	check_exp write_field env field_env exp in
      if !in_attach && not (List.mem id field_env) (* timers *)
      then
	(try let _ = List.assoc id write_field in ()
	with Not_found ->
	  error ln (pr "field %s read before it is written in attach"
		      (B.id2c id)));
      (read_var, idunion [(id,ln)] read_field)
  | Ast.UNARY(uop,exp,ln) -> check_exp write_field env field_env exp
  | Ast.BINARY(bop,exp1,exp2,ln) ->
      let (read_var1,read_field1) =
	check_exp write_field env field_env exp1 in
      let (read_var2,read_field2) =
	check_exp write_field env field_env exp2 in
      (idunion read_var1 read_var2,idunion read_field1 read_field2)
  | Ast.PBINARY(bop,exp1,states1,exp2,states2,ln) ->
      let (read_var1,read_field1) =
	check_exp write_field env field_env exp1 in
      let (read_var2,read_field2) =
	check_exp write_field env field_env exp2 in
      (idunion read_var1 read_var2,idunion read_field1 read_field2)
  | Ast.INDR(exp,ln) -> check_exp write_field env field_env exp
  | Ast.PRIM(fn,args,ln) ->
      List.fold_left
	(function (read_var,read_field) ->
	  function x ->
	    let (read_var1,read_field1) =
	      check_exp write_field env field_env x in
	    (idunion read_var1 read_var,idunion read_field1 read_field))
	(check_exp write_field env field_env fn) args
  | Ast.SCHEDCHILD(exp,procs,ln) ->
      check_exp write_field env field_env exp
  | Ast.IN(exp,st,sti,tv,insrc,crit,ln) ->
      let (read_var,read_field) =
	check_exp write_field env field_env exp in
      (read_var,read_field)
       | Ast.EMPTY(st,sti,tv,insrc,ln) ->
      ([],[])
  | _ -> ([],[])


(* checks variables and fields read or written by a statement *)
let rec check_stm read_var read_field write_var write_field
    (env : B.identifier list) (field_env : B.identifier list) = function
    Ast.IF(exp,st1,st2,ln) ->
      let (read_var1,read_field1) = check_exp write_field env field_env exp in
      let read_var = Aux.union read_var1 read_var in
      let read_field = Aux.union read_field1 read_field in
      let (read_var2,read_field2,write_var2,write_field2) =
	check_stm read_var read_field write_var write_field env field_env st1 in
      let (read_var3,read_field3,write_var3,write_field3) =
	check_stm read_var read_field write_var write_field env field_env st2 in
      (idunion read_var2 read_var3,
       idunion read_field2 read_field3,
       idunion write_var2 write_var3,
       idunion write_field2 write_field3)
  | Ast.FOR(id,_,sts,_,stmt,_,ln) ->
      check_stm read_var read_field write_var write_field (id::env) field_env
	stmt
  | Ast.FOR_WRAPPER(_,stmts,ln) ->
      List.fold_left
	(function (read_var,read_field,write_var,write_field) ->
	  function stmt ->
	    check_stm read_var read_field write_var write_field
	      env field_env stmt)
	([],[],[],[]) stmts
  | Ast.SWITCH(exp,lsc,st2,ln) ->
      let (read_var0,read_field00) = check_exp write_field env field_env exp in
      let read_var = idunion read_var0 read_var in
      let read_field = idunion read_field00 read_field in
      List.fold_left
	(function (read_var2,read_field2,write_var2,write_field2) ->
	  function Ast.SEQ_CASE(_,_,st1,_) ->
	    let (read_var1,read_field1,write_var1,write_field1) =
	      check_stm read_var read_field write_var write_field
		env field_env st1 in
	    (idunion read_var1 read_var2,
	     idunion read_field1 read_field2,
	     idunion write_var1 write_var2,
	     idunion write_field1 write_field2))
	(match st2 with
	  None -> ([],[],[],[])
	| Some st ->
	    check_stm read_var read_field write_var write_field
	      env field_env st)
	lsc
  | Ast.SEQ(decls,lstmt,ln) as seq->
      let (read_var1,read_field1,env) =
	List.fold_left
	  (function (read_var,read_field,env) ->
	    function
		Ast.VALDEF(Ast.VARDECL(ty,id,sys, laazy,_,ln),exp,const,a) ->
            let (read_var1,read_field1) =
		    check_exp write_field env field_env exp in
		  (idunion read_var1 read_var, idunion read_field1 read_field,
		   Aux.union [id] env)
	      |	_ ->
		  raise (Error.Error (Printf.sprintf
					"%s: unexpected local decl"
					(B.loc2c ln))))
	  ([],[],env) decls in
      let read_var = idunion read_var1 read_var in
      let read_field = idunion read_field1 read_field in
      List.fold_left
	(function (read_var,read_field,write_var,write_field) ->
	  function x ->
	    check_stm read_var read_field write_var write_field env field_env x)
	(read_var,read_field,write_var,write_field) lstmt
  | Ast.RETURN(Some exp,ln) ->
      let (read_var1,read_field1) = check_exp write_field env field_env exp in
      let read_var = idunion read_var1 read_var in
      let read_field = idunion read_field1 read_field in
      (read_var,read_field,write_var,write_field)
  | Ast.MOVE(proc,dst,srcinfo,vsrcinfo,dstinfo,auto,state_end,_) ->
      let (read_var1,read_field1) = check_exp write_field env field_env proc in
      let read_var = idunion read_var1 read_var in
      let read_field = idunion read_field1 read_field in
      (read_var,read_field,write_var,write_field)
  | Ast.MOVEFWD(proc,srcdst,state_end,_) ->
      let (read_var1,read_field1) = check_exp write_field env field_env proc in
      let read_var = idunion read_var1 read_var in
      let read_field = idunion read_field1 read_field in
      (read_var,read_field,write_var,write_field)
  | Ast.ASSIGN(lexp,rexp,affectsrtd,ln) ->
      let check str id a read write =
	(try let rl = List.assoc id read in (* prevly read *)
	try let _ = List.assoc id write in ()(*prevly written too*)
	with Not_found -> (
	  if B.modl rl = B.modl a
	  then warning a (pr "%s" "reached here")
	  else
	    warning a (pr "written %s %s previously read in %s"
             str (B.id2c id) (B.loc2c rl)))
	with Not_found -> warning a ((pr "%s" "Not previously used"))) in
      let (read_var1,read_field1) = check_exp write_field env field_env rexp in
      let read_var = idunion read_var1 read_var in
      let read_field = idunion read_field1 read_field in
      let (read_var2,read_field2,write_var2,write_field2) =
	match lexp with
	  Ast.VAR(id,_,a) ->
	    if List.mem id env
	    then ([],[],[],[])
	    else (* only check for non-local variables *)
	      begin
		check "variable" id a read_var write_var;
		([],[],[(id,a)],[])
	      end
	| Ast.FIELD(exp,id,_,a) ->
	    let (read_var3,read_field3) =
	      check_exp write_field env field_env exp in
	    check "field" id a read_field write_field;
	    (read_var3,read_field3,[],[(id,a)])
	| _ -> raise (Error.Error "unexpected left-hand side expression") in
      let read_var = idunion read_var2 read_var in
      let read_field = idunion read_field2 read_field in
      let write_var = idunion write_var2 write_var in
      let write_field = idunion write_field2 write_field in
      (read_var,read_field,write_var,write_field)
  | Ast.PRIMSTMT(fn,args,ln) ->
      let (read_var1,read_field1) =
	List.fold_left
	  (function (read_var,read_field) ->
	    function x ->
	      let (read_var1,read_field1) =
		check_exp write_field env field_env x in
	      (idunion read_var1 read_var,idunion read_field1 read_field))
	  (check_exp write_field env field_env fn) args in
      let read_var = idunion read_var1 read_var in
      let read_field = idunion read_field1 read_field in
      (read_var,read_field,write_var,write_field)
  | Ast.ASSERT(exp,ln) ->
      let (read_var1,read_field1) = check_exp write_field env field_env exp in
      let read_var = idunion read_var1 read_var in
      let read_field = idunion read_field1 read_field in
      (read_var,read_field,write_var,write_field)
  (* the following can't occur or no expression subterms *)
  | _ -> (read_var,read_field,write_var,write_field)

(* --------------------- Handlers and functions --------------------- *)

(* Checks variables have previously been assigned in core and process handlers. Ignores
 * timers, system variables and lazy variables as these are
 * computed on the fly and hence do not need to be assigned before
 * being read *)
let process_handlers ignored_vars process_handlers core_handlers =
  let handlers = process_handlers@core_handlers in
  List.iter
    (function (Ast.EVENT(nm,param,stmt,attr)) ->
      in_attach:= false;
      (* Adds events to list of ignored variables *)
      let locals = Aux.union ignored_vars (Ast.params2ids [param]) in
      (* This currently assumes that the only fields
       * are the event parameters *)
      let _ = check_stm [] [] [] [] locals ignored_vars stmt in ())
   handlers

(* Checks variables have previously been assigned in interface   functions. Ignores
 * timers, system variables and lazy variables as these are
 * computed on the fly and hence do not need to be assigned before
 * being read *)
let process_ifunctions ignored_vars ifunctions =
  List.iter
    (function Ast.FUNDEF(_,ret,nm,params,stmt,inl,attr) ->
      in_attach := (B.id2c nm = "attach");
      let locals = Aux.union ignored_vars (Ast.params2ids params) in
      let _ = check_stm [] [] [] [] locals ignored_vars stmt in ())
    ifunctions

(* --------------------------- Entry point -------------------------- *)

(* This function ensures that all variables that are not lazy, system variables
 * or timers are initialised before they are read *)
let readwrite(Ast.SCHEDULER (nm,
             cstdefs,enums,dom,grp,procdecls,fundecls,valdefs,domains,states,cstates,
		     criteria,trace,phandlers,chandlers,ifunctions,functions,
		     attr))  =
  (* First filter out variables in fields of the process
   * structure, of the core structure. These are variables that  should be ignored. There is no need to
   * consider global variable declarations as these variables must be defined, not simply declared. *)
    (* Timers in processes*)
    let timer_vars_processes = Aux.option_filter
      (function
	  Ast.VARDECL(B.TIMER(_),id,_,_,_,_) -> Some id
	    | _ -> None)
      procdecls in
    let timer_vars_cores =
          begin match states with
            Ast.PSTATE(vars) -> []
            | Ast.CORE(cv,processes) ->  Aux.option_filter
                (function
	            Ast.VARDECL(B.TIMER(_),id,_,_,_,_) -> Some id
                | _ -> None)
               cv
        end in
    let timer_vars  = timer_vars_processes@timer_vars_cores in
    (* Processes *)
    let sys_vars_processes = Aux.option_filter
      (function
	    Ast.VARDECL(_,id,true,_,_,_) -> Some id
	    | _ -> None)
      procdecls in
    let sys_vars_cores=
          begin match states with
            Ast.PSTATE(vars) -> []
            | Ast.CORE(cv,processes) ->  Aux.option_filter
                (function
	            Ast.VARDECL(_,id,true,_,_,_) -> Some id
	            | _ -> None)
                cv
            end in
    let sys_vars = sys_vars_processes@timer_vars_cores in
    let lazy_vars_processes = Aux.option_filter
      (function
	    Ast.VARDECL(_,id,_,true,Some x,_)-> Some id
	    | _ -> None)
      procdecls in
    let lazy_vars_cores =
          begin match states with
            Ast.PSTATE(vars) -> []
            | Ast.CORE(cv,processes) ->  Aux.option_filter
                (function
	            Ast.VARDECL(_,id,_,true,Some x,_) -> Some id
	            | _ -> None)
                cv
        end in
    let lazy_vars = lazy_vars_processes@lazy_vars_cores in
    (* Next, add constants *)
    let consts = Aux.option_filter
      (function
	  Ast.VALDEF(Ast.VARDECL(_,id,_,_,_,_),_,_,_) -> Some id
    | Ast.SYSDEF(Ast.VARDECL(_,id,_,_,_,_),_,_) -> Some id
	| _ -> None)
      cstdefs in
    let ignored_vals = timer_vars@sys_vars@lazy_vars@consts in
 let _ =
    (* Next, check that there are no other variables used before they
 * are assigned in core/process handlers, interface functions
 * and user-defined functions *)
    process_handlers ignored_vals phandlers chandlers;
    (* No need to check functions because they will automaticall
     * be inlined by the process handler calls *)
    process_ifunctions ignored_vals in () ;
