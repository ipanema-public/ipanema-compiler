(* $Id$ *)

module B = Objects
module VB = Vobjects
module CS = Class_state
module ET = Event_types
module ETBC = Event_types_bc (* all BLOCKED classes compressed to BLOCKED *)
module ET26 = Event_types26
module ET26BC = Event_types26_bc(* all BLOCKED classes compressed to BLOCKED *)
(*module ETH = Event_types_hls*)

exception LookupErrException

(* -------------------------- various RTSes ------------------------- *)

type entry = CS.classname * VB.vtype * VB.modified

type type_rule =
    B.isquasi * (entry list * CS.classname list) list * entry list list

let app_events_init_automaton() =
  (*if (!B.hls)
  then ETH.app_events_init_automaton()
  else*)
  match !B.config with
   B.LINUX -> ETBC.app_events_init_automaton()
  | B.LINUX26 -> ET26BC.app_events_init_automaton()

let app_events_automaton() =
  (*if (!B.hls)
  then ETH.app_events_automaton()
  else*)
  match !B.config with
  B.LINUX -> ETBC.app_events_automaton()
  | B.LINUX26 -> ET26BC.app_events_automaton()

let system_events() =
  (*if (!B.hls)
  then ETH.system_event_specifications()
  else*)
  match !B.config with
   B.LINUX -> ETBC.system_event_specifications()
  | B.LINUX26 -> ET26BC.system_event_specifications()

let event_specifications() =
  (*if (!B.hls)
  then ETH.event_specifications()
  else*)
  match !B.config with
   B.LINUX -> ETBC.event_specifications()
  | B.LINUX26 -> ET26BC.event_specifications()

let vs_event_specifications() =
  (*if (!B.hls)
  then ETH.vs_event_specifications()
  else*)
  match !B.config with
   B.LINUX -> ETBC.vs_event_specifications()
  |B.LINUX26 -> ET26BC.vs_event_specifications()

let initial_event_specifications() =
  (*if (!B.hls)
  then ETH.initial_event_specifications()
  else*)
  match !B.config with
   B.LINUX -> ETBC.initial_event_specifications()
  | B.LINUX26 -> ET26BC.initial_event_specifications()

let interface_specifications() =
  (*if (!B.hls)
  then ETH.interface_specifications()
  else*)
  match !B.config with
   B.LINUX -> ETBC.interface_specifications()
  | B.LINUX26 -> ET26BC.interface_specifications()

let vs_interface_specifications() =
  (*if (!B.hls)
  then ETH.vs_interface_specifications()
  else*)
  match !B.config with
   B.LINUX -> ETBC.vs_interface_specifications()
  | B.LINUX26 -> ET26BC.vs_interface_specifications()

let top_level_events() =
  (*if (!B.hls)
  then ETH.top_level_events()
  else*)
  match !B.config with
   B.LINUX -> ETBC.top_level_events()
  | B.LINUX26 -> ET26BC.top_level_events()

(* --------------------- misc ---------------------- *)

let rec lookup x = function
    [] -> raise LookupErrException
  | ((y,v)::r) -> if x = y then v else lookup x r

let rec addto_env nm vl = function
    [] -> [(nm,[vl])]
  | ((n,v) as x)::r ->
      if nm = n
      then
	if Aux.member vl v
	    (* should be checked by config compiler *)
	then raise (Error.Error "repeated Qs not allowed")
	else (n,vl::v)::r
      else x :: (addto_env nm vl r)

(* ------------------------------ Misc ------------------------------ *)

(* identifiers are state names in the following *)

let init_events = (ref [] : Ast.event_name list ref)
(*
let app_events  = (ref [] : Ast.event_name list ref)
let sys_events  = (ref [] : Ast.event_name list ref)
*)
let init_automaton = (ref [] : (int * Ast.event_name * int) list ref)

let all_event_names = (ref [] : Ast.event_name list ref)

let automaton_start = 0

let drop_attach_detach aut =
  List.filter
    (function
	(src,nm,int,dst) ->
	  match Ast.event_name2c nm with
	    "attach" -> false (* not PS events *)
	  | "detach" -> false
	  | _ -> true)
    aut

let init_automaton _ =
  let aut = app_events_init_automaton() in
    drop_attach_detach aut

let automaton _ =
  let aut = app_events_automaton() in
  drop_attach_detach aut

(* --------------------- top-level event names ---------------------- *)

let top_level_event_names_loop row_fn entry_fn top_level_events =
  List.map
    (function row ->
      let (_,_,res1) =
	List.fold_left
	  (function (prev_events,prev_ids,res) ->
	    function Ast.EVENT_NAME(nm,s,a) as x ->
	      let extended = prev_ids @ [nm] in
	      (prev_events @ [x],
	       extended, (row_fn prev_events x extended s a) :: res))
	  ([],[],[]) row in
      let res2 = entry_fn (List.hd row) in
      (List.rev res1,res2))
    top_level_events

let top_level_event_names = ref ([] : Ast.event_name list)

let get_top_level_event_names top_level_events =
  let tlres =
    top_level_event_names_loop
      (function _ -> function _ ->
	function specd_name -> function star -> function attr ->
	Ast.EVENT_NAME(List.flatten specd_name,star,attr))
      (function entry_point -> ())
      top_level_events in
  List.flatten (List.map (function (x,_) -> x) tlres)

let flatten_specd_name specd (Ast.EVENT_NAME(nm,s,a)) =
  let name = (List.fold_left
		(function whole_name ->
		  function (Ast.EVENT_NAME(specdnm,_,_)) ->
		    whole_name @ specdnm)
		[] specd) @ nm in
  (Ast.EVENT_NAME(name,s,a))

(* filled in later, in verification, when handlers are available *)
let specialized_events =
  ref ([] : (Ast.event_name list * Ast.event_name) list)

(* -------------------------- entry point --------------------------- *)

(* sort by event name, such that the most specific names comes first *)
let init_event_env _ =
  init_events := initial_event_specifications();
(* not currently used
  app_events := application_event_specifications();
  sys_events := system_event_specifications();
*)
  all_event_names :=
      List.map (function (nm,_) -> nm) (event_specifications());
  top_level_event_names :=
    get_top_level_event_names (top_level_events())
