(*check if there is any assignment from user to the cload*)
(*cload should only be modified by the compiler*)

module B = Objects
module CS = Class_state

let rec error ln str =(
Error.update_error();
Printf.printf "%s: %s \n" (B.loc2c ln) str);;

let rec warning ln str =( Printf.printf "warning: %s: %s \n" (B.loc2c ln) str);;

let rec pr = (Printf.sprintf);;

let rec checkmultStmts list = match list with
  |[] -> true
  |stmt::rest -> (check_Stmt stmt) && (checkmultStmts rest)

  and check_Stmt  = (function
  Ast.ASSIGN(Ast.FIELD( exp , id , _ , _ ) , _, _ , _ ) ->
     not (B.ty  (Ast.get_exp_attr exp) = B.CORE && B.id2c id = "cload")
| Ast.IF(exp,stm1,stm2,attr) -> (check_Stmt stm1) && (check_Stmt stm2)
| Ast.FOR(id,stid,l,dir,stm,crit,attr) -> (check_Stmt stm)
| Ast.FOR_WRAPPER(label,stms,attr) -> (checkmultStmts stms)
| Ast.SEQ(decls,stmts,attr) -> (checkmultStmts stmts)
| Ast.SWITCH(_ , seq, stmtopt ,_) ->
  let def = match stmtopt with
      None -> true
    | Some stmt -> (check_Stmt stmt)
   in
   let newList = List.map (function Ast.SEQ_CASE(_,_,stmt,_) -> check_Stmt stmt) seq in newList;
   List.fold_left (fun def x -> def && x) def newList;
| Ast.SAFEMOVEFWD(_ , _ , stmt , _ , _) -> (check_Stmt stmt)
| stmt -> true);;



let rec check_func funcs = (match funcs with
  | [] -> true
  | Ast.FUNDEF( _ , _ , _ , _ ,stmt, _ , _ ) :: restF ->
    ((check_Stmt stmt) && (check_func restF))
  | _ -> raise (Error.Error "Unexpected function") );;

let rec check_Handler handler = (match handler with
  | [] -> true
  | Ast.EVENT( _ , _ ,stmt,_ , _) :: restH ->
     ((check_Stmt stmt) && (check_Handler restH))
  | _ ->  raise (Error.Error "Unexpected handle ") );;

(* let rec check_Handler handler =
  List.map (function (Ast.EVENT(nm,param,stmt,attr)) -> Ast.EVENT(nm,param,check_Stmt stmt,attr)) handler *)

(************************ entry point ****************************************)
let rec user_modify_cload (Ast.SCHEDULER(nm,
  cstdefs,enums,dom,
  grp,procdecls,fundecls,
  valdefs,domains,pcstate,cstates,
   criteria,trace,handlers,
   chandlers,ifunctions,functions,attr)) =
                         ((check_Handler handlers) &&
                          (check_Handler chandlers ) &&
                           (check_func ifunctions) &&
                            (check_func functions));;
