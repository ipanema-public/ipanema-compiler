(* $Id$ *)

(* This does nothing for interface functions *)
(* maybe should check for overlapping or contradictory assertions *)
(* for NOWHERE, should check that the process is not in some real state*)

(* problem: lookup_handler works when the event type name is more specific
than the handler name (eg event type = unblock.schedule, handler =
unblock.* ), but does not work when the event type name is less specific
than the handler name (eg event type = unblock.*, handler name =
unblock.schedule.  In this case, unblock.* should match everything
beginning with unblock, but perhaps not the things for which a more
specific event type has already been specified. *)
(* slight improvement: unblock.* should now match everything beginning with
unblock, but there is no effort to detect whether there is something else
that would match better. *)

module B = Objects
module VB = Vobjects
module CS = Class_state

(* -------------------- Miscellaneous operations -------------------- *)

let handler_name = ref ""

let for_ctr = ref 0 (* to create recognizable unknown variables for fors *)

exception StmtErrException
exception ExpErrException
exception AssertErrException of string

let pr = Printf.sprintf

let estrs = ref ([] : string list)

let error (line,str) =
  let estr =
    if (line > 0)
    then Printf.sprintf "line %d: %s%s" line (!handler_name) str
    else str in
  if not (List.mem estr (!estrs))
  then
    begin
      Error.update_error();
      estrs := estr :: (!estrs);
      Printf.printf "%s\n" estr
    end
  else ()

let internal_error(line,n) =
  if (line > 0)
  then
    raise (Error.Error(pr "line %i: %sinternal error %i"
			 line (!handler_name) n))
  else raise (Error.Error(pr "%sinternal error %i" (!handler_name) n))

let debug = false

(* --------------------- The state environment --------------------- *)

type state_env =
    (B.identifier *               (* state name *)
       (VB.vtype *                (* contents description *)
	  CS.state_info list *    (* list of possible sources *)
	  VB.modified))           (* has this state ever been modified *)
      list

(* state in app automaton and initial configuration *)
let current_input_config = ref (0,([] : state_env))

type var_env =
    (B.identifier *               (* variable name *)
       (CS.state_info list *      (* states possibly containing its value *)
	  VB.vproc list))         (* possible values of the variable *)
      list

(* ---------------------------- Errors ----------------------------- *)

let config2c env =
  Aux.set2c
    (List.fold_left
       (function prev ->
	 function
	    (* (x,(VB.UNKNOWN,_,_)) -> prev
	   |*) (x,(y,_,VB.MODIFIED)) ->
	       ((B.id2c(x))^"! -> "^(VB.vt2c(y))) :: prev
	   | (x,(y,_,_)) -> ((B.id2c(x))^" -> "^(VB.vt2c(y))) :: prev)
       [] env)

let venv2c venv = Aux.set2c (List.map (function(id,_)->B.id2c id) venv)

let state_name (CS.STATE(id,_,_,_)) = id

let states2c states =
  Aux.set2c (List.map (function st -> B.id2c(state_name st)) states)

(* in more detail ...
let states2c states =
  Aux.set2c
    (List.map
       (function
	   CS.STATE(id,cls,ty,vis) ->
	     Printf.sprintf "<%s,%s,%s,%s>"
	       (B.id2c id)
	       (CS.class2c cls)
	       (match ty with
		 CS.PROC -> "proc"
	       | CS.QUEUE(CS.FIFO) -> "fifo"
	       | CS.QUEUE(CS.EMPTY) -> "empty"
	       | CS.QUEUE(CS.SORTED(CS.LIFODEFAULT)) -> "lifo-sorted"
	       | CS.QUEUE(CS.SORTED(CS.FIFODEFAULT)) -> "fifo-sorted"
	       | CS.QUEUE(CS.SORTED(CS.NODEFAULT)) -> "sorted")
	       (match vis with
		 CS.PUBLIC -> "public"
	       | CS.PRIVATE -> "private"))
       states)
*)

let stmtErr (attr,str) =
  (error(-1,
	 Printf.sprintf "line %d: %s%s" (B.line attr) (!handler_name) str);
   raise StmtErrException)

let stmtErrId (attr,str,id) =
  (error(-1,
	 Printf.sprintf "line %d: %s%s %s"
	   (B.line attr) (!handler_name) str (B.id2c id));
   raise StmtErrException)

let stmtErrNoline str =
  (error(-1,Printf.sprintf "%s%s" (!handler_name) str);
   raise StmtErrException)

let expErrId(attr,str,id) =
  (error (-1,
	  Printf.sprintf "%d: %s%s %s"
	    (B.line attr) (!handler_name) str (B.id2c id));
   raise ExpErrException)

let assert_warning(line,str) =
  let (automaton_state,config) = !current_input_config in
  if (line > 0)
  then Printf.printf "line %d: %s%s assert added\n" line (!handler_name) str
  else Printf.printf "%s assert added\n" str;
  if automaton_state >= 0
  then Printf.printf "%s\n" (config2c config);
  Printf.printf "\n"

let wstrs = ref ([] : string list)

let warning(line,str) =
  let wstr =
    if (line > 0)
    then Printf.sprintf "line %d: warning: %s%s" line (!handler_name) str
    else Printf.sprintf "warning: %s" str in
  if not (List.mem wstr (!wstrs))
  then (wstrs := wstr :: (!wstrs); Printf.printf "%s\n" wstr)

(* ------------------ State environment operations ----------------- *)

(* an environment mapping process names to their initial states and
a list of the states that are nonempty initially *)
type source_env = (VB.vprocname * CS.state_info list) list * CS.state_info list

let state_names = (ref [] : B.identifier list ref)

let set_state_names states =
  state_names :=
    List.map
      (function
	  Ast.QUEUE(clsname,queue_typ,id,vis,attr) -> id
	| Ast.PROCESS(clsname,id,previd,vis,attr) -> id)
      states

exception LookupErrException of string

let rec lookup x = function
    [] -> raise (LookupErrException "")
  | ((y,v)::r) -> if x = y then v else lookup x r

let rec lookup_id x = function
    [] -> raise (LookupErrException (B.id2c x))
  | ((y,v)::r) -> if B.ideq(x,y) then v else lookup_id x r

let rec lookup3 x = function
    [] -> raise (Error.Error("lookup3: unknown class"));
  | ((y,v,_)::r) -> if x = y then v else lookup3 x r

let extend_env nm vl env = (nm,vl)::env

let extend_idenv nm vl env = 
  let rec loop = function
      [] -> raise (LookupErrException "")
    | (n,v)::r ->
	if B.ideq(n,nm) then (n,vl)::r else (n,v)::(loop r)
  in try loop env with LookupErrException _ -> (nm,vl)::env

let rec explicate_unknown id = function
    [] -> raise (LookupErrException (B.id2c id))
  | ((n,(VB.UNKNOWN,srcs,modd)) as entry) :: r ->
      if B.ideq(id,n)
      then (n,(VB.MEMBER([VB.VP(VB.UNKNOWNPROC,List.sort compare srcs)]),
	       srcs,modd))::r
      else entry::(explicate_unknown id r)
  | x :: r -> x :: (explicate_unknown id r)

let extend_all states (vt,srcs) (env : state_env) =
  List.fold_left
    (function env ->
      function CS.STATE(id,_,_,_) ->
	let (_,_,modd) = lookup_id id env in
	extend_idenv id (vt,srcs,modd) env)
    env states

let all_unknown env =
  List.map (function (id,vl) -> (id,(VB.UNKNOWN,[],VB.MODIFIED))) env

let lub_all envs : state_env =
  List.map
    (function id ->
      (id,
       List.fold_left
	 (function (pvt,psrcs,pmodd) -> function env ->
	   let (vt,srcs,modd) = lookup_id id env in
	   (VB.lub_vtype pvt vt, Aux.union psrcs srcs,
	    VB.lub_modified (pmodd, modd)))
	 (VB.BOT,[],VB.UNMODIFIED) envs))
    (!state_names)

let mk_empty_venv venv =
  List.map (function (id,_) -> (id,([],[]))) venv

let rec lub_venv venv1 venv2 model attr str =
  List.map2
    (function (id1,(srcs1,procs1)) ->
      function (id2,(srcs2,procs2)) ->
	if B.ideq(id1,id2)
	then (id1,(Aux.union srcs1 srcs2, Aux.union procs1 procs2))
	else internal_error(-1,1))
    (restrict venv1 model attr (str^"-a")) (restrict venv2 model attr (str^"-b"))

(* [] used to represent a "bottom" environment for folds
and bot_lub_venv venv1 venv2 model attr =
  match (venv1,venv2) with
    ([],[]) -> []
  | ([],x) -> restrict x model attr
  | (x,[]) -> restrict x model attr
  | _ -> lub_venv venv1 venv2 model attr *)

and restrict venv model attr str =
  try List.map (function (id,_) -> (id,lookup_id id venv)) model
  with LookupErrException _ ->
    stmtErr(attr,
	    pr "environment %s not compatible with model %s: %s"
	      (venv2c venv) (venv2c model) str)

let drop_sources l = List.map (function (st,_) -> st) l

let get_class stid =
  let CS.STATE(_,cls,_,_) = lookup_id stid (!CS.state_env) in
  cls

let order_state_env l =
  List.sort
    (function (stid1,_) ->
      function (stid2,_) ->
	let cls1 = get_class stid1
	and cls2 = get_class stid2 in
	if cls1 = cls2
	then compare stid1 stid2
	else if CS.less_than(cls1,cls2) then -1 else 1)
    l

(* ------------------- Looking up event handlers ------------------- *)

let match_event_prev prev nm events =
  List.filter
    (function (Ast.EVENT(name,specd,param,stmt,attr)) ->
      Ast.event_names_equal prev specd && Ast.event_name_equal nm name)
    events

let handler_name_lookup env nm =
  let all_matches =
    Aux.option_filter
      (function (ev_name,handler_names) ->
	if Ast.event_name_equal ev_name nm
	then Some handler_names
	else None)
      env in
  match all_matches with
    [x] -> x
  | l ->
      (error(-1,pr "handler %s found %d times"
	       (Ast.event_name2c nm) (List.length l));
       raise (Error.Error "fail");
       [])

let lookup_handler nm env prev events orig_events =
  let concrete_handlers = handler_name_lookup env nm in
  List.flatten
    (List.map
       (function nm ->
	 let first_try = match_event_prev prev nm events in
	 match (prev,first_try) with
	   (x::xs,[]) ->
	     List.map
	       (function Ast.EVENT(name,_,param,stmt,attr) ->
		 Ast.EVENT(name,prev,param,stmt,attr))
	       (match_event_prev [] nm orig_events)
	 | (_,res) -> res)
       concrete_handlers)

(* --------- Outputs associated with the current event type -------- *)

let event_param = ref(B.mkId "")

let src_tgt_handler = ref false

(* used in the treatment of forwardImmediate() *)
let possible_outputs =
  ref ([] : (Objects.identifier * Events.config) list list)

(* Old approach
(* finds the public states mentioning the source or target *)
(* if a possible resulting class is the same as a possible source class:
when there is only one possible source for the forwardimmediate, take that
state rather than the public representitive of the corresponding class.
when there are multiple source classes take those states in addition to the
public representitives. *)
let find_target_states srcs =
  let source_classes =
    Aux.make_set (List.map (function CS.STATE(_,cls,_,_) -> cls) srcs) in
  let replace = (List.length source_classes) = 1 in
  let output_classes =
    Aux.map_union
      (function possible_output ->
	Aux.make_set
	  (Aux.option_filter
	     (function (id,(vt,srcs,modd)) ->
	       match vt with
		 VB.MEMBER(l) ->
		   if VB.vp_member VB.SRC l || VB.vp_member VB.TGT l
		   then Some (get_class id)
		   else None
	       | _ -> None)
	     possible_output))
      (!possible_outputs) in
  let get_public cls =
    Aux.option_filter
      (function
	  (id,(CS.STATE(_,cls1,_,CS.PUBLIC) as x)) ->
	    if cls1 = cls then Some x else None
	| _ -> None)
      (!CS.state_env) in
  Aux.flat_map
    (function output_class ->
      if Aux.member output_class source_classes
      then
	let sources_in_class =
	  List.filter
	    (function CS.STATE(_,cls,_,_) -> cls = output_class)
	    srcs in
	if replace
	then sources_in_class
	else (get_public output_class) @ sources_in_class
      else get_public output_class)
    output_classes

(* takes everything that is non-empty and doesn't conflict with
forward_sources *)
let find_possible_target_states forward_sources =
  Aux.map_union
    (function possible_output ->
      Aux.option_filter
	(function (id,(vt,srcs,modd)) ->
	  match vt with
	    VB.MEMBER(l) ->
	      if Aux.subset forward_sources srcs
	      then
		(match lookup_id id (!CS.state_env) with
		  CS.STATE(id,cls,stateinfo,CS.PUBLIC) as x -> Some x
		| _ -> None)
	      else None
	  | _ -> None)
	possible_output)
    (!possible_outputs)
*)

(* New approach *)

let forwardImmediateOutputs = ref ([] : CS.classname list)

(* if a possible resulting class is the same as a possible source class:
when there is only one possible source for the forwardimmediate, take that
state rather than the public representitive of the corresponding class.
when there are multiple source classes take those states in addition to the
public representitives. *)
let find_target_states srcs =
  let source_classes =
    Aux.make_set (List.map (function CS.STATE(_,cls,_,_) -> cls) srcs) in
  let replace = (List.length source_classes) = 1 in
  let output_classes = !forwardImmediateOutputs in
  let get_public cls =
    Aux.option_filter
      (function
	  (id,(CS.STATE(_,cls1,_,CS.PUBLIC) as x)) ->
	    if cls1 = cls then Some x else None
	| _ -> None)
      (!CS.state_env) in
  Aux.flat_map
    (function output_class ->
      if Aux.member output_class source_classes
      then
	let sources_in_class =
	  List.filter
	    (function CS.STATE(_,cls,_,_) -> cls = output_class)
	    srcs in
	if replace
	then sources_in_class
	else (get_public output_class) @ sources_in_class
      else get_public output_class)
    output_classes

(* ---------------- Compatability of two environments -------------- *)

let rec check_compat inputs outputs print_nm attr = function
    [] -> true
  | new_env::rest ->
      if List.exists (compatible_env new_env) outputs
      then check_compat inputs outputs print_nm attr rest
      else
	(error(B.line attr,
		"\nfrom input:\n"^(config2c inputs)^
		"\nobtained output:\n"^(config2c new_env)^
		"\nwhich is not compatible with any of:");
	  List.iter (print_mismatch new_env) outputs;
	  Printf.printf "\n";
	  let _ = check_compat inputs outputs print_nm attr rest in false)

and compatible_env computed specified =
  List.for_all
    (function (id,(spec,srcs,modd)) ->
      let (computed_spec,computed_srcs,computed_modd) =
	lookup_id id computed in
      Aux.subset computed_srcs srcs &&
      VB.compatible(computed_spec,spec) &&
      (VB.lub_modified(computed_modd,modd) = modd))
    specified

and print_mismatch computed specified =
  Printf.printf "%s\n" (config2c specified);
  let _ =
  List.exists
    (function (id,(spec,srcs,modd)) ->
      let (computed_spec,computed_srcs,computed_modd) =
	lookup_id id computed in
      if VB.compatible(computed_spec,spec)
      then
	(if not(Aux.subset computed_srcs srcs)
	then (Printf.printf "%s mismatches in the allowed sources\n"
		(B.id2c id);
	      Printf.printf "computed sources %s\n" (states2c computed_srcs);
	      Printf.printf "allowed sources %s\n" (states2c srcs);
	      true)
	else
	  if not((VB.lub_modified(computed_modd,modd) = modd))
	  then
	    (Printf.printf "%s mismatches in modification:\n" (B.id2c id);
	     Printf.printf "   computed: %s, specified: %s\n"
	       (VB.modified2c computed_modd) (VB.modified2c modd);
	     true)
	  else false)
      else false)
    specified in
  ()

(* ---------------- Information about a set of states -------------- *)

(* returns empty * non_empty * unknown *)
let summarize_nonempty_states states env =
  List.fold_left
    (function (nonempty,unknown) ->
      function CS.STATE(id,clsname,state_type,_) as x ->
	let (vt,srcs,modd) = lookup_id id env in
	match vt with
	  VB.MEMBER(l) ->   (x::nonempty,unknown)
	| VB.EMPTY ->       (nonempty,unknown)
	| VB.UNKNOWN ->     (nonempty,x::unknown)
	| VB.BOT ->         internal_error(-1,2))
    ([],[]) states

(* ----------------- Moving a process between states --------------- *)

let rec move p src dst env venv =
  if src = dst
  then (env,venv)
  else
    let (remd,new_env) = srcsub_definite src p env in
    (dstadd dst remd new_env VB.MODIFIED,
     venv_move [src] p dst venv)

and srcsub_definite (CS.STATE(id,_,ty,_)) p env =
  let fn =
    match ty with
      CS.PROC -> VB.remove_proc_definite
    | CS.QUEUE(_) -> VB.remove_queue in
  let (vt,srcs,modd) = lookup_id id env in
  let (remd,newvt,newsrcs) = fn p vt srcs in
  (remd,extend_idenv id (newvt,newsrcs,modd) env)

and srcsub_possible (CS.STATE(id,_,ty,_)) p env =
  let fn =
    match ty with
      CS.PROC -> VB.remove_proc_possible
    | CS.QUEUE(_) -> VB.remove_queue in
  let (vt,srcs,modd) = lookup_id id env in
  let (remd,newvt,newsrcs) = fn p vt srcs in
  (remd,extend_idenv id (newvt,newsrcs,modd) env)

(* sometimes used to add something, and sometimes used to explicate something,
so whether addition occurs has to be an argument (modd) *)
and dstadd (CS.STATE(id,_,ty,_)) p env modd =
  let fn =
    match ty with
      CS.PROC -> VB.add_proc
    | CS.QUEUE(_) -> VB.add_queue in
  let (vt,srcs,_) = lookup_id id env in
  let vt =
    (match modd with
      VB.UNMODIFIED -> VB.drop_unknownproc vt
    | _ -> vt) in
  let (newvt,newsrcs) = fn p vt srcs in
  extend_idenv id (newvt,newsrcs,modd) env

and venv_move moved_srcs moved_procs dst venv =
  List.map
    (function (id,(srcs,procs)) ->
      match Aux.intersect moved_procs procs with
	[] -> (id,(srcs,procs))
      |	_ ->
	  (if List.length procs = 1 && procs = moved_procs
	  then (id,([dst],procs))
	  else match moved_srcs with
	    [moved_src] ->
	      (id,(Aux.union [dst] (Aux.remove moved_src srcs), procs))
	  | _ -> (id,(Aux.union [dst] srcs, procs))))
    venv

let rec moves p srcs dst env venv attr =
  match summarize_nonempty_states srcs env with
    ([],[]) -> stmtErr(attr,"all possible sources are empty")
  | ([],[src]) -> move p src dst env venv
  | ([src],[]) -> move p src dst env venv
  | (nonempty,unknown) ->
      (* all non-empty possible sources become unknown *)
      (match nonempty@unknown with
	[src] ->
	  let (remd,new_env) = srcsub_definite src p env in
	  (dstadd dst remd new_env VB.MODIFIED,
	   venv_move [src] p dst venv)
      |	all ->
	  let (remd,new_env) =
	    (List.fold_left
	       (function (remd,env) ->
		 function src ->
		   let (remd1,env) = srcsub_possible src p env in
		   (remd1::remd,env))
	       ([],env) all) in
	  let all_equal = function
	      [] -> true
	    | (x::xs) -> List.for_all (function y -> x = y) xs in
	  if all_equal remd
	  then
	    (match remd with
	      [] ->
		(dstadd dst p new_env VB.MODIFIED,
		 venv_move all p dst venv)
	    | (x::_) ->
		(dstadd dst x new_env VB.MODIFIED,
		 venv_move all p dst venv))
	  else raise(Error.Error("multiple removed processes not treated")))

(* -------------------------- Assertions --------------------------- *)

let assert_empty id info attr =
  (assert_warning(B.line attr,"empty");
  let line = B.line attr in
  let new_attr = B.mkAttr(line) in
  Ast.ASSERT(Ast.EMPTY(id,[info],B.BOTH,false,B.updty new_attr B.BOOL),
	     new_attr))

let assert_nonempty id info attr =
  (assert_warning(B.line attr,"nonempty");
  let line = B.line attr in
  let new_attr = B.updty (B.mkAttr(line)) B.BOOL in
  Ast.ASSERT(Ast.UNARY(Ast.NOT,Ast.EMPTY(id,[info],B.BOTH,false,new_attr),
		       new_attr),
	     new_attr))

let assert_is_src_or_tgt exp attr =
  (assert_warning(B.line attr,"is source or target");
  let new_attr = B.mkAttr(B.line attr) in
  let bool_attr = B.updty new_attr B.BOOL in
  let (decls,new_exp) = Ast.name_expr exp new_attr in
  let mk_test src_tgt = Ast.BINARY(Ast.EQ,new_exp,src_tgt,bool_attr) in
  let fld_exp field =
    let new_attr = B.mkAttr(B.line attr) in
    Ast.FIELD(Ast.VAR(!event_param,B.updty new_attr B.EVENT),B.mkId field,
	      B.updty new_attr B.PROCESS) in
  let test =
    Ast.BINARY(Ast.OR,
	       mk_test (fld_exp "source"),
	       mk_test (fld_exp "target"),
	       bool_attr) in
  Ast.SEQ(decls,[Ast.ASSERT(test,new_attr)],new_attr))

let assert_in_block exp sources attr stmtfn =
  (assert_warning(B.line attr,"in");
  let new_attr = B.mkAttr(B.line attr) in
  let (decls,new_exp) = Ast.name_expr exp new_attr in
  let tests =
    List.rev
      (List.map
	 (function CS.STATE(id,_,_,_) as x ->
	   Ast.IN(new_exp,id,[x],B.BOTH,false,B.updty new_attr B.BOOL))
	 sources) in
  let test =
    match tests with
      [] -> internal_error(B.line attr,3)
    | (last::prev) ->
	List.fold_left
	  (function prev ->
	    function test ->
	      Ast.BINARY(Ast.OR,test,prev,B.updty new_attr B.BOOL))
	  last
	  prev in
  Ast.SEQ(decls,[Ast.ASSERT(test,new_attr);stmtfn new_exp],new_attr))

let assert_not_in_block exp sources attr stmtfn =
  (assert_warning(B.line attr,"not in");
  let new_attr = B.mkAttr(B.line attr) in
  let (decls,new_exp) = Ast.name_expr exp new_attr in
  let tests =
    List.rev
      (List.map
	 (function CS.STATE(id,_,_,_) as x ->
	   Ast.IN(new_exp,id,[x],B.BOTH,false,B.updty new_attr B.BOOL))
	 sources) in
  let test =
    match tests with
      [] -> internal_error(B.line attr,4)
    | (last::prev) ->
	List.fold_left
	  (function prev ->
	    function test ->
	      Ast.BINARY(Ast.AND,test,prev,B.updty new_attr B.BOOL))
	  last
	  prev in
  let test = Ast.UNARY(Ast.NOT,test,B.updty new_attr B.BOOL) in
  Ast.SEQ(decls,[Ast.ASSERT(test,new_attr);stmtfn new_exp],new_attr))

let prefix = function
    ([],stm,attr) -> stm
  | (stms,Ast.SEQ(decls,body,_),attr) -> Ast.SEQ(decls,stms@body,attr)
  | (stms,stm,attr) -> Ast.SEQ([],stms@[stm],attr)

(* -------------------------- Expressions -------------------------- *)

exception EmptyException

let rec do_verif_exp
    (exp : Ast.expr)
    (env : state_env)
    (venv : var_env)
    (srcenv : source_env) :
    state_env * Ast.expr * Ast.stmt list =
  (match exp with
      Ast.INT(n,attr) -> (env,exp,[])

    | Ast.VAR(id,attr) ->
	(try
	  match lookup_id id (!CS.state_env) with
	    (CS.STATE(_,_,CS.PROC,_) as x) ->
	      (try check_nonempty x env exp attr
	      with EmptyException ->
		expErrId(attr,"reference to an empty state",id))
	  | _ -> expErrId(attr,"reference to a queue",id)
	with LookupErrException _ -> (env,exp,[]))

    | Ast.FIELD(exp,fld,attr) ->
	let (env,exp,checks) = do_verif_exp exp env venv srcenv in
	(env,Ast.FIELD(exp,fld,attr),checks)

    | Ast.BOOL(v,attr) -> (env,exp,[])

    | Ast.UNARY(uop,exp,attr) ->
	let (env,exp,checks) = do_verif_exp exp env venv srcenv in
	(env,Ast.UNARY(uop,exp,attr),checks)

    | Ast.BINARY(bop,exp1,exp2,attr) ->
	let (env1,exp1,l1) = do_verif_exp exp1 env venv srcenv in
	let (env2,exp2,l2) = do_verif_exp exp2 env1 venv srcenv in
	(env2,Ast.BINARY(bop,exp1,exp2,attr),l1@l2)

    | Ast.INDR(exp,attr) ->
	let (env,exp,checks) = do_verif_exp exp env venv srcenv in
	(env,Ast.INDR(exp,attr),checks)
	  
    | Ast.SELECT(Some(CS.STATE(id,_,_,_) as x),attr) ->
	(try check_nonempty x env exp attr
	with EmptyException -> stmtErrId(attr,"select from an empty state",id))

    | Ast.SELECT(None,attr) -> internal_error(B.line attr,5)

    | Ast.EMPTY(id,states,_,_,attr) -> (env,exp,[])

    | Ast.PRIM(f,exps,attr) ->
	let (env,exps,l) =
	  List.fold_left
	    (function (env,exps,l) ->
	      function exp ->
		let (env1,exp1,l1) = do_verif_exp exp env venv srcenv in
		(env1,exp1::exps,l1@l))
	    (env,[],[]) exps in
	(env,Ast.PRIM(f,List.rev exps,attr),l)

    | Ast.NEXT(exp1,nprocs,attr) ->
	let ((env1,exp,checks),(srcs,procs,envp)) =
	  verif_proc_exp exp env venv srcenv in
	(env1,exp,checks)

    | Ast.SRCONSCHED(attr) -> (env,exp,[])

    | Ast.AREF(_,_,attr) -> internal_error(B.line attr,18)

    | Ast.IN(exp,id,x,y,z,attr) ->
	let (env,exp,checks) = do_verif_exp exp env venv srcenv in
	(env,Ast.IN(exp,id,x,y,z,attr),checks)

    | Ast.MOVEFWDEXP(_,_,attr) -> internal_error(B.line attr,19))

and check_nonempty st env exp attr =
  match summarize_nonempty_states [st] env with
    ([],[]) ->  (* all empty case *)
      raise EmptyException
  | ([x],[]) -> (* nonempty case *)
      (env,exp,[])
  | ([],[CS.STATE(id,_,_,_) as x]) -> (* unknown case *)
      (explicate_unknown id env, exp, [assert_nonempty id x attr])
  | _ -> internal_error(-1,6)

and verif_exp
    (exp : Ast.expr)
    (env : state_env)
    (venv : var_env)
    (srcenv : source_env) :
    state_env * Ast.expr * Ast.stmt list =
  try do_verif_exp exp env venv srcenv
  with ExpErrException -> (env, exp, [])

(* the state_env in the left returned tuple is the state environment if the
process is not used/moved, and the state_env in the right returned tuple is
the state environment if the process is used/moved. *)
(* srcs is the set of possible current positions of the process.  The
processes' original positions can be obtained from the processes themselves *)
and verif_proc_exp
    (exp : Ast.expr)
    (env : state_env)
    (venv : var_env)
    (srcenv : source_env) :
    (state_env * Ast.expr * Ast.stmt list) *
    (CS.state_info list * VB.vproc list * state_env) =
  match exp with
    Ast.NEXT(exp,nprocs,attr) ->
	let ((env1,exp,checks),(srcs,procs,envp)) =
	  verif_proc_exp exp env venv srcenv in
	let src_tgt_check =
	  List.for_all
	    (function p -> VB.vp_member VB.SRC [p] || VB.vp_member VB.TGT [p])
	    procs in
	if src_tgt_check
	then
	  ((env1,
	       Ast.NEXT(exp,Aux.union (drop_vb_sources procs) nprocs,attr),
	       checks),
	      (srcs,procs,envp))
	else ((env1,Ast.NEXT(exp,[VB.SRC;VB.TGT],attr),
	       (assert_is_src_or_tgt exp attr) :: checks),
	      (srcs,procs,envp))
  | _ ->   
      try
      (let (env,exp,checks) as x = do_verif_exp exp env venv srcenv in
      (x,
       try
	 (match exp with
	   Ast.VAR(id,attr) ->
	     (try
	       match lookup_id id (!CS.state_env) with
		 (CS.STATE(_,_,CS.PROC,_) as x) ->
                 (* should look for the src of the process in this state,
                    not just the name of the state *)
		   let (vt,srcs,_) = lookup_id id env in
		   (match VB.known_contents vt with
		     [p] -> ([x],[p],env)
		   | _ -> internal_error(B.line attr,7))
	       | _ -> internal_error(B.line attr,8)
	     with LookupErrException _ ->
	       (try
		 let (srcs,procs) = lookup_id id venv in
		 (Aux.map_union (function p -> find_proc p env attr) procs,
		  procs,env)
	       with LookupErrException _ ->
		 expErrId(attr,"unknown process identifier",id)))
	       
	 | Ast.FIELD(exp,fld,attr) ->
	     (match Ast.is_src_or_tgt exp fld srcenv with
	       VB.VP(VB.UNKNOWNPROC,states) as x ->
		 (nonempty_states env, [x], env)
	     | p -> (find_proc p env attr, [p], env))
	     
	 | Ast.SELECT(Some(CS.STATE(id,_,_,_) as st),attr) ->
	     (match lookup_id id env with
	       (VB.MEMBER(l),srcs,modd) ->
		 ([st],[VB.VP(VB.UNKNOWNPROC,List.sort compare srcs)],
		  extend_idenv id
		    (VB.mk_min_seq srcs (List.length l), srcs, modd) env)
	     | _ -> internal_error(B.line attr,9))
	     
	 | _ -> internal_error(-1,10))
	   
       with ExpErrException ->
	 (nonempty_states env,[VB.VP(VB.UNKNOWNPROC,[])],env)))
      with ExpErrException ->
	 (* just give up if the expression gives an error *)
	raise StmtErrException

and nonempty_states env =
  let (nonempty,unknown) = summarize_nonempty_states (CS.all_states()) env in
  nonempty@unknown

(* find the state containing a process, if any *)
(* when some processes are equal (ie on the same child scheduler) they should
all be in the same place, so we just look for the first one *)
and find_proc proc env attr =
  let rec get_proc = function
      VB.VP(p,_) -> p
    | VB.EQUAL(x::xs) -> get_proc x
    | _ -> internal_error(B.line attr,17) in
  let p = get_proc proc in
  let any =
    List.filter
      (function (id,(vt,_,_)) ->
	List.exists (function x -> VB.vp_member p [x]) (VB.known_contents vt))
      env in
  match any with
    [] ->
      List.fold_left
	(function prev ->
	  function (id,(vt,_,_)) ->
	  let CS.STATE(_,_,ty,_) as x =
	    lookup_id id (!CS.state_env) in
	  if VB.contains_unknown ty vt
	  then x::prev
	  else prev)
	[] env
  | [(id,vt)] -> [lookup_id id (!CS.state_env)]
  | _ ->
      stmtErr(attr,pr "process %s in more than one state" (VB.vprocname2c p))

and drop_vb_sources l =
  List.map
    (function
	VB.VP(vp,_) -> vp
      |	VB.EQUAL(vps) -> (* keep only tgt or src if one present *)
	  let res = drop_vb_sources vps in
	  if Aux.member VB.TGT res
	  then VB.TGT
	  else
	    if Aux.member VB.SRC res
	    then VB.SRC
	    else
	      (match res with
		(x::xs) -> x
	      | _ -> internal_error(-1,19)))
    l

(* ------------------------- Declarations -------------------------- *)

(* The third result is a function that allows to put the body of the
sequence statement within the nested sequences containing the declarations
and the checks needed for them *)

let rec verif_defs defs env venv srcenv =
  List.fold_left
    (function (prev_env,venv,acc_decls,acc_stms) ->
      function 
	  Ast.VALDEF(Ast.VARDECL(ty,id,x,y),exp,isconst,attr) ->
	    let proc_sched_decl _ =
	      let ((_,exp,checks),(srcs,procs,env1)) =
		verif_proc_exp exp prev_env venv srcenv in
	      let d = Ast.VALDEF(Ast.VARDECL(ty,id,x,y),exp,isconst,attr) in
	      let (acc_decls,acc_stms) =
		build_accs d acc_decls acc_stms attr checks in
	      (env1,extend_idenv id (srcs,procs) venv,acc_decls,acc_stms) in
	    (match ty with
	      B.PROCESS -> proc_sched_decl ()
	    | B.SCHEDULER -> proc_sched_decl ()
	    | ty ->
		let (new_env,exp,checks) =
		  verif_exp exp prev_env venv srcenv in
		let d = Ast.VALDEF(Ast.VARDECL(ty,id,x,y),exp,isconst,attr) in
		let (acc_decls,acc_stms) =
		  build_accs d acc_decls acc_stms attr checks in
		(new_env,venv,acc_decls,acc_stms))
	| _ -> internal_error(-1,11))
    (env,venv,[],function stm -> stm) defs

and build_accs decl acc_decls acc_stms attr = function
    [] -> (decl::acc_decls,acc_stms)
  | checks ->
      ([decl],
       function stm ->
	 acc_stms (Ast.SEQ(List.rev acc_decls, checks @ [stm], attr)))

(* -------------------------- Statements --------------------------- *)

(* used by both IF-IN statements and SWITCH statements *)
type in_info = THEN_ELSE | ELSE_ONLY | NO_ELSE

let handler_outputs = ref ([] : state_env list)

(* srcs can be less precise than env_proc, if the tested process is a variable
because venv is less precise than env.  perhaps venv should become just
as precise as env.  the effect is that one can think that the process
is in multiple states even if one knows exactly what the process is and
that process appears in a specific place in the env *)
let rec verif_in (srcs,procs,env_proc) states env_assert venv srcenv stm attr :
    ((state_env list * var_env) option * Ast.stmt) * in_info =
  let (nonempty,unknown) = summarize_nonempty_states srcs env_assert in
  let possible_srcs = nonempty@unknown in
  if Aux.subset possible_srcs states
  then (verif_stmt stm env_assert venv srcenv, NO_ELSE)
  else match Aux.intersect possible_srcs states with
    [] ->
      ((Some([env_assert], venv), stm), ELSE_ONLY)
  | [dst] ->
      (verif_stmt stm (dstadd dst procs env_proc VB.UNMODIFIED) venv srcenv,
       THEN_ELSE)
  | _ ->
      (verif_stmt stm env_assert venv srcenv, THEN_ELSE)

and combine_two_envs env1_venv1 env2_venv2 model attr =
  match (env1_venv1,env2_venv2) with
    (None,None) -> None
  | ((Some(envb,venvb)) as x,None) -> x
  | (None,(Some(envc,venvc) as x)) -> x
  | (Some(envb,venvb),Some(envc,venvc)) ->
      Some (Aux.union envb envc, lub_venv venvb venvc model attr "one")
    
	
and verif_stmt
    (stmt : Ast.stmt)
    (env : state_env)
    (venv : var_env)
    (srcenv : source_env)
    : (state_env list * var_env) option * Ast.stmt =
  try
    (match stmt with
      Ast.IF(exp,stmt1,stmt2,attr) ->
	(match exp with
	  Ast.EMPTY(id,states,vl,insrc,eattr) ->
	    (match summarize_nonempty_states states env with
	      ([],[]) -> (* all empty *)
		let (envb_venvb,stmt1a) = verif_stmt stmt1 env venv srcenv
		in (envb_venvb,
		    Ast.IF(Ast.EMPTY(id,states,B.lubtestval(B.TRUE,vl),
				     insrc,eattr),
			   stmt1a,stmt2,attr))
	    | (x::nonempty,_) -> (* at least one nonempty *)
		let (envb_venvb,stmt2a) = verif_stmt stmt2 env venv srcenv
		in (envb_venvb,
		    Ast.IF(Ast.EMPTY(id,states,B.lubtestval(B.FALSE,vl),
				     insrc,eattr),
			   stmt1,stmt2a,attr))
	    | ([],[CS.STATE(id0,_,_,_)]) ->
		let (envb_venvb,stmt1a) =
		  let (_,_,modd) = lookup_id id0 env in
		  verif_stmt stmt1
		    (extend_idenv id0 (VB.EMPTY,[],modd) env)
		    venv
		    srcenv in
		let (envc_venvc,stmt2a) =
		  verif_stmt stmt2 (explicate_unknown id0 env)
		    venv srcenv in
		let res_stm =
		  Ast.IF(Ast.EMPTY(id,states,B.BOTH,insrc,eattr),
			 stmt1a,stmt2a,attr) in
		(combine_two_envs envb_venvb envc_venvc venv attr, res_stm)
	    | ([],unknown) ->
		let (envb_venvb,stmt1a) =
		  verif_stmt stmt1 (extend_all unknown (VB.EMPTY,[]) env) venv
		    srcenv in
		    (* consider that each unknown state contains something *)
		let (envc_venvc,stmt2a) =
		  (List.fold_left
		     (function (envc_venvc,stmt2) ->
		       function CS.STATE(id0,_,_,_) ->
			 let (env1_venv1,stmt2a) =
			   verif_stmt stmt2 (explicate_unknown id0 env)
			     venv srcenv in
			 (combine_two_envs envc_venvc env1_venv1 venv attr,
			  stmt2a))
		     (Some([], mk_empty_venv venv),stmt2) unknown) in
		let res_stm = Ast.IF(Ast.EMPTY(id,states,B.BOTH,insrc,eattr),
				     stmt1a,stmt2a,attr) in
		(combine_two_envs envb_venvb envc_venvc venv attr, res_stm))
	| Ast.IN(exp1,id,states,vl,insrc,in_attr) ->
	    let ((enva,exp1,checks),proc_info) =
	      verif_proc_exp exp1 env venv srcenv in
	    (match verif_in proc_info states enva venv srcenv stmt1 in_attr
	    with
	      ((env1a_venv1a,stmt1a),NO_ELSE) ->
		(env1a_venv1a,
		 prefix(checks,
			Ast.IF(Ast.IN(exp1,id,states,B.lubtestval(B.TRUE,vl),
				      insrc,in_attr),
			       stmt1a,stmt2,attr),
			attr))
	    | (_,ELSE_ONLY) ->
		let (env2a_venv2a,stmt2a) =
		  verif_stmt stmt2 enva venv srcenv in
		(env2a_venv2a,
		 prefix(checks,
			Ast.IF(Ast.IN(exp1,id,states,B.lubtestval(B.FALSE,vl),
				      insrc,in_attr),
			       stmt1,stmt2a,attr),
			attr))
	    | ((env1a_venv1a,stmt1a),branch) ->
		let (env2a_venv2a,stmt2a) =
		  verif_stmt stmt2 enva venv srcenv in
		(combine_two_envs env1a_venv1a env2a_venv2a venv attr,
		 prefix(checks,
			Ast.IF(Ast.IN(exp1,id,states,B.BOTH,
				      insrc,in_attr),
			       stmt1a,stmt2a,attr),
			attr)))
	| Ast.SRCONSCHED(attr)
	    when
	    (match find_proc (VB.VP(VB.SRC,[])) env attr with
	      [CS.STATE(_,CS.NOWHERE,_,_)] -> true
	    | _ -> false) ->
		let (enva, exp, checks) = verif_exp exp env venv srcenv in
		let (envc_venvc,stmt2a) = verif_stmt stmt2 enva venv srcenv in
		(envc_venvc,prefix(checks,Ast.IF(exp,stmt1,stmt2a,attr),attr))
	| _ ->
	    let (enva, exp, checks) = verif_exp exp env venv srcenv in
	    let (envb_venvb,stmt1a) = verif_stmt stmt1 enva venv srcenv in
	    let (envc_venvc,stmt2a) = verif_stmt stmt2 enva venv srcenv in
	    (combine_two_envs envb_venvb envc_venvc venv attr,
	     prefix(checks,Ast.IF(exp,stmt1a,stmt2a,attr),attr)))

    | Ast.FOR(id,stid,states,dir,stmt,attr) ->
	let venv0 = extend_idenv id ([],[]) venv in
	let (new_env,undo_for_env) = make_new_for_env env states in
	let (env_venv1,new_stmt) =
	  fixpoint id states stmt new_env venv0 srcenv true attr in
	(match env_venv1 with
	  None -> (None,Ast.FOR(id,stid,states,dir,new_stmt,attr))
	| Some (env,venv1) ->
	    (Some(List.map undo_for_env env,restrict venv1 venv attr "xxx"),
	     Ast.FOR(id,stid,states,dir,new_stmt,attr)))
	  
    | Ast.SWITCH(exp,cases,default,attr) ->
	let ((enva,exp,checks),proc_info) =
	  verif_proc_exp exp env venv srcenv in
	let (envs_new_venv, new_cases) =
	  List.fold_left
	    (function (prev_env_prev_venv,prev_cases) ->
	      function Ast.SEQ_CASE(pat,states,stm,attr) ->
		match verif_in proc_info states enva venv srcenv stm attr with
		  (_,ELSE_ONLY) ->
		    (prev_env_prev_venv,
		     Ast.SEQ_CASE(pat,states,stm,attr) :: prev_cases)
		| ((Some(env1,venv1) as x,stm1),_) ->
		    (combine_two_envs prev_env_prev_venv x venv attr,
		     Ast.SEQ_CASE(pat,states,stm1,attr) :: prev_cases)
		| ((None,stm1),_) ->
		    (prev_env_prev_venv,
		     Ast.SEQ_CASE(pat,states,stm1,attr) :: prev_cases))
	    (None,[]) cases in
	(match default with
	  None -> 
	    (envs_new_venv,
	     prefix(checks,Ast.SWITCH(exp,List.rev new_cases,None,attr),attr))
	| Some x ->
	    let (def_envs_def_new_venv, new_def) =
	      verif_stmt x env venv srcenv in
	    (combine_two_envs def_envs_def_new_venv envs_new_venv venv attr,
	     prefix(checks,
		    Ast.SWITCH(exp,List.rev new_cases,Some new_def,attr),
		    attr)))
	  
    | Ast.SEQ(decls,stms,attr) ->
	let (env1,venv1,decls1,fn) = verif_defs decls env venv srcenv in
	let (envs1_venv2,stms1) =
	  List.fold_left
	    (function
		(None,prev) -> (function stm -> (None,prev))
	      | (Some(envs,venv),prev) ->
		  function stm ->
		(* from here to the ***'s implements a sequence with no
		   lubbing of intermediate state environments.  for our
		   examples, this doesn't seem too expensive. *)
		    let (envs3_venv3,stm1) =
		      List.fold_left
			(function (envs_venv,stm) ->
			  function env ->
			    let (envs3_venv3,stm1) =
			      verif_stmt stm env venv srcenv in
			    (combine_two_envs envs_venv envs3_venv3 venv attr,
			     stm1))
			(Some([],venv),stm) envs in
		    (envs3_venv3,stm1::prev)
		  (* *** *)
		(*
		let (envs3,venv3,stm1) =
		  verif_stmt stm (lub_all envs) venv srcenv in
		(envs3,venv3,stm1::prev)
		   *))
	    (Some([env1],venv1),[]) stms in
	let new_stm = fn(Ast.SEQ(List.rev decls1, List.rev stms1, attr)) in
	(match envs1_venv2 with
	  None -> (None,new_stm)
	| Some(envs1,venv2) ->
	    (Some(envs1,restrict venv2 venv attr "yyy"),new_stm))
	  
    | Ast.RETURN(Some(exp),attr) ->
	(* not sure there is anything to verify here *)
	let (env1,exp,checks) = verif_exp exp env venv srcenv in
	handler_outputs := Aux.union [env1] (!handler_outputs);
	(None,prefix(checks,Ast.RETURN(Some(exp),attr),attr))

    | Ast.RETURN(None,attr) ->
	handler_outputs := Aux.union [env] (!handler_outputs);
	(None,Ast.RETURN(None,attr))

    | Ast.MOVE(exp,state,tcsrc,verifsrcs,
	       Some(CS.STATE(dstid,dstcls,dsttype,_) as dst),auto_allowed,
	       state_end,attr)

	(* for virtual schedulers, should only allow a move within classes
	   or from running to ready *)
      ->
	let ((_,exp,checks),(srcs,procs,envp)) =
	  verif_proc_exp exp env venv srcenv in
	(* check whether destination is free *)
	let (env2,dst_add_stms) =
	  (match dsttype with
	    CS.PROC ->
	      (match summarize_nonempty_states [dst] envp with
		([],[]) -> (envp,[])
	      |	([],[x]) ->
		  (extend_idenv dstid (VB.EMPTY,[],VB.MODIFIED) envp,
		   [assert_empty dstid dst attr])
	      | _ -> stmtErrId(attr,"move to non-empty state",dstid))
	  | CS.QUEUE(_) -> (envp,[])) in
	let (new_srcs,src_add_stms,(env3,venv3)) =
	  (match tcsrc with
	     (* verified in type checker *)
	    Some(CS.STATE(id,_,_,_) as src) ->
	      ([src],
	       Ast.MOVE(exp,state,Some(src),[src],Some(dst),auto_allowed,
			state_end,attr),
	       move procs src dst env2 venv)
	  | None ->
	      let new_srcs = Aux.union srcs verifsrcs in
	      (new_srcs,
	       Ast.MOVE(exp,state,None,new_srcs,
			Some(dst),auto_allowed,state_end,attr),
	       moves procs srcs dst env2 venv attr))
	in
	if Aux.member dst new_srcs
	then
	  if auto_allowed (* always the case for => from src pgm *)
	  then
	    (match dsttype with
	      CS.QUEUE(CS.SORTED(_)) -> ()
	    | _ ->
		warning (B.line attr,
			 pr "possible move from %s to itself (may be ignored)"
			   (B.id2c dstid)))
	  else warning (B.line attr,
			pr "possible move from %s to itself (may be ignored)"
			  (B.id2c dstid));
	(match (!handler_name,!B.schedtype) with
	  (_,B.VIRTSCHED) ->
	    let src_classes =
	      Aux.make_set
		(List.map (function CS.STATE(_,cls,_,_) -> cls) new_srcs) in
	    let CS.STATE(_,dst_cls,_,_) = dst in
	    let changed_classes =
	      List.filter
		(function src_cls -> not (src_cls = dst_cls))
		src_classes in
	    (match (!handler_name,changed_classes,dst_cls) with
	      (_,[],_) -> ()
	    | (_,[CS.RUNNING],CS.READY) -> ();
	    | ("detach: ",_,CS.TERMINATED) ->
		(* detach is only called when the scheduler has been verified
		   to contain no processes *)
		();
	    | _ -> error(B.line attr,
			 pr "cannot move from classes %s to class %s"
			   (Aux.set2c(List.map CS.class2c changed_classes))
			   (CS.class2c dst_cls)))
	| (_,B.PROCSCHED) -> ());
	(Some([env3],venv3),prefix(checks@dst_add_stms,src_add_stms,attr))

    | Ast.MOVE(exp,state,_,_,None,_,_,attr) -> internal_error(B.line attr,12)
	       
    | Ast.MOVEFWD(exp,verifsrcsdsts,state_end,attr) ->
	verif_move_fwd exp verifsrcsdsts attr env venv srcenv
	  (function (exp,verifsrcsdsts,attr) ->
	    Ast.MOVEFWD(exp,verifsrcsdsts,state_end,attr))

    | Ast.SAFEMOVEFWD(exp,verifsrcsdsts,stmt,state_end,attr) ->
	let (enva_venva,stma) = verif_stmt stmt env venv srcenv in
	let (envb_venvb,stm) =
	  verif_move_fwd exp verifsrcsdsts attr env venv srcenv
	    (function (exp,verifsrcsdsts,attr) ->
	      Ast.SAFEMOVEFWD(exp,verifsrcsdsts,stma,state_end,attr)) in
	(combine_two_envs envb_venvb enva_venva venv attr,stm)

    | Ast.ASSIGN(loc,exp,sorted_fld,attr) ->
	(* not sure there is anything to verify here *)
	let (env0,loc,checks0,sorted_exp) = verif_loc env venv srcenv loc in
	let modif_venv _ =
	  (match loc with
	      Ast.VAR(id,attr) ->
		let ((_,exp,checks),(srcs,procs,env1)) =
		  verif_proc_exp exp env0 venv srcenv in
		(Some([env1],extend_idenv id (srcs,procs) venv),
		 prefix(checks0@checks,
			Ast.ASSIGN(loc,exp,sorted_exp||sorted_fld,attr),
			attr))
	    | _ -> stmtErr(attr,"only assignments to process-typed variables"
			   ^" supported")) in
	(match B.ty attr with
	  B.PROCESS -> modif_venv ()
	| B.SCHEDULER -> modif_venv ()
	| _ ->
	    let (env1,exp,checks) = verif_exp exp env0 venv srcenv in
	    (Some([env1],venv),
	     prefix(checks0@checks,
		    Ast.ASSIGN(loc,exp,sorted_exp||sorted_fld,attr),
		    attr)))

    | Ast.DEFER(attr) -> (Some([env],venv),stmt)

    | Ast.BREAK(attr) -> (Some([env],venv),stmt)(* not a good implementation *)

    | Ast.PRIMSTMT(f,args,attr) ->
	let (env1,checks1) =
	  List.fold_left
	    (function (pre_env,pre_checks) ->
	      function exp ->
		let (res_env,exp,res_checks) =
		  verif_exp exp pre_env venv srcenv in
		(res_env,pre_checks@res_checks))
	    (env,[]) args in
	(Some([env1],venv),prefix(checks1,Ast.PRIMSTMT(f,args,attr),attr))
    | Ast.ERROR(str,attr) -> (None,stmt)
    | Ast.ASSERT(expr,attr) ->
	try (Some([interp_assert env venv srcenv attr expr],venv),stmt)
	with AssertErrException str -> 
	  stmtErr(attr, pr "current state incompatible with %s assertion" str))
  with StmtErrException -> (Some([env],venv),stmt)

and verif_move_fwd exp verifsrcsdsts attr env venv srcenv rebuild =
  let ((_,exp,exp_checks),(srcs,procs,envp)) =
    verif_proc_exp exp env venv srcenv in
	(* exp must be source or target *)
  let checks =
    if not (!src_tgt_handler) ||
    List.for_all
      (function p ->
	VB.vp_member VB.SRC [p] || VB.vp_member VB.TGT [p])
      procs
    then exp_checks
    else (assert_is_src_or_tgt exp attr) :: exp_checks in
  let dsts = find_target_states srcs in
	(* problem: we can move to more than one state *)
  if dsts = [] then stmtErr(attr,"no destinations");
  let (envs,venv,stm,_) =
    List.fold_left
      (function (acc_env,acc_venv,acc_stms,start) ->
	function (CS.STATE(dstid,dstcls,dsttype,_)) as dst ->
		(* do nothing if the process is already known to be in the
		   destination state.  but this will not always work when
		   the source is a variable, and srcs does not accurately
		   reflect what is known about the position of the process *)
	  if [dst] = srcs
	  then (Aux.union [envp] acc_env,
		(if start
		then venv
		else lub_venv venv acc_venv venv attr "eight"),
		acc_stms,start)
	  else
	    let (env2,dst_add_stms) =
	      (match dsttype with
		CS.PROC ->
		  (match summarize_nonempty_states [dst] envp with
		    ([],[]) -> (envp,[])
		  | ([],[x]) ->
		      (extend_idenv dstid (VB.EMPTY,[],VB.MODIFIED) envp,
		       [assert_empty dstid dst attr])
		  | _ -> stmtErrId(attr,"move to non-empty state",dstid))
	      | CS.QUEUE(_) -> (envp,[])) in
	    let (env3,venv3) =
	      moves procs srcs dst env2 venv attr in
	    let new_venv =
	      if start
	      then venv3
	      else lub_venv venv3 acc_venv venv attr "nine" in
	    (Aux.union [env3] acc_env, new_venv,
	     prefix(dst_add_stms,acc_stms,attr), false))
      ([],[],
       rebuild(exp,
	       Aux.union
		 (Aux.flat_map
		    (function s -> List.map (function d -> (s,d)) dsts)
		    srcs)
		 verifsrcsdsts,
	       attr),
       true)
      dsts in
  (Some(envs,venv),prefix(checks,stm,attr))
    
    
and verif_loc env venv srcenv = function
    Ast.VAR(id,attr) as x -> (env,x,[],false)
  | Ast.FIELD(exp,fld,attr) ->
      let verif_proc_sched _ =
	let ((env1,exp,checks),(srcs,procs,_)) =
	    verif_proc_exp exp env venv srcenv in
	  (env1,Ast.FIELD(exp,fld,attr),checks,any_sorted srcs) in
      (match B.ty(Ast.get_exp_attr exp) with
	B.PROCESS -> verif_proc_sched ()
      |	B.SRCPROCESS -> verif_proc_sched ()
      |	B.SCHEDULER -> verif_proc_sched ()
      |	_ -> 
	  let (env1,exp,checks) = verif_exp exp env venv srcenv in
	  (env1,Ast.FIELD(exp,fld,attr),checks,false))
  | _ -> raise (Error.Error("verif_loc: internal error"))

and any_sorted states =
  List.exists
    (function
	CS.STATE(_,_,CS.QUEUE(CS.SORTED(_)),_) -> true
      |	_ -> false)
    states

(* assert for previous possible input configurations has to be compatible
with the current state.  If this cannot be shown for some desired
protocols, then the language of assertions will have to be enriched *)

and interp_assert env venv srcenv attr = function
    Ast.EMPTY(_,states,_,_,_) ->
      (match summarize_nonempty_states states env with
	(x::nonempty,_) -> raise (AssertErrException "empty")
      |	(_,unknown) -> extend_all unknown (VB.EMPTY,[]) env)
  | Ast.UNARY(Ast.NOT,Ast.EMPTY(_,states,_,_,_),_) ->
      (match summarize_nonempty_states states env with
	([],[]) -> raise (AssertErrException "non-empty")
      |	(x::nonempty,_) -> env
      |	(_,[CS.STATE(id,clsname,state_type,_)]) ->
	  explicate_unknown id env
      |	(_,unknown) -> env)
  | Ast.IN(exp,_,states,_,_,_) ->
      let ((_,exp,_), (srcs,procs,_)) = verif_proc_exp exp env venv srcenv in
      if Aux.subset srcs states
      then
	(match summarize_nonempty_states states env with
	  ([],[]) -> raise (AssertErrException "in")
	| (x::nonempty,_) -> env
	| (_,[state]) -> dstadd state procs env VB.UNMODIFIED
	| (_,unknown) -> env)
      else raise (AssertErrException "in")
  | Ast.UNARY(Ast.NOT,exp,_) ->
      (try let _ = interp_assert env venv srcenv attr exp in
	raise (AssertErrException "not")
      with (AssertErrException _) -> env)
  | Ast.BINARY(Ast.OR,test,prev,attr) ->
      (try interp_assert env venv srcenv attr test
      with AssertErrException _ -> interp_assert env venv srcenv attr prev)
  | Ast.BINARY(Ast.AND,test,prev,attr) ->
      (let env = interp_assert env venv srcenv attr test in
       interp_assert env venv srcenv attr prev)
  | Ast.BINARY(Ast.EQ,_,_,_) ->
      Printf.printf "warning: not sure what to do for an = assert\n";
      env
  | _ -> internal_error(B.line attr,13)

(* ---------------------------- Foreach ---------------------------- *)

and make_new_for_env env states =
  let unknownid _ =
    let ctr = !for_ctr in
    for_ctr := ctr + 1;
    VB.S(ctr,"for") in
  let (new_env,new_procs) =
    List.fold_left
      (function (new_env,new_procs) ->
	function CS.STATE(state_id,_,state_ty,_) as dst ->
	  let (vt,srcs,modd) = lookup_id state_id env in
	  match vt with
	    VB.EMPTY -> (new_env,new_procs)
	  | VB.UNKNOWN ->
	      let s = unknownid() in
	      (dstadd dst [VB.VP(s,srcs)] env VB.UNMODIFIED,s::new_procs)
	  | VB.MEMBER(l) ->
	      let no_unknown =
		List.filter
		  (function
		      VB.VP(VB.UNKNOWNPROC,_) -> false
		    | _ -> true)
		  l in
	      let s = unknownid() in
	      (extend_idenv state_id
		 (VB.MEMBER(VB.VP(s,srcs)::no_unknown),srcs,modd)
		 env,
	       s::new_procs)
	  | _ -> internal_error(-1,25))
      (env,[]) states in
  let unknownify env =
    List.map
      (function (id,(vt,srcs,modd)) as x ->
	match vt with
	  VB.EMPTY -> x
	| VB.UNKNOWN -> x
	| VB.MEMBER(l) ->
	    let (tochange, unchanged) =
	      List.partition
		(function
		    VB.VP(nm,_) -> Aux.member nm new_procs
		  | VB.EQUAL(_) -> internal_error(-1,26))
		l in
	    (match (tochange,unchanged) with
	      ([],_) -> x
	    | (VB.VP(_,srcs)::_,[]) ->
		(id,(VB.MEMBER([VB.VP(VB.UNKNOWNPROC,srcs)]),srcs,modd))
	    | (_,l1) -> (id,(VB.MEMBER(l1),srcs,modd)))
	| _ -> internal_error(-1,27))
      env in
  (new_env,unknownify)

and one_iter id states stmt env venv srcenv attr =
  List.fold_left
    (function (prev_envs_prev_venv,stmt,at_least_once) ->
      function CS.STATE(state_id,_,state_ty,_) as dst ->
	let (vt,srcs,modd) = lookup_id state_id env in
	let srcs = List.sort compare srcs in
	let env = extend_idenv state_id (vt,srcs,VB.UNMODIFIED) env in
	let contents = VB.known_contents vt in
	let at_least_once = at_least_once || not(contents = []) in
        (* run once on the known contents *)
	let (envs_known_venv_known,stmt_known) =
	  List.fold_left
	    (function (envs_venvs,stmt) ->
	      function vp ->
		let (envs1_venv1,stmt1) =
		  verif_stmt stmt env (extend_idenv id ([dst],[vp]) venv) 
		    srcenv in
		(combine_two_envs envs_venvs envs1_venv1 venv attr, stmt1))
	    (Some([],List.map (function (id,_) -> (id,([],[]))) venv),stmt)
	    contents in
	let envs_known_venv_known =
	  (match envs_known_venv_known with
	    Some (envs_known,venv_known) ->
	      Some(check_restore_modd envs_known state_id modd attr,venv_known)
	  | None -> None) in
	(combine_two_envs envs_known_venv_known prev_envs_prev_venv venv attr,
	 stmt_known, at_least_once))
    (Some([],List.map (function (id,_) -> (id,([],[]))) venv),stmt,false)
      states

(* State should be UNMODIFIED in the loop body.  Restore to the old value *)
and check_restore_modd envs state_id modd attr =
  List.map
    (function env ->
      match lookup_id state_id env with
	(_,_,VB.MODIFIED) ->
	  stmtErr(attr,
		  pr "processes should not be added to state %s in loop body"
		    (B.id2c state_id))
      |	(vt,srcs,_) -> extend_idenv state_id (vt,srcs,modd) env)
    envs

and fixpoint id states stmt env venv srcenv first attr =
  let (new_envs_new_venv,new_stmt,at_least_once) =
    one_iter id states stmt env venv srcenv attr in
  match new_envs_new_venv with
    None -> (None,new_stmt)
  | Some(new_envs,new_venv) ->
      let new_env =
	(if (first && at_least_once)
	then (lub_all new_envs)
	else (lub_all (env::new_envs))) in
      if compatible_env env new_env
	  && (List.for_all2
		(function (old_id,(old_srcs,old_procs)) ->
		  function (new_id,(new_srcs,new_procs)) ->
		    if B.ideq(old_id,new_id)
		    then
		      Aux.subset new_srcs old_srcs &&
		      Aux.subset new_procs old_procs
		    else stmtErrNoline("venv out of order"))
	    venv new_venv)
      then (Some([env],venv),new_stmt)
      else
	fixpoint id states new_stmt
	  new_env
	  (if (first && at_least_once)
	  then (lub_venv (mk_empty_venv venv) new_venv venv attr "fourteen")
	  else (lub_venv venv new_venv venv attr "fifteen"))
	  srcenv false attr

(* ------------------- Initial sources environment ----------------- *)

let collect_nonempty env =
  Aux.option_filter
    (function
	(_,(VB.EMPTY,_,_)) -> None
      | (id,_) -> Some (lookup_id id (!CS.state_env)))
    env


let collect_special env =
  List.fold_left
    (function prev ->
      function (id,(vp,_,_)) ->
	let info = [lookup_id id (!CS.state_env)] in
	(List.map (function x -> (x,info))
	   (Aux.map_union VB.collect_ps (VB.known_contents vp)))
	@ prev)
    [] env

(* ---------------------- Interface functions ---------------------- *)

(* if the type is the generic interface function type, it contains tgt
rather than the argument name.  *)
let instantiate_env env = function
  [(id,argproc)] ->
    List.map
      (function
	  (id,(VB.MEMBER(l),srcs,modd)) ->
	    let new_l = List.map (VB.vp_replace VB.TGT argproc) l in
	    (id,(VB.MEMBER(new_l),srcs,modd))
	| x -> x)
      env
  | _ -> internal_error(-1,27)

let rec verif_one_ifunction nm params stmt attr inputs outputs imemotable
   generic =
  let (proc_args,_) =
    List.fold_left
      (function (proc_args,pos) ->
	function Ast.VARDECL(ty,id,imported,attr) ->
	  if ty = (B.sched_select B.PROCESS B.SCHEDULER)
	  then ((id,VB.ARG(pos))::proc_args,pos+1)
	  else (proc_args,pos+1))
      ([],1) params in
  if (List.length proc_args > 1)
  then
    (if generic
    then
      error (B.line attr,
	     "untyped interface functions with multiple process "^
	     "arguments not supported")
    else multi_arg_warning nm);
  let res =
  List.fold_left
    (function (prev_stmt,res_envs) ->
      function (env,f) ->
	let (env,outputs) =
	  if generic
	  then (instantiate_env env proc_args,
		match outputs with
		  Some outputs ->
		    Some
		      (List.map (function env -> instantiate_env env proc_args)
			 outputs)
		| None -> None)
	  else (env,outputs) in
	try
	  (prev_stmt, Aux.union (Hashtbl.find imemotable (nm,env)) res_envs)
	with Not_found ->
	  (let venv =
	    List.map
	      (function (id,argproc) ->
		    let st = find_arg argproc env attr in
		    let vl = VB.VP(argproc,[st]) in
		    (id,([st],[vl])))
	      proc_args in
	  handler_outputs := [];
	  let (_, new_stmt) =
	    verif_stmt prev_stmt env venv
	      (collect_special env, collect_nonempty env) in
	  let new_envs = !handler_outputs in
	  (*
	     drop the test, because it can occur when the interface
	     function detects an error
	  if new_envs = []
	  then raise (Error.Error (pr "1 no result: %s" (B.id2c nm)));
	   *)
	  Hashtbl.add imemotable (nm,env) new_envs;
	  (match outputs with
	    None -> (new_stmt,new_envs@res_envs)
	  | Some outputs ->
	      if check_compat env outputs (B.id2c nm) attr new_envs
	      then (new_stmt,new_envs@res_envs)
	      else (*should be changed back to prev_stmt*)
		(new_stmt,new_envs@res_envs))))
    (stmt,[]) inputs in
  res
    
and multi_arg_warning nm =
  Printf.printf "warning: interface function %s has multiple\n" (B.id2c nm);
  Printf.printf "process/scheduler typed parameters.\nThey are";
  Printf.printf "assumed to be disjoint.\n"

(* find the state containing a process, if any *)
and find_arg p env attr =
  let any =
    List.filter
      (function (id,(vt,_,_)) ->
	List.exists
	  (function x -> VB.vp_member p [x])
	  (VB.known_contents vt))
      env in
  match any with
    [] -> stmtErr(attr,pr "process argument not found in\n%s" (config2c env))
  | [(id,vt)] -> lookup_id id (!CS.state_env)
  | _ -> stmtErr(attr,pr "process %s in more than one state"
		   (VB.vp2c(VB.VP(p,[]))))

(* distinguish between those that do not perform a move operation, which
are ignored, those for which a type is provided, and those for which the
default interface function type should be used *)
let rec classify_ifunctions ifunctions =
  let env = !Events.interface_env in
  let (ignore,type_provided,no_type_provided) =
    List.fold_left
      (function (ignore,type_provided,no_type_provided) ->
	function Ast.FUNDEF(ret,nm,params,stmt,inl,attr) as x ->
	  let snm = B.id2c nm in
	  if move_possible stmt
	  then
	    try
	      let _ = lookup_id nm (!Events.interface_env) in
	      (ignore,x::type_provided,no_type_provided)
	    with
	      LookupErrException _ ->
		(ignore,type_provided,x::no_type_provided)
	  else (x::ignore,type_provided,no_type_provided))
      ([],[],[]) ifunctions in
  (List.rev ignore,List.rev type_provided,List.rev no_type_provided)

and move_possible = function
    Ast.IF(exp,stm1,stm2,attr) -> move_possible stm1 || move_possible stm2
  | Ast.SWITCH(exp,cases,default,attr) ->
      (List.exists
	(function Ast.SEQ_CASE(pat,states,stm1,attr) -> move_possible stm1)
	cases)
	or
      (match default with
	None -> false
      |	Some x -> move_possible x)
  | Ast.SEQ(decls,stms,attr) -> List.exists move_possible stms
  | Ast.FOR(index,state,state_info,dir,stm1,attr) -> move_possible stm1
  | Ast.MOVE(exp,state,srcs,verifsrcs,dst,auto_allowed,state_end,attr) -> true
  | stm -> false

(* --------------------------- Handlers ---------------------------- *)

(* Idea:
Start with nowhere as unknown and everything else empty.
Find all of the event types that apply.
Analyze the handlers according to the current state, with unknowns
instantiated according to the event type.
Normalize the outputs, such that if a state contains anything, only
one process is mentioned; use "S" as the vproc, to facilitate comparison.
Add new outputs to the worklist.
Pick a new configuration from the worklist, if any, and go back to
step 2. *)

(* in config and concretized inputs, the states should be mentioned in the
same order *)

(* compatibility:

If the input type is unknown, then it matches anything in the current
configuration.  In this case, we analyze with respect to the current
configuration.

If the input type is empty, then it matches unknown or empty in the current
configuration.  In this case, we analyze with respect to empty.

If the input type is member, then it matches unknown or member in the
current configuration.  In this case, we analyze with respect to the input
type.
*)

let match_config_to_type config concretized_inputs =
  Aux.option_filter
    (function (input,fwds) ->
      let matching_input =
	Aux.option_for_all2
	  (function (cfg_id,(cfg_vt,cfg_srcs,cfg_modd)) ->
	    function (input_id,(input_vt,input_srcs,input_modd)) as x ->
	      if not(cfg_id = input_id)
	      then internal_error(-1,14);
	      match (input_vt,cfg_vt) with
		(VB.UNKNOWN,_) ->
		  Some (input_id,(cfg_vt,input_srcs,input_modd))
	      | (VB.EMPTY,VB.UNKNOWN) -> Some x
	      | (VB.EMPTY,VB.EMPTY) -> Some x
	      | (VB.MEMBER(cfg_l),VB.UNKNOWN) -> Some x
	      | (VB.MEMBER(cfg_l),VB.MEMBER(input_l)) -> Some x
	      | _ -> None)
	  config input in
      match matching_input with
	Some x -> Some (x,fwds)
      |	None -> None)
    concretized_inputs

let normalize config =
  order_state_env
    (List.map
       (function (id,(vt,srcs,modd)) as x ->
	 match (get_class id,vt) with
	   (* no rule can read from terminated *)
	   (CS.TERMINATED,_) -> (id,(VB.EMPTY,srcs,modd))
      	 | (_,VB.MEMBER(_)) ->
              (id,(VB.MEMBER([VB.VP(VB.UNKNOWNPROC,[])]),srcs,modd))
	 | _ -> x)
       config)

let normalize_all types =
   Aux.flat_map
     (function (nm,types) ->
       List.map
         (function (Events.EVENT_TYPE(inputs,outputs,class_outputs)) ->
           (nm,
	    List.map (function (i,f) -> (order_state_env i, f)) inputs,
            List.map order_state_env outputs,
	    class_outputs))
	 types)
    types

(* Normally, we analyze the handler with respect to the current state.
   For bossa.schedule, however, it could be that the current state does
   not match a valid input.  We can just stop then, because some other
   possible input will be the same as if we were to pick up with the
   bossa.schedule event later *)

let is_empty_unk env id =
  match lookup_id id env with
    (VB.EMPTY,_,_) -> true
  | (VB.UNKNOWN,_,_) -> true
  | _ -> false

let verif_handler (Ast.EVENT(name,specd,param,stmt,attr))
    env outputs class_outputs =
  (match param with
    Ast.VARDECL(B.EVENT,id,imported,attr) -> event_param := id
  | _ -> error (B.line attr, "incorrect event argument list"));
  possible_outputs := outputs;
  handler_outputs := [];
  let (_, new_stmt) =
    verif_stmt stmt env [] (collect_special env, collect_nonempty env) in
  let new_envs = !handler_outputs in
  if new_envs = []
  then raise (Error.Error (pr "2 no result %s" (Ast.event_name2c name)));
  let _ = check_compat env outputs (Ast.event_name2c name) attr new_envs in
  Some(new_envs,Ast.EVENT(name,specd,param,new_stmt,attr))

let rec replace_handler prev (Ast.EVENT(nm,specd,_,_,_) as new_handler) =
  function
    [] -> [new_handler]
  | (Ast.EVENT(name,specd,param,stmt,attr) as x)::rest ->
      if Ast.event_name_equal nm name && Ast.event_names_equal prev specd
      then new_handler :: rest
      else x :: (replace_handler prev new_handler rest)

let rec replace_function (Ast.FUNDEF(_,nm,_,_,_,_) as new_function) = function
    [] -> internal_error(-1,16)
  | (Ast.FUNDEF(ret,name,params,stmt,inl,attr) as x)::rest ->
      if nm = name
      then new_function :: rest
      else x :: (replace_function new_function rest)

let adjust_schedule_inputs possible_inputs =
  List.filter
    (function (env,fwds) ->
      let running =
	match CS.concretize_class CS.RUNNING with
	  [id] -> state_name id
	| _ -> internal_error(-1,15) in
      let readys = List.map state_name (CS.concretize_class CS.READY) in
      (* if running is non-empty or readys are all empty, then restart *)
      if not (is_empty_unk env running) ||
         List.for_all (is_empty_unk env) readys
      then false
      else true)
    possible_inputs

let analyze_untyped_ifunctions nm ifunctions automaton_tgt
    possible_inputs concretized_outputs imemotable =
  src_tgt_handler := false;
  Aux.map_union
    (function Ast.FUNDEF(ret,fnm,params,stmt,inl,attr) as x ->
      handler_name := pr "%s: " (B.id2c fnm);
      let (new_body,possible_outputs) =
	verif_one_ifunction fnm params stmt attr
	  possible_inputs (Some concretized_outputs) imemotable true in
      let new_function = Ast.FUNDEF(ret,fnm,params,new_body,inl,attr) in
      ifunctions := replace_function new_function (!ifunctions);
      List.map (function x -> (nm,(automaton_tgt,normalize x)))
	possible_outputs)
  (!ifunctions)

let analyze_typed_ifunctions nm ifunctions config automaton_tgt imemotable =
  src_tgt_handler := false;
  Aux.map_union
    (function Ast.FUNDEF(ret,fnm,params,stmt,inl,attr) as x ->
      handler_name := pr "%s: " (B.id2c fnm);
      let types = lookup_id fnm (!Events.interface_env) in
      let (new_body,possible_outputs) =
	List.fold_left
	  (function (prev_stmt,prev_outputs) ->
	    function Events.EVENT_TYPE(inputs, outputs, class_outputs) ->
	      let possible_inputs =
		match_config_to_type config
		  (List.map
		     (function (x,f) ->
		       (order_state_env x,f))
		     inputs) in
	      let (new_stmt,possible_outputs) =
		verif_one_ifunction fnm params prev_stmt attr
		  possible_inputs (Some outputs) imemotable false in
	      (new_stmt,Aux.union possible_outputs prev_outputs))
	  (stmt,[]) types in
      let new_function = Ast.FUNDEF(ret,fnm,params,new_body,inl,attr) in
      ifunctions := replace_function new_function (!ifunctions);
      List.map (function x -> (nm,(automaton_tgt,normalize x)))
	possible_outputs)
  (!ifunctions)

let analyze_handlers handlers relevant_handlers
    prev automaton_state automaton_tgt
    possible_inputs concretized_outputs class_outputs memotable =
  Aux.map_union
    (function handler ->
      let (Ast.EVENT(nm,_,_,_,_)) = handler in
      let str_nm = Ast.event_name2c nm in
      handler_name := pr "%s: " str_nm;
      src_tgt_handler := not (str_nm = "bossa.schedule" || str_nm = "preempt");
      let possible_inputs =
	if str_nm = "bossa.schedule" && prev = []
	then adjust_schedule_inputs possible_inputs
	else possible_inputs in
      let (new_configs,new_handler) =
	List.fold_left
	  (function (new_configs,new_handler) ->
	    function (input,fwds) ->
	      current_input_config := (automaton_state,input);
	      try
		(let (newer_configs,newer_handler) =
		  try (Hashtbl.find memotable (prev,nm,input),new_handler)
		  with Not_found ->
		    (forwardImmediateOutputs := fwds;
		     let res =
		       verif_handler new_handler input concretized_outputs
			 class_outputs
		     in match res with
		       Some ((newer_configs,newer_handler) as x) ->
			 Hashtbl.add memotable (prev,nm,input) newer_configs;
			 x
		     | None -> raise Not_found)
		in
		let configs_to_add =
		  List.map
		    (function x -> (nm,([nm],automaton_tgt,normalize x)))
		    newer_configs in
		(Aux.union configs_to_add new_configs,newer_handler))
	      with Not_found -> (new_configs,new_handler))
	  ([],handler) possible_inputs in
      handlers := replace_handler prev new_handler (!handlers);
      List.fold_left
	(function res ->
	  function (nm,(p,aut_tgt,env)) ->
	    let tmp =
	      (nm,
	       (p,aut_tgt,
		List.map
		  (function (id,(vt,srcs,modd)) ->
		    (id,(vt,srcs,VB.UNMODIFIED)))
		  env)) in
	    if Aux.member tmp res then res else tmp::res)
	[] new_configs)
    relevant_handlers
    
(* a state is the number of a node in the automaton and a mapping of
   states to their contents *)

let match_with_spec prev nm specialized_events =
  let suffixes = List.rev (Aux.non_empty_suffixes prev) in
  let rec loop = function
      [] -> []
    | (prev::rest) ->
	let matching_events =
	  List.filter
	    (function (specd,name) ->
	      Ast.event_names_equal prev specd &&
	      Ast.event_name_equal nm name)
	    specialized_events in
	(match matching_events with
	  [] -> loop rest
	| [_] -> prev
	| _ -> error(-1,"more than one match for spec"); []) in
  loop suffixes

let rec get_output_states_blah handler_env specialized_events handlers
    typed_ifunctions untyped_ifunctions
    concretized_types concretized_itypes automaton memotable imemotable
    orig_handlers
    (prev,automaton_state,config) =
  (* collect the edges relevant to the current state in the automaton *)
  let automaton_dests =
    Aux.option_filter
      (function (src,nm,int,tgt) ->
	if src = automaton_state
	then
	  (match int with
	    Objects.INTIBLE -> Some(nm,[],tgt)
	  | Objects.UNINTIBLE ->
	      let prev = match_with_spec prev nm specialized_events in
	      Some(nm,prev,tgt))
	else None)
      automaton in
  (* collect the event types relating to the relevant edges, independent of
     the current configuration *)
  let collect_all nm automaton_dests =
    Aux.option_filter
      (function (name,prev,tgt) -> (* prev is Some x iff x -> name is unint *)
	if Ast.event_name_equal nm name
	then Some (prev,tgt)
	else None)
      automaton_dests in
  let possible_types =
    List.fold_left
      (function types ->
	function (nm,inputs,outputs,class_outputs) ->
	  (List.map
	     (function (prev,tgt) ->
	       (nm,prev,tgt,inputs,outputs,class_outputs))
	     (collect_all nm automaton_dests))
	  @ types)
      [] concretized_types in
  Aux.union
    (match
      Aux.make_set
	(Aux.option_filter
	   (function (nm,prev,automaton_tgt,_,_,_) ->
	     if Ast.event_name2c nm = "generic_interface"
	     then Some (nm,automaton_tgt)
	     else None)
	   possible_types) with
      [(nm,automaton_tgt)] ->
	let res =
	analyze_typed_ifunctions nm typed_ifunctions config automaton_tgt
	  imemotable in
	List.map
	  (function (a,(b,x)) -> check_no_arg x; (a,([nm],b,x)))
	  res
    | _ -> [])
    (Aux.map_union
       (function (nm,prev,automaton_tgt,concretized_inputs,concretized_outputs,
		  class_outputs) ->
	 let possible_inputs =
	   match_config_to_type config concretized_inputs in
	 if Ast.event_name2c nm = "generic_interface"
	 then
	   let res =
	     analyze_untyped_ifunctions nm untyped_ifunctions automaton_tgt
	       possible_inputs concretized_outputs imemotable in
	   List.map
	     (function (a,(b,x)) -> check_no_arg x; (a,([nm],b,x)))
	     res
	 else
	   begin
	     let relevant_handlers =
	       lookup_handler nm handler_env prev (!handlers) orig_handlers in
	     (* is the following test needed?  isn't this checking taken
		care of by hierarchy.ml? *)
	     if relevant_handlers = [] &&
	       not (Ast.event_name2c nm = "unblock.timer.*")
	     then error(-1,pr "no handler matches %s" (Ast.event_name2c nm));
	     let res =
	       analyze_handlers handlers relevant_handlers
		 prev automaton_state automaton_tgt
		 possible_inputs concretized_outputs class_outputs
		 memotable in
	     List.iter
	       (function (_,(_,_,x)) -> check_no_arg x)
	       res;
	     res
	   end)
       possible_types)

and check_no_arg config =
  List.iter
    (function (_,(x,_,_)) ->
      List.iter
	(function
	    VB.ARG(_) -> raise (Error.Error "unexpected arg!!!")
	  | _ -> ())
	(Aux.map_union VB.collect_ps (VB.known_contents x)))
    config

let get_output_states handler_env specialized_events handlers
    typed_ifunctions untyped_ifunctions
    concretized_types concretized_itypes automaton memotable imemotable
    orig_handlers
    (prev,automaton_state,config) =
  let res = 
    get_output_states_blah handler_env specialized_events handlers
      typed_ifunctions untyped_ifunctions
      concretized_types concretized_itypes automaton memotable imemotable
      orig_handlers
      (prev,automaton_state,config) in
  List.iter (function (_,(_,_,x)) -> check_no_arg x) res;
  res
    
let print_config (prev,n,config) =
  Printf.printf "%s%d: %s\n"
    (match prev with
      [] -> ""
    | _ -> (String.concat " " (List.map Ast.event_name2c prev)) ^ " ")
    n (config2c config)

let rec print_transitions = function
    Model.START((_,config)) -> Printf.printf "%s\n" (config2c config)
  | Model.TRANSITION(prev,nm,(_,config)) -> print_transitions prev;
      Printf.printf " -%s-> " (Ast.event_name2c nm);
      Printf.printf "%s\n" (config2c config)

let find_transition (current_state,nm,tgt) transitions =
  let already_seen =
    List.exists
      (function
	  Model.START(config) -> tgt = config
	| Model.TRANSITION(prev,nm,config) -> tgt = config)
      transitions in
  if already_seen
  then Model.NOTHING_NEW
  else
    let (possible,others) =
      List.partition
	(function
	    Model.START(config) -> current_state = config
	  | Model.TRANSITION(prev,nm,config) -> current_state = config)
	transitions in
    match possible with
      (x::rest) -> Model.PREVTRANS(x,rest@others)
    | [] -> Model.NOT_FOUND

let handler_loop handler_env specialized_events handlers
    typed_functions untyped_functions
    concretized_types concretized_itypes
    automaton starting_state memotable imemotable =
  let hcell = ref handlers in
  let tcell = ref typed_functions in
  let utcell = ref untyped_functions in
  let (configs,transitions) =
    Model.model starting_state
      (get_output_states handler_env specialized_events hcell tcell utcell
	 concretized_types concretized_itypes automaton
	 memotable imemotable handlers)
      find_transition print_config in
  (configs,(!hcell),(!tcell)@(!utcell))

let rec verif_handlers default handler_env specialized_events event_env
    handlers typed_ifunctions untyped_ifunctions =
  let memotable =
    (Hashtbl.create(500) :
      (Ast.event_name list * Ast.event_name * state_env, state_env list)
       Hashtbl.t) in
  let imemotable =
    (Hashtbl.create(500) :
      (B.identifier * state_env, state_env list) Hashtbl.t) in

  let (init_configs, init_handlers, init_ifunctions) =
    match !B.schedtype with
  (* A VS either initially has something in its sorted queue, if it is the
  new root, or it has nothing, if it is added as a new leaf *)
      B.VIRTSCHED ->
	([([],Events.automaton_start,
	  order_state_env
	    (List.map
	       (function
		   (st,CS.STATE(id,CS.NOWHERE,ty,_)) ->
		     (st,(VB.UNKNOWN,[],VB.MODIFIED))
		 | (st,CS.STATE(id,CS.READY,CS.QUEUE(CS.SORTED(_)),_)) ->
		     (st,(VB.UNKNOWN,[],VB.MODIFIED))
		 | (st,_) -> (st,(VB.EMPTY,[],VB.MODIFIED)))
	       (!CS.state_env)))],
	 handlers,typed_ifunctions)

  (* A PS initially runs its new_initial_process event, if it is a default
  scheduler and the first to be loaded, or it runs its attach function *)
    | B.PROCSCHED ->
	let init_state =
	  order_state_env
	    (List.map
	       (function
		   (st,(CS.STATE(id,CS.NOWHERE,ty,_) as x)) ->
		     (st,(VB.MEMBER([VB.VP(VB.ARG(1),[x])]),
			  [],VB.MODIFIED))
		 | (st,_) -> (st,(VB.EMPTY,[],VB.MODIFIED)))
	       (!CS.state_env)) in
	let (init_configs, init_handlers, _) =
	  if default
	  then
	    handler_loop handler_env specialized_events handlers [] []
	      (normalize_all event_env)
	      (normalize_all (!Events.interface_env))
	      (Events.init_automaton())
	      [([],Events.automaton_start,init_state)]
	      memotable imemotable
	  else ([], handlers, []) in
	let (init_attach_configs, init_ifunctions) =
	  List.fold_left
	    (function (new_configs,new_ifunctions) ->
	      function Ast.FUNDEF(ret,nm,params,stmt,inl,attr) as x ->
		if B.id2c nm = "attach"
		then
		  (handler_name := pr "%s: " (B.id2c nm);
		  let (new_stmt,outputs) =
		    verif_one_ifunction nm params stmt attr
		      [(init_state,[])] None imemotable false in
		  (outputs@new_configs,
		   (Ast.FUNDEF(ret,nm,params,new_stmt,inl,attr)
		   :: new_ifunctions)))
		else (new_configs,x::new_ifunctions))
	    ([],[]) typed_ifunctions in
	let init_ifunctions = List.rev init_ifunctions in
	(init_configs @
	 (List.map (function x -> ([],Events.automaton_start,x))
	    init_attach_configs),
	 init_handlers, init_ifunctions) in
  let init_configs =
    Aux.make_set
      (List.map
	 (function (_,_,config) ->
	   ([],Events.automaton_start,normalize config))
	 init_configs) in
  let (configs,handlers,ifunctions) =
    handler_loop handler_env specialized_events init_handlers
      init_ifunctions untyped_ifunctions (normalize_all event_env)
      (normalize_all (!Events.interface_env))
      (Events.automaton()) init_configs memotable imemotable in
  (handlers, ifunctions)

(* --------------------- handler preprocessing ---------------------- *)

let handler_environment =
  ref ([] : (Ast.event_name * Ast.event_name list) list)

let preprocess_handlers events handlers procdecls globaldefs =
  let event_names =
    Aux.option_filter
      (function (nm,_) ->
	match (!B.schedtype,Ast.event_name2c nm) with
	  (B.PROCSCHED,"attach") | (B.PROCSCHED,"detach") |
	  (_,"generic_interface") -> None
	| _ -> Some nm)
      events in
  let handler_names =
    List.map (function Ast.EVENT(name,_,_,_,_) -> name) handlers in
  Hierarchy.associate event_names handler_names procdecls globaldefs

let preprocess_specialized_events handler_env top_level_events =
  let tlnames =
    Events.top_level_event_names_loop
      (function prev -> function last ->
	function specd_name -> function star -> function attr ->
	let concretized_prev =
	  List.map (handler_name_lookup handler_env) prev in
	let suffixes =
	  List.rev(Aux.non_empty_suffixes concretized_prev) @ [[]] in
	let chosen_prev =
	  List.find
	    (List.for_all (function [x] -> true | _ -> false))
	    suffixes in
	(List.flatten chosen_prev,last))
      (function entry_point -> ())
      top_level_events in
  List.flatten (List.map (function (x,_) -> x) tlnames)

let instantiate_top_level_handlers handlers handler_env top_level_events =
  let entry_points = List.map List.hd top_level_events in
  let invert_envs =
    Aux.option_filter
      (function Ast.EVENT_NAME(nm,_,_) as x ->
	match handler_name_lookup handler_env x with
	  [Ast.EVENT_NAME(nm1,_,_) as y] ->
	    if (List.length nm1) < (List.length nm)
	    then Some ((y,x),x)
	    else None
	| _ -> None)
      entry_points in
  let (invert_env,handler_env_extension) = List.split invert_envs in
  (List.flatten
     (List.map
	(function Ast.EVENT(name,specd,param,stmt,attr) as x ->
	  x ::
	  (Aux.option_filter
	     (function (hnm,tynm) ->
	       if Ast.event_name_equal name hnm
	       then Some (Ast.EVENT(tynm,specd,param,stmt,attr))
	       else None)
	     invert_env))
	handlers),
   handler_env_extension)

(* -------------------------- entry point --------------------------- *)

let verify ast =
  let (Ast.SCHEDULER(nm,
		     cstdefs,enums,dom,grp,procdecls,fundecls,valdefs,states,
		     criteria,trace,handlers,ifunctions,functions,attr))
      as ast =
    Pre_verif.preprocess ast in
  let events = B.sched_select (!Events.event_env) (!Events.vs_event_env) in
  let top_level_events = Events.top_level_events() in
  let handler_env = preprocess_handlers events handlers procdecls valdefs in
  flush stdout;
  let (handlers,handler_env_extension) =
    instantiate_top_level_handlers handlers handler_env top_level_events in
  let handler_env =
    List.map
      (function (tnm,hnms) as x ->
	match List.filter (Ast.event_name_equal tnm) handler_env_extension with
	  [x] -> (tnm,[tnm])
	| _ -> x)
      handler_env in
  handler_environment := handler_env;
  let specialized_events =
    preprocess_specialized_events handler_env top_level_events in
  Events.specialized_events := specialized_events;
  set_state_names states;
  let (ignore,type_provided,no_type_provided) =
    classify_ifunctions ifunctions in
  let (new_handlers,new_ifunctions) =
    verif_handlers default handler_env (Aux.make_set specialized_events)
      events handlers type_provided no_type_provided in
  flush stdout;
  Ast.SCHEDULER(nm,default,
		cstdefs,enums,dom,grp,procdecls,fundecls,valdefs,states,criteria,
		trace,new_handlers,ignore@new_ifunctions,functions,attr)
