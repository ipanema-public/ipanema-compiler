(* drop unnecessary nested sequences *)

let rec process_stm = function
    Ast.IF(tst,thn,els,a) ->
      [Ast.IF(tst,process_one_stm thn,process_one_stm els,a)]
  | Ast.FOR(x,y,z,d,stm,crit,a) ->
      [Ast.FOR(x,y,z,d,process_one_stm stm,crit,a)]
  | Ast.FOR_WRAPPER(x,stms,a) ->
      [Ast.FOR_WRAPPER(x,Aux.flat_map process_stm stms,a)]
  | Ast.SWITCH(exp,cases,dflt,a) ->
      let cases =
	List.map
	  (function Ast.SEQ_CASE(a,b,stm,c) ->
	    Ast.SEQ_CASE(a,b,process_one_stm stm,c))
	  cases in
      let dflt =
	match dflt with
	  None -> None
	| Some stm -> Some (process_one_stm stm) in
      [Ast.SWITCH(exp,cases,dflt,a)]
  | Ast.SEQ([],stms,a) -> Aux.flat_map process_stm stms
  | Ast.SEQ(decls,stms,a) ->
      [Ast.SEQ(decls,Aux.flat_map process_stm stms,a)]
  | Ast.SAFEMOVEFWD(exp,x,s,b,a) ->
      [Ast.SAFEMOVEFWD(exp,x,process_one_stm s,b,a)]
  | stm -> [stm]

and process_one_stm stm =
  match process_stm stm with
    [x] -> x
  | stms -> Ast.SEQ([],stms,Ast.get_stm_attr stm)

(* --------------------------- Entry point -------------------------- *)

let process_handlers handlers =
  List.map
    (function Ast.EVENT(nm,param,stm,syn,attr) ->
      Ast.EVENT(nm,param,process_one_stm stm,syn,attr))
    handlers

let process_functions functions =
  List.map
    (function Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr) ->
      Ast.FUNDEF(tl,ret,nm,params,process_one_stm stmt,inl,attr))
    functions

let braces
    (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		   fundecls,valdefs,domains,states,cstates,criteria,
		   trace,handlers,chandlers,ifunctions,functions,attr)) =
  Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,
		fundecls,valdefs,domains,states,cstates,criteria,
		trace,process_handlers handlers,process_handlers chandlers,
		process_functions ifunctions, process_functions functions,attr)
