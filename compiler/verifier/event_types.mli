type entry = Class_state.classname * Vobjects.vtype * Vobjects.modified

type event_class_type =
    Objects.isquasi *
      (entry list * Class_state.classname list) list *
      entry list list

val initial_event_specifications : unit -> Ast.event_name list
val system_event_specifications : unit -> Ast.event_name list
val application_event_specifications : unit -> Ast.event_name list
val top_level_events : unit -> Ast.event_name list list
val scheduler_events : unit -> Ast.event_name list list

val app_events_init_automaton :
    unit -> (int * Ast.event_name * Objects.intinfo * int) list
val app_events_automaton :
    unit -> (int * Ast.event_name * Objects.intinfo * int) list
val event_specifications :
    unit -> (Ast.event_name * event_class_type list) list
val vs_event_specifications :
    unit -> (Ast.event_name * event_class_type list) list
val interface_specifications :
    unit -> (Objects.identifier * event_class_type list) list
val vs_interface_specifications :
    unit -> (Objects.identifier * event_class_type list) list
