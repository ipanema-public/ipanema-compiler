(* Utility functions for verification *)

module B  = Objects
module CS = Class_state

exception LookupErrException of string option ;;


(******************* Global Environments ******************)

(* Environment Definition *)
type environment =  {
    (* List of identifiers for which value is known. We assume that this
    * is correctly typed. Otherwise, will fail when executing function.
    * For simplicity*)
    assigned_values: (B.identifier * Ast.expr) list;
    (* Hypothetical enviroments. These are values for which we
    * speculate the value. These value assignments should only
    * be queried after the value_env in verifier_util has been
    * queried, as value_env represents the true values of the
    * assignments at that point in time *)
    guessed_values: (B.identifier * Ast.expr) list;
    (* Stores mapping from function identifier to function
    * definition *)
    functions: (B.identifier * Ast.fundef) list
}

(* Generates environment *)
let mkEnv (assigned_vals:(B.identifier * Ast.expr) list)
          (guessed_vals: (B.identifier * Ast.expr) list)
          (fn: (B.identifier * Ast.fundef) list): environment=
    {
        assigned_values = assigned_vals ;
        guessed_values = guessed_vals ;
        functions = fn;
    }

let updFctEnv environment (fn: (B.identifier * Ast.fundef) list): environment =
    {
        assigned_values =  environment.assigned_values;
        guessed_values = environment.guessed_values;
        functions = fn;
    }

let lookfor_exp id env =
  snd (List.find (fun (idl, exp) -> B.idnameq (id, idl)) env.assigned_values)

(* Internal type to encode the forking structure of control flow
 * statements like if, for, switch and return expressions of interest.
 * The number of branches will be at least equal to the number of
 * expressions, and at most equal to the n + 1 expressions (in the case
 * of IF, of if SWITCH has default, for example *)
type 'a reachability =
    REACH of 'a * environment
  | FORK of Ast.expr list option *  (* branching statement *)
      ('a reachability) option list *  (* move or return statements *)
      environment

let verif_env = ref ({
    assigned_values = [];
    guessed_values = [];
    functions = [];
})

(* Takes cartesian product of two lists, returns the merged
 * list: ak [[a,b],[c,d]] and [[e,f],[g,h]] will return
 * [a,b,e,f], [a,b,g,h],[c,d,e,f],[c,d,g,h] *)
let rec list_cartesian (li1: ('a list list)) (li2: ('a list list)) : 'a list list  =
      List.concat (List.map (
          fun x1 ->
              List.map (fun x2 -> x1@x2) li2)
          li1)

  (* Test if two fields or variables are equal *)
let rec id_equal var1 var2 =
    match var1,var2 with
       Ast.FIELD(exp1,id1,_,_), Ast.FIELD(exp2,id2,_,_)
       -> ((id1 = id2) && id_equal exp1 exp2 )
       | Ast.VAR(id1,_,_), Ast.VAR(id2,_,_) -> (Objects.id2c id1 = Objects.id2c id2)
       | Ast.BOOL(b1,_), Ast.BOOL(b2,_) -> b1 = b2
       | Ast.INT(i1,_), Ast.INT(i2,_) -> i1 = i2
       | _ -> false

(* FIXME: Find a cleaner to uniquely identify fields
 * Converts field (or variable) to identifier*)
let field_to_id expression =
     let rec field_to_id_ = (fun expression accu ->
      match expression with
       Ast.FIELD(exp1,id1,_,_) ->
           let accu =  (
               match accu with
               "" -> Objects.id2c id1
              |_ -> (Objects.id2c id1)^"_"^accu
           ) in
           field_to_id_ exp1 accu
       | Ast.VAR(id1,_,_) ->  (
               match accu with
               "" -> Objects.id2c id1
               | _ -> (Objects.id2c id1)^"_"^accu
       )
       | Ast.SELF _ ->  "self"
       | _ -> raise (Error.Error "Unsupported type"))
     in  Objects.ID(field_to_id_ expression "",0)


(* Utility / error  functions *)
let rec lookup_id x li= match li with
    [] -> raise (LookupErrException (Some(B.id2c x)))
  | ((y,v)::r) -> let equal = Objects.idnameq(x,y) in
    if (equal = true) then v else lookup_id x r

let rec remove_id (x: 'a)  (li: ('a * 'b) list) : (('a * 'b) list)  = match li with
    [] -> []
  | ((y,v)::r) -> if (Objects.idnameq(x,y) = true) then (remove_id x r) else (y,v)::(remove_id x r)


(* For a given list of id, expressions, returns all the identifiers which
 * have an expression equal to the one given as argument
 * TODO: efficiency concerns + make polymorphic (currently can't because
 * of equality test that is not overloaded *)
let reverse_lookup_var (x:B.identifier) (li: (B.identifier * Ast.expr) list) =
    let fn =  (fun accu (key,el) ->
                match el with
                     Ast.FIELD(_,_,_,_) as fi -> (
                         if (Objects.idnameq(key,x) = true) then ((field_to_id(fi))::accu)
                         else accu
                     )
                   | Ast.VAR(id,_,_) ->  (
                         if (Objects.idnameq(key,x)=true) then (id::accu)
                         else accu
                         )
                   | _ -> accu) in
    List.fold_left fn [] li

(* Applies reverse_lookup_var transitively, that is, if (a,b), and (b,c)
 * will return [a,b] *)
let rec reverse_lookup_var_trans (x:B.identifier) (li: (B.identifier * Ast.expr ) list) =
    let keys = reverse_lookup_var x li in
    let fn = (fun accu id ->
                let k = reverse_lookup_var id li in
                k@accu
             )  in
    List.fold_left fn [] keys


(* First check in assigned values, then in guessed values.
 * That way, known value always takes precedence*)
let lookup_var_env id: Ast.expr =
   ( try (
        lookup_id id (!verif_env).assigned_values
   ) with LookupErrException _ -> (
            lookup_id id (!verif_env).guessed_values
   ))


(* Test if a field is contained in a list of expressions *)
let rec is_contained (var: Ast.expr) (vars: Ast.expr list): bool  =
    match vars with
        [] -> false
        | car::cdr ->
                let is_member = id_equal var car in
                if (is_member = true) then true
                else is_contained var cdr

(* Test if an identifier is contained in a list of expressions *)
let rec is_contained_id (var: Objects.identifier) (vars: Objects.identifier list): bool  =
    match vars with
        [] -> false
        | car::cdr ->
                if (var = car) then true
                else is_contained_id var cdr


(* Test two lists of fields/vars intersect *)
let rec intersect (fields_l: Ast.expr list) (fields_r: Ast.expr list) : bool =
    match fields_l with
    [] -> false
    | x::xs-> if is_contained x fields_r then true
              else intersect xs fields_r

(* Checks if a variable/field is equal to another variable or
 * field by looking at, first, the assigned values, and next,
 * the guessed values. Currently supports only expressions
 * that are variable or fields*)
let is_equal (exp1:Ast.expr) (exp2:Ast.expr) =
    (* First, identify list of expressions that this
     * expression can be mapped to *)
    match exp1 with
         Ast.FIELD(_,_,_,_) as fi -> (
              let id1 = field_to_id fi in
              let keys = reverse_lookup_var_trans id1 (!verif_env).assigned_values  in
              match exp2 with
                 Ast.FIELD(_,_,_,_) as fi2 ->
                     let id2 = field_to_id fi2 in
                     (Objects.id2c id1  = Objects.id2c id2) || is_contained_id id2 keys
                 | Ast.VAR(_,_,_) as fi2 ->
                     let id2 = field_to_id fi2 in
                     is_contained_id id2 keys
                 | _ -> false
         )
       | Ast.VAR(_,_,_) as fi ->  (
              let id1 = field_to_id fi in
              let keys = reverse_lookup_var_trans id1 (!verif_env).assigned_values  in
                match exp2 with
                 Ast.FIELD(_,_,_,_) as fi2 ->
                     let id2 = field_to_id fi2 in
                     is_contained_id id2 keys
                 | Ast.VAR(_,_,_) as fi2 ->
                     let id2 = field_to_id fi2 in
                     (Objects.id2c id1 = Objects.id2c id2)||is_contained_id id2 keys
                 | _ -> false
        )
       | _ -> false

(*************** State typing **********************)

let in_core_event = (ref false: bool ref)

let type_pstate (exp:Ast.expr) : CS.classname option =
  match exp with
    Ast.VAR(nm,_,attr) ->
      (try Some(CS.state2class(lookup_id nm (!CS.state_env)))
       with LookupErrException _ -> None)
  | Ast.FIELD(_,fld,_,attr) -> (* FIXME: 1st exp is ignored ! *)
     (try Some(CS.state2class(lookup_id fld (!CS.state_env)))
      with LookupErrException _ -> None)
  | _ -> None

let type_cstate (exp:Ast.expr): CS.state_info option =
  match exp with
    Ast.VAR(nm,_,attr) ->
      (try Some(lookup_id nm (!CS.cstate_env))
       with LookupErrException _ -> None)
  | Ast.FIELD(_,fld,_,attr) ->
     (try Some(lookup_id fld (!CS.cstate_env))
       with LookupErrException _ -> None)
  | _ -> None


(************* Expression Evaluation ************)
(* Partially evaluates expression based on known values.
 * Simplifies expressions as much as possible *)
let rec simplify_expression (expression:Ast.expr) : Ast.expr =
    match expression with
        Ast.UNARY(uop,expr,li) -> (
           let expr = simplify_expression expr in
           match uop with
            Ast.NOT ->
                ( match expr with
                 Ast.BOOL(a,li) -> Ast.BOOL(not a,li)
                | _  -> expression
                )
            | Ast.COMPLEMENT -> expression
        )
        | Ast.BINARY(bop,expr1,expr2,li) -> (
            let expr1 = simplify_expression expr1 in
            let expr2 = simplify_expression expr2 in
                match expr1 , expr2 with
                    Ast.INT(i,li), Ast.INT(i2,li2) -> (
                        match bop with
                           Ast.PLUS -> Ast.INT (i+i2,li)
                           | Ast.MINUS ->
                                   let a = i - i2 in
                                   if (a < 0) then raise (Error.Error "No negative numbers supported") ;
                                   Ast.INT (i-i2,li)
                            | Ast.TIMES -> Ast.INT (i * i2,li)
                            | Ast.DIV -> Ast.INT(i/i2,li)
                      (*      | Ast.MOD -> Ast.INT((mod i i2), li) *)
                            | Ast.EQ -> Ast.BOOL(i=i2,li)
                            | Ast.NEQ -> Ast.BOOL(i!=i2,li)
                            | Ast.GEQ -> Ast.BOOL(i>i2,li)
                            (* | BITAND -> INT((land i i2),li)
                            | BITOR -> INT((lnot i i2),li) *)
                           (* | Ast.LSHIFT -> Ast.INT((lsl i i2),li)
                            | Ast.RSHIFT -> Ast.INT((lsr i i2),li) *)
                            | _ -> expression
                    )
                    | Ast.BOOL(a,li), Ast.BOOL(a2,li1) -> (
                            match bop with
                                Ast.AND -> Ast.BOOL(a&&a2,li)
                                |Ast.OR -> Ast.BOOL(a||a2,li)
                                |Ast.EQ -> Ast.BOOL(a=a2,li)
                                |_ -> expression
                    )
                    | Ast.FIELD(_,_,_,_), Ast.FIELD(_,_,_,_)  -> (
                        (* TODO add support for aliasing *)
                            match bop with
                               Ast.EQ ->  (
                                   let equal = id_equal expr1 expr2 in
                                   match equal with
                                   true -> Ast.BOOL(true,li)
                                   | false -> expression (* Cannot simplify as could
                                   potentially refer to the same expression. TODO: when
                                   aliasing is precise, change *)
                               )
                               | _ -> expression
                    )
                    | Ast.VAR(_,_,_), Ast.VAR(_,_,_) -> (
                            match bop with
                               Ast.EQ ->  (
                                   let equal = id_equal expr1 expr2 in
                                   match equal with
                                   true -> Ast.BOOL(true,li)
                                   | false -> expression (* Cannot simplify as could
                                   potentially refer to the same expression. TODO: when
                                   aliasing is precise, change *)
                               )
                               | _ -> expression
                    )
                    | _ , _ -> expression
        )
        | Ast.VAR(id,_,_) ->
            (try
               lookup_var_env id
            with LookupErrException _ -> expression)
        | Ast.FIELD(_,_,_,_) ->
            let id = field_to_id expression in
            (try
                lookup_var_env id
            with LookupErrException _ -> expression)
        | Ast.BOOL(_,_) -> expression
        | Ast.INT(_,_) -> expression
	| Ast.VALID _ -> expression
	| Ast.PRIM _ -> expression
        | _ ->
	   Aux.error (Ast.get_exp_attr expression, "unimplemented expression");
	   Pp.pretty_print_exp expression;
	   assert(false)


(* Next prune expressions: if expression is true, branches that follow
 * will never be executed and can be pruned. If expression is false, branch
 * for this expression will never be executed, subsequent expressions might be executed *)

 let rec prune_func = ( fun (exp:Ast.expr list ) (branches: ('a reachability option list)) ((accum_exp, accum_branches):Ast.expr list * 'a reachability option list)->
   match exp with
       [] ->  (accum_exp, accum_branches@branches)
     | e::es ->  (
       match e with
           Ast.BOOL(true,_) ->  (
             match branches with
                 [] -> raise (Error.Error "Incorrect reachability structure")
               | b::bs -> (accum_exp@[e], accum_branches@[b])
           )
         | Ast.BOOL(false,_) ->  (
           match branches with
               [] -> raise (Error.Error "Incorrect reachability structure. Cannot have fewer branches than control flow expressions")
             | b::bs -> prune_func es bs (accum_exp, accum_branches)
         )
         | _ -> (
           match branches with
               [] -> raise (Error.Error "Incorrect reachability struture. Cannot have
                                fewer branches than control flow expressions")
             | b::bs -> prune_func es bs (accum_exp@[e], accum_branches@[b])
         )
     )
 )

(* Given an assignment of variables, simplifies the reachability structure*)
let rec evaluate_reachability (reach: 'a reachability option): 'a reachability option =
   (* 1) assign environment to value_env
    * 2) go through reachability, simplifying expressions using simplify_expression
    * 3) return new reachability structure  *)
    match reach with
        None -> None
       | Some(REACH(stmt,_)) -> reach (* Cannot simplify further *)
       | Some(FORK(exp,branches,env)) ->  (
           verif_env:=env;
           (* Compute new simplified expressions *)
           let new_exp =  ( match exp with
              None -> None
              | Some(exp) -> Some(List.map (simplify_expression) exp)
           ) in
           let (new_exp, new_branches) =  (
                match new_exp with
                    None -> (new_exp,branches)
                   | Some(new_exp) ->  (
                           let (new_exp,branches) = prune_func new_exp branches ([],[]) in
                           (Some(new_exp),branches)
                    )
            ) in
            let new_branches = List.map (fun x -> evaluate_reachability x ) new_branches in
            let nb_branches = List.length new_branches in
            if (nb_branches = 1) then (
                 (* This is no longer a fork, just return*)
                List.hd new_branches
            ) else  (
                Some(FORK(new_exp, new_branches, !verif_env)))
            )

  (* TODO merge two functions
   * Checks that  the bool_function, appplied to the move
   * statement returns true for some paths in the reachability
   * structure *)
 let rec check_some_paths (reach: 'a reachability option ) (bool_function: 'a option -> environment -> bool) : bool =
    match reach with
        | None -> bool_function None (mkEnv [] [] [])
        | Some (REACH(stmt, env)) -> bool_function (Some(stmt)) env
        | Some (FORK (expr, paths,_)) ->
	  (* Path should never be empty *)
          List.fold_left (fun accu path -> accu || check_some_paths path bool_function) false paths

 (* Checks that  the bool_function, appplied to the move
  * statement returns true for all paths in the reachability
  * structure *)
 let rec check_all_paths (reach: 'a reachability option ) (bool_function: 'a option->  environment ->bool) : bool =
    match reach with
        | None -> false
        | Some (REACH(stmt, env)) -> bool_function (Some(stmt)) env
        | Some (FORK (expr, paths,_)) ->
	  (* Path should never be empty *)
          List.fold_left (fun accu path -> accu && check_all_paths path bool_function) true paths

  (* Checks that  the bool_function, appplied to the move
  * statement returns true for all paths in the reachability
  * structure *)
 let rec check_no_paths(reach: 'a reachability option ) (bool_function: 'a option ->  environment ->bool) : bool =
    match reach with
        | None -> true
        | Some (REACH(stmt, env)) -> not (bool_function (Some(stmt)) env)
        | Some (FORK (expr, paths,_)) ->
             List.fold_left (
                fun accu path -> if (accu = true) then
                    check_no_paths path bool_function  else false
            )  true paths (* Path should never be empty *)

 let check_process_class (exp:Ast.expr) (cla:CS.classname) : bool =
   let exp_type = type_pstate exp in
   match exp_type with
       Some (exp_class) -> exp_class = cla
     | None -> false

let check_core_class (exp: Ast.expr) (cla:CS.corename) : bool =
    let exp_type = type_cstate exp in
    match exp_type with
        Some(exp_type) -> ((CS.cstate2class exp_type) = cla)
        | None -> false

   (* Utility function to extract all reachable
 * move statements. This function makes the assumption
 * that all code paths contain a single move
 * TODOs:
     * 1) merge move and return functions into a single more general one?
     * 2) generalise function so that can support more than one move
     * statements *)
let rec extract_move_unique_ (bool_fn: (Ast.stmt -> bool)) (stmt: Ast.stmt): (Ast.stmt reachability) option =
     match stmt with
        Ast.IF(branch,stmt1,stmt2,_) ->  (
            (* Because branches are mutually exclusive, merge
             * two moves into a single reachable move *)
            let if_stmt = extract_move_unique_ bool_fn stmt1 in
            let else_stmt = extract_move_unique_ bool_fn stmt2 in
            match branch with
                  Ast.BOOL(true,_) -> if_stmt
                | Ast.BOOL(false,_) -> else_stmt
                | _ ->  (
                    match if_stmt , else_stmt with
                        None , None  -> None
                      | _ ->
			(Some (FORK(Some [branch],
				    [if_stmt]@[else_stmt],
				    (!verif_env))))
                )
        )
     | Ast.FOR(id,inv,state,_,loop,_,_) -> (
                (* FIXME: consider break/continue if don't dismiss for automatically *)
       let Some [value] = inv in
       let vals = remove_id id (!verif_env).assigned_values in
       verif_env :=
         mkEnv ((id,value)::vals)
           (!verif_env).guessed_values
           (!verif_env).functions;
       let inner_for = extract_move_unique_ bool_fn loop in
       match inner_for with
         None -> None
       | _ ->
          (Some (FORK(inv,
		      [Some(REACH(stmt, !verif_env))]
		      @[inner_for],!verif_env)))
        )
        | Ast.SEQ(vals,stmts,_) ->  (

                (* Add local variables to environment *)
                List.iter (
                        fun (Ast.VALDEF(Ast.VARDECL(_,name,_,_,_,_,_),exp,_,_)) ->
                            verif_env :=  (
                              mkEnv ((name,exp)::(!verif_env).assigned_values) (!verif_env).guessed_values (!verif_env).functions)) vals;


                (* TODO deal with break/continue *)
                let accu = [] in
                let reachability_list =
                    List.fold_left
                    (fun reachabilities s ->
                        let reachability = extract_move_unique_ bool_fn s in
                        reachability::reachabilities) accu stmts in

                (* Remove local variables from environment *)
                List.iter (
                        fun (Ast.VALDEF(Ast.VARDECL(_,name,_,_,_,_,_),exp,_,_)) ->
                            verif_env :=  (
                                mkEnv (remove_id name (!verif_env.assigned_values)) (!verif_env).guessed_values (!verif_env).functions)) vals;

                (* Merge list. There should only be one None *)
                let reachability_list = List.filter
                    (function
                        None -> false
                       | Some x -> true) reachability_list in
                let length  = List.length reachability_list in
                if (length > 1) then raise (Error.Error "Multiple move statements in code path");
                if (length = 0) then None
                else List.hd reachability_list
                )

        | Ast.ASSIGN(assigned,value,_,_) -> (
          if B.ty (Ast.get_exp_attr assigned) = B.PROCESS then
            begin
              match assigned with
                Ast.VAR(_,_,_)
              | Ast.FIELD(_,_,_,_) ->  (
                let assigned_id = field_to_id assigned in
                let vals = remove_id assigned_id (!verif_env).assigned_values in
                verif_env :=
                  mkEnv ((assigned_id,value)::vals) (!verif_env).guessed_values (!verif_env).functions )
              | _ -> ()
            end;
          None
        )
        | Ast.SWITCH(switch,cases, default,li) ->
           (
                let stmts = List.map (function Ast.SEQ_CASE(id,state,stmt,li) -> stmt)  cases in
                let default = match default with
                    None -> []
                    | Some stmt -> [stmt] in
                let stmts = stmts@default in
                let accu = [] in
                let reachability_list =
                    List.fold_left
                    (fun reachabilities stmt ->
                        let reachability = extract_move_unique_ bool_fn stmt in
                        [reachability]@reachabilities) accu stmts in
               (* Unlike SEQ, these are all possibilities, so create a FORK *)
                let count = List.length (
                    List.filter (function None -> false | Some x -> true) reachability_list) in
                if (count > 0) then (Some (FORK(Some[switch],reachability_list,!verif_env)))
                else None
            )
        | Ast.PRIMSTMT(exp,_,li) ->  (
            (* Assumption: functions are not mutually recursive, so these calls will
             * terminate *)
              match exp with
                Ast.VAR(id,_,attr) ->
		  begin
                    let f_ =
		      try Some (lookup_id id (!verif_env).functions)
		      with
			LookupErrException _ -> None
		    (*  (* This should be a system function calls which
			is not listed in (!verif_env).functions *)
			raise (Error.Error (__LOC__ ^
			": Unknown function call at "^B.loc2c li))
		    *)
		    in
		    match f_ with
		      None -> None
		    | Some (Ast.FUNDEF(trylock,ty,id,parm,stmt,inl,li)) ->
                       extract_move_unique_ bool_fn stmt
		  end
              | _ -> raise (Error.Error "Incorrect function call")
         )
         | Ast.MOVE(exp1,exp2,_,_,_,_,_,li) as stmt -> (
              if (bool_fn stmt) then (Some (REACH(stmt,!verif_env)))
              else None
         )
        | _ -> None

let extract_move_unique (bool_fn: (Ast.stmt -> bool)) (stmt: Ast.stmt):
(Ast.stmt reachability) option =
    (*  verif_env := mkEnv [] [] []; *)
    extract_move_unique_ bool_fn stmt


 let rec extract_return_unique_ (bool_function:Ast.expr -> bool) (stmt: Ast.stmt): (Ast.expr option reachability) option =
     match stmt with
        Ast.IF(branch,stmt1,stmt2,_) ->  (
            (* Because branches are mutually exclusive, merge
             * two moves into a single reachable move *)
            let if_stmt = extract_return_unique_ bool_function stmt1 in
            let else_stmt = extract_return_unique_ bool_function stmt2 in
            match branch with
                Ast.BOOL(true,_) -> if_stmt
                | Ast.BOOL(false,_) -> else_stmt
                | _ ->  (
                    match if_stmt , else_stmt with
                        None , None  -> None
                        | _ ->  (Some (FORK(Some [branch],[if_stmt]@[else_stmt], !verif_env)))
                )
        )
        | Ast.FOR(id,inv,_,_,loop,_,_) -> (
                (* FIXME: consider break/continue if don't dismiss for automatically *)
          let Some [value] = inv in
          let vals = remove_id id (!verif_env).assigned_values in
          verif_env :=
            mkEnv ((id,value)::vals)
              (!verif_env).guessed_values
              (!verif_env).functions;
          let inner_for = extract_return_unique_ bool_function loop in
          match inner_for with
            None -> None
          | _ ->
             (Some (FORK(inv, [None]@[inner_for], !verif_env))))

        | Ast.SEQ(vals,stmts,_) ->  (

                (* Add local variables to environment *)
                List.iter (
                        fun (Ast.VALDEF(Ast.VARDECL(_,name,_,_,_,_,_),exp,_,_)) ->
                            verif_env :=  (
                                mkEnv ((name,exp)::(!verif_env).assigned_values) (!verif_env).guessed_values (!verif_env).functions)) vals;


                (* TODO deal with break/continue *)
                let accu = [] in
                let reachability_list =
                    List.fold_left
                    (fun reachabilities stmt ->
                        let reachability = extract_return_unique_ bool_function stmt in
                        reachability::reachabilities) accu stmts in

                (* Remove local variables from environment *)
                List.iter (
                        fun (Ast.VALDEF(Ast.VARDECL(_,name,_,_,_,_,_),exp,_,_)) ->
                            verif_env :=  (
                                mkEnv (remove_id name !verif_env.assigned_values) (!verif_env).guessed_values (!verif_env).functions)) vals;


                (* Merge list. There should only be one None *)
                let reachability_list = List.filter
                    (function
                        None -> false
                       | Some x -> true) reachability_list in
                let length  = List.length reachability_list in
                if (length > 1) then raise (Error.Error "Multiple move statements in code path");
                if (length = 0) then None
                else List.hd reachability_list
        )
        | Ast.ASSIGN(assigned,value,_,_) ->  (
          if B.ty (Ast.get_exp_attr assigned) = B.PROCESS then
            begin
              match assigned with
                Ast.VAR(_,_,_)
              | Ast.FIELD(_,_,_,_) ->
                 let assigned_id = field_to_id assigned in
                 let vals = remove_id assigned_id (!verif_env).assigned_values in
                 verif_env :=
                   mkEnv ((assigned_id,value)::vals) (!verif_env).guessed_values (!verif_env).functions
              | _ -> ()
            end;
          None
        )
        | Ast.SWITCH(switch,cases, default,li) ->
           (
                let stmts = List.map (function Ast.SEQ_CASE(id,state,stmt,li) -> stmt)  cases in
                let default = match default with
                    None -> []
                    | Some stmt -> [stmt] in
                let stmts = stmts@default in
                let accu = [] in
                let reachability_list =
                    List.fold_left
                    (fun reachabilities stmt ->
                        let reachability = extract_return_unique_ bool_function stmt in
                        [reachability]@reachabilities) accu stmts in
               (* Unlike SEQ, these are all possibilities, so create a FORK *)
                let count = List.length (
                    List.filter (function None -> false | Some x -> true) reachability_list) in
                if (count > 0) then (Some (FORK(Some[switch],reachability_list,!verif_env)))
                else None
            )
        | Ast.PRIMSTMT(exp,_,li) ->  (
            (* Assumption: functions are not mutually recursive, so these calls will
             * terminate *)
              match exp with
                Ast.VAR(id,_,attr) ->
                    let Ast.FUNDEF(trylock,ty,id,parm,stmt,inl,li) =
                        (try lookup_id id (!verif_env).functions with
                                 LookupErrException _ -> raise (Error.Error (__LOC__ ^": Unknown function call")))
                    in
                    extract_return_unique_ bool_function stmt
                | _ -> raise (Error.Error "Incorrect function call")
         )
         | Ast.RETURN(expr,li) -> Some (REACH(expr,!verif_env))
         | _ -> None
 let extract_return_unique (bool_fn: (Ast.expr -> bool)) (stmt: Ast.stmt):
(Ast.expr option reachability) option =
    verif_env := mkEnv [] [] [];
    extract_return_unique_ bool_fn stmt




(****************** High-level functions **********************)

 let lookup_event handlers enm  =  List.hd
   (List.filter(
     fun (Ast.EVENT(nm,_,_,_,_)) ->
       if (Ast.event_name2c nm = enm) then true else false)
      handlers)
