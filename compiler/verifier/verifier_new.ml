module B = Objects
module CS = Class_state
module Util = Verif_util

exception LookupErrException of string option ;;


(* Verifies that, for the core entry event, the target core is placed
 * in either active or sleeping*)
let verify_core_entry (Ast.EVENT(name,param,body,_,att)) =
   let (id,att) = match param with
                    Ast.VARDECL(_,id,_,_,_,_,att) -> (id,att) in
   let new_field = Ast.mkFIELD(Ast.VAR(id,[],att), "target",[],-1) in
   let active_state = CS.ACTIVE in
   let sleeping_state= CS.SLEEPING in
   let cores =  (fun stmt -> match stmt with
                   Ast.MOVE(src,dest,_,_,_,_,_,_) -> (
                       Util.is_equal src new_field)
                   | _ -> false) in
   let bool_fn = ( fun stmt env ->
        match stmt with
            Some(Ast.MOVE(src,dest,_,_,_,_,_,_)) ->  (
                let type_target = Util.type_cstate dest in
                 match type_target with
                    None -> false
                  | Some type_target -> (
                      let act = (CS.cstate2class type_target) = active_state in
                      let sleep = (CS.cstate2class type_target)  = sleeping_state in (act||sleep)))
            | _ -> false
            ) in
     let reach_move = Util.extract_move_unique cores body in
     let reach_move = Util.evaluate_reachability reach_move in
     Util.check_all_paths reach_move bool_fn

(* Verifies that, for every process event, the set of transitions
 * is correct *)
let verify_process_transitions pevents =
    List.fold_left (fun accu ((Ast.EVENT(nm,_,_,_,_)) as x) ->
      match Automaton.verify_event x with
	  None -> accu
	| Some (move_req, edges, err) ->
          let msg = Printf.sprintf
	    "Error in event: %s. Incorrect transition %s" (Ast.event_name2c nm) err
	  in
          let msg = if move_req then
                      msg ^"\nTransition is mandatory on all paths."
                    else msg
          in
          let tr =
            "Valid transition(s) is/are:"::
            List.map (fun edge ->
                match edge with
                  Automaton.EDGE(Automaton.CLASS(src),Automaton.CLASS(dst), _) ->
                   let srcstr = CS.safe_class2c src
                   and dststr = CS.safe_class2c dst in
                   "Transition from "^ srcstr ^ " to " ^ dststr) edges
          in
          let msg = String.concat "\n" (msg::tr) in
          raise (Error.Error msg)
    ) true pevents

(* Wrapper function for entry/exit process
 * functions/events *)
let verify_entry phandlers chandlers ifunctions =
    let attach_fn = List.hd (List.filter (
            fun (Ast.FUNDEF(_,_,id,_,_,_,_)) ->
            if (B.id2c id = "attach") then true else false) ifunctions) in
    let detach_event = Util.lookup_event phandlers "detach" in
    let core_entry_event  = Util.lookup_event chandlers "core_entry" in
    if not (Verif_detach.verify_detach detach_event) then
      raise (Error.Error "Transition to TERMINATED in detach not reachable");
     if not (Verif_attach.verify_attach attach_fn) then
      raise (Error.Error "Transition to READY in attach not reachable");
    if not (verify_core_entry core_entry_event) then
      raise (Error.Error "Transition to ACTIVE/SLEEPING in core_entry not reachable")

(* -------------------------- entry point --------------------------- *)

(* At this point in the code,
 * we have the following guarantees:
     * Code is not mutually recursive
     * All functions/statements/events are correctly typed
     * All process variables are initialised in attach
     * All core variables have been initialised in core_init
     * All required core and process events have been defined, and there are no duplicate process/core events or variables
     * Transitions are correctly placed
 *)

let verify  (Ast.SCHEDULER(nm,
             cstdefs,enums,dom,grp,procdecls,fundecls,valdefs,domains,states,cstates,
		     criteria,trace,phandlers,chandlers,ifunctions,functions,
		     attr) as ast) =
  (* Initialise event structures *)
  Events.init_pevents_env();
  Events.init_cevents_env();

  (* Initialise state transitions *)
  Automaton.init_process_automaton();


  (* Verify that process automaton is consistent: that it contains
   * all the states/events *)
  Automaton.verify_consistent !Automaton.process_transitions true;

  let checkModIfStillHold = Verif_cload_assignments.user_modify_cload ast in
    if checkModIfStillHold = false then
      raise (Error.Error "cload modification by the developer is detected");


  let checkDeclarationProcess = Verif_initprocess.check_process_init ast in
    if (List.length checkDeclarationProcess > 0) then
      let def = "Please make sure to initialize all the process variables\n"
      in
        let errmsg =List.fold_left (fun def (Ast.VARDECL(_,id,_,_,_,_,attr)) ->
          def^"missing initialization for field "^(B.id2c id)^" "^(B.loc2c attr)^"\n") def checkDeclarationProcess;
        in
          raise (Error.Error errmsg)
    else ();


  (* TODO: do the same for cores *)

  (* Initialise function environment *)
  let all_functions = ifunctions@functions in
  let funtokeep =  List.map
      (fun (Ast.FUNDEF(_,_,nm,_,_,_,_) as fn) -> (nm,fn)) all_functions
  in
  let newenv =
    Util.updFctEnv !Util.verif_env funtokeep
  in
  Util.verif_env := newenv;
  (* (!Util.verif_env).functions := List.fold_left *)
  (*     (fun acc fn -> *)
  (* 	match fn with *)
  (* 	  Ast.FUNDEF(_,_,nm,_,_,_,_) -> (nm,fn)::acc *)
  (* 	| _ -> acc *)
  (*     ) [] all_functions ; *)

  (* Verify that attach, if returns true
   * or void, will place process in READY queue.
   * Verify that detach places processes. Verify that
   * core_entry places cores in ACTIVE/READY*)
  verify_entry phandlers chandlers ifunctions ;

  (* Schedule cannot manipulate shared variables *)
  (* verify_schedule phandlers ;*)

  (* verfy process transitions *)
  if not (verify_process_transitions phandlers) then
    raise (Error.Error " Transitions defined here do not follow transition automaton");
   (*verify_core_transitions chandlers; *)

  (* Verify appropriate locks are taken
   * FIXME Following subsequent discussions,
   * this may no longer up to date *)
   Lock_verif.analyze ast;
