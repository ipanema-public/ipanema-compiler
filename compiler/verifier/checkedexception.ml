(* Check that a core is set before it is used in a computation.
   Approximate control flow, ie nothing done about control flow from the bottom
of a loop to the top.*)

module B = Objects
module CS = Class_state

let error ln tested_vars str =
  Error.update_error();
  Printf.eprintf "%s: need to check %s using 'valid()' before attempting to %s\n"
    (B.loc2c ln) (String.concat ", " (List.map B.id2c tested_vars)) str

let warning ln str =
  Printf.printf "warning: %s: %s \n" (B.loc2c ln) str

let rec lookup x = function
    [] -> None
  | ((y,v)::r) -> if B.ideq(x,y) then Some v else lookup x r

let rec negate id = function
  [] -> []
  | (id2, isset)::xs ->
     if id == id2 then (id, not isset)::xs
     else (id2, isset)::(negate id xs)

let rec settrue id = function
    [] -> []
  | (id2, isset)::xs ->
     if id == id2 then (id, true)::xs
     else (id2, isset)::(settrue id xs)

let ok (var_env : (B.identifier*bool) list)
    (tested_vars : B.identifier list) =
  List.for_all
    (function(t)->
      List.exists
        (function((id,isset)) -> t == id && isset)
        var_env)
    tested_vars

let check_id ln var_env id =
  match lookup id var_env with
  | Some v -> (var_env, [id])
  | None ->
     raise (Error.Error (__LOC__ ^": "^ B.loc2c ln ^": identifier "^ B.id2c id ^" not found" ))
      (* (var_env, []) *)

let rec check_exp (var_env : (B.identifier*bool) list)
    (tested_vars : B.identifier list) = function
    Ast.VAR(id,_,ln) -> check_id ln var_env id
  | Ast.FIELD(exp,id,_,ln) ->
    (match check_exp var_env tested_vars exp with
       (var_env, [root_id]) ->
       let newid = B.ID((B.id2c root_id)^"." ^(B.id2c id),B.line ln) in
       check_id ln var_env newid
     | (_,[]) -> (var_env, [])
     | _ ->
	raise (Error.Error "Checkedexception: not expecting more than one identifier"))
  | Ast.EMPTY(id,sti,tv,insrc,ln) ->
    (var_env, tested_vars)
  | Ast.UNARY(_,exp,ln)
  | Ast.INDR(exp,ln)
  | Ast.FIRST(exp, _,_,ln) ->
    check_exp var_env tested_vars exp
  | Ast.VALID(exp, _, ln) as topexp ->
     (match check_exp var_env tested_vars exp with
      (var_env, [id]) ->
       (match lookup id var_env with
          Some true ->
          error ln [id] "already set, no need to test";
	  (var_env, [id]) (*FIXME custom error string*)
        | Some false -> (settrue id var_env, [id])
	| None -> error ln [id] "use unknown identifier";
		  (var_env, tested_vars))
      | (_, []) -> error ln [] "use unknown identifier";
		   Pp.pretty_print_exp topexp;
		   (var_env, tested_vars)
      | (_, _) -> warning ln "Unexpected expression for 'valid()'";
		  failwith "unexpected")
  | Ast.PRIM(fn,args,ln)->
    let tested_vars = List.fold_left
                         (function t -> function expr ->
                           let (_, tested_vars2) = (check_exp var_env t expr) in
                           tested_vars2)
                         tested_vars args in
    (var_env,tested_vars)
  | Ast.SUM ln
  | Ast.MIN ln
  | Ast.MAX ln
  | Ast.COUNT ln
  | Ast.FNOR ln
  | Ast.DISTANCE ln
  | Ast.INT(_,ln)
  | Ast.BOOL(_,ln)
  | Ast.SELECT(_,ln)
  | Ast.PARENT(ln)
  | Ast.SCHEDCHILD(_,_,ln)
  | Ast.SRCONSCHED(ln)
  | Ast.ALIVE(_,_,ln) ->
    (var_env, tested_vars)
  | Ast.IN(exp,st,sti,tv,insrc,_,ln) ->
    check_exp var_env tested_vars exp
  | Ast.PBINARY(bop,exp1,_,exp2,_,ln)
  | Ast.BINARY(bop,exp1,exp2,ln) ->
    let (var_env1, t1) = check_exp var_env tested_vars exp1 in
    let (var_env2, t2) = check_exp var_env1 tested_vars exp2 in
    (var_env2, Aux.union t1 t2)
  | Ast.AREF _
  | Ast.MOVEFWDEXP _ ->
      raise (Error.Error "Checkedexception: internal error")

let ok_one_exp errstr var_env exp ln =
    let (env1, tested_vars) = (check_exp var_env [] exp) in
    (if not (ok env1 tested_vars)
    then
      error ln tested_vars errstr);
    var_env

let intro_decl
    (var_env : (B.identifier*bool) list) = function
    Ast.VALDEF(Ast.VARDECL(B.CORE,id,_,_,_,_),_,_,_) ->
    (id, false)::var_env
    | _ -> var_env

let rec check_stm
    (var_env : (B.identifier*bool) list) = function
    Ast.IF(exp,st1,st2,ln) ->
    (match check_exp var_env [] exp with
       (var_env1, [tested_var]) ->
       let var_env2 = negate tested_var var_env1 in
       ignore(check_stm var_env1 st1);
       ignore(check_stm var_env2 st2);
       (var_env)
     | (var_env, []) ->
	ignore(check_stm var_env st1);
	ignore(check_stm var_env st2);
	(var_env)
     | _ -> error ln [] "You must test at most a single variable";
	    (var_env))
  | Ast.SEQ(decls,lstmt,ln) ->
    let var_env = List.fold_left intro_decl var_env decls in
    List.fold_left check_stm var_env lstmt
  | Ast.MOVE(src,dst,srcinfo,vsrcinfo,dstinfo,auto,state_end,ln) ->
    ok_one_exp "move" var_env src ln
  | Ast.RETURN(Some exp,ln) ->
    ok_one_exp "return" var_env exp ln
  | Ast.STEAL(exp,ln) ->
    ok_one_exp "steal_for" var_env exp ln
  | Ast.FOR(id,_,sts,_,stmt,crit,ln) ->
     check_stm var_env stmt (*FIXME extend env if id refers to a core*)
  | Ast.SWITCH(exp,lsc,st2,ln) ->
     ignore(ok_one_exp "switch condition" var_env exp ln);
    List.iter (function Ast.SEQ_CASE(ids, sti, stmt, attr) ->
        ignore(check_stm var_env stmt)) lsc;
    (match st2 with
       Some stmt -> check_stm var_env stmt;
     | None -> var_env)
  (*| Ast.FOR_WRAPPER(_,stmts,ln)
  | Ast.MOVEFWD(proc,srcdst,state_end,_)*)
  | Ast.ASSIGN(_,exp,_,ln) ->
    ok_one_exp "assign" var_env exp ln
  | Ast.RETURN(None,ln)
  | Ast.ERROR(_,ln)
  | Ast.BREAK(ln)
  | Ast.CONTINUE(ln)->
    var_env
 | Ast.ASSERT(exp,ln) ->
   ok_one_exp "assert" var_env exp ln
  | Ast.PRIMSTMT(fn,args,ln) ->
    let tested_vars = List.fold_left (function t -> function expr ->
        let (_, tested_vars2) = (check_exp var_env t expr) in
        tested_vars2)
      [] args in
    (if not (ok var_env tested_vars)
    then
      error ln tested_vars "primstmt");
    var_env
  | Ast.FOR_WRAPPER (_, loops, ln) ->
    ignore(List.map (check_stm var_env) loops);
    var_env
  | Ast.MOVEFWD (_, _, _, _)
  | Ast.SAFEMOVEFWD (_, _, _, _, _)
  | Ast.DEFER _ -> failwith "Checkedexception: Unexpected construct"

(* --------------------- Handlers and functions --------------------- *)

let process_handlers env handlers =
  List.iter
    (function
	(Ast.EVENT(nm,param,stmt,attr)) ->
	let id = (function Ast.VARDECL(_,id,_,_,_,_) -> id) param in
	let var_env =
	  ((B.ID("current", -1), true)::
	     (id,true)::
	       (B.ID((B.id2c id)^ ".source", -1), true)::
	       (B.ID((B.id2c id)^ ".target", -1), true)::
		 env) in
	ignore(check_stm var_env stmt))
        handlers

let process_functions env functions =
  List.iter
    (function
	Ast.FUNDEF(_,ret,nm,params,stmt,inl,attr) ->
	let locals = List.map (fun id -> (id, true)) (Ast.params2ids params) in
	(* FIXME: var_env / scope for functions *)
	ignore(check_stm (env@locals) stmt))
    functions

(* --------------------------- Entry point -------------------------- *)

let checkfirst typenv handlers chandlers functions =
  let env = List.fold_left
	      (fun res (id, (typ, _)) ->
	       match typ with
		 B.CORE -> if List.mem_assoc id res then
			     res
			   else(id, false)::res
	       | _ -> res
	      ) [] typenv
  in
  process_handlers env handlers;
  process_handlers env chandlers;
  process_functions [] functions;
