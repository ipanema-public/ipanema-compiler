(* $Id$ *)

(* The semantics of switch has changed.  Now if there are three BLOCKED states
and only one is mentioned specifically, then a subsequent case for BLOCKED
refers to the other two.  Referring to either of the remaining BLOCKED states
after the reference to BLOCKED, however, is an error. *)

module B = Objects
module CS = Class_state
module Event = Events
module Util = Verif_util

(* -------------------- Miscellaneous operations -------------------- *)

exception LookupErrException of string option
exception StmtErrException
exception ExpErrException of B.typ

let in_handler = ref false
let in_persistent_timer_handler = ref false
let move_allowed = ref false (* not allowed in VS attach or exported fns *)
let in_core_event = ref false (* move operates on process and core states *)

let pr = Printf.sprintf

let nl_error dloc str =
  (Error.update_error();
   if !Error.debug then
     Printf.eprintf "%s -- %s\n" dloc str
   else
     Printf.eprintf "%s\n" str)

let error dloc (attr,str) =
  (Error.update_error();
   if !Error.debug then
     Printf.eprintf "%s -- %s: %s\n" dloc (B.loc2c attr) str
   else
     Printf.eprintf "%s: %s\n" (B.loc2c attr) str)

let stmtErr dloc (attr,str) =
  error dloc (attr, str);
  raise StmtErrException

let expErr dloc (res,attr,str) =
  error dloc (attr, str);
  raise (ExpErrException res)

let expected_err dloc attr str = function
    Some(t) -> expErr dloc (t,attr,str)
  | None -> stmtErr dloc (attr,str)

let rec lookup x = function
    [] -> raise (LookupErrException None)
  | ((y,v)::r) -> if x = y then v else lookup x r

let rec lookup_id x = function
    [] -> raise (LookupErrException (Some(B.id2c x)))
  | ((y,v)::r) -> if B.ideq(x,y) then v else lookup_id x r

let rec lookup_str x = function
    [] -> raise (LookupErrException (Some x))
  | ((y,v)::r) -> if B.strideq(x,y) then v else lookup_str x r

let extend_env nm vl env = (nm,vl)::env

let rec id_member x = function
    [] -> false
  | (y::r) -> B.ideq(x,y) || id_member x r

(* ----------------------- User-declared types ---------------------- *)

(* boolean indicates whether the program can modify the fields of the structure
   (true implies that the program cannot) *)

let struct_env =
  ref ([] : (B.identifier * (bool * (B.identifier * B.typ) list)) list)

let init_struct_env _ =
  struct_env :=
    [(B.mkId("process_event"),
      (true,[(B.mkId("source"),B.SRCPROCESS);(B.mkId("target"),B.PROCESS)]));
     (B.mkId("core_event"),
      (true,[(B.mkId("source"),B.CORE);(B.mkId("target"),B.CORE)]));
     (B.mkId("task_struct"),
      (false,[(B.mkId("need_resched"),B.INT)]))]

let extend_struct_env info = (struct_env := info :: (!struct_env))

(* returns a new environment, reflecting the enum declarations, and an environment containing the maximum and minimum value for each type
* FIXME: why SchedState and Scheduler_state and two defs of SchedState?*)
let rec typedef_enum typedefs env constenv =
  let rr = (B.ENUM(B.mkId("SchedState")),true (* is const *)) in
  let env =
    extend_env (B.mkId("RUNNING")) rr
      (extend_env (B.mkId("READY")) rr
	 (extend_env (B.mkId("BLOCKED")) rr env)) in
  let rr = (B.ENUM(B.mkId("SchedState")),false) in
  let env = extend_env (B.mkId("Scheduler_state")) rr env in
  let rr = (B.ENUM(B.mkId("CoreState")),true (* is const *)) in
  let env =
    extend_env (B.mkId("ACTIVE")) rr
      (extend_env (B.mkId("IDLE")) rr
	 (extend_env (B.mkId("SLEEPING")) rr env)) in
  let rr = (B.ENUM(B.mkId("CoreState")),false) in
  let env = extend_env (B.mkId("Core_state")) rr env in
  let rr = (B.NULL,true) in
  let env = extend_env (B.mkId("NULL")) rr env in
  let (env,erenv) =
    List.fold_left
      (function (env,erenv) ->
	function
	    Ast.STRUCTDEF(id,decls,attr) ->
	      let idtys =
		List.map (function Ast.VARDECL(ty,id,imported,lz,_,de,attr) -> (id,ty))
		  decls in
	      extend_struct_env (id,(false,idtys));
	      (env,erenv)
	  | Ast.ENUMDEF(id,idlist,attr) ->
	      ((List.map (function x -> (x,(B.ENUM(id),true))) idlist) @ env,
	       (id, (0,(List.length idlist)-1 (* max value *))) :: erenv)
	  | Ast.ENUMVALDEF(id,idlist,attr) ->
	      ((List.map
		  (function (x,exp) ->
		    let _ = all_int_constant constenv attr exp in
		    (x,(B.ENUM(id),true)))
		  idlist)
	       @ env,
	       (id, (0,(List.length idlist)-1 (* max value *))) :: erenv)
	  | Ast.RANGEDEF(id,exp1,exp2,attr) ->
	      let v1 = all_int_constant constenv attr exp1
	      and v2 = all_int_constant constenv attr exp2 in
	      if v2 < v1
	      then error __LOC__ (attr,pr "range %s is empty" (B.id2c id));
	      (env, (id,(v1,v2)) :: erenv)
	  | Ast.TIMERDEF(id,decls,attr) ->
	      let idtys =
		List.map (function Ast.VARDECL(ty,id,imported,lz,_,de,attr) -> (id,ty))
		  decls in
	      extend_struct_env (id,(false,idtys));
	      (env,erenv))
      (env,[]) typedefs in
  (* combine enum-range env with constenv to allow key ordering criteria to
     refer to constant variables with the value 0 or 1 *)
  (env, erenv @ (List.map (function (id,vl) -> (id,(vl,vl))) constenv))

and all_int_constant env attr = function
    Ast.INT(n,attr) -> n
  | Ast.VAR(id,_,attr) ->
      (try lookup_id id env
      with LookupErrException _ ->
	(error __LOC__ (attr,
	       pr "unknown or non-constant identifier %s" (B.id2c id));
	 0))
  | Ast.UNARY(Ast.COMPLEMENT,exp,attr) ->
      (* not sure if the following is right *)
      lnot (all_int_constant env attr exp)
  | Ast.BINARY(bop,exp1,exp2,attr) ->
      (let v1 = all_int_constant env attr exp1
      and v2 = all_int_constant env attr exp2 in
      match bop with
	Ast.PLUS   -> v1 + v2
      | Ast.MINUS  -> v1 - v2
      | Ast.TIMES  -> v1 * v2
      | Ast.DIV    -> v1 / v2
      | Ast.MOD    -> v1 mod v2
      | Ast.BITAND -> v1 land v2
      | Ast.BITOR  -> v1 lor v2
      | Ast.LSHIFT -> v1 lsl v2
      | Ast.RSHIFT -> v1 lsr v2
      | _ ->
	  error __LOC__ (attr, "invalid operation in enum def or range bound");
	  0)
  | _ -> (error __LOC__ (attr,
		"invalid expression in enum def or range bound");
	  0)

(* ------------------ Information about structures ------------------ *)

(* Adds fields belonging to core struc to env *)
let add_corev_defn corev pstates =
  let idtys =
    List.map (function Ast.VARDECL(ty,id,imported,lz,_,de,attr) -> (id,ty))
      corev in
  extend_struct_env (B.mkId("core"), (false, idtys@pstates));
  List.map (fun (id,ty) -> (id,(ty,false))) idtys

(* Adds fields belonging to domain/group/process struct to env *)
let add_struct_defn name decls =
  let idtys =
    List.map (function Ast.VARDECL(ty,id,imported,lz,_,de,attr) -> (id,ty))
      decls in
   extend_struct_env (B.mkId(name), (false, idtys))

let get_fields struct_type =
  match struct_type with
    B.PEVENT -> lookup_str "process_event" (!struct_env)
  | B.CEVENT -> lookup_str "core_event" (!struct_env)
  | B.CORE -> lookup_str "core" (!struct_env)
  | B.DOMAIN -> lookup_str "domain" (!struct_env)
  | B.GROUP -> lookup_str "group" (!struct_env)
  | B.PROCESS -> lookup_str "process" (!struct_env)
  | B.SRCPROCESS -> lookup_str "process" (!struct_env)
  | B.STRUCT(nm) -> lookup_id nm (!struct_env)
  | B.TIMER(B.PERSISTENT(Some(nm))) -> lookup_id nm (!struct_env)
  | _ -> (false, []) (* empty list of field in RO mode *)

let type_structure_field struct_type field attr
    (src_valid : bool) (tgt_valid : B.testval) expected =
  let (isconst,fldenv) =
    (try
       match struct_type with
         B.PEVENT -> lookup_str "process_event" (!struct_env)
       | B.CEVENT -> lookup_str "core_event" (!struct_env)
       | B.CORE -> lookup_str "core" (!struct_env)
       | B.DOMAIN -> lookup_str "domain" (!struct_env)
       | B.GROUP -> lookup_str "group" (!struct_env)
       | B.PROCESS ->
         if !in_persistent_timer_handler && not (tgt_valid = B.TRUE)
         then expected_err __LOC__ attr "access to unchecked target" expected;
         lookup_str "process" (!struct_env)
       | B.SRCPROCESS ->
         if src_valid
         then
           (lookup_str "process" (!struct_env))
         else expected_err __LOC__  attr "access to field of unchecked source" expected
       | B.STRUCT(nm) -> lookup_id nm (!struct_env)
       | B.TIMER(B.PERSISTENT(Some(nm))) -> lookup_id nm (!struct_env)
       | _ -> let msg = pr "variable of type '%s': access to '%s'"
			   (B.type2c struct_type) (B.id2c field) in
	      expected_err __LOC__  attr (msg ^ " field of non-structure type") expected
     with LookupErrException _ ->
       expected_err __LOC__  attr ("unknown structure '"^ (B.type2c struct_type)
			  ^ "'") expected) in
  (try
     let fldty = lookup_id field fldenv in
     (fldty,isconst)
   with LookupErrException _ ->
     expected_err __LOC__  attr ("unknown field '"^(B.id2c(field))^"' in structure '"^(B.type2c struct_type)^"'") expected)

(* ------------------ Information about functions ------------------- *)

let fun_env =
  (ref [] : (B.identifier * (B.typ * B.typ list)) list ref)

let parent_env =
  (ref [] : (B.identifier * (B.typ * B.typ list)) list ref)

let all_fn_names = ref (List.map B.mkId (B.prim_names()))

let init_fun_env _ =
  fun_env :=
    (if !B.high_res
    then [(B.mkId "make_time",(B.TIME,[B.INT;B.INT]));
	   (B.mkId "make_cycle_time",(B.TIME,[B.INT;B.CYCLES]));
	   (B.mkId "make_cycles",(B.CYCLES,[B.INT]));
	   (B.mkId "time_to_seconds",(B.INT,[B.TIME]));
	   (B.mkId "time_to_nanoseconds",(B.INT,[B.TIME]));
	   (B.mkId "time_to_jiffies",(B.INT,[B.TIME]));
	   (B.mkId "time_to_subjiffies",(B.CYCLES,[B.TIME]))]
    else []) @
     ((B.mkId "print_trace_info",(B.VOID,[])) ::
     (B.mkId "now",((if (!B.high_res) then B.INDR(B.TIME) else B.TIME),[])) ::
     (B.mkId "time_to_ticks",(B.INT,[B.TIME])) ::
     (B.mkId "ticks_to_time",(B.TIME,[B.INT])) ::
     (B.mkId "time_to_ms",(B.INT,[B.TIME])) ::
     (B.mkId "ms_to_time",(B.TIME,[B.INT])) ::
     (B.mkId "start_absolute_timer",
      (B.VOID,[B.TIMER(B.NOT_PERSISTENT);B.TIME])) ::
     (B.mkId "start_relative_timer",
      (B.VOID,[B.TIMER(B.NOT_PERSISTENT);B.TIME])) ::
     (B.mkId "delete_bossa_timer",(*not in source language, added in pre_type*)
      (B.VOID,[B.TIMER(B.NOT_PERSISTENT)])) ::
     (B.mkId "stop_timer",(B.VOID,[B.TIMER(B.NOT_PERSISTENT)])) ::
     (B.mkId "get_user_int",(B.INT,[B.PORT])) ::
     (B.mkId "system_cores",(B.SET(B.CORE),[])) ::
     (B.mkId "steal_for_dom",(B.VOID,[B.CORE;B.DOMAIN])) ::
     (B.mkId "policy_metadata",(B.PROCESS,[B.INDR(B.STRUCT(B.mkId "task_struct"))])) ::
     match !B.config with
       B.LINUX45 -> [])

(* return type cannot contain process or event *)
let rec add_fundecls fun_env fundecls =
  let names =
    List.map
      (function
	  Ast.FUNDECL(tl,ret,nm,params,attr) ->
	    if Aux.member B.VOID params
	    then error __LOC__ (attr,"void not allowed as a parameter type");
	    if (not(valid_return_type ret))
	    then error __LOC__ (attr,
		       pr "invalid return type %s from function %s"
			 (B.type2c(ret)) (B.id2c(nm)));
	    if List.mem nm !all_fn_names then
	      error __LOC__ (attr, "duplicate function name declarations " ^ (B.id2c nm));
	    nm)
      fundecls in
  if Aux.is_set (names @ !all_fn_names)
  then
    begin
      all_fn_names := names @ !all_fn_names;
      fun_env :=
	(List.map
	   (function Ast.FUNDECL(tl,ret,nm,params,attr) -> (nm, (ret,params)))
	   fundecls) @ (!fun_env)
    end

and valid_return_type = function
    B.SCHEDULER -> false
  | B.PEVENT | B.CEVENT -> false
  | B.INDR(t) -> valid_return_type t
  | _ -> true

(* -------------------- Information about states -------------------- *)

let state_names = (ref [] : B.identifier list ref)
let prev_names = (ref [] : B.identifier list ref)
let cstate_names = (ref [] : B.identifier list ref)

(* ---------------------------- Collection ---------------------------- *)
let rec elem_typ coll = match coll with
    B.SYSTEM(x) -> elem_typ x
  | B.INDR(x) -> elem_typ x
  | B.QUEUE(x) -> x
  | B.SET(x) -> x
  | B.ENUM(B.ID("SchedState", _)) -> B.PROCESS
  | _ -> raise StmtErrException

(* ---------------------------- Coercion ---------------------------- *)

let coerce msg (actual:B.typ) (desired:B.typ) attr =
  match (actual,desired) with
    (B.INT,B.RANGE(_))
  | (B.RANGE(_),B.INT) -> ()
                        
  | (B.OPAQUE int,B.INT)
    | (B.INT, B.OPAQUE int) ->
     begin
       match int with
         "uint32_t" | "u64" -> ()
         | _ ->
            expErr __LOC__ (desired,attr,
		            "cannot coerce "^B.type2c(actual)^" to "^B.type2c(desired)^
		              " in "^msg)
     end

  | (B.INDR(B.TIME),B.TIME) -> ()
  | (B.ENUM(x),B.ENUM(y)) ->
      if not(B.ideq(x,y))
      then expErr __LOC__ (desired,attr,
		  "cannot coerce "^B.type2c(actual)^" to "^B.type2c(desired)^
		  " in "^msg)
  | (B.NULL,B.SYSTEM(_)) -> ()
  | (B.NULL,B.PORT) -> ()
  | (B.NULL,B.STRUCT(_)) -> ()
  | (B.NULL,B.CORE) -> ()
  | (B.NULL,B.DOMAIN) -> ()
  | (B.NULL,B.GROUP) -> ()
  | (B.TIMER(B.PERSISTENT(_)),B.TIMER(B.NOT_PERSISTENT)) -> ()
  | (B.TIMER(B.PERSISTENT(Some(_))),B.TIMER(B.PERSISTENT(None))) -> ()
  | (B.SET(_), B.SET(B.NULL)) -> ()
  | (B.QUEUE(_), B.SET(B.NULL)) -> ()
  | _ ->
      if not(actual = desired)
      then expErr __LOC__ (desired,attr,
		  "cannot coerce "^B.type2c(actual)^" to "^B.type2c(desired)^
		  " in "^msg)

let coerce_int msg (actual:B.typ) (desired:B.typ) attr : B.typ =
  match (actual,desired) with
    (B.INT, B.OPAQUE int) ->
     begin
       match int with
         "uint32_t" | "u64" -> desired
         | _ ->
            expErr __LOC__ (desired,attr,
		            "cannot coerce "^B.type2c(actual)^" to "^B.type2c(desired)^
		              " in "^msg)
     end
  | _ -> expErr __LOC__ (desired,attr,
		         "cannot coerce "^B.type2c(actual)^" to "^B.type2c(desired)^
		           " in "^msg)

(* --------------------------- Expressions -------------------------- *)

let rec type_exp
    (exp : Ast.expr)
    (env : (B.identifier * (B.typ * bool)) list)
    (expected : B.typ option)
    (src_valid : bool)
    (tgt_valid : B.testval) :
    B.typ * Ast.expr =
  try
    (match exp with
      Ast.SELF attr ->
	(check_expected src_valid tgt_valid expected B.CORE attr;
	 (B.CORE, Ast.SELF(B.updty attr B.CORE)))
    | Ast.INT(n,attr) ->
	(check_expected src_valid tgt_valid expected B.INT attr;
	 (B.INT, Ast.INT(n, B.updty attr B.INT)))

    | Ast.VAR(id,_,attr) ->
	(try
	  let (ty,_) = lookup_id id env in
	  (check_expected src_valid tgt_valid expected ty attr;
       (ty, Ast.VAR(id,[],B.updty attr ty)))
	with LookupErrException _ ->
	  expected_err __LOC__  attr ("unknown identifier "^(B.id2c(id))) expected)

    | Ast.FIELD(exp,fld,_,attr) ->
	(* isconst is defined at the level of the structure *)
	let (ty,exp1) = type_exp exp env None src_valid tgt_valid in
	let (fldty,_) = type_structure_field ty fld attr src_valid tgt_valid
	    expected in
	(check_expected src_valid tgt_valid expected fldty attr;
     (fldty,Ast.FIELD(exp1,fld,[],B.updty attr fldty)))

    | Ast.BOOL(v,attr) ->
	(check_expected src_valid tgt_valid expected B.BOOL attr;
	 (B.BOOL, Ast.BOOL(v,B.updty attr B.BOOL)))

    | Ast.PARENT(attr) -> failwith "not possible, must be used in a call"

    | Ast.UNARY(Ast.NOT,exp,attr) ->
	let (_,exp1) = type_exp exp env (Some(B.BOOL)) src_valid tgt_valid in
	(check_expected src_valid tgt_valid expected B.BOOL attr;
	 (B.BOOL, Ast.UNARY(Ast.NOT,exp1,B.updty attr B.BOOL)))

    | Ast.UNARY(Ast.COMPLEMENT,exp,attr) ->
	let (_,exp1) = type_exp exp env (Some(B.INT)) src_valid tgt_valid in
	(check_expected src_valid tgt_valid expected B.INT attr;
	 (B.INT, Ast.UNARY(Ast.COMPLEMENT,exp1,B.updty attr B.INT)))

    | Ast.BINARY(bop,exp1,exp2,attr) ->
	let (t1,exp1a) = type_exp exp1 env None src_valid tgt_valid
	and (t2,exp2a) = type_exp exp2 env None src_valid tgt_valid in
	let t1 = unrange (unsys (drop_indr_time attr (t1,!B.high_res)))
	and t2 = unrange (unsys (drop_indr_time attr (t2,!B.high_res))) in
	let check_set _ =
	  match t2 with
	    B.CORE | B.GROUP | B.DOMAIN ->
	      (let expected = [B.SET B.CORE;B.SET B.GROUP;B.SET B.DOMAIN]
	       in match bop with
	       Ast.PLUS -> Some (bin_check (t1,B.SET t2) attr expected "+")
	     | Ast.MINUS -> Some (bin_check (t1,B.SET t2) attr expected "-")
	     | _ ->
		expected_err __LOC__ attr "only set manipulation with +/- are allowed in this context."
		  (Some (B.SET t2)))
	  | _ -> None
	in
	let res =
	  (match bop with
	    Ast.PLUS ->
	      (match check_set () with
		None -> bin_check (t1,t2) attr [B.INT;B.TIME;B.CYCLES;B.OPAQUE("uint32_t")] "+"
	      | Some res -> res)
	  | Ast.MINUS ->
	     (match check_set () with
	       None -> bin_check (t1,t2) attr [B.INT;B.TIME;B.CYCLES;B.OPAQUE("uint32_t")] "-"
	     | Some res -> res)
	  | Ast.TIMES -> mul_time_int_op attr "*" (t1,t2)
	  | Ast.DIV -> div_time_int_op attr "/" (t1,t2)
	  | Ast.MOD -> div_time_int_op attr "%" (t1,t2)
	  | Ast.AND -> bin_check (t1,t2) attr [B.BOOL] "&&"
	  | Ast.OR -> bin_check (t1,t2) attr [B.BOOL] "||"
	  | Ast.BITAND -> bin_check (t1,t2) attr [B.INT] "&"
	  | Ast.BITOR -> bin_check (t1,t2) attr [B.INT] "|"
	  | Ast.EQ -> eq_check (t1,t2) attr "=="
	  | Ast.NEQ -> eq_check (t1,t2) attr "!="
	  | Ast.LT -> rel_check (t1,t2) attr "<"
	  | Ast.GT -> rel_check (t1,t2) attr ">"
	  | Ast.LEQ -> rel_check (t1,t2) attr "<="
	  | Ast.GEQ -> rel_check (t1,t2) attr ">="
	  | Ast.LSHIFT ->
	     bin_check_int (t1,t2) attr [B.CYCLES;B.TIME;B.INT;B.OPAQUE("uint32_t")] "<<"
	  | Ast.RSHIFT ->
	     bin_check_int (t1,t2) attr [B.CYCLES;B.TIME;B.INT;B.OPAQUE("uint32_t")] ">>")
	in (check_expected src_valid tgt_valid expected res attr;
	    (res, Ast.BINARY(bop,exp1a,exp2a,B.updty attr res)))

    | Ast.TERNARY(exp1, exp2, exp3, attr) ->
	let (_,exp1a) = type_exp exp1 env (Some B.BOOL) src_valid tgt_valid
	and (t1,exp2a) = type_exp exp2 env None src_valid tgt_valid
	and (t2,exp3a) = type_exp exp3 env None src_valid tgt_valid in
	if t1 = t2 then
	  (t1, Ast.TERNARY(exp1a, exp2a, exp3a, B.updty attr t1))
	else expected_err __LOC__ attr "secondary and ternary expression must have the same type." None

    | Ast.PBINARY(bop,exp1,states1,exp2,states2,attr) ->
	raise (Error.Error "internal error: pbinary unexpected")

    | Ast.INDR(exp,attr) ->
	(match type_exp exp env None src_valid tgt_valid with
	  (B.INDR(t),exp1) ->
	    (check_expected src_valid tgt_valid expected t attr;
	     (t, Ast.INDR(exp1,B.updty attr t)))
	| _ -> expected_err __LOC__  attr "invalid indirection expression" expected)

     (*FIXME: also handle cores*)
    | Ast.SELECT(_,attr) ->
	let ty = B.PROCESS in
	(check_expected src_valid tgt_valid expected ty attr;
	 (ty, Ast.SELECT(!CS.selected, B.updty attr ty)))
    | Ast.FIRST(exp, crit, _,attr) ->
       let (ty_coll,exp1) = type_exp exp env None src_valid tgt_valid in
       let ty1 = elem_typ ty_coll in
       let crit1 = match crit with
	   None -> None
	 | Some (Ast.CRIT_ID(key, order, id, fnid, attr)) ->
	    (try
	       let (ty, _) =
                 if B.id2c id = "" then
                   (B.DOMAIN, false)
                 else lookup_id id env
               in
	       Some (Ast.CRIT_ID(key, order, id, fnid, B.updty attr ty))
	     with LookupErrException _ ->
	       stmtErr __LOC__  (attr, ("unknown identifier "^(B.id2c(id))))
	    )
	 | Some (Ast.CRIT_EXPR(key, order, expr, fnid, attr)) ->
	    let fenv =
	      let (isconst, fields) = get_fields ty1 in
	      List.map(fun (id, ty) -> (id, (ty, isconst))) fields
	    in
	    let (ty, expr) = type_exp expr (fenv@env) None false B.UNK in
	    Some (Ast.CRIT_EXPR(key, order, expr, fnid, B.updty attr ty))
       in
       (check_expected src_valid tgt_valid expected ty1 attr;
	(ty1, Ast.FIRST(exp1, crit1, !CS.selected,  B.updty attr ty1)))

    | Ast.EMPTY(id,dum,vl,insrc,attr) ->
       (try
	  let concretized =
	    if !in_core_event then CS.concretize4coreprocess id
	    else CS.concretize id
	  in
	  (check_expected src_valid tgt_valid expected B.BOOL attr;
	   (B.BOOL, Ast.EMPTY(id,concretized,vl,insrc,B.updty attr B.BOOL)))
	with _ ->
	  let (ty,_) = lookup_id id env in
          let success =  match ty with
              B.SET _ | B.QUEUE _-> true
              | _ -> false
          in
          if success then
            (B.BOOL, Ast.EMPTY(id,dum, vl, insrc,B.updty attr ty))
	  else expErr __LOC__ (ty,attr,"expected set/queue or class name for empty, found "^B.type2c(ty))
       )
    | Ast.VALID(exp,_,attr) ->
      let (ty1,exp1) = type_exp exp env None src_valid tgt_valid in
	(check_expected src_valid tgt_valid expected B.BOOL attr;
	 (B.BOOL, Ast.VALID(exp1,!CS.selected,B.updty attr B.BOOL)))

    | Ast.PRIM((Ast.VAR(id,_,fn_attr) as fn),exps,attr)
    | Ast.PRIM((Ast.FIELD(Ast.PARENT(_),id,_,fn_attr) as fn),exps,attr) ->
	(try
	  let (ret,param_types) =
	    match fn with
	      Ast.VAR(_,_,_) -> lookup_id id (!fun_env)
	    | Ast.FIELD(_,_,_,_) -> lookup_id id (!parent_env)
	    | _ -> failwith "not possible" in
	  let exps1 =
	    List.map2
	      (function exp ->
		function ptype ->
		  let (ty,exp1) = type_exp exp env None src_valid tgt_valid in
		  coerce (pr "call to %s" (B.id2c id)) ty ptype attr;
		  exp1)
	      exps param_types in
	  let snm = B.id2c id in
	  if (snm = "start_absolute_timer" || snm = "start_relative_timer")
	      && !in_persistent_timer_handler && not (tgt_valid = B.TRUE)
	  then error __LOC__ (attr,
		     ("restarting timer is only valid when there is a "^
		      "target process"));
      (ret,Ast.PRIM(Ast.VAR(id,[],fn_attr),exps1,B.updty attr ret))
	with
	  LookupErrException _ ->
	    expected_err __LOC__  attr (pr "unknown function %s" (B.id2c(id))) expected
	| Invalid_argument str ->
	    expected_err __LOC__  attr
	      (pr "%s\nwrong number of arguments to %s" str (B.id2c(id)))
	      expected)

    | Ast.PRIM((Ast.COUNT fn_attr as fn),exps,attr) ->
	(try
	   let (ret,param_types) = (B.INT,
                                    List.map (function a -> B.SET(B.NULL)) exps) in
	  let exps1 =
	    List.map2
	      (function exp ->
		function ptype ->
		  let (ty,exp1) = type_exp exp env None src_valid tgt_valid in
		  coerce (pr "call to %s" (Ast.exp2c fn)) ty ptype attr;
		  exp1)
	      exps param_types in
	  (ret,Ast.PRIM(Ast.COUNT fn_attr,exps1,B.updty attr ret))
	with
	  Invalid_argument str ->
	    expected_err __LOC__  attr
	      (pr "%s\nwrong number of arguments to %s" str (Ast.exp2c(fn)))
	      expected)

    | Ast.PRIM((Ast.FNOR fn_attr as fn),exps,attr) ->
       (* Printf.printf "detected Ast.PRIM(%s) in type.ml at %s\n" (Ast.exp2c fn) (B.loc2c attr); *)
        (try
          let (ret,param_types) = (B.BOOL,[B.SET(B.NULL);B.BOOL]) in
          let exps1 =
            List.map2
              (function exp ->
                function ptype ->
                  let (ty,exp1) = type_exp exp env None src_valid tgt_valid in
                  coerce (pr "call to %s" (Ast.exp2c fn)) ty ptype attr;
                  exp1)
              exps param_types in
          (ret,Ast.PRIM(Ast.FNOR fn_attr,exps1,B.updty attr ret))
        with
          Invalid_argument str ->
            expected_err __LOC__  attr
              (pr "%s\nwrong number of arguments to %s" str (Ast.exp2c(fn)))
              expected)

    | Ast.PRIM(intfn,exps,attr) ->
       (* Printf.printf "detected Ast.PRIM(%s) in type.ml at %s\n" (Ast.exp2c intfn) (B.loc2c attr); *)
       (let (intfnname, ret,param_types) =
	  (* FIXME: Could/Should we use fun_env ? *)
	  match intfn with
	    Ast.DISTANCE _ -> (Ast.exp2c(intfn), B.INT, [B.INT])
	  | Ast.SUM _ | Ast.MIN _ | Ast.MAX _ (* | Ast.FNOR _ *)
	    -> begin
		match expected with
		    None ->
                    (* Printf.printf "int\n"; *)
		      let tys = List.fold_left
			(fun acc _ -> B.INT::acc)
			[]
			exps
		      in
                    (Ast.exp2c(intfn), B.INT, tys)
		  | Some ty ->
		    match ty with
		      B.INT | B.BOOL | B.TIME | B.CYCLES ->
                               (* Printf.printf "%s\n" (B.type2c ty); *)
			let tys = List.fold_left
			  (fun acc _ -> ty::acc)
			  []
			  exps
			in
			(Ast.exp2c(intfn), ty, tys)
		      | _ ->
		   expected_err __LOC__  attr (pr "unexpected type with function %s" (Ast.exp2c(intfn))) expected
		  end
	  | _ -> Pp.pretty_print_exp intfn;
	    expected_err __LOC__  attr (pr "unknown non-id function:") expected
	in
	try
	  let exps1 =
	    List.map2
	      (function exp ->
			function ptype ->
		                 let (ty,exp1) = type_int_fn_exp exp env None src_valid tgt_valid in
                                 (* Printf.printf "ty=%s;ptype=%s\n" (B.type2c ty) (B.type2c ptype); *)
				 coerce (pr "call to %s" intfnname) ty ptype attr;
				 exp1)
	      exps param_types in
	  (ret,Ast.PRIM(intfn, exps1, B.updty attr ret))
	with Invalid_argument str ->
	  expected_err __LOC__  attr
		       (pr "%s\nwrong number of arguments to %s" str intfnname)
		       expected)

    | Ast.SCHEDCHILD(exp,procs,attr) ->
	if !in_handler
	then
	  let (ty,exp1) = type_exp exp env (Some(B.PROCESS)) src_valid tgt_valid in
	  (B.SCHEDULER,Ast.SCHEDCHILD(exp1,procs,B.updty attr B.SCHEDULER))
	else expErr __LOC__ (B.SCHEDULER,attr,"next only allowed in handlers")

    | Ast.SRCONSCHED(attr) ->
	if !in_handler
	then (B.BOOL, Ast.SRCONSCHED(B.updty attr B.BOOL))
	else expErr __LOC__ (B.BOOL,attr,"srcOnSched only allowed in handlers")
    | Ast.AREF(_,_,attr) -> raise (Error.Error "type: internal error")
    | Ast.IN(exp,locexp,_,vl,insrc,crit,attr) ->
       let (t2,locexp1) = type_exp locexp env None src_valid tgt_valid in
       let expty = Some (
	 match t2 with
	   B.SET t1 -> t1
	 | B.QUEUE t1 -> t1
	 | _ ->
	    expErr __LOC__ (t2,attr,"expected set/queue, found "^B.type2c(t2))
       ) in
       let (t1,exp1) = type_exp exp env expty src_valid tgt_valid in
	let id = match locexp with
	    Ast.VAR(id, _, _) | Ast.FIELD(_, id, _, _) -> id
	    | _ -> failwith "Expected location expression" in
	(check_expected src_valid tgt_valid expected B.BOOL attr;
	 match t1 with
	   B.PROCESS | B.SRCPROCESS ->
	      (B.BOOL,
	       Ast.IN(exp1,locexp1,CS.concretize id,vl,insrc,crit,B.updty attr B.BOOL))
	   | B.CORE ->
	      let sti = try
		  CS.concretize4core id
		with e ->
		  (* error (attr, Printexc.to_string e); *)
		  [CS.CSTATE(id,CS.UNK)]
	      in
	      (B.BOOL,
	       Ast.IN(exp1,locexp1,sti,vl,insrc,crit,B.updty attr B.BOOL))
	   | _ -> expErr __LOC__ (B.BOOL,attr,"invalid argument to in"))

    | Ast.MOVEFWDEXP(_,_,_) -> raise (Error.Error "type: internal error")
    | _ ->
       begin
	 Pp.pretty_print_exp exp;
	 failwith "Unexpected expression: "
       end
    )
  with ExpErrException x -> (x,exp)


and err res attr op expected a1 a2 =
  expErr __LOC__ (res,attr,
	 pr "invalid arguments to %s:\n  expected %s, found (%s,%s)"
	   op expected (B.type2c(a1)) (B.type2c(a2)))

and mul_time_int_op attr op = function
    (B.INT,B.TIME) -> B.TIME
  | x -> bin_check_int x attr [B.TIME;B.INT;B.OPAQUE "uint32_t"] op

and div_time_int_op attr op = function
    (B.TIME,B.TIME) -> B.INT
  | (B.CYCLES,B.CYCLES) -> B.INT
  | x -> bin_check_int x attr [B.CYCLES;B.TIME;B.INT;B.OPAQUE "uint32_t"] op

and drop_indr_time attr = function
    (B.INDR(B.TIME),true) -> B.TIME
  | (B.INDR(B.TIME),false) -> expErr __LOC__ (B.TIME,attr,"invalid type time*")
  | (t,_) -> t

and bin_check (a1,a2) attr ts op =
  let (a1, a2) =
    if a1 = a2
    then (a1, a2)
    else if a1 = B.INT
    then (coerce_int (pr "invalid arguments to %s" op) a1 a2 attr, a2)
    else if a2 = B.INT
    then (a1, coerce_int (pr "invalid arguments to %s" op) a2 a1 attr)
    else (a1, a2)
  in
  match List.filter (function t -> a1 = t && a2 = t) ts with
    [res] -> res
  | _ ->
     let choose_res =
	try List.find (function t -> a1 = t) ts with Not_found ->
	  try List.find (function t -> a2 = t) ts with Not_found ->
	    List.hd ts in
      let type_strings =
	List.map (function t -> pr "(%s,%s)" (B.type2c(t)) (B.type2c(t))) ts in
      expErr __LOC__ (choose_res,attr,
	     pr "invalid arguments to %s:\n  expected %s, found (%s,%s)"
	       op (Aux.set2c type_strings) (B.type2c(a1)) (B.type2c(a2)))

and bin_check_int (a1,a2) attr ts op =
  match List.filter (function t -> a1 = t && a2 = B.INT) ts with
    [res] -> res
  | _ ->
      let type_strings =
	List.map (function t -> pr "(%s,int)" (B.type2c(t))) ts in
      expErr __LOC__ (a1,attr,
	     pr "invalid arguments to %s:\n  expected %s, found (%s,%s)"
	       op (Aux.set2c type_strings) (B.type2c(a1)) (B.type2c(a2)))

and eq_check args attr op =
  match args with
    (B.ENUM(x),B.ENUM(y)) ->
      if (B.ideq(x,y))
      then B.BOOL
      else bop_error B.BOOL attr op args
  | (B.STRUCT(x),B.STRUCT(y)) ->
      if (B.ideq(x,y))
      then B.BOOL
      else bop_error B.BOOL attr op args
  | (B.STRUCT(_),B.NULL) | (B.NULL,B.STRUCT(_))
  | (B.PORT,B.NULL) | (B.NULL,B.PORT)
  | (B.INT,B.INT) | (B.CORE, B.CORE)
  | (B.BOOL,B.BOOL) | (B.TIME,B.TIME) | (B.PROCESS,B.PROCESS)
  | (B.SCHEDULER,B.SCHEDULER) -> B.BOOL
  | (_,_) -> bop_error B.BOOL attr op args

and rel_check args attr op =
  match args with
    (B.INT,B.INT) -> B.BOOL
  | (B.TIME,B.TIME) -> B.BOOL
  | (B.CYCLES,B.CYCLES) -> B.BOOL
  | (B.PROCESS,B.PROCESS) -> B.BOOL
  | (B.SRCPROCESS,B.PROCESS) -> B.BOOL
  | (B.PROCESS,B.SRCPROCESS) -> B.BOOL
  | (B.SRCPROCESS,B.SRCPROCESS) -> B.BOOL
  | (B.SCHEDULER,B.SCHEDULER) -> B.BOOL
  | (B.ENUM(x),B.ENUM(y)) ->
      if (B.ideq(x,y))
      then B.BOOL
      else bop_error B.BOOL attr op args
  | (_,_) -> bop_error B.BOOL attr op args

and bop_error res attr op (arg1,arg2) =
  expErr __LOC__ (res,attr,
	 pr "invalid arguments to %s: (%s,%s)"
	   op (B.type2c(arg1)) (B.type2c(arg2)))

and check_expected src_valid tgt_valid expected res attr =
  match expected with
    None -> ()
  | Some(t) ->
      match(t,unrange (unsys res)) with
	(B.ENUM(x),B.ENUM(y)) ->
	  if not(B.ideq(x,y))
	  then expErr __LOC__ (t,attr,"expected "^B.type2c(t)^" found "^B.type2c(res))
      |	(B.PROCESS,B.SRCPROCESS) ->
	  if not src_valid
	  then expErr __LOC__ (t,attr,"source process not valid")
      | (B.NULL,B.SYSTEM(_)) -> ()
      | (B.CORE, B.NULL) -> ()
      |	(a,b) ->
	  if not(a = b)
	  then expErr __LOC__ (t,attr,"expected "^B.type2c(t)^" found "^B.type2c(res))

and type_pstate_exp exp =
  match exp with
    Ast.VAR(nm,_,attr) ->
      (try Some(lookup_id nm (!CS.state_env))
       with LookupErrException _ -> None)
  | Ast.FIELD(_,fld,_,attr) ->
     (try Some(lookup_id fld (!CS.state_env))
       with LookupErrException _ -> None)
  | _ -> None

and type_cstate_exp env exp : CS.state_info option =
  match exp with
    Ast.VAR(nm,_,attr) ->
      (try Some(lookup_id nm (!CS.cstate_env))
       with LookupErrException _ -> None)
    | Ast.FIELD(Ast.VAR(nm,_,_),fld,_,attr) ->
      (try
	 let (ty,_) = lookup_id nm env in
	 match ty with
	     B.CEVENT ->
	       if B.id2c fld = "target" then
		 Some (CSTATE (fld, CS.UNK))
	       else None
	   | _ ->
	     try Some(lookup_id fld (!CS.cstate_env))
	     with LookupErrException _ -> None
       with LookupErrException _ -> None
      )
  | _ -> None

and type_int_fn_exp (* For internal function that works on sets and queues *)
      (exp : Ast.expr)
      (env : (B.identifier * (B.typ * bool)) list)
      (expected : B.typ option)
      (src_valid : bool)
      (tgt_valid : B.testval) :
      B.typ * Ast.expr =
  try
    (match exp with
      Ast.FIELD(exp,fld,_,attr) ->
    (* isconst is defined at the level of the structure *)
       let (ty,exp1) =
	 match type_exp exp env None src_valid tgt_valid with
	   (B.QUEUE ty, exp1) -> (ty, exp1)
	 | (B.SET ty, exp1) -> (ty, exp1)
	(* | _ -> let msg = pr "structure of type '%s': " (B.type2c struct_type) in
	   expected_err __LOC__  attr (msg ^ "field of non-structure type") expected *)
	 | (B.PROCESS, exp1) as r -> r (* Upgrade RUNNING to a set *)
	 | _ -> expected_err __LOC__  attr "queue/set expected" None
       in
       let (fldty,_) =
	 type_structure_field ty fld attr src_valid tgt_valid expected
       in
    (check_expected src_valid tgt_valid expected fldty attr;
     (fldty,Ast.FIELD(exp1,fld,[],B.updty attr fldty)))
    | Ast.VAR(nm, _, attr) ->
       match type_exp exp env None src_valid tgt_valid with
	 (B.QUEUE ty, exp1) -> (ty, exp1)
       | (B.SET ty, exp1) -> (ty, exp1)
	(* | _ -> let msg = pr "structure of type '%s': " (B.type2c struct_type) in
	   expected_err __LOC__  attr (msg ^ "field of non-structure type") expected *)
       | _ -> expected_err __LOC__  attr "queue/set expected" None
    | _ -> failwith ("internal function at "^ string_of_int (B.line (Ast.get_exp_attr exp))))
  with ExpErrException x -> (x,exp)

and unrange = function
    B.RANGE(_) -> B.INT
  | t -> t

and unsys = function
    B.SYSTEM(t) -> t
  | t -> t

(* -------------------------- Declarations ------------------------- *)

let type_decl (Ast.VARDECL(ty,id,imported,lz,_,de,attr)) = (id,(ty,false))

let type_decl_wupd env decl =
  match decl with
      Ast.VARDECL(ty,id,imported,lz,_,None,attr) -> decl
    | Ast.VARDECL(ty,id,imported,lz,sh,Some de,attr) ->
      let (_, de_ty) = type_exp de env (Some ty) false B.FALSE in
      (Ast.VARDECL(ty,id,imported,lz,sh,Some de_ty,B.updty attr ty))

let rec type_def d env src_valid tgt_valid =
  match d with
    Ast.VALDEF(decl,exp,isconst,attr) ->
    let (id,(ty,_)) = type_decl decl in
    let (t1,exp1) = type_exp exp env None src_valid tgt_valid in
    (match (ty,isconst) with
       (B.TIMER(_),Ast.CONST) -> ()
     |	(B.TIMER(_),_) ->
       error __LOC__ (attr, "timer typed declaration must be constant")
     |	_ -> ());
    (coerce (pr "initialization of %s" (B.id2c id)) t1 ty attr;
     ((id,(ty,isconst=Ast.CONST)),
      Ast.VALDEF(decl,exp1,isconst,B.updty attr ty)))
  | Ast.UNINITDEF(decl,attr) ->
    let (id,(ty,_)) = type_decl decl in
    ((id,(ty,false)),Ast.UNINITDEF(decl,B.updty attr ty))
  | Ast.SYSDEF(decl,isconst,attr) ->
    let (id,(ty,_)) = type_decl decl in
    (match (ty,isconst) with
       (B.TIMER(_),Ast.CONST) -> ()
     |	(B.TIMER(_),_) ->
       error __LOC__ (attr, "timer typed declaration must be constant")
     |	_ -> ());
    ((id,(ty,isconst=Ast.CONST)),Ast.SYSDEF(decl,isconst,B.updty attr ty))
  | Ast.DUMMYDEF(id,exp,attr) ->
    let (t1,exp1) = type_exp exp env None src_valid tgt_valid in
    ((id,(t1,true)),
     Ast.VALDEF(Ast.VARDECL(t1,id,false,false,CS.UNSHARED, None,attr),exp1,
                Ast.CONST,B.updty attr t1))

let type_defs defs env src_valid tgt_valid =
  let (new_env,new_defs) =
    List.fold_left
      (function (new_env,new_defs) ->
	function x ->
	  let (env_elem, def_elem) = type_def x new_env src_valid tgt_valid in
	  (env_elem :: new_env, def_elem :: new_defs))
      (env,[]) defs in
  (List.rev new_env,List.rev new_defs)

(* the following assumes that the environment is always extended with
   extend_env, which just conses new things onto the front, making an
   environment usable as a record of all the names that have been declared *)
let rec check_globals = function
    [] -> ()
  | (x,_)::rest ->
      (try let _ = List.assoc x rest in
      nl_error __LOC__ (pr "variable %s appears more than once among global variables, enumerated types, and state names" (B.id2c x))
      with Not_found -> ());
      check_globals rest

(* ----------- Extra treatment for constant declarations ----------- *)

(* Filters out all integer constants *)
let constant_values defs =
  List.fold_left
    (function const_env ->
      function
	  Ast.VALDEF(Ast.VARDECL(B.INT,id,imported,lz,_,de,attr),exp,Ast.DEFCONST,_) ->
	    (id,all_int_constant const_env attr exp) :: const_env
	| Ast.VALDEF(Ast.VARDECL(_,id,imported,lz,_,de,attr),exp,Ast.DEFCONST,_) ->
	   const_env
	| Ast.SYSDEF(Ast.VARDECL(B.INT,id,imported,lz,_,de,attr),Ast.CONST,_) ->
	   const_env
	| _ -> nl_error __LOC__ ("unexpected non-constant declaration"); const_env)
    [] defs

(* --------------------------- Statements -------------------------- *)
let rec type_for_process id state_ids_opt dir stmt crit attr env ret src_valid tgt_valid =
  let state_info =
    match state_ids_opt with
	Some(stids) -> List.map (fun exp ->
	  match exp with
	      Ast.VAR(id,_, _)  -> (exp, Some (List.hd(CS.concretize id)))
	    | Ast.FIELD(parent, field,_, _) ->
	      try
		ignore(CS.id2class field);
	       	(exp, Some (List.hd(CS.concretize field)))
	      with Error.Error _ ->
		(exp, None)
		| _ -> failwith "Bad foreach list"
	) stids
      | None ->
	List.map (fun st ->
	  (Ast.VAR(B.mkId(CS.state2c st),[],attr),Some st))
	  (CS.all_states())
  in
  let new_env =
    extend_env id (B.PROCESS,false) env in
  let (stmt1,cb) = type_stmt stmt new_env ret src_valid tgt_valid true in
	  (* duplicate code for each state *)
  let loops =
    List.map
      (function
      (exp, info_opt) ->
	match info_opt with
	    None -> Ast.FOR(id,state_ids_opt,[],dir,stmt,crit,attr)
	  | Some (CS.STATE(state_id,_,CS.PROC,_) as info) ->
	    let pattr = B.updty attr B.PROCESS in
	    let expr = match exp with
		Ast.VAR(_,_,_) -> Ast.VAR(state_id,[],pattr)
	      | Ast.FIELD(nm, _,_,_) -> Ast.FIELD(nm, state_id, [], pattr)
	      | _ -> stmtErr __LOC__  (attr, "Unexpected location expression")
	    in
	    let vardecl =
	      Ast.VARDECL(B.PROCESS,
			  id,false,false,CS.UNSHARED,None,pattr) in
	    Ast.IF(Ast.EMPTY(state_id,[info],B.UNK,true,
			     B.updtst (B.updty attr B.BOOL)
			       (Ast.mk_test_index())),
		   Ast.SEQ([],[],attr),
		   Ast.SEQ([Ast.VALDEF(vardecl,expr,Ast.VARIABLE,attr)],
			   [stmt1],attr),
		   attr)
	  | Some(CS.STATE(state_id,_,CS.QUEUE(_),_) as info)->
	    let expr = match exp with
		Ast.VAR(_,_,_) -> Ast.VAR(state_id,[],B.mkAttr(-1))
	      | Ast.FIELD(nm, _,_,_) ->
		Ast.FIELD(nm, state_id, [], B.mkAttr(-1))
	      | _ -> stmtErr __LOC__  (attr, "Unexpected expression")
	    in
            Ast.FOR(id,Some [expr],[info],dir,stmt1,crit,attr)
	  | Some(CS.CSTATE _) -> failwith "Unsupported")
      state_info in
  if cb
  then (Ast.FOR_WRAPPER(B.fresh_idx "break",loops,attr), false)
  else (Ast.SEQ([],loops,attr),false)

and type_for_other id type_ state_ids dir stmt crit attr env ret src_valid tgt_valid =
  let new_env = extend_env id (type_,false) env in
  let (stmt1,cb) = type_stmt stmt new_env ret src_valid tgt_valid true in
  (* TODO: Use FORWRAPPER is cb *)
  (Ast.FOR(id,Some state_ids,[],dir,stmt1,crit,attr), false)

and type_stmt
    (stmt : Ast.stmt)
    (env : (B.identifier * (B.typ * bool)) list)
    (ret : B.typ option)
    (src_valid : bool) (* *)
    (tgt_valid : B.testval) (* *)
    (break_valid : bool)
    : Ast.stmt * bool (* contains break *) =
  try
    (match stmt with
      Ast.IF(exp,stmt1,stmt2,attr) ->
	let (_,exp1) = type_exp exp env (Some(B.BOOL)) src_valid tgt_valid in
	let new_src_valid =
	  (match exp with Ast.SRCONSCHED(attr) -> true | _ -> false) in
	let (true_tgt_valid,false_tgt_valid) =
	  (match exp with
	    Ast.ALIVE(_,_,_) -> (B.TRUE,B.FALSE)
	  | _ -> (tgt_valid,tgt_valid)) in
	let (stmt1a,cb1) =
	  type_stmt stmt1 env ret (src_valid || new_src_valid)
	    true_tgt_valid break_valid in
	let (stmt2a,cb2) =
	  type_stmt stmt2 env ret (not new_src_valid && src_valid)
	    false_tgt_valid break_valid in
	(Ast.IF(exp1,stmt1a,stmt2a,attr),cb1||cb2)

    | Ast.FOR_WRAPPER(label,stmts,attr) -> raise (Error.Error "cannot occur")

    | Ast.FOR(id,state_ids_opt,_,dir,stmt,crit,attr) as for_stmt ->
	(* state_id can be either a state or a class or missing, and can
	   refer to process variables or queues *)
       begin
         let (typed_states, state_ids) =
	   List.split (
	       match state_ids_opt with
		 None ->
		  List.map (fun st ->
		      (B.PROCESS, (Ast.VAR(B.mkId(CS.state2c st),[],attr))))
		    (CS.all_states())
	       | Some state_ids ->
		    List.map
		      (fun expr -> type_exp expr env None src_valid tgt_valid)
		      state_ids
	     )
	 in
	 let type_ =
           List.fold_left (fun rty ty ->
	       match rty with
	         None -> Some ty
	       | Some prev_ty ->
	          if ty = prev_ty then
		    rty
	          else
		    (error __LOC__ (attr, "the types of the elements in state list are different !");
		     rty)
	     ) None typed_states
         in
	 match type_ with
	   Some B.PROCESS ->
	    type_for_process id state_ids_opt dir stmt crit attr env ret src_valid tgt_valid
	 | Some (B.QUEUE ty) | Some (B.SET ty) ->
	    type_for_other id ty state_ids dir stmt crit attr env ret src_valid tgt_valid
	 | Some ty -> stmtErr __LOC__ (attr, ("Expecting queue or set: " ^
						"type "^B.type2c ty ^" is not iterable."))
	 | None -> stmtErr __LOC__ (attr, ("Unable to typecheck identifier " ^ B.id2c id))
       end

    | Ast.SWITCH(exp,cases,default,attr) ->
      let (_,exp1) = type_exp exp env (Some(B.PROCESS)) src_valid tgt_valid in
      let cases_info = check_cases cases attr in
      let (new_cases,cbs) =
	List.split
	  (List.map2
	     (function Ast.SEQ_CASE(pat,_,stmt,attr) ->
	       function case_info ->
		 let (new_stmt,cb) =
		   type_stmt stmt env ret src_valid tgt_valid break_valid in
		 (Ast.SEQ_CASE(pat,case_info,new_stmt,attr), cb))
	     cases cases_info) in
      let (new_default,cb) =
	(match default with
	    None -> (None,false)
	  | Some x ->
	    let (new_default,cb) =
	      type_stmt x env ret src_valid tgt_valid break_valid in
	    ((Some new_default),cb)) in
      (Ast.SWITCH(exp1,new_cases,new_default,attr),
       List.exists (function x -> x) (cb::cbs))

    | Ast.SEQ(decls,stmts,attr) ->
	let (new_env,new_decls) = type_defs decls env src_valid tgt_valid in
	let (new_stmts,cb) =
	  List.fold_left
	    (function (new_stmts,cbs) ->
	      function stmt ->
		let (new_stmt,cb) =
		  type_stmt stmt new_env ret src_valid tgt_valid break_valid in
		(new_stmt :: new_stmts, cb||cbs))
	    ([],false) stmts in
	(Ast.SEQ(new_decls,List.rev new_stmts,attr), cb)

    | Ast.RETURN(exp,attr) ->
	(match (ret,exp) with
	  (Some(B.VOID), Some(e)) ->
	    stmtErr __LOC__ (attr,
		    "no return expression allowed in void-typed function")
	| (Some(t), Some(e)) ->
	    let (ty,e1) = type_exp e env ret src_valid tgt_valid in
	    (coerce "return" ty t attr;
	     (Ast.RETURN(Some(e1),attr),false))
	| (None,Some(e)) ->
	    let (ty,e1) =
	      type_exp e env (Some(B.ENUM(B.mkId("SchedState"))))
		src_valid tgt_valid in
	    (Ast.RETURN(Some(e1),attr),false)
	| (Some(B.VOID),None) -> (Ast.RETURN(exp,attr),false)
	| (Some(t),None) ->
	    stmtErr __LOC__ (attr,pr "missing return expression %s" (B.type2c t))
	| (None,None) -> (Ast.RETURN(exp,attr),false))

    | Ast.STEAL(exp,attr) ->
	let (_,exp1) = type_exp exp env (Some(B.CORE)) src_valid tgt_valid in
	(Ast.STEAL(exp1,attr),false)

    | Ast.MOVE(exp,state_exp,_,_,_,auto_allowed,state_end,attr) ->
       if not !in_core_event then
	   (try
	       let ((st_exp_ty,is_cst), st_exp1) =
		 type_loc env src_valid tgt_valid state_exp in
           (match type_pstate_exp st_exp1 with
              Some (CS.STATE(_,cldst,state_type,_) as dst) ->
            (match state_end with
               Ast.HEAD
             | Ast.TAIL ->
               (match state_type with
                  CS.QUEUE(CS.SELECT(CS.LIFODEFAULT))
                |	CS.QUEUE(CS.SELECT(CS.FIFODEFAULT)) -> ()
                |	_ -> stmtErr __LOC__ (attr,"invalid head or tail reference"))
             | Ast.EITHER -> ());
            if !move_allowed
            then
              let (_,exp1) =
                type_exp exp env (Some(B.PROCESS))
                  src_valid tgt_valid in
              (match type_pstate_exp exp with
                 Some((CS.STATE(srcid,clsrc,_,_) as src)) ->
		 let state = (match type_pstate_exp state_exp with
				Some(CS.STATE(stateid,_,_,_)) -> B.id2c stateid
			      | _ -> "")
		 in
                 if (B.id2c srcid) = state
                 then stmtErr __LOC__ (attr,
                              pr "invalid move on process state: %s => %s"
                                (B.id2c srcid) state)
                 else (Ast.MOVE(exp1,st_exp1,Some src,[src],Some dst,
                                auto_allowed,state_end,attr),
                       false)
               | None -> (Ast.MOVE(exp1,st_exp1,None,[],Some dst,
                                   auto_allowed,state_end,attr),
                          false))
            else stmtErr __LOC__ (attr,"move not allowed")
	    | None -> stmtErr __LOC__ (attr,"invalid process state")
         )
	   with LookupErrException _ ->
             stmtErr __LOC__  (attr,"destination must be a process state")
	   )
       else
	   (try
	       let ((st_exp_ty,is_cst), st_exp1) =
		 type_loc env src_valid tgt_valid state_exp in
           (match type_cstate_exp env st_exp1 with
          | Some (CS.CSTATE(_,ctyp) as dst) ->
            if !move_allowed
            then
              let (_,exp1) = type_exp exp env (Some(B.CORE)) src_valid tgt_valid in
              (match type_cstate_exp env exp with
                 Some((CS.CSTATE(srcid,clsrc) as src)) ->
		 let state = (match type_cstate_exp env state_exp with
				Some(CS.CSTATE(stateid,_)) -> B.id2c stateid
			      | _ -> "")
		 in
                 if (B.id2c srcid) = state
                 then stmtErr __LOC__ (attr,
                              pr "invalid move on core state: %s => %s"
                                (B.id2c srcid) state)
                 else (Ast.MOVE(exp1,st_exp1,Some src,[src],Some dst,
                                auto_allowed,state_end,attr),
                       false)
               | None -> (Ast.MOVE(exp1,st_exp1,None,[],Some dst,
                                   auto_allowed,state_end,attr),
                          false))
            else stmtErr __LOC__ (attr,"move not allowed")
	  | None -> stmtErr __LOC__ (attr,"invalid core state")
         )
       with LookupErrException _ ->
         stmtErr __LOC__  (attr,"destination must be a core state or a core set")
       )

    | Ast.MOVEFWD(exp,_,state_end,attr) ->
	if !in_handler
	then
	  let (_,exp1) =
	    type_exp exp env (Some(B.PROCESS))
	      src_valid tgt_valid in
	  (Ast.MOVEFWD(exp1,[],state_end,attr),false)
	else
	  stmtErr __LOC__ (attr,"forwardImmediate not allowed in interface functions")

    | Ast.SAFEMOVEFWD(exp,_,stm,state_end,attr) ->
	if !in_handler
	then
	  let (_,exp1) =
	    type_exp exp env (Some(B.PROCESS))
	      src_valid tgt_valid in
	  let (stmta,cb) =
	    type_stmt stm env ret src_valid tgt_valid break_valid in
	  (Ast.SAFEMOVEFWD(exp1,[],stmta,state_end,attr),cb)
	else
	  stmtErr __LOC__ (attr,"forwardImmediate not allowed in interface functions")

    | Ast.ASSIGN(expl,expr,sorted_fld,attr) ->
	let ((ty,isconst),expl1) = type_loc env src_valid tgt_valid expl
	and (actual,expr1) = type_exp expr env None src_valid tgt_valid in
	if isconst
	then stmtErr __LOC__ (attr,"constant left hand side of assignment")
	else (coerce "assignment" actual ty attr;
	      (Ast.ASSIGN(expl1,expr1,sorted_fld,attr),false))

    | Ast.DEFER(attr) -> (stmt,false)

    | Ast.BREAK(attr) ->
	if break_valid
	then (stmt,true)
	else stmtErr __LOC__ (attr,"unexpected break")
    | Ast.CONTINUE(attr) ->
	if break_valid
	then (stmt,true)
	else stmtErr __LOC__ (attr,"unexpected continue")


    | Ast.PRIMSTMT((Ast.VAR(id,_,fn_attr) as fn),exps,attr)
    | Ast.PRIMSTMT((Ast.FIELD(Ast.PARENT(_),id,_,fn_attr) as fn),exps,attr)  ->
	(try
	  let (ret,param_types) =
	    match fn with
	      Ast.VAR(_,_,_) -> lookup_id id (!fun_env)
	    | Ast.FIELD(_,_,_,_) -> lookup_id id (!parent_env)
	    | _ -> failwith "not possible" in
	  let exps1 =
	    List.map2
	      (function exp ->
		function ptype ->
		  let ptype =
		    (* not clear why this is done, but check_expected
		       drops the SYSTEM from a type before comparing it *)
		    match ptype with
		      B.SYSTEM(t) -> t
		    | t -> t in
		  let (ty,exp1) = type_exp exp env None src_valid tgt_valid in
		  coerce (pr "call to %s" (B.id2c id)) ty ptype attr;
		  exp1)
	      exps param_types in
      (Ast.PRIMSTMT(Ast.VAR(id,[],fn_attr),exps1,attr),false)
	with
	  LookupErrException _ ->
	    stmtErr __LOC__ (attr,(pr "unknown function %s" (B.id2c id)))
	| Invalid_argument str ->
	    stmtErr __LOC__ (attr,
		    pr "%s\nwrong number of arguments to %s" str (B.id2c(id))))

    | Ast.PRIMSTMT(_,exps,attr) -> stmtErr __LOC__ (attr,"unknown function")

    | Ast.ERROR(str,attr) -> (stmt,false)

    | Ast.ASSERT(exp,_) -> raise (Error.Error "type: internal error"))
  with
    StmtErrException -> (stmt,false)
  | ExpErrException _ -> (stmt,false)

and check_cases cases attr =
  let (remaining_states,line_labels) =
    List.fold_left
      (function (remaining_states,line_labels) ->
	function (Ast.SEQ_CASE(lbls,_,_,_)) ->
	  let (remaining_states,local_line_labels) =
	    List.fold_left
	      (function (remaining_states,line_labels) ->
		function label ->
		  let conc_states = CS.concretize label in
		  let inter = Aux.intersect conc_states remaining_states in
		  match inter with
		    [] -> stmtErr __LOC__ (attr,
				  pr "switch: case label %s doesn't match any untreated states"
				    (B.id2c label))
		  | _ -> (Aux.subtract remaining_states conc_states,
			  inter@line_labels))
	      (remaining_states,[]) lbls in
	    (remaining_states,(List.rev local_line_labels)::line_labels))
      ((List.map (function (_,s) -> s) (!CS.state_env)),[])
      cases in
  let line_labels = List.rev line_labels in
  let all_states =
    List.map (function (CS.STATE(id,_,_,_)) -> id)
      (List.flatten line_labels) in
  if (Aux.is_set all_states)
  then
    (match Aux.subset_fail (!state_names) all_states with
      None ->
	(match Aux.subset_fail all_states (!state_names) with
	  None -> line_labels
	| Some(extra_state) ->
	    stmtErr __LOC__ (attr,
		    pr "switch: the case label %s is not a state name"
		      (B.id2c extra_state)))
    | Some(missing_state) ->
	stmtErr __LOC__ (attr,
		pr "switch: the state %s is not among the case labels"
		  (B.id2c missing_state)))
  else stmtErr __LOC__ (attr,"switch: state names cannot be repeated")

and type_loc env src_valid tgt_valid : Ast.expr -> ((B.typ * bool) * Ast.expr) = function
    Ast.VAR(id,_,attr) ->
    (try
	let ((ty,isconst) as x) = lookup_id id env in
	(x,Ast.VAR(id,[],B.updty attr ty))
      with LookupErrException _ ->
	stmtErr __LOC__ (attr,pr "unknown location %s" (B.id2c(id))))
  | Ast.FIELD(exp,fld,_,attr) ->
      let (ty,exp1) = type_exp exp env None src_valid tgt_valid in
      let ((fldty,isconst) as x) =
	type_structure_field ty fld attr src_valid tgt_valid None in
      (match fldty with
	B.TIMER(_) -> stmtErr __LOC__ (attr,"cannot assign to a timer-typed field")
      |	_ -> ());
      (x,Ast.FIELD(exp1,fld,[],B.updty attr fldty))
  | _ -> raise (Error.Error("type: internal error"))

(* -------------------- Information about states -------------------- *)

let type_states states env =
  (* Obtain identifier of states *)
  let pname = B.mkId("process") in
  let env =
    let (isconst,fenv) = lookup_id pname (!struct_env) in
    List.map(fun (id, ty) -> (id, (ty, isconst))) fenv @ env
  in
  let states =
    List.map (function
  Ast.QUEUE(clsname,shared,queue_typ,id,vis,po, attr) ->
    let new_po = match po with
	None -> None
      | Some (Ast.CRIT_ID(key, order, id, fnid, attr)) ->
	 (try
	    let (ty, _) = lookup_id id env in
	    Some (Ast.CRIT_ID(key, order, id, fnid, B.updty attr ty))
	  with LookupErrException _ ->
	    stmtErr __LOC__  (attr, ("unknown identifier "^(B.id2c(id))))
	 )

      | Some (Ast.CRIT_EXPR(key, order, expr, fnid, attr)) ->
	 let (ty, expr) = type_exp expr env None false B.UNK in
	 Some (Ast.CRIT_EXPR(key, order, expr, fnid, B.updty attr ty))
    in
    Ast.QUEUE(clsname,shared,queue_typ,id,vis,new_po, attr)
    | x -> x) states in
  let new_state_names =
    List.map
      (function
	  Ast.QUEUE(clsname,shared,queue_typ,id,vis,po, attr) -> id
        | Ast.PROCESS(clsname,shared,id,previd,vis,attr) -> id)
      states in
  (* Computes the identifier of states that are not terminated or
   * nowhere *)
  let non_empty_state_names =
    List.fold_left
      (function prev ->
	function
	    Ast.QUEUE(CS.NOWHERE,shared,_,id,vis,po, attr) -> prev
	  | Ast.QUEUE(CS.TERMINATED,shared,CS.EMPTY,id,vis,po, attr) ->
	      (id::prev)
	  | Ast.QUEUE(clsname,shared,CS.EMPTY,id,vis,po, attr) -> id::prev
	  | Ast.QUEUE(clsname,shared,_,id,vis,po,attr) -> id::prev
          | Ast.PROCESS(clsname,shared,id,previd,vis,attr) -> id::prev)
      [] states in
  (* FIXME: Do we still need previous states? *)
  let new_prev_names =
    List.fold_left
      (function acc ->
	function
            Ast.PROCESS(clsname,shared,id,Ast.BOSSA_PREV previd,vis,attr) ->
	      previd::acc
          | Ast.PROCESS(clsname,shared,id,Ast.RTS_PREV previd,vis,attr) ->
	      previd::acc
	  | _ -> acc)
      []
      states in
  (* Computes list of classes (not identifiers) *)
  let running_count = List.length (List.filter
      (function Ast.PROCESS(CS.RUNNING,shared,id,previd,vis,attr) -> true
                | Ast.QUEUE(CS.RUNNING, shared,id,previd,vis,po,attr) -> true
                | _ -> false)
      states) in
  let new_classes =
    List.map (function Ast.QUEUE(clsname,shared,queue_typ,id,vis,po, attr) -> clsname
      | Ast.PROCESS(clsname,shared,id,previd,vis,attr) -> clsname)
      states in
  let type_state = function
      Ast.QUEUE(clsname,shared,queue_typ,id,vis,po,attr) ->
      let cur = (id,CS.STATE(id,clsname,CS.QUEUE(queue_typ),vis)) in
      (CS.state_env := cur :: (!CS.state_env); [(id,(B.QUEUE(B.PROCESS),true))])
    | Ast.PROCESS(clsname,shared,id,previd,vis,attr) ->
      (CS.state_env := (id,CS.STATE(id,clsname,CS.PROC,vis))
                       :: (!CS.state_env);
       let ty = B.PROCESS in
       match previd with
         Ast.BOSSA_PREV prev -> [(id,(ty,true));(prev,(ty,true))]
       | Ast.RTS_PREV prev -> [(id,(ty,true));(prev,(ty,true))]
       | Ast.NO_PREV -> [(id,(ty,true))])
  in
  (
   if not(Aux.subset (CS.all_classes false) new_classes)
   then nl_error __LOC__ ("At least one state must be declared for each class");
   if (running_count > 1) then nl_error __LOC__ ("Only one RUNNING class can be defined");
   if Aux.is_set new_state_names
   then (state_names := non_empty_state_names;
	 prev_names := new_prev_names;
	 (states,
	 List.fold_left
	   (function env -> function st -> (type_state st) @ env)
	   env states))
   else (nl_error __LOC__ ("State names must be disjoint"); (states, env)))


(* Updates appropriate core state datastructures
 * and ensures correct core states are declared *)
let type_cstates states env =
    (* Obtain identifiers of states *)
    let new_state_names =
        List.map
          (function
              Ast.CSET(cls, id, attr)
	    | Ast.CSTATE(cls, id, attr) -> id)
          states in
     (* Count number of active states *)
   let active_count =
    List.length
      (List.filter
	 (function
	     Ast.CSET(CS.ACTIVE,id,attr)
	   | Ast.CSTATE(CS.ACTIVE, id, attr) -> true
    |  _ -> false)
	 states) in
    let type_cstate = function
      Ast.CSET(clsname, id, attr) ->
      let cur = (id, CS.CSTATE(id,clsname)) in
      CS.cstate_env := cur :: !CS.cstate_env;
      [(id,(B.SET(B.CORE),true))]
    | Ast.CSTATE(clsname, id, attr) ->
      let cur = (id, CS.CSTATE(id,clsname)) in
      CS.cstate_env := cur :: !CS.cstate_env;
      [(id,(B.CORE,true))]
  in
  (
    if (active_count = 0) then
      raise (Error.Error "At least one ACTIVE class is required");
   if Aux.is_set new_state_names
    then (
   cstate_names := new_state_names;
   List.fold_left
	   (function env -> function st -> (type_cstate st) @ env)
	   env states)
    else (nl_error __LOC__ ("state names must be disjoint"); env))

(* ------------------------ Ordering criteria ----------------------- *)

(* Checks that the ordering criterion key is correct *)
let rec verify_criteria criteria env enum_range_env pname =
  let pname = B.mkId("process") in
  let fenv =
    let (_,fenv) = lookup_id pname (!struct_env) in
    List.map (function (id,vl) -> (id,(vl,true))) fenv in
  let enum_or_range id env =
    (try
      (match lookup_id id fenv with
	(B.ENUM(e_id) as ty,_)  -> (lookup_id e_id enum_range_env,ty)
      | (B.RANGE(r_id) as ty,_) -> (lookup_id r_id enum_range_env,ty)
      | _ ->
	  (nl_error __LOC__ (
	   pr "key criterion %s must have enum or range type"
	     (B.id2c id));
	   ((0,0),B.VOID)))
    with LookupErrException _ ->
      (nl_error __LOC__ (pr "invalid criterion %s" (B.id2c id)); ((0,0),B.VOID))) in
  let (_,l) =
    List.fold_left
      (function (non_key,prev) ->
	function
	    Ast.CRIT_ID(Ast.KEY(_,_),order,id,fnid,attr) ->
	      if non_key
	      then error __LOC__ (attr,
			 "key must only appear in a prefix of the "^
			 "ordering criteria");
	      let ((mn,mx),ty) = enum_or_range id fenv in
	      let newkey = Ast.KEY(mn,mx) in
	      (non_key, Ast.CRIT_ID(newkey,order,id,fnid,B.updty attr ty) :: prev)
	  | Ast.CRIT_ID(Ast.NOKEY,order,id,fnid,attr) as x ->
	      (try
		(match lookup_id id fenv with
		  (ty,_)  ->
		    let typed = Ast.CRIT_ID(Ast.NOKEY,order,id,fnid,B.updty attr ty)
		    in (true,typed::prev))
	      with LookupErrException _ ->
		(error __LOC__ (attr,pr "invalid criterion %s" (B.id2c id));
		 (true,x::prev))))
	        (false,[]) criteria in
  List.rev l

(* ------------------------------ Trace ----------------------------- *)

let get_id = function
    Ast.TRACEVAR(fldid,id,attr) -> (id,attr)
  | _ -> raise (Error.Error "internal error")

let verify_trace_expressions env ids =
  let pname = B.mkId("process") in
  let fenv = let (_,fenv) = lookup_id pname (!struct_env) in fenv in
  List.map
    (function idexp ->
      let (id,attr) = get_id idexp in
      if Aux.member id (!state_names)
      then
	(try
	  let (ty,_) = lookup_id id env in
	  Ast.TRACEVAR(B.mkId((B.id2c id)^"_data"),id,B.updty attr ty)
	with LookupErrException _ ->
	  Ast.TRACEQUEUE(B.mkId((B.id2c id)^"_data"),id,B.updty attr B.INT))
      else
	try
	  let (ty,_) = lookup_id id env in
	  Ast.TRACEVAR(id,id,B.updty attr ty)
	with
	  LookupErrException _ ->
	    (try
	      let ty = lookup_id id fenv in
	      Ast.TRACEFIELD(id,B.updty attr ty)
	    with
	      LookupErrException _ ->
		(error __LOC__ (attr,"unknown traced variable");
		 Ast.TRACEVAR(id,id,attr))))
    ids

let verify_trace env handlers = function
    Ast.NOTRACE -> Ast.NOTRACE
  | Ast.PRETRACE(count,info,attr) ->
      let seen_events = ref (None : Ast.event_name list option) in
      let seen_expressions = ref (None : Ast.classified_exp list option) in
      let seen_test = ref (None : Ast.expr option) in
      List.iter
	(function
	    Ast.TRACEEVENTS(events) ->
	      (match !seen_events with
		None -> (*
		  let event_names =
		    List.map (function Ast.EVENT(nm,_,_,_) -> nm) handlers in
          (* FIXME: handle trace hierarchy *)

		  List.iter
		    (function event ->
		      if not (List.exists (Ast.event_name_prefix event)
				event_names)
		      then error __LOC__ (attr,
				 pr "traced event %s not declared"
				   (Ast.event_name2c event))) events *)
		  seen_events := Some events;
	      |	Some _ ->
		  error __LOC__ (attr, "duplicate events declaration"))
	  | Ast.TRACEEXPS(ids) ->
	      (match !seen_expressions with
		None ->
		  let new_exps = verify_trace_expressions env ids in
		  seen_expressions := Some new_exps;
	      |	Some _ ->
		  error __LOC__ (attr, "duplicate expression declaration"))
	  | Ast.TRACETEST(exp) ->
	      (match !seen_test with
		None ->
		  let (_,exp) = type_exp exp env (Some B.BOOL) false B.BOTH in
		  seen_test := Some exp
	      |	Some _ ->
		  error __LOC__ (attr, "duplicate test declaration")))
	info;
      Ast.TRACE(count,
		(match !seen_events with None -> [] | Some x -> x),
		(match !seen_expressions with None -> [] | Some x -> x),
		!seen_test,
		attr)
  | _ -> raise (Error.Error "internal error")

(* --------------- Check Core Init -------------- *)

(* Test if two fields or variables are equal *)
let rec field_equal var1 var2 =
    match var1,var2 with
       Ast.FIELD(exp1,id1,_,_), Ast.FIELD(exp2,id2,_,_)
       -> ((id1 = id2) && field_equal exp1 exp2 )
       | Ast.VAR(id1,_,_), Ast.VAR(id2,_,_) -> (Objects.id2c id1 = Objects.id2c id2)
       | _ -> false

(* Test if a field is contained in a list of expressions *)
let rec is_contained (Ast.FIELD(exp,id,_,attr) as field)  (cores: Ast.expr list): bool  =
    let l = List.length cores in
    match cores with
        [] -> false
        | car::cdr ->
                let is_member = field_equal field car in
                if (is_member = true) then true
                else is_contained field cdr

(* TODO: instantiate environment so that aliasing can be taken into account*)
let rec check_core_entry_rec (stmt: Ast.stmt)
    (core: Ast.expr) (((move_def,decl, def) as env): (bool * (B.identifier * (B.typ * bool)) list * B.identifier list)) : bool * B.identifier list * Ast.expr =
    let (moved, vars, core) =
    match stmt with
     Ast.IF(_,stmt1,stmt2,_) ->
         let (move_if,if_branch,core) = check_core_entry_rec stmt1 core env  in
         let (move_else,else_branch,core) = check_core_entry_rec stmt2 core env in
         ((move_if && move_else),def@(Aux.intersect if_branch else_branch),core)
    | Ast.FOR(_,_,_,_,_,_,_) -> (move_def,def,core) (* can never treat a variable
  defined a loop as defined, as can't know whether variable
  will be empty *)
      | Ast.SEQ(_,stmts,_) ->
         let accu = (move_def,def,core) in
          (* Once detects that the core has been
           * moved, stops iterating over statements
           * as all core variables must be declared BEFORE
           * the move *)
          let (fin_move,defined,core) = List.fold_left
          (fun (move_def,def,core) stmt ->
              if move_def = false
              then
                  begin
                  let (move,defined,core) = check_core_entry_rec stmt core (move_def,decl, def) in
                  let count = List.length defined in
                  (move,defined,core)
                  end
              else (move_def,def,core)
          ) accu stmts in (fin_move,defined,core)
      | Ast.ASSIGN(expr1,expr2,_,_) ->
	ignore(type_stmt stmt decl None false B.UNK false);
        if move_def = false  then
          begin
            match expr1 with
                Ast.FIELD(exp,id,_,attr) as fi->
		  (* TODO: account for aliasing here, call
		     reverse_lookup_trans*)
		  begin
		    try
                      let contained =  Util.is_equal exp core in
                      if contained then (move_def,id::def,core)
                      else (move_def,def,core)
		    with LookupErrException e ->
		      stmtErr __LOC__ (attr, (B.id2c id ^" not found"))
		  end
	      | Ast.VAR(id,_,attr) ->
		 (move_def,id::def,core)
              | _ -> (move_def,def,core)
          end
            else (move_def,def,core)
    | Ast.MOVE(exp,_,_,_,_,_,_,a) ->
            (* TODO, account for aliasing here *)
            let contained = Util.is_equal exp core in
            if (contained = false) then
                    error __LOC__ (a,"Move can only refer to target
                    core");
            if (move_def = true) then
                error __LOC__ (a, "Can only move core once");
            (true, def, core)
    (* TODO add SWITCH, add function calls *)
    | _ -> (move_def,def,core)
    in (moved, Aux.make_set vars, core)


(* TODO: simplify conditional structures like what was done
 * in Verifier *)
(* Check that all non-system core variables are initialised
 * Assumption 1: No need to consider functions as they cannot
 * take a core or core event as argument
 * cores is a list of aliases for the entering core *)
let check_core_entry (event: B.identifier * Ast.expr) core_variables procdecls body attr =
  let (eventid, core) = event in
   let core_vars = List.map
        (fun var -> match var with
           Ast.VARDECL(ty,id,_,_,_,_,_) -> (id, (ty, false))) core_variables  in
    let sys_core_vars = Aux.option_filter
                (function
	            Ast.VARDECL(ty,id,true,_,_,_,_) -> Some (id, (ty, false))
	            | _ -> None)
                core_variables in
    let lazy_core_vars = Aux.option_filter (* lazy definition *)
                (function
	            Ast.VARDECL(ty,id,_,true,_,Some x,_) -> Some (id, (ty, false))
	            | _ -> None) core_variables in
    let init_core_vars = Aux.option_filter (* initial definition *)
                (function
	            Ast.VARDECL(ty,id,_,false,_,Some x,attr) -> Some (id, attr)
	            | _ -> None) core_variables in
    let domains = Aux.option_filter
                (function
                Ast.VARDECL(B.SET(B.DOMAIN),id,_,_,_,_,_) -> Some (id, (B.SET(B.DOMAIN), false))
                | _ -> None) core_variables in
    let global_vars = (eventid, (B.CEVENT,true))::sys_core_vars@lazy_core_vars@domains in
    let global_ids = List.map (fun (id, _) -> id) global_vars in
   (* Computes variables that have been defined in handler
    * before the move. Also returns whether move exists *)
   let (move_defined, user_defined_vars, core) =
     check_core_entry_rec body core (false,core_vars@global_vars, global_ids) in
   let defined_vars = user_defined_vars @ global_ids in
   let core_var_ids = List.map (fun (id, _) -> id) core_vars in
   let undefined_vars = Aux.subtract core_var_ids defined_vars in
    if not (Aux.empty init_core_vars)
    then
      let init =
	   if List.length init_core_vars > 1 then
	     "Core fields must not be initialized: "^
	       (Aux.set2c (List.map
			     (fun (id, attr) -> B.id2c id ^"("^string_of_int (B.line attr)^")")
			     init_core_vars))
	   else
	     let (id, attr) = List.hd init_core_vars in
	     "Core field must not be initialized: "^ B.id2c id ^"("^string_of_int (B.line attr)^")."
	 in error __LOC__ (attr,
		  "core_entry must explicitly initialize all the fields of the target core.\n"^
		    "No initialization is allowed at the declaration level.\n\t" ^ init);
    if not (Aux.empty undefined_vars)
    then let missing =
	   if List.length undefined_vars > 1 then
	     "Missing fields are "^(Aux.set2c (List.map B.id2c undefined_vars))
	   else
	     "Missing field is "^ B.id2c (List.hd undefined_vars)^"."
	 in error __LOC__ (attr,
		  "core_entry does not set some fields of the target core"^
		    " before the move.\n\t" ^ missing);
    if (move_defined = false)
        then error __LOC__ (attr, "failed to move the target core
        to an ACTIVE or IDLE queue")

(* check that all non-system process fields are initialized.  check that
   there is no state change or assignment to globals before an error *)
let check_attach params procdecls stmt =
  let decls = List.map (function (Ast.VARDECL(_,id,_,_,_,_,attr)) -> id) params in
  let (Ast.VARDECL(_,id,_,_,_,_,attr)) = List.hd params in (* attached process *)
  let init_decls =
    Aux.option_filter
      (function
    Ast.VARDECL(_,id,true,_,_,_,_) -> None (* imported *)
	| Ast.VARDECL(B.TIMER(_),id,false,_,_,_,_) -> None
	| Ast.VARDECL(_,id,false,false,_,Some _,attr) -> Some (id, attr) (* init declaration *)
	| Ast.VARDECL(_,id,false,false,_,None,_) -> None (* pure declaration *)
	| Ast.VARDECL(_,id,false,true,_,Some _,_) -> None (* lazy *)
      )
      procdecls in
  let procdecls =
    Aux.option_filter
      (function
          Ast.VARDECL(_,id,true,_,_,_,_) -> None (* imported *)
	| Ast.VARDECL(B.TIMER(_),id,false,_,_,_,_) -> None
	| Ast.VARDECL(_,id,false,false,_,_,_) -> Some id (* pure and init declaration *)
	| Ast.VARDECL(_,id,false,true,_,Some _,_) -> None (* lazy *)
      )
      procdecls in
  let rec loop state_change decls procdecls = function
      Ast.IF(tst,thn,els,a) ->
	let (state_change1,procdecls1) =
	  loop state_change decls procdecls thn in
	let (state_change2,procdecls2) =
	  loop state_change decls procdecls els in
	(state_change1 || state_change2, Aux.union procdecls1 procdecls2)
    | Ast.FOR_WRAPPER(_,stmt,a) -> raise (Error.Error "unexpected for wrapper")
    | Ast.FOR(id,_,_,_,stmt,crit,a) ->
	let (state_change1,procdecls1) =
	  loop state_change (id::decls) procdecls stmt in
	(* two iterations should be enough, since the domain is boolean *)
	if not (state_change = state_change1)
	then loop state_change1 (id::decls) procdecls1 stmt
	else (state_change1,procdecls1)
    | Ast.SWITCH(_,cases,default,a) ->
	let (state_changes,procdeclss) =
	  List.split
	    (List.map
	       (function Ast.SEQ_CASE(_,_,stmt,_) ->
		 loop state_change decls procdecls stmt)
	       cases) in
	let (state_change2,procdecls2) =
	  (match default with
	    None -> (false, [])
	  | Some default -> loop state_change decls procdecls default) in
	(List.mem true (state_change2 :: state_changes),
	 List.fold_left Aux.union procdecls2 procdeclss)
    | Ast.SEQ(ldecls,stmts,a) ->
	List.fold_left
	  (function (state_change,procdecls) ->
	    function stm ->
	      let (state_change1,procdecls1) =
		loop state_change decls procdecls stm in
	      (state_change || state_change1,procdecls1))
	  (state_change,procdecls) stmts
    | Ast.RETURN(_,a) -> (false,procdecls)
    | Ast.MOVE(_,_,_,_,_,_,_,a) -> (true,procdecls)
    | Ast.MOVEFWD(_,_,_,a) -> (true,procdecls)
    | Ast.SAFEMOVEFWD(_,_,_,_,a) -> (true,procdecls)
    | Ast.ASSIGN(Ast.VAR(id1,_,_),_,_,a) when not(List.mem id1 decls) ->
	(true,procdecls)
    | Ast.ASSIGN(Ast.FIELD(Ast.VAR(id1,_,_),fld,_,_),_,_,a) when B.ideq(id,id1) ->
	let procdecls =
	  List.filter (function fld1 -> not (fld = fld1)) procdecls in
	(state_change,procdecls)
    | Ast.ERROR(_,a) ->
	if state_change
	then error __LOC__ (a,"error not allowed after state change in attach");
	(false,[])
    | _ -> (state_change,procdecls) in ()
  (* if not (Aux.empty init_decls)
    then
      let init =
	   if List.length init_decls > 1 then
	     "Process fields must not be initialized: "^
	       (Aux.set2c (List.map
			     (fun (id, attr) -> B.id2c id ^"("^string_of_int (B.line attr)^")")
			     init_decls))
	   else
	     let (id, attr) = List.hd init_decls in
	     "Process field must not be initialized: "^ B.id2c id ^"("^string_of_int (B.line attr)^")."
	 in error __LOC__ (attr,
		  "attach must explicitly initialize all the fields of the target process.\n"^
		    "No initialization is allowed at the declaration level.\n\t" ^ init);
  match loop false decls procdecls stmt with
    (_,[]) -> ()
  | (_,l) ->
      error __LOC__ (attr,
	    (Printf.sprintf
	       "attach does not set some fields of the process parameter %s\n"
	       (B.id2c id)) ^
	    (Printf.sprintf "missing fields are %s\n"
	       (Aux.set2c (List.map B.id2c l)))^
	    "note that the initialization must use the name of the\n"^
	    "process parameter explicitly\n") *)

(* --------------------- Handlers type checking --------------------- *)

let check_core_var per_cpu_env core_var =
  List.iter (fun (Ast.VARDECL(ty,id,imported,lz,_,de,attr)) ->
    if lz then
    match de with
	None -> ()
      | Some exp -> ()
  ) core_var

(* The set of defined core handlers should be exactly the
 * required set *)
let rec check_cevents chandlers corev procs =
  let declared_names = List.map (function Ast.EVENT(Ast.EVENT_NAME(id,a),_,_,_,_) -> id) chandlers
  and required_names = List.map (function Ast.EVENT_NAME(id,a) -> id) !Events.cevents in
  if not(Aux.is_set declared_names)
  then nl_error __LOC__ ("some core event handlers duplicated");
  let missing_names = Aux.subtract required_names declared_names in
   List.iter
	  (function nm ->
	    nl_error __LOC__ ( "Missing required core event: "^(B.id2c nm))) missing_names ;
    (* Type check core_entry specifically *)
    (* TODO: function doesn't currently support
     * core aliasing. Will only work if we do
     * ce.target.field =
     *)
    List.iter
        (function Ast.EVENT(nm,param,stmt,_,attrb) ->
            if Ast.event_name2c nm = "core_entry" then
                begin
                    let (id,att) = match param with
			Ast.VARDECL(_,id,_,_,_,_,att) -> (id,att) in
		    let event = Ast.VAR(id,[],att) in
                    let new_field = Ast.mkFIELD(event, "target",[],-1) in
                    check_core_entry (id, new_field) corev procs stmt attrb
                end
        ) chandlers

(* The set of defined process handlers should be exactly the
 * required set *)

  let rec check_pevents phandlers =
  let declared_names = List.map (function Ast.EVENT(Ast.EVENT_NAME(id,a),_,_,_,_) -> id) phandlers
  and required_names = List.map (function
      Ast.EVENT_NAME(id,a) -> id) !Events.pevents in
  let set = Aux.is_set declared_names in
  if ( set = false)
  then nl_error __LOC__ ("some process event handlers duplicated");
  let missing_names = Aux.subtract required_names declared_names in
   List.iter
	  (function nm ->
	    nl_error __LOC__ ("Missing required process event: "^(B.id2c nm))) missing_names

let type_handlers handlers var_env timers_with_targets
    persistent_timers star_timer_with_target =
  in_handler := true;
  move_allowed := true;
  List.map
    (function
	(Ast.EVENT(nm,(Ast.VARDECL(B.PEVENT,id,false,false,_,None,vattr) as param),
		   stmt,syn,attr)) ->
	in_core_event := false;
	  in_persistent_timer_handler := false;
	  let snm = Ast.event_name2c nm in
	  let env =
	    match snm with
		"schedule" -> var_env
	      | _ -> extend_env id (B.PEVENT,true) var_env
	  in
	  let (new_stmt,_) =
	    type_stmt stmt env None false B.BOTH false in
	  Ast.EVENT(nm, param, new_stmt, syn, attr)
       |(Ast.EVENT(nm,(Ast.VARDECL(B.CEVENT,id,false,false,_,None,vattr) as param),
		   stmt,syn, attr)) ->
	  let snm = Ast.event_name2c nm in
	  in_core_event := true;
	  let env = extend_env id (B.CEVENT,true) var_env in
	  let (new_stmt,_) = type_stmt stmt env None false B.BOTH false in
	  Ast.EVENT(nm, param, new_stmt, syn, attr)
      |	_ -> raise (Error.Error "internal error"))
    handlers



let rec check_no_return = function
    Ast.IF(exp,stmt1,stmt2,attr) ->
      check_no_return stmt1; check_no_return stmt2
  | Ast.FOR_WRAPPER(label,stmts,attr) -> raise (Error.Error "cannot occur")
  | Ast.FOR(id,state_ids,_,dir,stmt,_,attr) -> check_no_return stmt
  | Ast.SWITCH(exp,cases,default,attr) ->
      List.iter
	(function Ast.SEQ_CASE(pat,_,stmt,attr) -> check_no_return stmt)
	cases;
      (match default with
	None -> ()
     | Some s -> check_no_return s)
  | Ast.SEQ(decls,stmts,attr) -> List.iter check_no_return stmts
  | Ast.RETURN(exp,attr) -> error __LOC__ (attr, "return not allowed")
  | Ast.SAFEMOVEFWD(exp,_,stm,state_end,attr) -> check_no_return stm
  | Ast.ERROR(str,attr) -> error __LOC__ (attr, "error not allowed")
  | s -> ()



(* Checks that attach takes as first parameter
 * a process and that no other function takes
 * as argument a process or scheduler*)
let check_params exported nm params attr =
  match (B.id2c nm) with
    "attach" ->
    (match params with
	(Ast.VARDECL(ty,id,imported,lz,_,de,attr)) :: rest ->
	  if not (ty = B.PROCESS) then
	    error __LOC__ (attr,"'process' type expected for the first argument of attach: "^B.type2c ty)
	  else ()
      |	_ -> error __LOC__ (attr,"attach must have at least a 'process' argument"))
    | _ ->
      if exported
      then
	List.iter
	  (function
	  Ast.VARDECL(B.PROCESS,id,imported,lz,_,de,attr)
        | Ast.VARDECL(B.CORE,id,imported,lz,_,de,attr)
	| Ast.VARDECL(B.SCHEDULER,id,imported,lz,_,de,attr)
        | Ast.VARDECL(B.CEVENT,id,imported,lz,_,de,attr)
	| Ast.VARDECL(B.PEVENT,id,imported,lz,_,de,attr) ->
	error __LOC__ (attr,
		      ("an exported function cannot have an event, process, core "^
		       "or scheduler argument"))
	    | _ -> ())
	  params
      else ()

let type_params env = function
Ast.VARDECL(ty,id,imported,lz,sh,de,attr) ->
  (try lookup_id id env;
       error __LOC__ (attr, "Hidden variable "^ (B.id2c id))
   with LookupErrException _ -> ());
  Ast.VARDECL(ty,id,imported,lz,sh,de,B.updty attr ty)

let type_functions functions var_env proc_decls params_modifiable =
  in_handler := false;
  in_persistent_timer_handler := false;
  List.map
    (function Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr) ->
      let exported = inl = Ast.EXPORTED in
      let snm = B.id2c nm in
      move_allowed := not exported ;
      check_params exported nm params attr; (*only for attach/detach/exported*)
      if snm = "attach" (* only for ifunctions *)
      then (in_core_event := false;
	    check_attach params proc_decls stmt)
      else if tl = Ast.TRYLOCK then
	begin
	  move_allowed := true;
	  in_core_event := false
	end
      ;
      let new_params = List.map (type_params var_env) params in
      let (new_stmt,_) =
	type_stmt stmt
	  (List.fold_left
	    (function env ->
	       function Ast.VARDECL(ty,id,imported,lz,_,de,attr) ->
		 extend_env id (ty,params_modifiable) env)
	     var_env new_params)
	  (Some(ret))
	  false B.BOTH false in
      Ast.FUNDEF(tl,ret,nm,new_params,new_stmt,inl,attr))
    functions

let type_ifunctions functions var_env proc_decls =
  in_handler := false;
  in_persistent_timer_handler := false;
  let names =
    List.map
      (function Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr) -> (B.id2c nm))
      functions in
  if (not (Aux.member "attach" names))
  then nl_error __LOC__ ("Scheduler must define an attach function");
  if (not (Aux.member "init" names))
  then nl_error __LOC__ ("Scheduler must define an init function");
  type_functions functions var_env proc_decls true

let make_pstates_core_var = function
    Ast.QUEUE(_,_,_,id,_,_,_) -> (id, B.QUEUE(B.PROCESS))
  | Ast.PROCESS(_,_,id,_,_,_) -> (id, B.PROCESS)

let type_pcstate per_cpu_env steal = function
    Ast.CORE(cv, pstates, _) ->
      let ty_cv = List.map (type_decl_wupd per_cpu_env) cv in
      Ast.CORE(ty_cv, pstates, steal)
  | Ast.PSTATE(pstates) -> Ast.PSTATE(pstates)

let type_steal_thread_blk env = function
((fs,fv, fe), (sv, sv2, se2), (mv1, mv2, mv3_, mv4, ms, se, ms2)) ->
    let svenv = type_decl sv in
    let (sid, (B.INDR sty, b)) = svenv in
    let svenv2 = (sid, (sty, b)) in
  let senv = svenv :: env in
  let mv1env = type_decl mv1 in
  let fenv = type_decl fs :: type_decl fv :: env
  and menv =
    match mv3_ with
      None -> mv1env :: type_decl mv2 :: type_decl mv4 :: senv
    | Some mv3 -> mv1env :: type_decl mv2 :: type_decl mv3 :: type_decl mv4 :: senv
  in
  move_allowed := true;
  (svenv2, mv1env,
   ((fs,fv, fst (type_stmt fe fenv (Some B.BOOL) false B.UNK false)),
    (sv,
     sv2,
     fst (type_stmt se2 senv (Some (B.CORE)) false B.UNK false)),
    (mv1, mv2, mv3_, mv4,
     fst (type_stmt ms menv None false B.UNK false),
     snd (type_exp se menv (Some B.BOOL) false B.UNK),
     ms2))
  )

let type_steal_group env = function
((fs,fv, fe), (sv1, sv2, sv3, se2), (se)) ->
  let svenv = [type_decl sv1 ; type_decl sv2 ; type_decl sv3] in
  let fenv = type_decl fs :: type_decl fv :: env
  and smenv = svenv @ env
  in
  move_allowed := true;
  (svenv,
   ((fs,fv, fst (type_stmt fe fenv (Some B.BOOL) false B.UNK false)),
    (sv1, sv2, sv3,
     fst (type_stmt se2 smenv (Some (B.GROUP)) false B.UNK false)),
    snd (type_exp se smenv (Some B.BOOL) false B.UNK))
  )

let type_steal_thread env steal =
  match steal with
    Ast.FLAT_STEAL_THREAD st ->
      let (_, _, st) = type_steal_thread_blk env st in
      Ast.FLAT_STEAL_THREAD st
  | Ast.ITER_STEAL_THREAD (st, until, post) ->
     let (fvenv, svenv, st) = type_steal_thread_blk env st in
     let bigenv = fvenv::svenv::env in
     Ast.ITER_STEAL_THREAD (st,
			    snd (type_exp until bigenv (Some B.BOOL) false B.UNK),
			    fst (List.split (List.map (fun s -> type_stmt s bigenv None false B.UNK false) post)))

let type_steal env steal =
  match steal with
    Ast.STEAL_THREAD s -> Ast.STEAL_THREAD (type_steal_thread env s)
  | Ast.ITER_STEAL_GROUP (sg, st, post, postgrp) ->
     let (fvenv, sg) = type_steal_group env sg in
     let bigenv = fvenv @ env in
     Ast.ITER_STEAL_GROUP (sg, type_steal_thread bigenv st,
			   fst (List.split (List.map (fun s -> type_stmt s bigenv None false B.UNK false) post)),
			   fst (List.split (List.map (fun s -> type_stmt s bigenv None false B.UNK false) postgrp)))
  | Ast.FLAT_STEAL_GROUP (sg, st, postgrp) ->
     let (fvenv, sg) = type_steal_group env sg in
     let bigenv = fvenv @ env in
     Ast.FLAT_STEAL_GROUP (sg, type_steal_thread (fvenv@env) st,
			   fst (List.split (List.map (fun s -> type_stmt s bigenv None false B.UNK false) postgrp)))

let type_steal_param env (dom, dstg, dst, steal) =
  let env = if dom <> "" then
      (B.mkId dom, (B.DOMAIN, false)):: env
    else env
  in
  let env = if dstg <> "" then
      (B.mkId dstg, (B.GROUP, false)):: env
    else env
  in
  let env = if dst <> "" then
      (B.mkId dst, (B.CORE, false)):: env
    else env
  in
  (dom, dstg, dst, type_steal env steal)

(* -------------------------- entry point --------------------------- *)

let type_check ast =
  (* Check that there are no duplicate declarations *)
  Duplicate.check_duplicates ast;
  (* Check that functions are not mutually recursive *)
  Recursive.recursive ast;
  (* Renames keywords to avoid conflicts with reserved keywords*)
  let ast = Rename.preprocess ast in
  let (Ast.SCHEDULER(nm,
		     cstdefs,enums,dom,grp,procdecls,fundecls,valdefs,domains,pcstate,cstates,
		     criteria,trace,phandlers,chandlers,ifunctions,functions,
		     attr)) =
   (* Adds a number of new states/variables that
    * are linked to the system. Ensures functions well constructed
    * (adds missing returns if necessary)*)
  Pre_type.preprocess ast in
  (* init and populate the envs *)
  init_struct_env();
  init_fun_env();
  Event.init_pevents_env();
  Event.init_cevents_env();
  (* Checks constant types: that is that
   * assignment is consistent and refers to
   * variables that have already been defined *)
  let (cst_env,cstdefs1) = type_defs cstdefs [] false B.BOTH in
  (* Filters out all constants of integer type *)
  let constenv = constant_values cstdefs in
  (* Returns enum environment, including minimum and
   * maximum value of each enum. *)
  let (enum_env,enum_range_env) = typedef_enum enums cst_env constenv in
  let (core_var, ps, steal) = match pcstate with
      Ast.CORE(cv,pstates, steal) -> (cv, List.map make_pstates_core_var pstates, steal)
    | Ast.PSTATE(pstates) -> ([], List.map make_pstates_core_var pstates, None) in
  (* check function type, including duplicate functions *)
  add_fundecls fun_env fundecls; (*to fun_env*)
  add_struct_defn "domain" dom; (*to struct_env*)
  add_struct_defn "group" grp; (*to struct_env*)
  add_struct_defn "process" procdecls; (*to struct_env*)
  let core_venv = add_corev_defn core_var ps in (* to struct_env *)
  (* Updates appropriate state variables and ensures
   * states correctly defined *)
  let (pcstate, state_env) =
    (match pcstate with
      Ast.CORE(cv,states, steal) ->
	let (states, state_env) = type_states states enum_env in
	(Ast.CORE(cv, states, steal), state_env)
    |Ast.PSTATE(states) ->
       let (states, state_env) = type_states states enum_env in
       (Ast.PSTATE(states), state_env)
    ) in
  (* Updates appropriate core state variables and ensures
   * core states correctly defined *)
  let core_env = type_cstates cstates state_env in
  (* Type check non-constant global variables *)
  let (var_env,valdefs) = type_defs valdefs core_env false B.BOTH in
(*
  Bossa legacy for the 'ordering' block

   (* Checks that the ordering criteria is valid for process*)
  let criteria = verify_criteria criteria var_env enum_range_env "process" in
  (* Checks that the ordering criteria is valid for core *)
  let criteria = verify_criteria criteria var_env enum_range_env "core" in
*)
  let handlers = phandlers @ chandlers in
  let trace = verify_trace var_env handlers trace in
  let per_cpu_env = var_env @ core_venv (* from core variables *) in
  let env =  per_cpu_env @ core_env (* from core states *) in
  let steal = Aux.option_apply (type_steal_param env) steal in

  (* Type checks process event handlers *)
  check_pevents phandlers;
  let phandlers =
      type_handlers phandlers env [] [] false in
  (* Type checks core event handlers *)
  check_core_var per_cpu_env core_var;
  check_cevents chandlers core_var procdecls;
  let chandlers =
      type_handlers chandlers env [] [] false in
  let functions = type_functions functions per_cpu_env procdecls false in
  let ifunctions = type_functions ifunctions var_env procdecls false in
  let all_functions = functions@ifunctions in
  (* Check that a core is set before used in a computation*)
  (* FIXME: To enable and fix
     Checkedexception.checkfirst env phandlers chandlers all_functions;
  *)
  let domenv =
    List.map (fun (field, ty) -> (field, (ty, true)))
      (snd(lookup_str "domain" (!struct_env))) in
  let dom_ty = List.map (type_decl_wupd (per_cpu_env@domenv)) dom in
  let grpenv =
    List.map (fun (field, ty) -> (field, (ty, true)))
      (snd(lookup_str "group" (!struct_env))) in
  let grp_ty = List.map (type_decl_wupd (per_cpu_env@grpenv)) grp in
  let procdecls_ty = List.map (type_decl_wupd per_cpu_env) procdecls in
  let pcstate_ty = type_pcstate env steal pcstate in

  let new_ast = Lock.generate_lock
                  (Ast.SCHEDULER(nm,
		                cstdefs,enums,dom_ty,grp_ty,procdecls_ty,
		                fundecls,valdefs,domains,pcstate_ty,cstates,criteria,
		                trace,phandlers,chandlers,ifunctions,functions,attr))
  in
  (*
    FIXME: Inlining is good but tricky, see the head comment of Inline module.
    It would be good to improve. Do not inline for the moment to preserve functions
    in the C code, as done in manually written examples.
  *)
  (*  Braces.braces((Inline.inline new_ast)) *)
  Braces.braces(new_ast)
