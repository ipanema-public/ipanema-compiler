val type_check : Ast.scheduler -> Ast.scheduler
val get_fields : Objects.typ -> (bool * (Objects.identifier * Objects.typ) list)
