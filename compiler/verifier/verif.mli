val verify : Ast.scheduler -> Ast.scheduler
val handler_name_lookup :
 (Ast.event_name * Ast.event_name list) list -> Ast.event_name ->
   Ast.event_name list
val handler_environment : (Ast.event_name * Ast.event_name list) list ref
