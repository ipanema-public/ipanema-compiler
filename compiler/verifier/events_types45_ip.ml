module CS = Class_state
module VB = Vobjects


type entry = (CS.classname * VB.vtype * VB.modified)

type event_class_type =
    (* FIXME: what is isquasi used for *)
    Objects.isquasi * 
        (entry list * Class_state.classname list) list *
        entry list list 

let pevent_specifications _ = [
    Ast.mkEVENT_NAME("new",-1);
    Ast.mkEVENT_NAME("schedule",-1);
    Ast.mkEVENT_NAME("block",-1);
    Ast.mkEVENT_NAME("unblock",-1);
    Ast.mkEVENT_NAME("tick",-1);
    Ast.mkEVENT_NAME("yield",-1);
    Ast.mkEVENT_NAME("detach",-1);
]

let cevent_specifications _ = [
    Ast.mkEVENT_NAME("core_entry",-1);
    Ast.mkEVENT_NAME("core_exit",-1);
    Ast.mkEVENT_NAME("newly_idle",-1);
    Ast.mkEVENT_NAME("enter_idle",-1);
    Ast.mkEVENT_NAME("exit_idle",-1);
    Ast.mkEVENT_NAME("balancing",-1);
]

(* FIXME: according to transition rules *)
let interface_specifications _ = []

