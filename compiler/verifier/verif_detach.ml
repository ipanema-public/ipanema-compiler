(* Verifies that, upon receiving a detach event, the target process is
 * placed in TERMINATED *)

module Util = Verif_util
module B = Objects
module CS = Class_state

(* Verify that detach places process
 * in terminated state 
 * TODO: add support for core aliasing *)
 let verify_detach (Ast.EVENT(name,param,body,_,att)) =
     let (id,att) = match param with 
                    Ast.VARDECL(_,id,_,_,_,_,att) -> (id,att) in
                    let new_field = Ast.mkFIELD(Ast.VAR(id,[],att), "target",[],-1) in
     let cores =  (fun stmt -> match stmt with
                   Ast.MOVE(src,dest,_,_,_,_,_,_) -> (
                       Util.is_equal src new_field)
                   | _ -> false) in
    (* Identify move in body, keeping control flow structure *)
     let reach = Util.extract_move_unique cores body in 
     (* Test that all paths result in a move to terminated *) 
     let terminated_state = CS.TERMINATED in 
     let test_fn = ( fun stmt env ->
        match stmt with 
            Some(Ast.MOVE(src,dest,_,_,_,_,_,_)) ->  (
                let type_target = Util.type_pstate dest in
                match type_target with
                    None -> false
                  | Some type_target -> type_target = terminated_state
            )
            | _ -> false ) in
     Util.check_all_paths reach test_fn 


