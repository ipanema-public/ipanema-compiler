(* Verification of the ATTACH function in the Bossa Interface. This function has the
 * following guarantees: if it is of type void, the process in the first
 * argument of the function will have necessarily been placed in attached.
 * If it is of return bool, if the function returns true, the process will
 * have been placed in ready. Conversely, if it returns false, the process
 * will not have been placed in ready. The process is sound but not complete
 * as it relies on static analysis only *)

module Util = Verif_util
module B = Objects
module CS = Class_state

exception InconsistentException ;;

(* Generates possible environments if result of expression is true or false *) 
let rec if_expr_has_value (expr: (Ast.expr)) (result: bool) : (B.identifier * Ast.expr) list list = 
    match expr with 
          Ast.UNARY(uop,e,li) ->  (
              match uop with 
                Ast.NOT -> ( 
                    match result with 
                        true ->  if_expr_has_value e false 
                        | false -> if_expr_has_value e true 
                )
                | _ -> [[]] (* Not yet taken into account *)
         )
        | Ast.BINARY(bop,expr1,expr2,li) ->   (
                let left_true = if_expr_has_value expr1 true in
                let right_true = if_expr_has_value expr2 true in
                let left_false = if_expr_has_value expr1 false in 
                let right_false = if_expr_has_value expr2 false in
                match bop with 
                       Ast.OR -> (
                           match result with 
                               true -> ((Util.list_cartesian left_true right_true)
                                       @ (Util.list_cartesian left_true right_false)
                                       @ (Util.list_cartesian left_false right_true))
                              | false -> Util.list_cartesian left_false right_false
                       )
                      | Ast.AND -> (
                            match result with
                                true  -> (Util.list_cartesian left_true right_true)
                                | false -> (Util.list_cartesian left_false right_false)
                                       @(Util.list_cartesian left_true right_false)
                                       @ (Util.list_cartesian left_false right_true)
                      )
                      | _ -> raise (Error.Error "Unsupported. FIXME")
        )
        | Ast.FIELD(_,_,_,li) ->  (
                match result with
                true -> [[(Util.field_to_id expr, Ast.BOOL(true,li))]]
                | false -> [[(Util.field_to_id expr, Ast.BOOL(false,li))]]
        )
        | Ast.VAR(id,_,li) -> (
                match result with
                true -> [[(id, Ast.BOOL(true,li))]]
                | false -> [[(id, Ast.BOOL(false,li))]]
        )
        | Ast.BOOL(b,_) -> (
            if (b!=result) then raise InconsistentException else [[]]
        )
        | _ -> [[]] (* We cannot infer any other information for the remaining structures *)


(* Verifies that environment is consistent: ak, no two identifiers
 * have a different value assignments *)
let consistent_assignment (environment: (B.identifier * Ast.expr) list) : bool =
    let accum = (true,[]) in
    let rec dedup_fun  = ( fun (cons,accu) env ->
        match env with
            [] -> (cons,accu)
            | (id,assign)::xs ->  (
                    try
                        let res = Util.lookup_id id accu  in
                        if (Util.id_equal res assign) then dedup_fun (cons,accu) xs
                        else (false,accu)
                     with Util.LookupErrException _ ->
                        dedup_fun (cons,(id,assign)::accu) xs
                        (* dedup_fun (cons,[]) xs  *)
            )
    ) in
    let (cons,accu) = dedup_fun accum environment
    in cons


(* Given the value of a boolean expression, generates possible value assignments that can be inferred from the expression having this specific value*)
let generate_consistent_environments (expr: (Ast.expr)) (result:bool) : (B.identifier * Ast.expr) list list =
   let possible_environments = if_expr_has_value expr result in
   (* Add values that are already known *)
   let assigned_env = (!Util.verif_env).assigned_values in
   let all_env= List.map (fun x -> assigned_env@x) possible_environments in
   let rec filter_fn = (fun a b ->
       match a, b with
             [] , [] -> []
            |x::xs, y::ys -> (if (consistent_assignment y = true ) then x::(filter_fn xs ys) else filter_fn xs ys)
            | _ -> raise (Error.Error "Cannot happen")) in
   let result = filter_fn possible_environments all_env in
   if (List.length result = 0) then raise InconsistentException
   else result



(* Verifies that there is no MOVE '=>' in attach *)
let check_attach (exp: Ast.expr) (reach: Ast.stmt Util.reachability option)
(result:bool) : bool =
        let dummy_bool_fun b env:bool = b in
        try (
        (* Compute all possible environments *)
        let environments = generate_consistent_environments exp result in
        (* Internal function check the absence of MOVE '=>' *)
        let test_fn: (Ast.stmt option -> Util.environment -> bool)  = (fun stmt ->
        match stmt with
            Some (Ast.MOVE(src,dest,_,_,_,_,_,_)) -> dummy_bool_fun false
            | _ -> dummy_bool_fun true ) in

        let rec check_fun = ( fun envs ->
            match envs with
            [] -> true
           | x::xs ->  (
                let reach = Util.evaluate_reachability reach  in
                if (reach = None) then true
                 (*I suppose no MOVE is reachable if reach = None *)
                 (*TO DO AYMERIC some testing to be sure !!*)
                else
                    if (result = true) then (
                        let is_reachable = Util.check_all_paths reach test_fn in
                        if (is_reachable = true) then check_fun xs
                        else false
                    )else (
                        let is_reachable = Util.check_no_paths  reach test_fn in
                        if (is_reachable = true) then check_fun xs
                        else false
                    )
           )
         )
         in check_fun environments
        )
        with InconsistentException -> true


(****************** Entry Point **************************)

(* Checks that if attach returns true or void, the
 * process taken as argument will be necessarily
 * placed into READY. Throws exception if this
 * cannot be statically determined.
 *
 * ASSUMPTION: (pre_type.ml ensures that every code path already
 * ends in return*)

 let verify_attach (Ast.FUNDEF(trylock,ty,id,parm,stmts,inl,li)): bool =
    (* Distinguish between two cases: if signature of attach is void, then
     * all paths must place target process in ready, else, if return true,
     * must place attach in ready *)
     let verified =
 
     let is_void = match ty with
        B.VOID -> true
        | B.BOOL -> false
        | _ -> raise (Error.Error "Incorrect attach type. Must be BOOL or VOID")
     in
     (* Previously checked that the first argument of attach will be of type
      * process *)
     let (id,att) = match (List.hd parm) with
                    Ast.VARDECL(_,id,_,_,_,_,att) -> (id,att) in
     let new_var= (Ast.VAR(id,[],att)) in
     let cores =  (fun stmt -> match stmt with
                   Ast.MOVE(src,dest,_,_,_,_,_,_) -> (
                       Util.is_equal src new_var)
                   | _ -> false) in
     let reach_move = Util.extract_move_unique cores stmts in
     (*same as above test to do Aymeric to be sure*)
    if (reach_move = None ) then true
    else 
    if (is_void) then 
        (* If attach is of type void check the absence of MOVE '=>'  *)
        let dummy_bool_fun b env :bool = b in
        let test_fn = ( fun stmt ->
        match stmt with
            Some(Ast.MOVE(src,dest,_,_,_,_,_,_)) ->
             (* for attach_void_move_ko *)
            let _ = raise (Error.Error "MOVE not allowed in attach"); in dummy_bool_fun false
            | _ -> dummy_bool_fun true ) in
        Util.check_all_paths reach_move test_fn
      else  (
        (* Identify all the return statements, and iterate over all such statements both when
         * attach returns true, and when attach returns false*)
        let reach_return = Util.extract_return_unique (fun x -> true) stmts in
        let rec flatten =
            (fun accu reach ->
            match reach with
                Some(Util.REACH(None,env)) -> accu
               | Some (Util.REACH(Some(e),env)) -> accu@[(e,env)]
               | None -> accu
               | Some(Util.FORK(e, branches,_)) -> accu  @ (List.fold_left flatten [] branches)
        )
        in
        (* Extract complete list of boolean expressions returned *)
        let return_list: (Ast.expr * Util.environment) list = flatten [] reach_return in

        (* Must verify that if return true, then will take a path that will place process in ready *)
        let if_true: bool =
            (List.fold_left
                        (fun accu (return_stmt,env) -> if (accu = true) 
                        then (
                            Util.verif_env:=env;
                            check_attach return_stmt reach_move true
                        )
                        else false)
                        true
                        return_list
            )
          in
    
        if (if_true = false) then raise (Error.Error "MOVE not allowed in attach");

         (* Must verify that if return false, then will *not* take a path that will place process in ready *)
        let if_false: bool =
             List.fold_left 
                    (fun accu (return_stmt,env) -> if (accu = true ) 
                    then (
                        Util.verif_env:=env;
                        check_attach return_stmt reach_move false
                    )
                    else false)
                    true
                    return_list in

        if (if_false = false) then raise (Error.Error "Process could be placed in ready despite attach returning false");
        if_true&&if_false
     )
        in verified


