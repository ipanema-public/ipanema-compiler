(*Check that there are no duplicate declarations of variables. The current checking is pessimistic and does not allow shadowing of variables  - ex: one cannot declare foo globally and redeclare foo in a function *)

module B = Objects
module CS = Class_state 

let error ln str =
  Error.update_error();
  Printf.printf "%s: %s \n" (B.loc2c ln) str

let warning ln str =
  Printf.printf "warning: %s: %s \n" (B.loc2c ln) str

let pr = Printf.sprintf

(* Extracts variable declarations from variable definitions *)
let vardefs_to_decls defs=  
 match defs with
      Ast.VALDEF(decl,_,_,_) -> decl
	| Ast.UNINITDEF(decl,_) -> decl
	| Ast.SYSDEF(decl,_,_) -> decl
	| _ -> raise (Error.Error "expected variable definition")
      
(* This function extracts a list of variable identifiers
 * defined or declared in this function *)
let function_to_ids ids (Ast.FUNDEF(_,return_type,identifier,param_list,body_stmt,inline,fun_line)) =
    let Ast.SEQ(defs, stmts, line) = 
      match body_stmt with
        Ast.SEQ(defs,stmts,line) -> Ast.SEQ(defs,stmts,line)
      | _ -> (error fun_line "Unexpected function definition") ; 
    Ast.SEQ([],[],fun_line) in
    let new_vals = List.map vardefs_to_decls defs 
    in ids@new_vals

   
(* This function extracts a list of variable identifiers
 * defined or declared in this handler*)
let handler_to_ids ids (Ast.EVENT(_,_,body_stmt,_,line)) = 
    let Ast.SEQ(defs,stmts, line) = 
      match body_stmt with
        Ast.SEQ(defs,stmts,line) -> Ast.SEQ(defs,stmts,line)
      | _ -> (error line "Unexpected handler definition") ; 
    Ast.SEQ([],[],line) in
    let new_vals = List.map vardefs_to_decls defs 
    in ids@new_vals


(* Entry point *)
let check_duplicates
      (Ast.SCHEDULER(nm,cstdefs,enums,dom,grp,procdecls,fundecls,valdefs,
		     domains,pcstate,cstates, criteria,trace,
		     handlers,chandlers,ifunctions,functions, attr)) =  
(* Compute list of constants and global declarations *)
let valdefs = cstdefs@valdefs in
let alldecls =
    List.map
      (function
 	  Ast.VALDEF(decl,_,_,_)
	| Ast.UNINITDEF(decl,_)
	| Ast.SYSDEF(decl,_,_) -> decl
	| _ -> raise (Error.Error "expected variable definition"))
      valdefs in
(* Compute list of core variables *) 
let corevardecl = match pcstate with
    Ast.CORE(cv,_,_) -> cv
    | Ast.PSTATE(_) -> [] in
let all_variables = alldecls@corevardecl in
(* Compute list of core and process event handlers *)
let events = handlers@chandlers in
(* Compute list of all function variables *)
let functions = ifunctions@functions in
(* Check unicity. Handlers and functions open a new scope *)
List.iter (fun e ->
  let all_variables = handler_to_ids all_variables e in
  Ast.no_dup_params all_variables "variable"
)  events;
List.iter (fun f ->
  let all_variables = function_to_ids all_variables f in
  Ast.no_dup_params all_variables "variable"
)  functions;
Ast.no_dup_params procdecls "variable"
