
type entry = (Class_state.classname * Vobjects.vtype * Vobjects.modified)

type event_class_type =
    (* FIXME: what is isquasi used for *)
    Objects.isquasi * 
        (entry list * Class_state.classname list) list *
        entry list list 

val pevent_specifications : unit -> Ast.event_name list
val cevent_specifications : unit -> Ast.event_name list
(* FIXME: according to transition rules *)
val interface_specifications :
    unit -> (Objects.identifier * event_class_type list) list
 

