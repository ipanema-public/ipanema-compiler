module B = Objects
module VB = Vobjects
module CS = Class_state

type entry = (CS.classname * VB.vtype * VB.modified)

type event_class_type =
    Objects.isquasi *
      (entry list * Class_state.classname list) list *
      entry list list

let initial_event_specifications _ = 
  [Ast.mkEVENT_NAME(["process";"new"],false,-1)]

let system_event_specifications _ = 
  [Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1);
   Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1);
   Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1);
   Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1);
   Ast.mkEVENT_NAME(["preempt"],false,-1);
   Ast.mkEVENT_NAME(["system";"clocktick"],false,-1);
   Ast.mkEVENT_NAME(["compute_state"],false,-1)]

let application_event_specifications _ = 
  [Ast.mkEVENT_NAME(["block"],true,-1);
   Ast.mkEVENT_NAME(["process";"new"],false,-1);
   Ast.mkEVENT_NAME(["unblock";"nonpreemptive"],false,-1);
   Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1);
   Ast.mkEVENT_NAME(["yield";"system";"pause"],true,-1);
   Ast.mkEVENT_NAME(["yield";"system";"immediate"],true,-1);
   Ast.mkEVENT_NAME(["yield";"user"],true,-1);
   Ast.mkEVENT_NAME(["process";"end"],false,-1);
   Ast.mkEVENT_NAME(["bossa";"schedule"],false,-1);
   Ast.mkEVENT_NAME(["preempt"],false,-1);
   Ast.mkEVENT_NAME(["generic_interface"],false,-1);
   Ast.mkEVENT_NAME(["attach"],false,-1);Ast.mkEVENT_NAME(["detach"],false,-1)]

let top_level_events _ = 
  [[Ast.mkEVENT_NAME(["block"],true,-1);
    Ast.mkEVENT_NAME(["bossa";"schedule"],false,-1)];
   [Ast.mkEVENT_NAME(["yield";"system";"immediate"],true,-1);
    Ast.mkEVENT_NAME(["bossa";"schedule"],false,-1)];
   [Ast.mkEVENT_NAME(["yield";"system";"pause"],true,-1);
    Ast.mkEVENT_NAME(["bossa";"schedule"],false,-1)];
   [Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1)];
   [Ast.mkEVENT_NAME(["bossa";"schedule"],false,-1)];
   [Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1)];
   [Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1)];
   [Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1)]]

let scheduler_events _ = 
  [[Ast.mkEVENT_NAME(["system";"clocktick"],false,-1)];
   [Ast.mkEVENT_NAME(["process";"new"],false,-1)];
   [Ast.mkEVENT_NAME(["unblock";"nonpreemptive"],false,-1)];
   [Ast.mkEVENT_NAME(["yield";"user"],true,-1)];
   [Ast.mkEVENT_NAME(["process";"end"],false,-1)]]

let app_events_init_automaton _ =
  [(0,Ast.mkEVENT_NAME(["process";"new"],false,-1),B.INTIBLE,1);
   (1,Ast.mkEVENT_NAME(["bossa";"schedule"],false,-1),B.INTIBLE,2)]

let app_events_automaton _ =
  [(0,Ast.mkEVENT_NAME(["detach"],false,-1),B.INTIBLE,4);
   (0,Ast.mkEVENT_NAME(["attach"],false,-1),B.INTIBLE,4);
   (0,Ast.mkEVENT_NAME(["generic_interface"],false,-1),B.INTIBLE,4);
   (0,Ast.mkEVENT_NAME(["yield";"user"],true,-1),B.INTIBLE,4);
   (0,Ast.mkEVENT_NAME(["unblock";"nonpreemptive"],false,-1),B.INTIBLE,4);
   (0,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,4);
   (0,Ast.mkEVENT_NAME(["process";"new"],false,-1),B.INTIBLE,4);
   (0,Ast.mkEVENT_NAME(["yield";"system";"pause"],true,-1),B.INTIBLE,3);
   (0,Ast.mkEVENT_NAME(["yield";"system";"immediate"],true,-1),B.INTIBLE,3);
   (0,Ast.mkEVENT_NAME(["compute_state"],false,-1),B.INTIBLE,2);
   (0,Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),B.INTIBLE,2);
   (0,Ast.mkEVENT_NAME(["preempt"],false,-1),B.INTIBLE,2);
   (0,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,2);
   (0,Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),B.INTIBLE,2);
   (0,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,2);
   (0,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,2);
   (0,Ast.mkEVENT_NAME(["block"],true,-1),B.INTIBLE,1);
   (4,Ast.mkEVENT_NAME(["compute_state"],false,-1),B.INTIBLE,6);
   (4,Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),B.INTIBLE,6);
   (4,Ast.mkEVENT_NAME(["preempt"],false,-1),B.INTIBLE,6);
   (4,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,6);
   (4,Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),B.INTIBLE,6);
   (4,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,6);
   (4,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,6);
   (4,Ast.mkEVENT_NAME(["bossa";"schedule"],false,-1),B.INTIBLE,5);
   (6,Ast.mkEVENT_NAME(["compute_state"],false,-1),B.INTIBLE,6);
   (6,Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),B.INTIBLE,6);
   (6,Ast.mkEVENT_NAME(["preempt"],false,-1),B.INTIBLE,6);
   (6,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,6);
   (6,Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),B.INTIBLE,6);
   (6,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,6);
   (6,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,6);
   (6,Ast.mkEVENT_NAME(["bossa";"schedule"],false,-1),B.INTIBLE,5);
   (5,Ast.mkEVENT_NAME(["preempt"],false,-1),B.UNINTIBLE,7);
   (5,Ast.mkEVENT_NAME(["detach"],false,-1),B.INTIBLE,4);
   (5,Ast.mkEVENT_NAME(["attach"],false,-1),B.INTIBLE,4);
   (5,Ast.mkEVENT_NAME(["generic_interface"],false,-1),B.INTIBLE,4);
   (5,Ast.mkEVENT_NAME(["yield";"user"],true,-1),B.INTIBLE,4);
   (5,Ast.mkEVENT_NAME(["unblock";"nonpreemptive"],false,-1),B.INTIBLE,4);
   (5,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,4);
   (5,Ast.mkEVENT_NAME(["process";"new"],false,-1),B.INTIBLE,4);
   (5,Ast.mkEVENT_NAME(["yield";"system";"pause"],true,-1),B.INTIBLE,3);
   (5,Ast.mkEVENT_NAME(["yield";"system";"immediate"],true,-1),B.INTIBLE,3);
   (5,Ast.mkEVENT_NAME(["compute_state"],false,-1),B.INTIBLE,2);
   (5,Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),B.INTIBLE,2);
   (5,Ast.mkEVENT_NAME(["preempt"],false,-1),B.INTIBLE,2);
   (5,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,2);
   (5,Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),B.INTIBLE,2);
   (5,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,2);
   (5,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,2);
   (5,Ast.mkEVENT_NAME(["block"],true,-1),B.INTIBLE,1);
   (7,Ast.mkEVENT_NAME(["bossa";"schedule"],false,-1),B.INTIBLE,0);
   (7,Ast.mkEVENT_NAME(["detach"],false,-1),B.INTIBLE,4);
   (7,Ast.mkEVENT_NAME(["attach"],false,-1),B.INTIBLE,4);
   (7,Ast.mkEVENT_NAME(["generic_interface"],false,-1),B.INTIBLE,4);
   (7,Ast.mkEVENT_NAME(["yield";"user"],true,-1),B.INTIBLE,4);
   (7,Ast.mkEVENT_NAME(["unblock";"nonpreemptive"],false,-1),B.INTIBLE,4);
   (7,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,4);
   (7,Ast.mkEVENT_NAME(["process";"new"],false,-1),B.INTIBLE,4);
   (7,Ast.mkEVENT_NAME(["yield";"system";"pause"],true,-1),B.INTIBLE,3);
   (7,Ast.mkEVENT_NAME(["yield";"system";"immediate"],true,-1),B.INTIBLE,3);
   (7,Ast.mkEVENT_NAME(["compute_state"],false,-1),B.INTIBLE,8);
   (7,Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),B.INTIBLE,8);
   (7,Ast.mkEVENT_NAME(["preempt"],false,-1),B.INTIBLE,8);
   (7,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,8);
   (7,Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),B.INTIBLE,8);
   (7,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,8);
   (7,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,8);
   (7,Ast.mkEVENT_NAME(["block"],true,-1),B.INTIBLE,1);
   (8,Ast.mkEVENT_NAME(["detach"],false,-1),B.INTIBLE,4);
   (8,Ast.mkEVENT_NAME(["attach"],false,-1),B.INTIBLE,4);
   (8,Ast.mkEVENT_NAME(["generic_interface"],false,-1),B.INTIBLE,4);
   (8,Ast.mkEVENT_NAME(["yield";"user"],true,-1),B.INTIBLE,4);
   (8,Ast.mkEVENT_NAME(["unblock";"nonpreemptive"],false,-1),B.INTIBLE,4);
   (8,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,4);
   (8,Ast.mkEVENT_NAME(["process";"new"],false,-1),B.INTIBLE,4);
   (8,Ast.mkEVENT_NAME(["bossa";"schedule"],false,-1),B.INTIBLE,0);
   (8,Ast.mkEVENT_NAME(["yield";"system";"pause"],true,-1),B.INTIBLE,3);
   (8,Ast.mkEVENT_NAME(["yield";"system";"immediate"],true,-1),B.INTIBLE,3);
   (8,Ast.mkEVENT_NAME(["compute_state"],false,-1),B.INTIBLE,8);
   (8,Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),B.INTIBLE,8);
   (8,Ast.mkEVENT_NAME(["preempt"],false,-1),B.INTIBLE,8);
   (8,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,8);
   (8,Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),B.INTIBLE,8);
   (8,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,8);
   (8,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,8);
   (8,Ast.mkEVENT_NAME(["block"],true,-1),B.INTIBLE,1);
   (3,Ast.mkEVENT_NAME(["bossa";"schedule"],false,-1),B.UNINTIBLE,9);
   (9,Ast.mkEVENT_NAME(["preempt"],false,-1),B.UNINTIBLE,7);
   (9,Ast.mkEVENT_NAME(["detach"],false,-1),B.INTIBLE,4);
   (9,Ast.mkEVENT_NAME(["attach"],false,-1),B.INTIBLE,4);
   (9,Ast.mkEVENT_NAME(["generic_interface"],false,-1),B.INTIBLE,4);
   (9,Ast.mkEVENT_NAME(["yield";"user"],true,-1),B.INTIBLE,4);
   (9,Ast.mkEVENT_NAME(["unblock";"nonpreemptive"],false,-1),B.INTIBLE,4);
   (9,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,4);
   (9,Ast.mkEVENT_NAME(["process";"new"],false,-1),B.INTIBLE,4);
   (9,Ast.mkEVENT_NAME(["yield";"system";"pause"],true,-1),B.INTIBLE,3);
   (9,Ast.mkEVENT_NAME(["yield";"system";"immediate"],true,-1),B.INTIBLE,3);
   (9,Ast.mkEVENT_NAME(["compute_state"],false,-1),B.INTIBLE,2);
   (9,Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),B.INTIBLE,2);
   (9,Ast.mkEVENT_NAME(["preempt"],false,-1),B.INTIBLE,2);
   (9,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,2);
   (9,Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),B.INTIBLE,2);
   (9,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,2);
   (9,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,2);
   (9,Ast.mkEVENT_NAME(["block"],true,-1),B.INTIBLE,1);
   (2,Ast.mkEVENT_NAME(["detach"],false,-1),B.INTIBLE,4);
   (2,Ast.mkEVENT_NAME(["attach"],false,-1),B.INTIBLE,4);
   (2,Ast.mkEVENT_NAME(["generic_interface"],false,-1),B.INTIBLE,4);
   (2,Ast.mkEVENT_NAME(["yield";"user"],true,-1),B.INTIBLE,4);
   (2,Ast.mkEVENT_NAME(["unblock";"nonpreemptive"],false,-1),B.INTIBLE,4);
   (2,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,4);
   (2,Ast.mkEVENT_NAME(["process";"new"],false,-1),B.INTIBLE,4);
   (2,Ast.mkEVENT_NAME(["yield";"system";"pause"],true,-1),B.INTIBLE,3);
   (2,Ast.mkEVENT_NAME(["yield";"system";"immediate"],true,-1),B.INTIBLE,3);
   (2,Ast.mkEVENT_NAME(["compute_state"],false,-1),B.INTIBLE,2);
   (2,Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),B.INTIBLE,2);
   (2,Ast.mkEVENT_NAME(["preempt"],false,-1),B.INTIBLE,2);
   (2,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,2);
   (2,Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),B.INTIBLE,2);
   (2,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,2);
   (2,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,2);
   (2,Ast.mkEVENT_NAME(["block"],true,-1),B.INTIBLE,1);
   (1,Ast.mkEVENT_NAME(["compute_state"],false,-1),B.INTIBLE,12);
   (1,Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),B.INTIBLE,12);
   (1,Ast.mkEVENT_NAME(["preempt"],false,-1),B.INTIBLE,12);
   (1,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,12);
   (1,Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),B.INTIBLE,12);
   (1,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,12);
   (1,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,12);
   (1,Ast.mkEVENT_NAME(["process";"end"],false,-1),B.INTIBLE,11);
   (1,Ast.mkEVENT_NAME(["bossa";"schedule"],false,-1),B.UNINTIBLE,10);
   (12,Ast.mkEVENT_NAME(["compute_state"],false,-1),B.INTIBLE,12);
   (12,Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),B.INTIBLE,12);
   (12,Ast.mkEVENT_NAME(["preempt"],false,-1),B.INTIBLE,12);
   (12,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,12);
   (12,Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),B.INTIBLE,12);
   (12,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,12);
   (12,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,12);
   (12,Ast.mkEVENT_NAME(["process";"end"],false,-1),B.INTIBLE,13);
   (13,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,16);
   (13,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.UNINTIBLE,14);
   (13,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,15);
   (13,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.UNINTIBLE,14);
   (16,Ast.mkEVENT_NAME(["compute_state"],false,-1),B.INTIBLE,17);
   (16,Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),B.INTIBLE,17);
   (16,Ast.mkEVENT_NAME(["preempt"],false,-1),B.INTIBLE,17);
   (16,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,17);
   (16,Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),B.INTIBLE,17);
   (16,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,17);
   (16,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,17);
   (17,Ast.mkEVENT_NAME(["compute_state"],false,-1),B.INTIBLE,17);
   (17,Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),B.INTIBLE,17);
   (17,Ast.mkEVENT_NAME(["preempt"],false,-1),B.INTIBLE,17);
   (17,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,17);
   (17,Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),B.INTIBLE,17);
   (17,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,17);
   (17,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,17);
   (15,Ast.mkEVENT_NAME(["compute_state"],false,-1),B.INTIBLE,18);
   (15,Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),B.INTIBLE,18);
   (15,Ast.mkEVENT_NAME(["preempt"],false,-1),B.INTIBLE,18);
   (15,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,18);
   (15,Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),B.INTIBLE,18);
   (15,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,18);
   (15,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,18);
   (18,Ast.mkEVENT_NAME(["compute_state"],false,-1),B.INTIBLE,18);
   (18,Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),B.INTIBLE,18);
   (18,Ast.mkEVENT_NAME(["preempt"],false,-1),B.INTIBLE,18);
   (18,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,18);
   (18,Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),B.INTIBLE,18);
   (18,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,18);
   (18,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,18);
   (11,Ast.mkEVENT_NAME(["compute_state"],false,-1),B.INTIBLE,19);
   (11,Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),B.INTIBLE,19);
   (11,Ast.mkEVENT_NAME(["preempt"],false,-1),B.INTIBLE,19);
   (11,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,19);
   (11,Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),B.INTIBLE,19);
   (11,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,19);
   (11,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,19);
   (11,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,16);
   (11,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.UNINTIBLE,14);
   (11,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,15);
   (11,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.UNINTIBLE,14);
   (19,Ast.mkEVENT_NAME(["compute_state"],false,-1),B.INTIBLE,19);
   (19,Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),B.INTIBLE,19);
   (19,Ast.mkEVENT_NAME(["preempt"],false,-1),B.INTIBLE,19);
   (19,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,19);
   (19,Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),B.INTIBLE,19);
   (19,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,19);
   (19,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,19);
   (10,Ast.mkEVENT_NAME(["preempt"],false,-1),B.UNINTIBLE,21);
   (10,Ast.mkEVENT_NAME(["process";"end"],false,-1),B.INTIBLE,11);
   (10,Ast.mkEVENT_NAME(["detach"],false,-1),B.INTIBLE,4);
   (10,Ast.mkEVENT_NAME(["attach"],false,-1),B.INTIBLE,4);
   (10,Ast.mkEVENT_NAME(["generic_interface"],false,-1),B.INTIBLE,4);
   (10,Ast.mkEVENT_NAME(["yield";"user"],true,-1),B.INTIBLE,4);
   (10,Ast.mkEVENT_NAME(["unblock";"nonpreemptive"],false,-1),B.INTIBLE,4);
   (10,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,4);
   (10,Ast.mkEVENT_NAME(["process";"new"],false,-1),B.INTIBLE,4);
   (10,Ast.mkEVENT_NAME(["yield";"system";"pause"],true,-1),B.INTIBLE,3);
   (10,Ast.mkEVENT_NAME(["yield";"system";"immediate"],true,-1),B.INTIBLE,3);
   (10,Ast.mkEVENT_NAME(["compute_state"],false,-1),B.INTIBLE,20);
   (10,Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),B.INTIBLE,20);
   (10,Ast.mkEVENT_NAME(["preempt"],false,-1),B.INTIBLE,20);
   (10,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,20);
   (10,Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),B.INTIBLE,20);
   (10,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,20);
   (10,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,20);
   (10,Ast.mkEVENT_NAME(["block"],true,-1),B.INTIBLE,1);
   (21,Ast.mkEVENT_NAME(["bossa";"schedule"],false,-1),B.INTIBLE,0);
   (21,Ast.mkEVENT_NAME(["process";"end"],false,-1),B.INTIBLE,11);
   (21,Ast.mkEVENT_NAME(["detach"],false,-1),B.INTIBLE,4);
   (21,Ast.mkEVENT_NAME(["attach"],false,-1),B.INTIBLE,4);
   (21,Ast.mkEVENT_NAME(["generic_interface"],false,-1),B.INTIBLE,4);
   (21,Ast.mkEVENT_NAME(["yield";"user"],true,-1),B.INTIBLE,4);
   (21,Ast.mkEVENT_NAME(["unblock";"nonpreemptive"],false,-1),B.INTIBLE,4);
   (21,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,4);
   (21,Ast.mkEVENT_NAME(["process";"new"],false,-1),B.INTIBLE,4);
   (21,Ast.mkEVENT_NAME(["yield";"system";"pause"],true,-1),B.INTIBLE,3);
   (21,Ast.mkEVENT_NAME(["yield";"system";"immediate"],true,-1),B.INTIBLE,3);
   (21,Ast.mkEVENT_NAME(["compute_state"],false,-1),B.INTIBLE,22);
   (21,Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),B.INTIBLE,22);
   (21,Ast.mkEVENT_NAME(["preempt"],false,-1),B.INTIBLE,22);
   (21,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,22);
   (21,Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),B.INTIBLE,22);
   (21,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,22);
   (21,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,22);
   (21,Ast.mkEVENT_NAME(["block"],true,-1),B.INTIBLE,1);
   (22,Ast.mkEVENT_NAME(["detach"],false,-1),B.INTIBLE,4);
   (22,Ast.mkEVENT_NAME(["attach"],false,-1),B.INTIBLE,4);
   (22,Ast.mkEVENT_NAME(["generic_interface"],false,-1),B.INTIBLE,4);
   (22,Ast.mkEVENT_NAME(["yield";"user"],true,-1),B.INTIBLE,4);
   (22,Ast.mkEVENT_NAME(["unblock";"nonpreemptive"],false,-1),B.INTIBLE,4);
   (22,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,4);
   (22,Ast.mkEVENT_NAME(["process";"new"],false,-1),B.INTIBLE,4);
   (22,Ast.mkEVENT_NAME(["bossa";"schedule"],false,-1),B.INTIBLE,0);
   (22,Ast.mkEVENT_NAME(["yield";"system";"pause"],true,-1),B.INTIBLE,3);
   (22,Ast.mkEVENT_NAME(["yield";"system";"immediate"],true,-1),B.INTIBLE,3);
   (22,Ast.mkEVENT_NAME(["process";"end"],false,-1),B.INTIBLE,13);
   (22,Ast.mkEVENT_NAME(["compute_state"],false,-1),B.INTIBLE,22);
   (22,Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),B.INTIBLE,22);
   (22,Ast.mkEVENT_NAME(["preempt"],false,-1),B.INTIBLE,22);
   (22,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,22);
   (22,Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),B.INTIBLE,22);
   (22,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,22);
   (22,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,22);
   (22,Ast.mkEVENT_NAME(["block"],true,-1),B.INTIBLE,1);
   (20,Ast.mkEVENT_NAME(["detach"],false,-1),B.INTIBLE,4);
   (20,Ast.mkEVENT_NAME(["attach"],false,-1),B.INTIBLE,4);
   (20,Ast.mkEVENT_NAME(["generic_interface"],false,-1),B.INTIBLE,4);
   (20,Ast.mkEVENT_NAME(["yield";"user"],true,-1),B.INTIBLE,4);
   (20,Ast.mkEVENT_NAME(["unblock";"nonpreemptive"],false,-1),B.INTIBLE,4);
   (20,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,4);
   (20,Ast.mkEVENT_NAME(["process";"new"],false,-1),B.INTIBLE,4);
   (20,Ast.mkEVENT_NAME(["yield";"system";"pause"],true,-1),B.INTIBLE,3);
   (20,Ast.mkEVENT_NAME(["yield";"system";"immediate"],true,-1),B.INTIBLE,3);
   (20,Ast.mkEVENT_NAME(["process";"end"],false,-1),B.INTIBLE,13);
   (20,Ast.mkEVENT_NAME(["compute_state"],false,-1),B.INTIBLE,20);
   (20,Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),B.INTIBLE,20);
   (20,Ast.mkEVENT_NAME(["preempt"],false,-1),B.INTIBLE,20);
   (20,Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),B.INTIBLE,20);
   (20,Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),B.INTIBLE,20);
   (20,Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),B.INTIBLE,20);
   (20,Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),B.INTIBLE,20);
   (20,Ast.mkEVENT_NAME(["block"],true,-1),B.INTIBLE,1)]

let event_specifications _ =
  [(Ast.mkEVENT_NAME(["attach"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);
         (CS.RUNNING, VB.UNKNOWN, VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
        VB.UNMODIFIED);(CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["block"],true,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);(CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["bossa";"schedule"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]),
         VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.P(0),[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
        VB.MODIFIED);(CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["compute_state"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED); (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED); (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["detach"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.MEMBER([VB.VP(VB.TGT,[])]),
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.MEMBER([VB.VP(VB.TGT,[])]),
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.MEMBER([VB.VP(VB.TGT,[])]),
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.MEMBER([VB.VP(VB.TGT,[])]),
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["generic_interface"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED); (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED); (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
          (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED); (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["preempt"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["process";"end"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.MEMBER([VB.VP(VB.TGT,[])]),
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.MEMBER([VB.VP(VB.TGT,[])]),
        VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["process";"new"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);
         (CS.RUNNING, VB.MEMBER([VB.VP(VB.SRC,[])]), VB.UNMODIFIED);(CS.READY,
         VB.UNKNOWN, VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
        VB.UNMODIFIED); (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);
         (CS.RUNNING, VB.MEMBER([VB.VP(VB.SRC,[])]), VB.UNMODIFIED);(CS.READY,
         VB.UNKNOWN, VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.SRC,[]);VB.VP(VB.TGT,[])]),
        VB.UNMODIFIED); (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);
         (CS.RUNNING, VB.EMPTY, VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
        VB.UNMODIFIED); (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.QUASI,
      [([(CS.NOWHERE, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);
         (CS.RUNNING, VB.EMPTY, VB.UNMODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.SRC,[])]), VB.UNMODIFIED);
         (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
        VB.UNMODIFIED); (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.QUASI,
      [([(CS.NOWHERE, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);
         (CS.RUNNING, VB.EMPTY, VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
        VB.UNMODIFIED);(CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["unblock";"nonpreemptive"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
        VB.UNMODIFIED); (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.UNMODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
        VB.UNMODIFIED); (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);         (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]),
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED); (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED); (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED); (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]),
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(1),[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(1),[]);VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED, VB.MEMBER([VB.VP(VB.TGT,[])]),
         VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED); (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED); (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED); (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
          (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED); (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.UNMODIFIED);
         (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.TGT,[]);VB.VP(VB.P(0),[])]), VB.UNMODIFIED);
         (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]),
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]),
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]),
         VB.UNMODIFIED); (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED);
         (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
         VB.UNMODIFIED);
         (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(1),[])]), VB.UNMODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);
         (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(1),[]);VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(1),[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(1),[]);VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(1),[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED);
         (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(1),[]);VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(1),[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(1),[]);VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.TGT,[]);VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.UNMODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);
         (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED, VB.MEMBER([VB.VP(VB.TGT,[])]),
         VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED); (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.UNMODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);
         (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["yield";"system";"immediate"],true,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
        VB.UNMODIFIED); (CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["yield";"system";"pause"],true,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
        VB.UNMODIFIED);(CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["yield";"user"],true,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.UNMODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]),
        VB.UNMODIFIED);(CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]]);
      (B.QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]])])]

let vs_event_specifications _ =
  [(Ast.mkEVENT_NAME(["attach"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.RUNNING])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["block"],true,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["bossa";"schedule"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.RUNNING])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["compute_state"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY;CS.RUNNING])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.READY;CS.BLOCKED])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.READY;CS.BLOCKED])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY;CS.RUNNING])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.READY;CS.BLOCKED])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY;CS.RUNNING])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.READY;CS.BLOCKED])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["detach"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.RUNNING;CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.RUNNING;CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.RUNNING;CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["generic_interface"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY;CS.RUNNING])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.READY;CS.BLOCKED])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.READY;CS.BLOCKED])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY;CS.RUNNING])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.READY;CS.BLOCKED])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY;CS.RUNNING])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.READY;CS.BLOCKED])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["preempt"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["process";"end"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.RUNNING])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["process";"new"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.EQUAL([VB.VP(VB.SRC,[]); VB.VP(VB.TGT,[])])]),
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.RUNNING;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.EQUAL([VB.VP(VB.SRC,[]); VB.VP(VB.TGT,[])])]),
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.EQUAL([VB.VP(VB.SRC,[]); VB.VP(VB.TGT,[])])]),
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.RUNNING;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.EQUAL([VB.VP(VB.SRC,[]); VB.VP(VB.TGT,[])])]),
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.EQUAL([VB.VP(VB.SRC,[]); VB.VP(VB.TGT,[])])]),
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.EQUAL([VB.VP(VB.SRC,[]); VB.VP(VB.TGT,[])])]),
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.EQUAL([VB.VP(VB.SRC,[]); VB.VP(VB.TGT,[])])]),
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.EQUAL([VB.VP(VB.SRC,[]); VB.VP(VB.TGT,[])])]),
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["system";"clocktick"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.RUNNING;CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.RUNNING;CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.RUNNING;CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["unblock";"nonpreemptive"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.READY;CS.BLOCKED])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.RUNNING])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.READY;CS.BLOCKED])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["unblock";"preemptive"],false,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.READY;CS.BLOCKED])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.READY;CS.BLOCKED])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.READY;CS.RUNNING])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.READY;CS.BLOCKED])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.READY;CS.RUNNING])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.READY;CS.BLOCKED])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["unblock";"timer";"notarget"],true,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.READY;CS.RUNNING])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.READY;CS.RUNNING])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["unblock";"timer";"persistent"],true,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.EQUAL([VB.VP(VB.TGT,[]); VB.VP(VB.UNKNOWNPROC,[])])]),
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.EQUAL([VB.VP(VB.TGT,[]); VB.VP(VB.UNKNOWNPROC,[])])]),
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.RUNNING;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
        VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.RUNNING;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.EQUAL([VB.VP(VB.TGT,[]); VB.VP(VB.UNKNOWNPROC,[])])]),
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.RUNNING;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.P(0),[])]),
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.RUNNING;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[]);VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.TGT,[]);VB.VP(VB.P(0),[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.EQUAL([VB.VP(VB.TGT,[]); VB.VP(VB.UNKNOWNPROC,[])]);
                   VB.VP(VB.P(0),[])]),
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.EQUAL([VB.VP(VB.TGT,[]); VB.VP(VB.UNKNOWNPROC,[])])]),
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.EQUAL([VB.VP(VB.TGT,[]); VB.VP(VB.UNKNOWNPROC,[])])]),
        VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.TGT,[]);VB.VP(VB.P(0),[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
        VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[]);VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.RUNNING;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.EQUAL([VB.VP(VB.TGT,[]); VB.VP(VB.UNKNOWNPROC,[])])]),
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.RUNNING;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.TGT,[]);VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[]);VB.VP(VB.P(0),[])]), VB.MODIFIED);
         (CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.RUNNING;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.EQUAL([VB.VP(VB.TGT,[]); VB.VP(VB.UNKNOWNPROC,[])]);
                   VB.VP(VB.P(0),[])]),
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[]);VB.VP(VB.P(0),[])]), VB.MODIFIED);
         (CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.RUNNING;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
        VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.P(0),[])]),
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.RUNNING;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.EQUAL([VB.VP(VB.TGT,[]); VB.VP(VB.UNKNOWNPROC,[])])]),
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.P(0),[])]),
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.RUNNING;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.EQUAL([VB.VP(VB.TGT,[]); VB.VP(VB.UNKNOWNPROC,[])]);
                   VB.VP(VB.P(0),[])]),
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.EQUAL([VB.VP(VB.TGT,[]); VB.VP(VB.UNKNOWNPROC,[])])]),
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[]);VB.VP(VB.P(0),[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.EQUAL([VB.VP(VB.TGT,[]); VB.VP(VB.UNKNOWNPROC,[])])]),
        VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.TGT,[]);VB.VP(VB.P(0),[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.TGT,[]);VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.RUNNING;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
        VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
        VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[]);VB.VP(VB.P(0),[])]), VB.MODIFIED);
         (CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.RUNNING;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.TGT,[]);VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
        VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
        VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.P(0),[])]),
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.RUNNING;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
        VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[]);VB.VP(VB.P(0),[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[]);VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(1),[])]), VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(1),[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(1),[])]), VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(1),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(1),[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(1),[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(1),[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(1),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(0),[]);
                   VB.EQUAL([VB.VP(VB.TGT,[]); VB.VP(VB.UNKNOWNPROC,[])])]),
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.RUNNING;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(0),[]);
                   VB.EQUAL([VB.VP(VB.TGT,[]); VB.VP(VB.UNKNOWNPROC,[])])]),
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.RUNNING;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.RUNNING;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["unblock";"timer";"target"],true,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY;CS.RUNNING])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.P(0),[])]), VB.MODIFIED);(CS.READY,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN,
         VB.MODIFIED);(CS.TERMINATED, VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY,
        VB.MEMBER([VB.VP(VB.P(0),[]);VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY;CS.RUNNING])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY;CS.RUNNING])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
        VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
        VB.EMPTY, VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED;CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["yield";"system";"immediate"],true,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["yield";"system";"pause"],true,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]])]);
   (Ast.mkEVENT_NAME(["yield";"user"],true,-1),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.READY, VB.UNKNOWN,
         VB.MODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED,
         VB.EMPTY, VB.UNMODIFIED)],
       [CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
         (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.READY])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);
        (CS.BLOCKED, VB.UNKNOWN, VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
         VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
         VB.UNMODIFIED)],
       [CS.BLOCKED])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.MODIFIED);(CS.BLOCKED,
        VB.MEMBER([VB.VP(VB.TGT,[])]), VB.MODIFIED);(CS.TERMINATED, VB.EMPTY,
        VB.UNMODIFIED)]])])]

let interface_specifications _ =
  [(B.mkId("attach"),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.MEMBER([VB.VP(VB.ARG(1),[])]), VB.UNMODIFIED);
         (CS.RUNNING, VB.UNKNOWN, VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED); (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.ARG(1),[])]),
        VB.UNMODIFIED);(CS.TERMINATED,
        VB.UNKNOWN, VB.UNMODIFIED)]])]);
   (B.mkId("detach"),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.MEMBER([VB.VP(VB.ARG(1),[])]),
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.MEMBER([VB.VP(VB.ARG(1),[])]),
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.MEMBER([VB.VP(VB.ARG(1),[])]),
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.ARG(1),[])]),
         VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.MEMBER([VB.VP(VB.ARG(1),[])]),
        VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.ARG(1),[])]), VB.UNMODIFIED);(CS.READY,
         VB.UNKNOWN, VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);
        (CS.TERMINATED, VB.MEMBER([VB.VP(VB.ARG(1),[])]), VB.UNMODIFIED)]])])]

let vs_interface_specifications _ =
  [(B.mkId("attach"),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.MEMBER([VB.VP(VB.ARG(1),[])]), VB.UNMODIFIED);
         (CS.RUNNING, VB.UNKNOWN, VB.UNMODIFIED);(CS.READY, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);(CS.BLOCKED,
        VB.UNKNOWN, VB.UNMODIFIED);(CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)]])]);
   (B.mkId("detach"),
     [(B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING,
         VB.MEMBER([VB.VP(VB.ARG(1),[])]), VB.UNMODIFIED);(CS.READY,
         VB.UNKNOWN, VB.UNMODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.UNMODIFIED);
         (CS.TERMINATED, VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.EMPTY,
        VB.MODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);(CS.BLOCKED,
        VB.UNKNOWN, VB.UNMODIFIED);(CS.TERMINATED,
        VB.MEMBER([VB.VP(VB.ARG(1),[])]), VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.MEMBER([VB.VP(VB.ARG(1),[])]),
         VB.UNMODIFIED);(CS.BLOCKED, VB.UNKNOWN, VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);(CS.BLOCKED,
        VB.UNKNOWN, VB.UNMODIFIED);(CS.TERMINATED,
        VB.MEMBER([VB.VP(VB.ARG(1),[])]), VB.UNMODIFIED)]]);
      (B.NOT_QUASI,
      [([(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
         VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);(CS.BLOCKED,
         VB.MEMBER([VB.VP(VB.ARG(1),[])]), VB.UNMODIFIED);(CS.TERMINATED,
         VB.UNKNOWN, VB.UNMODIFIED)],
       [])],
      [[(CS.NOWHERE, VB.UNKNOWN, VB.UNMODIFIED);(CS.RUNNING, VB.UNKNOWN,
        VB.UNMODIFIED);(CS.READY, VB.UNKNOWN, VB.UNMODIFIED);(CS.BLOCKED,
        VB.UNKNOWN, VB.UNMODIFIED);(CS.TERMINATED,
        VB.MEMBER([VB.VP(VB.ARG(1),[])]), VB.UNMODIFIED)]])])]
