(* $Id$ *)

module B = Objects
module CS = Class_state

(* The purpose of this phase is to normalize the program into a form that
is convenient for verification.  This includes in particular extracting
expressions that are relevant for verification from compound conditional
tests.  This process can result in code duplication.  A corresponding
post-processing phase eliminates this duplication when the same code
results from the duplicated branches after the analysis. *)

(* One pass, bottom-up detection of test expressions relevant to
verification *)

type skel =
    Neither of bool | Left of skel | Right of skel | Both of skel * skel
  | One of skel

let pc_aux = function
    ((true,skel1),(true,skel2)) ->   (true,Both(skel1,skel2))
  | ((true,skel1),(false,skel2)) ->  (true,Left(skel1))
  | ((false,skel1),(true,skel2)) ->  (true,Right(skel2))
  | ((false,skel1),(false,skel2)) -> (false,Neither(false))

let rec parse_cond verifiable = function
    Ast.BINARY(Ast.AND,exp1,exp2,attr) ->
      pc_aux (parse_cond verifiable exp1,parse_cond verifiable exp2)
  | Ast.BINARY(Ast.OR,exp1,exp2,attr) ->
      pc_aux (parse_cond verifiable exp1,parse_cond verifiable exp2)
  | Ast.UNARY(Ast.NOT,exp,attr) ->
      (match parse_cond verifiable exp with
	(b,Neither(false)) -> (b,Neither(false))
      |	(b,x) -> (b,(One x)))
  | exp -> let res = verifiable exp in (res, Neither(res))

(* Reorganize a conditional so that interesting tests are not negated and are by themselves *)

let rec reassociate_cond s1 s2 attr skel = function
    ((Ast.BINARY(Ast.AND,exp1,exp2,attr)) as exp) ->
      (match skel with
	Both(skel1,skel2) ->
	  reassociate_cond (reassociate_cond s1 s2 attr skel2 exp2)
	    s2 attr skel1 exp1
      |	Left(skel1) ->
	  reassociate_cond (Ast.IF(exp2, s1, s2, attr)) s2 attr skel1 exp1
      |	Right(skel2) ->
	  Ast.IF(exp1, reassociate_cond s1 s2 attr skel2 exp2, s2, attr)
      |	_ -> Ast.IF(exp, s1, s2, attr)) (* must be Neither(_) *)

  | ((Ast.BINARY(Ast.OR,exp1,exp2,attr)) as exp) ->
    (match skel with
      Both(skel1,skel2) ->
        reassociate_cond s1 (reassociate_cond s1 s2 attr skel2 exp2)
	  attr skel1 exp1
    | Left(skel1) ->
	reassociate_cond s1 (Ast.IF(exp2, s1, s2, attr)) attr skel1 exp1
    | Right(skel2) ->
	Ast.IF(exp1, s1, reassociate_cond s1 s2 attr skel2 exp2, attr)
    | _ -> Ast.IF(exp, s1, s2, attr)) (* must be Neither(_) *)
  | Ast.UNARY(Ast.NOT,exp,attr) as oexp ->
      (match skel with
	Neither(true) -> reassociate_cond s2 s1 attr skel exp
      |	One(skel) -> reassociate_cond s2 s1 attr skel exp
      |	_ -> Ast.IF(oexp, s1, s2, attr))
  | exp -> Ast.IF(exp, s1, s2, attr)

(* -------------------- Expand empty class test --------------------- *)

let rec orify op attr = function
    [] -> Printf.printf "%s: " (B.loc2c attr);
	raise (Error.Error "orify: internal error 2")
  | [x] -> x
  | x::xs -> Ast.BINARY(op,x,orify op attr xs,attr)

let rec expand_empty = function
    Ast.BINARY(Ast.AND,exp1,exp2,attr) ->
      Ast.BINARY(Ast.AND,expand_empty exp1,expand_empty exp2,attr)
  | Ast.BINARY(Ast.OR,exp1,exp2,attr) ->
      Ast.BINARY(Ast.OR,expand_empty exp1,expand_empty exp2,attr)
  | Ast.UNARY(Ast.NOT,exp,attr) ->
      Ast.UNARY(Ast.NOT,expand_empty exp,attr)
  | Ast.EMPTY(id,[],tv,_,attr) as x -> x
  | Ast.EMPTY(id,[states],tv,_,attr) as x -> x
  | Ast.EMPTY(id,states,tv,_(*false (* not from src prog *)*),attr) ->
      orify Ast.AND attr
	(List.map
	   (function
	       (CS.STATE(id,_,_,_) as s) -> Ast.EMPTY(id,[s],tv,false,attr))
	   states)
  | exp -> exp

(* -------- Search for conditionals that need reorganization -------- *)

let rec preprocess_stmt verifiable = function
    Ast.IF(exp,stm1,stm2,attr) ->
      let exp = expand_empty exp in
      let (_,skel) = parse_cond verifiable exp in
      reassociate_cond
	(preprocess_stmt verifiable stm1) (preprocess_stmt verifiable stm2)
	attr skel exp

  | Ast.FOR(id,stid,l,dir,stm,crit,attr) ->
      Ast.FOR(id,stid,l,dir,preprocess_stmt verifiable stm,crit,attr)

  | Ast.FOR_WRAPPER(label,stms,attr) ->
      Ast.FOR_WRAPPER(label,List.map (preprocess_stmt verifiable) stms,attr)

  | Ast.SWITCH(exp,cases,default,attr) ->
      Ast.SWITCH(exp,
		 List.map
		   (function Ast.SEQ_CASE(pat,l,stm,attr) ->
		     Ast.SEQ_CASE(pat,l,preprocess_stmt verifiable stm,attr))
		   cases,
		 Aux.app_option (preprocess_stmt verifiable) default,
		 attr)

  | Ast.SEQ(decls,stmts,attr) ->
      Ast.SEQ(decls,List.map (preprocess_stmt verifiable) stmts,attr)

  | stmt -> stmt

(* --------------------- Handlers and functions --------------------- *)

let preprocess_handlers handlers verifiable =
  List.map
    (function (Ast.EVENT(nm,param,stmt,attr)) ->
      Ast.EVENT(nm,param,preprocess_stmt verifiable stmt,attr))
    handlers

let preprocess_functions functions verifiable =
  List.map
    (function (Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr)) ->
      Ast.FUNDEF(tl,ret,nm,params,preprocess_stmt verifiable stmt,inl,attr))
    functions

let preprocess verifiable
    (Ast.SCHEDULER(nm,
		   cstdefs,enums,dom,grp,procdecls,
		   fundecls,valdefs,domains,states,cstates,criteria,
		   trace,phandlers,chandlers,ifunctions,functions,attr)) =
  (Ast.SCHEDULER(nm,
		 cstdefs,enums,dom,grp,procdecls,
		 fundecls,valdefs,domains,states,cstates,criteria,
		 trace,
		 preprocess_handlers phandlers verifiable,
		 preprocess_handlers chandlers verifiable,
		 preprocess_functions ifunctions verifiable,
		 (* Should we preprocess regular functions? *)
		 preprocess_functions functions verifiable,
		 attr))
