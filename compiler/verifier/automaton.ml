(* Describes a state transition automaton and the functions that go with it.
 * Model state automaton as a labelled graphs *)

module B = Objects
module CS = Class_state
module Util = Verif_util

(********************** Types *****************************)

(* A state can be a core state or a process state. An automaton can have
 * a single set *)
type states =
    CLASS of Class_state.classname
  | CORE_CLASS of Class_state.corename

(* Edge consists of a source, a destination, and the list
 * of events that can cause that transition *)
type edge =
    EDGE of states * (* source *)
            states * (* destination *)
            string list (* events that can induce the transition *)

type automaton =
    AUT of string   (* Name of automaton *)
        * edge list (* Edges and nodes *)


(************** Environment *****************************************)

(* This automaton describes transitions that are valid only
 * for the target process or core in the event, the exception
 * to that is the schedule event, where a process (we don't know which one,
 * is allowed to be scheduled*)
let process_transitions = (ref (AUT("process",[])): automaton ref)
let core_transitions =  (ref (AUT("core",[])): automaton ref)

(************* Utility Methods (manipulate automaton) ****************)

(* Utility / error  functions *)

let is_in_edge (EDGE(src,dest,events):edge) (event_name:string) : bool =
    (Aux.member event_name events)

(* Checks whether a  given event has been defined in the global
 * process event structure *)
let is_defined_pevent (name:string list) : bool =
    let event_names = List.map
        (fun (Ast.EVENT_NAME(id,attr)) -> B.id2c id)
        !Events.pevents in
 List.fold_left (fun accu x -> accu && (Aux.member x event_names)) true name

(* Checks whether a given event has been defined in the global
 * core event structure *)
let is_defined_cevent (name:string list ) : bool =
 let event_names = List.map
    (fun (Ast.EVENT_NAME(id,attr)) -> B.id2c id)
    !Events.cevents in
 List.fold_left (fun accu x -> Aux.member x event_names) true name


(************* High-level functions **********************************)

(* Initialises core automaton with corresponding core names, events.
 * This function needs to be updated if the events change *)
let init_core_automaton _ : unit =
    assert(true)

(* Initialises process automaton with corresponding process class
 * names, events. This function needs to be updated if the events
 * change *)
let init_process_automaton _ : unit =
    let init_transition = (EDGE(CLASS(CS.NOWHERE),CLASS(CS.READY),[])) in
    let transitions= [
        (EDGE(CLASS(CS.NOWHERE),CLASS(CS.READY),["new"]));
        (EDGE(CLASS(CS.READY),CLASS(CS.TERMINATED),["detach"]));
        (EDGE(CLASS(CS.READY),CLASS(CS.RUNNING),["schedule"]));
        (EDGE(CLASS(CS.READY),CLASS(CS.BLOCKED),["block"]));
        (EDGE(CLASS(CS.RUNNING),CLASS(CS.TERMINATED),["detach"]));
        (EDGE(CLASS(CS.RUNNING),CLASS(CS.BLOCKED),["block"]));
        (EDGE(CLASS(CS.RUNNING),CLASS(CS.READY),["yield";"tick"]));
        (EDGE(CLASS(CS.RUNNING),CLASS(CS.RUNNING),["yield";"tick"]));
        (EDGE(CLASS(CS.BLOCKED),CLASS(CS.READY),["unblock"]));
        (EDGE(CLASS(CS.BLOCKED),CLASS(CS.TERMINATED),["detach"]));
        (EDGE(CLASS(CS.ANYWHERE),CLASS(CS.TERMINATED),["detach"]))
    ] in 
    process_transitions:= AUT("process", init_transition::transitions)

(* Verify that the generated automaton is consistent:
 * All states defined in system must be included in automaton
 * All events defined in system must be included in automaton
 * Conversely, the states and events mentioned here must be referred
 * to in the system
 * Automaton contains either exclusively CLASS types or CORE_CLASS types.
 * That is, it is either the automaton for a process, or for a core. *)
let verify_consistent (AUT(name,edges): automaton ) (is_process: bool): unit =
  let check_fn = (fun (EDGE(src,dest,events)) ->
      match src, dest with
          CLASS(name_src), CLASS(name_dest) -> (
            if not is_process then raise (Error.Error "Cannot mix process and events")
	    else if not (is_defined_pevent events) then
	      raise (Error.Error "There are some missing process events")
          )
        | CORE_CLASS(_), CORE_CLASS(_) -> (
          if is_process then raise (Error.Error "Cannot mix process and events")
	  else if not (is_defined_cevent events) then
	    raise (Error.Error "There are some missing core events")
	    )
        | _ -> raise (Error.Error "Cannot mix process and events")
  ) in
  List.iter check_fn edges

(* List all transitions that arise when receiving an event matching this name *)
let for_event (AUT(name,edges): automaton) (event: Ast.event_name): edge list =
    let event_st = Ast.event_name2c event in
    let filter_fn = fun edge -> is_in_edge edge event_st in
    List.filter filter_fn edges

let rec classify_expression (event_name:string) (target_field:string) (exp: Ast.expr) env =
  let strexp = Ast.exp2c exp in
  let line = string_of_int (B.line (Ast.get_exp_attr exp)) in
  match exp with
    Ast.VAR(B.ID("READY",_), _, _) -> CLASS(CS.READY)
  |  Ast.VAR(id, _, _) | Ast.FIELD(_, id, _, _) ->
	begin
	  match (Util.type_pstate exp) with
              None ->
		if strexp = target_field then
		  match event_name with
		      "new" -> CLASS(CS.NOWHERE)
		    | "detach"-> CLASS(CS.ANYWHERE)
		    | "tick" | "yield" | "block" -> CLASS(CS.RUNNING)
		    | "unblock" -> CLASS(CS.BLOCKED)
		    | "schedule" -> CLASS(CS.NOWHERE)
		    | _ -> failwith (line^": Error handling event "^event_name^
					" with expression "^strexp)
		else
		  begin
		    try
		      let exp2 = Util.lookfor_exp id env in
		      classify_expression event_name target_field exp2 env
		    with _ ->
		      failwith (__LOC__ ^": "^line ^ ": Unable to classify var/field "^ strexp ^
				  " in event "^event_name)
		  end
	  | Some src -> CLASS(src)
	end
    | Ast.FIRST(fexp, _, _, _) ->
      classify_expression event_name target_field fexp env
    | _ ->
      failwith (__LOC__ ^": "^ line^": Unable to classify expression "^ strexp ^
		   " in event "^event_name)

(* Function generates a function that will test whether a given
 * move statement is a valid transition for this event.
 * Some of the information from the source event cannot be inferred
 * statically, and relies on assumptions about the source.
 * Additionally returns a flag that states whether a transition
 * must occur for this process*)
let generate_filter_process (edges:edge list) (event_name: string)
                       : ((string -> Ast.stmt option -> Util.environment -> bool)*bool) =
  let must_move =
    List.fold_left
      (fun accu edge ->
	match edge with
	    EDGE(CLASS(src),CLASS(tgt),_) ->
              if src = tgt then false
	      else accu
	  | EDGE _ -> accu)
      true edges in
  let return_function: string -> Ast.stmt option -> Util.environment -> bool =
    (fun target_field stmt env ->
      match stmt with
          Some (Ast.MOVE(src,target,_,_,_,_,_,att)) -> (
            let type_src = classify_expression event_name target_field src env in
            let type_target = (
              match (Util.type_pstate target) with
                  None -> None
                | Some target -> Some(CLASS(target)))
            in
            let dest_edges = List.fold_left
              ( fun d_edges (EDGE(src,dest,_)) ->
		if type_src = src then dest::d_edges else d_edges)
              [] edges in
            match type_target with
                None -> false
              | Some(type_target) ->
                Aux.member type_target dest_edges
            )
        | None -> true
        | _ -> false) in
  (return_function, must_move)

(* Assumption: current automaton assumes that events that are not the 
 * target cannot change state *)
let verify_event ((Ast.EVENT(nm,params,body,syn,attr)): Ast.event_decl)
    : (bool * edge list * string ) option =
  let event_name = Ast.event_name2c nm in
    (* FIXME: The compute_state event is added by the pre_type phase
       Nico: Is it still useful ?
    *)
  if (event_name = "compute_state") then None
  else (
    let edges =  for_event !process_transitions nm in
    let (id,att) = match params with 
        Ast.VARDECL(_,id,_,_,_,_,att) -> (id,att) in
    let target_field = Ast.mkFIELD(Ast.VAR(id,[],att), "target",[],-1) in
 
    (* In Schedule, allow any process in ready to become running *)
    let cores: (Ast.stmt -> bool)   =  (match event_name with
        "schedule" -> (fun stmt -> true)
      | "tick" | "yield" | "block" -> ((fun stmt -> match stmt with
          Ast.MOVE(src,dest,_,_,_,_,_,_) ->
	    if Ast.exp2c src = Ast.exp2c target_field then
	      true
	    else
              let type_src =
		match (Util.type_pstate src) with
                    None -> CLASS(CS.ANYWHERE)
                  | Some src -> CLASS(src)
              in
              (type_src = CLASS(CS.RUNNING))
          | _ -> false))
      | _ -> ((fun stmt -> match stmt with
          Ast.MOVE(src,dest,_,_,_,_,_,_) ->
            Util.is_equal src target_field
          | _ -> false))) in
    let cores_neg: (Ast.stmt -> bool) = (match event_name with
        "schedule" -> (fun stmt -> false)
      | "tick" | "yield" | "block"  -> ((fun stmt -> match stmt with
          Ast.MOVE(src,dest,_,_,_,_,_,_) ->
            let type_src =
              match (Util.type_pstate src) with
                  None -> CLASS(CS.ANYWHERE)
                | Some src -> CLASS(src)
            in
            not(Util.is_equal src target_field || (type_src = CLASS(CS.RUNNING)))
          | _ -> false))
      | _ -> ((fun stmt -> match stmt with
          Ast.MOVE(src,dest,_,_,_,_,_,_) ->
            not (Util.is_equal src target_field)
          | _ -> false))) in

    (* Extracts all transitions for core target *)
    let transitions_me = Util.extract_move_unique cores body in
    let transitions_me = Util.evaluate_reachability transitions_me in
   (* Generates a function that checks against allowed transitions*)
    let (func_me,move_req) = generate_filter_process edges event_name in
    let correct_me =
        (* All paths must have some transition *)
        if move_req then Util.check_all_paths transitions_me (func_me (Ast.exp2c target_field))
        (* Otherwise, target process is allowed to remain in the same state *)
        else Util.check_some_paths transitions_me (func_me (Ast.exp2c target_field)) in

    (* Extracts all transitions for other processes *)
    let transitions_other = Util.extract_move_unique cores_neg body in
    let transitions_other = Util.evaluate_reachability transitions_other in
    (* No non-target process is allowed to transition to another state *)
    let correct_other = (transitions_other = None) in
    if not correct_me then
      if event_name = "schedule" then
        Some (move_req,edges,
              "in schedule event")
      else
        Some (move_req, edges,
              "on e.target (or an alias of it) in "^ event_name^" event")
    else if not correct_other then
      Some (move_req, edges,
            "on another process than e.target (or an alias of it) in "^event_name^" event")
    else
      None
  )
