(* Insert the admission criteria.
Criteria:
   At the beginning of the attach function.
Admit_attach:
   At the end of the attach function or at the end of the process.new handler.
Admit_detach:
   At the end of the detach function or at the end of the process.end handler.

Note that the admission criteria is a kind of aspect, but not on state
transitions.

This is done before verification so that collection of information about
process fields being modified can be collected in the normal way.
*)

module B = Objects
module CS = Class_state

let error ln str =
  Error.update_error();
  Printf.printf "%s: %s \n" (B.loc2c ln) str

(* ----------------------- Create insert code ----------------------- *)

(* Real criteria in the single return at the end, but there may be code
before derived from inlining some function *)
let rec split_criteria = function
    Ast.RETURN(Some(exp),_) -> ([],exp)
  | Ast.SEQ(decls,((_::_) as stms),attr) ->
      let rstms = List.rev stms in
      let last = List.hd rstms in
      let rest = List.rev(List.tl rstms) in
      let (criteria_stm_list,criteria) = split_criteria last in
      (rest@criteria_stm_list,criteria)
  | _ -> raise (Error.Error "admit: unexpected criteria")

let create_criteria criteria_params criteria_list decls =
  let critparamids = Ast.params2ids criteria_params in
  List.map
    (function criteria ->
      let (criteria_stm_list,criteria) = split_criteria criteria in
      let crit_attr = Ast.get_exp_attr criteria in
      let free_vars = Ast.collect_exp_vars critparamids criteria in
      B.capture_check free_vars decls (error crit_attr);
      let error_string =
	match B.modl crit_attr with
	  None ->
	    Printf.sprintf "The following criteria was not satisfied:\\n%s\\n"
	      (Ast.exp2c criteria)
	| Some modref ->
	    (match !modref with
	      B.MODULE_NAME nm ->
		Printf.sprintf
		  "The following criteria of module %s was not satisfied:\\n%s\\n"
		  (B.id2c nm) (Ast.exp2c criteria)
	    | _ -> raise (Error.Error "admit not expected in schedule")) in
      Ast.mkBLOCK crit_attr
	(criteria_stm_list@
	 [Ast.IF(Ast.UNARY(Ast.NOT,criteria,B.updty crit_attr B.BOOL),
		 Ast.ERROR(error_string,crit_attr),
		 Ast.SEQ([],[],crit_attr),
		 crit_attr)]))
    criteria_list

let create_attach_detach procexp param stmt decls =
  let stmt_attr = Ast.get_stm_attr stmt in
  (* capture check for body *)
  let free_vars = Ast.collect_stm_vars (Ast.params2ids [param]) stmt in
  B.capture_check free_vars decls (error stmt_attr);
  (* bind the process parameter *)
  Ast.SEQ([Ast.VALDEF(param,procexp,Ast.VARIABLE,stmt_attr)],[stmt],stmt_attr)

(* ---------- Add just before return, ending state change ----------- *)

(* for attach, try to put the new code as high as possible, so that it will
take place while the process is not yet on the scheduler, ie not on ready.
for detach, try to put it below the last state change, so that it will
take place after the process is not on the scheduler, is not on ready. *)

(* Last argument is a reversed list of statements. *)

let rec add_in_list in_attach decls end_code = function
    [] -> (false,[])
  | stmt :: rest ->
      match stmt with
	Ast.IF(tst,thn,els,attr) ->
	  let (placed1,thn) = add_at_end in_attach decls end_code thn attr in
	  let (placed2,els) = add_at_end in_attach decls end_code els attr in
	  (match (placed1,placed2) with
	    (true,true) -> (true,Ast.IF(tst,thn,els,attr) :: rest)
	  | (true,false) ->
	      (true,
	       Ast.IF(tst,thn,Ast.mkBLOCK attr [end_code;els],attr) :: rest)
	  | (false,true) ->
	      (true,
	       Ast.IF(tst,Ast.mkBLOCK attr [end_code;thn],els,attr) :: rest)
	  | (false,false) ->
	      let (placed,rest) = add_in_list in_attach decls end_code rest in
	      (placed, stmt :: rest))
      | Ast.FOR(id,stnm,states,dir,body, _, attr) ->
	  let (placed,body) = add_at_end in_attach decls end_code body attr in
	  if placed
	  then (* can't put end_code in a loop *)
	    (true, end_code :: stmt :: rest)
	  else
	    let (placed,rest) = add_in_list in_attach decls end_code rest in
	    (placed, stmt :: rest)
      | Ast.FOR_WRAPPER(brklbl,stmts,attr) ->
	  let (placed,stmts) =
	    add_in_list in_attach decls end_code (List.rev stmts) in
	  if placed
	  then (* can't put end_code in a loop *)
	    (true,end_code :: stmt :: rest)
	  else
	    let (placed,rest) = add_in_list in_attach decls end_code rest in
	    (placed, stmt :: rest)
      | Ast.SWITCH(exp,cases,def,attr) ->
	  let (placed,new_cases) =
	    List.fold_left
	      (function (placed,new_cases) ->
		function Ast.SEQ_CASE(pat,states,stmt,attr) ->
		  let (placed1,new_stmt) =
		    add_at_end in_attach decls end_code stmt attr in
		  (placed1::placed,
		   Ast.SEQ_CASE(pat,states,new_stmt,attr)::cases))
	      ([],[]) cases in
	  let new_cases = List.rev new_cases in
	  let any_placed = List.exists (function x -> x) placed in
	  let mk_newer_cases _ =
	    List.map2
	      (function Ast.SEQ_CASE(pat,states,stmt,attr) ->
		function
		    true -> Ast.SEQ_CASE(pat,states,stmt,attr)
		  | false ->
		      Ast.SEQ_CASE(pat,states,
				   Ast.SEQ([],[end_code;stmt],attr),
				   attr))
	      new_cases placed in
	  (match def with
	    None ->
	      if any_placed
	      then (true,Ast.SWITCH(exp,mk_newer_cases (),def,attr) :: rest)
	      else
		let (placed,rest) =
		  add_in_list in_attach decls end_code rest in
		(placed, stmt :: rest)
	  | Some stm ->
	      let (placed1,stm) =
		add_at_end in_attach decls end_code stm attr in
	      let any_placed = placed1 || any_placed in
	      if any_placed
	      then (true,Ast.SWITCH(exp,mk_newer_cases (),
				    (if placed1
				    then Some stm
				    else
				      Some(Ast.SEQ([],[end_code;stm],attr))),
				    attr) :: rest)
	      else
		let (placed,rest) =
		  add_in_list in_attach decls end_code rest in
		(placed, stmt :: rest))
      | Ast.SEQ(ldecls,stmts,attr) ->
	  let new_decls = Ast.valdefs2ids ldecls in
	  let (placed1,stmts1) =
	    add_in_list in_attach (new_decls @ decls) end_code
	      (List.rev stmts) in
	  if placed1
	  then (placed1, Ast.SEQ(ldecls,List.rev stmts1,attr) :: rest)
	  else
	    let (placed,rest) = add_in_list in_attach decls end_code rest in
	    (placed, stmt :: rest)
      | Ast.RETURN(_,_)
      | Ast.DEFER(_)
      | Ast.PRIMSTMT(_,_,_)
      | Ast.BREAK(_) ->
	  let (placed,rest) = add_in_list in_attach decls end_code rest in
	  (placed, stmt :: rest)
      | Ast.MOVE(_,_,_,_,_,_,_,_)
      | Ast.MOVEFWD(_,_,_,_) ->
	  if in_attach
	  then
	    let (placed,rest) = add_in_list in_attach decls end_code rest in
	    (placed, stmt :: rest)
	  else (true,(end_code :: stmt :: rest))
      | Ast.SAFEMOVEFWD(exp,srcdst,defstmt,state_end,attr) ->
	  if in_attach
	  then
	    let (placed,defstmt) =
	      add_at_end in_attach decls end_code defstmt attr in
	    if placed
	    then (true, end_code :: stmt :: rest)
	    else
	      let (placed,rest) = add_in_list in_attach decls end_code rest in
	      (placed, stmt :: rest)
	  else (true,(end_code :: stmt :: rest))
      | Ast.ASSIGN(Ast.VAR(id,_,a),rexp,modsrtd,attr) when List.mem id decls ->
	  let (placed,rest) = add_in_list in_attach decls end_code rest in
	  (placed, stmt :: rest)
      | Ast.ASSIGN(lexp,rexp,modsrtd,attr) ->
	  (true,(end_code :: stmt :: rest))
      | Ast.ERROR(str,attr) ->
	  (true,stmt :: rest)
      | Ast.ASSERT(exp,attr) -> raise (Error.Error "unexpected assert")

and add_at_end in_attach decls end_code stmt attr =
  let (placed,stmts) = add_in_list in_attach decls end_code [stmt] in
  (placed,Ast.mkBLOCK attr (List.rev stmts))

let enter_add_at_end in_attach decls end_code stmt attr =
  match add_at_end in_attach decls end_code stmt attr with
    (true,s) -> s
  (* didn't find anywhere it has to be, so put it at the beginning *)
  | (false,s) -> Ast.SEQ([],[end_code;s],attr)
(* ----------------------- Handlers, fuctions ----------------------- *)

let process_handlers attach detach handlers =
  List.map
    (function
	(Ast.EVENT(nm,specd,(Ast.VARDECL(evtype,id,_,_,_,_) as param),stmt,attr)) as x->
	  let process in_attach params update =
	    let end_code =
	      create_attach_detach
        (Ast.FIELD(Ast.VAR(id,[],B.updty attr evtype),
               B.mkId "target", [],
			   B.updty attr
			     (B.PROCESS)))
		params update [id] in
	    let stmt = enter_add_at_end in_attach [id] end_code stmt attr in
	    Ast.EVENT(nm,specd,param,Ast.SEQ([],[stmt],attr),attr) in
	  match (attach,detach,Ast.event_name2c2 nm) with
	    (Some(attach_params,attach),_,Some "process.new") ->
	      process true attach_params attach
	  | (_,Some(detach_params,detach),Some "process.end") ->
	      process false detach_params detach
	  | _ -> x)
    handlers

let process_functions criteria_params criteria_list attach detach functions =
  List.map
    (function Ast.FUNDEF(tl,ret,nm,params,stmt,inl,attr) as x ->
      let fnparamids = Ast.params2ids params in
      let mkcode param update =
	let (proc_param,proc_type) =
	  match List.hd params with
	    Ast.VARDECL(B.PROCESS,id,_,_,_,_) ->
	      (id,B.PROCESS)
	  | _ ->
	      raise
		(Error.Error
		   "unexpected first argument to attach or detach") in
    create_attach_detach (Ast.VAR(proc_param,[],B.updty attr proc_type))
	  (param : Ast.vardecl) update fnparamids in
      let insert_code in_attach code extra =
	let new_code = enter_add_at_end in_attach fnparamids code stmt attr in
	Ast.FUNDEF(tl,ret,nm,params,Ast.SEQ([],extra [new_code],attr),
		   inl,attr) in
      match (attach,detach,B.id2c nm) with
	(Some(attach_param,attach),_,"attach") ->
	  insert_code true (mkcode attach_param attach)
	    (function x ->
	      (create_criteria criteria_params criteria_list fnparamids)@x)
      |	(_,Some(detach_param,detach),"detach") ->
	  insert_code false (mkcode detach_param detach) (function x -> x)
      |	_ -> x)
    functions

let process
    (Ast.SCHEDULER(nm,default,cstdefs,enums,parentfns,procdecls,
		   fundecls,valdefs,states,cstates,
		    criteria,admit,trace,handlers,chandlers,ifunctions,functions,
		      aspects,attr)) =
  let (valdefs,handlers,ifunctions) =
    match admit with
      None -> (valdefs,handlers,ifunctions)
    | Some(Ast.ADMIT(locals,crit_params,crits,attach,detach,attr)) ->
       (valdefs@locals,
	process_handlers attach detach handlers,
	process_functions crit_params crits attach detach ifunctions) in
  Ast.SCHEDULER(nm,default,cstdefs,enums,parentfns,procdecls,
		fundecls,valdefs,states,cstates,
		criteria,None,trace,handlers,chandlers,ifunctions,functions,
		      aspects,attr)
