(* This takes care of all event selection required because of the use of
the hierarchy.  This impacts the decision of how to determine the type of
each event handler, which handlers to trace, which handlers to specialize,
and how to distribute handlers between top-level functions and the generic
scheduler function.

This phase helps with these decisions.  This phase is given a list of names
aout which information is known (e.g. names for which types are defined or
names that are to be traced) and a list of names for which handler
definitions are provided, and returns an association list mapping each name
about which information is known to a list of names of handlers that are
described by the name. *)

(*
Let [a] be any sequence of hierarchical levels, not ending with a *.

In type checking, if we have a type for [a], then we check anything that
matches this exactly.  If nothing matches it exactly, then we check
anything of the form [a'].*, where a' is the maximal prefix of a that is
not equal to a and for which a handler exists for [a'].*.

Also, in type checking, if we have a type for [a].*, then we check anything
that matches this exactly or has the form [a].[b].* or [a].[b], for any b.
Regardless of whether this turns up any matches, we also check anything of
the form [a'].*, where a' is the maximal prefix of a for which a handler
exists for [a'].*.  In this case, a' can be equal to a.
*)

module B = Objects

let find_exact_match name handler_names =
  match List.filter (Ast.event_name_equal name) handler_names with
    [] -> None
  | [x] -> Some x
  | _ -> raise(Error.Error "duplicate event name")

(* strictly more specific *)
let find_more_specific_matches name handler_names =
  List.filter
    (function (Ast.EVENT_NAME(nm,a) as n) ->
      not (Ast.event_name_equal n name) &&
      (let pref = Aux.non_empty_prefixes nm in
      let all =
	List.rev (List.map (function p -> Ast.EVENT_NAME(p,true,a)) pref) in
      List.exists (Ast.event_name_equal name) all))
    handler_names

let find_less_specific_match (Ast.EVENT_NAME(nm,star,a) as x) handler_names =
  let pref = Aux.non_empty_prefixes nm in
  let all =
    List.rev(List.map (function p -> Ast.EVENT_NAME(p,true,a)) pref) in
  let rec loop = function
      [] ->  None
    | (max::rest) ->
	(match List.filter (Ast.event_name_equal max) handler_names with
	  [] -> loop rest
	| [res] -> Some res
	| _ -> raise(Error.Error "duplicate event name")) in
  loop all

let find info_name (handler_names : Ast.event_name list) =
  match info_name with
    Ast.EVENT_NAME(nm,true,a) ->
      (match Ast.event_name2c info_name with
	"unblock.timer.target.*"
      | "unblock.timer.notarget.*" 
      | "unblock.timer.persistent.*" ->
	  let less = find_less_specific_match info_name handler_names in
	  let more = find_more_specific_matches info_name handler_names in
	  (less,more)
      |	_ ->
	  (match find_less_specific_match info_name handler_names with
	    None ->
	      raise(Error.Error(Printf.sprintf "no match for event %s"
				  (Ast.event_name2c info_name)))
	  | x -> (x, find_more_specific_matches info_name handler_names)))
  | Ast.EVENT_NAME(nm,false,a) ->
      (match find_exact_match info_name handler_names with
	None ->
	  (match find_less_specific_match info_name handler_names with
	    None ->
	      raise(Error.Error(Printf.sprintf "no match for event %s"
				  (Ast.event_name2c info_name)))
	  | Some res ->
	      if Ast.event_name_equal (Ast.EVENT_NAME(nm,true,a)) res
	      then
		raise(Error.Error
			(Printf.sprintf
			   "event name %s occurs both starred and unstarred"
			   (Ast.event_name2c (Ast.EVENT_NAME(nm,true,a)))));
	      (Some res, []))
      |	Some x -> (None,[x]))

let compare_names (Ast.EVENT_NAME(nm1,_,_)) (Ast.EVENT_NAME(nm2,_,_)) =
  compare nm1 nm2

(* ----------------------------- timers ----------------------------- *)

let check_timers env names =
  List.iter
    (function (nm,hnames) ->
      if Ast.event_name2c nm = "unblock.timer.*"
      then
	let required_names =
	  List.map
	    (function n -> "unblock.timer."^(B.id2c n))
	    names in
	let provided_names =
	  List.map Ast.event_name2c hnames in
	if not (Aux.subset provided_names required_names &&
		Aux.subset required_names provided_names)
	then
	  begin
	    Printf.printf "required timer names:\n";
	    Printf.printf "%s\n" (Aux.set2c required_names);
	    Printf.printf "provided timer names:\n";
	    Printf.printf "%s\n" (Aux.set2c provided_names);
	    raise(Error.Error "incorrect set of timer event names")
	  end
	else ()
      else ())
    env

(* -------------------------- entry point --------------------------- *)

(* Hierarchical event types *)
let associate info_names (handler_names : Ast.event_name list)
     procdecls globaldefs =
  let info_names = List.rev (List.sort compare_names info_names) in
  let (_,res) =
    List.fold_left
      (function (handler_names,res) ->
	function iname ->
	  let (less_specific,more_specific) = find iname handler_names in
	  let new_handler_names = Aux.subtract handler_names more_specific in
	  match less_specific with
	    Some x -> (new_handler_names,(iname, x::more_specific)::res)
	  | None -> (new_handler_names,(iname, more_specific)::res))
      (handler_names,[]) info_names in
  check_timers res (Ast.collect_timer_names procdecls globaldefs);
  res
